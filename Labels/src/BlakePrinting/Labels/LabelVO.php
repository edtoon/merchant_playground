<?php

namespace BlakePrinting\Labels;

use BlakePrinting\Db\ValueObject;
use JsonSerializable;

class LabelVO implements JsonSerializable, ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $creator = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $updater = null;
    /** @var int */
    private $labelTypeId = null;
    /** @var int */
    private $locationId = null;

    public function jsonSerialize() {
        return [
            "id" => $this->id,
            "created" => $this->created,
            "creator" => $this->creator,
            "updated" => $this->updated,
            "updater" => $this->updater,
            "label_type_id" => $this->labelTypeId,
            "location_id" => $this->locationId
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param int $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getUpdater()
    {
        return $this->updater;
    }

    /**
     * @param int $updater
     */
    public function setUpdater($updater)
    {
        $this->updater = $updater;
    }

    /**
     * @return int
     */
    public function getLabelTypeId()
    {
        return $this->labelTypeId;
    }

    /**
     * @param int $labelTypeId
     */
    public function setLabelTypeId($labelTypeId)
    {
        $this->labelTypeId = $labelTypeId;
    }

    /**
     * @return int
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * @param int $locationId
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;
    }
}
