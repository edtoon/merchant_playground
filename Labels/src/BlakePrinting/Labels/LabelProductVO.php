<?php

namespace BlakePrinting\Labels;

use BlakePrinting\Db\ValueObject;
use BlakePrinting\Util\Utils;
use JsonSerializable;

class LabelProductVO implements JsonSerializable, ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $labelId = null;
    /** @var int */
    private $productId = null;
    /** @var int */
    private $quantity = null;
    /** @var int */
    private $year = null;
    /** @var int */
    private $supplierOrganizationId = null;
    /** @var int */
    private $catalogQualityGradeId = null;

    public function jsonSerialize() {
        return [
            "vo" => Utils::getReflectionClassByName(get_class($this))->getShortName(),
            "id" => $this->id,
            "created" => $this->created,
            "updated" => $this->updated,
            "label_id" => $this->labelId,
            "product_id" => $this->productId,
            "quantity" => $this->quantity,
            "year" => $this->year,
            "supplier_organization_id" => $this->supplierOrganizationId,
            "catalog_quality_grade_id" => $this->catalogQualityGradeId
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getLabelId()
    {
        return $this->labelId;
    }

    /**
     * @param int $labelId
     */
    public function setLabelId($labelId)
    {
        $this->labelId = $labelId;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param int $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return int
     */
    public function getSupplierOrganizationId()
    {
        return $this->supplierOrganizationId;
    }

    /**
     * @param int $supplierOrganizationId
     */
    public function setSupplierOrganizationId($supplierOrganizationId)
    {
        $this->supplierOrganizationId = $supplierOrganizationId;
    }

    /**
     * @return int
     */
    public function getCatalogQualityGradeId()
    {
        return $this->catalogQualityGradeId;
    }

    /**
     * @param int $catalogQualityGradeId
     */
    public function setCatalogQualityGradeId($catalogQualityGradeId)
    {
        $this->catalogQualityGradeId = $catalogQualityGradeId;
    }
}
