<?php

namespace BlakePrinting\Labels;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static LabelTypeDAO instance(PDO $connection = null, $schemaName = null)
 * @method LabelTypeVO findById($id, $options = null, $useCache = false)
 * @method LabelTypeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method LabelTypeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method LabelTypeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method LabelTypeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method LabelTypeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method LabelTypeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method LabelTypeVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class LabelTypeDAO extends AbstractDataAccessObject {
    const LABEL_TYPE_INVENTORY_BOX = "box";
    const LABEL_TYPE_INVENTORY_PALLET = "pallet";
    const LABEL_TYPE_INVENTORY_SHELF = "shelf";
}
