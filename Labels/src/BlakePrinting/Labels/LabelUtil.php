<?php

namespace BlakePrinting\Labels;

use BlakePrinting\Catalog\CatalogPartDAO;
use BlakePrinting\Catalog\CatalogProductDAO;
use BlakePrinting\Db\PDOQuery;
use PDO;

class LabelUtil {
    private function __construct() { }

    /**
     * @param int $labelId
     * @return array
     */
    public static function &getLabelContents($labelId, PDO $connection = null) {
        $catalogPartDao =& CatalogPartDAO::instance($connection);
        $catalogProductDao =& CatalogProductDAO::instance($connection);
        $labelPartDao =& LabelPartDAO::instance($connection);
        $labelProductDao =& LabelProductDAO::instance($connection);

        return PDOQuery::select()
            ->column("label_part.id")
            ->column("'Part' AS type")
            ->column("catalog_part.name")
            ->column("quantity")
            ->column("year")
            ->column("supplier_organization_id")
            ->column("catalog_quality_grade_id")
            ->column("COALESCE(label_part.updated, label_part.created) as updated")
            ->from($labelPartDao)
            ->join($catalogPartDao, ["label_part.part_id = catalog_part.id"])
            ->where("label_part.label_id", $labelId, PDO::PARAM_INT)
            ->enclose(PDOQuery::QUERY_ENCLOSE_BODY)
            ->unionAll(
                PDOQuery::select()
                    ->column("label_product.id")
                    ->column("'Product' AS type")
                    ->column("catalog_product.name")
                    ->column("quantity")
                    ->column("year")
                    ->column("supplier_organization_id")
                    ->column("catalog_quality_grade_id")
                    ->column("COALESCE(label_product.updated, label_product.created) as updated")
                    ->from($labelProductDao)
                    ->join($catalogProductDao, ["label_product.product_id = catalog_product.id"])
                    ->where("label_product.label_id", $labelId, PDO::PARAM_INT)
                    ->enclose()
            )
            ->orderBy("updated DESC")
            ->getResults($connection)
        ;
    }
}
