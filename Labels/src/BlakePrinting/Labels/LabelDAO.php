<?php

namespace BlakePrinting\Labels;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static LabelDAO instance(PDO $connection = null, $schemaName = null)
 * @method LabelVO findById($id, $options = null, $useCache = false)
 * @method LabelVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method LabelVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method LabelVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method LabelVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method LabelVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method LabelVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class LabelDAO extends AbstractDataAccessObject {
}
