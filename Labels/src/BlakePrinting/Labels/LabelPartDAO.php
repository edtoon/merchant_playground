<?php

namespace BlakePrinting\Labels;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static LabelPartDAO instance(PDO $connection = null, $schemaName = null)
 * @method LabelPartVO findById($id, $options = null, $useCache = false)
 * @method LabelPartVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method LabelPartVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method LabelPartVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method LabelPartVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method LabelPartVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method LabelPartVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class LabelPartDAO extends AbstractDataAccessObject {
    /**
     * @return bool|int
     */
    public function &store(LabelPartVO $labelPartVo) {
        $connection =& $this->getConnection();
        $id = $labelPartVo->getId();

        if($id === null) {
            return PDOQuery::insert()
                ->into($this)
                ->value("created", "UNIX_TIMESTAMP()")
                ->value("label_id", $labelPartVo->getLabelId(), PDO::PARAM_INT)
                ->value("part_id", $labelPartVo->getPartId(), PDO::PARAM_INT)
                ->value("quantity", $labelPartVo->getQuantity(), PDO::PARAM_INT)
                ->value("year", $labelPartVo->getYear(), PDO::PARAM_INT)
                ->value("supplier_organization_id", $labelPartVo->getSupplierOrganizationId(), PDO::PARAM_INT)
                ->value("catalog_quality_grade_id", $labelPartVo->getCatalogQualityGradeId(), PDO::PARAM_INT)
                ->executeEnsuringLastInsertId($connection)
            ;
        } else {
            $updateCount =& PDOQuery::update()
                ->table($this)
                ->set("updated", "UNIX_TIMESTAMP()")
                ->setIfNotNull("label_id", $labelPartVo->getLabelId(), PDO::PARAM_INT)
                ->setIfNotNull("part_id", $labelPartVo->getPartId(), PDO::PARAM_INT)
                ->setIfNotNull("quantity", $labelPartVo->getQuantity(), PDO::PARAM_INT)
                ->setIfNotNull("year", $labelPartVo->getYear(), PDO::PARAM_INT)
                ->setIfNotNull("supplier_organization_id", $labelPartVo->getSupplierOrganizationId(), PDO::PARAM_INT)
                ->setIfNotNull("catalog_quality_grade_id", $labelPartVo->getCatalogQualityGradeId(), PDO::PARAM_INT)
                ->where("id", $id, PDO::PARAM_INT)
                ->executeGetRowCount($connection)
            ;
            $updated = ($updateCount > 0);

            return $updated;
        }
    }

    /**
     * @param int $labelPartId
     * @return int
     */
    public function &delete($labelPartId) {
        return PDOQuery::delete()
            ->from($this)
            ->where("id", $labelPartId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
