<?php

namespace BlakePrinting\Labels;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static LabelProductDAO instance(PDO $connection = null, $schemaName = null)
 * @method LabelProductVO findById($id, $options = null, $useCache = false)
 * @method LabelProductVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method LabelProductVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method LabelProductVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method LabelProductVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method LabelProductVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method LabelProductVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class LabelProductDAO extends AbstractDataAccessObject {
    /**
     * @return bool|int
     */
    public function &store(LabelProductVO $labelProductVo) {
        $connection =& $this->getConnection();
        $id = $labelProductVo->getId();

        if($id === null) {
            return PDOQuery::insert()
                ->into($this)
                ->value("created", "UNIX_TIMESTAMP()")
                ->value("label_id", $labelProductVo->getLabelId(), PDO::PARAM_INT)
                ->value("product_id", $labelProductVo->getProductId(), PDO::PARAM_INT)
                ->value("quantity", $labelProductVo->getQuantity(), PDO::PARAM_INT)
                ->value("year", $labelProductVo->getYear(), PDO::PARAM_INT)
                ->value("supplier_organization_id", $labelProductVo->getSupplierOrganizationId(), PDO::PARAM_INT)
                ->value("catalog_quality_grade_id", $labelProductVo->getCatalogQualityGradeId(), PDO::PARAM_INT)
                ->executeEnsuringLastInsertId($connection)
            ;
        } else {
            $updateCount =& PDOQuery::update()
                ->table($this)
                ->set("updated", "UNIX_TIMESTAMP()")
                ->setIfNotNull("label_id", $labelProductVo->getLabelId(), PDO::PARAM_INT)
                ->setIfNotNull("product_id", $labelProductVo->getProductId(), PDO::PARAM_INT)
                ->setIfNotNull("quantity", $labelProductVo->getQuantity(), PDO::PARAM_INT)
                ->setIfNotNull("year", $labelProductVo->getYear(), PDO::PARAM_INT)
                ->setIfNotNull("supplier_organization_id", $labelProductVo->getSupplierOrganizationId(), PDO::PARAM_INT)
                ->setIfNotNull("catalog_quality_grade_id", $labelProductVo->getCatalogQualityGradeId(), PDO::PARAM_INT)
                ->where("id", $id, PDO::PARAM_INT)
                ->executeGetRowCount($connection)
            ;
            $updated = ($updateCount > 0);

            return $updated;
        }
    }

    /**
     * @param int $labelProductId
     * @return int
     */
    public function &delete($labelProductId) {
        return PDOQuery::delete()
            ->from($this)
            ->where("id", $labelProductId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
