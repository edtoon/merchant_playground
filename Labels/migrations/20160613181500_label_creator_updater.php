<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Labels\LabelDAO;
use BlakePrinting\Users\UserDAO;

class LabelCreatorUpdater extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();
        $labelDao =& LabelDAO::instance($connection);
        $userDao =& UserDAO::instance($connection);
        $userTbl = $userDao->formatSchemaAndTableName();

        $this->alterDao(
            $labelDao,
            "ADD creator SMALLINT(5) UNSIGNED, " .
            "ADD CONSTRAINT label_ibfk_4 FOREIGN KEY (creator) REFERENCES " . $userTbl . " (id), " .
            "ADD updater SMALLINT(5) UNSIGNED, " .
            "ADD CONSTRAINT label_ibfk_5 FOREIGN KEY (updater) REFERENCES " . $userTbl . " (id)"
        );
        PDOQuery::update()
            ->table($labelDao)
            ->set("creator", "user_id")
            ->executeGetRowCount($connection)
        ;
        $this->alterDao($labelDao, "DROP FOREIGN KEY label_ibfk_2, DROP COLUMN user_id");
    }

    public function down() {
        $connection =& $this->getConnection();
        $labelDao =& LabelDAO::instance($connection);
        $userDao =& UserDAO::instance($connection);

        $this->alterDao(
            $labelDao,
            "ADD user_id SMALLINT(5) UNSIGNED, " .
            "ADD CONSTRAINT label_ibfk_2 FOREIGN KEY (user_id) REFERENCES " . $userDao->formatSchemaAndTableName() . " (id)"
        );
        PDOQuery::update()
            ->table($labelDao)
            ->set("user_id", "creator")
            ->executeGetRowCount($connection)
        ;
        $this->alterDao($labelDao, "DROP FOREIGN KEY label_ibfk_5, DROP COLUMN updater, DROP FOREIGN KEY label_ibfk_4, DROP COLUMN creator");
    }
}
