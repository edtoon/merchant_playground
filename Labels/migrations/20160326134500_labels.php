<?php

use BlakePrinting\Catalog\CatalogPartDAO;
use BlakePrinting\Catalog\CatalogProductDAO;
use BlakePrinting\Catalog\CatalogQualityGradeDAO;
use BlakePrinting\Db\DbMigration;
use BlakePrinting\Labels\LabelDAO;
use BlakePrinting\Labels\LabelPartDAO;
use BlakePrinting\Labels\LabelProductDAO;
use BlakePrinting\Labels\LabelTypeDAO;
use BlakePrinting\Locations\LocationDAO;
use BlakePrinting\Organizations\OrganizationDAO;
use BlakePrinting\Users\UserDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class Labels extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $labelTypeDao = new LabelTypeDAO($connection);
        $this->tableDao($labelTypeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $labelDao = new LabelDAO($connection);
        $userDao = new UserDAO($connection);
        $locationDao = new LocationDAO($connection);
        $this->tableDao($labelDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("label_type_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("user_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("location_id", "integer", ["signed" => false])
            ->addForeignKey("label_type_id", $labelTypeDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("user_id", $userDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("location_id", $locationDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->save()
        ;

        $labelPartDao = new LabelPartDAO($connection);
        $catalogPartDao = new CatalogPartDao($connection);
        $catalogQualityGradeDao = new CatalogQualityGradeDao($connection);
        $organizationDao = new OrganizationDAO($connection);
        $labelTbl = $labelDao->formatSchemaAndTableName();
        $organizationTbl = $organizationDao->formatSchemaAndTableName();
        $catalogQualityGradeTbl = $catalogQualityGradeDao->formatSchemaAndTableName();
        $this->tableDao($labelPartDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("label_id", "biginteger", ["signed" => false])
            ->addColumn("part_id", "biginteger", ["signed" => false])
            ->addColumn("quantity", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addColumn("year", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("supplier_organization_id", "integer", ["signed" => false])
            ->addColumn("catalog_quality_grade_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addForeignKey("label_id", $labelTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("part_id", $catalogPartDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("supplier_organization_id", $organizationTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("catalog_quality_grade_id", $catalogQualityGradeTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("quantity")
            ->addIndex("year")
            ->save()
        ;

        $labelProductDao = new LabelProductDAO($connection);
        $catalogProductDao = new CatalogProductDao($connection);
        $this->tableDao($labelProductDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("label_id", "biginteger", ["signed" => false])
            ->addColumn("product_id", "biginteger", ["signed" => false])
            ->addColumn("quantity", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addColumn("year", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("supplier_organization_id", "integer", ["signed" => false])
            ->addColumn("catalog_quality_grade_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addForeignKey("label_id", $labelTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("product_id", $catalogProductDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("supplier_organization_id", $organizationTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("catalog_quality_grade_id", $catalogQualityGradeTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("quantity")
            ->addIndex("year")
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(new LabelProductDAO($connection));
        $this->dropDao(new LabelPartDAO($connection));
        $this->dropDao(new LabelDAO($connection));
        $this->dropDao(new LabelTypeDAO($connection));
    }
}
