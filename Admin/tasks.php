<?php

require_once(__DIR__ . "/../vendor/autoload.php");

$executor = new BlakePrinting\Admin\Tasks\AdminTaskExecutor();
$executor->run();
