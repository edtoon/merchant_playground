<?php

namespace BlakePrinting\Admin\Users;

use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Users\UserDAO;
use BlakePrinting\Users\UserRoleDAO;
use BlakePrinting\Users\UserVO;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use Monolog\Logger;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use PDO;

class UserProvider implements UserProviderInterface {
    /** @var array */
    private static $instances = [];
    /** @var PDO */
    private $connection = null;
    /** @var UserVO */
    private $userVo = null;

    public static function &instance(PDO $connection = null) {
        if($connection === null) {
            $connection =& PdoUtil::getMysqlConnection();
        }

        $connectionId = PDOUtil::getUniqueIdentifierForConnection($connection);
        $instance = null;

        if(array_key_exists($connectionId, self::$instances)) {
            $instance =& self::$instances[$connectionId];
        } else {
            $instance = new self($connection);

            self::$instances[$connectionId] =& $instance;
        }

        return $instance;
    }

    private function __construct(PDO $connection) {
        $this->connection =& $connection;
    }

    /**
     * @return UserVO
     */
    public function getUserVo() {
        return $this->userVo;
    }

    public function loadUserByUsername($username) {
        if(empty($username) || $username == "NONE_PROVIDED") {
            $ex = new UsernameNotFoundException("Missing username.");

            LogUtil::logException(Logger::DEBUG, $ex, "Missing username");

            throw $ex;
        }

        $user = $this->getUser($username);

        if($user === null) {
            throw new UsernameNotFoundException("No user with name '" . $username . "' was found.");
        }

        return $user;
    }

    public function refreshUser(UserInterface $user) {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        $storedUser = $this->getUser($user->getUsername());

        if($storedUser === null) {
            throw new UnsupportedUserException(sprintf('User "%s" not found.', $user->getUsername()));
        }

        return new User($storedUser->getUserId(), $storedUser->getUsername(), $storedUser->getPassword(), $storedUser->getRoles(), $storedUser->isEnabled(), $storedUser->isAccountNonExpired(), $storedUser->isCredentialsNonExpired() && $storedUser->getPassword() === $user->getPassword(), $storedUser->isAccountNonLocked());
    }

    public function supportsClass($class) {
        return $class === User::class;
    }

    /**
     * @param string $username
     * @return User
     */
    private function &getUser($username) {
        $userDao = new UserDAO($this->connection);
        $userVo =& $userDao->findByUsername($username);
        $user = null;

        if($userVo !== null) {
            $userRoleDao = new UserRoleDAO($this->connection);
            $roles =& $userRoleDao->listRoles($userVo->getId());
            $expiration = $userVo->getExpiration();
            $isExpired = ($expiration !== null && $expiration <= Utils::getUtcTime());
            $user = new User(
                $userVo->getId(), $userVo->getName(), $userVo->getPassword(), $roles,
                $userVo->getEnabled(), !$isExpired, true, ($userVo->getLocked() !== true)
            );
        }

        $this->userVo =& $userVo;

        return $user;
    }
}
