<?php

namespace BlakePrinting\Admin\Users;

use BlakePrinting\Admin\AppUtil;
use BlakePrinting\Admin\Sessions\AdminSessionEventDAO;
use BlakePrinting\Admin\Sessions\AdminSessionEventLogDAO;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Facilities\FacilityDAO;
use BlakePrinting\Globalization\TimezoneVO;
use BlakePrinting\Users\UserDAO;
use BlakePrinting\Admin\WebApplication;
use BlakePrinting\Users\Entities\UserFacilityDAO;
use BlakePrinting\Users\UserRoleDAO;
use BlakePrinting\Users\UserUtil;
use BlakePrinting\Util\Utils;
use DateTime;
use DateTimeZone;
use PDO;
use ReflectionClass;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UsersControllerProvider implements ControllerProviderInterface {
    public function connect(Application $app) {
        $controllers = $app["controllers_factory"];

        if($app instanceof WebApplication) {
            $controllers->get("/", self::class . "::displayUsers")->bind("users");
            $controllers->get("/users.json", self::class . "::jsonUsers")->bind("users_json");
            $controllers->get("/{userName}/add_role", self::class . "::addRole")->bind("users_add_role");
            $controllers->get("/{userName}/remove_role", self::class . "::removeRole")->bind("users_remove_role");
            $controllers->get("/{userName}/add_facility", self::class . "::addFacility")->bind("users_add_facility");
            $controllers->get("/{userName}/remove_facility", self::class . "::removeFacility")->bind("users_remove_facility");
            $controllers->get("/{userName}/events.json", self::class . "::jsonUserEvents")->bind("users_events_json");
            $controllers->get("/{userName}", self::class . "::displayUser")->bind("users_display");
        }

        return $controllers;
    }

    public function displayUsers(WebApplication $app) {
        if(!$app->isGranted(UserRoleDAO::ROLE_ADMIN)) {
            throw new AccessDeniedException();
        }

        return $app["twig"]->render("users/users.html.twig", [
            "nav_section" => "users"
        ]);
    }

    public function displayUser(WebApplication $app, $userName) {
        if(!$app->isGranted(UserRoleDAO::ROLE_ADMIN)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $userDao = new UserDao($connection);
        $userVo =& $userDao->findByUsername($userName);

        if($userVo === null) {
            $app->abort(400, "Username '" . $userName . "' not found");
        }

        $userRoleDao = new UserRoleDAO($connection);
        $roles =& $userRoleDao->listRoles($userVo->getId());
        $reflectionClass =& Utils::getReflectionClassByName(UserRoleDAO::class);
        $constants = array_keys($reflectionClass->getConstants());
        $possibleRoles = array_diff($constants, $roles);

        $facilities =& UserUtil::getUserFacilities($userVo->getId(), $connection, "name ASC");
        $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
        $possibleFacilities = ($app->isGranted(UserRoleDAO::ROLE_SUPERADMIN) ?
            FacilityDAO::instance($connection)->findAll(null, "name ASC") :
            UserUtil::getUserFacilities($userId, $connection, "name ASC")
        );

        return $app["twig"]->render("users/users_user.html.twig", [
            "nav_section" => "users",
            "user" => $userVo,
            "roles" => $roles,
            "possible_roles" => $possibleRoles,
            "facilities" => $facilities,
            "possible_facilities" => $possibleFacilities
        ]);
    }

    public function addRole(WebApplication $app, Request $request, $userName) {
        if(!$app->isGranted(UserRoleDAO::ROLE_ADMIN)) {
            throw new AccessDeniedException();
        }

        $role = trim($request->query->get("_role"));

        if(empty($role)) {
            $app["session"]->getFlashBag()->add("danger", "No role provided");
            return $app->redirect($app->path("users_display", ["userName" => $userName]));
        }

        if(UserRoleDAO::ROLE_SUPERADMIN == $role) {
            $app["session"]->getFlashBag()->add("danger", "Invalid role provided");
            return $app->redirect($app->path("users_display", ["userName" => $userName]));
        }

        $connection =& $app["merchant.mysql.connection"];
        $userDao = new UserDao($connection);
        $userVo =& $userDao->findByUsername($userName);

        if($userVo === null) {
            $app["session"]->getFlashBag()->add("danger", "Username '" . $userName . "' not found");

            return $app->redirect($app->path("users"));
        }

        $userRoleDao = new UserRoleDao($connection);

        $userRoleDao->addRole($userVo->getId(), $role);

        $app["session"]->getFlashBag()->add("success", "Added role: " . $role);

        return $app->redirect($app->path("users_display", ["userName" => $userName]));
    }

    public function removeRole(WebApplication $app, Request $request, $userName) {
        if(!$app->isGranted(UserRoleDAO::ROLE_ADMIN)) {
            throw new AccessDeniedException();
        }

        $role = trim($request->query->get("_role"));

        if(empty($role)) {
            return $app->json(["error" => "No role provided"], 400);
        }

        if(UserRoleDAO::ROLE_SUPERADMIN == $role) {
            return $app->json(["error" => "Invalid role provided"], 400);
        }

        $connection =& $app["merchant.mysql.connection"];
        $userDao = new UserDao($connection);
        $userVo =& $userDao->findByUsername($userName);

        if($userVo === null) {
            return $app->json(["error" => "Username '" . $userName . "' not found'"], 400);
        }

        $userRoleDao = new UserRoleDao($connection);

        $userRoleDao->removeRole($userVo->getId(), $role);

        $app["session"]->getFlashBag()->add("success", "Removed role: " . $role);

        return $app->redirect($app->path("users_display", ["userName" => $userName]));
    }

    public function addFacility(WebApplication $app, Request $request, $userName) {
        if(!$app->isGranted(UserRoleDAO::ROLE_ADMIN)) {
            throw new AccessDeniedException();
        }

        $facilityId = trim($request->query->get("_facility_id"));

        if(empty($facilityId)) {
            $app["session"]->getFlashBag()->add("danger", "No facility provided");
            return $app->redirect($app->path("users_display", ["userName" => $userName]));
        }

        $connection =& $app["merchant.mysql.connection"];
        $userDao = new UserDao($connection);
        $userVo =& $userDao->findByUsername($userName);

        if($userVo === null) {
            $app["session"]->getFlashBag()->add("danger", "Username '" . $userName . "' not found");

            return $app->redirect($app->path("users"));
        }

        $userFacilityDao =& UserFacilityDAO::instance($connection);

        $userFacilityDao->addFacility($userVo->getId(), $facilityId);

        $app["session"]->getFlashBag()->add("success", "Added facility #" . $facilityId);

        return $app->redirect($app->path("users_display", ["userName" => $userName]));
    }

    public function removeFacility(WebApplication $app, Request $request, $userName) {
        if(!$app->isGranted(UserRoleDAO::ROLE_ADMIN)) {
            throw new AccessDeniedException();
        }

        $facilityId = trim($request->query->get("_facility_id"));

        if(empty($facilityId)) {
            return $app->json(["error" => "No facility provided"], 400);
        }

        $connection =& $app["merchant.mysql.connection"];
        $userDao = new UserDao($connection);
        $userVo =& $userDao->findByUsername($userName);

        if($userVo === null) {
            return $app->json(["error" => "Username '" . $userName . "' not found'"], 400);
        }

        $userFacilityDao =& UserFacilityDAO::instance($connection);

        $userFacilityDao->removeFacility($userVo->getId(), $facilityId);

        $app["session"]->getFlashBag()->add("success", "Removed facility #" . $facilityId);

        return $app->redirect($app->path("users_display", ["userName" => $userName]));
    }

    public function jsonUserEvents(WebApplication $app, Request $request, $userName) {
        if(!$app->isGranted(UserRoleDAO::ROLE_ADMIN)) {
            throw new AccessDeniedException();
        }

        $sort = $request->query->get("sort") ?: "occurred";

        if(!in_array($sort, ["occurred", "ipv4", "ipv6", "admin_session_id"])) {
            $app->abort(400, "Invalid sort parameter");
        }

        $order = AppUtil::getOrderFromQuery($request, "order", "desc");
        $limit = AppUtil::getIntFromQuery($request, "limit", 20);
        $offset = AppUtil::getIntFromQuery($request, "offset", 0);
        $connection =& $app["merchant.mysql.connection"];
        $userDao = new UserDao($connection);
        $userVo =& $userDao->findByUsername($userName);

        if($userVo === null) {
            return $app->json(["error" => "Username '" . $userName . "' not found'"], 400);
        }

        $adminSessionEventDao = new AdminSessionEventDAO($connection);
        $adminSessionEventLogDao = new AdminSessionEventLogDAO($connection);
        $adminSessionEventLogs = $adminSessionEventLogDao->findAll(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$userVo) {
                $pdoQuery->where("user_id", $userVo->getId(), PDO::PARAM_INT);
            }, $sort . " " . $order, $offset . ", " . $limit
        );
        /** @var TimezoneVO $timezoneVo */
        $timezoneVo = $app["twig"]->getGlobals()["timezone"];
        $dateTimeZone = new DateTimeZone($timezoneVo->getName());
        $admin_session_event_names = [];
        $rows = [];

        foreach($adminSessionEventLogs as &$adminSessionEventLog) {
            $adminSessionEventId = $adminSessionEventLog->getAdminSessionEventId();
            $row = [];

            if(!isset($admin_session_event_names[$adminSessionEventId])) {
                $adminSessionEvent =& $adminSessionEventDao->findById($adminSessionEventId);

                $admin_session_event_names[$adminSessionEventId] = $adminSessionEvent->getName();
            }

            $occurredDateTime = new DateTime("@" . $adminSessionEventLog->getOccurred());

            $occurredDateTime->setTimezone($dateTimeZone);

            $row["event_name"] = $admin_session_event_names[$adminSessionEventId];
            $row["occurred"] = $occurredDateTime->format("Y-m-d H:i:s");
            $row["ipv4"] = $adminSessionEventLog->getIpv4();
            $row["ipv6"] = $adminSessionEventLog->getIpv6();
            $row["admin_session_id"] = $adminSessionEventLog->getAdminSessionId();

            $rows[] = $row;
        }

        unset($adminSessionEventLog);

        $results = ["total" => count($rows)];

        $results["rows"] =& $rows;

        return $app->json($results);
    }

    public function jsonUsers(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_ADMIN)) {
            throw new AccessDeniedException();
        }

        $sort = $request->query->get("sort") ?: "id";

        if(!in_array($sort, ["id", "name", "roles", "updated"])) {
            $app->abort(400, "Invalid sort parameter");
        }

        $order = AppUtil::getOrderFromQuery($request, "order", "desc");
        $limit = AppUtil::getIntFromQuery($request, "limit", 20);
        $offset = AppUtil::getIntFromQuery($request, "offset", 0);
        $connection =& $app["merchant.mysql.connection"];
        $userDao = new UserDao($connection);
        $userRoleDao = new UserRoleDao($connection);
        $users = $userDao->findAll(null, $sort . " " . $order, $offset . ", " . $limit);
        /** @var TimezoneVO $timezoneVo */
        $timezoneVo = $app["twig"]->getGlobals()["timezone"];
        $dateTimeZone = new DateTimeZone($timezoneVo->getName());
        $rows = [];

        foreach($users as &$user) {
            $userId = $user->getId();
            $row = [];
            $lastUpdatedDateTime = new DateTime("@" . ($user->getUpdated() ?: $user->getCreated()));

            $lastUpdatedDateTime->setTimezone($dateTimeZone);

            $row["id"] = $userId;
            $row["name"] = $user->getName();
            $row["updated"] = $lastUpdatedDateTime->format("Y-m-d H:i:s");

            $roles =& $userRoleDao->listRoles($userId);

            if(count($roles) < 1) {
                $row["roles"] = null;
            } else {
                $roleStr = "";
                $first = true;

                foreach($roles as &$role) {
                    $roleStr .= ($first ? "" : ", ") . $role;
                    $first = false;
                }

                unset($role);

                $row["roles"] = $roleStr;
            }

            $rows[] = $row;
        }

        unset($user);

        $results = ["total" => count($rows)];

        $results["rows"] =& $rows;

        return $app->json($results);
    }
}
