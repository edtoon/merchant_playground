<?php

namespace BlakePrinting\Admin\Users;

use BlakePrinting\Util\Utils;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class User implements AdvancedUserInterface, EquatableInterface {
    /** @var int */
    private $userId;
    /** @var string */
    private $username;
    /** @var string */
    private $password;
    /** @var bool */
    private $enabled;
    /** @var bool */
    private $accountNonExpired;
    /** @var bool */
    private $credentialsNonExpired;
    /** @var bool */
    private $accountNonLocked;
    /** @var (Role|string)[] */
    private $roles;

    /**
     * @param int $userId
     * @param string $username
     * @param string $password
     * @param array $roles
     * @param bool $enabled
     * @param bool $userNonExpired
     * @param bool $credentialsNonExpired
     * @param bool $userNonLocked
     */
    public function __construct($userId, $username, $password, array $roles = [], $enabled = true, $userNonExpired = true, $credentialsNonExpired = true, $userNonLocked = true) {
        $this->userId = $userId;
        $this->username = $username;
        $this->password = $password;
        $this->enabled = $enabled;
        $this->accountNonExpired = $userNonExpired;
        $this->credentialsNonExpired = $credentialsNonExpired;
        $this->accountNonLocked = $userNonLocked;
        $this->roles = $roles;
    }

    public function isEqualTo(UserInterface $user) {
        if($user instanceof self) {
            if(Utils::valuesMatch($user->getUserId(), $this->getUserId()) &&
                Utils::valuesMatch($user->getUsername(), $this->getUsername()) &&
                Utils::valuesMatch($user->getPassword(), $this->getPassword()) &&
                Utils::valuesMatch($user->isEnabled(), $this->isEnabled()) &&
                Utils::valuesMatch($user->isAccountNonExpired(), $this->isAccountNonExpired()) &&
                Utils::valuesMatch($user->isCredentialsNonExpired(), $this->isCredentialsNonExpired()) &&
                Utils::valuesMatch($user->isAccountNonLocked(), $this->isAccountNonLocked()) &&
                Utils::valuesMatch($user->getSalt(), $this->getSalt())
            ) {
                $userRoles = $user->getRoles();

                if(count($userRoles) == count($this->roles) &&
                    empty(array_diff($userRoles, $this->roles)) &&
                    empty(array_diff($this->roles, $userRoles))
                ) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return int
     */
    public function getUserId() {
        return $this->userId;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getPassword() {
        return $this->password;
    }

    public function isEnabled() {
        return $this->enabled;
    }

    public function isAccountNonExpired() {
        return $this->accountNonExpired;
    }

    public function isAccountNonLocked() {
        return $this->accountNonLocked;
    }

    public function isCredentialsNonExpired() {
        return $this->credentialsNonExpired;
    }

    public function getSalt() {
        return null;
    }

    public function getRoles() {
        return $this->roles;
    }

    public function eraseCredentials() {
    }

    public function __toString() {
        return $this->getUsername();
    }
}
