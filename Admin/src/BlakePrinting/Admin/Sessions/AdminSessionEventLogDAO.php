<?php

namespace BlakePrinting\Admin\Sessions;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static AdminSessionEventLogDAO instance(PDO $connection = null, $schemaName = null)
 * @method AdminSessionEventLogVO findById($id, $options = null, $useCache = false)
 * @method AdminSessionEventLogVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method AdminSessionEventLogVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method AdminSessionEventLogVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method AdminSessionEventLogVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method AdminSessionEventLogVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method AdminSessionEventLogVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class AdminSessionEventLogDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    public function store($userId, $event, $ipv4 = null, $ipv6 = null, $sessionId = null) {
        $conn =& $this->getConnection();
        $adminSessionEventDao = new AdminSessionEventDAO($conn);
        $insert =& PDOQuery::insert()->into($this)
            ->value("user_id", $userId, PDO::PARAM_INT)
            ->value("admin_session_event_id", "(SELECT id FROM " . $adminSessionEventDao->formatSchemaAndTableName() . " WHERE name = " . $conn->quote($event) . ")")
            ->value("occurred", "UNIX_TIMESTAMP()")
            ->value("admin_session_id", $sessionId, PDO::PARAM_STR)
        ;

        if($ipv4 !== null) {
            $insert->value("ipv4", $ipv4, PDO::PARAM_STR, "INET6_ATON");
        }

        if($ipv6 !== null) {
            $insert->value("ipv6", $ipv6, PDO::PARAM_STR, "INET6_ATON");
        }

        return $insert->executeGetRowCount($conn);
    }
}
