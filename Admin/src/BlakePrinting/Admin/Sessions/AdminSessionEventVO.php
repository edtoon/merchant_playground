<?php

namespace BlakePrinting\Admin\Sessions;

use BlakePrinting\Db\ValueObject;

class AdminSessionEventVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var string */
    private $name = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
