<?php

namespace BlakePrinting\Admin\Sessions;

use BlakePrinting\Db\ValueObject;

class AdminSessionEventLogVO implements ValueObject {
    /** @var int */
    private $userId = null;
    /** @var int */
    private $adminSessionEventId = null;
    /** @var int */
    private $occurred = null;
    /** @var string */
    private $ipv4 = null;
    /** @var string */
    private $ipv6 = null;
    /** @var string */
    private $adminSessionId = null;

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getAdminSessionEventId()
    {
        return $this->adminSessionEventId;
    }

    /**
     * @param int $adminSessionEventId
     */
    public function setAdminSessionEventId($adminSessionEventId)
    {
        $this->adminSessionEventId = $adminSessionEventId;
    }

    /**
     * @return int
     */
    public function getOccurred()
    {
        return $this->occurred;
    }

    /**
     * @param int $occurred
     */
    public function setOccurred($occurred)
    {
        $this->occurred = $occurred;
    }

    /**
     * @return string
     */
    public function getIpv4()
    {
        return $this->ipv4;
    }

    /**
     * @param string $ipv4
     */
    public function setIpv4($ipv4)
    {
        $this->ipv4 = $ipv4;
    }

    /**
     * @return string
     */
    public function getIpv6()
    {
        return $this->ipv6;
    }

    /**
     * @param string $ipv6
     */
    public function setIpv6($ipv6)
    {
        $this->ipv6 = $ipv6;
    }

    /**
     * @return string
     */
    public function getAdminSessionId()
    {
        return $this->adminSessionId;
    }

    /**
     * @param string $adminSessionId
     */
    public function setAdminSessionId($adminSessionId)
    {
        $this->adminSessionId = $adminSessionId;
    }
}
