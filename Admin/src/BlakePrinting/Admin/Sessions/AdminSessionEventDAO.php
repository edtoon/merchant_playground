<?php

namespace BlakePrinting\Admin\Sessions;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static AdminSessionEventDAO instance(PDO $connection = null, $schemaName = null)
 * @method AdminSessionEventVO findById($id, $options = null, $useCache = false)
 * @method AdminSessionEventVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method AdminSessionEventVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method AdminSessionEventVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method AdminSessionEventVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method AdminSessionEventVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method AdminSessionEventVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class AdminSessionEventDAO extends AbstractDataAccessObject {
}
