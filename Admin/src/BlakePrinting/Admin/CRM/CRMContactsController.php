<?php

namespace BlakePrinting\Admin\CRM;

use BlakePrinting\Admin\AppUtil;
use BlakePrinting\Admin\WebApplication;
use BlakePrinting\Contacts\ContactAddressDAO;
use BlakePrinting\Contacts\ContactAddressVO;
use BlakePrinting\Contacts\ContactPersonAddressDAO;
use BlakePrinting\Contacts\ContactPersonDAO;
use BlakePrinting\Contacts\ContactPersonDataFeedDAO;
use BlakePrinting\Contacts\ContactPersonPersonTypeDAO;
use BlakePrinting\Contacts\ContactPersonTypeDAO;
use BlakePrinting\Contacts\ContactPersonVO;
use BlakePrinting\DataSources\DataFeedDAO;
use BlakePrinting\DataSources\DataSourceDAO;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Globalization\CountryDAO;
use BlakePrinting\Globalization\CurrencyDAO;
use BlakePrinting\Organizations\OrganizationContactPersonDAO;
use BlakePrinting\Organizations\OrganizationDAO;
use BlakePrinting\Users\UserDAO;
use BlakePrinting\Users\UserRoleDAO;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use PDO;
use ReflectionClass;
use Silex\ControllerCollection;
use Stringy\StaticStringy as S;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class CRMContactsController {
    public static function connect(WebApplication $app, ControllerCollection $controllers) {
        $controllers->get("/contacts/{contactPersonId}/addresses.json", self::class . "::jsonContactAddresses")
            ->bind("crm_contacts_contact_addresses_json")
        ;
        $controllers->get("/contacts/{contactPersonId}/organizations.json", self::class . "::jsonContactOrganizations")
            ->bind("crm_contacts_contact_organizations_json")
        ;
        $controllers->get("/contacts/names.json", self::class . "::jsonContactNameSearch")
            ->bind("crm_contacts_names_json")
        ;
        $controllers->get("/contacts/organizations.json", self::class . "::jsonContactOrganizationSearch")
            ->bind("crm_contacts_organizations_json")
        ;
        $controllers->get("/contacts/{contactPersonId}", self::class . "::displayContact")
            ->bind("crm_contacts_contact")
        ;
        $controllers->post("/contacts", self::class . "::createContact")
            ->bind("crm_contacts_create")
        ;
        $controllers->get("/contacts.json", self::class . "::jsonContactSearch")
            ->bind("crm_contacts_json")
        ;
        $controllers->get("/contacts", self::class . "::displayContacts")
            ->bind("crm_contacts")
        ;
    }

    public function jsonContactAddresses(WebApplication $app, Request $request, $contactPersonId) {
        if($app->isGranted(UserRoleDAO::ROLE_TRIAL)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $contactPersonDao =& ContactPersonDAO::instance($connection);
        $contactPersonVo = ((Utils::isIntegerValue($contactPersonId) && $contactPersonId > 0) ? $contactPersonDao->findById($contactPersonId) : null);

        if($contactPersonVo === null) {
            $app->abort(Response::HTTP_NOT_FOUND, "Missing or invalid contact person ID");
        }

        $contactPersonAddressDao =& ContactPersonAddressDAO::instance($connection);
        $contactAddressDao =& ContactAddressDAO::instance($connection);
        $contactAddressTbl = $contactAddressDao->formatSchemaAndTableName();
        $countryDao =& CountryDAO::instance($connection);
        $countryTbl = $countryDao->formatSchemaAndTableName();
        $page = AppUtil::getIntFromQuery($request, "page");
        $sortColumn = Utils::getTrimmedStringOrNull($request->query->get("sort_column"));
        $sortDirection = AppUtil::getOrderFromQuery($request, "sort_direction");
        $count = AppUtil::getIntFromQuery($request, "count");
        $select =& PDOQuery::select()
            ->calculateFoundRows()
            ->columns($contactAddressTbl . ".*")
            ->column(("COALESCE(" . $contactAddressTbl . ".updated, " . $contactAddressTbl . ".created) AS modified"))
            ->column($countryTbl . ".name as country_name")
            ->from($contactPersonAddressDao)
            ->join($contactAddressDao, ["address_id = " . $contactAddressTbl . ".id"])
            ->leftJoin($countryDao, ["country_id = " . $countryTbl . ".id"])
            ->where("person_id", $contactPersonId, PDO::PARAM_INT)
        ;

        $sortColumn = ($sortColumn === null ? null : (string)S::underscored($sortColumn));

        if(in_array($sortColumn, ["line1", "line2", "line3", "city", "district", "state_or_region", "postal_code", "country_name"])) {
            $sortColumn = "UCASE(" . $sortColumn . ")";
        } else {
            switch($sortColumn) {
                case "id":
                    $sortColumn = ($contactAddressTbl . ".id");
                    break;
                case "country":
                    $sortColumn = "UCASE(country_name)";
                    break;
                default:
                    $sortColumn = ("COALESCE(" . $contactAddressTbl . ".updated, " . $contactAddressTbl . ".created)");
                    break;
            }
        }

        $select->orderBy($sortColumn . ($sortDirection === null ? "" : (" " . $sortDirection)));

        if($count !== null && $count > 0) {
            $select->limit($count);

            if($page !== null && $page > 1) {
                $select->offset(($page - 1) * $count);
            }
        }

        $calculated = $select->getCalculatedRowCount();
        $rows = $select->getResults($connection);
        $results = [
            "total" => count($rows),
            "calculated" => $calculated,
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function jsonContactOrganizations(WebApplication $app, Request $request, $contactPersonId) {
        if($app->isGranted(UserRoleDAO::ROLE_TRIAL)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $contactPersonDao =& ContactPersonDAO::instance($connection);
        $contactPersonVo = ((Utils::isIntegerValue($contactPersonId) && $contactPersonId > 0) ? $contactPersonDao->findById($contactPersonId) : null);

        if($contactPersonVo === null) {
            $app->abort(Response::HTTP_NOT_FOUND, "Missing or invalid contact person ID");
        }

        $organizationContactPersonDao =& OrganizationContactPersonDAO::instance($connection);
        $organizationDao =& OrganizationDAO::instance($connection);
        $organizationTbl = $organizationDao->formatSchemaAndTableName();
        $currencyDao =& CurrencyDAO::instance($connection);
        $currencyTbl = $currencyDao->formatSchemaAndTableName();
        $page = AppUtil::getIntFromQuery($request, "page");
        $sortColumn = Utils::getTrimmedStringOrNull($request->query->get("sort_column"));
        $sortDirection = AppUtil::getOrderFromQuery($request, "sort_direction");
        $count = AppUtil::getIntFromQuery($request, "count");
        $select =& PDOQuery::select()
            ->calculateFoundRows()
            ->columns($organizationTbl . ".*")
            ->column(("COALESCE(" . $organizationTbl . ".updated, " . $organizationTbl . ".created) AS modified"))
            ->column($currencyTbl . ".name as currency_name")
            ->from($organizationContactPersonDao)
            ->join($organizationDao, ["organization_id = " . $organizationTbl . ".id"])
            ->leftJoin($currencyDao, ["currency_id = " . $currencyTbl . ".id"])
            ->where("contact_person_id", $contactPersonId, PDO::PARAM_INT)
        ;

        switch($sortColumn) {
            case "id":
                $sortColumn = ($organizationTbl . ".id");
                break;
            case "name":
                $sortColumn = "UCASE(" . $organizationTbl . ".name)";
                break;
            case "code":
                $sortColumn = "UCASE(code)";
                break;
            case "currency":
                $sortColumn = "UCASE(currency_name)";
                break;
            default:
                $sortColumn = ("COALESCE(" . $organizationTbl . ".updated, " . $organizationTbl . ".created)");
                break;
        }

        $select->orderBy($sortColumn . ($sortDirection === null ? "" : (" " . $sortDirection)));

        if($count !== null && $count > 0) {
            $select->limit($count);

            if($page !== null && $page > 1) {
                $select->offset(($page - 1) * $count);
            }
        }

        $calculated = $select->getCalculatedRowCount();
        $rows = $select->getResults($connection);
        $results = [
            "total" => count($rows),
            "calculated" => $calculated,
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function displayContact(WebApplication $app, $contactPersonId) {
        if($app->isGranted(UserRoleDAO::ROLE_TRIAL)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $contactPersonDao =& ContactPersonDAO::instance($connection);
        $contactPersonVo = ((Utils::isIntegerValue($contactPersonId) && $contactPersonId > 0) ? $contactPersonDao->findById($contactPersonId) : null);

        if($contactPersonVo === null) {
            $app->abort(Response::HTTP_NOT_FOUND, "Missing or invalid contact person ID");
        }

        $userDao =& UserDAO::instance($connection);
        $creatorVo = ($contactPersonVo->getCreator() === null ? null : $userDao->findById($contactPersonVo->getCreator()));
        $updaterVo = ($contactPersonVo->getUpdater() === null ? null : $userDao->findById($contactPersonVo->getUpdater()));

        return $app["twig"]->render("crm/crm_contact.html.twig", [
            "nav_section" => "crm",
            "contact_person" => $contactPersonVo,
            "creator" => $creatorVo,
            "updater" => $updaterVo
        ]);
    }

    public function createContact(WebApplication $app, Request $request) {
        if($app->isGranted(UserRoleDAO::ROLE_TRIAL)) {
            throw new AccessDeniedException();
        }

        $total = 0;
        $requestContent = $request->getContent();

        if(!empty($requestContent)) {
            $logger =& LogUtil::getLogger();
            $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
            $connection =& $app["merchant.mysql.connection"];
            $contact = json_decode($requestContent);

            $logger->debug("Contact creation request: " . Utils::getVarDump($contact));

            $contactPersonTypeVo = null;

            if(isset($contact->contact_person_type_id)) {
                $contactPersonTypeVo =& ContactPersonTypeDAO::instance($connection)->findById($contact->contact_person_type_id);
            }

            if($contactPersonTypeVo === null) {
                AppUtil::addError("contactPersonTypeId", "Missing or invalid contact type");
            }

            $organizationVo = null;

            if(isset($contact->organization_name)) {
                $organizations =& OrganizationDAO::instance($connection)->findAllByColumn(
                    "name", $contact->organization_name, PDO::PARAM_STR
                );
                $organizationCount = count($organizations);

                if($organizationCount === 1) {
                    $organizationVo =& $organizations[0];
                } else if($organizationCount > 1) {
                    AppUtil::addError("organizationName", "Results for organization name are ambiguous");
                }
            }

            $countryVo = null;

            if(isset($contact->country_id)) {
                $countryVo =& CountryDAO::instance($connection)->findById($contact->country_id);
            }

            if($countryVo === null) {
                AppUtil::addError("countryId", "Missing or invalid country");
            }

            /** @var ContactAddressVO $contactAddressVo */
            $contactAddressVo = Utils::createCopy($contact, ContactAddressVO::class, [], [], true);

            $contactAddressVo->setCountryId($countryVo->getId());
            $contactAddressVo->setCreator($userId);

            AppUtil::validateAndLogErrors($app, $contactAddressVo);

            $contactPersonVo = new ContactPersonVO();

            $contactPersonVo->setCreator($userId);

            if(isset($contact->name)) {
                $contactPersonVo->setName($contact->name);
            }

            AppUtil::validateAndLogErrors($app, $contactPersonVo);

            $errorResponse = AppUtil::getErrorResponse($app);

            if($errorResponse !== null) {
                return $errorResponse;
            }

            $contactAddressDao =& ContactAddressDAO::instance($connection);
            $contactPersonAddressDao =& ContactPersonAddressDAO::instance($connection);
            $contactPersonDao =& ContactPersonDAO::instance($connection);
            $contactPersonPersonTypeDao =& ContactPersonPersonTypeDAO::instance($connection);
            $organizationContactPersonDao =& OrganizationContactPersonDAO::instance($connection);
            $contactPersonId =& $contactPersonDao->insert($contactPersonVo);
            $contactAddressVo =& $contactAddressDao->findOrCreate(
                $contactAddressVo, "FOR UPDATE",
                ["line1", "line2", "line3", "city", "district", "state_or_region", "postal_code", "country_id"],
                ["created", "creator", "updated", "updater"]
            );

            $contactPersonAddressDao->store($contactPersonId, $contactAddressVo->getId());
            $contactPersonPersonTypeDao->store($contactPersonId, $contactPersonTypeVo->getId());

            if($organizationVo !== null) {
                $organizationContactPersonDao->store($organizationVo->getId(), $contactPersonId);
            }

            $total++;
        }

        $results = [
            "total" => $total,
            "rows" => []
        ];

        return $app->json($results);
    }

    public function jsonContactSearch(WebApplication $app, Request $request) {
        if($app->isGranted(UserRoleDAO::ROLE_TRIAL)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $select =& $this->generateContactSearch($app, $request);
        $rows = $select->getResults($connection);
        $calculated = $select->getCalculatedRowCount();
        $results = [
            "total" => count($rows),
            "calculated" => $calculated,
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function jsonContactNameSearch(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_CATALOG)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $select =& $this->generateContactSearch($app, $request, ["contact_person_name"], 10);
        $rows =& $select->getColumnValues($connection);
        $results = [
            "total" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function jsonContactOrganizationSearch(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_CATALOG)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $select =& $this->generateContactSearch($app, $request, ["organization_name"], 10);
        $rows =& $select->getColumnValues($connection);
        $results = [
            "total" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    /**
     * @param WebApplication $app
     * @param Request $request
     * @param string[] $columns
     * @param int $count
     * @return PDOQuery
     */
    private function &generateContactSearch(WebApplication $app, Request $request, array $columns = [], $count = null) {
        $connection =& $app["merchant.mysql.connection"];
        $sortColumn = Utils::getTrimmedStringOrNull($request->query->get("sort_column"));
        $sortDirection = AppUtil::getOrderFromQuery($request, "sort_direction");
        $count = ($count === null ? AppUtil::getIntFromQuery($request, "count") : $count);
        $page = AppUtil::getIntFromQuery($request, "page");
        $dataSourceId = Utils::getTrimmedStringOrNull($request->query->get("data_source_id"));
        $contactPersonTypeId = AppUtil::getIntFromQuery($request, "contact_person_type_id");
        $name = Utils::getTrimmedStringOrNull($request->query->get("name"));
        $organizationName = Utils::getTrimmedStringOrNull($request->query->get("organization_name"));
        $countryId = AppUtil::getIntFromQuery($request, "country_id");
        $contactPersonDao =& ContactPersonDAO::instance($connection);
        $contactPersonTbl = $contactPersonDao->formatSchemaAndTableName();
        $contactPersonAddressDao =& ContactPersonAddressDAO::instance($connection);
        $contactPersonAddressTbl = $contactPersonAddressDao->formatSchemaAndTableName();
        $contactAddressDao =& ContactAddressDAO::instance($connection);
        $contactAddressTbl = $contactAddressDao->formatSchemaAndTableName();
        $organizationContactPersonDao =& OrganizationContactPersonDAO::instance($connection);
        $organizationContactPersonTbl = $organizationContactPersonDao->formatSchemaAndTableName();
        $organizationDao =& OrganizationDAO::instance($connection);
        $organizationTbl = $organizationDao->formatSchemaAndTableName();
        $countryDao =& CountryDAO::instance($connection);
        $countryTbl = $countryDao->formatSchemaAndTableName();
        $select =& PDOQuery::select()
            ->distinct()
            ->from($contactPersonDao)
            ->leftJoin($contactPersonAddressTbl, [$contactPersonTbl . ".id = " . $contactPersonAddressTbl . ".person_id"])
            ->leftJoin($contactAddressTbl, [$contactPersonAddressTbl . ".address_id = " . $contactAddressTbl . ".id"])
            ->leftJoin($countryTbl, [$contactAddressTbl . ".country_id = " . $countryTbl . ".id"])
        ;

        if($organizationName === null) {
            $select
                ->leftJoin($organizationContactPersonTbl, [$contactPersonTbl . ".id = " . $organizationContactPersonTbl . ".contact_person_id"])
                ->leftJoin($organizationTbl, [$organizationContactPersonTbl . ".organization_id = " . $organizationTbl . ".id"])
            ;
        } else {
            $select
                ->join($organizationContactPersonTbl, [$contactPersonTbl . ".id = " . $organizationContactPersonTbl . ".contact_person_id"])
                ->join($organizationTbl, [$organizationContactPersonTbl . ".organization_id = " . $organizationTbl . ".id"])
                ->where("UCASE(" . $organizationTbl . ".name) LIKE UCASE(" . $connection->quote("%" . $organizationName . "%") . ")")
            ;
        }

        if(empty($columns)) {
            $columns = [
                "contact_person_id", "contact_person_name", "modified",
                "organization_id", "organization_name",
                "country_id", "country_name"
            ];
        }

        foreach($columns as &$column) {
            switch($column) {
                case "contact_person_id":
                    $select->column([$contactPersonTbl . ".id" => "contact_person_id"]);
                    break;
                case "contact_person_name":
                    $select->column([$contactPersonTbl . ".name" => "contact_person_name"]);
                    break;
                case "modified":
                    $select->column(("COALESCE(" . $contactPersonTbl . ".updated, " . $contactPersonTbl . ".created) AS modified"));
                    break;
                case "organization_id":
                    $select->column([$organizationTbl . ".id" => "organization_id"]);
                    break;
                case "organization_name":
                    $select->column([$organizationTbl . ".name" => "organization_name"]);
                    break;
                case "country_id":
                    $select->column([$countryTbl . ".id" => "country_id"]);
                    break;
                case "country_name":
                    $select->column([$countryTbl . ".name" => "country_name"]);
                    break;
            }
        }

        unset($column);

        if($name !== null && !S::isBlank($name)) {
            $select->where("UCASE(" . $contactPersonTbl . ".name) LIKE UCASE(" . $connection->quote("%" . $name . "%") . ")");
        }

        if($countryId !== null) {
            $select->where($countryTbl . ".id", $countryId, PDO::PARAM_INT);
        }

        if($dataSourceId !== null) {
            $contactPersonDataFeedDao =& ContactPersonDataFeedDAO::instance($connection);
            $contactPersonDataFeedTbl = $contactPersonDataFeedDao->formatSchemaAndTableName();

            if($dataSourceId === "manual") {
                $select
                    ->leftJoin($contactPersonDataFeedTbl, [$contactPersonTbl . ".id = " . $contactPersonDataFeedTbl . ".person_id"])
                    ->where($contactPersonDataFeedTbl . ".data_feed_id IS NULL")
                ;
            } else if(Utils::isIntegerValue($dataSourceId)) {
                $dataFeedDao =& DataFeedDAO::instance($connection);
                $dataFeedTbl = $dataFeedDao->formatSchemaAndTableName();
                $dataSourceDao =& DataSourceDAO::instance($connection);
                $dataSourceTbl = $dataSourceDao->formatSchemaAndTableName();

                $select
                    ->join($contactPersonDataFeedDao, [$contactPersonTbl . ".id = " . $contactPersonDataFeedTbl . ".person_id"])
                    ->join($dataFeedDao, [$contactPersonDataFeedTbl . ".data_feed_id = " . $dataFeedTbl . ".id"])
                    ->join($dataSourceDao, [$dataFeedTbl . ".data_source_id = " . $dataSourceTbl . ".id"])
                    ->where($dataSourceTbl . ".id", $dataSourceId, PDO::PARAM_INT)
                ;
            }
        }

        if($contactPersonTypeId !== null) {
            $contactPersonPersonTypeDao =& ContactPersonPersonTypeDAO::instance($connection);
            $contactPersonPersonTypeTbl = $contactPersonPersonTypeDao->formatSchemaAndTableName();
            $contactPersonTypeDao =& ContactPersonTypeDAO::instance($connection);
            $contactPersonTypeTbl = $contactPersonTypeDao->formatSchemaAndTableName();

            $select
                ->join($contactPersonPersonTypeDao, [$contactPersonTbl . ".id = " . $contactPersonPersonTypeTbl . ".person_id"])
                ->join($contactPersonTypeDao, [$contactPersonPersonTypeTbl . ".person_type_id = " . $contactPersonTypeTbl . ".id"])
                ->where($contactPersonTypeTbl . ".id", $contactPersonTypeId, PDO::PARAM_INT)
            ;
        }

        $orderBy = null;

        switch($sortColumn) {
            case "id":
                $orderBy = "contact_person_id";
                break;
            case "organization":
                $orderBy = "UCASE(organization_name)";
                break;
            case "country":
                $orderBy = "UCASE(country_name)";
                break;
            case "modified":
                $orderBy = ("COALESCE(" . $organizationTbl . ".updated, " . $organizationTbl . ".created)");
                break;
        }

        if($orderBy === null && in_array("contact_person_name", $columns)) {
            $orderBy = "UCASE(contact_person_name)";
        }

        if($orderBy !== null) {
            $select->orderBy($orderBy . ($sortDirection === null ? "" : (" " . $sortDirection)));
        }

        if($count !== null && $count > 0) {
            $select->calculateFoundRows()->limit($count);

            if($page !== null && $page > 1) {
                $select->offset(($page - 1) * $count);
            }
        }

        return $select;
    }

    public function displayContacts(WebApplication $app) {
        if($app->isGranted(UserRoleDAO::ROLE_TRIAL)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $countryDao =& CountryDAO::instance($connection);
        $countries =& $countryDao->findAll(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$countryDao, &$connection) {
                $contactPersonAddressDao =& ContactPersonAddressDAO::instance($connection);
                $contactAddressDao =& ContactAddressDAO::instance($connection);
                $contactAddressTbl = $contactAddressDao->formatSchemaAndTableName();
                $countryTbl = $countryDao->formatSchemaAndTableName();

                $pdoQuery
                    ->distinct()
                    ->columns($countryTbl . ".*")
                    ->join($contactAddressDao, [$countryTbl . ".id = country_id"])
                    ->join($contactPersonAddressDao, [$contactAddressTbl . ".id = address_id"])
                ;
            }, "UCASE(name) ASC"
        );

        return $app["twig"]->render("crm/crm_contacts.html.twig", [
            "nav_section" => "crm",
            "side_section" => "contacts",
            "data_sources" => DataSourceDAO::instance($connection)->findAll(null, "UCASE(name) ASC"),
            "contact_person_types" => ContactPersonTypeDAO::instance($connection)->findAll(null, "UCASE(name) ASC"),
            "countries" => $countries
        ]);
    }
}
