<?php

namespace BlakePrinting\Admin\CRM;

use BlakePrinting\Admin\WebApplication;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;

class CRMControllerProvider implements ControllerProviderInterface {
    public function connect(Application $app) {
        $controllers = $app["controllers_factory"];

        if($app instanceof WebApplication) {
            CRMContactsController::connect($app, $controllers);

            $controllers->get("/", self::class . "::displayDefault")
                ->bind("crm")
            ;
        }

        return $controllers;
    }

    public function displayDefault(WebApplication $app) {
        return $app["twig"]->render("crm/crm.html.twig", [
            "nav_section" => "crm"
        ]);
    }
}
