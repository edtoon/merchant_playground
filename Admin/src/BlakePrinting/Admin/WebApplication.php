<?php

namespace BlakePrinting\Admin;

use BlakePrinting\Admin\CRM\CRMControllerProvider;
use BlakePrinting\Admin\Organizations\OrganizationsControllerProvider;
use BlakePrinting\Admin\Sales\SalesControllerProvider;
use BlakePrinting\Admin\Shipping\ShippingControllerProvider;
use BlakePrinting\Cache\MemcacheUtil;
use BlakePrinting\Admin\Accounting\AccountingControllerProvider;
use BlakePrinting\Admin\Amazon\AmazonControllerProvider;
use BlakePrinting\Admin\Catalog\CatalogControllerProvider;
use BlakePrinting\Admin\Gateway\GatewayControllerProvider;
use BlakePrinting\Admin\Labels\LabelsControllerProvider;
use BlakePrinting\Admin\Printers\PrintersControllerProvider;
use BlakePrinting\Admin\Sessions\AdminSessionEventLogDAO;
use BlakePrinting\Admin\Users\User;
use BlakePrinting\Admin\Users\UserProvider;
use BlakePrinting\Globalization\TimezoneDAO;
use BlakePrinting\Users\UserDAO;
use BlakePrinting\Users\UserRoleDAO;
use BlakePrinting\Admin\Users\UsersControllerProvider;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Admin\Profile\ProfileControllerProvider;
use BlakePrinting\Users\UserVO;
use BlakePrinting\Util\ConfigUtil;
use BlakePrinting\Util\LogUtil;
use Exception;
use Monolog\Logger;
use PDO;
use Silex\Application;
use Silex\Application\SecurityTrait;
use Silex\Application\UrlGeneratorTrait;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\SecurityServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\MemcachedSessionHandler;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class WebApplication extends Application {
    const SESSION_LIFETIME = 60 * 60 * 12;

    use SecurityTrait;
    use UrlGeneratorTrait;

    public function __construct(array $values = []) {
        parent::__construct($values);

        $app =& $this;
        $app["debug"] = true;

        ErrorHandler::register();

        ini_set("session.cookie_lifetime", self::SESSION_LIFETIME);
        ini_set("session.gc_maxlifetime", self::SESSION_LIFETIME);
        ini_set("session.gc_probability", 1);
        ini_set("session.gc_divisor", 1);

        session_set_cookie_params(self::SESSION_LIFETIME);

        $app->register(new MonologServiceProvider(), [
            "monolog.level" => Logger::DEBUG,
            "monolog.logfile" => "php://stderr"
        ]);

        LogUtil::configureLogger(__CLASS__);

        $logger =& LogUtil::getLogger();
        $merchantConnection =& PDOUtil::getMysqlConnection();
        $adminMemcache =& MemcacheUtil::getMemcached();
        $userProvider =& UserProvider::instance($merchantConnection);

        $app["merchant.mysql.connection"] = $merchantConnection;
        $app["admin.memcache"] = $adminMemcache;

        $app->register(new SecurityServiceProvider(), [
            "security.role_hierarchy" => [
                UserRoleDAO::ROLE_ADMIN => [UserRoleDAO::ROLE_AUTH, UserRoleDAO::ROLE_CATALOG, UserRoleDAO::ROLE_FINANCE, UserRoleDAO::ROLE_INVENTORY],
                UserRoleDAO::ROLE_SUPERADMIN => [UserRoleDAO::ROLE_ADMIN, UserRoleDAO::ROLE_AUTH, UserRoleDAO::ROLE_CATALOG, UserRoleDAO::ROLE_FINANCE, UserRoleDAO::ROLE_INVENTORY],
                UserRoleDAO::ROLE_TRIAL => [UserRoleDAO::ROLE_AUTH]
            ],
            "security.firewalls" => [
                "login" => ["pattern" => "^/login*"],
                "gateway" => ["pattern" => "^/gw*"],
                "authenticated" => [
                    "pattern" => "^.*$",
                    "form" => [
                        "login_path" => "/login",
                        "check_path" => "/profile/login_check",
                        "username_parameter" => "_username",
                        "password_parameter" => "_password"
                    ],
                    "logout" => ["logout_path" => "/logout", "invalidate_session" => true],
                    "users" => function() use (&$userProvider) {
                        return $userProvider;
                    }
                ]
            ],
            "security.encoder.digest" => function() {
                return UserDAO::getPasswordEncoder();
            }
        ]);
        $app->register(new SessionServiceProvider(), [
            "session.storage.handler" => new MemcachedSessionHandler(
                $adminMemcache, [
                    "expiretime" => self::SESSION_LIFETIME,
                    "prefix" => "merchant:session"
                ]
            ),
            "session.storage.options" => [
                "cookie_lifetime" => self::SESSION_LIFETIME
            ]
        ]);
        $app->register(new TwigServiceProvider(), [
            "twig.path" => ConfigUtil::getAppDir() . "/views"
        ]);
        $app->register(new ValidatorServiceProvider());

        $app->before(function() use (&$app, &$merchantConnection, &$userProvider) {
            $app['session']->migrate(false, self::SESSION_LIFETIME);

            $userVo = $userProvider->getUserVo();
            $timezoneVo = ((
                ($userVo === null || $userVo->getTimezoneId() === null) ?
                null : TimezoneDAO::instance($merchantConnection)->findById($userVo->getTimezoneId())
            ) ?: TimezoneDAO::instance($merchantConnection)->findByColumn("name", TimezoneDAO::DEFAULT_TIMEZONE, PDO::PARAM_STR));

            $app["twig"]->addGlobal("user", $userVo);
            $app["twig"]->addGlobal("timezone", $timezoneVo);
        });
        $app->error(function(Exception $e, $code) use (&$merchantConnection) {
            PDOUtil::rollbackConnection($merchantConnection);
        });
        $app->after(function() use (&$merchantConnection) {
            PDOUtil::commitConnection($merchantConnection);
        });

        $app->on(SecurityEvents::INTERACTIVE_LOGIN, function(InteractiveLoginEvent $event) use (&$app, &$merchantConnection, &$logger) {
            $token = $event->getAuthenticationToken();
            $user = $token->getUser();
            $username = $user->getUsername();

            $logger->info("Authenticated user: " . $username);

            if($user instanceof User) {
                $userId = $user->getUserId();
                $adminSessionEventLogDao =& AdminSessionEventLogDAO::instance($merchantConnection);

                $adminSessionEventLogDao->store($userId, "login", $_SERVER['REMOTE_ADDR'], null, $app["session"]->getId());
            }
        });
        $app->on(AuthenticationEvents::AUTHENTICATION_FAILURE, function(AuthenticationEvent $event) use (&$logger) {
            $logger->notice("Failed to authenticate user: " . $event->getAuthenticationToken()->getUsername());
        });

        $app->mount("/accounting", new AccountingControllerProvider());
        $app->mount("/amazon", new AmazonControllerProvider());
        $app->mount("/catalog", new CatalogControllerProvider());
        $app->mount("/crm", new CRMControllerProvider());
        $app->mount("/gw", new GatewayControllerProvider());
        $app->mount("/labels", new LabelsControllerProvider());
        $app->mount("/organizations", new OrganizationsControllerProvider());
        $app->mount("/printers", new PrintersControllerProvider());
        $app->mount("/profile", new ProfileControllerProvider());
        $app->mount("/sales", new SalesControllerProvider());
        $app->mount("/shipping", new ShippingControllerProvider());
        $app->mount("/users", new UsersControllerProvider());

        $app->get("/dash", function() use (&$app) {
            return $app["twig"]->render("dash.html.twig", ["nav_section" => "dashboard"]);
        })->bind("dashboard");
        $app->get("/login", function(Request $request) use (&$app) {
            $lastError = $app["security.last_error"]($request);
            $lastUsername = $app["session"]->get("_security.last_username");

            if(!empty($lastError)) {
                $app["session"]->getFlashBag()->add(
                    "danger",
                    "Error trying to login" . (empty($lastUsername) ? "" : (" with username: " . $lastUsername)) .
                    " - " . $lastError
                );
            }

            return $app["twig"]->render("login/login.html.twig");
        });
        $app->get("/login_create", function() use (&$app) {
            return $app["twig"]->render("login/login_create.html.twig");
        })->bind("login_create");
        $app->post("/login_create", function(Request $request) use (&$app, &$merchantConnection, &$logger) {
            $username = trim($request->request->get("_username"));
            $usernameEmpty = empty($username);
            $new = $request->request->get("_new");
            $newEmpty = empty($new);
            $confirm = $request->request->get("_confirm");
            $confirmEmpty = empty($confirm);

            if(!$usernameEmpty || !$newEmpty || !$confirmEmpty) {
                if($usernameEmpty) {
                    $app["session"]->getFlashBag()->add("danger", "Username not provided");
                }

                if($newEmpty) {
                    $app["session"]->getFlashBag()->add("danger", "Password not provided");
                }

                if($confirmEmpty) {
                    $app["session"]->getFlashBag()->add("danger", "Password not provided");
                }

                if(!$usernameEmpty && !$newEmpty && !$confirmEmpty) {
                    if($new != $confirm) {
                        $app["session"]->getFlashBag()->add("danger", "Password did not match confirmation");
                    } else {
                        $userDao =& UserDAO::instance($merchantConnection);
                        $userVo =& $userDao->findByColumn("name", $username, PDO::PARAM_STR, null, null, "FOR UPDATE");

                        if($userVo !== null) {
                            $app["session"]->getFlashBag()->add("danger", "Username '" . $username . "' already exists");
                        } else {
                            $userVo = new UserVO($username, $new);

                            AppUtil::validateAndLogErrors($app, $userVo);

                            $errors =& AppUtil::getErrors();

                            if(empty($errors)) {
                                $userRoleDao =& UserRoleDAO::instance($merchantConnection);
                                $userId = $userDao->create($username, $new);

                                $logger->debug("User: " . $username . ", created, pass: " . $new);
                                $userRoleDao->addRole($userId, UserRoleDAO::ROLE_TRIAL);

                                $app["session"]->getFlashBag()->add("success", "User: " . $username . " created, please login");

                                return $app->redirect("login");
                            }

                            foreach($errors as $category => &$categoryErrors) {
                                foreach($categoryErrors as &$errorMessage) {
                                    $app["session"]->getFlashBag()->add("danger", $errorMessage);
                                }

                                unset($errorMessage);
                            }

                            unset($categoryErrors);
                        }
                    }
                }
            }

            return $app->redirect("login_create");
        })->bind("login_create_submit");
        $app->get("/", function() use (&$app) {
            if($app['security.authorization_checker']->isGranted(UserRoleDAO::ROLE_AUTH)) {
                return $app->redirect($app->path("dashboard"));
            } else {
                return $app->redirect($app->path("logout"));
            }
        });
    }
}
