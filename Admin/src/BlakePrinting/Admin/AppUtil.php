<?php

namespace BlakePrinting\Admin;

use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;

class AppUtil {
    private static $errors = [];

    private function __construct() { }

    /**
     * @param Application $app
     * @return int
     */
    public static function getUserId(Application $app) {
        return $app["security.token_storage"]->getToken()->getUser()->getUserId();
    }

    /**
     * @param Application $app
     * @return string
     */
    public static function getUsername(Application $app) {
        return $app["security.token_storage"]->getToken()->getUsername();
    }

    /**
     * @param string $category
     * @param string $message
     */
    public static function addError($category, $message) {
        if(!isset(self::$errors[$category])) {
            self::$errors[$category] = [];
        }

        self::$errors[$category][] = $message;
    }

    public static function &getErrors() {
        return self::$errors;
    }

    /**
     * @param Application $app
     * @return JsonResponse
     */
    public static function getErrorResponse(Application $app) {
        if(!empty(self::$errors)) {
            return $app->json(["success" => false, "errors" => self::$errors], Response::HTTP_BAD_REQUEST);
        }

        return null;
    }

    /**
     * @param Application $app
     * @param mixed $value
     * @param string $pathExtra
     */
    public static function validateAndLogErrors(Application $app, $value, $pathExtra = "") {
        $validationErrors = $app["validator"]->validate($value);

        if(count($validationErrors) > 0) {
            $logger =& LogUtil::getLogger();

            foreach($validationErrors as &$error) {
                if($error instanceof ConstraintViolation) {
                    $errorPath = $error->getPropertyPath() . $pathExtra;

                    AppUtil::addError($errorPath, $error->getMessage());

                    $logger->debug("Error at path: " . $errorPath . " - " . $error->getMessage());
                }
            }
        }
    }

    /**
     * @param Request $request
     * @param string $paramName
     * @param string $defaultValue
     * @return mixed|null
     */
    public static function getBootstrapTableParam(Request &$request, $paramName, $defaultValue = null) {
        $paramValue = $request->request->get($paramName);

        if($paramValue !== null && $paramValue != "Empty") {
            return $paramValue;
        }

        return $defaultValue;
    }

    /**
     * @param Request $request
     * @param string $paramName
     * @param int $defaultValue
     * @return int
     */
    public static function getIntFromQuery(Request &$request, $paramName, $defaultValue = null) {
        return self::getIntFromValue($request->query->get($paramName), $defaultValue);
    }

    /**
     * @param Request $request
     * @param string $paramName
     * @param float $defaultValue
     * @return float
     */
    public static function getFloatFromQuery(Request &$request, $paramName, $defaultValue = null) {
        return self::getFloatFromValue($request->query->get($paramName), $defaultValue);
    }

    /**
     * @param Request $request
     * @param string $paramName
     * @param int $defaultValue
     * @return int
     */
    public static function getIntFromRequest(Request &$request, $paramName, $defaultValue = null) {
        return self::getIntFromValue($request->request->get($paramName), $defaultValue);
    }

    /**
     * @param Request $request
     * @param string $paramName
     * @param float $defaultValue
     * @return float
     */
    public static function getFloatFromRequest(Request &$request, $paramName, $defaultValue = null) {
        return self::getFloatFromValue($request->request->get($paramName), $defaultValue);
    }

    /**
     * @param mixed $paramValue
     * @param int $defaultValue
     * @return int
     */
    public static function getIntFromValue($paramValue, $defaultValue = null) {
        $paramValue = Utils::getTrimmedStringOrNull($paramValue);

        if($paramValue !== null && Utils::isIntegerValue($paramValue)) {
            return (int)$paramValue;
        }

        return $defaultValue;
    }

    /**
     * @param mixed $paramValue
     * @param float $defaultValue
     * @return float
     */
    public static function getFloatFromValue($paramValue, $defaultValue = null) {
        $paramValue = Utils::getTrimmedStringOrNull($paramValue);

        if($paramValue !== null && Utils::isFloatValue($paramValue)) {
            return (float)$paramValue;
        }

        return $defaultValue;
    }

    /**
     * @param Request $request
     * @param string $paramName
     * @param string $defaultValue
     * @return string
     */
    public static function getOrderFromQuery(Request &$request, $paramName, $defaultValue = null) {
        return self::getOrderFromValue($request->query->get($paramName), $defaultValue);
    }

    /**
     * @param Request $request
     * @param string $paramName
     * @param string $defaultValue
     * @return string
     */
    public static function getOrderFromRequest(Request &$request, $paramName, $defaultValue = null) {
        return self::getOrderFromValue($request->request->get($paramName), $defaultValue);
    }

    /**
     * @param string $paramValue
     * @param string $defaultValue
     * @return string
     */
    public static function getOrderFromValue($paramValue, $defaultValue = null) {
        $paramValue = Utils::getTrimmedStringOrNull($paramValue);

        if($paramValue !== null) {
            if(Utils::isIntegerValue($paramValue)) {
                $intValue = (int)$paramValue;

                if($intValue === 1) {
                    return "asc";
                } else if($intValue === -1) {
                    return "desc";
                }
            } else if($paramValue === "asc") {
                return "asc";
            } else if($paramValue === "desc") {
                return "desc";
            }
        }

        return $defaultValue;
    }
}
