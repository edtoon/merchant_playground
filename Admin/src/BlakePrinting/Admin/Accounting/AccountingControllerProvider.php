<?php

namespace BlakePrinting\Admin\Accounting;

use BlakePrinting\Accounting\ChartAccountDAO;
use BlakePrinting\Accounting\ChartAccountTypeDAO;
use BlakePrinting\Accounting\ChartAccountVO;
use BlakePrinting\Accounting\ChartOfAccountsDAO;
use BlakePrinting\Accounting\ChartOfAccountsVO;
use BlakePrinting\Accounting\FiscalPeriodVO;
use BlakePrinting\Accounting\GeneralLedgerDAO;
use BlakePrinting\Accounting\GeneralLedgerVO;
use BlakePrinting\Accounting\JournalEntryGeneralLedgerAccountDAO;
use BlakePrinting\Accounting\JournalEntryGeneralLedgerAccountVO;
use BlakePrinting\Accounting\GeneralLedgerAccountDAO;
use BlakePrinting\Accounting\GeneralLedgerAccountVO;
use BlakePrinting\Accounting\SetOfBooksDAO;
use BlakePrinting\Accounting\JournalEntryDAO;
use BlakePrinting\Accounting\JournalEntryVO;
use BlakePrinting\Accounting\FiscalPeriodDAO;
use BlakePrinting\Accounting\FiscalPeriodTypeDAO;
use BlakePrinting\Accounting\SetOfBooksVO;
use BlakePrinting\Admin\AppUtil;
use BlakePrinting\DataSources\DataSourceDAO;
use BlakePrinting\Db\DbUtil;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Globalization\CurrencyDAO;
use BlakePrinting\Globalization\TimezoneDAO;
use BlakePrinting\InkHeroGlobal\ApInvoiceDAO;
use BlakePrinting\InkHeroGlobal\BusinessDAO;
use BlakePrinting\InkHeroGlobal\EmployeeDAO;
use BlakePrinting\InkHeroGlobal\TaxableCategoryDAO;
use BlakePrinting\InkHeroGlobal\TaxPayerDAO;
use BlakePrinting\Organizations\OrganizationDAO;
use BlakePrinting\Users\UserRoleDAO;
use BlakePrinting\Admin\WebApplication;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use DateTime;
use DateTimeZone;
use Exception;
use PDO;
use ReflectionClass;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class AccountingControllerProvider implements ControllerProviderInterface {
    public function connect(Application $app) {
        $controllers = $app["controllers_factory"];

        if($app instanceof WebApplication) {
            $controllers->get("/source_documents.json", self::class . "::jsonSourceDocuments")
                ->bind("accounting_source_document_search_json")
            ;
            $controllers->get("/source_documents", self::class . "::displaySourceDocuments")
                ->bind("accounting_source_documents")
            ;
            $controllers->post("/create_journal_entry", self::class . "::createJournalEntry")
                ->bind("accounting_journal_entry_create")
            ;
            $controllers->get("/general_journal_entries.json", self::class . "::jsonJournalEntries")
                ->bind("accounting_journal_entries_json")
            ;
            $controllers->get("/general_journal", self::class . "::displayGeneralJournal")
                ->bind("accounting_general_journal")
            ;
            $controllers->post("/update_general_ledger_accounts", self::class . "::updateGeneralLedgerAccounts")
                ->bind("accounting_general_ledger_accounts_update")
            ;
            $controllers->get("/general_ledger_accounts.json", self::class . "::jsonGeneralLedgerAccounts")
                ->bind("accounting_general_ledger_accounts_json")
            ;
            $controllers->get("/general_ledger", self::class . "::displayGeneralLedger")
                ->bind("accounting_general_ledger")
            ;
            $controllers->post("/create_general_ledger", self::class . "::createGeneralLedger")
                ->bind("accounting_general_ledger_create")
            ;
            $controllers->get("/general_ledgers.json", self::class . "::jsonGeneralLedgers")
                ->bind("accounting_general_ledgers_json")
            ;
            $controllers->get("/set_of_books", self::class . "::displaySetOfBooks")
                ->bind("accounting_set_of_books")
            ;
            $controllers->post("/create_set_of_books", self::class . "::createSetOfBooks")
                ->bind("accounting_set_of_books_create")
            ;
            $controllers->get("/sets_of_books.json", self::class . "::jsonSetsOfBooks")
                ->bind("accounting_sets_of_books_json")
            ;
            $controllers->get("/sets_of_books", self::class . "::displaySetsOfBooks")
                ->bind("accounting_sets_of_books")
            ;
            $controllers->post("/create_fiscal_period", self::class . "::createFiscalPeriod")
                ->bind("accounting_fiscal_period_create")
            ;
            $controllers->get("/fiscal_periods.json", self::class . "::jsonFiscalPeriods")
                ->bind("accounting_fiscal_periods_json")
            ;
            $controllers->get("/fiscal_periods", self::class . "::displayFiscalPeriods")
                ->bind("accounting_fiscal_periods")
            ;
            $controllers->delete("/delete_chart_account", self::class . "::deleteChartAccount")
                ->bind("accounting_chart_account_delete")
            ;
            $controllers->post("/create_chart_account", self::class . "::createChartAccount")
                ->bind("accounting_chart_account_create")
            ;
            $controllers->get("/chart_accounts.json", self::class . "::jsonChartAccounts")
                ->bind("accounting_chart_accounts_json")
            ;
            $controllers->get("/chart_accounts", self::class . "::displayChartAccounts")
                ->bind("accounting_chart_accounts")
            ;
            $controllers->post("/create_chart_of_accounts", self::class . "::createChartOfAccounts")
                ->bind("accounting_chart_of_accounts_create")
            ;
            $controllers->get("/charts_of_accounts.json", self::class . "::jsonChartsOfAccounts")
                ->bind("accounting_charts_of_accounts_json")
            ;
            $controllers->get("/charts_of_accounts", self::class . "::displayChartsOfAccounts")
                ->bind("accounting_charts_of_accounts")
            ;
            $controllers->get("/", self::class . "::displayDefault")
                ->bind("accounting")
            ;
        }

        return $controllers;
    }

    public function jsonSourceDocuments(WebApplication $app) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $apInvoiceDao =& ApInvoiceDAO::instance($connection);
        $invoices =& $apInvoiceDao->findAll(null, "invoiceid DESC", 25);
        $rows = [];

        if(!empty($invoices)) {
            $taxableCategoryDao =& TaxableCategoryDAO::instance($connection);
            $employeeDao =& EmployeeDAO::instance($connection);
            $taxPayerDao =& TaxPayerDAO::instance($connection);
            $businessDao =& BusinessDAO::instance($connection);

            foreach($invoices as &$invoice) {
                $dateTime = ($invoice->getDate() === null ? null : DateTime::createFromFormat(DbUtil::MYSQL_DATE_FORMAT . " T", $invoice->getDate() . " UTC"));
                $taxableCategory = ($invoice->getCategory() === null ? null : $taxableCategoryDao->findById($invoice->getCategory(), null, true));
                $employee = ($invoice->getEmployee() === null ? null : $employeeDao->findById($invoice->getEmployee(), null, true));
                $vendor = ($invoice->getVendorid() === null ? null : $taxPayerDao->findById($invoice->getVendorid(), null, true));
                $business = ($invoice->getBusiness() === null ? null : $businessDao->findById($invoice->getBusiness(), null, true));

                $rows[] = [
                    "id" => $invoice->getInvoiceid(),
                    "created" => $dateTime->getTimestamp(),
                    "nickname" => $invoice->getNickname(),
                    "category" => ($taxableCategory === null ? null : $taxableCategory->getCategoryname()),
                    "employee" => ($employee === null ? null : ($employee->getFirstname() . " " . $employee->getLastname())),
                    "vendor" => ($vendor === null ? $invoice->getVendorname() : $vendor->getPayeename()),
                    "business" => ($business === null ? null : $business->getBusinessname()),
                    "total" => $invoice->getTotal(),
                    "currency" => $invoice->getCurrency()
                ];
            }

            unset($invoice);
        }

        $results = [
            "total" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function displaySourceDocuments(WebApplication $app) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];

        return $app["twig"]->render("accounting/accounting_source_documents.html.twig", [
            "nav_section" => "accounting",
            "side_section" => "source_documents",
            "data_sources" => DataSourceDAO::instance($connection)->findAll(null, "name ASC")
        ]);
    }

    public function createJournalEntry(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $logger =& LogUtil::getLogger();
        $requestContent = $request->getContent();
        $accounts = 0;
        $total = 0;

        if(!empty($requestContent)) {
            $journalEntry = Utils::decodeJson($requestContent);
            $generalLedgerId = (isset($journalEntry["generalLedgerId"]) ? $journalEntry["generalLedgerId"] : null);
            $datetime = (isset($journalEntry["datetime"]) ? $journalEntry["datetime"] : null);
            $timezoneId = (isset($journalEntry["timezone_id"]) ? $journalEntry["timezone_id"] : null);
            $entryAccounts = (isset($journalEntry["accounts"]) ? $journalEntry["accounts"] : []);
            $connection =& $app["merchant.mysql.connection"];
            $entryAccountCount = count($entryAccounts);
            $generalLedgerVo = null;
            $timezoneVo = null;

            $logger->debug("Creation of a journal entry touching " . $entryAccountCount . " accounts requested: " . Utils::getVarDump($journalEntry));

            if(isset($journalEntry["id"])) {
                $app->abort(Response::HTTP_BAD_REQUEST, "Cannot specify ID to create");
            }

            if($generalLedgerId !== null && $generalLedgerId > 0) {
                $generalLedgerVo =& GeneralLedgerDAO::instance($connection)->findById($generalLedgerId);
            }

            if($generalLedgerVo === null) {
                $app->abort(Response::HTTP_BAD_REQUEST, "Missing or invalid general ledger ID");
            }

            if($timezoneId !== null && $timezoneId > 0) {
                $timezoneVo =& TimezoneDAO::instance($connection)->findById($timezoneId);
            }

            if($timezoneVo === null) {
                $app->abort(Response::HTTP_BAD_REQUEST, "Missing or invalid timezone ID");
            }

            $journalEntryDao =& JournalEntryDAO::instance($connection);
            $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
            $dateTimeZone = new DateTimeZone($timezoneVo->getName());
            $journalEntry["occurred"] = DateTime::createFromFormat("Y-m-d H:i:s", $datetime, $dateTimeZone)->getTimestamp();
            /** @var JournalEntryVO $journalEntryVo */
            $journalEntryVo =& Utils::createCopy($journalEntry, JournalEntryVO::class);

            $journalEntryVo->setCreator($userId);

            $journalEntryId =& $journalEntryDao->insert($journalEntryVo);

            if($journalEntryId > 0) {
                $total++;

                if($entryAccountCount > 0 && !empty($datetime)) {
                    $journalEntryGeneralLedgerAccountDao =& JournalEntryGeneralLedgerAccountDAO::instance($connection);

                    foreach($entryAccounts as &$account) {
                        $debitCents = (isset($account["debit"]) ? Utils::moneyStringToCents($account["debit"]) : 0);
                        $creditCents = (isset($account["credit"]) ? Utils::moneyStringToCents($account["credit"]) : 0);

                        if($debitCents > 0 && $creditCents > 0) {
                            throw new Exception("Cannot debit and credit the same account in a single journal entry.");
                        } else if($debitCents <= 0 && $creditCents <= 0) {
                            throw new Exception("Must either debit or credit each account in a journal entry.");
                        } else if($debitCents > 0) {
                            $account["amount"] = Utils::moneyCentsToString($debitCents * -1);
                        } else {
                            $account["amount"] = Utils::moneyCentsToString($creditCents);
                        }

                        $account["journalEntryId"] = $journalEntryId;

                        /** @var JournalEntryGeneralLedgerAccountVO $journalEntryLedgerAccountVo */
                        $journalEntryLedgerAccountVo = Utils::createCopy($account, JournalEntryGeneralLedgerAccountVO::class);
                        $insertResult = $journalEntryGeneralLedgerAccountDao->insert($journalEntryLedgerAccountVo);

                        if($insertResult > 0) {
                            $accounts++;
                        }
                    }

                    unset($journalEntry);
                }
            }
        }

        $results = [
            "total" => $total,
            "accounts" => $accounts,
            "rows" => []
        ];

        return $app->json($results);
    }

    public function jsonJournalEntries(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $generalLedgerId = AppUtil::getIntFromQuery($request, "general_ledger_id");
        $generalLedgerVo = null;

        if($generalLedgerId !== null && $generalLedgerId > 0) {
            $generalLedgerVo =& GeneralLedgerDAO::instance($connection)->findById($generalLedgerId);
        }

        if($generalLedgerVo === null) {
            $app->abort(Response::HTTP_BAD_REQUEST, "Missing or invalid general ledger ID");
        }

        $journalEntries =& JournalEntryDAO::instance($connection)->findAllByColumn(
            "general_ledger_id", $generalLedgerVo->getId(), PDO::PARAM_INT
        );
        $journalEntryGeneralLedgerAccountDao =& JournalEntryGeneralLedgerAccountDAO::instance($connection);
        $generalLedgerAccountDao =& GeneralLedgerAccountDAO::instance($connection);
        $chartAccountDao =& ChartAccountDAO::instance($connection);
        $chartAccountTypeDao =& ChartAccountTypeDAO::instance($connection);
        $currencyDao =& CurrencyDAO::instance($connection);
        $rows = [];

        foreach($journalEntries as &$journalEntryVo) {
            $journalEntryGeneralLedgerAccounts =& $journalEntryGeneralLedgerAccountDao->findAllByColumn(
                "journal_entry_id", $journalEntryVo->getId(), PDO::PARAM_INT
            );
            $debits = [];
            $credits = [];
            $accounts = [];

            foreach($journalEntryGeneralLedgerAccounts as &$journalEntryGeneralLedgerAccountVo) {
                $generalLedgerAccountVo =& $generalLedgerAccountDao->findById($journalEntryGeneralLedgerAccountVo->getGeneralLedgerAccountId());
                $chartAccountVo =& $chartAccountDao->findById($generalLedgerAccountVo->getChartAccountId());
                $chartAccountTypeVo =& $chartAccountTypeDao->findById($chartAccountVo->getChartAccountTypeId(), null, true);
                $currencyVo =& $currencyDao->findById($generalLedgerAccountVo->getCurrencyId(), null, true);
                $accountRow = [
                    "general_ledger_account_id" => $journalEntryGeneralLedgerAccountVo->getGeneralLedgerAccountId(),
                    "amount" => $journalEntryGeneralLedgerAccountVo->getAmount(),
                    "chart_account_id" => $generalLedgerAccountVo->getChartAccountId(),
                    "chart_account_code" => $chartAccountVo->getCode(),
                    "chart_account_name" => $chartAccountVo->getName(),
                    "chart_account_type" => $chartAccountTypeVo->getName(),
                    "debit_direction" => $chartAccountTypeVo->getDebitDirection(),
                    "credit_direction" => $chartAccountTypeVo->getCreditDirection(),
                    "currency_id" => $generalLedgerAccountVo->getCurrencyId(),
                    "currency_iso_code" => $currencyVo->getIsoCode(),
                    "currency_name" => $currencyVo->getName(),
                    "currency_decimals" => $currencyVo->getDecimals()
                ];

                if(($chartAccountTypeVo->getDebitDirection() > 0 && $journalEntryGeneralLedgerAccountVo->getAmount() > 0) ||
                    ($chartAccountTypeVo->getDebitDirection() < 0 && $journalEntryGeneralLedgerAccountVo->getAmount() < 0)
                ) {
                    $debits[] = $accountRow;
                } else {
                    $credits[] = $accountRow;
                }
            }

            unset($journalEntryGeneralLedgerAccountVo);

            foreach($debits as &$debit) {
                $accounts[] = $debit;
            }

            unset($debit);

            foreach($credits as &$credit) {
                $accounts[] = $credit;
            }

            unset($credit);

            $rows[] = [
                "id" => $journalEntryVo->getId(),
                "created" => $journalEntryVo->getCreated(),
                "updated" => $journalEntryVo->getUpdated(),
                "general_ledger_id" => $journalEntryVo->getGeneralLedgerId(),
                "occurred" => $journalEntryVo->getOccurred(),
                "memo" => $journalEntryVo->getMemo(),
                "accounts" => $accounts
            ];
        }

        unset($journalEntryVo);

        $results = [
            "total" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function displayGeneralJournal(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $generalLedgerId = AppUtil::getIntFromQuery($request, "general_ledger_id");
        $generalLedgerVo = null;

        if($generalLedgerId !== null && $generalLedgerId > 0) {
            $generalLedgerVo =& GeneralLedgerDAO::instance($connection)->findById($generalLedgerId);
        }

        if($generalLedgerVo === null) {
            $app->abort(Response::HTTP_BAD_REQUEST, "Missing or invalid general ledger ID");
        }

        return $app["twig"]->render("accounting/accounting_general_journal.html.twig", [
            "nav_section" => "accounting",
            "general_ledger" => $generalLedgerVo,
            "set_of_books" => SetOfBooksDAO::instance($connection)->findById($generalLedgerVo->getSetOfBooksId()),
            "organization" => OrganizationDAO::instance($connection)->findById($generalLedgerVo->getOrganizationId()),
            "chart_of_accounts" => ChartOfAccountsDAO::instance($connection)->findById($generalLedgerVo->getChartOfAccountsId()),
            "general_ledger_currency" => CurrencyDAO::instance($connection)->findById($generalLedgerVo->getCurrencyId()),
            "currencies" => CurrencyDAO::instance($connection)->findAll(),
            "timezones" => TimezoneDAO::instance($connection)->findAll(null, "name ASC")
        ]);
    }

    public function updateGeneralLedgerAccounts(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
        $generalLedgerId = AppUtil::getIntFromQuery($request, "general_ledger_id");
        $generalLedgerVo = null;

        if($generalLedgerId !== null && $generalLedgerId > 0) {
            $generalLedgerVo =& GeneralLedgerDAO::instance($connection)->findById($generalLedgerId);
        }

        if($generalLedgerVo === null) {
            $app->abort(Response::HTTP_BAD_REQUEST, "Missing or invalid general ledger ID");
        }

        $total = 0;
        $updated = 0;
        $requestContent = $request->getContent();

        if(!empty($requestContent)) {
            $logger =& LogUtil::getLogger();
            $generalLedgerAccounts = json_decode($requestContent);
            $total = count($generalLedgerAccounts);

            $logger->debug("Update of " . $total . " general ledger accounts requested: " . Utils::getVarDump($generalLedgerAccounts));

            if($total > 0) {
                $generalLedgerAccountDao =& GeneralLedgerAccountDAO::instance($connection);
                $dateTimeZone = new DateTimeZone("America/Los_Angeles");

                foreach($generalLedgerAccounts as &$generalLedgerAccountVo) {
                    $generalLedgerAccountVo->general_ledger_id = $generalLedgerId;
                    $generalLedgerAccountVo->start = (
                        empty($generalLedgerAccountVo->start) ? null :
                        DateTime::createFromFormat("Y-m-d H:i:s", $generalLedgerAccountVo->start, $dateTimeZone)->getTimestamp()
                    );
                    /** @var GeneralLedgerAccountVO $updatedLedgerAccountVo */
                    $updatedLedgerAccountVo = Utils::createCopy($generalLedgerAccountVo, GeneralLedgerAccountVO::class);
                    $existingLedgerAccountVo = null;

                    if($updatedLedgerAccountVo->getId() !== null) {
                        $existingLedgerAccountVo =& $generalLedgerAccountDao->findById($updatedLedgerAccountVo->getId(), "FOR UPDATE");
                    }

                    if(empty($updatedLedgerAccountVo->getOpeningBalance())) {
                        $updatedLedgerAccountVo->setOpeningBalance(0);
                    }

                    if($existingLedgerAccountVo === null) {
                        throw new Exception("Tried to update non-existing ledger account: " . Utils::getVarDump($updatedLedgerAccountVo));
                    } else if(Utils::propertiesDiffer($existingLedgerAccountVo, $updatedLedgerAccountVo, ["created", "creator", "updated", "updater"])) {
                        $updatedLedgerAccountVo->setUpdater($userId);
                        $updateResult = $generalLedgerAccountDao->update($updatedLedgerAccountVo);

                        if($updateResult > 0) {
                            $updated++;
                        }
                    }
                }

                unset($generalLedgerAccountVo);
            }
        }

        $results = [
            "total" => $total,
            "updated" => $updated,
            "rows" => []
        ];

        return $app->json($results);
    }

    public function jsonGeneralLedgerAccounts(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
        $generalLedgerId = AppUtil::getIntFromQuery($request, "general_ledger_id");
        $generalLedgerVo = null;

        if($generalLedgerId !== null && $generalLedgerId > 0) {
            $generalLedgerVo =& GeneralLedgerDAO::instance($connection)->findById($generalLedgerId);
        }

        if($generalLedgerVo === null) {
            $app->abort(Response::HTTP_BAD_REQUEST, "Missing or invalid general ledger ID");
        }

        $chartAccountDao =& ChartAccountDAO::instance($connection);
        $chartAccounts =& $chartAccountDao->findAllByColumn("chart_of_accounts_id", $generalLedgerVo->getChartOfAccountsId(), PDO::PARAM_INT);
        $generalLedgerAccountDao =& GeneralLedgerAccountDAO::instance($connection);
        $currencyDao =& CurrencyDAO::instance($connection);
        $rows = [];

        foreach($chartAccounts as &$chartAccountVo) {
            $generalLedgerAccountProtoVo = new GeneralLedgerAccountVO();

            $generalLedgerAccountProtoVo->setGeneralLedgerId($generalLedgerVo->getId());
            $generalLedgerAccountProtoVo->setChartAccountId($chartAccountVo->getId());

            $generalLedgerAccountVo =& $generalLedgerAccountDao->findByPrototype($generalLedgerAccountProtoVo, null, null, "FOR UPDATE");

            if($generalLedgerAccountVo === null) {
                $generalLedgerAccountProtoVo->setCreator($userId);
                $generalLedgerAccountProtoVo->setCurrencyId($generalLedgerVo->getCurrencyId());
                $generalLedgerAccountProtoVo->setOpeningBalance(0);

                $generalLedgerAccountId =& $generalLedgerAccountDao->insert($generalLedgerAccountProtoVo);
                $generalLedgerAccountVo =& $generalLedgerAccountDao->findById($generalLedgerAccountId);
            }

            $currencyVo =& $currencyDao->findById($generalLedgerAccountVo->getCurrencyId(), null, true);
            $rows[] = [
                "id" => $generalLedgerAccountVo->getId(),
                "created" => $generalLedgerAccountVo->getCreated(),
                "updated" => $generalLedgerAccountVo->getUpdated(),
                "general_ledger_id" => $generalLedgerAccountVo->getGeneralLedgerId(),
                "chart_account_id" => $chartAccountVo->getId(),
                "currency_id" => $currencyVo->getId(),
                "start" => $generalLedgerAccountVo->getStart(),
                "opening_balance" => $generalLedgerAccountVo->getOpeningBalance(),
                "chart_account_code" => $chartAccountVo->getCode(),
                "chart_account_name" => $chartAccountVo->getName(),
                "currency_iso_code" => $currencyVo->getIsoCode(),
                "currency_name" => $currencyVo->getName(),
                "currency_decimals" => $currencyVo->getDecimals()
            ];
        }

        unset($chartAccountVo);

        $results = [
            "total" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function displayGeneralLedger(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $generalLedgerId = AppUtil::getIntFromQuery($request, "general_ledger_id");
        $generalLedgerVo = null;

        if($generalLedgerId !== null && $generalLedgerId > 0) {
            $generalLedgerVo =& GeneralLedgerDAO::instance($connection)->findById($generalLedgerId);
        }

        if($generalLedgerVo === null) {
            $app->abort(Response::HTTP_BAD_REQUEST, "Missing or invalid general ledger ID");
        }

        return $app["twig"]->render("accounting/accounting_general_ledger.html.twig", [
            "nav_section" => "accounting",
            "general_ledger" => $generalLedgerVo,
            "set_of_books" => SetOfBooksDAO::instance($connection)->findById($generalLedgerVo->getSetOfBooksId()),
            "organization" => OrganizationDAO::instance($connection)->findById($generalLedgerVo->getOrganizationId()),
            "chart_of_accounts" => ChartOfAccountsDAO::instance($connection)->findById($generalLedgerVo->getChartOfAccountsId()),
            "general_ledger_currency" => CurrencyDAO::instance($connection)->findById($generalLedgerVo->getCurrencyId()),
            "currencies" => CurrencyDAO::instance($connection)->findAll()
        ]);
    }

    public function createGeneralLedger(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $requestContent = $request->getContent();
        $rows = [];

        if(!empty($requestContent)) {
            $logger =& LogUtil::getLogger();
            $generalLedger = json_decode($requestContent);

            $logger->debug("General ledger creation request: " . Utils::getVarDump($generalLedger));

            $connection =& $app["merchant.mysql.connection"];
            $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
            $generalLedgerDao =& GeneralLedgerDAO::instance($connection);
            /** @var GeneralLedgerVO $generalLedgerVo */
            $generalLedgerVo =& Utils::createCopy($generalLedger, GeneralLedgerVO::class);

            $generalLedgerVo->setCreator($userId);

            $generalLedgerId =& $generalLedgerDao->insert($generalLedgerVo);

            $rows[] = $generalLedgerDao->findById($generalLedgerId);
        }

        $results = [
            "total" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function jsonGeneralLedgers(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $setOfBooksId = AppUtil::getIntFromQuery($request, "set_of_books_id");
        $setOfBooksVo = null;

        if($setOfBooksId !== null) {
            $setOfBooksVo =& SetOfBooksDAO::instance($connection)->findById($setOfBooksId);

            if($setOfBooksVo === null) {
                $app->abort(Response::HTTP_BAD_REQUEST, "Missing or invalid set of books ID");
            }
        }

        $generalLedgers =& GeneralLedgerDAO::instance($connection)->findAll(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$setOfBooksVo) {
                if($setOfBooksVo !== null) {
                    $pdoQuery->where("set_of_books_id", $setOfBooksVo->getId());
                }
            }, "name ASC"
        );
        $rows = [];

        if(!empty($generalLedgers)) {
            $organizationDao =& OrganizationDAO::instance($connection);
            $chartOfAccountsDao =& ChartOfAccountsDAO::instance($connection);

            foreach($generalLedgers as &$generalLedgerVo) {
                $organizationVo =& $organizationDao->findById($generalLedgerVo->getOrganizationId());
                $chartOfAccountsVo =& $chartOfAccountsDao->findById($generalLedgerVo->getChartOfAccountsId());

                $rows[] = [
                    "id" => $generalLedgerVo->getId(),
                    "created" => $generalLedgerVo->getCreated(),
                    "updated" => $generalLedgerVo->getUpdated(),
                    "set_of_books_id" => $generalLedgerVo->getSetOfBooksId(),
                    "organization_id" => $generalLedgerVo->getOrganizationId(),
                    "chart_of_accounts_id" => $generalLedgerVo->getChartOfAccountsId(),
                    "name" => $generalLedgerVo->getName(),
                    "currency_id" => $generalLedgerVo->getCurrencyId(),
                    "organization_code" => $organizationVo->getCode(),
                    "organization_name" => $organizationVo->getName(),
                    "chart_of_accounts" => $chartOfAccountsVo->getName()
                ];
            }

            unset($generalLedgerVo);
        }

        $results = [
            "total" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function displaySetOfBooks(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $setOfBooksId = AppUtil::getIntFromQuery($request, "set_of_books_id");
        $setOfBooksVo = null;

        if($setOfBooksId !== null && $setOfBooksId > 0) {
            $setOfBooksVo =& SetOfBooksDAO::instance($connection)->findById($setOfBooksId);
        }

        if($setOfBooksVo === null) {
            $app->abort(Response::HTTP_BAD_REQUEST, "Missing or invalid set of books ID");
        }

        return $app["twig"]->render("accounting/accounting_set_of_books.html.twig", [
            "nav_section" => "accounting",
            "set_of_books" => $setOfBooksVo,
            "organizations" => OrganizationDAO::instance($connection)->findAll(null, "name ASC"),
            "charts_of_accounts" => ChartOfAccountsDAO::instance($connection)->findAll(null, "name ASC"),
            "currencies" => CurrencyDAO::instance($connection)->findAll(),
            "general_ledgers" => GeneralLedgerDAO::instance($connection)->findAllByColumn("set_of_books_id", $setOfBooksVo->getId(), PDO::PARAM_INT)
        ]);
    }

    public function createSetOfBooks(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $requestContent = $request->getContent();
        $rows = [];

        if(!empty($requestContent)) {
            $logger =& LogUtil::getLogger();
            $setOfBooks = json_decode($requestContent);

            $logger->debug("Set of books creation request: " . Utils::getVarDump($setOfBooks));

            $connection =& $app["merchant.mysql.connection"];
            $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
            $setOfBooksDao =& SetOfBooksDAO::instance($connection);
            /** @var SetOfBooksVO $setOfBooksVo */
            $setOfBooksVo =& Utils::createCopy($setOfBooks, SetOfBooksVO::class);

            $setOfBooksVo->setCreator($userId);

            $setOfBooksId =& $setOfBooksDao->insert($setOfBooksVo);

            $rows[] = $setOfBooksDao->findById($setOfBooksId);
        }

        $results = [
            "total" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function jsonSetsOfBooks(WebApplication $app) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $setsOfBooks =& SetOfBooksDAO::instance($connection)->findAll(null, "name ASC");
        $rows = [];

        if(!empty($setsOfBooks)) {
            foreach($setsOfBooks as &$setOfBooks) {
                $rows[] = [
                    "id" => $setOfBooks->getId(),
                    "created" => $setOfBooks->getCreated(),
                    "updated" => $setOfBooks->getUpdated(),
                    "name" => $setOfBooks->getName()
                ];
            }

            unset($setOfBooks);
        }

        $results = [
            "total" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function displaySetsOfBooks(WebApplication $app) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        return $app["twig"]->render("accounting/accounting_sets_of_books.html.twig", [
            "nav_section" => "accounting",
            "side_section" => "sets_of_books"
        ]);
    }

    public function createFiscalPeriod(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $requestContent = $request->getContent();
        $rows = [];

        if(!empty($requestContent)) {
            $logger =& LogUtil::getLogger();
            $fiscalPeriod = json_decode($requestContent);

            $logger->debug("Fiscal period creation request: " . Utils::getVarDump($fiscalPeriod));

            $connection =& $app["merchant.mysql.connection"];
            $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
            $fiscalPeriodDao =& FiscalPeriodDAO::instance($connection);
            /** @var FiscalPeriodVO $fiscalPeriodVo */
            $fiscalPeriodVo =& Utils::createCopy($fiscalPeriod, FiscalPeriodVO::class, [], ["start", "end"]);
            $timezoneVo =& TimezoneDAO::instance($connection)->findById($fiscalPeriod->timezone_id);
            $dateTimeZone = new DateTimeZone($timezoneVo->getName());

            $fiscalPeriodVo->setCreator($userId);
            $fiscalPeriodVo->setStart((new DateTime($fiscalPeriod->start . ' 00:00:00', $dateTimeZone))->getTimestamp());
            $fiscalPeriodVo->setEnd((new DateTime($fiscalPeriod->end . ' 23:59:59', $dateTimeZone))->getTimestamp());

            $fiscalPeriodId =& $fiscalPeriodDao->insert($fiscalPeriodVo);

            $rows[] = $fiscalPeriodDao->findById($fiscalPeriodId);
        }

        $results = [
            "total" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function jsonFiscalPeriods(WebApplication $app) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $fiscalPeriodDao =& FiscalPeriodDAO::instance($connection);
        $fiscalPeriods =& $fiscalPeriodDao->findAll(null, "end DESC, start DESC");
        $rows = [];

        if(!empty($fiscalPeriods)) {
            $fiscalPeriodTypeDao =& FiscalPeriodTypeDAO::instance($connection);

            foreach($fiscalPeriods as &$fiscalPeriod) {
                $rows[] = [
                    "id" => $fiscalPeriod->getId(),
                    "created" => $fiscalPeriod->getCreated(),
                    "updated" => $fiscalPeriod->getUpdated(),
                    "fiscal_period_type_id" => $fiscalPeriod->getFiscalPeriodTypeId(),
                    "name" => $fiscalPeriod->getName(),
                    "start" => $fiscalPeriod->getStart(),
                    "end" => $fiscalPeriod->getEnd(),
                    "fiscal_period_type" => $fiscalPeriodTypeDao->findById($fiscalPeriod->getFiscalPeriodTypeId(), null, true)->getName()
                ];
            }

            unset($fiscalPeriod);
        }

        $results = [
            "total" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function displayFiscalPeriods(WebApplication $app) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];

        return $app["twig"]->render("accounting/accounting_fiscal_periods.html.twig", [
            "nav_section" => "accounting",
            "side_section" => "fiscal_periods",
            "fiscal_period_types" => FiscalPeriodTypeDAO::instance($connection)->findAll(null, "name ASC"),
            "timezones" => TimezoneDAO::instance($connection)->findAll(null, "name ASC")
        ]);
    }

    public function deleteChartAccount(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
        $chartAccountId = AppUtil::getIntFromQuery($request, "chart_account_id");
        $chartAccountDao =& ChartAccountDAO::instance($connection);
        $chartAccountVo = null;
        $rows = [];

        if($chartAccountId !== null && $chartAccountId > 0) {
            $chartAccountVo =& $chartAccountDao->findById($chartAccountId, "FOR UPDATE");
        }

        if($chartAccountVo === null) {
            $app->abort(Response::HTTP_BAD_REQUEST, "Missing or invalid account ID");
        }

        $chartOfAccountsDao =& ChartOfAccountsDAO::instance($connection);
        $chartOfAccountsVo =& $chartOfAccountsDao->findById($chartAccountVo->getChartOfAccountsId(), "FOR UPDATE");

        PDOQuery::delete()
            ->from(GeneralLedgerAccountDAO::instance($connection))
            ->where("chart_account_id", $chartAccountId, PDO::PARAM_INT)
            ->executeGetRowCount($connection)
        ;

        $deleted =& $chartAccountDao->delete($chartAccountVo->getId());

        if($deleted > 0) {
            $chartOfAccountsVo->setUpdater($userId);
            $chartOfAccountsDao->update($chartOfAccountsVo);

            $rows[] = self::createChartAccountRow($chartAccountVo, ChartAccountTypeDAO::instance($connection));
        }

        $results = [
            "total" => count($rows),
            "deleted" => $deleted,
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function createChartAccount(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $requestContent = $request->getContent();
        $rows = [];

        if(!empty($requestContent)) {
            $logger =& LogUtil::getLogger();
            $chartAccount = json_decode($requestContent);

            $logger->debug("Chart account creation request: " . Utils::getVarDump($chartAccount));

            $connection =& $app["merchant.mysql.connection"];
            $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
            $chartOfAccountsId = AppUtil::getIntFromQuery($request, "chart_of_accounts_id");
            $chartOfAccountsDao =& ChartOfAccountsDAO::instance($connection);
            $chartOfAccountsVo =& $chartOfAccountsDao->findById($chartOfAccountsId, "FOR UPDATE");
            $chartAccountDao =& ChartAccountDAO::instance($connection);
            /** @var ChartAccountVO $chartAccountVo */
            $chartAccountVo =& Utils::createCopy($chartAccount, ChartAccountVO::class);

            $chartAccountVo->setCreator($userId);
            $chartAccountVo->setChartOfAccountsId($chartOfAccountsVo->getId());

            $chartAccountId =& $chartAccountDao->insert($chartAccountVo);
            $chartAccountVo =& $chartAccountDao->findById($chartAccountId);
            $chartOfAccountsVo->setUpdater($userId);
            $chartOfAccountsDao->update($chartOfAccountsVo);

            $rows[] = $chartAccountVo;
        }

        $results = [
            "total" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function jsonChartAccounts(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $chartOfAccountsId = AppUtil::getIntFromQuery($request, "chart_of_accounts_id");
        $chartOfAccountsVo = null;

        if($chartOfAccountsId !== null && $chartOfAccountsId > 0) {
            $chartOfAccountsVo =& ChartOfAccountsDAO::instance($connection)->findById($chartOfAccountsId);
        }

        if($chartOfAccountsVo === null) {
            $app->abort(Response::HTTP_BAD_REQUEST, "Missing or invalid chart of accounts ID");
        }

        $chartAccountDao =& ChartAccountDAO::instance($connection);
        $chartAccounts =& $chartAccountDao->findAllByColumn("chart_of_accounts_id", $chartOfAccountsId, PDO::PARAM_INT, "code ASC, name DESC");
        $rows = [];

        if(!empty($chartAccounts)) {
            $chartAccountTypeDao =& ChartAccountTypeDAO::instance($connection);

            foreach($chartAccounts as &$chartAccount) {
                $rows[] = self::createChartAccountRow($chartAccount, $chartAccountTypeDao);
            }

            unset($chartAccount);
        }

        $results = [
            "total" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    private static function createChartAccountRow(ChartAccountVO &$chartAccountVo, ChartAccountTypeDAO &$chartAccountTypeDao) {
        $chartAccountTypeVo =& $chartAccountTypeDao->findById($chartAccountVo->getChartAccountTypeId(), null, true);

        return [
            "id" => $chartAccountVo->getId(),
            "created" => $chartAccountVo->getCreated(),
            "updated" => $chartAccountVo->getUpdated(),
            "chart_of_accounts_id" => $chartAccountVo->getChartOfAccountsId(),
            "chart_account_type_id" => $chartAccountVo->getChartAccountTypeId(),
            "code" => $chartAccountVo->getCode(),
            "name" => $chartAccountVo->getName(),
            "chart_account_type" => $chartAccountTypeVo->getName(),
            "to_increase" => ($chartAccountTypeVo->getDebitDirection() > 0 ? "dr" : "cr")
        ];
    }

    public function displayChartAccounts(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $chartOfAccountsId = AppUtil::getIntFromQuery($request, "chart_of_accounts_id");
        $chartOfAccountsDao =& ChartOfAccountsDAO::instance($connection);
        $chartOfAccountsVo = null;

        if($chartOfAccountsId !== null && $chartOfAccountsId > 0) {
            $chartOfAccountsVo =& $chartOfAccountsDao->findById($chartOfAccountsId);
        }

        if($chartOfAccountsVo === null) {
            $app->abort(Response::HTTP_BAD_REQUEST, "Missing or invalid chart of accounts ID");
        }

        return $app["twig"]->render("accounting/accounting_chart_accounts.html.twig", [
            "nav_section" => "accounting",
            "chart_account_types" => ChartAccountTypeDAO::instance($connection)->findAll(null, "name ASC"),
            "chart_of_accounts" => $chartOfAccountsDao->findById($chartOfAccountsId)
        ]);
    }

    public function createChartOfAccounts(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $requestContent = $request->getContent();
        $rows = [];

        if(!empty($requestContent)) {
            $logger =& LogUtil::getLogger();
            $chartOfAccounts = json_decode($requestContent);

            $logger->debug("Chart of accounts creation request: " . Utils::getVarDump($chartOfAccounts));

            $connection =& $app["merchant.mysql.connection"];
            $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
            $chartOfAccountsDao =& ChartOfAccountsDAO::instance($connection);
            /** @var ChartOfAccountsVO $chartOfAccountsVo */
            $chartOfAccountsVo = Utils::createCopy($chartOfAccounts, ChartOfAccountsVO::class);

            $chartOfAccountsVo->setCreator($userId);

            $chartOfAccountsId =& $chartOfAccountsDao->insert($chartOfAccountsVo);

            $rows[] = $chartOfAccountsDao->findById($chartOfAccountsId);
        }

        $results = [
            "total" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function jsonChartsOfAccounts(WebApplication $app) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $chartsOfAccounts =& ChartOfAccountsDAO::instance($connection)->findAll(null, "name ASC");
        $rows = [];

        if(!empty($chartsOfAccounts)) {
            foreach($chartsOfAccounts as &$chartOfAccounts) {
                $rows[] = [
                    "id" => $chartOfAccounts->getId(),
                    "created" => $chartOfAccounts->getCreated(),
                    "updated" => $chartOfAccounts->getUpdated(),
                    "name" => $chartOfAccounts->getName()
                ];
            }

            unset($chartOfAccounts);
        }

        $results = [
            "total" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function displayChartsOfAccounts(WebApplication $app) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        return $app["twig"]->render("accounting/accounting_charts_of_accounts.html.twig", [
            "nav_section" => "accounting",
            "side_section" => "charts_of_accounts"
        ]);
    }

    public function displayDefault(WebApplication $app) {
        if(!$app->isGranted(UserRoleDAO::ROLE_FINANCE)) {
            throw new AccessDeniedException();
        }

        return $app["twig"]->render("accounting/accounting.html.twig", [
            "nav_section" => "accounting"
        ]);
    }
}
