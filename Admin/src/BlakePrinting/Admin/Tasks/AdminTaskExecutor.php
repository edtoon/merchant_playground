<?php

namespace BlakePrinting\Admin\Tasks;

use BlakePrinting\Db\PDOUtil;
use BlakePrinting\InkHeroGlobal\Tasks\ImportSaleInvoiceTask;
use BlakePrinting\Tasks\AbstractTask;
use BlakePrinting\Tasks\TaskLockDAO;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\InkHeroApi\Tasks\ImportInkHeroApiTask;
use BlakePrinting\WalmartMarketplace\Orders\Tasks\ImportWalmartOrdersTask;
use Exception;
use Monolog\Logger;
use PDO;

class AdminTaskExecutor {
    const APP_NAME = "AdminTaskExecutor";
    const APP_VERSION = "0.0.1";

    public function run() {
        LogUtil::configureLogger(__CLASS__);

        $connection =& PDOUtil::getMysqlConnection();
        $taskLockDao =& TaskLockDAO::instance($connection);

        PDOUtil::pauseSqlLogging();

        try {
            $taskLockDao->deleteExpired();
        } finally {
            PDOUtil::resumeSqlLogging();
        }

        PDOUtil::commitConnection($connection);

        /*
        $this->executeTask(
            $connection,
            new ImportSaleInvoiceTask(
                "global_sale_invoices", self::APP_NAME, self::APP_VERSION, 60, $connection
            )
        );
        $this->executeTask(
            $connection,
            new ImportWalmartOrdersTask(
                "walmart_orders", self::APP_NAME, self::APP_VERSION, 60, $connection
            )
        );
        */
        $this->executeTask(
            $connection,
            new ImportInkHeroApiTask(
                "ink_hero_api", self::APP_NAME, self::APP_VERSION, 60, $connection
            )
        );
    }

    private function executeTask(PDO &$connection, AbstractTask $task) {
        LogUtil::configureLogger(get_class($task));

        try {
            $exitCode = 0;

            if($task->lock()) {
                try {
                    $exitCode = $task->run();
                } finally {
                    $task->unlock();
                }
            }

            if($exitCode === 0) {
                PDOUtil::commitConnection($connection);
            }
        } catch(Exception $e) {
            LogUtil::logException(Logger::ERROR, $e, "Error executing task: " . get_class($task));
        } finally {
            PDOUtil::rollbackConnection($connection);
        }
    }
}
