<?php

namespace BlakePrinting\Admin\Gateway;

use BlakePrinting\Admin\WebApplication;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Facilities\FacilityLocationDAO;
use BlakePrinting\Labels\LabelDAO;
use BlakePrinting\Labels\LabelTypeDAO;
use BlakePrinting\Labels\LabelUtil;
use BlakePrinting\Locations\LocationDAO;
use BlakePrinting\Organizations\OrganizationDAO;
use BlakePrinting\Printers\GatewayApiKeyDAO;
use BlakePrinting\Printers\GatewayDAO;
use BlakePrinting\Printers\GatewayPrinterDAO;
use BlakePrinting\Printers\GatewayPrinterLabelQueueDAO;
use BlakePrinting\Printers\GatewayVO;
use BlakePrinting\Printers\PrinterAttributeDAO;
use BlakePrinting\Printers\PrinterConnectionIpv4DAO;
use BlakePrinting\Printers\PrinterDAO;
use BlakePrinting\Printers\PrinterStatusDAO;
use BlakePrinting\Printers\PrinterVO;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use Exception;
use PDO;
use ReflectionClass;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class GatewayControllerProvider implements ControllerProviderInterface {
    const READY_STATUS = "Online";

    public function connect(Application $app) {
        $controllers =& $app["controllers_factory"];

        if($app instanceof WebApplication) {
            $controllers->post("/v1", self::class . "::apiV1")
                ->bind("gateway_v1")
            ;
        }

        return $controllers;
    }

    public function apiV1(WebApplication $app, Request $request) {
        $contentType = $request->getContentType();
        $content = $request->getContent();
        $logger =& LogUtil::getLogger();

        $logger->debug("Content type: " . $contentType . ", content: " . $content);

        if($contentType == "json" && !empty($content)) {
            $apiRequest = json_decode($content, true, 512, JSON_BIGINT_AS_STRING);

            if($apiRequest !== null && !empty($apiRequest["api_key"]) && !empty($apiRequest["action"])) {
                $apiKey =& $apiRequest["api_key"];
                $connection =& $app["merchant.mysql.connection"];
                $gatewayApiKeyDao =& GatewayApiKeyDAO::instance($connection);
                $gatewayApiKeyVo =& $gatewayApiKeyDao->findByColumn("api_key", $apiKey, PDO::PARAM_STR);

                if($gatewayApiKeyVo !== null) {
                    $gatewayId = $gatewayApiKeyVo->getGatewayId();
                    $gatewayApiKeyId = $gatewayApiKeyVo->getId();
                    $update = PDOQuery::update()
                        ->table($gatewayApiKeyDao)
                        ->set("updated", "UNIX_TIMESTAMP()")
                        ->where("id", $gatewayApiKeyId, PDO::PARAM_INT)
                    ;

                    if($gatewayApiKeyVo->getActivated() === null) {
                        $update->set("activated", "UNIX_TIMESTAMP()");
                    }

                    $update->executeEnsuringRowCount($connection, 1);

                    $logger->debug("Using api key #" . $gatewayApiKeyId . " on gateway #" . $gatewayId);

                    $gatewayDao =& GatewayDAO::instance($connection);
                    $gatewayVo =& $gatewayDao->findById($gatewayId);

                    if($gatewayVo !== null) {
                        switch($apiRequest["action"]) {
                            case "ping":
                                return $this->apiV1Ping($app, $gatewayVo, $apiRequest);
                            break;
                            case "complete":
                                return $this->apiV1Complete($app, $gatewayVo, $apiRequest);
                            break;
                        }
                    }
                }
            }
        }

        $results = [
            "total" => 0,
            "rows" => []
        ];

        return $app->json($results);
    }

    private function apiV1Ping(WebApplication &$app, GatewayVO &$gatewayVo, array &$apiRequest) {
        $connection =& $app["merchant.mysql.connection"];

        $this->storeGatewayPrinters($connection, $gatewayVo, $apiRequest);

        $gatewayPrinterLabelQueueDao =& GatewayPrinterLabelQueueDAO::instance($connection);
        $labelDao =& LabelDAO::instance($connection);
        $labelTypeDao =& LabelTypeDAO::instance($connection);
        $printerConnectionIpv4Dao =& PrinterConnectionIpv4DAO::instance($connection);
        $queuedLabels =& PDOQuery::select()
            ->column("label.id")
            ->column("label_type.name AS type")
            ->column("gateway_printer_label_queue.printer_id")
            ->column("INET6_NTOA(ipv4) AS ipv4")
            ->column("port")
            ->from($gatewayPrinterLabelQueueDao)
            ->join($labelDao, ["gateway_printer_label_queue.label_id = label.id"])
            ->join($labelTypeDao, ["label.label_type_id = label_type.id"])
            ->join($printerConnectionIpv4Dao, ["gateway_printer_label_queue.printer_id = printer_connection_ipv4.printer_id"])
            ->where("gateway_id", $gatewayVo->getId(), PDO::PARAM_INT)
            ->where("completed IS NULL")
            ->where("cancelled IS NULL")
            ->getResults($connection)
        ;

        if(!empty($queuedLabels)) {
            foreach($queuedLabels as &$queuedLabel) {
                $items = LabelUtil::getLabelContents($queuedLabel["id"], $connection);

                $queuedLabel["itemCount"] = count($items);
                $queuedLabel["items"] =& $items;
            }

            unset($queuedLabel);
        }

        $results = [
            "total" => count($queuedLabels),
            "rows" => $queuedLabels
        ];

        return $app->json($results);
    }

    private function apiV1Complete(WebApplication &$app, GatewayVO &$gatewayVo, array &$apiRequest) {
        $connection =& $app["merchant.mysql.connection"];
        $labelId = (empty($apiRequest["label_id"]) ? null : $apiRequest["label_id"]);
        $printerId = (empty($apiRequest["printer_id"]) ? null : $apiRequest["printer_id"]);
        $total = 0;
        $stored = 0;

        if($labelId !== null && $printerId !== null) {
            $gatewayPrinterLabelQueueDao =& GatewayPrinterLabelQueueDAO::instance($connection);
            $total++;

            $stored =& $gatewayPrinterLabelQueueDao->store($gatewayVo->getId(), $printerId, $labelId, true);
        }

        $results = [
            "total" => $total,
            "stored" => $stored,
            "rows" => []
        ];

        return $app->json($results);
    }

    private function storeGatewayPrinters(PDO $connection, GatewayVO &$gatewayVo, array &$apiRequest) {
        $gatewayPrinterDao =& GatewayPrinterDAO::instance($connection);
        $gatewayId = $gatewayVo->getId();
        $keepPrinterIds = [];

        if(!empty($apiRequest["printers"])) {
            $gatewayFacilityId = $gatewayVo->getFacilityId();
            $organizationDao =& OrganizationDAO::instance($connection);
            $zebraOrganization =& $organizationDao->findByColumn("name", OrganizationDAO::ORGANIZATION_BASE_ZEBRA["name"], PDO::PARAM_STR);

            if($zebraOrganization === null) {
                throw new Exception("Couldn't find Zebra organization entry");
            }

            $zebraOrganizationId = $zebraOrganization->getId();

            foreach($apiRequest["printers"] as &$printer) {
                $companyAbbreviation =& $printer["company_abbreviation"];

                if($companyAbbreviation !== "ZBR") {
                    throw new Exception("Unrecognized company abbreviation: " . $companyAbbreviation);
                }

                $printerId =& self::storePrinter($connection, $printer, $zebraOrganizationId);

                $keepPrinterIds[] = $printerId;

                $gatewayPrinterDao->store($gatewayId, $printerId);

                self::storeZebraPrinterStatus($connection, $printer, $printerId, $gatewayFacilityId);
                self::storeZebraPrinterIpv4($connection, $printer, $printerId);
                self::storeZebraPrinterAttributes($connection, $printer, $printerId);
            }
        }

        $gatewayPrinterDao->deletePrintersNotIn($gatewayId, $keepPrinterIds);
    }

    /**
     * @param int $manufacturerOrganizationId
     * @return int
     */
    private function &storePrinter(PDO $connection, array &$printer, $manufacturerOrganizationId) {
        $printerProtoVo = new PrinterVO();

        $printerProtoVo->setManufacturerOrganizationId($manufacturerOrganizationId);
        $printerProtoVo->setProductName($printer["product_name"]);
        $printerProtoVo->setProductNumber($printer["product_number"]);
        $printerProtoVo->setSerialNumber($printer["serial_number"]);
        $printerProtoVo->setSystemName($printer["system_name"]);

        $printerDao =& PrinterDao::instance($connection);
        $printerVoList =& $printerDao->findAllByPrototype($printerProtoVo, null, null, "FOR UPDATE");
        $printerVo = (empty($printerVoList) ? null : $printerVoList[0]);

        if($printerVo === null) {
            $printerId =& $printerDao->insert($printerProtoVo);
            $printerVo =& $printerProtoVo;

            $printerVo->setId($printerId);
        }

        $printerId = $printerVo->getId();

        return $printerId;
    }

    /**
     * @param int $printerId
     * @param int $facilityId
     */
    private static function storeZebraPrinterStatus(PDO $connection, array &$printer, &$printerId, &$facilityId) {
        $facilityLocationDao =& FacilityLocationDAO::instance($connection);
        $printerStatusDao =& PrinterStatusDAO::instance($connection);
        $existingStatusVo =& $printerStatusDao->findById($printerId, "FOR UPDATE");
        $printerStatusDesc =& $printer["port_status"];
        $useNetwork =& $printer["use_network"];
        $ready = ($printerStatusDesc == self::READY_STATUS && $useNetwork === true);
        $facilityLocationId = $facilityLocationDao->findByColumn("facility_id", $facilityId, PDO::PARAM_INT)->getLocationId();
        $locationId = $facilityLocationId;
        $existingLocationId = null;

        if($existingStatusVo !== null) {
            $locationDao =& LocationDAO::instance($connection);
            $existingLocationId = $existingStatusVo->getLocationId();

            if($existingLocationId != $facilityLocationId) {
                if($locationDao->checkLocationDescendsFrom($existingLocationId, $facilityLocationId)) {
                    $locationId = $existingLocationId;
                } else {
                    $logger =& LogUtil::getLogger();

                    $logger->warn(
                        "Printer #" . $printerId . " moved from location #" . $existingLocationId . " " .
                        "to a different facility (facilityId: " . $facilityId . ", locationId: " . $facilityLocationId . ")"
                    );
                }
            }
        }

        if($existingStatusVo === null || (
            !Utils::valuesMatch($printerStatusDesc, $existingStatusVo->getStatusDescription()) ||
            !Utils::valuesMatch($useNetwork, $existingStatusVo->isUseNetwork()) ||
            !Utils::valuesMatch($ready, $existingStatusVo->isReady() ||
            !Utils::valuesMatch($existingLocationId, $locationId))
        )) {
            $printerStatusDao->store($printerId, $locationId, $printerStatusDesc, $useNetwork, $ready);
        }
    }

    /**
     * @param int $printerId
     */
    private static function storeZebraPrinterIpv4(PDO $connection, array &$printer, &$printerId) {
        $printerConnectionIpv4Dao =& PrinterConnectionIpv4DAO::instance($connection);
        $printerIpv4 =& $printer["ip_address_v4"];
        $printerPort =& $printer["printer_port"];
        $existingIpv4Connections =& $printerConnectionIpv4Dao->findAll(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$printerId) {
                $pdoQuery->where("printer_id", $printerId, PDO::PARAM_INT);
            }
        );
        $ipv4ConnectionAlreadyExists = false;

        foreach($existingIpv4Connections as &$existingIpv4Connection) {
            if(Utils::valuesMatch($printerIpv4, $existingIpv4Connection->getIpv4()) &&
                Utils::valuesMatch($printerPort, $existingIpv4Connection->getPort())
            ) {
                $ipv4ConnectionAlreadyExists = true;
            } else {
                $printerConnectionIpv4Dao->delete($existingIpv4Connection->getId());
            }
        }

        unset($existingIpv4Connection);

        if(!$ipv4ConnectionAlreadyExists) {
            $printerConnectionIpv4Dao->create(
                $printerId, $printerPort, $printerIpv4,
                $printer["default_gateway_v4"],
                $printer["subnet_mask"]
            );
        }
    }

    /**
     * @param int $printerId
     */
    private static function storeZebraPrinterAttributes(PDO $connection, array &$printer, &$printerId) {
        $printerAttributeDao =& PrinterAttributeDAO::instance($connection);

        $printerAttributeDao->deleteAttributes($printerId);

        if(!empty($printer["date_code"])) {
            $printerAttributeDao->storeAttribute($printerId, PrinterAttributeDAO::ATTR_DATE_CODE, $printer["date_code"]);
        }

        if(!empty($printer["discovery_version"])) {
            $printerAttributeDao->storeAttribute($printerId, PrinterAttributeDAO::ATTR_DISCOVERY_VERSION, $printer["discovery_version"]);
        }

        if(!empty($printer["encrypted_get_community_name"])) {
            $printerAttributeDao->storeAttribute($printerId, PrinterAttributeDAO::ATTR_GET_COMMUNITY_NAME, $printer["encrypted_get_community_name"]);
        }

        if(!empty($printer["encrypted_set_community_name"])) {
            $printerAttributeDao->storeAttribute($printerId, PrinterAttributeDAO::ATTR_SET_COMMUNITY_NAME, $printer["encrypted_set_community_name"]);
        }

        if(!empty($printer["firmware_version"])) {
            $printerAttributeDao->storeAttribute($printerId, PrinterAttributeDAO::ATTR_FIRMWARE_VERSION, $printer["firmware_version"]);
        }

        if(!empty($printer["hardware_address"])) {
            $printerAttributeDao->storeAttribute($printerId, PrinterAttributeDAO::ATTR_HARDWARE_ADDRESS, $printer["hardware_address"]);
        }

        if(!empty($printer["port_name"])) {
            $printerAttributeDao->storeAttribute($printerId, PrinterAttributeDAO::ATTR_PORT_NAME, $printer["port_name"]);
        }
    }
}
