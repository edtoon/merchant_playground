<?php

namespace BlakePrinting\Admin\Amazon;

use BlakePrinting\Admin\WebApplication;
use BlakePrinting\AmazonMws\EndpointDAO;
use BlakePrinting\AmazonMws\Sellers\CredentialDAO;
use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceDAO;
use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceListDAO;
use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceParticipationDAO;
use BlakePrinting\Users\UserRoleDAO;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use Exception;
use PDO;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class AmazonControllerProvider implements ControllerProviderInterface {
    public function connect(Application $app) {
        $controllers = $app["controllers_factory"];

        if($app instanceof WebApplication) {
            $controllers->post("/marketplaces", self::class . "::updateMarketplaces")
                ->bind("amazon_marketplaces_update")
            ;
            $controllers->get("/marketplaces.json", self::class . "::jsonMarketplaces")
                ->bind("amazon_marketplaces_json")
            ;
            $controllers->get("/marketplaces", self::class . "::displayMarketplaces")
                ->bind("amazon_marketplaces")
            ;
            $controllers->get("/", self::class . "::displayDefault")
                ->bind("amazon")
            ;
        }

        return $controllers;
    }

    public function updateMarketplaces(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_ADMIN)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
        $total = 0;
        $updated = 0;
        $requestContent = $request->getContent();

        if(!empty($requestContent)) {
            $logger =& LogUtil::getLogger();
            $marketplaceInstances = json_decode($requestContent);
            $total = count($marketplaceInstances);

            $logger->debug("Update of " . $total . " marketplaces requested: " . Utils::getVarDump($marketplaceInstances));

            if($total > 0) {
                $marketplaceInstanceDao =& MarketplaceInstanceDAO::instance($connection);

                foreach($marketplaceInstances as &$marketplaceInstance) {
                    $marketplaceInstanceVo =& $marketplaceInstanceDao->findById($marketplaceInstance->id, "FOR UPDATE");

                    if($marketplaceInstanceVo === null) {
                        throw new Exception("Tried to update non-existing marketplace: " . Utils::getVarDump($marketplaceInstance));
                    } else if($marketplaceInstanceVo->getActive() != $marketplaceInstance->active) {
                        $marketplaceInstanceVo->setActive($marketplaceInstance->active);
                        $marketplaceInstanceVo->setUpdater($userId);

                        $updateResult = $marketplaceInstanceDao->update($marketplaceInstanceVo);

                        if($updateResult > 0) {
                            $updated++;
                        }
                    }
                }

                unset($generalLedgerAccountVo);
            }
        }

        $results = [
            "total" => $total,
            "updated" => $updated,
            "rows" => []
        ];

        return $app->json($results);
    }

    public function jsonMarketplaces(WebApplication $app) {
        if(!$app->isGranted(UserRoleDAO::ROLE_ADMIN)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $marketplaceInstanceDao =& MarketplaceInstanceDAO::instance($connection);
        $marketplaceInstances =& $marketplaceInstanceDao->findAll(null, "name ASC");
        $rows = [];

        if(!empty($marketplaceInstances)) {
            $credentialDao =& CredentialDAO::instance($connection);
            $marketplaceInstanceParticipationDao =& MarketplaceInstanceParticipationDAO::instance($connection);

            foreach($marketplaceInstances as &$marketplaceInstanceVo) {
                $credentialVo = $credentialDao->findById($marketplaceInstanceVo->getCredentialId(), null, true);
                $marketplaceInstanceParticipationVo =& $marketplaceInstanceParticipationDao->findByColumn(
                    "marketplace_instance_id", $marketplaceInstanceVo->getId(), PDO::PARAM_INT
                );
                $updatedOptions = [$marketplaceInstanceVo->getUpdated()];
                $participating = null;

                if($credentialVo->getUpdated() !== null) {
                    $updatedOptions[] = $credentialVo->getUpdated();
                }

                if($marketplaceInstanceParticipationVo !== null) {
                    $participating = ($marketplaceInstanceParticipationVo->isHasSellerSuspendedListings() ? false : true);
                    $updatedOptions[] = $marketplaceInstanceParticipationVo->getCreated();

                    if($marketplaceInstanceParticipationVo->getUpdated() !== null) {
                        $marketplaceInstanceParticipationVo->getUpdated();
                    }
                }

                $rows[] = [
                    "id" => $marketplaceInstanceVo->getId(),
                    "created" => $marketplaceInstanceVo->getCreated(),
                    "updated" => max($updatedOptions),
                    "name" => $marketplaceInstanceVo->getName(),
                    "marketplace_id" => $marketplaceInstanceVo->getMwsMarketplaceId(),
                    "domain" => $marketplaceInstanceVo->getDomainName(),
                    "active" => $marketplaceInstanceVo->getActive(),
                    "seller_id" => $credentialVo->getSellerId(),
                    "participating" => $participating
                ];
            }

            unset($invoice);
        }

        $results = [
            "total" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function displayMarketplaces(WebApplication $app) {
        if(!$app->isGranted(UserRoleDAO::ROLE_ADMIN)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $marketplaceInstanceListDao =& MarketplaceInstanceListDAO::instance($connection);
        $marketplaceInstanceLists =& $marketplaceInstanceListDao->findAll();
        $credentialDao =& CredentialDAO::instance($connection);
        $endpointDao =& EndpointDAO::instance($connection);
        $credentials = [];

        if($marketplaceInstanceLists !== null) {
            foreach($marketplaceInstanceLists as &$marketplaceInstanceListVo) {
                $credentialVo =& $credentialDao->findById($marketplaceInstanceListVo->getCredentialId());
                $endpointVo =& $endpointDao->findById($credentialVo->getEndpointId(), null, true);

                $credentials[] = [
                    "seller_id" => $credentialVo->getSellerId(),
                    "endpoint" => $endpointVo->getEndpoint(),
                    "list_created" => $marketplaceInstanceListVo->getCreated(),
                    "list_updated" => $marketplaceInstanceListVo->getUpdated(),
                    "list_next_token" => $marketplaceInstanceListVo->getNextToken(),
                ];
            }

            unset($marketplaceInstanceListVo);
        }

        return $app["twig"]->render("amazon/amazon_marketplaces.html.twig", [
            "nav_section" => "amazon",
            "side_section" => "marketplaces",
            "credentials" => $credentials
        ]);
    }

    public function displayDefault(WebApplication $app) {
        return $app["twig"]->render("amazon/amazon.html.twig", [
            "nav_section" => "amazon"
        ]);
    }
}
