<?php

namespace BlakePrinting\Admin\Sales;

use BlakePrinting\Admin\WebApplication;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;

class SalesControllerProvider implements ControllerProviderInterface {
    public function connect(Application $app) {
        $controllers = $app["controllers_factory"];

        if($app instanceof WebApplication) {
            SalesOrdersController::connect($app, $controllers);

            $controllers->get("/", self::class . "::displayDefault")
                ->bind("sales")
            ;
        }

        return $controllers;
    }

    public function displayDefault(WebApplication $app) {
        return $app["twig"]->render("sales/sales.html.twig", [
            "nav_section" => "sales"
        ]);
    }
}
