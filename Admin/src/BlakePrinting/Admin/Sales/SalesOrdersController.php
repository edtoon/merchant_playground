<?php

namespace BlakePrinting\Admin\Sales;

use BlakePrinting\Admin\AppUtil;
use BlakePrinting\Admin\WebApplication;
use BlakePrinting\Catalog\CatalogProductDAO;
use BlakePrinting\Contacts\ContactAddressAddressTypeDAO;
use BlakePrinting\Contacts\ContactAddressDAO;
use BlakePrinting\Contacts\ContactAddressTypeDAO;
use BlakePrinting\Contacts\ContactPersonAddressDAO;
use BlakePrinting\Contacts\ContactPersonDAO;
use BlakePrinting\DataSources\DataFeedDAO;
use BlakePrinting\DataSources\DataSourceDAO;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Globalization\CountryDAO;
use BlakePrinting\Globalization\CurrencyDAO;
use BlakePrinting\Organizations\OrganizationDAO;
use BlakePrinting\Sales\FulfillmentStatusDAO;
use BlakePrinting\Sales\Orders\SalesOrderCatalogProductDAO;
use BlakePrinting\Sales\Orders\SalesOrderShipmentDAO;
use BlakePrinting\Sales\Orders\SalesOrderShipmentVO;
use BlakePrinting\Sales\PaymentMethodDAO;
use BlakePrinting\Sales\SalesChannelDAO;
use BlakePrinting\Sales\Orders\SalesOrderContactAddressDAO;
use BlakePrinting\Sales\Orders\SalesOrderContactPersonDAO;
use BlakePrinting\Sales\Orders\SalesOrderDAO;
use BlakePrinting\Sales\Orders\SalesOrderDataFeedDAO;
use BlakePrinting\Sales\Orders\SalesOrderTaxDAO;
use BlakePrinting\Shipping\ShipmentCatalogProductDAO;
use BlakePrinting\Shipping\ShipmentCatalogProductVO;
use BlakePrinting\Shipping\ShipmentDAO;
use BlakePrinting\Shipping\ShipmentStatusDAO;
use BlakePrinting\Shipping\ShipmentStatusVO;
use BlakePrinting\Shipping\ShipmentVO;
use BlakePrinting\Taxes\TaxDAO;
use BlakePrinting\Taxes\TaxIdentifierTypeDAO;
use BlakePrinting\Taxes\TaxRateDAO;
use BlakePrinting\Taxes\TaxTypeDAO;
use BlakePrinting\Users\UserDAO;
use BlakePrinting\Users\UserRoleDAO;
use BlakePrinting\Users\UserUtil;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use PDO;
use ReflectionClass;
use Silex\ControllerCollection;
use Stringy\StaticStringy as S;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class SalesOrdersController {
    public static function connect(WebApplication $app, ControllerCollection $controllers) {
        $controllers->get("/orders/purchasers.json", self::class . "::jsonOrderPurchaserSearch")
            ->bind("sales_orders_purchasers_json")
        ;
        $controllers->get("/orders/organizations.json", self::class . "::jsonOrderPurchaserOrganizationSearch")
            ->bind("sales_order_purchasers_organizations_json")
        ;
        $controllers->get("/orders/{salesOrderId}/addresses.json", self::class . "::jsonOrderAddresses")
            ->bind("sales_orders_order_addresses_json")
        ;
        $controllers->get("/orders/{salesOrderId}/taxes.json", self::class . "::jsonOrderTaxes")
            ->bind("sales_orders_order_taxes_json")
        ;
        $controllers->get("/orders/{salesOrderId}/products.json", self::class . "::jsonOrderProducts")
            ->bind("sales_orders_order_products_json")
        ;
        $controllers->get("/orders/{salesOrderId}/shipments.json", self::class . "::jsonOrderShipments")
            ->bind("sales_orders_order_shipments_json")
        ;
        $controllers->get("/orders/{salesOrderId}/ship", self::class . "::doShipOrder")
            ->bind("sales_orders_order_ship")
        ;
        $controllers->get("/orders/_dash", self::class . "::displayDashboardSnippet")
            ->bind("sales_orders_dashboard_snippet")
        ;
        $controllers->get("/orders/{salesOrderId}", self::class . "::displayOrder")
            ->bind("sales_orders_order")
        ;
        $controllers->get("/orders.json", self::class . "::jsonOrdersSearch")
            ->bind("sales_orders_json")
        ;
        $controllers->get("/orders", self::class . "::displayOrders")
            ->bind("sales_orders")
        ;
    }

    public function jsonOrderAddresses(WebApplication $app, Request $request, $salesOrderId) {
        if($app->isGranted(UserRoleDAO::ROLE_TRIAL)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $salesOrderDao =& SalesOrderDAO::instance($connection);
        $salesOrderVo = ((Utils::isIntegerValue($salesOrderId) && $salesOrderId > 0) ? $salesOrderDao->findById($salesOrderId) : null);

        if($salesOrderVo === null) {
            $app->abort(Response::HTTP_NOT_FOUND, "Missing or invalid sales order ID");
        }

        $salesOrderContactAddressTbl = SalesOrderContactAddressDAO::instance($connection)->formatSchemaAndTableName();
        $contactAddressTbl = ContactAddressDAO::instance($connection)->formatSchemaAndTableName();
        $countryTbl = CountryDAO::instance($connection)->formatSchemaAndTableName();
        $page = AppUtil::getIntFromQuery($request, "page");
        $sortColumn = Utils::getTrimmedStringOrNull($request->query->get("sort_column"));
        $sortDirection = AppUtil::getOrderFromQuery($request, "sort_direction");
        $count = AppUtil::getIntFromQuery($request, "count");
        $select =& PDOQuery::select()
            ->calculateFoundRows()
            ->columns($contactAddressTbl . ".*")
            ->column(("COALESCE(" . $contactAddressTbl . ".updated, " . $contactAddressTbl . ".created) AS modified"))
            ->column($countryTbl . ".name AS country_name")
            ->from($salesOrderContactAddressTbl)
            ->join($contactAddressTbl, ["contact_address_id = " . $contactAddressTbl . ".id"])
            ->leftJoin($countryTbl, ["country_id = " . $countryTbl . ".id"])
            ->where("sales_order_id", $salesOrderId, PDO::PARAM_INT)
        ;

        $sortColumn = ($sortColumn === null ? null : (string)S::underscored($sortColumn));

        if(in_array($sortColumn, ["line1", "line2", "line3", "city", "district", "state_or_region", "postal_code", "country_name"])) {
            $sortColumn = "UCASE(" . $sortColumn . ")";
        } else {
            switch($sortColumn) {
                case "id":
                    $sortColumn = ($contactAddressTbl . ".id");
                    break;
                case "country":
                    $sortColumn = "UCASE(country_name)";
                    break;
                default:
                    $sortColumn = ("COALESCE(" . $contactAddressTbl . ".updated, " . $contactAddressTbl . ".created)");
                    break;
            }
        }

        $select->orderBy($sortColumn . ($sortDirection === null ? "" : (" " . $sortDirection)));

        if($count !== null && $count > 0) {
            $select->limit($count);

            if($page !== null && $page > 1) {
                $select->offset(($page - 1) * $count);
            }
        }

        $calculated = $select->getCalculatedRowCount();
        $rows = $select->getResults($connection);
        $results = [
            "total" => count($rows),
            "calculated" => $calculated,
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function jsonOrderTaxes(WebApplication $app, Request $request, $salesOrderId) {
        if($app->isGranted(UserRoleDAO::ROLE_TRIAL)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $salesOrderDao =& SalesOrderDao::instance($connection);
        $salesOrderVo = ((Utils::isIntegerValue($salesOrderId) && $salesOrderId > 0) ? $salesOrderDao->findById($salesOrderId) : null);

        if($salesOrderVo === null) {
            $app->abort(Response::HTTP_NOT_FOUND, "Missing or invalid sales order ID");
        }

        $page = AppUtil::getIntFromQuery($request, "page");
        $sortColumn = Utils::getTrimmedStringOrNull($request->query->get("sort_column"));
        $sortDirection = AppUtil::getOrderFromQuery($request, "sort_direction");
        $count = AppUtil::getIntFromQuery($request, "count");
        $salesOrderTaxTbl = SalesOrderTaxDAO::instance($connection)->formatSchemaAndTableName();
        $taxRateTbl = TaxRateDAO::instance($connection)->formatSchemaAndTableName();
        $taxTbl = TaxDAO::instance($connection)->formatSchemaAndTableName();
        $taxTypeTbl = TaxTypeDAO::instance($connection)->formatSchemaAndTableName();
        $taxIdentifierTypeTbl = TaxIdentifierTypeDAO::instance($connection)->formatSchemaAndTableName();
        $select =& PDOQuery::select()
            ->calculateFoundRows()
            ->column($taxTbl . ".id")
            ->column([($taxTypeTbl . ".name") => "type_name"])
            ->column([($taxTypeTbl . ".description") => "type_description"])
            ->column($taxTbl . ".code")
            ->column($taxTbl . ".name")
            ->column($taxTbl . ".description")
            ->column($taxRateTbl . ".percentage")
            ->column($taxRateTbl . ".variable")
            ->column($taxRateTbl . ".include_in_price")
            ->column([($taxIdentifierTypeTbl . ".name") => "tax_identifier_name"])
            ->column("tax_identifier")
            ->column("variable_percentage")
            ->column("tax_amount")
            ->from($salesOrderTaxTbl)
            ->leftJoin($taxRateTbl, [$salesOrderTaxTbl . ".tax_rate_id = " . $taxRateTbl . ".id"])
            ->leftJoin($taxTbl, [$taxRateTbl . ".tax_id = " . $taxTbl . ".id"])
            ->leftJoin($taxTypeTbl, [$taxTbl . ".tax_type_id = " . $taxTypeTbl . ".id"])
            ->leftJoin($taxIdentifierTypeTbl, [$salesOrderTaxTbl . ".tax_identifier_type_id = " . $taxIdentifierTypeTbl . ".id"])
            ->where("sales_order_id", $salesOrderId, PDO::PARAM_INT)
        ;

        $sortColumn = ($sortColumn === null ? null : (string)S::underscored($sortColumn));

        switch($sortColumn) {
            case "percentage":
            case "variable":
            case "includeInPrice":
            case "variablePercentage":
                break;
            case "typeName":
            case "taxIdentifierName":
            case "taxIdentifier":
                $sortColumn = "UCASE(" . $sortColumn . ")";
                break;
            case "id":
                $sortColumn = $taxTbl . "." . $sortColumn;
                break;
            case "code":
            case "name":
                $sortColumn = "UCASE(" . $taxTbl . "." . $sortColumn . ")";
                break;
            case "tax_amount":
            default:
                $sortColumn = "CAST(tax_amount AS DECIMAL(19,4))";
                break;
        }

        $select->orderBy($sortColumn . ($sortDirection === null ? "" : (" " . $sortDirection)));

        if($count !== null && $count > 0) {
            $select->limit($count);

            if($page !== null && $page > 1) {
                $select->offset(($page - 1) * $count);
            }
        }

        $rows =& $select->getResults($connection);
        $calculated = $select->getCalculatedRowCount();
        $results = [
            "total" => count($rows),
            "calculated" => $calculated,
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function jsonOrderProducts(WebApplication $app, Request $request, $salesOrderId) {
        if($app->isGranted(UserRoleDAO::ROLE_TRIAL)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $salesOrderDao =& SalesOrderDao::instance($connection);
        $salesOrderVo = ((Utils::isIntegerValue($salesOrderId) && $salesOrderId > 0) ? $salesOrderDao->findById($salesOrderId) : null);

        if($salesOrderVo === null) {
            $app->abort(Response::HTTP_NOT_FOUND, "Missing or invalid sales order ID");
        }

        $page = AppUtil::getIntFromQuery($request, "page");
        $sortColumn = Utils::getTrimmedStringOrNull($request->query->get("sort_column"));
        $sortDirection = AppUtil::getOrderFromQuery($request, "sort_direction");
        $count = AppUtil::getIntFromQuery($request, "count");
        $salesOrderCatalogProductTbl = SalesOrderCatalogProductDAO::instance($connection)->formatSchemaAndTableName();
        $catalogProductTbl = CatalogProductDAO::instance($connection)->formatSchemaAndTableName();
        $salesOrderShipmentTbl = SalesOrderShipmentDAO::instance($connection)->formatSchemaAndTableName();
        $shipmentCatalogProductTbl = ShipmentCatalogProductDAO::instance($connection)->formatSchemaAndTableName();
        $select =& PDOQuery::select()
            ->calculateFoundRows()
            ->column($catalogProductTbl . ".id")
            ->column($catalogProductTbl . ".name")
            ->column($salesOrderCatalogProductTbl . ".quantity")
            ->column(["SUM(COALESCE(" . $shipmentCatalogProductTbl . ".quantity, 0))" => "shipped"])
            ->from($salesOrderCatalogProductTbl)
            ->leftJoin($catalogProductTbl, [$salesOrderCatalogProductTbl . ".catalog_product_id = " . $catalogProductTbl . ".id"])
            ->leftJoin($salesOrderShipmentTbl, [$salesOrderCatalogProductTbl . ".sales_order_id = " . $salesOrderShipmentTbl . ".sales_order_id"])
            ->leftJoin($shipmentCatalogProductTbl, [
                $salesOrderShipmentTbl . ".shipment_id = " . $shipmentCatalogProductTbl . ".shipment_id",
                $salesOrderCatalogProductTbl . ".catalog_product_id = " . $shipmentCatalogProductTbl . ".catalog_product_id"
            ])
            ->where($salesOrderCatalogProductTbl . ".sales_order_id", $salesOrderId, PDO::PARAM_INT)
            ->groupBy($catalogProductTbl . ".id")
            ->groupBy($catalogProductTbl . ".name")
            ->groupBy($salesOrderCatalogProductTbl . ".quantity")
        ;

        $sortColumn = ($sortColumn === null ? null : (string)S::underscored($sortColumn));

        switch($sortColumn) {
            case "id":
                break;
            case "name":
                $sortColumn = "UCASE(" . $catalogProductTbl . "." . $sortColumn . ")";
                break;
            case "quantity":
            default:
                $sortColumn = "quantity";
                break;
        }

        $select->orderBy($sortColumn . ($sortDirection === null ? "" : (" " . $sortDirection)));

        if($count !== null && $count > 0) {
            $select->limit($count);

            if($page !== null && $page > 1) {
                $select->offset(($page - 1) * $count);
            }
        }

        $rows =& $select->getResults($connection);
        $calculated = $select->getCalculatedRowCount();
        $results = [
            "total" => count($rows),
            "calculated" => $calculated,
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function jsonOrderShipments(WebApplication $app, Request $request, $salesOrderId) {
        if($app->isGranted(UserRoleDAO::ROLE_TRIAL)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $salesOrderDao =& SalesOrderDAO::instance($connection);
        $salesOrderVo = ((Utils::isIntegerValue($salesOrderId) && $salesOrderId > 0) ? $salesOrderDao->findById($salesOrderId) : null);

        if($salesOrderVo === null) {
            $app->abort(Response::HTTP_NOT_FOUND, "Missing or invalid sales order ID");
        }

        $salesOrderShipmentTbl = SalesOrderShipmentDAO::instance($connection)->formatSchemaAndTableName();
        $shipmentTbl = ShipmentDAO::instance($connection)->formatSchemaAndTableName();
        $shipmentStatusTbl = ShipmentStatusDAO::instance($connection)->formatSchemaAndTableName();
        $shipmentCatalogProductTbl = ShipmentCatalogProductDAO::instance($connection)->formatSchemaAndTableName();
        $organizationTbl = OrganizationDAO::instance($connection)->formatSchemaAndTableName();
        $contactPersonTbl = ContactPersonDAO::instance($connection)->formatSchemaAndTableName();
        $page = AppUtil::getIntFromQuery($request, "page");
        $sortColumn = Utils::getTrimmedStringOrNull($request->query->get("sort_column"));
        $sortDirection = AppUtil::getOrderFromQuery($request, "sort_direction");
        $count = AppUtil::getIntFromQuery($request, "count");
        $select =& PDOQuery::select()
            ->calculateFoundRows()
            ->column($shipmentTbl . ".id")
            ->column([$shipmentStatusTbl . ".name" => "status"])
            ->column(["sender_org.name" => "sender"])
            ->column(["recipient_org.name" => "recipient_organization"])
            ->column([$contactPersonTbl . ".name" => "recipient_contact_person"])
            ->column(["SUM(COALESCE(" . $shipmentCatalogProductTbl . ".quantity, 0))" => "product_count"])
            ->from($salesOrderShipmentTbl)
            ->join($shipmentTbl, [$salesOrderShipmentTbl . ".shipment_id = " . $shipmentTbl . ".id"])
            ->join($shipmentStatusTbl, [$shipmentTbl . ".shipment_status_id = " . $shipmentStatusTbl . ".id"])
            ->leftJoin($organizationTbl . " sender_org", [$shipmentTbl . ".sender_organization_id = sender_org.id"])
            ->leftJoin($organizationTbl . " recipient_org", [$shipmentTbl . ".recipient_organization_id = recipient_org.id"])
            ->leftJoin($contactPersonTbl, [$shipmentTbl . ".contact_person_id = " . $contactPersonTbl . ".id"])
            ->leftJoin($shipmentCatalogProductTbl, [
                $shipmentTbl . ".id = " . $shipmentCatalogProductTbl . ".shipment_id"
            ])
            ->where($salesOrderShipmentTbl . ".sales_order_id", $salesOrderId, PDO::PARAM_INT)
            ->groupBy($shipmentTbl . ".id")
            ->groupBy("status")
            ->groupBy("sender")
            ->groupBy("recipient_organization")
            ->groupBy("recipient_contact_person")
        ;

        $sortColumn = ($sortColumn === null ? null : (string)S::underscored($sortColumn));

        if(in_array($sortColumn, ["status", "sender", "recipient_organization", "recipient_contact_person"])) {
            $sortColumn = "UCASE(" . $sortColumn . ")";
        } else {
            switch($sortColumn) {
                case "product_count":
                    break;
                case "id":
                default:
                    $sortColumn = ($shipmentTbl . ".id");
                    break;
            }
        }

        $select->orderBy($sortColumn . ($sortDirection === null ? "" : (" " . $sortDirection)));

        if($count !== null && $count > 0) {
            $select->limit($count);

            if($page !== null && $page > 1) {
                $select->offset(($page - 1) * $count);
            }
        }

        $calculated = $select->getCalculatedRowCount();
        $rows = $select->getResults($connection);
        $results = [
            "total" => count($rows),
            "calculated" => $calculated,
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function doShipOrder(WebApplication $app, $salesOrderId) {
        if($app->isGranted(UserRoleDAO::ROLE_TRIAL)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $salesOrderDao =& SalesOrderDAO::instance($connection);
        $salesOrderVo = ((Utils::isIntegerValue($salesOrderId) && $salesOrderId > 0) ? $salesOrderDao->findById($salesOrderId) : null);

        if($salesOrderVo === null) {
            $app->abort(Response::HTTP_NOT_FOUND, "Missing or invalid sales order ID");
        }

        $shipmentDao =& ShipmentDAO::instance($connection);
        $shipmentCatalogProductDao =& ShipmentCatalogProductDAO::instance($connection);
        $salesOrderShipmentDao =& SalesOrderShipmentDAO::instance($connection);
        $salesOrderCatalogProductDao =& SalesOrderCatalogProductDAO::instance($connection);
        $shippingAddressTypeVo =& ContactAddressTypeDAO::instance($connection)->findByColumn(
            "name", ContactAddressTypeDAO::ADDRESS_TYPE_SHIPPING, PDO::PARAM_STR, null, null, null, true
        );
        $contactAddressDao =& ContactAddressDAO::instance($connection);
        $contactAddressTbl = $contactAddressDao->formatSchemaAndTableName();
        $shippingAddressVo =& $contactAddressDao->findSingle(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$connection, &$salesOrderId, &$contactAddressTbl, &$shippingAddressTypeVo) {
                $salesOrderContactAddressTbl = SalesOrderContactAddressDAO::instance($connection)->formatSchemaAndTableName();
                $contactAddressAddressTypeTbl = ContactAddressAddressTypeDAO::instance($connection)->formatSchemaAndTableName();

                $pdoQuery
                    ->join($salesOrderContactAddressTbl, [$contactAddressTbl . ".id = " . $salesOrderContactAddressTbl . ".contact_address_id"])
                    ->join($contactAddressAddressTypeTbl, [
                        $contactAddressAddressTypeTbl . ".address_id = " . $contactAddressTbl . ".id",
                        $contactAddressAddressTypeTbl . ".address_type_id = " . $shippingAddressTypeVo->getId()
                    ])
                    ->where($salesOrderContactAddressTbl . ".sales_order_id", $salesOrderId, PDO::PARAM_INT)
                ;
            }
        );
        $rows = [];

        if($shippingAddressVo !== null) {
            $shipmentStatusDao =& ShipmentStatusDAO::instance($connection);
            $shippedProducts =& $shipmentCatalogProductDao->findAll(
                function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$connection, &$salesOrderId, &$shipmentDao, &$shipmentStatusDao, &$shipmentCatalogProductDao, &$salesOrderShipmentDao) {
                    $shipmentCatalogProductTbl = $shipmentCatalogProductDao->formatSchemaAndTableName();
                    $salesOrderShipmentTbl = $salesOrderShipmentDao->formatSchemaAndTableName();
                    $shipmentTbl = $shipmentDao->formatSchemaAndTableName();
                    $shipmentStatusTbl = $shipmentStatusDao->formatSchemaAndTableName();

                    $pdoQuery
                        ->columns($shipmentCatalogProductTbl . ".*")
                        ->join($shipmentTbl, [$shipmentCatalogProductTbl . ".shipment_id = " . $shipmentTbl . ".id"])
                        ->join($shipmentStatusTbl, [$shipmentTbl . ".shipment_status_id = " . $shipmentStatusTbl . ".id"])
                        ->join($salesOrderShipmentTbl, [$shipmentTbl . ".id = " . $salesOrderShipmentTbl . ".shipment_id"])
                        ->where($salesOrderShipmentTbl . ".sales_order_id", $salesOrderId, PDO::PARAM_INT)
                        ->where($shipmentStatusTbl . ".name", ShipmentStatusDAO::SHIPMENT_STATUS_CANCELLED, PDO::PARAM_STR, "<>")
                    ;
                }, null, null, "FOR UPDATE"
            );
            $orderProducts =& $salesOrderCatalogProductDao->findAllByColumn(
                "sales_order_id", $salesOrderId, PDO::PARAM_INT, null, null, "FOR UPDATE"
            );
            $toBeShipped = [];

            if($orderProducts !== null) {
                foreach($orderProducts as &$orderProductVo) {
                    $orderedProductId = $orderProductVo->getCatalogProductId();
                    $orderedQuantity = $orderProductVo->getQuantity();

                    if(array_key_exists($orderedProductId, $toBeShipped)) {
                        $orderedQuantity += $toBeShipped[$orderedProductId];
                    }

                    $toBeShipped[$orderedProductId] = $orderedQuantity;
                }

                unset($orderProductVo);
            }

            if($shippedProducts !== null) {
                foreach($shippedProducts as &$shippedProductVo) {
                    $shippedProductId = $shippedProductVo->getCatalogProductId();

                    if(array_key_exists($shippedProductId, $toBeShipped)) {
                        $toBeShippedQuantity = $toBeShipped[$shippedProductId] - $shippedProductVo->getQuantity();

                        if($toBeShippedQuantity < 1) {
                            unset($toBeShipped[$shippedProductId]);
                        } else {
                            $toBeShipped[$shippedProductId] = $toBeShippedQuantity;
                        }
                    }
                }
            }

            if(Utils::isNonEmptyArray($toBeShipped)) {
                $contactPersonDao =& ContactPersonDAO::instance($connection);
                $contactPersonVo =& $contactPersonDao->findSingle(
                    function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$connection, &$contactPersonDao, &$salesOrderDao, &$salesOrderId, &$shippingAddressVo) {
                        $contactPersonTbl = $contactPersonDao->formatSchemaAndTableName();
                        $salesOrderTbl = $salesOrderDao->formatSchemaAndTableName();
                        $salesOrderContactPersonTbl = SalesOrderContactPersonDAO::instance($connection)->formatSchemaAndTableName();
                        $salesOrderContactAddressTbl = SalesOrderContactAddressDAO::instance($connection)->formatSchemaAndTableName();
                        $contactPersonAddressTbl = ContactPersonAddressDAO::instance($connection)->formatSchemaAndTableName();

                        $pdoQuery
                            ->columns($contactPersonTbl . ".*")
                            ->join($salesOrderContactPersonTbl, [$contactPersonTbl . ".id = " . $salesOrderContactPersonTbl . ".contact_person_id"])
                            ->join($salesOrderTbl, [$salesOrderContactPersonTbl . ".sales_order_id = " . $salesOrderTbl . ".id"])
                            ->join($salesOrderContactAddressTbl, [$salesOrderTbl . ".id = " . $salesOrderContactAddressTbl . ".sales_order_id"])
                            ->join($contactPersonAddressTbl, [
                                $contactPersonAddressTbl . ".address_id = " . $salesOrderContactAddressTbl . ".contact_address_id",
                                $contactPersonAddressTbl . ".person_id = " . $salesOrderContactPersonTbl . ".contact_person_id"
                            ])
                            ->where($salesOrderTbl . ".id", $salesOrderId, PDO::PARAM_INT)
                            ->where($salesOrderContactAddressTbl . ".contact_address_id", $shippingAddressVo->getId(), PDO::PARAM_INT)
                        ;
                    }
                );
                $pendingStatusVo =& $shipmentStatusDao->findOrCreate(
                    new ShipmentStatusVO(ShipmentStatusDAO::SHIPMENT_STATUS_PENDING)
                );
                $shipmentVo = new ShipmentVO();

                $shipmentVo->setSenderOrganizationId($salesOrderVo->getSellerOrganizationId());
                $shipmentVo->setRecipientOrganizationId($salesOrderVo->getPurchaserOrganizationId());
                $shipmentVo->setContactAddressId($shippingAddressVo->getId());
                $shipmentVo->setShipmentStatusId($pendingStatusVo->getId());

                if($contactPersonVo !== null) {
                    $shipmentVo->setContactPersonId($contactPersonVo->getId());
                }

                $shipmentId =& $shipmentDao->insert($shipmentVo);
                $shipmentVo =& $shipmentDao->findById($shipmentId);

                foreach($toBeShipped as $productId => &$quantity) {
                    $shipmentCatalogProductDao->insert(
                        new ShipmentCatalogProductVO($shipmentId, $productId, $quantity)
                    );
                }

                unset($quantity);

                $salesOrderShipmentDao->insert(
                    new SalesOrderShipmentVO($salesOrderId, $shipmentId)
                );

                $rows[] = $shipmentVo;
            }
        }

        $results = [
            "inserted" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function displayDashboardSnippet(WebApplication $app, Request $request) {
        $results = [];

        if(!$app->isGranted(UserRoleDAO::ROLE_TRIAL)) {
            $connection =& $app["merchant.mysql.connection"];
            $salesOrderDao =& SalesOrderDAO::instance($connection);
            $fulfillmentStatusDao =& FulfillmentStatusDAO::instance($connection);
            $orderStatusCounts =& PDOQuery::select()
                ->column("name")
                ->column(["COUNT(*)" => "cnt"])
                ->from($fulfillmentStatusDao)
                ->leftJoin($salesOrderDao, [
                    $fulfillmentStatusDao->formatSchemaAndTableName() . ".id = " . $salesOrderDao->formatSchemaAndTableName() . ".fulfillment_status_id"
                ])
                ->groupBy("name")
                ->getResults($connection)
            ;
            $templateVars = [];
            $severity = 1;

            Utils::forEachConstantInObjectClass(
                $fulfillmentStatusDao,
                function($_, $constantName, $constantValue) use (&$templateVars, &$orderStatusCounts, &$severity) {
                    $logger =& LogUtil::getLogger();
                    $count = 0;

                    foreach($orderStatusCounts as &$orderStatusCount) {
                        $statusName = $orderStatusCount["name"];

                        if($statusName !== $constantValue) {
                            continue;
                        }

                        $count = $orderStatusCount["cnt"];
                    }

                    unset($orderStatusCount);

                    $templateVars[$constantValue . "_count"] = $count;
                    $logger->debug($constantValue . " => " . $count);

                    if($constantValue === "pending" && $count > 0) {
                        $severity = 3;
                    } else if($severity === 1 && $constantValue === "partial" && $count > 0) {
                        $severity = 2;
                    }
                },
                "FULFILLMENT_STATUS_"
            );

            $templateVars["severity"] = ($severity === 3 ? "danger" : ($severity === 2 ? "warning" : "default"));
            $results["html"] = $app["twig"]->render("sales/sales_orders_dash_snippet.html.twig", $templateVars);
        }

        return $app->json($results);
    }

    public function displayOrder(WebApplication $app, $salesOrderId) {
        if($app->isGranted(UserRoleDAO::ROLE_TRIAL)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $salesOrderDao =& SalesOrderDAO::instance($connection);
        $salesOrderVo = ((Utils::isIntegerValue($salesOrderId) && $salesOrderId > 0) ? $salesOrderDao->findById($salesOrderId) : null);

        if($salesOrderVo === null) {
            $app->abort(Response::HTTP_NOT_FOUND, "Missing or invalid sales order ID");
        }

        $userDao =& UserDAO::instance($connection);
        $organizationDao =& OrganizationDAO::instance($connection);
        $salesChannelDao =& SalesChannelDAO::instance($connection);
        $currencyDao =& CurrencyDAO::instance($connection);
        $paymentMethodDao =& PaymentMethodDAO::instance($connection);
        $fulfillmentStatusDao =& FulfillmentStatusDAO::instance($connection);
        $contactPersonDao =& ContactPersonDAO::instance($connection);
        $dataFeedDao =& DataFeedDAO::instance($connection);
        $creatorVo = ($salesOrderVo->getCreator() === null ? null : $userDao->findById($salesOrderVo->getCreator()));
        $updaterVo = ($salesOrderVo->getUpdater() === null ? null : $userDao->findById($salesOrderVo->getUpdater()));
        $sellerOrganizationVo = ($salesOrderVo->getSellerOrganizationId() === null ? null : $organizationDao->findById($salesOrderVo->getSellerOrganizationId()));
        $purchaserOrganizationVo = ($salesOrderVo->getPurchaserOrganizationId() === null ? null : $organizationDao->findById($salesOrderVo->getPurchaserOrganizationId()));
        $fulfillmentStatusVo = ($salesOrderVo->getFulfillmentStatusId() === null ? null : $fulfillmentStatusDao->findById($salesOrderVo->getFulfillmentStatusId()));
        $salesChannelVo = ($salesOrderVo->getSalesChannelId() === null ? null : $salesChannelDao->findById($salesOrderVo->getSalesChannelId()));
        $purchaserCurrencyVo = ($salesOrderVo->getPurchaserCurrencyId() === null ? null : $currencyDao->findById($salesOrderVo->getPurchaserCurrencyId()));
        $saleCurrencyVo = ($salesOrderVo->getSaleCurrencyId() === null ? null : $currencyDao->findById($salesOrderVo->getSaleCurrencyId()));
        $paymentMethodVo = ($salesOrderVo->getPaymentMethodId() === null ? null : $paymentMethodDao->findById($salesOrderVo->getPaymentMethodId()));
        $contactPersons =& $contactPersonDao->findAll(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$salesOrderDao, &$contactPersonDao, &$salesOrderId, &$connection) {
                $salesOrderContactPersonTbl = SalesOrderContactPersonDAO::instance($connection)->formatSchemaAndTableName();
                $contactPersonTbl = $contactPersonDao->formatSchemaAndTableName();

                $pdoQuery
                    ->join($salesOrderContactPersonTbl, [$contactPersonTbl . ".id = " . $salesOrderContactPersonTbl . ".contact_person_id"])
                    ->where("sales_order_id", $salesOrderId, PDO::PARAM_INT)
                ;
            }
        );
        $dataFeeds =& $dataFeedDao->findAll(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$salesOrderDao, &$dataFeedDao, &$salesOrderId, &$connection) {
                $dataFeedTbl = $dataFeedDao->formatSchemaAndTableName();
                $salesOrderDataFeedTbl = SalesOrderDataFeedDAO::instance($connection)->formatSchemaAndTableName();

                $pdoQuery
                    ->join($salesOrderDataFeedTbl, [$dataFeedTbl . ".id = " . $salesOrderDataFeedTbl . ".data_feed_id"])
                    ->where("sales_order_id", $salesOrderId, PDO::PARAM_INT)
                ;
            }
        );

        return $app["twig"]->render("sales/sales_order.html.twig", [
            "nav_section" => "sales",
            "sales_order" => $salesOrderVo,
            "seller_organization" => $sellerOrganizationVo,
            "purchaser_organization" => $purchaserOrganizationVo,
            "fulfillment_status" => $fulfillmentStatusVo,
            "sales_channel" => $salesChannelVo,
            "purchaser_currency" => $purchaserCurrencyVo,
            "sale_currency" => $saleCurrencyVo,
            "payment_method" => $paymentMethodVo,
            "contact_persons" => $contactPersons,
            "data_feeds" => $dataFeeds,
            "data_sources" => DataSourceDAO::instance($connection)->findAll(null, "UCASE(name)"),
            "creator" => $creatorVo,
            "updater" => $updaterVo
        ]);
    }

    public function jsonOrdersSearch(WebApplication $app, Request $request) {
        if($app->isGranted(UserRoleDAO::ROLE_TRIAL)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $salesOrderTbl = SalesOrderDAO::instance($connection)->formatSchemaAndTableName();
        $fulfillmentStatusTbl = FulfillmentStatusDAO::instance($connection)->formatSchemaAndTableName();
        $salesChannelTbl = SalesChannelDAO::instance($connection)->formatSchemaAndTableName();
        $countryTbl = CountryDAO::instance($connection)->formatSchemaAndTableName();
        $organizationTbl = OrganizationDAO::instance($connection)->formatSchemaAndTableName();
        $currencyTbl = CurrencyDAO::instance($connection)->formatSchemaAndTableName();
        $salesOrderContactAddressTbl = SalesOrderContactAddressDAO::instance($connection)->formatSchemaAndTableName();
        $contactAddressTbl = ContactAddressDAO::instance($connection)->formatSchemaAndTableName();
        $salesOrderContactPersonTbl = SalesOrderContactPersonDAO::instance($connection)->formatSchemaAndTableName();
        $contactPersonTbl = ContactPersonDAO::instance($connection)->formatSchemaAndTableName();
        $salesOrderTaxTbl = SalesOrderTaxDAO::instance($connection)->formatSchemaAndTableName();
        $taxRateTbl = TaxRateDAO::instance($connection)->formatSchemaAndTableName();
        $taxTbl = TaxDAO::instance($connection)->formatSchemaAndTableName();
        $search =& $this->generateOrdersSearch($app, $request)
            ->calculateFoundRows()
            ->distinct()
            ->column($salesOrderTbl . ".id")
        ;
        $salesOrderIds =& $search->getColumnValues($connection);
        $rows = [];

        if(!empty($salesOrderIds)) {
            foreach($salesOrderIds as &$salesOrderId) {
                $row = PDOQuery::select()
                    ->column($salesOrderTbl . ".id")
                    ->column($salesOrderTbl . ".created")
                    ->column($salesOrderTbl . ".updated")
                    ->column($salesOrderTbl . ".ordered")
                    ->column($salesOrderTbl . ".total_amount")
                    ->column([($fulfillmentStatusTbl . ".name") => "fulfillment_status_name"])
                    ->column([($salesChannelTbl . ".name") => "sales_channel"])
                    ->column(["seller_organization.name" => "seller_organization_name"])
                    ->column(["purchaser_organization.name" => "purchaser_organization_name"])
                    ->column(["sale_currency.iso_code" => "sale_currency_code"])
                    ->from($salesOrderTbl)
                    ->leftJoin($fulfillmentStatusTbl, [$salesOrderTbl . ".fulfillment_status_id = " . $fulfillmentStatusTbl . ".id"])
                    ->leftJoin($salesChannelTbl, [$salesOrderTbl . ".sales_channel_id = " . $salesChannelTbl . ".id"])
                    ->leftJoin($organizationTbl . " AS seller_organization", [$salesOrderTbl . ".seller_organization_id = seller_organization.id"])
                    ->leftJoin($organizationTbl . " AS purchaser_organization", [$salesOrderTbl . ".purchaser_organization_id = purchaser_organization.id"])
                    ->leftJoin($currencyTbl . " AS sale_currency", [$salesOrderTbl . ".sale_currency_id = sale_currency.id"])
                    ->where($salesOrderTbl . ".id", $salesOrderId, PDO::PARAM_INT)
                    ->getSingleRow($connection)
                ;
                $row["countries"] = PDOQuery::select()
                    ->column("iso_code")
                    ->from($salesOrderContactAddressTbl)
                    ->join($contactAddressTbl, [$salesOrderContactAddressTbl . ".contact_address_id = " . $contactAddressTbl . ".id"])
                    ->join($countryTbl, [$contactAddressTbl . ".country_id = " . $countryTbl . ".id"])
                    ->where("sales_order_id", $salesOrderId, PDO::PARAM_INT)
                    ->getColumnValues($connection)
                ;
                $row["purchasers"] = PDOQuery::select()
                    ->columns("id", "name")
                    ->from($salesOrderContactPersonTbl)
                    ->join($contactPersonTbl, [$salesOrderContactPersonTbl . ".contact_person_id = " . $contactPersonTbl . ".id"])
                    ->where("sales_order_id", $salesOrderId, PDO::PARAM_INT)
                    ->getResults($connection)
                ;
                $row["taxes"] = PDOQuery::select()
                    ->column("tax_amount")
                    ->column(["COALESCE(COALESCE(" . $taxTbl . ".code, " . $taxTbl . ".name), " . $taxTbl . ".description)" => "tax_desc"])
                    ->from($salesOrderTaxTbl)
                    ->leftJoin($taxRateTbl, [$salesOrderTaxTbl . ".tax_rate_id = " . $taxRateTbl . ".id"])
                    ->leftJoin($taxTbl, [$taxRateTbl . ".tax_id = " . $taxTbl . ".id"])
                    ->where("sales_order_id", $salesOrderId, PDO::PARAM_INT)
                    ->getResults($connection)
                ;

                $rows[] = $row;
            }

            unset($salesOrderId);
        }

        $results = [
            "total" => count($rows),
            "calculated" => $search->getCalculatedRowCount(),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function jsonOrderPurchaserSearch(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_CATALOG)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $select =& $this->generateOrdersSearch($app, $request, ["contact_person_name"], 10)
            ->distinct()
        ;
        $rows =& $select->getColumnValues($connection);
        $results = [
            "total" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function jsonOrderPurchaserOrganizationSearch(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_CATALOG)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $select =& $this->generateOrdersSearch($app, $request, ["purchaser_organization_name"], 10)
            ->distinct()
        ;
        $rows =& $select->getColumnValues($connection);
        $results = [
            "total" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    /**
     * @param WebApplication $app
     * @param Request $request
     * @param string[] $columns
     * @param int $count
     * @return PDOQuery
     */
    private function &generateOrdersSearch(WebApplication $app, Request $request, array $columns = [], $count = null) {
        $connection =& $app["merchant.mysql.connection"];
        $sortColumn = (Utils::getTrimmedStringOrNull($request->query->get("sort_column")) ?: "id");
        $sortDirection = AppUtil::getOrderFromQuery($request, "sort_direction", "ASC");
        $count = ($count === null ? AppUtil::getIntFromQuery($request, "count", 10) : $count);
        $page = AppUtil::getIntFromQuery($request, "page", 1);
        $dataSourceId = Utils::getTrimmedStringOrNull($request->query->get("data_source_id"));
        $fulfillmentStatusId = AppUtil::getIntFromQuery($request, "fulfillment_status_id");
        $salesChannelId = AppUtil::getIntFromQuery($request, "sales_channel_id");
        $sellerOrganizationId = AppUtil::getIntFromQuery($request, "seller_organization_id");
        $purchaserOrganizationName = Utils::getTrimmedStringOrNull($request->query->get("purchaser_organization_name"));
        $name = Utils::getTrimmedStringOrNull($request->query->get("name"));
        $countryId = AppUtil::getIntFromQuery($request, "country_id");
        $salesOrderTbl = SalesOrderDAO::instance($connection)->formatSchemaAndTableName();
        $fulfillmentStatusTbl = FulfillmentStatusDAO::instance($connection)->formatSchemaAndTableName();
        $salesChannelTbl = SalesChannelDAO::instance($connection)->formatSchemaAndTableName();
        $contactPersonTbl = ContactPersonDAO::instance($connection)->formatSchemaAndTableName();
        $contactAddressTbl = ContactAddressDAO::instance($connection)->formatSchemaAndTableName();
        $organizationTbl = OrganizationDAO::instance($connection)->formatSchemaAndTableName();
        $countryTbl = CountryDAO::instance($connection)->formatSchemaAndTableName();
        $currencyTbl = CurrencyDAO::instance($connection)->formatSchemaAndTableName();
        $salesOrderContactPersonTbl = SalesOrderContactPersonDAO::instance($connection)->formatSchemaAndTableName();
        $salesOrderContactAddressTbl = SalesOrderContactAddressDAO::instance($connection)->formatSchemaAndTableName();
        $select =& PDOQuery::select()
            ->from($salesOrderTbl)
        ;

        if($dataSourceId !== null) {
            $salesOrderDataFeedTbl = SalesOrderDataFeedDAO::instance($connection)->formatSchemaAndTableName();

            if($dataSourceId === "manual") {
                $select
                    ->where("NOT EXISTS (" .
                        "SELECT 1 " .
                        "FROM " . $salesOrderDataFeedTbl . " " .
                        "WHERE " . $salesOrderDataFeedTbl . ".sales_order_id = " . $salesOrderTbl . ".id" .
                    ")")
                ;
            } else if(Utils::isIntegerValue($dataSourceId)) {
                $dataFeedTbl = DataFeedDAO::instance($connection)->formatSchemaAndTableName();
                $dataSourceTbl = DataSourceDAO::instance($connection)->formatSchemaAndTableName();

                $select
                    ->join($salesOrderDataFeedTbl, [$salesOrderTbl . ".id = " . $salesOrderDataFeedTbl . ".sales_order_id"])
                    ->join($dataFeedTbl, [$salesOrderDataFeedTbl . ".data_feed_id = " . $dataFeedTbl . ".id"])
                    ->join($dataSourceTbl, [$dataFeedTbl . ".data_source_id = " . $dataSourceTbl . ".id"])
                    ->where($dataSourceTbl . ".id", $dataSourceId, PDO::PARAM_INT)
                ;
            }
        }

        if($fulfillmentStatusId !== null) {
            $select->where("fulfillment_status_id", $fulfillmentStatusId, PDO::PARAM_INT);
        }

        if($salesChannelId !== null) {
            $select->where("sales_channel_id", $salesChannelId, PDO::PARAM_INT);
        }

        if($sortColumn === "fulfillmentStatus") {
            $select->leftJoin($fulfillmentStatusTbl, [$salesOrderTbl . ".fulfillment_status_id = " . $fulfillmentStatusTbl . ".id"]);
        }

        if($sortColumn === "channel") {
            $select->leftJoin($salesChannelTbl, [$salesOrderTbl . ".sales_channel_id = " . $salesChannelTbl . ".id"]);
        }

        if($sellerOrganizationId !== null) {
            $select
                ->join($organizationTbl . " AS seller_organization", [$salesOrderTbl . ".seller_organization_id = seller_organization.id"])
                ->where("seller_organization_id", $sellerOrganizationId, PDO::PARAM_INT)
            ;
        } else if($sortColumn === "sellerOrganization") {
            $select->leftJoin($organizationTbl . " AS seller_organization", [$salesOrderTbl . ".seller_organization_id = seller_organization.id"]);
        }

        $includePurchaserOrganizationName = in_array("purchaser_organization_name", $columns);

        if($includePurchaserOrganizationName || $purchaserOrganizationName !== null) {
            $select->join($organizationTbl . " AS purchaser_organization", [$salesOrderTbl . ".purchaser_organization_id = purchaser_organization.id"]);

            if($purchaserOrganizationName !== null) {
                $select->where("UCASE(purchaser_organization.name) LIKE UCASE(" . $connection->quote("%" . $purchaserOrganizationName . "%") . ")");
            }

            if($includePurchaserOrganizationName) {
                $select->column(["purchaser_organization.name" => "purchaser_organization_name"]);
            }
        } else if($sortColumn === "purchaserOrganization") {
            $select->leftJoin($organizationTbl . " AS purchaser_organization", [$salesOrderTbl . ".purchaser_organization_id = purchaser_organization.id"]);
        }

        $includeContactName = in_array("contact_person_name", $columns);

        if($includeContactName || $name !== null) {
            $select
                ->join($salesOrderContactPersonTbl, [$salesOrderTbl . ".id = " . $salesOrderContactPersonTbl . ".sales_order_id"])
                ->join($contactPersonTbl, [$salesOrderContactPersonTbl . ".contact_person_id = " . $contactPersonTbl . ".id"])
            ;

            if($name !== null) {
                $select->where("UCASE(" . $contactPersonTbl . ".name) LIKE UCASE(" . $connection->quote("%" . $name . "%") . ")");
            }

            if($includeContactName) {
                $select->column([($contactPersonTbl . ".name") => "contact_person_name"]);
            }
        }

        if($countryId !== null) {
            $select
                ->join($salesOrderContactAddressTbl, [$salesOrderTbl . ".id = " . $salesOrderContactAddressTbl . ".sales_order_id"])
                ->join($contactAddressTbl, [$salesOrderContactAddressTbl . ".contact_address_id = " . $contactAddressTbl . ".id"])
                ->join($countryTbl, [$contactAddressTbl . ".country_id = " . $countryTbl . ".id"])
                ->where($countryTbl . ".id", $countryId, PDO::PARAM_INT)
            ;
        }

        if($sortColumn === "currency") {
            $select->leftJoin($currencyTbl, [$salesOrderTbl . ".sale_currency_id = " . $currencyTbl . ".id"]);
        }

        $orderBy = null;

        switch($sortColumn) {
            case "id":
            case "ordered":
                $orderBy = $salesOrderTbl . "." . $sortColumn;
                break;
            case "modified":
                $orderBy = "COALESCE(" . $salesOrderTbl . ".updated, " . $salesOrderTbl . ".created)";
                break;
            case "fulfillmentStatus":
                $orderBy = "UCASE(" . $fulfillmentStatusTbl . ".name)";
                break;
            case "channel":
                $orderBy = "UCASE(" . $salesChannelTbl . ".name)";
                break;
            case "sellerOrganization":
                $orderBy = "UCASE(seller_organization.name)";
                break;
            case "purchaserOrganization":
                $orderBy = "UCASE(purchaser_organization.name)";
                break;
            case "currency":
                $orderBy = "UCASE(" . $currencyTbl . ".iso_code)";
                break;
            case "amount":
                $orderBy = "CAST(" . $salesOrderTbl . ".total_amount AS DECIMAL(19,4))";
                break;
        }

        if($orderBy !== null) {
            $select->orderBy($orderBy . ($sortDirection === null ? "" : (" " . $sortDirection)));
        }

        if($count !== null && $count > 0) {
            $select->limit($count);

            if($page !== null && $page > 1) {
                $select->offset(($page - 1) * $count);
            }
        }

        return $select;
    }

    public function displayOrders(WebApplication $app) {
        if($app->isGranted(UserRoleDAO::ROLE_TRIAL)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
        $countryDao =& CountryDAO::instance($connection);
        $countries =& $countryDao->findAll(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$countryDao, &$connection) {
                $salesOrderContactAddressTbl = SalesOrderContactAddressDAO::instance($connection)->formatSchemaAndTableName();
                $contactAddressTbl = ContactAddressDAO::instance($connection)->formatSchemaAndTableName();
                $countryTbl = $countryDao->formatSchemaAndTableName();

                $pdoQuery
                    ->distinct()
                    ->columns($countryTbl . ".*")
                    ->join($contactAddressTbl, [$countryTbl . ".id = " . $contactAddressTbl . ".country_id"])
                    ->join($salesOrderContactAddressTbl, [$contactAddressTbl . ".id = " . $salesOrderContactAddressTbl . ".contact_address_id"])
                ;
            }, "UCASE(name) ASC"
        );
        $sellerOrganizations =& UserUtil::getUserOrganizations($userId, $connection, "UCASE(name) ASC");

        return $app["twig"]->render("sales/sales_orders.html.twig", [
            "nav_section" => "sales",
            "side_section" => "orders",
            "data_sources" => DataSourceDAO::instance($connection)->findAll(null, "UCASE(name) ASC"),
            "sales_channels" => SalesChannelDAO::instance($connection)->findAll(null, "UCASE(name) ASC"),
            "fulfillment_statuses" => FulfillmentStatusDAO::instance($connection)->findAll(null, "UCASE(name) ASC"),
            "seller_organizations" => $sellerOrganizations,
            "countries" => $countries
        ]);
    }
}
