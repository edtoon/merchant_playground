<?php

namespace BlakePrinting\Admin\Catalog;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static CatalogSelectionPartDAO instance(PDO $connection = null, $schemaName = null)
 * @method CatalogSelectionPartVO findById($id, $options = null, $useCache = false)
 * @method CatalogSelectionPartVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogSelectionPartVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogSelectionPartVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method CatalogSelectionPartVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogSelectionPartVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogSelectionPartVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class CatalogSelectionPartDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $catalogSelectionSessionId
     * @param int $partId
     * @param int $quantity
     * @return int
     */
    public function &store($catalogSelectionSessionId, $partId, $quantity) {
        return PDOQuery::insert()
            ->into($this)
            ->value("catalog_selection_session_id", $catalogSelectionSessionId, PDO::PARAM_INT)
            ->value("part_id", $partId, PDO::PARAM_INT)
            ->value("quantity", $quantity, PDO::PARAM_INT)
            ->onDuplicateValue("quantity", "VALUES(quantity)")
            ->executeGetRowCount($this->getConnection())
        ;
    }

    /**
     * @param int $catalogSelectionSessionId
     * @param int $partId
     * @return int
     */
    public function &delete($catalogSelectionSessionId, $partId) {
        return PDOQuery::delete()
            ->from($this)
            ->where("catalog_selection_session_id", $catalogSelectionSessionId, PDO::PARAM_INT)
            ->where("part_id", $partId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
