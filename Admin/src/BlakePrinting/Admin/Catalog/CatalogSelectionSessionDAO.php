<?php

namespace BlakePrinting\Admin\Catalog;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static CatalogSelectionSessionDAO instance(PDO $connection = null, $schemaName = null)
 * @method CatalogSelectionSessionVO findById($id, $options = null, $useCache = false)
 * @method CatalogSelectionSessionVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogSelectionSessionVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogSelectionSessionVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method CatalogSelectionSessionVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogSelectionSessionVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogSelectionSessionVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method CatalogSelectionSessionVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class CatalogSelectionSessionDAO extends AbstractDataAccessObject {
    /**
     * @param int $id
     * @return int
     */
    public function &delete($id) {
        return PDOQuery::delete()
            ->from($this)
            ->where("id", $id, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
