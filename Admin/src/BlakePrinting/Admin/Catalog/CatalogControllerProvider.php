<?php

namespace BlakePrinting\Admin\Catalog;

use BlakePrinting\Catalog\CatalogPartDAO;
use BlakePrinting\Catalog\CatalogPartIdentifierDAO;
use BlakePrinting\Catalog\CatalogProductDAO;
use BlakePrinting\Catalog\CatalogProductIdentifierDAO;
use BlakePrinting\Catalog\CatalogProductPartDAO;
use BlakePrinting\Admin\AppUtil;
use BlakePrinting\DataSources\DataSourceDAO;
use BlakePrinting\DataSources\DataSourceIdentifierDAO;
use BlakePrinting\Users\UserRoleDAO;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Admin\WebApplication;
use BlakePrinting\Util\Utils;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Collator;
use PDO;

class CatalogControllerProvider implements ControllerProviderInterface {
    public function connect(Application $app) {
        $controllers = $app["controllers_factory"];

        if($app instanceof WebApplication) {
            $controllers->post("/update_selections", self::class . "::updateSelections")
                ->bind("catalog_update_selections")
            ;
            $controllers->get("/search.json", self::class . "::jsonCatalogSearch")
                ->bind("catalog_search_json")
            ;
            $controllers->get("/search_names.json", self::class . "::jsonNameSearch")
                ->bind("catalog_search_names_json")
            ;
            $controllers->get("/", self::class . "::displayIndex")
                ->bind("catalog")
            ;
        }

        return $controllers;
    }

    public function updateSelections(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_CATALOG)) {
            throw new AccessDeniedException();
        }

        $total = 0;
        $stored = 0;
        $deleted = 0;
        $requestContent = $request->getContent();

        if(!empty($requestContent)) {
            $updateItems = json_decode($requestContent);

            if(Utils::isNonEmptyArray($updateItems)) {
                $connection =& $app["merchant.mysql.connection"];
                $catalogSelectionSessionDao = new CatalogSelectionSessionDAO($connection);
                $catalogSelectionProductDao = new CatalogSelectionProductDAO($connection);
                $catalogSelectionPartDao = new CatalogSelectionPartDAO($connection);
                $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
                $catalogSelectionSessionVo =& $catalogSelectionSessionDao->findOrCreate(new CatalogSelectionSessionVO($userId));
                $catalogSelectionSessionId = $catalogSelectionSessionVo->getId();

                foreach($updateItems as &$updateItem) {
                    $id =& $updateItem->id;
                    $type =& $updateItem->type;
                    $quantity =& $updateItem->quantity;
                    $total++;

                    if($type == "Product") {
                        if($quantity > 0) {
                            if($catalogSelectionProductDao->store($catalogSelectionSessionId, $id, $quantity) > 0) {
                                $stored++;
                            }
                        } else {
                            if($catalogSelectionProductDao->delete($catalogSelectionSessionId, $id) > 0) {
                                $deleted++;
                            }
                        }
                    } else if($type == "Part") {
                        if($quantity > 0) {
                            if($catalogSelectionPartDao->store($catalogSelectionSessionId, $id, $quantity) > 0) {
                                $stored++;
                            }
                        } else {
                            if($catalogSelectionPartDao->delete($catalogSelectionSessionId, $id) > 0) {
                                $deleted++;
                            }
                        }
                    }
                }

                unset($updateItem);
            }
        }

        $results = [
            "total" => $total,
            "stored" => $stored,
            "deleted" => $deleted,
            "rows" => []
        ];

        return $app->json($results);
    }

    public function jsonCatalogSearch(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_CATALOG)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $catalogSelectionSessionDao = new CatalogSelectionSessionDAO($connection);
        $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
        $catalogSelectionSessionVo =& $catalogSelectionSessionDao->findByColumn("user_id", $userId, PDO::PARAM_INT);
        $items =& $this->genericSearch($app, $request);
        $results = [
            "total" => count($items),
            "rows" => $items
        ];

        if($catalogSelectionSessionVo !== null) {
            $results["catalog_selection_session_id"] = $catalogSelectionSessionVo->getId();
        }

        return $app->json($results);
    }

    public function jsonNameSearch(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_CATALOG)) {
            throw new AccessDeniedException();
        }

        $terms = $request->query->get("search_terms");

        if(empty($terms)) {
            $app->abort(400, "No search terms specified");
        }

        $connection =& $app["merchant.mysql.connection"];
        $catalogSelectionSessionDao = new CatalogSelectionSessionDAO($connection);
        $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
        $catalogSelectionSessionVo =& $catalogSelectionSessionDao->findByColumn("user_id", $userId, PDO::PARAM_INT);
        $names =& $this->genericSearch($app, $request, true);
        $collator = new Collator("en_US");

        $collator->sort($names);

        $results = [
            "total" => count($names),
            "rows" => $names
        ];

        if($catalogSelectionSessionVo !== null) {
            $results["catalog_selection_session_id"] = $catalogSelectionSessionVo->getId();
        }

        return $app->json($results);
    }

    /**
     * @param bool $namesOnly
     * @return array
     */
    private function &genericSearch(WebApplication $app, Request $request, $namesOnly = false) {
        $source = AppUtil::getIntFromQuery($request, "search_source");
        $terms = $request->query->get("search_terms");
        $type = $request->query->get("search_type");
        $cartOnly = $request->query->get("search_cart");
        $connection =& $app["merchant.mysql.connection"];
        $dataSourceDao = new DataSourceDAO($connection);
        $dataSourceIdentifierDao = new DataSourceIdentifierDAO($connection);
        $catalogSelectionSessionDao = new CatalogSelectionSessionDAO($connection);
        $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
        $items = [];

        if(empty($type) || $type == "products") {
            $catalogProductDao = new CatalogProductDAO($connection);
            $catalogSelectionProductDao = new CatalogSelectionProductDAO($connection);
            $catalogProductQuery =& PDOQuery::select()
                ->columns("catalog_product.name")
                ->from($catalogProductDao)
            ;

            if($namesOnly) {
                $catalogProductQuery->distinct();
            } else {
                $catalogProductQuery->columns("catalog_product.id", "catalog_selection_product.quantity");
            }

            if($cartOnly == "true") {
                $catalogProductQuery
                    ->join($catalogSelectionProductDao, ["catalog_product.id = catalog_selection_product.product_id"])
                    ->join($catalogSelectionSessionDao, [
                        "catalog_selection_product.catalog_selection_session_id = catalog_selection_session.id",
                        "catalog_selection_session.user_id = " . $userId
                    ])
                    ->where("catalog_selection_product.quantity > 0")
                ;
            } else {
                $catalogProductQuery
                    ->leftJoin($catalogSelectionProductDao, ["catalog_product.id = catalog_selection_product.product_id"])
                    ->leftJoin($catalogSelectionSessionDao, [
                        "catalog_selection_product.catalog_selection_session_id = catalog_selection_session.id",
                        "catalog_selection_session.user_id = " . $userId
                    ])
                ;
            }

            if(!empty($terms)) {
                $catalogProductQuery->where("UPPER(catalog_product.name) like UPPER(" . $connection->quote("%" . $terms . "%") . ")");
            }

            if(!empty($source)) {
                $catalogProductIdentifierDao = new CatalogProductIdentifierDAO($connection);
                $catalogProductQuery
                    ->join($catalogProductIdentifierDao, ["catalog_product.id = catalog_product_identifier.product_id"])
                    ->join($dataSourceIdentifierDao, ["catalog_product_identifier.data_source_identifier_id = data_source_identifier.id"])
                    ->join($dataSourceDao, ["data_source_identifier.data_source_id = data_source.id"])
                    ->where("data_source.id", $source, PDO::PARAM_INT)
                ;
            }

            $catalogProductStmt = $catalogProductQuery->execute($connection);

            while(($catalogProduct = $catalogProductStmt->fetch(PDO::FETCH_ASSOC)) !== false) {
                if($namesOnly) {
                    $items[] = $catalogProduct["name"];
                } else {
                    $catalogProduct["type"] = "Product";

                    $items[] = $catalogProduct;
                }
            }
        }

        if(empty($type) || $type == "parts") {
            $catalogPartDao = new CatalogPartDAO($connection);
            $catalogSelectionPartDao = new CatalogSelectionPartDAO($connection);
            $catalogProductPartDao = new CatalogProductPartDAO($connection);
            $catalogPartQuery =& PDOQuery::select()
                ->columns("catalog_part.name")
                ->from($catalogPartDao)
                ->leftJoin($catalogProductPartDao, ["catalog_part.id = catalog_product_part.part_id"])
                ->whereNull("catalog_product_part.product_id")
            ;

            if($namesOnly) {
                $catalogPartQuery->distinct();
            } else {
                $catalogPartQuery->columns("catalog_part.id", "catalog_selection_part.quantity");
            }

            if($cartOnly == "true") {
                $catalogPartQuery
                    ->join($catalogSelectionPartDao, ["catalog_part.id = catalog_selection_part.part_id"])
                    ->join($catalogSelectionSessionDao, [
                        "catalog_selection_part.catalog_selection_session_id = catalog_selection_session.id",
                        "catalog_selection_session.user_id = " . $userId
                    ])
                    ->where("catalog_selection_part.quantity > 0")
                ;
            } else {
                $catalogPartQuery
                    ->leftJoin($catalogSelectionPartDao, ["catalog_part.id = catalog_selection_part.part_id"])
                    ->leftJoin($catalogSelectionSessionDao, [
                        "catalog_selection_part.catalog_selection_session_id = catalog_selection_session.id",
                        "catalog_selection_session.user_id = " . $userId
                    ])
                ;
            }

            if(!empty($terms)) {
                $catalogPartQuery->where("UPPER(catalog_part.name) like UPPER(" . $connection->quote("%" . $terms . "%") . ")");
            }

            if(!empty($source)) {
                $catalogPartIdentifierDao = new CatalogPartIdentifierDAO($connection);
                $catalogPartQuery
                    ->join($catalogPartIdentifierDao, ["catalog_part.id = catalog_part_identifier.part_id"])
                    ->join($dataSourceIdentifierDao, ["catalog_part_identifier.data_source_identifier_id = data_source_identifier.id"])
                    ->join($dataSourceDao, ["data_source_identifier.data_source_id = data_source.id"])
                    ->where("data_source.id", $source, PDO::PARAM_INT)
                ;
            }

            $catalogPartStmt = $catalogPartQuery->execute($connection);

            while(($catalogPart = $catalogPartStmt->fetch(PDO::FETCH_ASSOC)) !== false) {
                if($namesOnly) {
                    $items[] = $catalogPart["name"];
                } else {
                    $catalogPart["type"] = "Part";

                    $items[] = $catalogPart;
                }
            }
        }

        if($namesOnly) {
            $items = array_unique($items);
        }

        return $items;
    }

    public function displayIndex(WebApplication $app) {
        if(!$app->isGranted(UserRoleDAO::ROLE_CATALOG)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $dataSourceDao = new DataSourceDAO($connection);

        return $app["twig"]->render("catalog.html.twig", [
            "nav_section" => "catalog",
            "data_sources" => $dataSourceDao->findAll(null, "name ASC")
        ]);
    }
}
