<?php

namespace BlakePrinting\Admin\Catalog;

class CatalogSelectionPartVO {
    /** @var int */
    private $catalogSelectionSessionId = null;
    /** @var int */
    private $partId = null;
    /** @var int */
    private $quantity = null;

    /**
     * @return int
     */
    public function getCatalogSelectionSessionId()
    {
        return $this->catalogSelectionSessionId;
    }

    /**
     * @param int $catalogSelectionSessionId
     */
    public function setCatalogSelectionSessionId($catalogSelectionSessionId)
    {
        $this->catalogSelectionSessionId = $catalogSelectionSessionId;
    }

    /**
     * @return int
     */
    public function getPartId()
    {
        return $this->partId;
    }

    /**
     * @param int $partId
     */
    public function setPartId($partId)
    {
        $this->partId = $partId;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
}
