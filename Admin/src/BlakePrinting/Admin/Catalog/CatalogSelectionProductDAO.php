<?php

namespace BlakePrinting\Admin\Catalog;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static CatalogSelectionProductDAO instance(PDO $connection = null, $schemaName = null)
 * @method CatalogSelectionProductVO findById($id, $options = null, $useCache = false)
 * @method CatalogSelectionProductVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogSelectionProductVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogSelectionProductVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method CatalogSelectionProductVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogSelectionProductVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogSelectionProductVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class CatalogSelectionProductDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $catalogSelectionSessionId
     * @param int $productId
     * @param int $quantity
     * @return int
     */
    public function &store($catalogSelectionSessionId, $productId, $quantity) {
        return PDOQuery::insert()
            ->into($this)
            ->value("catalog_selection_session_id", $catalogSelectionSessionId, PDO::PARAM_INT)
            ->value("product_id", $productId, PDO::PARAM_INT)
            ->value("quantity", $quantity, PDO::PARAM_INT)
            ->onDuplicateValue("quantity", "VALUES(quantity)")
            ->executeGetRowCount($this->getConnection())
        ;
    }

    /**
     * @param int $catalogSelectionSessionId
     * @param int $productId
     * @return int
     */
    public function &delete($catalogSelectionSessionId, $productId) {
        return PDOQuery::delete()
            ->from($this)
            ->where("catalog_selection_session_id", $catalogSelectionSessionId, PDO::PARAM_INT)
            ->where("product_id", $productId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
