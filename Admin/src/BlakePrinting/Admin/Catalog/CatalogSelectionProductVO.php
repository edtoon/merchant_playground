<?php

namespace BlakePrinting\Admin\Catalog;

class CatalogSelectionProductVO {
    /** @var int */
    private $catalogSelectionSessionId = null;
    /** @var int */
    private $productId = null;
    /** @var int */
    private $quantity = null;

    /**
     * @return int
     */
    public function getCatalogSelectionSessionId()
    {
        return $this->catalogSelectionSessionId;
    }

    /**
     * @param int $catalogSelectionSessionId
     */
    public function setCatalogSelectionSessionId($catalogSelectionSessionId)
    {
        $this->catalogSelectionSessionId = $catalogSelectionSessionId;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
}
