<?php

namespace BlakePrinting\Admin\Organizations;

use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Admin\WebApplication;
use BlakePrinting\Organizations\OrganizationDAO;
use BlakePrinting\Util\Utils;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class OrganizationsControllerProvider implements ControllerProviderInterface {
    public function connect(Application $app) {
        $controllers = $app["controllers_factory"];

        if($app instanceof WebApplication) {
            $controllers->get("/names.json", self::class . "::jsonOrganizationNameSearch")
                ->bind("organizations_names_json")
            ;
        }

        return $controllers;
    }

    public function jsonOrganizationNameSearch(WebApplication $app, Request $request) {
        $connection =& $app["merchant.mysql.connection"];
        $organizationDao =& OrganizationDAO::instance($connection);
        $search = Utils::getTrimmedStringOrNull($request->query->get("search"));
        $select =& PDOQuery::select()
            ->column("name")
            ->from($organizationDao)
            ->orderBy("UCASE(name) ASC")
            ->limit(10)
        ;

        if($search !== null) {
            $select->where(
                "UCASE(code) LIKE UCASE(" . $connection->quote("%" . $search . "%") . ")" .
                " OR " .
                "UCASE(name) LIKE UCASE(" . $connection->quote("%" . $search . "%") . ")"
            );
        }

        $rows = $select->getColumnValues($connection);
        $results = [
            "total" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }
}
