<?php

namespace BlakePrinting\Admin\Printers;

use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Admin\WebApplication;
use BlakePrinting\Printers\GatewayDAO;
use BlakePrinting\Printers\GatewayPrinterDAO;
use BlakePrinting\Printers\PrinterDAO;
use BlakePrinting\Users\UserDAO;
use BlakePrinting\Users\Entities\UserFacilityDAO;
use PDO;
use ReflectionClass;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;

class PrintersControllerProvider implements ControllerProviderInterface {
    public function connect(Application $app) {
        $controllers = $app["controllers_factory"];

        if($app instanceof WebApplication) {
            $controllers->get("/all.json", self::class . "::jsonPrinters")
                ->bind("printers_json")
            ;
        }

        return $controllers;
    }

    public function jsonPrinters(WebApplication $app) {
        $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
        $connection =& $app["merchant.mysql.connection"];
        $printerDao = new PrinterDAO($connection);
        $printers =& $printerDao->findAll(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$userId, &$connection) {
                $gatewayPrinterDao = new GatewayPrinterDAO($connection);
                $gatewayDao = new GatewayDAO($connection);
                $userFacilityDao = new UserFacilityDAO($connection);
                $userDao = new UserDAO($connection);
                $pdoQuery
                    ->columns("printer.*")
                    ->join($gatewayPrinterDao, ["printer.id = gateway_printer.printer_id"])
                    ->join($gatewayDao, ["gateway_printer.gateway_id = gateway.id"])
                    ->join($userFacilityDao, ["gateway.facility_id = user_facility.facility_id"])
                    ->join($userDao, ["user.id = user_facility.user_id"])
                    ->where("user.id", $userId, PDO::PARAM_INT)
                ;
            },
            "product_name ASC, system_name ASC"
        );

        $results = ["total" => count($printers)];
        $results["rows"] =& $printers;

        return $app->json($results);
    }
}
