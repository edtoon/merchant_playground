<?php

namespace BlakePrinting\Admin\Labels;

use BlakePrinting\Catalog\CatalogQualityGradeDAO;
use BlakePrinting\Admin\Catalog\CatalogSelectionPartDAO;
use BlakePrinting\Admin\Catalog\CatalogSelectionProductDAO;
use BlakePrinting\Admin\Catalog\CatalogSelectionSessionDAO;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Facilities\FacilityLocationDAO;
use BlakePrinting\Labels\LabelDAO;
use BlakePrinting\Labels\LabelPartDAO;
use BlakePrinting\Labels\LabelPartVO;
use BlakePrinting\Labels\LabelProductDAO;
use BlakePrinting\Labels\LabelProductVO;
use BlakePrinting\Labels\LabelTypeDAO;
use BlakePrinting\Labels\LabelUtil;
use BlakePrinting\Labels\LabelVO;
use BlakePrinting\Locations\LocationDAO;
use BlakePrinting\Organizations\OrganizationTypeDAO;
use BlakePrinting\Organizations\OrganizationUtil;
use BlakePrinting\Printers\GatewayDAO;
use BlakePrinting\Printers\GatewayPrinterDAO;
use BlakePrinting\Printers\GatewayPrinterLabelQueueDAO;
use BlakePrinting\Printers\PrinterDAO;
use BlakePrinting\Printers\PrinterUtil;
use BlakePrinting\Users\UserDAO;
use BlakePrinting\Users\UserRoleDAO;
use BlakePrinting\Admin\WebApplication;
use BlakePrinting\Users\UserUtil;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use PDO;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class LabelsControllerProvider implements ControllerProviderInterface {
    public function connect(Application $app) {
        $controllers =& $app["controllers_factory"];

        if($app instanceof WebApplication) {
            $controllers->get("/{labelId}/print_history.json", self::class . "::jsonLabelPrintHistory")
                ->bind("labels_print_history_json")
            ;
            $controllers->get("/{labelId}/contents.json", self::class . "::jsonLabelContents")
                ->bind("labels_contents_json")
            ;
            $controllers->post("/{labelId}/edit.json", self::class . "::editLabel")
                ->bind("labels_edit_json")
            ;
            $controllers->get("/{labelId}/edit", self::class . "::displayEdit")
                ->bind("labels_edit")
            ;
            $controllers->post("/create.json", self::class . "::createLabel")
                ->bind("labels_create_json")
            ;
            $controllers->get("/create", self::class . "::displayCreate")
                ->bind("labels_create")
            ;
            $controllers->get("/search.json", self::class . "::jsonSearchLabels")
                ->bind("labels_search_json")
            ;
            $controllers->get("/", self::class . "::displayIndex")
                ->bind("labels")
            ;
        }

        return $controllers;
    }

    public function editLabel(WebApplication $app, Request $request, $labelId) {
        if(!$app->isGranted(UserRoleDAO::ROLE_INVENTORY)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
        $labelDao =& LabelDAO::instance($connection);
        $labelVo = (empty($labelId) ? null : $labelDao->findById($labelId, "FOR UPDATE"));

        if($labelVo === null) {
            $app->abort(Response::HTTP_BAD_REQUEST, "Missing or invalid label ID");
        }

        if(!UserUtil::checkUserAuthorizedForLocation($userId, $labelVo->getLocationId(), $connection)) {
            throw new AccessDeniedException();
        }

        $total = 0;
        $stored = 0;
        $deleted = 0;
        $requestContent = $request->getContent();

        if(!empty($requestContent)) {
            $logger =& LogUtil::getLogger();
            $label = json_decode($requestContent);
            $itemCount = count($label->items);

            $logger->debug("Update of label #" . $labelId . " requested: " . Utils::getVarDump($label));

            if($itemCount > 0) {
                $connection =& $app["merchant.mysql.connection"];
                $labelPartDao =& LabelPartDAO::instance($connection);
                $labelProductDao =& LabelProductDAO::instance($connection);

                foreach($label->items as &$item) {
                    $total++;

                    switch($item->type) {
                        case "Part":
                            if($item->quantity > 0) {
                                $labelPartVo = new LabelPartVO();

                                $labelPartVo->setId($item->id);
                                $labelPartVo->setQuantity($item->quantity);
                                $labelPartVo->setYear($item->year);
                                $labelPartVo->setSupplierOrganizationId($item->supplier_organization_id);
                                $labelPartVo->setCatalogQualityGradeId($item->catalog_quality_grade_id);

                                if($labelPartDao->store($labelPartVo) > 0) {
                                    $stored++;
                                }
                            } else {
                                $labelPartDao->delete($item->id);

                                $deleted++;
                            }

                            break;
                        case "Product":
                            if($item->quantity > 0) {
                                $labelProductVo = new LabelProductVO();

                                $labelProductVo->setId($item->id);
                                $labelProductVo->setQuantity($item->quantity);
                                $labelProductVo->setYear($item->year);
                                $labelProductVo->setSupplierOrganizationId($item->supplier_organization_id);
                                $labelProductVo->setCatalogQualityGradeId($item->catalog_quality_grade_id);

                                if($labelProductDao->store($labelProductVo) > 0) {
                                    $stored++;
                                }
                            } else {
                                $labelProductDao->delete($item->id);

                                $deleted++;
                            }

                            break;
                    }
                }

                unset($item);
            }
        }

        if($stored > 0 || $deleted > 0) {
            $labelVo->setUpdater($userId);
            $labelDao->update($labelVo);
        }

        $results = [
            "total" => $total,
            "stored" => $stored,
            "deleted" => $deleted,
            "rows" => []
        ];

        return $app->json($results);
    }

    public function jsonLabelPrintHistory(WebApplication $app, Request $request, $labelId) {
        if(!$app->isGranted(UserRoleDAO::ROLE_INVENTORY)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
        $labelVo = null;

        if(!empty($labelId)) {
            $labelDao =& LabelDAO::instance($connection);
            $labelVo =& $labelDao->findById($labelId);
        }

        if($labelVo === null) {
            $app->abort(Response::HTTP_BAD_REQUEST, "Missing or invalid label ID");
        }

        if(!UserUtil::checkUserAuthorizedForLocation($userId, $labelVo->getLocationId(), $connection)) {
            throw new AccessDeniedException();
        }

        $gatewayPrinterLabelQueueDao =& GatewayPrinterLabelQueueDAO::instance($connection);
        $printerDao =& PrinterDAO::instance($connection);
        $historyItems =& PDOQuery::select()
            ->column("product_name")
            ->column("system_name")
            ->column("gateway_printer_label_queue.created AS queued")
            ->column("completed")
            ->column("cancelled")
            ->column("COALESCE(gateway_printer_label_queue.updated, gateway_printer_label_queue.created) AS updated")
            ->from($gatewayPrinterLabelQueueDao)
            ->join($printerDao, ["gateway_printer_label_queue.printer_id = printer.id"])
            ->where("gateway_printer_label_queue.label_id", $labelId, PDO::PARAM_INT)
            ->orderBy("updated DESC")
            ->getResults($connection)
        ;
        $results = [
            "total" => count($historyItems),
            "rows" => $historyItems
        ];

        return $app->json($results);
    }

    public function jsonLabelContents(WebApplication $app, Request $request, $labelId) {
        if(!$app->isGranted(UserRoleDAO::ROLE_INVENTORY)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
        $labelVo = null;

        if(!empty($labelId)) {
            $labelDao =& LabelDAO::instance($connection);
            $labelVo =& $labelDao->findById($labelId);
        }

        if($labelVo === null) {
            $app->abort(Response::HTTP_BAD_REQUEST, "Missing or invalid label ID");
        }

        if(!UserUtil::checkUserAuthorizedForLocation($userId, $labelVo->getLocationId(), $connection)) {
            throw new AccessDeniedException();
        }

        $labelItems =& LabelUtil::getLabelContents($labelId, $connection);
        $results = [
            "total" => count($labelItems),
            "rows" => $labelItems
        ];

        return $app->json($results);
    }

    public function displayEdit(WebApplication $app, Request $request, $labelId) {
        if(!$app->isGranted(UserRoleDAO::ROLE_INVENTORY)) {
            throw new AccessDeniedException();
        }

        $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
        $connection =& $app["merchant.mysql.connection"];
        $labelDao =& LabelDAO::instance($connection);
        $labelVo = (empty($labelId) ? null : $labelDao->findById($labelId));

        if($labelVo === null) {
            $app->abort(Response::HTTP_BAD_REQUEST, "Missing or invalid label ID");
        }

        if(!UserUtil::checkUserAuthorizedForLocation($userId, $labelVo->getLocationId(), $connection)) {
            throw new AccessDeniedException();
        }

        $labelTypeDao =& LabelTypeDAO::instance($connection);
        $labelTypeVo =& $labelTypeDao->findById($labelVo->getLabelTypeId());
        $userDao =& UserDAO::instance($connection);
        $creatorVo =& $userDao->findById($labelVo->getCreator());
        $updaterVo = ($labelVo->getUpdater() === null ? null : $userDao->findById($labelVo->getUpdater()));

        return $app["twig"]->render("labels/labels_edit.html.twig", [
            "nav_section" => "labels",
            "label" => $labelVo,
            "label_type" => $labelTypeVo,
            "creator" => $creatorVo,
            "updater" => $updaterVo,
            "printers" => PrinterUtil::getUserFacilityPrinters(
                $userId, $connection, "product_name ASC, system_name ASC"
            ),
            "supplier_organizations" => OrganizationUtil::getOrganizationsByType(
                OrganizationTypeDAO::ORGANIZATION_TYPE_SUPPLIER, $connection, "code ASC"
            ),
            "catalog_quality_grades" => CatalogQualityGradeDAO::instance($connection)->findAll(null, "id ASC")
        ]);
    }

    public function createLabel(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_INVENTORY)) {
            throw new AccessDeniedException();
        }

        $total = 0;
        $created = 0;
        $queued = 0;
        $requestContent = $request->getContent();

        if(!empty($requestContent)) {
            $logger =& LogUtil::getLogger();
            $label = json_decode($requestContent);

            $logger->debug("Label creation request: " . Utils::getVarDump($label));

            $itemCount = count($label->items);
            $connection =& $app["merchant.mysql.connection"];
            $labelDao =& LabelDAO::instance($connection);
            $labelPartDao =& LabelPartDAO::instance($connection);
            $labelProductDao =& LabelProductDAO::instance($connection);
            $labelTypeDao =& LabelTypeDAO::instance($connection);
            $facilityLocationDao =& FacilityLocationDAO::instance($connection);
            $gatewayDao =& GatewayDAO::instance($connection);
            $gatewayPrinterDao =& GatewayPrinterDAO::instance($connection);
            $gatewayPrinterLabelQueueDao =& GatewayPrinterLabelQueueDAO::instance($connection);
            $catalogSelectionPartDao =& CatalogSelectionPartDAO::instance($connection);
            $catalogSelectionProductDao =& CatalogSelectionProductDAO::instance($connection);
            $catalogSelectionSessionDao =& CatalogSelectionSessionDAO::instance($connection);
            $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
            $labelTypeVo =& $labelTypeDao->findById($label->label_type_id);
            $gatewayPrinterVo =& $gatewayPrinterDao->findByColumn("printer_id", $label->printer_id, PDO::PARAM_INT);
            $gatewayVo =& $gatewayDao->findById($gatewayPrinterVo->getGatewayId());
            $facilityLocationVo =& $facilityLocationDao->findByColumn("facility_id", $gatewayVo->getFacilityId(), PDO::PARAM_INT);
            $deletedParts = [];
            $deletedProducts = [];

            for($i = 0; $i < $label->count; $i++) {
                if($itemCount > 0) {
                    $labelVo = new LabelVO();

                    $labelVo->setCreator($userId);
                    $labelVo->setLabelTypeId($label->label_type_id);
                    $labelVo->setLocationId($facilityLocationVo->getLocationId());

                    $labelId =& $labelDao->insert($labelVo);

                    $total++;

                    switch($labelTypeVo->getName()) {
                        case LabelTypeDAO::LABEL_TYPE_INVENTORY_BOX:
                            foreach($label->items as &$item) {
                                switch($item->type) {
                                    case "Part":
                                        $labelPartVo = new LabelPartVO();

                                        $labelPartVo->setLabelId($labelId);
                                        $labelPartVo->setPartId($item->id);
                                        $labelPartVo->setQuantity($item->quantity);
                                        $labelPartVo->setYear($item->year);
                                        $labelPartVo->setSupplierOrganizationId($item->supplier_organization_id);
                                        $labelPartVo->setCatalogQualityGradeId($item->catalog_quality_grade_id);

                                        $labelPartDao->store($labelPartVo);

                                        if(in_array($item->id, $deletedParts) === false) {
                                            $catalogSelectionPartDao->delete($label->catalog_selection_session_id, $item->id);

                                            $deletedParts[] = $item->id;
                                        }
                                    break;
                                    case "Product":
                                        $labelProductVo = new LabelProductVO();

                                        $labelProductVo->setLabelId($labelId);
                                        $labelProductVo->setProductId($item->id);
                                        $labelProductVo->setQuantity($item->quantity);
                                        $labelProductVo->setYear($item->year);
                                        $labelProductVo->setSupplierOrganizationId($item->supplier_organization_id);
                                        $labelProductVo->setCatalogQualityGradeId($item->catalog_quality_grade_id);

                                        $labelProductDao->store($labelProductVo);

                                        if(in_array($item->id, $deletedProducts) === false) {
                                            $catalogSelectionProductDao->delete($label->catalog_selection_session_id, $item->id);

                                            $deletedProducts[] = $item->id;
                                        }
                                    break;
                                }
                            }

                            unset($item);
                            break;
                    }

                    $created++;

                    $gatewayPrinterLabelQueueDao->store($gatewayPrinterVo->getGatewayId(), $label->printer_id, $labelId);

                    $queued++;
                }
            }

            $catalogSelectionSessionDao->delete($label->catalog_selection_session_id);
        }

        $results = [
            "total" => $total,
            "created" => $created,
            "queued" => $queued,
            "rows" => []
        ];

        return $app->json($results);
    }

    public function displayCreate(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_INVENTORY)) {
            throw new AccessDeniedException();
        }

        $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
        $connection =& $app["merchant.mysql.connection"];

        return $app["twig"]->render("labels/labels_create.html.twig", [
            "nav_section" => "labels",
            "side_section" => "create",
            "label_types" => LabelTypeDAO::instance($connection)->findAll(null, "name ASC"),
            "printers" => PrinterUtil::getUserFacilityPrinters(
                $userId, $connection, "product_name ASC, system_name ASC"
            ),
            "supplier_organizations" => OrganizationUtil::getOrganizationsByType(
                OrganizationTypeDAO::ORGANIZATION_TYPE_SUPPLIER, $connection, "code ASC"
            ),
            "catalog_quality_grades" => CatalogQualityGradeDAO::instance($connection)->findAll(null, "id ASC")
        ]);
    }

    public function jsonSearchLabels(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_INVENTORY)) {
            throw new AccessDeniedException();
        }

        $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
        $connection =& $app["merchant.mysql.connection"];
        $userFacilityLocationIds =& UserUtil::getUserFacilityLocations($userId, $connection);
        $locationDao =& LocationDAO::instance($connection);
        $locationTbl = $locationDao->formatSchemaAndTableName();
        $labelTypeDao =& LabelTypeDAO::instance($connection);
        $labelDao =& LabelDAO::instance($connection);
        $userDao =& UserDAO::instance($connection);
        $gatewayPrinterLabelQueueDao =& GatewayPrinterLabelQueueDAO::instance($connection);
        $gatewayPrinterLabelQueueTbl = $gatewayPrinterLabelQueueDao->formatSchemaAndTableName();
        $labels = [];

        foreach($userFacilityLocationIds as &$facilityLocationId) {
            $locationIds =& PDOQuery::select()
                ->column("id")
                ->from("(select id, parent_location_id from " . $locationTbl . " order by parent_location_id, id) base")
                ->from("(select @rootLocationId := " . $facilityLocationId . ") tmp")
                ->where("FIND_IN_SET(parent_location_id, @rootLocationId) > 0")
                ->where("@rootLocationId := CONCAT(@rootLocationId, ',', id)")
                ->getColumnValues($connection)
            ;
            $query =& PDOQuery::select()
                ->column("label.id")
                ->column("label.created")
                ->column("COALESCE(label.updated, label.created) as updated")
                ->column("label_type_id")
                ->column("creator")
                ->column("user.name AS user_name")
                ->column("location.code AS location_code")
                ->column("location.name AS location_name")
                ->column("(" .
                    "SELECT COUNT(*) " .
                    "FROM " . $gatewayPrinterLabelQueueTbl . " " .
                    "WHERE gateway_printer_label_queue.label_id = label.id " .
                    "AND completed IS NULL " .
                    "AND cancelled IS NULL " .
                ") AS queued")
                ->column("(" .
                    "SELECT COUNT(*) " .
                    "FROM " . $gatewayPrinterLabelQueueTbl . " " .
                    "WHERE gateway_printer_label_queue.label_id = label.id " .
                    "AND completed IS NOT NULL " .
                ") AS printed")
                ->column("(" .
                    "SELECT COUNT(*) " .
                    "FROM " . $gatewayPrinterLabelQueueTbl . " " .
                    "WHERE gateway_printer_label_queue.label_id = label.id " .
                    "AND cancelled IS NOT NULL " .
                ") AS cancelled")
                ->from($labelDao)
                ->join($userDao, ["creator = user.id"])
                ->join($locationDao, ["label.location_id = location.id"])
            ;

            if(!empty($locationIds)) {
                $query->where("(location_id = " . $facilityLocationId . " OR location_id IN (" . implode(",", $locationIds) . "))");
            } else {
                $query->where("location_id", $facilityLocationId, PDO::PARAM_INT);
            }

            $locationLabels =& $query->getResults($connection);

            if(!empty($locationLabels)) {
                foreach($locationLabels as &$locationLabel) {
                    $locationLabel["label_type_name"] = $labelTypeDao
                        ->findById($locationLabel["label_type_id"], null, true)
                        ->getName()
                    ;

                    $labels[] =& $locationLabel;
                }

                unset($locationLabel);
            }
        }

        usort($labels, function(&$a, &$b) {
            return $b["updated"] - $a["updated"];
        });

        $results = [
            "total" => count($labels),
            "rows" => $labels
        ];

        return $app->json($results);
    }

    public function displayIndex(WebApplication $app) {
        if(!$app->isGranted(UserRoleDAO::ROLE_INVENTORY)) {
            throw new AccessDeniedException();
        }

        return $app["twig"]->render("labels/labels.html.twig", [
            "nav_section" => "labels",
            "side_section" => "view"
        ]);
    }
}
