<?php

namespace BlakePrinting\Admin\Shipping;

use BlakePrinting\Admin\AppUtil;
use BlakePrinting\Admin\WebApplication;
use BlakePrinting\Catalog\CatalogProductDAO;
use BlakePrinting\Contacts\ContactAddressDAO;
use BlakePrinting\Contacts\ContactPersonDAO;
use BlakePrinting\DataSources\DataFeedDAO;
use BlakePrinting\DataSources\DataSourceDAO;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Globalization\CountryDAO;
use BlakePrinting\Organizations\OrganizationDAO;
use BlakePrinting\Sales\Orders\SalesOrderDAO;
use BlakePrinting\Sales\Orders\SalesOrderShipmentDAO;
use BlakePrinting\Sales\SalesChannelDAO;
use BlakePrinting\Sales\Orders\SalesOrderDataFeedDAO;
use BlakePrinting\Shipping\ShipmentCatalogProductDAO;
use BlakePrinting\Shipping\ShipmentDAO;
use BlakePrinting\Shipping\ShipmentDataFeedDAO;
use BlakePrinting\Shipping\ShipmentStatusDAO;
use BlakePrinting\Shipping\ShipmentVO;
use BlakePrinting\Shipping\ShippingCarrierDAO;
use BlakePrinting\Shipping\ShippingUtil;
use BlakePrinting\Users\UserDAO;
use BlakePrinting\Users\UserRoleDAO;
use BlakePrinting\Users\UserUtil;
use BlakePrinting\Util\Utils;
use PDO;
use ReflectionClass;
use Silex\ControllerCollection;
use Stringy\StaticStringy as S;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ShippingShipmentsController {
    public static function connect(WebApplication $app, ControllerCollection $controllers) {
        $connection =& $app["merchant.mysql.connection"];
        $shipmentConverter = function($shipment) use (&$app, &$connection) {
            $shipmentDao =& ShipmentDao::instance($connection);
            $shipmentVo = ((Utils::isIntegerValue($shipment) && $shipment > 0) ? $shipmentDao->findById($shipment) : null);

            if($shipmentVo === null) {
                $app->abort(Response::HTTP_NOT_FOUND, "Missing or invalid shipment ID");
            }

            return $shipmentVo;
        };
        $controllers->get("/shipments/recipient.json", self::class . "::jsonShipmentRecipientSearch")
            ->bind("shipping_shipments_recipient_json")
        ;
        $controllers->get("/shipments/organizations.json", self::class . "::jsonShipmentRecipientOrganizationSearch")
            ->bind("shipping_shipments_recipient_organizations_json")
        ;
        $controllers->get("/shipments/{shipment}/products.json", self::class . "::jsonShipmentProducts")
            ->bind("shipping_shipments_shipment_products_json")
            ->assert("shipment", '\d+')
            ->convert("shipment", $shipmentConverter)
        ;
        $controllers->post("/shipments/{shipment}/_easypost", self::class . "::displayEasypostModal")
            ->bind("shipping_shipments_shipment_easypost")
            ->assert("shipment", '\d+')
            ->convert("shipment", $shipmentConverter)
        ;
        $controllers->get("/shipments/{shipment}", self::class . "::displayShipment")
            ->bind("shipping_shipments_shipment")
            ->assert("shipment", '\d+')
            ->convert("shipment", $shipmentConverter)
        ;
        $controllers->get("/shipments.json", self::class . "::jsonShipmentsSearch")
            ->bind("shipping_shipments_json")
        ;
        $controllers->get("/shipments/", self::class . "::displayShipments")
            ->bind("shipping_shipments")
        ;
    }

    public function jsonShipmentProducts(WebApplication $app, Request $request, ShipmentVO $shipment) {
        if($app->isGranted(UserRoleDAO::ROLE_TRIAL)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $page = AppUtil::getIntFromQuery($request, "page");
        $sortColumn = Utils::getTrimmedStringOrNull($request->query->get("sort_column"));
        $sortDirection = AppUtil::getOrderFromQuery($request, "sort_direction");
        $count = AppUtil::getIntFromQuery($request, "count");
        $catalogProductTbl = CatalogProductDAO::instance($connection)->formatSchemaAndTableName();
        $shipmentCatalogProductTbl = ShipmentCatalogProductDAO::instance($connection)->formatSchemaAndTableName();
        $select =& PDOQuery::select()
            ->calculateFoundRows()
            ->column($catalogProductTbl . ".id")
            ->column($catalogProductTbl . ".name")
            ->column($shipmentCatalogProductTbl . ".quantity")
            ->from($shipmentCatalogProductTbl)
            ->leftJoin($catalogProductTbl, [$shipmentCatalogProductTbl . ".catalog_product_id = " . $catalogProductTbl . ".id"])
            ->where($shipmentCatalogProductTbl . ".shipment_id", $shipment->getId(), PDO::PARAM_INT)
        ;

        $sortColumn = ($sortColumn === null ? null : (string)S::underscored($sortColumn));

        switch($sortColumn) {
            case "id":
                break;
            case "name":
                $sortColumn = "UCASE(" . $catalogProductTbl . "." . $sortColumn . ")";
                break;
            case "quantity":
            default:
                $sortColumn = "quantity";
                break;
        }

        $select->orderBy($sortColumn . ($sortDirection === null ? "" : (" " . $sortDirection)));

        if($count !== null && $count > 0) {
            $select->limit($count);

            if($page !== null && $page > 1) {
                $select->offset(($page - 1) * $count);
            }
        }

        $rows =& $select->getResults($connection);
        $calculated = $select->getCalculatedRowCount();
        $results = [
            "total" => count($rows),
            "calculated" => $calculated,
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function displayEasypostModal(WebApplication $app, Request $request, ShipmentVO $shipment) {
        if($app->isGranted(UserRoleDAO::ROLE_TRIAL)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $shipmentStatusVo = ShippingUtil::getShipmentStatusVoByShipmentId($shipment->getId(), $connection, "FOR UPDATE");

        if(ShipmentStatusDAO::SHIPMENT_STATUS_PENDING !== $shipmentStatusVo->getName()) {
            AppUtil::addError("shipment", "Shipment is no longer in pending status");

            return AppUtil::getErrorResponse($app);
        }

        $easypostModalContext = new EasyPostModalContext($app, $request, $shipment, $connection);
        $errorResponse = AppUtil::getErrorResponse($app);

        if($errorResponse !== null) {
            return $errorResponse;
        }

        $vars =& $easypostModalContext->getTemplateVariables($shipment);
        $result = [
            "body" => $app["twig"]->render(
                "shipping/shipping_shipment_easypost_step" . $easypostModalContext->currentStepNumber . "_body.html.twig", $vars
            ),
            "footer" => $app["twig"]->render(
                "shipping/shipping_shipment_easypost_step" . $easypostModalContext->currentStepNumber . "_footer.html.twig", $vars
            ),
            "vars" => $app["twig"]->render(
                "shipping/shipping_shipment_easypost_vars.html.twig", $vars
            )
        ];

        if($easypostModalContext->requestedStepNumber === null) {
            return $app->json([
                "html" => $app["twig"]->render(
                    "shipping/shipping_shipment_easypost_modal.html.twig", $result
                )
            ]);
        }

        return $app->json($result);
    }

    public function displayShipment(WebApplication $app, ShipmentVO $shipment) {
        if($app->isGranted(UserRoleDAO::ROLE_TRIAL)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $userDao =& UserDAO::instance($connection);
        $organizationDao =& OrganizationDAO::instance($connection);
        $dataFeedDao =& DataFeedDAO::instance($connection);
        $creatorVo = ($shipment->getCreator() === null ? null : $userDao->findById($shipment->getCreator()));
        $updaterVo = ($shipment->getUpdater() === null ? null : $userDao->findById($shipment->getUpdater()));
        $senderOrganizationVo = ($shipment->getSenderOrganizationId() === null ? null : $organizationDao->findById($shipment->getSenderOrganizationId()));
        $recipientOrganizationVo = ($shipment->getRecipientOrganizationId() === null ? null : $organizationDao->findById($shipment->getRecipientOrganizationId()));
        $contactPersonVo = ($shipment->getContactPersonId() === null ? null : ContactPersonDAO::instance($connection)->findById($shipment->getContactPersonId()));
        $contactAddressVo = ($shipment->getContactAddressId() === null ? null : ContactAddressDAO::instance($connection)->findById($shipment->getContactAddressId()));
        $countryVo = ($contactAddressVo === null ? null : CountryDAO::instance($connection)->findById($contactAddressVo->getCountryId()));
        $carrierVo = ($shipment->getShippingCarrierId() === null ? null : ShippingCarrierDAO::instance($connection)->findById($shipment->getShippingCarrierId()));
        $shipmentStatusVo = ($shipment->getShipmentStatusId() === null ? null : ShipmentStatusDAO::instance($connection)->findById($shipment->getShipmentStatusId()));
        $salesOrderShipmentVo =& SalesOrderShipmentDAO::instance($connection)->findByColumn("shipment_id", $shipment->getId(), PDO::PARAM_INT);
        $salesOrderVo = ($salesOrderShipmentVo === null ? null : SalesOrderDAO::instance($connection)->findById($salesOrderShipmentVo->getSalesOrderId()));
        $dataFeeds =& $dataFeedDao->findAll(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$salesOrderDao, &$dataFeedDao, &$salesOrderId, &$connection) {
                $dataFeedTbl = $dataFeedDao->formatSchemaAndTableName();
                $salesOrderDataFeedTbl = SalesOrderDataFeedDAO::instance($connection)->formatSchemaAndTableName();

                $pdoQuery
                    ->join($salesOrderDataFeedTbl, [$dataFeedTbl . ".id = " . $salesOrderDataFeedTbl . ".data_feed_id"])
                    ->where("sales_order_id", $salesOrderId, PDO::PARAM_INT)
                ;
            }
        );

        return $app["twig"]->render("shipping/shipping_shipment.html.twig", [
            "nav_section" => "shipping",
            "shipment" => $shipment,
            "sender_organization" => $senderOrganizationVo,
            "recipient_organization" => $recipientOrganizationVo,
            "contact_person" => $contactPersonVo,
            "contact_address" => $contactAddressVo,
            "country" => $countryVo,
            "carrier" => $carrierVo,
            "shipment_status" => $shipmentStatusVo,
            "sales_order" => $salesOrderVo,
            "data_feeds" => $dataFeeds,
            "data_sources" => DataSourceDAO::instance($connection)->findAll(null, "UCASE(name)"),
            "creator" => $creatorVo,
            "updater" => $updaterVo
        ]);
    }

    public function jsonShipmentsSearch(WebApplication $app, Request $request) {
        if($app->isGranted(UserRoleDAO::ROLE_TRIAL)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $shipmentTbl = ShipmentDAO::instance($connection)->formatSchemaAndTableName();
        $shipmentStatusTbl = ShipmentStatusDAO::instance($connection)->formatSchemaAndTableName();
        $shippingCarrierTbl = SalesChannelDAO::instance($connection)->formatSchemaAndTableName();
        $countryTbl = CountryDAO::instance($connection)->formatSchemaAndTableName();
        $organizationTbl = OrganizationDAO::instance($connection)->formatSchemaAndTableName();
        $contactAddressTbl = ContactAddressDAO::instance($connection)->formatSchemaAndTableName();
        $contactPersonTbl = ContactPersonDAO::instance($connection)->formatSchemaAndTableName();
        $salesOrderShipmentTbl = SalesOrderShipmentDAO::instance($connection)->formatSchemaAndTableName();
        $search =& $this->generateShipmentsSearch($app, $request)
            ->calculateFoundRows()
            ->distinct()
            ->column($shipmentTbl . ".id")
        ;
        $shipmentIds =& $search->getColumnValues($connection);
        $rows = [];

        if(!empty($shipmentIds)) {
            foreach($shipmentIds as &$shipmentId) {
                $rows[] = PDOQuery::select()
                    ->distinct()
                    ->column($shipmentTbl . ".id")
                    ->column($shipmentTbl . ".created")
                    ->column($shipmentTbl . ".updated")
                    ->column($shipmentTbl . ".shipped")
                    ->column($shipmentTbl . ".contact_phone_id")
                    ->column($shipmentTbl . ".contact_person_id")
                    ->column($shipmentTbl . ".contact_address_id")
                    ->column($shipmentTbl . ".shipment_status_id")
                    ->column($salesOrderShipmentTbl . ".sales_order_id")
                    ->column([($shipmentStatusTbl . ".name") => "shipment_status_name"])
                    ->column([($shippingCarrierTbl . ".name") => "shipping_carrier_name"])
                    ->column(["sender_organization.name" => "sender_organization_name"])
                    ->column(["recipient_organization.name" => "recipient_organization_name"])
                    ->column([$contactPersonTbl . ".name" => "recipient_name"])
                    ->column([$countryTbl . ".iso_code" => "country_code"])
                    ->from($shipmentTbl)
                    ->leftJoin($shipmentStatusTbl, [$shipmentTbl . ".shipment_status_id = " . $shipmentStatusTbl . ".id"])
                    ->leftJoin($shippingCarrierTbl, [$shipmentTbl . ".shipping_carrier_id = " . $shippingCarrierTbl . ".id"])
                    ->leftJoin($organizationTbl . " AS sender_organization", [$shipmentTbl . ".sender_organization_id = sender_organization.id"])
                    ->leftJoin($organizationTbl . " AS recipient_organization", [$shipmentTbl . ".recipient_organization_id = recipient_organization.id"])
                    ->leftJoin($contactAddressTbl, [$shipmentTbl . ".contact_address_id = " . $contactAddressTbl . ".id"])
                    ->leftJoin($countryTbl, [$contactAddressTbl . ".country_id = " . $countryTbl . ".id"])
                    ->leftJoin($contactPersonTbl, [$shipmentTbl . ".contact_person_id = " . $contactPersonTbl . ".id"])
                    ->leftJoin($salesOrderShipmentTbl, [$shipmentTbl . ".id = " . $salesOrderShipmentTbl . ".shipment_id"])
                    ->where($shipmentTbl . ".id", $shipmentId, PDO::PARAM_INT)
                    ->getSingleRow($connection)
                ;
            }

            unset($shipmentId);
        }

        $results = [
            "total" => count($rows),
            "calculated" => $search->getCalculatedRowCount(),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function jsonShipmentRecipientSearch(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_CATALOG)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $select =& $this->generateShipmentsSearch($app, $request, ["contact_person_name"], 10)
            ->distinct()
        ;
        $rows =& $select->getColumnValues($connection);
        $results = [
            "total" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    public function jsonShipmentRecipientOrganizationSearch(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_CATALOG)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $select =& $this->generateShipmentsSearch($app, $request, ["recipient_organization_name"], 10)
            ->distinct()
        ;
        $rows =& $select->getColumnValues($connection);
        $results = [
            "total" => count($rows),
            "rows" => $rows
        ];

        return $app->json($results);
    }

    /**
     * @param WebApplication $app
     * @param Request $request
     * @param string[] $columns
     * @param int $count
     * @return PDOQuery
     */
    private function &generateShipmentsSearch(WebApplication $app, Request $request, array $columns = [], $count = null) {
        $connection =& $app["merchant.mysql.connection"];
        $sortColumn = (Utils::getTrimmedStringOrNull($request->query->get("sort_column")) ?: "id");
        $sortDirection = AppUtil::getOrderFromQuery($request, "sort_direction", "ASC");
        $count = ($count === null ? AppUtil::getIntFromQuery($request, "count", 10) : $count);
        $page = AppUtil::getIntFromQuery($request, "page", 1);
        $dataSourceId = Utils::getTrimmedStringOrNull($request->query->get("data_source_id"));
        $shippingCarrierId = AppUtil::getIntFromQuery($request, "shipping_carrier_id");
        $senderOrganizationId = AppUtil::getIntFromQuery($request, "sender_organization_id");
        $recipientOrganizationName = Utils::getTrimmedStringOrNull($request->query->get("recipient_organization_name"));
        $name = Utils::getTrimmedStringOrNull($request->query->get("name"));
        $countryId = AppUtil::getIntFromQuery($request, "country_id");
        $shipmentTbl = ShipmentDAO::instance($connection)->formatSchemaAndTableName();
        $shipmentStatusTbl = ShipmentStatusDAO::instance($connection)->formatSchemaAndTableName();
        $shippingCarrierTbl = ShippingCarrierDAO::instance($connection)->formatSchemaAndTableName();
        $contactPersonTbl = ContactPersonDAO::instance($connection)->formatSchemaAndTableName();
        $contactAddressTbl = ContactAddressDAO::instance($connection)->formatSchemaAndTableName();
        $organizationTbl = OrganizationDAO::instance($connection)->formatSchemaAndTableName();
        $countryTbl = CountryDAO::instance($connection)->formatSchemaAndTableName();
        $select =& PDOQuery::select()
            ->from($shipmentTbl)
        ;

        if($dataSourceId !== null) {
            $shipmentDataFeedTbl = ShipmentDataFeedDAO::instance($connection)->formatSchemaAndTableName();

            if($dataSourceId === "manual") {
                $select
                    ->where("NOT EXISTS (" .
                        "SELECT 1 " .
                        "FROM " . $shipmentDataFeedTbl . " " .
                        "WHERE " . $shipmentDataFeedTbl . ".shipment_id = " . $shipmentTbl . ".id" .
                    ")")
                ;
            } else if(Utils::isIntegerValue($dataSourceId)) {
                $dataFeedTbl = DataFeedDAO::instance($connection)->formatSchemaAndTableName();
                $dataSourceTbl = DataSourceDAO::instance($connection)->formatSchemaAndTableName();

                $select
                    ->join($shipmentDataFeedTbl, [$shipmentTbl . ".id = " . $shipmentDataFeedTbl . ".shipment_id"])
                    ->join($dataFeedTbl, [$shipmentDataFeedTbl . ".data_feed_id = " . $dataFeedTbl . ".id"])
                    ->join($dataSourceTbl, [$dataFeedTbl . ".data_source_id = " . $dataSourceTbl . ".id"])
                    ->where($dataSourceTbl . ".id", $dataSourceId, PDO::PARAM_INT)
                ;
            }
        }

        if($shippingCarrierId !== null) {
            $select->where("shipping_carrier_id", $shippingCarrierId, PDO::PARAM_INT);
        }

        if($sortColumn === "statusName") {
            $select->leftJoin($shipmentStatusTbl, [$shipmentTbl . ".shipment_status_id = " . $shipmentStatusTbl . ".id"]);
        }

        if($sortColumn === "carrier") {
            $select->leftJoin($shippingCarrierTbl, [$shipmentTbl . ".shipping_carrier_id = " . $shippingCarrierTbl . ".id"]);
        }

        if($senderOrganizationId !== null) {
            $select
                ->join($organizationTbl . " AS sender_organization", [$shipmentTbl . ".sender_organization_id = sender_organization.id"])
                ->where("sender_organization_id", $senderOrganizationId, PDO::PARAM_INT)
            ;
        } else if($sortColumn === "senderOrganization") {
            $select->leftJoin($organizationTbl . " AS sender_organization", [$shipmentTbl . ".sender_organization_id = sender_organization.id"]);
        }

        $includeRecipientOrganizationName = in_array("recipient_organization_name", $columns);

        if($includeRecipientOrganizationName || $recipientOrganizationName !== null) {
            $select->join($organizationTbl . " AS recipient_organization", [$shipmentTbl . ".recipient_organization_id = recipient_organization.id"]);

            if($recipientOrganizationName !== null) {
                $select->where("UCASE(recipient_organization.name) LIKE UCASE(" . $connection->quote("%" . $recipientOrganizationName . "%") . ")");
            }

            if($includeRecipientOrganizationName) {
                $select->column(["recipient_organization.name" => "recipient_organization_name"]);
            }
        } else if($sortColumn === "recipientOrganization") {
            $select->leftJoin($organizationTbl . " AS recipient_organization", [$shipmentTbl . ".recipient_organization_id = recipient_organization.id"]);
        }

        $includeContactName = in_array("contact_person_name", $columns);

        if($includeContactName || $name !== null) {
            $select->join($contactPersonTbl, [$shipmentTbl . ".contact_person_id = " . $contactPersonTbl . ".id"]);

            if($name !== null) {
                $select->where("UCASE(" . $contactPersonTbl . ".name) LIKE UCASE(" . $connection->quote("%" . $name . "%") . ")");
            }

            if($includeContactName) {
                $select->column([($contactPersonTbl . ".name") => "contact_person_name"]);
            }
        } else {
            $select->leftJoin($contactPersonTbl, [$shipmentTbl . ".contact_person_id = " . $contactPersonTbl . ".id"]);
        }

        if($countryId !== null) {
            $select
                ->join($contactAddressTbl, [$shipmentTbl . ".contact_address_id = " . $contactAddressTbl . ".id"])
                ->join($countryTbl, [$contactAddressTbl . ".country_id = " . $countryTbl . ".id"])
                ->where($countryTbl . ".id", $countryId, PDO::PARAM_INT)
            ;
        } else {
            $select
                ->leftJoin($contactAddressTbl, [$shipmentTbl . ".contact_address_id = " . $contactAddressTbl . ".id"])
                ->leftJoin($countryTbl, [$contactAddressTbl . ".country_id = " . $countryTbl . ".id"])
            ;
        }

        if($sortColumn === "order") {
            $salesOrderShipmentTbl = SalesOrderShipmentDAO::instance($connection)->formatSchemaAndTableName();
            $sortColumn = "sales_order_id";

            $select->leftJoin($salesOrderShipmentTbl, [$shipmentTbl . ".id = " . $salesOrderShipmentTbl . ".shipment_id"]);
        }

        $orderBy = null;

        switch($sortColumn) {
            case "id":
            case "created":
            case "shipped":
                $orderBy = $shipmentTbl . "." . $sortColumn;
                break;
            case "sales_order_id":
                $orderBy = $sortColumn;
                break;
            case "modified":
                $orderBy = "COALESCE(" . $shipmentTbl . ".updated, " . $shipmentTbl . ".created)";
                break;
            case "carrier":
                $orderBy = "UCASE(" . $shippingCarrierTbl . ".name)";
                break;
            case "country":
                $orderBy = "UCASE(" . $countryTbl . ".iso_code)";
                break;
            case "statusName":
                $orderBy = "UCASE(" . $shipmentStatusTbl . ".name)";
                break;
            case "contactName":
                $orderBy = "UCASE(" . $contactPersonTbl . ".name)";
                break;
            case "senderOrganization":
                $orderBy = "UCASE(sender_organization.name)";
                break;
            case "recipientOrganization":
                $orderBy = "UCASE(recipient_organization.name)";
                break;
        }

        if($orderBy !== null) {
            $select->orderBy($orderBy . ($sortDirection === null ? "" : (" " . $sortDirection)));
        }

        if($count !== null && $count > 0) {
            $select->limit($count);

            if($page !== null && $page > 1) {
                $select->offset(($page - 1) * $count);
            }
        }

        return $select;
    }

    public function displayShipments(WebApplication $app) {
        if($app->isGranted(UserRoleDAO::ROLE_TRIAL)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
        $countryDao =& CountryDAO::instance($connection);
        $countries =& $countryDao->findAll(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$countryDao, &$connection) {
                $shipmentTbl = ShipmentDAO::instance($connection)->formatSchemaAndTableName();
                $contactAddressTbl = ContactAddressDAO::instance($connection)->formatSchemaAndTableName();
                $countryTbl = $countryDao->formatSchemaAndTableName();

                $pdoQuery
                    ->distinct()
                    ->columns($countryTbl . ".*")
                    ->join($contactAddressTbl, [$countryTbl . ".id = " . $contactAddressTbl . ".country_id"])
                    ->join($shipmentTbl, [$contactAddressTbl . ".id = " . $shipmentTbl . ".contact_address_id"])
                ;
            }, "UCASE(name) ASC"
        );
        $senderOrganizations =& UserUtil::getUserOrganizations($userId, $connection, "UCASE(name) ASC");

        return $app["twig"]->render("shipping/shipping_shipments.html.twig", [
            "nav_section" => "shipping",
            "side_section" => "shipments",
            "data_sources" => DataSourceDAO::instance($connection)->findAll(null, "UCASE(name) ASC"),
            "sender_organizations" => $senderOrganizations,
            "shipping_carriers" => ShippingCarrierDAO::instance($connection)->findAll(null, "UCASE(name) ASC"),
            "countries" => $countries
        ]);
    }
}
