<?php

namespace BlakePrinting\Admin\Shipping;

use BlakePrinting\Admin\AppUtil;
use BlakePrinting\Contacts\ContactAddressDAO;
use BlakePrinting\Contacts\ContactPersonDAO;
use BlakePrinting\EasyPost\EasyPostAccountDAO;
use BlakePrinting\EasyPost\EasyPostAccountVO;
use BlakePrinting\EasyPost\EasyPostAddressUtil;
use BlakePrinting\EasyPost\EasyPostAddressVO;
use BlakePrinting\EasyPost\EasyPostParcelUtil;
use BlakePrinting\Facilities\FacilityVO;
use BlakePrinting\Organizations\OrganizationDAO;
use BlakePrinting\Shipping\ShipmentVO;
use BlakePrinting\Users\UserUtil;
use BlakePrinting\Util\Utils;
use PDO;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class EasyPostModalContext {
    /** @var int */
    public $currentStepNumber = null;
    /** @var int */
    public $requestedStepNumber = null;
    /** @var float */
    public $parcelLength = null;
    /** @var float */
    public $parcelWidth = null;
    /** @var float */
    public $parcelHeight = null;
    /** @var float */
    public $parcelWeight = null;
    /** @var FacilityVO[] */
    public $facilities = null;
    /** @var FacilityVO */
    public $facilityVo = null;
    /** @var EasyPostAccountVO[] */
    public $easypostAccounts = null;
    /** @var EasyPostAccountVO */
    public $easypostAccountVo = null;
    /** @var EasyPostAddressVo */
    public $easypostAddressVo = null;

    public function __construct(Application $app, Request $request, ShipmentVO &$shipmentVo, PDO &$connection = null) {
        $this->requestedStepNumber = AppUtil::getIntFromRequest($request, "step_number");
        $this->parcelLength = AppUtil::getFloatFromRequest($request, "parcel_length");
        $this->parcelWidth = AppUtil::getFloatFromRequest($request, "parcel_width");
        $this->parcelHeight = AppUtil::getFloatFromRequest($request, "parcel_height");
        $this->parcelWeight = AppUtil::getFloatFromRequest($request, "parcel_weight");

        $facilityId = AppUtil::getIntFromRequest($request, "facility_id");
        $easypostAccountId = AppUtil::getIntFromRequest($request, "easypost_account_id");

        $userId = AppUtil::getUserId($app);
        $this->facilities =& UserUtil::getUserFacilities($userId, $connection, "name ASC");
        $facilityCount = ($this->facilities === null ? 0 : count($this->facilities));

        if($facilityCount === 0) {
            AppUtil::addError("user", "No shipping facilities authorized - please contact an administrator");
        } else if($facilityCount === 1) {
            $this->facilityVo =& $this->facilities[0];
        } else if($facilityId !== null) {
            foreach($this->facilities as &$availableFacilityVo) {
                if(Utils::valuesMatch($facilityId, $availableFacilityVo->getId())) {
                    $this->facilityVo = $availableFacilityVo;
                    break;
                }
            }

            unset($availableFacilityVo);
        }

        $this->easypostAccounts =& EasyPostAccountDAO::instance($connection)->findAll(null, "name ASC");
        $easypostAccountCount = ($this->easypostAccounts === null ? 0 : count($this->easypostAccounts));

        if($easypostAccountCount === 0) {
            AppUtil::addError("easypost", "No EasyPost account configured - please contact an administrator");
        } else if($easypostAccountCount === 1) {
            $this->easypostAccountVo =& $this->easypostAccounts[0];
        } else if($easypostAccountId !== null) {
            foreach($this->easypostAccounts as &$availableEasypostAccountVo) {
                if(Utils::valuesMatch($easypostAccountId, $availableEasypostAccountVo->getId())) {
                    $this->easypostAccountVo = $availableEasypostAccountVo;
                    break;
                }
            }

            unset($availableEasypostAccountVo);
        }

        $this->currentStepNumber = 1;

        if($this->requestedStepNumber === 1 || $this->facilityVo === null || $this->easypostAccountVo === null) {
            return;
        }

        $recipientOrganizationVo = ($shipmentVo->getRecipientOrganizationId() === null ? null : OrganizationDAO::instance($connection)->findById($shipmentVo->getRecipientOrganizationId()));
        $contactPersonVo = ($shipmentVo->getContactPersonId() === null ? null : ContactPersonDAO::instance($connection)->findById($shipmentVo->getContactPersonId()));
        $contactAddressVo = ($shipmentVo->getContactAddressId() === null ? null : ContactAddressDAO::instance($connection)->findById($shipmentVo->getContactAddressId()));

        $this->easypostAddressVo =& EasyPostAddressUtil::findOrCreateAddressVo($contactAddressVo, $contactPersonVo, $recipientOrganizationVo, $this->easypostAccountVo, $connection);

        if($this->easypostAddressVo !== null) {
            $this->currentStepNumber = 2;
        }

        if($this->requestedStepNumber === 2 || $this->easypostAddressVo === null) {
            return;

        }

        if($this->parcelLength === null || $this->parcelLength <= 0) {
            AppUtil::addError("parcel", "Length must be a valid, positive float");
        }

        if($this->parcelWidth === null || $this->parcelWidth <= 0) {
            AppUtil::addError("parcel", "Width must be a valid, positive float");
        }

        if($this->parcelHeight === null || $this->parcelHeight <= 0) {
            AppUtil::addError("parcel", "Height must be a valid, positive float");
        }

        if($this->parcelWeight === null || $this->parcelWeight <= 0) {
            AppUtil::addError("parcel", "Weight must be a valid, positive float");
        }

        if(Utils::isNonEmptyArray(AppUtil::getErrors())) {
            return;
        }
    }

    public function &getTemplateVariables(ShipmentVO &$shipmentVo) {
        $vars = [
            "shipment" => $shipmentVo
        ];

        if($this->parcelLength !== null) {
            $vars["parcel_length"] = $this->parcelLength;
        }

        if($this->parcelWidth !== null) {
            $vars["parcel_width"] = $this->parcelWidth;
        }

        if($this->parcelHeight !== null) {
            $vars["parcel_height"] = $this->parcelHeight;
        }

        if($this->parcelWeight !== null) {
            $vars["parcel_weight"] = $this->parcelWeight;
        }

        if($this->facilities !== null) {
            $vars["facilities"] = $this->facilities;
        }

        if($this->easypostAccounts !== null) {
            $vars["easypost_accounts"] = $this->easypostAccounts;
        }

        if($this->facilityVo !== null) {
            $vars["facility"] = $this->facilityVo;
        }

        if($this->easypostAccountVo !== null) {
            $vars["easypost_account"] = $this->easypostAccountVo;
        }

        if($this->easypostAddressVo !== null) {
            $vars["easypost_address"] = $this->easypostAddressVo;
        }

        return $vars;
    }
}
