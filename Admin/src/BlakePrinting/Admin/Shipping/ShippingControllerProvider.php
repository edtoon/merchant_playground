<?php

namespace BlakePrinting\Admin\Shipping;

use BlakePrinting\Admin\WebApplication;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;

class ShippingControllerProvider implements ControllerProviderInterface {
    public function connect(Application $app) {
        $controllers = $app["controllers_factory"];

        if($app instanceof WebApplication) {
            ShippingShipmentsController::connect($app, $controllers);

            $controllers->get("/", self::class . "::displayDefault")
                ->bind("shipping")
            ;
        }

        return $controllers;
    }

    public function displayDefault(WebApplication $app) {
        return $app["twig"]->render("shipping/shipping.html.twig", [
            "nav_section" => "shipping"
        ]);
    }
}
