<?php

namespace BlakePrinting\Admin\Profile;

use BlakePrinting\AmazonMws\EndpointDAO;
use BlakePrinting\AmazonMws\Sellers\CredentialDAO;
use BlakePrinting\AmazonMws\Sellers\CredentialVO;
use BlakePrinting\Facilities\FacilityDAO;
use BlakePrinting\Globalization\TimezoneDAO;
use BlakePrinting\Globalization\TimezoneVO;
use BlakePrinting\Printers\GatewayApiKeyDAO;
use BlakePrinting\Printers\GatewayApiKeyVO;
use BlakePrinting\Printers\GatewayDAO;
use BlakePrinting\Users\UserDAO;
use BlakePrinting\Users\UserRoleDAO;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Admin\AppUtil;
use BlakePrinting\Admin\WebApplication;
use DateTime;
use DateTimeZone;
use PDO;
use ReflectionClass;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ProfileControllerProvider implements ControllerProviderInterface {
    public function connect(Application $app) {
        $controllers = $app["controllers_factory"];

        if($app instanceof WebApplication) {
            $controllers
                ->get("/", function() use (&$app) {
                    return $app->redirect($app->path("profile_settings"));
                })
                ->bind("profile")
            ;
            $controllers->get("/settings", self::class . "::displaySettings")
                ->bind("profile_settings")
            ;
            $controllers->post("/settings_change", self::class . "::updateSettings")
                ->bind("profile_settings_update")
            ;
            $controllers->get("/auth", self::class . "::displayAuth")->bind("profile_auth");
            $controllers->post("/auth_change", self::class . "::updateAuth")
                ->bind("profile_auth_update")
            ;
            $controllers->get("/mws_credentials", self::class . "::displayMwsCredentials")
                ->bind("profile_mws_credentials")
            ;
            $controllers->post("/mws_credentials_add", self::class . "::addMwsCredential")
                ->bind("profile_mws_credentials_add")
            ;
            $controllers->post("/mws_credentials_update", self::class . "::updateMwsCredentials")
                ->bind("profile_mws_credentials_update")
            ;
            $controllers->get("/mws_credentials.json", self::class . "::jsonMwsCredentials")
                ->bind("profile_mws_credentials_json")
            ;
            $controllers->get("/printers", self::class . "::displayPrinters")
                ->bind("profile_printers")
            ;
            $controllers->get("/printers/facilities.json", self::class . "::jsonPrinterFacilities")
                ->bind("profile_printers_facilities_json")
            ;
            $controllers->get("/printers/facility.json", self::class . "::jsonFacilityPrinters")
                ->bind("profile_printers_facility_printers_json")
            ;
            $controllers->get("/printers/generate_facility_gateway_api_key", self::class . "::generateFacilityGatewayApiKey")
                ->bind("profile_printers_generate_facility_gateway_api_key")
            ;
        }

        return $controllers;
    }

    public function displaySettings(WebApplication $app) {
        $connection =& $app["merchant.mysql.connection"];
        $userDao =& UserDAO::instance($connection);
        $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
        $userVo =& $userDao->findById($userId);
        $timezoneDao =& TimezoneDAO::instance($connection);
        $timezones =& $timezoneDao->findAll(null, "name ASC");

        return $app["twig"]->render("profile/profile_settings.html.twig", [
            "nav_section" => "profile",
            "side_section" => "settings",
            "user" => $userVo,
            "timezones" => $timezones
        ]);
    }

    public function updateSettings(WebApplication $app, Request $request) {
        $timezoneId = AppUtil::getIntFromRequest($request, "timezone_id");
        $connection =& $app["merchant.mysql.connection"];
        $userDao =& UserDAO::instance($connection);
        $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
        $userVo =& $userDao->findById($userId, "FOR UPDATE");

        if($userVo->getTimezoneId() !== $timezoneId) {
            $userVo->setTimezoneId($timezoneId);

            if($userDao->update($userVo) > 0) {
                $app["session"]->getFlashBag()->add("success", "Updated settings.");
            }
        }

        return $app->redirect($app->path("profile_settings"));
    }

    public function displayAuth(WebApplication $app) {
        return $app["twig"]->render("profile/profile_auth.html.twig", [
            "nav_section" => "profile",
            "side_section" => "auth"
        ]);
    }

    public function updateAuth(WebApplication $app, Request $request) {
        $change = $request->request->get("_change");
        $confirm = $request->request->get("_confirm");

        if(!empty($change) && !empty($confirm) && $change == $confirm) {
            $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
            $connection =& $app["merchant.mysql.connection"];
            $userDao =& UserDAO::instance($connection);

            $userDao->setPassword($userId, $change);

            $app["session"]->getFlashBag()->add("success", "Updated password.");

            return $app->redirect($app->path("profile_auth"));
        } else {
            $app["session"]->getFlashBag()->add("danger", "Passwords were empty or did not match.");

            return $app->redirect($app->path("profile_auth"));
        }
    }

    public function displayMwsCredentials(WebApplication $app) {
        if(!$app->isGranted(UserRoleDAO::ROLE_ADMIN)) {
            throw new AccessDeniedException();
        }

        $endpointDao =& EndpointDAO::instance($app["merchant.mysql.connection"]);
        $endpoints =& $endpointDao->findAll(null, "id ASC");

        return $app["twig"]->render("profile/profile_mws_credentials.html.twig", [
            "nav_section" => "profile",
            "side_section" => "mws_credentials",
            "endpoints" => $endpoints
        ]);
    }

    public function addMwsCredential(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_ADMIN)) {
            throw new AccessDeniedException();
        }

        $connection =& $app["merchant.mysql.connection"];
        $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
        $endpointId = AppUtil::getIntFromRequest($request, "_endpoint_id");
        $awsAccessKeyId = trim($request->request->get("_access_key"));
        $awsSecretAccessKey = trim($request->request->get("_secret_access_key"));
        $sellerId = trim($request->request->get("_seller_id"));
        $credentialVo = new CredentialVO();

        $credentialVo->setEndpointId($endpointId);
        $credentialVo->setAwsAccessKeyId($awsAccessKeyId);
        $credentialVo->setAwsSecretAccessKey($awsSecretAccessKey);
        $credentialVo->setSellerId($sellerId);
        $credentialVo->setCreator($userId);

        AppUtil::validateAndLogErrors($app, $credentialVo);

        $errorResponse = AppUtil::getErrorResponse($app);

        if($errorResponse !== null) {
            return $errorResponse;
        }

        $credentialDao =& CredentialDAO::instance($connection);

        $credentialDao->insert($credentialVo);

        return $app->json(["success" => true]);
    }

    public function updateMwsCredentials(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_ADMIN)) {
            throw new AccessDeniedException();
        }

        $count = AppUtil::getIntFromRequest($request, "count");

        if($count === null || $count < 1) {
            return $app->json(["success" => false, "error" => "No count provided"], Response::HTTP_BAD_REQUEST);
        }

        $connection =& $app["merchant.mysql.connection"];
        $userId = $app["security.token_storage"]->getToken()->getUser()->getUserId();
        $endpointDao =& EndpointDAO::instance($connection);
        $credentialDao =& CredentialDAO::instance($connection);
        $credentials = [];

        for($i = 0; $i < $count; $i++) {
            $id = AppUtil::getBootstrapTableParam($request, "id" . $i);
            $endpoint = AppUtil::getBootstrapTableParam($request, "endpoint" . $i);
            $awsAccessKeyId = AppUtil::getBootstrapTableParam($request, "aws_access_key_id" . $i);
            $awsSecretAccessKey = AppUtil::getBootstrapTableParam($request, "aws_secret_access_key" . $i);
            $sellerId = AppUtil::getBootstrapTableParam($request, "seller_id" . $i);

            $endpointVo =& $endpointDao->findByColumn("endpoint", $endpoint, PDO::PARAM_STR, null, null, null, true);
            $credentialVo = new CredentialVO();

            $credentialVo->setId($id);
            $credentialVo->setUpdater($userId);
            $credentialVo->setEndpointId($endpointVo->getId());
            $credentialVo->setAwsAccessKeyId($awsAccessKeyId);
            $credentialVo->setAwsSecretAccessKey($awsSecretAccessKey);
            $credentialVo->setSellerId($sellerId);

            AppUtil::validateAndLogErrors($app, $credentialVo, $i);

            $credentials[] =& $credentialVo;
        }

        unset($credentialVo);

        $errorResponse = AppUtil::getErrorResponse($app);

        if($errorResponse !== null) {
            return $errorResponse;
        }

        foreach($credentials as &$credentialVo) {
            $credentialDao->update($credentialVo);
        }

        unset($credentialVo);

        return $app->json(["success" => true, "count" => $count]);
    }

    public function jsonMwsCredentials(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_ADMIN)) {
            throw new AccessDeniedException();
        }

        $sort = $request->query->get("sort") ?: "country_code";

        if(!in_array($sort, ["id", "endpoint", "aws_access_key_id", "aws_secret_access_key", "seller_id", "updated"])) {
            $app->abort(Response::HTTP_BAD_REQUEST, "Invalid sort parameter");
        }

        $order = AppUtil::getOrderFromQuery($request, "order", "asc");
        $limit = AppUtil::getIntFromQuery($request, "limit", 20);
        $offset = AppUtil::getIntFromQuery($request, "offset", 0);
        $connection =& $app["merchant.mysql.connection"];
        $credentialDao =& CredentialDAO::instance($connection);
        $endpointDao =& EndpointDAO::instance($connection);
        /** @var TimezoneVO $timezoneVo */
        $timezoneVo = $app["twig"]->getGlobals()["timezone"];
        $dateTimeZone = new DateTimeZone($timezoneVo->getName());
        $configs =& PDOQuery::select()
            ->columns(
                "config.id",
                "endpt.endpoint",
                "config.aws_access_key_id",
                "config.aws_secret_access_key",
                "config.seller_id",
                "COALESCE(config.updated, config.created) AS updated"
            )
            ->from(
                $credentialDao->formatSchemaAndTableName() . " config",
                $endpointDao->formatSchemaAndTableName() . " endpt"
            )
            ->where("config.endpoint_id", "endpt.id")
            ->orderBy($sort . " " . $order)
            ->limit($offset . ", " . $limit)
            ->getResults($connection)
        ;

        if($configs !== null) {
            foreach($configs as &$config) {
                $updatedTime = new DateTime("@" . $config["updated"]);

                $updatedTime->setTimezone($dateTimeZone);

                $config["updated"] = $updatedTime->format("Y-m-d H:i:s");
            }

            unset($config);
        }

        $results = ["total" => count($configs)];

        $results["rows"] =& $configs;

        return $app->json($results);
    }

    public function displayPrinters(WebApplication $app) {
        if(!$app->isGranted(UserRoleDAO::ROLE_INVENTORY)) {
            throw new AccessDeniedException();
        }

        return $app["twig"]->render("profile/profile_printers.html.twig", [
            "nav_section" => "profile",
            "side_section" => "printers"
        ]);
    }

    public function jsonPrinterFacilities(WebApplication $app) {
        if(!$app->isGranted(UserRoleDAO::ROLE_INVENTORY)) {
            throw new AccessDeniedException();
        }

        $username = $app["security.token_storage"]->getToken()->getUsername();
        $connection =& $app["merchant.mysql.connection"];
        $facilityDao =& FacilityDAO::instance($connection);
        $facilities =& $facilityDao->findAll(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$connection, &$username) {
                $pdoQuery
                    ->columns("facility.*")
                    ->join("facility.facility_organization", ["facility.id = facility_organization.facility_id"])
                    ->join("organization.organization", ["facility_organization.organization_id = organization.id"])
                    ->join("user.user_organization", ["organization.id = user_organization.organization_id"])
                    ->join("user.user", ["user.id = user_organization.user_id"])
                    ->where("user.name", $username, PDO::PARAM_STR)
                ;
            },
            "facility.name ASC, facility.code ASC"
        );

        $results = ["total" => count($facilities)];
        $results["rows"] =& $facilities;

        return $app->json($results);
    }

    public function generateFacilityGatewayApiKey(WebApplication $app, Request $request) {
        $facilityId = AppUtil::getIntFromQuery($request, "facility_id");
        $apiKeys = [];

        if($facilityId === null) {
            AppUtil::addError("facility_id", "Not specified");
        } else {
            $connection =& $app["merchant.mysql.connection"];
            $facilityDao =& FacilityDAO::instance($connection);
            $facility =& $facilityDao->findById($facilityId);

            if($facility === null) {
                AppUtil::addError("facility_id", "Not found");
            } else {
                $gatewayDao =& GatewayDAO::instance($connection);
                $gatewayVo =& $gatewayDao->findByColumn("facility_id", $facilityId, PDO::PARAM_INT);

                if($gatewayVo === null) {
                    AppUtil::addError("facility_id", "No valid gateway");
                } else {
                    $gatewayId = $gatewayVo->getId();
                    $apiKey = hash("sha256",
                        "" . microtime(true) . "_" .
                        $gatewayId . "_" .
                        $facilityId . "_" .
                        rand(0, getrandmax()) . "_" .
                        $app["security.token_storage"]->getToken()->getUsername()
                    );
                    $gatewayApiKeyDao =& GatewayApiKeyDAO::instance($connection);

                    $gatewayApiKeyDao->findOrCreate(new GatewayApiKeyVO($gatewayId, $apiKey));

                    $apiKeys[] = [
                        "facility_id" => $facilityId,
                        "facility_name" => $facility->getName(),
                        "facility_code" => $facility->getCode(),
                        "api_key" => $apiKey
                    ];
                }
            }
        }

        $errorResponse = AppUtil::getErrorResponse($app);

        if($errorResponse !== null) {
            return $errorResponse;
        }

        $results = ["total" => count($apiKeys)];
        $results["rows"] =& $apiKeys;

        return $app->json($results);
    }

    public function jsonFacilityPrinters(WebApplication $app, Request $request) {
        if(!$app->isGranted(UserRoleDAO::ROLE_INVENTORY)) {
            throw new AccessDeniedException();
        }

        $facilityId = AppUtil::getIntFromQuery($request, "facility_id");
        $printers = [];

        if($facilityId !== null) {
            $connection =& $app["merchant.mysql.connection"];
            $printers =& PDOQuery::select()
                ->distinct()
                ->columns("printer.*", "manufacturer_organization.name AS manufacturer", "printer_status.status_description", "printer_status.ready", "printer_status.updated AS status_updated")
                ->from("printer.printer")
                ->join("printer.printer_status", ["printer.id = printer_status.printer_id"])
                ->join("location.location", ["printer_status.location_id = location.id"])
                ->join("facility.facility_location", ["location.id = facility_location.location_id"])
                ->join("organization.organization AS manufacturer_organization", ["printer.manufacturer_organization_id = manufacturer_organization.id"])
                ->where("facility_location.facility_id", $facilityId, PDO::PARAM_INT)
                ->orderBy("printer.product_name ASC")
                ->getResults($connection)
            ;
        }

        $results = ["total" => count($printers)];
        $results["rows"] =& $printers;

        return $app->json($results);
    }
}
