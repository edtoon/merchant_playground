<?php

use BlakePrinting\Catalog\CatalogPartDAO;
use BlakePrinting\Catalog\CatalogProductDAO;
use BlakePrinting\Admin\Catalog\CatalogSelectionPartDAO;
use BlakePrinting\Admin\Catalog\CatalogSelectionProductDAO;
use BlakePrinting\Admin\Catalog\CatalogSelectionSessionDAO;
use BlakePrinting\Db\DbMigration;
use BlakePrinting\Users\UserDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class CatalogSelectionSession extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $catalogSelectionSessionDao = new CatalogSelectionSessionDAO($connection);
        $userDao = new UserDAO($connection);
        $this->tableDao($catalogSelectionSessionDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("user_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addForeignKey("user_id", $userDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("user_id", ["unique" => true])
            ->save()
        ;

        $catalogSelectionPartDao = new CatalogSelectionPartDAO($connection);
        $catalogPartDao = new CatalogPartDAO($connection);
        $catalogSelectionSessionTbl = $catalogSelectionSessionDao->formatSchemaAndTableName();
        $this->tableDao($catalogSelectionPartDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["catalog_selection_session_id", "part_id"]])
            ->addColumn("catalog_selection_session_id", "biginteger", ["signed" => false])
            ->addColumn("part_id", "biginteger", ["signed" => false])
            ->addColumn("quantity", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addForeignKey("catalog_selection_session_id", $catalogSelectionSessionTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("part_id", $catalogPartDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("quantity")
            ->save()
        ;

        $catalogSelectionProductDao = new CatalogSelectionProductDAO($connection);
        $catalogProductDao = new CatalogProductDAO($connection);
        $this->tableDao($catalogSelectionProductDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["catalog_selection_session_id", "product_id"]])
            ->addColumn("catalog_selection_session_id", "biginteger", ["signed" => false])
            ->addColumn("product_id", "biginteger", ["signed" => false])
            ->addColumn("quantity", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addForeignKey("catalog_selection_session_id", $catalogSelectionSessionTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("product_id", $catalogProductDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("quantity")
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(new CatalogProductDAO($connection));
        $this->dropDao(new CatalogPartDAO($connection));
        $this->dropDao(new CatalogSelectionSessionDAO($connection));
    }
}