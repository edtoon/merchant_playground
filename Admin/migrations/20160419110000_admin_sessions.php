<?php

use BlakePrinting\Admin\Sessions\AdminSessionEventDAO;
use BlakePrinting\Admin\Sessions\AdminSessionEventLogDAO;
use BlakePrinting\Db\DbMigration;
use BlakePrinting\Users\UserDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class AdminSessions extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $adminSessionEventDao =& AdminSessionEventDAO::instance($connection);
        $this->tableDao($adminSessionEventDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $adminSessionEventLogDao =& AdminSessionEventLogDAO::instance($connection);
        $userDao =& UserDAO::instance($connection);
        $this->tableDao($adminSessionEventLogDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("user_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("admin_session_event_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("occurred", "biginteger", ["signed" => false])
            ->addForeignKey("user_id", $userDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("admin_session_event_id", $adminSessionEventDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("occurred")
            ->save()
        ;
        $this->alterDao($adminSessionEventLogDao, "ADD ipv4 BINARY(4), ADD KEY (ipv4)");
        $this->alterDao($adminSessionEventLogDao, "ADD ipv6 BINARY(16), ADD KEY (ipv6)");
        $this->alterDao($adminSessionEventLogDao, "ADD admin_session_id VARBINARY(128), ADD KEY (admin_session_id)");
        $this->alterDao($adminSessionEventLogDao, "ADD UNIQUE (user_id, admin_session_event_id, occurred, ipv4, ipv6, admin_session_id)");
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(AdminSessionEventLogDAO::instance($connection));
        $this->dropDao(AdminSessionEventDAO::instance($connection));
    }
}
