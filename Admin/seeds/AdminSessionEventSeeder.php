<?php

use BlakePrinting\Admin\Sessions\AdminSessionEventDAO;
use BlakePrinting\Db\DbSeed;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Util\LogUtil;
use Monolog\Logger;

class AdminSessionEventSeeder extends DbSeed {
    public function run() {
        $connection =& $this->getConnection();

        try {
            $adminSessionEventDao = new AdminSessionEventDAO($connection);

            foreach(['login', 'logout', 'lock', 'unlock', 'enable', 'disable'] as $eventType) {
                PDOQuery::insert()->ignore()->into($adminSessionEventDao)->value("name", $eventType, PDO::PARAM_STR)->executeGetRowCount($connection);
            }

            PDOUtil::commitConnection($connection);
        } catch(Exception $e) {
            LogUtil::logException(Logger::ERROR, $e, "Error adding admin session event types");
        }

        PDOUtil::rollbackConnection($connection);
    }
}
