function addTextOrNbsp(element, text) {
    if(text !== null) {
        element.text(text);
    } else {
        element.html('&nbsp;');
    }
}

function addSeparatedTextOrNbsp(element, array, separator) {
    if(array !== null && array.length > 0) {
        if(typeof separator === 'undefined') {
            separator = ', ';
        }

        element.text(array.join(separator));
    } else {
        element.html('&nbsp;');
    }
}

/**
 * @param {!number} itemCount
 * @param {number=} currentPage
 * @param {number=} itemsPerPage
 * @param {function=} onPageChange
 * @returns {jQuery}
 */
function createPaginationNav(itemCount, currentPage, itemsPerPage, onPageChange) {
    var paginationData = getPaginationData(itemCount, currentPage, itemsPerPage);

    if(paginationData.pageCount < 2) {
        return null;
    }

    var $paginationNav = $('<nav/>');
    var $paginationList = $('<ul class="pagination" style="margin: 0;"/>').appendTo($paginationNav);

    for(i = 1; i <= paginationData.endPage; i++) {
        if(i === 1) {
            createPaginationElement(currentPage, paginationData, i, 'first', onPageChange).appendTo($paginationList);
            createPaginationElement(currentPage, paginationData, i, 'previous', onPageChange).appendTo($paginationList);
        }

        if(i >= paginationData.startPage && i <= paginationData.endPage) {
            createPaginationElement(currentPage, paginationData, i, i, onPageChange).appendTo($paginationList);
        }

        if(i === paginationData.endPage) {
            createPaginationElement(currentPage, paginationData, i, 'next', onPageChange).appendTo($paginationList);
            createPaginationElement(currentPage, paginationData, i, 'last', onPageChange).appendTo($paginationList);
        }
    }

    return $paginationNav;
}

/**
 * @param {!number} currentPage
 * @param {!object} paginationData
 * @param {!number} elementPage
 * @param {!string} elementType
 * @param {function=} onPageChange
 * @returns {jQuery}
 */
function createPaginationElement(currentPage, paginationData, elementPage, elementType, onPageChange) {
    var $li = $('<li>');
    var clickPage = null;
    var label = elementType;

    switch(elementType) {
        case 'first':
            label = 'First';

            if(currentPage === 1) {
                $li.addClass('disabled');
            } else {
                clickPage = 1;
            }
        break;
        case 'previous':
            label = 'Previous';

            if(currentPage === 1) {
                $li.addClass('disabled');
            } else {
                clickPage = currentPage - 1;
            }
        break;
        case 'next':
            label = 'Next';

            if(currentPage === paginationData.endPage) {
                $li.addClass('disabled');
            } else {
                clickPage = currentPage + 1;
            }
        break;
        case 'last':
            label = 'Last';

            if(currentPage === paginationData.pageCount) {
                $li.addClass('disabled');
            } else {
                clickPage = paginationData.pageCount;
            }
        break;
        default:
            if(currentPage === elementPage) {
                $li.addClass('active');
            } else {
                clickPage = elementPage;
            }
        break;
    }

    var $a = $('<a/>')
        .attr('href', '#')
        .attr('aria-label', ($.isNumeric(label) ? ('Page ' + label) : label))
        .text(label)
        .appendTo($li)
        .click(function(e) {
            e.preventDefault();

            if(clickPage !== null && onPageChange) {
                onPageChange(clickPage);
            }
        })
    ;

    return $li;
}

/**
 * http://jasonwatmore.com/post/2016/01/31/AngularJS-Pagination-Example-with-Logic-like-Google.aspx
 *
 * @param {!number} itemCount
 * @param {number=} currentPage
 * @param {number=} itemsPerPage
 * @returns {{currentPage: number, itemCount: number, itemsPerPage: number, pageCount: number, startPage: number, endPage: number}}
 */
function getPaginationData(itemCount, currentPage, itemsPerPage) {
    currentPage = currentPage || 1;
    itemsPerPage = itemsPerPage || 10;

    var pageCount = Math.ceil(itemCount / itemsPerPage);
    var startPage = 1;
    var endPage = pageCount;

    if(pageCount > 10) {
        if(currentPage <= 6) {
            endPage = 10;
        } else if(pageCount <= (currentPage + 4)) {
            startPage = pageCount - 9;
        } else {
            startPage = currentPage - 5;
            endPage = currentPage + 4;
        }
    }

    return {
        currentPage: currentPage,
        itemCount: itemCount,
        itemsPerPage: itemsPerPage,
        pageCount: pageCount,
        startPage: startPage,
        endPage: endPage
    };
}

/**
 * @param {!string} id
 * @param {!string} heading
 * @param {!string} text
 * @param {string=} confirmText
 * @param {string=} size
 * @param {string=} target
 * @param {function=} onClose
 */
function messageDialog(id, heading, text, confirmText, size, target, onClose) {
    confirmText = (confirmText ? confirmText : "Close");

    var $modal = $('<div class="modal fade" id="' + id + '" tabindex="-1" role="dialog" aria-labelledby="' + id + 'Label"></div>');
    var $modalDialog = $('<div class="modal-dialog' + (size ? (' modal-' + size) : '') + '" role="document">/').appendTo($modal);
    var $modalContent = $('<div class="modal-content"/>').appendTo($modalDialog);
    var $modalHeader = $(
        '<div class="modal-header">' +
        '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
        '</div>'
    ).appendTo($modalContent);

    $('<h4 class="modal-title" id="' + id + 'Label"/>').html(heading).appendTo($modalHeader);
    $('<div class="modal-body"/>').html(text).appendTo($modalContent);
    $(
        '<div class="modal-footer">' +
        '<button type="button" class="btn btn-default" data-dismiss="modal">' + confirmText + '</button>' +
        '</div>'
    ).appendTo($modalContent);

    $modal
        .appendTo(typeof target === 'undefined' ? $(document.body) : target)
        .on('hidden.bs.modal', function(e) {
            $(this).remove();

            if(typeof onClose === 'function') {
                onClose();
            }
        })
        .modal('show')
    ;
}

/**
 * @param {!string} text
 * @param {string=} level
 * @param {boolean=} dismissible
 * @param {string=} target
 * @param {int=} fadeInSecs
 */
function createAlert(text, level, dismissible, target, fadeInSecs) {
    var $alert = $('<div/>').addClass('alert').attr('role', 'alert');

    level = (level ? level : 'info');
    dismissible = (typeof dismissible === 'undefined' ? true : dismissible);
    target = (target ? target : '#alerts');

    if(dismissible) {
        $alert.addClass('alert-dismissible');

        $alert.html('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + text);
    } else {
        $alert.html(text);
    }

    $alert.addClass('alert-' + level).appendTo(target);

    if(fadeInSecs) {
        window.setTimeout(function() {
            $alert.fadeTo(1000, 0).slideUp(500, function(){
                $alert.remove();
            });
        }, (fadeInSecs * 1000));
    }
}

/**
 * @param {jqXHR} $ajax
 * @param {function=} onFailure
 * @param {boolean=} dismissible
 * @param {string=} target
 * @param {int=} fadeInSecs
 */
function wrapAjax($ajax, onFailure, dismissible, target, fadeInSecs) {
    $ajax.fail(function(jqXHR, textStatus, errorThrown) {
        if(jqXHR.responseText) {
            try {
                var response = $.parseJSON(jqXHR.responseText);

                if(response.hasOwnProperty('errors')) {
                    var allErrors = response['errors'];

                    for(var errorCategory in allErrors) {
                        if(allErrors.hasOwnProperty(errorCategory)) {
                            var categoryErrors = allErrors[errorCategory];
                            var errorText = 'Error' + (categoryErrors.length > 1 ? 's' : '') + ' for field: ' + errorCategory + ' - ';
                            var first = true;

                            for(var errorKey in categoryErrors) {
                                if(categoryErrors.hasOwnProperty(errorKey)) {
                                    errorText += (first ? '' : ', ') + '&quot;' + categoryErrors[errorKey] + '&quot;';
                                }

                                first = false;
                            }

                            createAlert(errorText, 'danger', dismissible, target, fadeInSecs);
                        }
                    }
                } else if(response.hasOwnProperty('error')) {
                    createAlert(response['error'], 'danger', dismissible, target, fadeInSecs);
                } else {
                    createAlert('Request failed - status: ' + textStatus, 'danger', dismissible, target, fadeInSecs);

                    if(console) {
                        console.log('Request failed - status: ' + textStatus + ', response: ' + jqXHR.responseText);
                    }
                }
            } catch(e) {
                createAlert('Request failed - status: ' + textStatus, 'danger', dismissible, target, fadeInSecs);

                if(console) {
                    console.log(e);
                    console.log('Received status: ' + textStatus + ', response: ' + jqXHR.responseText);
                }
            }
        } else {
            createAlert('Request failed - status: ' + textStatus, 'danger', dismissible, target, fadeInSecs);
        }

        if(onFailure) {
            onFailure(jqXHR, textStatus, errorThrown);
        }
    });
}

/**
 * @param {!string} str
 * @return {int}
 */
function moneyStringToCents(str) {
    var periodIdx = str.indexOf('.');

    if(periodIdx === -1) {
        str += '.0000';
    } else {
        while(str.length <= (periodIdx + 4)) {
            str += '0';
        }
    }

    return parseInt(str.replace('.', ''));
}

/**
 * @param {!int} cents
 * @return {string}
 */
function moneyCentsToString(cents) {
    var centsStr = cents.toString();
    var outputStr = null;

    if(centsStr.length < 5) {
        outputStr = '0.' + centsStr;
    } else {
        outputStr = centsStr.substring(0, centsStr.length - 4) + '.' + centsStr.substring(centsStr.length - 4);
    }

    var periodIdx = outputStr.indexOf('.');

    while(outputStr.length <= (periodIdx + 4)) {
        outputStr += '0';
    }

    return outputStr;
}

/**
 * @param {!int} unixtime
 * @param {!string} timezone
 * @param {boolean=} nbsp
 * @return {string}
 */
function timestampFormat(unixtime, timezone, nbsp) {
    if(unixtime === null || unixtime.length < 1) {
        return ((typeof nbsp === 'undefined' ? true : nbsp) ? '&nbsp;' : '');
    }

    timezone = ((typeof timezone === 'undefined' || timezone === null) ? 'America/Los_Angeles' : timezone);

    return moment.unix(unixtime).tz(timezone).format('YYYY-MM-DD HH:mm:ss');
}
