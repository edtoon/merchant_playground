function easypostDisplayModal(result) {
    if(result['html']) {
        $(result['html'])
            .appendTo($(document.body))
            .on('hidden.bs.modal', function(e) {
                $(this).remove();
            })
            .modal('show')
        ;
    }
}

function easypostConvertHiddenVarsToPostData() {
    var $vars = $('#easypostModalVars');
    var postData = {};

    $vars.children(['input[type=hidden']).each(function() {
        var $input = $(this);
        var name = $input.attr('name');
        var value = $input.val();

        if(value !== undefined) {
            postData[name] = value;
        }
    });

    return postData;
}

function easypostCompleteStep1(shipmentId) {
    var $alerts = $('#easypostModalAlerts');
    var $button = $('#easypostModalButton');
    var $body = $('#easypostModalBody');
    var $footer = $('#easypostModalFooter');
    var $vars = $('#easypostModalVars');
    var $selectFacility = $('#easypostModalSelectFacility');
    var $selectAccount = $('#easypostModalSelectAccount');
    var postData = easypostConvertHiddenVarsToPostData();

    postData['step_number'] = 2;

    $alerts.html('');

    if($selectFacility.length > 0) {
        var facilityId = $selectFacility.val();

        if(facilityId === undefined || facilityId === null) {
            createAlert('Please select a facility', 'danger', true, $alerts);
        } else {
            postData['facility_id'] = facilityId;
        }
    }

    if($selectAccount.length > 0) {
        var accountId = $selectAccount.val();

        if(accountId === undefined || accountId === null) {
            createAlert('Please select an account', 'danger', true, $alerts);
        } else {
            postData['easypost_account_id'] = accountId;
        }
    }

    if('facility_id' in postData && 'easypost_account_id' in postData) {
        $button.prop('disabled', true);

        wrapAjax(
            $.ajax({
                'type': 'POST',
                'url': shipmentId + '/_easypost',
                'data': postData
            })
            .done(function(result, textStatus, jqXHR) {
                if('body' in result) {
                    $body.html(result['body']);
                } else {
                    $body.html('');
                }

                if('footer' in result) {
                    $footer.html(result['footer']);
                } else {
                    $footer.html('');
                }

                if('vars' in result) {
                    $vars.html(result['vars']);
                } else {
                    $vars.html('');
                }
            }),
            function() {
                $button.prop('disabled', false);
            }, true, '#easypostModalAlerts'
        );
    }
}

function easypostCompleteStep2(shipmentId) {
    var $alerts = $('#easypostModalAlerts');
    var $button = $('#easypostModalButton');
    var $body = $('#easypostModalBody');
    var $footer = $('#easypostModalFooter');
    var $vars = $('#easypostModalVars');
    var length = $('#inputParcelLength').val();
    var width = $('#inputParcelWidth').val();
    var height = $('#inputParcelHeight').val();
    var weight = $('#inputParcelWeight').val();
    var postData = easypostConvertHiddenVarsToPostData();

    postData['step_number'] = 3;

    $alerts.html('');

    if(!length) {
        createAlert('Please enter a length', 'danger', true, $alerts);
    } else {
        postData['parcel_length'] = length;
    }

    if(!width) {
        createAlert('Please enter a width', 'danger', true, $alerts);
    } else {
        postData['parcel_width'] = width;
    }

    if(!height) {
        createAlert('Please enter a height', 'danger', true, $alerts);
    } else {
        postData['parcel_height'] = height;
    }

    if(!weight) {
        createAlert('Please enter a weight', 'danger', true, $alerts);
    } else {
        postData['parcel_weight'] = weight;
    }

    if(length && width && height && weight) {
        $button.prop('disabled', true);

        wrapAjax(
            $.ajax({
                'type': 'POST',
                'url': shipmentId + '/_easypost',
                'data': postData
            })
            .done(function(result, textStatus, jqXHR) {
                if('body' in result) {
                    $body.html(result['body']);
                } else {
                    $body.html('');
                }

                if('footer' in result) {
                    $footer.html('This was a parcel.');
                } else {
                    $footer.html('');
                }

                if('vars' in result) {
                    $vars.html(result['vars']);
                } else {
                    $vars.html('');
                }
            }),
            function() {
                $button.prop('disabled', false);
            }, true, '#easypostModalAlerts'
        );
    }
}
