<?php

require_once(is_dir(__DIR__ . "/../../vendor") ? (__DIR__ . "/../../vendor/autoload.php") : "/app/vendor/autoload.php");

use BlakePrinting\Admin\WebApplication;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;

$_indexLog =& LogUtil::getLogger();
$_indexLog->debug("_GET: " . Utils::getVarDump($_GET));
$_indexLog->debug("_POST: " . Utils::getVarDump($_POST));

$app = new WebApplication();

$app->run();
