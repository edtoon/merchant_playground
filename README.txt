--------------------------------------------------------------------------------
| Mac OS X Printer Gateway                                                     |
--------------------------------------------------------------------------------

To install the Printer Gateway on a Mac Mini run the following command:

  ./bin/install_gateway.sh <api_host> <api_key>

Gateway logs will be available in /var/log/blake/

Add the following line to your ~/.bash_profile if you want to use Homebrew's PHP
in your Terminal instead of the system version:

  export PATH="$(brew --prefix php56)/bin:$PATH"


--------------------------------------------------------------------------------
| Development workflow                                                         |
--------------------------------------------------------------------------------

Everything runs within Docker containers - there is no PHP interpreter installed
directly on the host (and if one exists it is not used.) On the first run you
will need to install Docker - either the Docker Toolbox on Windows or Mac which
is available at https://www.docker.com/products/docker-toolbox or on a Linux by
running the following command:

  curl -sSL https://get.docker.com/ | sh

Once Docker is installed you will need to create all Docker images and pull all
Composer dependencies by running:

   ./build.sh

Containers will be started for Apache and Memcached in all environments, as well
as MySQL in the development environment. If you would like to manually run these
containers at a later date you may do so by using the following commands:

  ./bin/run_memcached.sh
  ./bin/run_apache.sh
  ./bin/run_mysql.sh      # in development environments only

To perform some basic unit tests:

  ./test.sh


--------------------------------------------------------------------------------
| Production workflow                                                          |
--------------------------------------------------------------------------------

To execute scheduled jobs such as Amazon order processing run:

  APP_NAME="Admin" /mnt/blake/merchant/src/bin/run_php.sh -s

To stop the execution of scheduled jobs run:

  docker stop merchant_supervisor_admin
