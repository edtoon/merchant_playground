<?php

namespace BlakePrinting\AmazonMws;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static TaskResponseDAO instance(PDO $connection = null, $schemaName = null)
 * @method TaskResponseVO findById($id, $options = null, $useCache = false)
 * @method TaskResponseVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskResponseVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskResponseVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaskResponseVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskResponseVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskResponseVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class TaskResponseDAO extends AbstractDataAccessObject {
}
