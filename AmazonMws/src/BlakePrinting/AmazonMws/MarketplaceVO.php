<?php

namespace BlakePrinting\AmazonMws;

use BlakePrinting\Db\ValueObject;
use JsonSerializable;

class MarketplaceVO implements JsonSerializable, ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $regionId = null;
    /** @var int */
    private $endpointId = null;
    /** @var string */
    private $countryCode = null;
    /** @var string */
    private $mwsMarketplaceId = null;

    public function jsonSerialize() {
        return [
            "id" => $this->id,
            "created" => $this->created,
            "updated" => $this->updated,
            "region_id" => $this->regionId,
            "endpoint_id" => $this->endpointId,
            "country_code" => $this->countryCode,
            "mws_marketplace_id" => $this->mwsMarketplaceId
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getRegionId() {
        return $this->regionId;
    }

    /**
     * @param int $regionId
     */
    public function setRegionId($regionId) {
        $this->regionId = $regionId;
    }

    /**
     * @return int
     */
    public function getEndpointId()
    {
        return $this->endpointId;
    }

    /**
     * @param int $endpointId
     */
    public function setEndpointId($endpointId)
    {
        $this->endpointId = $endpointId;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @param string $countryCode
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
    }

    /**
     * @return string
     */
    public function getMwsMarketplaceId()
    {
        return $this->mwsMarketplaceId;
    }

    /**
     * @param string $mwsMarketplaceId
     */
    public function setMwsMarketplaceId($mwsMarketplaceId)
    {
        $this->mwsMarketplaceId = $mwsMarketplaceId;
    }
}
