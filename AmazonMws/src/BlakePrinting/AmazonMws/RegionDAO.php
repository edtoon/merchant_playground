<?php

namespace BlakePrinting\AmazonMws;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static RegionDAO instance(PDO $connection = null, $schemaName = null)
 * @method RegionVO findById($id, $options = null, $useCache = false)
 * @method RegionVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method RegionVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method RegionVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method RegionVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method RegionVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method RegionVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class RegionDAO extends AbstractDataAccessObject {
    const REGION_NORTH_AMERICA = "NA";
    const REGION_EUROPE = "EU";
    const REGION_FAR_EAST = "FE";
    const REGION_CHINA = "CN";
}
