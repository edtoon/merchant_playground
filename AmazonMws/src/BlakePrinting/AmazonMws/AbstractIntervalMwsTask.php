<?php

namespace BlakePrinting\AmazonMws;

use BlakePrinting\AmazonMws\Sellers\CredentialVO;
use BlakePrinting\Db\PDOQuery;
use PDO;
use ReflectionClass;

abstract class AbstractIntervalMwsTask extends AbstractMwsTask {
    /** @var int */
    private $intervalSeconds = null;

    /**
     * @param int $intervalSeconds
     * @param CredentialVO $credentialVo
     * @param string $taskName
     * @param string $appName
     * @param string $appVersion
     * @param int $lockExpirationSeconds
     * @param PDO $connection
     */
    public function __construct($intervalSeconds, CredentialVO $credentialVo, $taskName, $appName, $appVersion, $lockExpirationSeconds, PDO $connection) {
        parent::__construct($credentialVo, $taskName, $appName, $appVersion, $lockExpirationSeconds, $connection);

        $this->intervalSeconds = $intervalSeconds;
    }

    /**
     * @inheritdoc
     */
    public function evaluate() {
        $connection =& $this->getConnection();
        $taskRequestDao =& TaskRequestDAO::instance($connection);
        $taskRequestVo =& $taskRequestDao->findSingle(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) {
                $pdoQuery
                    ->where("task_id", $this->getTaskVo()->getId(), PDO::PARAM_INT)
                    ->where("credential_id", $this->getCredentialVo()->getId(), PDO::PARAM_INT)
                    ->where("created", "UNIX_TIMESTAMP() - " . $this->getIntervalSeconds(), null, ">")
                ;
            },
            null, 1
        );

        if($taskRequestVo !== null) {
            $taskResponseDao =& TaskResponseDAO::instance($connection);
            $taskResponseVo =& $taskResponseDao->findByColumn("task_request_id", $taskRequestVo->getId(), PDO::PARAM_INT);

            if($taskResponseVo !== null) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return int
     */
    public function getIntervalSeconds()
    {
        return $this->intervalSeconds;
    }
}
