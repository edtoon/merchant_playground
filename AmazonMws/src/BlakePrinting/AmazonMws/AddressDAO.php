<?php

namespace BlakePrinting\AmazonMws;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static AddressDAO instance(PDO $connection = null, $schemaName = null)
 * @method AddressVO findById($id, $options = null, $useCache = false)
 * @method AddressVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method AddressVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method AddressVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method AddressVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method AddressVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method AddressVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class AddressDAO extends AbstractDataAccessObject {
    public static function createSha256(AddressVO &$addressVo) {
        return hash("sha256",
            "_|" . $addressVo->getName() .
            "|" . $addressVo->getLine1() .
            "|" . $addressVo->getLine2() .
            "|" . $addressVo->getLine3() .
            "|" . $addressVo->getCity() .
            "|" . $addressVo->getDistrict() .
            "|" . $addressVo->getStateOrRegion() .
            "|" . $addressVo->getPostalCode() .
            "|" . $addressVo->getCountryCode() .
            "|" . $addressVo->getPhone() .
            "|_", true
        );
    }
}
