<?php

namespace BlakePrinting\AmazonMws;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static MarketplaceDAO instance(PDO $connection = null, $schemaName = null)
 * @method MarketplaceVO findById($id, $options = null, $useCache = false)
 * @method MarketplaceVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method MarketplaceVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method MarketplaceVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method MarketplaceVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method MarketplaceVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method MarketplaceVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class MarketplaceDAO extends AbstractDataAccessObject {
    const MARKETPLACE_CANADA = [
        "region" => RegionDAO::REGION_NORTH_AMERICA, "endpoint" => EndpointDAO::ENDPOINT_COM,
        "country_code" => "CA", "mws_marketplace_id" => "A2EUQ1WTGCTBG2"
    ];
    const MARKETPLACE_UNITED_STATES = [
        "region" => RegionDAO::REGION_NORTH_AMERICA, "endpoint" => EndpointDAO::ENDPOINT_COM,
        "country_code" => "US", "mws_marketplace_id" => "ATVPDKIKX0DER"
    ];
    const MARKETPLACE_MEXICO = [
        "region" => RegionDAO::REGION_NORTH_AMERICA, "endpoint" => EndpointDAO::ENDPOINT_COM,
        "country_code" => "MX", "mws_marketplace_id" => "A1AM78C64UM0Y8"
    ];
    const MARKETPLACE_GERMANY = [
        "region" => RegionDAO::REGION_EUROPE, "endpoint" => EndpointDAO::ENDPOINT_EU,
        "country_code" => "DE", "mws_marketplace_id" => "A1PA6795UKMFR9"
    ];
    const MARKETPLACE_SPAIN = [
        "region" => RegionDAO::REGION_EUROPE, "endpoint" => EndpointDAO::ENDPOINT_EU,
        "country_code" => "ES", "mws_marketplace_id" => "A1RKKUPIHCS9HS"
    ];
    const MARKETPLACE_FRANCE = [
        "region" => RegionDAO::REGION_EUROPE, "endpoint" => EndpointDAO::ENDPOINT_EU,
        "country_code" => "FR", "mws_marketplace_id" => "A13V1IB3VIYZZH"
    ];
    const MARKETPLACE_ITALY = [
        "region" => RegionDAO::REGION_EUROPE, "endpoint" => EndpointDAO::ENDPOINT_EU,
        "country_code" => "IT", "mws_marketplace_id" => "APJ6JRA9NG5V4"
    ];
    const MARKETPLACE_UNITED_KINGDOM = [
        "region" => RegionDAO::REGION_EUROPE, "endpoint" => EndpointDAO::ENDPOINT_EU,
        "country_code" => "UK", "mws_marketplace_id" => "A1F83G8C2ARO7P"
    ];
    const MARKETPLACE_INDIA = [
        "region" => RegionDAO::REGION_EUROPE, "endpoint" => EndpointDAO::ENDPOINT_IN,
        "country_code" => "IN", "mws_marketplace_id" => "A21TJRUUN4KGV"
    ];
    const MARKETPLACE_JAPAN = [
        "region" => RegionDAO::REGION_FAR_EAST, "endpoint" => EndpointDAO::ENDPOINT_JP,
        "country_code" => "JP", "mws_marketplace_id" => "A1VC38T7YXB528"
    ];
    const MARKETPLACE_CHINA = [
        "region" => RegionDAO::REGION_CHINA, "endpoint" => EndpointDAO::ENDPOINT_CN,
        "country_code" => "CN", "mws_marketplace_id" => "AAHKV2X7AFYLW"
    ];
}
