<?php

namespace BlakePrinting\AmazonMws;

use BlakePrinting\AmazonMws\Sellers\CredentialVO;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Tasks\AbstractTask;
use BlakePrinting\Util\LogUtil;
use PDO;
use ReflectionClass;

abstract class AbstractMwsTask extends AbstractTask {
    /** @var CredentialVO */
    private $credentialVo = null;
    /** @var int */
    private $taskRequestId = null;

    /**
     * @param CredentialVO $credentialVo
     * @param string $taskName
     * @param string $appName
     * @param string $appVersion
     * @param int $lockExpirationSeconds
     * @param PDO $connection
     */
    public function __construct(CredentialVO $credentialVo, $taskName, $appName, $appVersion, $lockExpirationSeconds, PDO $connection) {
        parent::__construct($taskName, $appName, $appVersion, $lockExpirationSeconds, $connection);

        $this->credentialVo = $credentialVo;
    }

    /**
     * @return MwsTaskConfig
     */
    protected abstract function getMwsTaskConfig();

    /**
     * @return int
     */
    protected abstract function executeMws();

    /**
     * @inheritdoc
     */
    public function evaluate() {
        $logger =& LogUtil::getLogger();
        $config = $this->getMwsTaskConfig();
        $connection =& $this->getConnection();
        $taskRequestDao =& TaskRequestDAO::instance($connection);
        $taskRequests =& $taskRequestDao->findAll(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) {
                $pdoQuery
                    ->where("task_id", $this->getTaskVo()->getId(), PDO::PARAM_INT)
                    ->where("created", "UNIX_TIMESTAMP() - " . $this->getLockExpirationSeconds(), null, ">=")
                ;
            }
        );
        $taskRequestCount = ($taskRequests === null ? 0 : count($taskRequests));

        $logger->debug(
            "" . $taskRequestCount . " requests in the past hour - " .
            "restore rate: " . $config->getRestoreRate() . ", " .
            "maximum quota: " . $config->getMaximumRequestQuota()
        );

        return true;
    }

    public final function execute() {
        $connection =& $this->getConnection();
        $taskRequestDao =& TaskRequestDAO::instance($connection);
        $taskRequestVo = new TaskRequestVO();

        $taskRequestVo->setTaskId($this->getTaskVo()->getId());
        $taskRequestVo->setCredentialId($this->getCredentialVo()->getId());

        $taskRequestId = $taskRequestDao->insert($taskRequestVo);

        $this->taskRequestId = $taskRequestId;

        return $this->executeMws();
    }

    /**
     * @param mixed $response
     * @param mixed $result
     */
    public function saveResponse(&$response, $result) {
        $taskResponseDao =& TaskResponseDAO::instance($this->getConnection());
        $taskResponseVo = new TaskResponseVO();

        $taskResponseVo->setTaskRequestId($this->getTaskRequestId());

        if($result !== null && is_object($result) && method_exists($result, "getNextToken")) {
            $taskResponseVo->setNextToken($result->getNextToken());
        }

        if($response !== null && is_object($response)) {
            if(method_exists($response, "getResponseHeaderMetadata")) {
                $responseHeaderMetadata = $response->getResponseHeaderMetadata();

                if(method_exists($responseHeaderMetadata, "getRequestId")) {
                    $taskResponseVo->setRequestId($responseHeaderMetadata->getRequestId());
                }

                if(method_exists($responseHeaderMetadata, "getResponseContext")) {
                    $taskResponseVo->setResponseContext($responseHeaderMetadata->getResponseContext());
                }

                if(method_exists($responseHeaderMetadata, "getTimestamp")) {
                    $taskResponseVo->setTimestamp($responseHeaderMetadata->getTimestamp());
                }

                if(method_exists($responseHeaderMetadata, "getQuotaMax")) {
                    $taskResponseVo->setQuotaMax($responseHeaderMetadata->getQuotaMax());
                }

                if(method_exists($responseHeaderMetadata, "getQuotaRemaining")) {
                    $taskResponseVo->setQuotaRemaining($responseHeaderMetadata->getQuotaRemaining());
                }

                if(method_exists($responseHeaderMetadata, "getQuotaResetsAt")) {
                    $taskResponseVo->setQuotaResetsAt($responseHeaderMetadata->getQuotaResetsAt());
                }
            }
       }

        $taskResponseDao->insert($taskResponseVo);
    }

    /**
     * @return CredentialVO
     */
    public final function getCredentialVo()
    {
        return $this->credentialVo;
    }

    /**
     * @return int
     */
    public function getTaskRequestId()
    {
        return $this->taskRequestId;
    }
}
