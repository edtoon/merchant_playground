<?php

namespace BlakePrinting\AmazonMws;

use BlakePrinting\Db\ValueObject;

class TaskResponseHeaderVO implements ValueObject {
    /** @var int */
    private $taskResponseId = null;
    /** @var string */
    private $name = null;
    /** @var string */
    private $value = null;

    /**
     * @return int
     */
    public function getTaskResponseId()
    {
        return $this->taskResponseId;
    }

    /**
     * @param int $taskResponseId
     */
    public function setTaskResponseId($taskResponseId)
    {
        $this->taskResponseId = $taskResponseId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}
