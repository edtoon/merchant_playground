<?php

namespace BlakePrinting\AmazonMws\Sellers;

use BlakePrinting\AmazonMws\AbstractIntervalMwsTask;
use BlakePrinting\AmazonMws\MwsTaskConfig;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use Exception;
use MarketplaceWebServiceSellers_Exception;
use MarketplaceWebServiceSellers_Model_ListMarketplaceParticipationsByNextTokenRequest;
use MarketplaceWebServiceSellers_Model_ListMarketplaceParticipationsRequest;
use Monolog\Logger;
use PDO;

class ListMarketplacesTask extends AbstractIntervalMwsTask {
    const TASK_INTERVAL_SECONDS = 3600;
    const TASK_LOCK_SECONDS = 60;
    const TASK_NAME = "mws_list_marketplaces";

    /**
     * @param CredentialVO $credentialVo
     * @param string $appName
     * @param string $appVersion
     * @param PDO $connection
     */
    public function __construct(CredentialVO $credentialVo, $appName, $appVersion, PDO $connection) {
        parent::__construct(self::TASK_INTERVAL_SECONDS, $credentialVo, self::TASK_NAME, $appName, $appVersion, self::TASK_LOCK_SECONDS, $connection);
    }

    public function getMwsTaskConfig() {
        $mwsTaskConfig = new MwsTaskConfig();

        $mwsTaskConfig->setMaximumRequestQuota(15);
        $mwsTaskConfig->setRestoreRate(1);
        $mwsTaskConfig->setHasNextToken(true);
        $mwsTaskConfig->setNextTokenSharesQuota(true);

        return $mwsTaskConfig;
    }

    protected function executeMws() {
        try {
            $this->listMarketplacesForConfig();

            return 0;
        } catch(MarketplaceWebServiceSellers_Exception $e) {
            LogUtil::logException(Logger::ERROR, $e,
                "MWS service call failed with status code: " . $e->getStatusCode() .
                ", error code: " . $e->getErrorCode() .
                ", error type: " . $e->getErrorType() .
                ", request ID: " . $e->getRequestId() .
                ", responseHeaderMetadata: " . $e->getResponseHeaderMetadata() .
                ", xml: " . $e->getXML()
            );
        } catch(Exception $e) {
            LogUtil::logException(Logger::ERROR, $e, "Error listing marketplaces");
        }

        return 1;
    }

    private function listMarketplacesForConfig() {
        $connection =& $this->getConnection();
        $credentialVo = $this->getCredentialVo();
        $credentialId = $credentialVo->getId();
        $marketplaceInstanceListDao =& MarketplaceInstanceListDAO::instance($connection);
        $marketplaceInstanceListVo =& $marketplaceInstanceListDao->findMostRecent($credentialVo);
        $nextToken = ($marketplaceInstanceListVo === null ? null : $marketplaceInstanceListVo->getNextToken());
        $sellersClientFactory = new SellersClientFactory();
        $sellersClient =& $sellersClientFactory->createClient($this->getAppName(), $this->getAppVersion(), $credentialVo, $connection);
        $response = null;
        $result = null;

        if($nextToken === null) {
            $marketplaceInstanceListVo = new MarketplaceInstanceListVO();
            $marketplaceInstanceListVo->setCredentialId($credentialId);
            $request = new MarketplaceWebServiceSellers_Model_ListMarketplaceParticipationsRequest();
            $request->setSellerId($credentialVo->getSellerId());
            $response = $sellersClient->listMarketplaceParticipations($request);
            $result = ($response === null ? null : $response->getListMarketplaceParticipationsResult());
        } else {
            $request = new MarketplaceWebServiceSellers_Model_ListMarketplaceParticipationsByNextTokenRequest();
            $request->setSellerId($credentialVo->getSellerId());
            $request->setNextToken($nextToken);
            $response = $sellersClient->listMarketplaceParticipationsByNextToken($request);
            $result = ($response === null ? null : $response->getListMarketplaceParticipationsByNextTokenResult());
        }

        AmazonMwsUtil::logResponse($response);
        $this->saveResponse($response, $result);

        if(empty($result)) {
            $logger =& LogUtil::getLogger();

            $logger->critical("No result received - seller id: " . $credentialVo->getSellerId() . ", response: " . Utils::getVarDump($response));
        } else {
            $marketplaceInstanceListVo->setNextToken($result->getNextToken());
            $marketplaceInstanceListVo->setResponseHeaderMetadata((string)$response->getResponseHeaderMetadata());

            if($marketplaceInstanceListVo->getId() === null) {
                $marketplaceInstanceListVo->setId($marketplaceInstanceListDao->insert($marketplaceInstanceListVo));
            } else {
                $marketplaceInstanceListDao->update($marketplaceInstanceListVo);
            }

            $marketplaceInstanceListId = $marketplaceInstanceListVo->getId();
            $instanceIdsByMwsMarketplaceId = [];
            $marketplaceList = $result->getListMarketplaces();
            $marketplaces = ($marketplaceList === null ? null : $marketplaceList->getMarketplace());
            $marketplaceInstanceDao =& MarketplaceInstanceDAO::instance($connection);
            $marketplaceInstanceListResultDao =& MarketplaceInstanceListResultDAO::instance($connection);

            if(!empty($marketplaces)) {
                foreach($marketplaces as &$marketplace) {
                    $mwsMarketplaceId = $marketplace->getMarketplaceId();
                    $marketplaceInstanceProtoVo = new MarketplaceInstanceVO();

                    $marketplaceInstanceProtoVo->setCredentialId($credentialId);
                    $marketplaceInstanceProtoVo->setMwsMarketplaceId($mwsMarketplaceId);

                    $marketplaceInstanceVo =& $marketplaceInstanceDao->findByPrototype($marketplaceInstanceProtoVo, null, null, "FOR UPDATE");

                    $marketplaceInstanceProtoVo->setName($marketplace->getName());
                    $marketplaceInstanceProtoVo->setDefaultCountryCode($marketplace->getDefaultCountryCode());
                    $marketplaceInstanceProtoVo->setDefaultCurrencyCode($marketplace->getDefaultCurrencyCode());
                    $marketplaceInstanceProtoVo->setDefaultLanguageCode($marketplace->getDefaultLanguageCode());
                    $marketplaceInstanceProtoVo->setDomainName($marketplace->getDomainName());
                    $marketplaceInstanceProtoVo->setActive(true);

                    if($marketplaceInstanceVo === null) {
                        $marketplaceInstanceProtoVo->setId($marketplaceInstanceDao->insert($marketplaceInstanceProtoVo));

                        $marketplaceInstanceVo =& $marketplaceInstanceProtoVo;
                    } else if(Utils::propertiesDiffer(
                        $marketplaceInstanceProtoVo, $marketplaceInstanceVo, ["id", "created", "creator", "updated", "updater"]
                    )) {
                        $marketplaceInstanceProtoVo->setId($marketplaceInstanceVo->getId());

                        $marketplaceInstanceDao->update($marketplaceInstanceProtoVo);
                    }

                    $marketplaceInstanceId = $marketplaceInstanceVo->getId();
                    $marketplaceInstanceListResultVo = new MarketplaceInstanceListResultVO();

                    $marketplaceInstanceListResultVo->setMarketplaceInstanceId($marketplaceInstanceId);
                    $marketplaceInstanceListResultVo->setMarketplaceInstanceListId($marketplaceInstanceListId);

                    $marketplaceInstanceListResultDao->insert($marketplaceInstanceListResultVo);

                    $instanceIdsByMwsMarketplaceId[$mwsMarketplaceId] = $marketplaceInstanceId;
                }

                unset($marketplace);
            }

            $marketplaceInstanceParticipationDao =& MarketplaceInstanceParticipationDAO::instance($connection);
            $participationList = $result->getListParticipations();
            $participations = ($participationList === null ? null : $participationList->getParticipation());

            if(!empty($participations)) {
                foreach($participations as &$participation) {
                    $mwsMarketplaceId = $participation->getMarketplaceId();

                    if(!isset($instanceIdsByMwsMarketplaceId[$mwsMarketplaceId])) {
                        continue;
                    }

                    $marketplaceInstanceId = $instanceIdsByMwsMarketplaceId[$mwsMarketplaceId];
                    $hasSellerSuspendedListings = Utils::getBooleanOrNull($participation->getHasSellerSuspendedListings());

                    $marketplaceInstanceParticipationDao->store($marketplaceInstanceId, $hasSellerSuspendedListings);
                }

                unset($participation);
            }

            if($marketplaceInstanceListVo->getNextToken() === null) {
                PDOQuery::update()
                    ->table($marketplaceInstanceDao)
                    ->set("active", false, PDO::PARAM_BOOL)
                    ->where("credential_id", $credentialId, PDO::PARAM_INT)
                    ->where(
                        "id",
                        "(" .
                            "SELECT marketplace_instance_id " .
                            "FROM " . $marketplaceInstanceListResultDao->formatSchemaAndTableName() . " " .
                            "WHERE marketplace_instance_list_id = " . $marketplaceInstanceListId .
                        ")",
                        null, "NOT IN"
                    )
                    ->executeGetRowCount($connection)
                ;

                $marketplaceInstanceListResultDao->removeCompleted();
                $marketplaceInstanceListDao->removePrevious($marketplaceInstanceListVo);
            }
        }
    }
}
