<?php

namespace BlakePrinting\AmazonMws\Sellers;

use BlakePrinting\Db\ValueObject;

class MarketplaceInstanceListVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $credentialId = null;
    /** @var string */
    private $nextToken = null;
    /** @var string */
    private $responseHeaderMetadata = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getCredentialId()
    {
        return $this->credentialId;
    }

    /**
     * @param int $credentialId
     */
    public function setCredentialId($credentialId)
    {
        $this->credentialId = $credentialId;
    }

    /**
     * @return string
     */
    public function getNextToken()
    {
        return $this->nextToken;
    }

    /**
     * @param string $nextToken
     */
    public function setNextToken($nextToken)
    {
        $this->nextToken = $nextToken;
    }

    /**
     * @return string
     */
    public function getResponseHeaderMetadata()
    {
        return $this->responseHeaderMetadata;
    }

    /**
     * @param string $responseHeaderMetadata
     */
    public function setResponseHeaderMetadata($responseHeaderMetadata)
    {
        $this->responseHeaderMetadata = $responseHeaderMetadata;
    }
}
