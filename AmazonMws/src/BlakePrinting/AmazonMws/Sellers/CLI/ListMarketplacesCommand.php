<?php

namespace BlakePrinting\AmazonMws\Sellers\CLI;

use BlakePrinting\AmazonMws\Sellers\CredentialDAO;
use BlakePrinting\AmazonMws\Sellers\ListMarketplacesTask;
use BlakePrinting\CLI\CLICommand;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Tasks\TaskLockDAO;
use BlakePrinting\Util\LogUtil;
use Exception;
use Monolog\Logger;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListMarketplacesCommand extends CLICommand {
    protected function configure() {
        $this
            ->setName("amazon:mws:sellers:list-marketplaces")
            ->setDescription("List MWS marketplaces")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $connection =& PDOUtil::getMysqlConnection();
        $logger =& LogUtil::getLogger();

        $logger->debug("Listing marketplaces");

        $taskLockDao =& TaskLockDAO::instance($connection);
        $credentialDao =& CredentialDAO::instance($connection);
        $credentials =& $credentialDao->findAll();

        if(!empty($credentials)) {
            foreach($credentials as &$credentialVo) {
                $listMarketplacesTask = new ListMarketplacesTask(
                    $credentialVo, Main::APP_NAME, Main::APP_VERSION, $connection
                );

                try {
                    $taskLockDao->deleteExpired();

                    PDOUtil::commitConnection($connection);

                    if($listMarketplacesTask->lock()) {
                        try {
                            $listMarketplacesTask->run();
                        } finally {
                            $listMarketplacesTask->unlock();
                        }
                    }

                    PDOUtil::commitConnection($connection);
                } catch(Exception $e) {
                    LogUtil::logException(Logger::ERROR, $e, "Error executing task");

                    return false;
                } finally {
                    PDOUtil::rollbackConnection($connection);
                }
            }

            unset($credentialVo);
        }

        return true;
    }
}
