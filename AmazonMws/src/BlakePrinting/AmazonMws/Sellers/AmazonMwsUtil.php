<?php

namespace BlakePrinting\AmazonMws\Sellers;

use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Util\LogUtil;
use PDO;

class AmazonMwsUtil {
    const MOCK_ENDPOINT_ID = -33;
    const MOCK_ENDPOINT = 'https://localhost/amazon_mws_mock';
    const MOCK_MARKETPLACE_ENDPOINT = 'https://localhost/amazon_mws_mock';
    const MOCK_MARKETPLACE_ID = 'AMZMOCK';
    const MOCK_SELLER_ID = 'AMZ_MOCK_123';

    private function __construct() { }

    public static function forEachActiveMarketplace(callable $marketFunc, callable $finalFunc = null, PDO $connection = null) {
        if($connection === null) {
            $connection =& PDOUtil::getMysqlConnection();
        }

        $credentialDao =& CredentialDAO::instance($connection);
        $credentials =& $credentialDao->findAll();

        try {
            if(!empty($credentials)) {
                $marketplaceInstanceDao =& MarketplaceInstanceDAO::instance($connection);

                foreach($credentials as &$credentialVo) {
                    $marketplaceInstances =& $marketplaceInstanceDao->findActive($credentialVo->getId());

                    if(!empty($marketplaceInstances)) {
                        foreach($marketplaceInstances as &$marketplaceInstanceVo) {
                            $marketFunc($marketplaceInstanceVo, $credentialVo, $connection);
                        }

                        unset($marketplaceInstanceVo);
                    }
                }

                unset($credentialVo);
            }
        } finally {
            if($finalFunc !== null) {
                $finalFunc($connection);
            }
        }
    }

    /**
     * @param mixed $response
     * @param bool $logXml
     */
    public static function logResponse(&$response, $logXml = true) {
        $logger =& LogUtil::getLogger();

        $logger->debug("=============================================================================");

        if(empty($response)) {
            $logger->critical("No response object received");
        } else {
            $logger->debug("ResponseHeaderMetadata: " . $response->getResponseHeaderMetadata());

            if($logXml == true) {
                if(isset($response->domElement)) {
                    $logger->debug("Response element: " . $response->domElement->ownerDocument->saveXML($response->domElement));
                } else {
                    $logger->debug("Response XML: " . $response->toXML());
                }
            }
        }
    }
}
