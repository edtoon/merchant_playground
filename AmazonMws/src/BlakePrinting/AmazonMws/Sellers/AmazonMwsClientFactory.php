<?php

namespace BlakePrinting\AmazonMws\Sellers;

use BlakePrinting\AmazonMws\EndpointDAO;
use BlakePrinting\AmazonMws\EndpointVO;
use BlakePrinting\Util\ConfigUtil;
use JamesHalsall\ConstantResolver\ConstantResolver;
use Stringy\StaticStringy as S;
use Exception;
use PDO;

abstract class AmazonMwsClientFactory {
    const FEED_API = 1;
    const REPORT_API = 2;
    const CART_API = 3;
    const CUSTOMER_API = 4;
    const FINANCE_API = 5;
    const FBA_INBOUND_API = 6;
    const FBA_INVENTORY_API = 7;
    const FBA_OUTBOUND_API = 8;
    const FULFILLMENT_API = 9;
    const PAYMENT_API = 10;
    const ORDER_API = 11;
    const PRODUCT_API = 12;
    const RECOMMENDATION_API = 13;
    const SELLER_API = 14;
    const SUBSCRIPTION_API = 15;

    private static $apiConfigs = [];

    /** @var int */
    private $api = null;
    /** @var string */
    private $clientLibraryPath = null;
    /** @var string */
    private $clientClass = null;
    /** @var string */
    private $mockClass = null;
    /** @var string */
    private $endpointExtension = null;
    /** @var EndpointVO */
    private $endpointVo = null;
    /** @var CredentialVO */
    private $credentialVo = null;

    /**
     * @param int $api API as identified by one of the class constants
     * @param string $clientLibraryPath Location of the client library's source code relative to the app dir
     * @param string $clientClass Class name of the client implementation
     * @param string $mockClass Class name of the mock implementation
     * @param string $endpointExtension Path of the library's endpoint under the marketplace URL
     */
    public function __construct($api, $clientLibraryPath, $clientClass, $mockClass, $endpointExtension) {
        $this->api = $api;
        $this->clientLibraryPath = $clientLibraryPath;
        $this->clientClass = $clientClass;
        $this->mockClass = $mockClass;
        $this->endpointExtension = $endpointExtension;

        self::$apiConfigs[$api] = [
            "path" => $clientLibraryPath,
            "client" => $clientClass,
            "mock" => $mockClass,
            "endpointExtension" => $endpointExtension
        ];
    }

    public function setEndpointVo(EndpointVO &$endpointVo) {
        $this->endpointVo =& $endpointVo;
    }

    public function setCredentialVo(CredentialVO &$credentialVo) {
        $this->credentialVo =& $credentialVo;
    }

    public function configure(CredentialVO &$credentialVo, PDO $connection) {
        $endpointDao =& EndpointDAO::instance($connection);
        $endpointVo =& $endpointDao->findById($credentialVo->getEndpointId());

        $this->setEndpointVo($endpointVo);
        $this->setCredentialVo($credentialVo);
    }

    /**
     * @param string $applicationName Name of the application accessing the API
     * @param string $applicationVersion Version of the application accessing the API
     */
    public function &createClient($applicationName, $applicationVersion, CredentialVO &$credentialVo = null, PDO $connection = null) {
        if($credentialVo !== null && $connection !== null) {
            $this->configure($credentialVo, $connection);
        }

        $api = $this->getApi();
        $apiConfig =& self::$apiConfigs[$api];
        $endpointExtension = $apiConfig["endpointExtension"];
        $serviceUrl = $this->endpointVo->getEndpoint() . (empty($endpointExtension) ? "" : $endpointExtension);
        $sellerId = ($this->credentialVo == null ? null : $this->credentialVo->getSellerId());
        $serviceConfig = [
            'ServiceURL' => $serviceUrl,
            'ProxyHost' => null,
            'ProxyPort' => -1,
            'ProxyUsername' => null,
            'ProxyPassword' => null,
            'MaxErrorRetry' => 3,
        ];
        $serviceClient = null;

        if(self::loadApi($api) === false) {
            $constantResolver = new ConstantResolver($this);

            throw new Exception("Failed to load api: " . $constantResolver->resolve($api));
        }

        if(S::startsWith($serviceUrl, AmazonMwsUtil::MOCK_ENDPOINT) || AmazonMwsUtil::MOCK_SELLER_ID == $sellerId) {
            $serviceClient = new $apiConfig["mock"]();
        } else {
            switch($api) {
                case self::FBA_INVENTORY_API:
                case self::REPORT_API:
                    $serviceClient = new $apiConfig["client"](
                        $this->credentialVo->getAwsAccessKeyId(),
                        $this->credentialVo->getAwsSecretAccessKey(),
                        $serviceConfig,
                        $applicationName,
                        $applicationVersion
                    );
                    break;
                case self::ORDER_API:
                case self::PRODUCT_API:
                case self::SELLER_API:
                case self::SUBSCRIPTION_API:
                    $serviceClient = new $apiConfig["client"](
                        $this->credentialVo->getAwsAccessKeyId(),
                        $this->credentialVo->getAwsSecretAccessKey(),
                        $applicationName,
                        $applicationVersion,
                        $serviceConfig
                    );
                    break;
            }
        }

        return $serviceClient;
    }

    /**
     * @return int
     */
    public function getApi() {
        return $this->api;
    }

    /**
     * @return string
     */
    public function getClientLibraryPath() {
        return $this->clientLibraryPath;
    }

    /**
     * @return string
     */
    public function getClientClass() {
        return $this->clientClass;
    }

    /**
     * @return string
     */
    public function getMockClass() {
        return $this->mockClass;
    }

    /**
     * @return string
     */
    public function getEndpointExtension() {
        return $this->endpointExtension;
    }

    private static function loadApi($api) {
        $apiConfigs =& self::$apiConfigs;

        return spl_autoload_register(function($className) use (&$api, &$apiConfigs) {
            $classDir = ConfigUtil::getVendorDir() . DIRECTORY_SEPARATOR . $apiConfigs[$api]["path"];

            do {
                $tryPath = $classDir . '/' . $className . '.php';

                if(file_exists($tryPath)) {
                    require($tryPath);
                    return true;
                }

                $underscorePos = strpos($className, "_");

                if(is_int($underscorePos) && $underscorePos > 0) {
                    $className[$underscorePos] = '/';
                }
            } while($underscorePos !== false);

            return false;
        });
    }
}
