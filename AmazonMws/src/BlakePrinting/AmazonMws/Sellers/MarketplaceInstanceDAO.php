<?php

namespace BlakePrinting\AmazonMws\Sellers;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;
use ReflectionClass;

/**
 * @method static MarketplaceInstanceDAO instance(PDO $connection = null, $schemaName = null)
 * @method MarketplaceInstanceVO findById($id, $options = null, $useCache = false)
 * @method MarketplaceInstanceVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method MarketplaceInstanceVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method MarketplaceInstanceVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method MarketplaceInstanceVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method MarketplaceInstanceVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method MarketplaceInstanceVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class MarketplaceInstanceDAO extends AbstractDataAccessObject {
    /**
     * @param int $credentialId
     * @return MarketplaceInstanceVO[]
     */
    public function &findActive($credentialId, $orderBy = null, $limit = null) {
        return $this->findAll(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$credentialId) {
                $pdoQuery
                    ->where("credential_id", $credentialId, PDO::PARAM_INT)
                    ->where("active", true, PDO::PARAM_BOOL)
                ;
            }, $orderBy, $limit
        );
    }
}
