<?php

namespace BlakePrinting\AmazonMws\Sellers;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static MarketplaceInstanceListDAO instance(PDO $connection = null, $schemaName = null)
 * @method MarketplaceInstanceListVO findById($id, $options = null, $useCache = false)
 * @method MarketplaceInstanceListVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method MarketplaceInstanceListVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method MarketplaceInstanceListVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method MarketplaceInstanceListVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method MarketplaceInstanceListVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method MarketplaceInstanceListVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class MarketplaceInstanceListDAO extends AbstractDataAccessObject {
    /**
     * @param CredentialVO $credentialVo
     * @return MarketplaceInstanceListVO
     */
    public function &findMostRecent(CredentialVO &$credentialVo) {
        return $this->findByColumn("credential_id", $credentialVo->getId(), PDO::PARAM_INT, "id DESC", 1);
    }

    /**
     * @param MarketplaceInstanceListVO $marketplaceInstanceListVo
     * @return int
     */
    public function removePrevious(MarketplaceInstanceListVO &$marketplaceInstanceListVo) {
        return PDOQuery::delete()->from($this)
            ->where("credential_id", $marketplaceInstanceListVo->getCredentialId(), PDO::PARAM_INT)
            ->where("id", $marketplaceInstanceListVo->getId(), PDO::PARAM_INT, "<")
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
