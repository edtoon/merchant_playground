<?php

namespace BlakePrinting\AmazonMws\Sellers;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static MarketplaceInstanceListResultDAO instance(PDO $connection = null, $schemaName = null)
 * @method MarketplaceInstanceListResultVO findById($id, $options = null, $useCache = false)
 * @method MarketplaceInstanceListResultVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method MarketplaceInstanceListResultVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method MarketplaceInstanceListResultVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method MarketplaceInstanceListResultVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method MarketplaceInstanceListResultVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method MarketplaceInstanceListResultVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class MarketplaceInstanceListResultDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    public function removeCompleted() {
        $connection =& $this->getConnection();
        $marketplaceInstanceListDao =& MarketplaceInstanceListDAO::instance($connection);

        return PDOQuery::delete()
            ->from($this)
            ->where(
                "marketplace_instance_list_id IN (" .
                    "SELECT id FROM " . $marketplaceInstanceListDao->formatSchemaAndTableName() . " " .
                    "WHERE next_token IS NULL" .
                ")"
            )
            ->executeGetRowCount($connection)
        ;
    }
}
