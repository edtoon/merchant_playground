<?php

namespace BlakePrinting\AmazonMws\Sellers;

use BlakePrinting\Db\ByteLimit;
use BlakePrinting\Db\ValueObject;
use Phinx\Db\Adapter\MysqlAdapter;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class CredentialVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $creator = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $updater = null;
    /** @var int */
    private $endpointId = null;
    /** @var string */
    private $awsAccessKeyId = null;
    /** @var string */
    private $awsSecretAccessKey = null;
    /** @var string */
    private $sellerId = null;

    public static function loadValidatorMetadata(ClassMetadata $metadata) {
        $metadata->addGetterConstraints(
            "id", [new Assert\Type("digit"), new Assert\Range(["min" => 1, "max" => MysqlAdapter::INT_SMALL])]
        );
        $metadata->addGetterConstraints(
            "created", [new Assert\Type("digit"), new Assert\Range(["min" => 1, "max" => MysqlAdapter::INT_BIG])]
        );
        $metadata->addGetterConstraints(
            "creator", [new Assert\Type("digit"), new Assert\Range(["min" => 1, "max" => MysqlAdapter::INT_SMALL])]
        );
        $metadata->addGetterConstraints(
            "updated", [new Assert\Type("digit"), new Assert\Range(["min" => 1, "max" => MysqlAdapter::INT_BIG])]
        );
        $metadata->addGetterConstraints(
            "updater", [new Assert\Type("digit"), new Assert\Range(["min" => 1, "max" => MysqlAdapter::INT_SMALL])]
        );
        $metadata->addGetterConstraints(
            "endpointId", [new Assert\NotNull(), new Assert\Type("digit"), new Assert\Range(["min" => 1, "max" => MysqlAdapter::INT_SMALL])]
        );
        $metadata->addGetterConstraints(
            "awsAccessKeyId", [new Assert\NotNull(), new Assert\NotBlank(), new Assert\Length(["min" => 8, "max" => 25]), new ByteLimit(50)]
        );
        $metadata->addGetterConstraints(
            "awsSecretAccessKey", [new Assert\NotNull(), new Assert\NotBlank(), new Assert\Length(["min" => 8, "max" => 50]), new ByteLimit(50)]
        );
        $metadata->addGetterConstraints(
            "sellerId", [new Assert\NotNull(), new Assert\NotBlank(), new Assert\Length(["min" => 8, "max" => 50]), new ByteLimit(50)]
        );
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param int $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getUpdater()
    {
        return $this->updater;
    }

    /**
     * @param int $updater
     */
    public function setUpdater($updater)
    {
        $this->updater = $updater;
    }

    /**
     * @return int
     */
    public function getEndpointId()
    {
        return $this->endpointId;
    }

    /**
     * @param int $endpointId
     */
    public function setEndpointId($endpointId)
    {
        $this->endpointId = $endpointId;
    }

    /**
     * @return string
     */
    public function getAwsAccessKeyId()
    {
        return $this->awsAccessKeyId;
    }

    /**
     * @param string $awsAccessKeyId
     */
    public function setAwsAccessKeyId($awsAccessKeyId)
    {
        $this->awsAccessKeyId = $awsAccessKeyId;
    }

    /**
     * @return string
     */
    public function getAwsSecretAccessKey()
    {
        return $this->awsSecretAccessKey;
    }

    /**
     * @param string $awsSecretAccessKey
     */
    public function setAwsSecretAccessKey($awsSecretAccessKey)
    {
        $this->awsSecretAccessKey = $awsSecretAccessKey;
    }

    /**
     * @return string
     */
    public function getSellerId()
    {
        return $this->sellerId;
    }

    /**
     * @param string $sellerId
     */
    public function setSellerId($sellerId)
    {
        $this->sellerId = $sellerId;
    }
}
