<?php

namespace BlakePrinting\AmazonMws\Sellers;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static MarketplaceInstanceParticipationDAO instance(PDO $connection = null, $schemaName = null)
 * @method MarketplaceInstanceParticipationVO findById($id, $options = null, $useCache = false)
 * @method MarketplaceInstanceParticipationVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method MarketplaceInstanceParticipationVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method MarketplaceInstanceParticipationVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method MarketplaceInstanceParticipationVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method MarketplaceInstanceParticipationVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method MarketplaceInstanceParticipationVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class MarketplaceInstanceParticipationDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, "marketplace_instance_id");
    }

    public function store($marketplaceInstanceId, $hasSellerSuspendedListings) {
        return PDOQuery::insert()
            ->into($this)
            ->value("marketplace_instance_id", $marketplaceInstanceId, PDO::PARAM_INT)
            ->value("created", "UNIX_TIMESTAMP()")
            ->value("has_seller_suspended_listings", $hasSellerSuspendedListings, PDO::PARAM_BOOL)
            ->onDuplicateValue("updated", "UNIX_TIMESTAMP()")
            ->onDuplicateValue("has_seller_suspended_listings", "VALUES(has_seller_suspended_listings)")
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
