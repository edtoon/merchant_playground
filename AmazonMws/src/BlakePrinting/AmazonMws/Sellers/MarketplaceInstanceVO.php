<?php

namespace BlakePrinting\AmazonMws\Sellers;

use BlakePrinting\Db\ValueObject;

class MarketplaceInstanceVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $creator = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $updater = null;
    /** @var int */
    private $credentialId = null;
    /** @var string */
    private $mwsMarketplaceId = null;
    /** @var string */
    private $name = null;
    /** @var string */
    private $defaultCountryCode = null;
    /** @var string */
    private $defaultCurrencyCode = null;
    /** @var string */
    private $defaultLanguageCode = null;
    /** @var string */
    private $domainName = null;
    /** @var bool */
    private $active = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param int $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getUpdater()
    {
        return $this->updater;
    }

    /**
     * @param int $updater
     */
    public function setUpdater($updater)
    {
        $this->updater = $updater;
    }

    /**
     * @return int
     */
    public function getCredentialId() {
        return $this->credentialId;
    }

    /**
     * @param int $credentialId
     */
    public function setCredentialId($credentialId) {
        $this->credentialId = $credentialId;
    }

    /**
     * @return string
     */
    public function getMwsMarketplaceId()
    {
        return $this->mwsMarketplaceId;
    }

    /**
     * @param string $mwsMarketplaceId
     */
    public function setMwsMarketplaceId($mwsMarketplaceId)
    {
        $this->mwsMarketplaceId = $mwsMarketplaceId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDefaultCountryCode()
    {
        return $this->defaultCountryCode;
    }

    /**
     * @param string $defaultCountryCode
     */
    public function setDefaultCountryCode($defaultCountryCode)
    {
        $this->defaultCountryCode = $defaultCountryCode;
    }

    /**
     * @return string
     */
    public function getDefaultCurrencyCode()
    {
        return $this->defaultCurrencyCode;
    }

    /**
     * @param string $defaultCurrencyCode
     */
    public function setDefaultCurrencyCode($defaultCurrencyCode)
    {
        $this->defaultCurrencyCode = $defaultCurrencyCode;
    }

    /**
     * @return string
     */
    public function getDefaultLanguageCode()
    {
        return $this->defaultLanguageCode;
    }

    /**
     * @param string $defaultLanguageCode
     */
    public function setDefaultLanguageCode($defaultLanguageCode)
    {
        $this->defaultLanguageCode = $defaultLanguageCode;
    }

    /**
     * @return string
     */
    public function getDomainName()
    {
        return $this->domainName;
    }

    /**
     * @param string $domainName
     */
    public function setDomainName($domainName)
    {
        $this->domainName = $domainName;
    }

    /**
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active) {
        $this->active = $active;
    }
}
