<?php

namespace BlakePrinting\AmazonMws\Sellers;

use MarketplaceWebServiceSellers_Client;
use MarketplaceWebServiceSellers_Interface;
use MarketplaceWebServiceSellers_Mock;
use PDO;

/**
 * @method MarketplaceWebServiceSellers_Interface createClient($applicationName, $applicationVersion, CredentialVO &$credentialVo = null, PDO $connection = null)
 */
class SellersClientFactory extends AmazonMwsClientFactory {
    public function __construct() {
        parent::__construct(
            AmazonMwsClientFactory::SELLER_API, "amazon_mws_sellers/src",
            MarketplaceWebServiceSellers_Client::class, MarketplaceWebServiceSellers_Mock::class,
            "/Sellers/2014-09-30"
        );
    }
}
