<?php

namespace BlakePrinting\AmazonMws\Sellers;

use BlakePrinting\Db\ValueObject;

class MarketplaceInstanceParticipationVO implements ValueObject {
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $marketplaceInstanceId = null;
    /** @var bool */
    private $hasSellerSuspendedListings = null;

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return bool
     */
    public function isHasSellerSuspendedListings()
    {
        return $this->hasSellerSuspendedListings;
    }

    /**
     * @param bool $hasSellerSuspendedListings
     */
    public function setHasSellerSuspendedListings($hasSellerSuspendedListings)
    {
        $this->hasSellerSuspendedListings = $hasSellerSuspendedListings;
    }

    /**
     * @return int
     */
    public function getMarketplaceInstanceId()
    {
        return $this->marketplaceInstanceId;
    }

    /**
     * @param int $marketplaceInstanceId
     */
    public function setMarketplaceInstanceId($marketplaceInstanceId)
    {
        $this->marketplaceInstanceId = $marketplaceInstanceId;
    }
}
