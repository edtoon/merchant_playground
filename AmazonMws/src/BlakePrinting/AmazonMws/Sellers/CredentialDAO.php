<?php

namespace BlakePrinting\AmazonMws\Sellers;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static CredentialDAO instance(PDO $connection = null, $schemaName = null)
 * @method CredentialVO findById($id, $options = null, $useCache = false)
 * @method CredentialVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CredentialVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CredentialVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method CredentialVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CredentialVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CredentialVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class CredentialDAO extends AbstractDataAccessObject {
}
