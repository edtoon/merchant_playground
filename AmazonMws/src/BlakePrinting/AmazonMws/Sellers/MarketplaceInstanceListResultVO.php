<?php

namespace BlakePrinting\AmazonMws\Sellers;

use BlakePrinting\Db\ValueObject;

class MarketplaceInstanceListResultVO implements ValueObject {
    /** @var int */
    private $marketplaceInstanceListId = null;
    /** @var int */
    private $marketplaceInstanceId = null;

    /**
     * @return int
     */
    public function getMarketplaceInstanceListId()
    {
        return $this->marketplaceInstanceListId;
    }

    /**
     * @param int $marketplaceInstanceListId
     */
    public function setMarketplaceInstanceListId($marketplaceInstanceListId)
    {
        $this->marketplaceInstanceListId = $marketplaceInstanceListId;
    }

    /**
     * @return int
     */
    public function getMarketplaceInstanceId()
    {
        return $this->marketplaceInstanceId;
    }

    /**
     * @param int $marketplaceInstanceId
     */
    public function setMarketplaceInstanceId($marketplaceInstanceId)
    {
        $this->marketplaceInstanceId = $marketplaceInstanceId;
    }
}
