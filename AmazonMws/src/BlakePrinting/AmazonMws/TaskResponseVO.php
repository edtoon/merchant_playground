<?php

namespace BlakePrinting\AmazonMws;

use BlakePrinting\Db\ValueObject;

class TaskResponseVO implements ValueObject {
    /** @var int */
    private $taskRequestId = null;
    /** @var int */
    private $created = null;
    /** @var string */
    private $nextToken = null;
    /** @var string */
    private $requestId = null;
    /** @var string */
    private $responseContext = null;
    /** @var string */
    private $timestamp = null;
    /** @var int */
    private $quotaMax = null;
    /** @var int */
    private $quotaRemaining = null;
    /** @var string */
    private $quotaResetsAt = null;

    /**
     * @return int
     */
    public function getTaskRequestId()
    {
        return $this->taskRequestId;
    }

    /**
     * @param int $taskRequestId
     */
    public function setTaskRequestId($taskRequestId)
    {
        $this->taskRequestId = $taskRequestId;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getNextToken()
    {
        return $this->nextToken;
    }

    /**
     * @param string $nextToken
     */
    public function setNextToken($nextToken)
    {
        $this->nextToken = $nextToken;
    }

    /**
     * @return string
     */
    public function getRequestId()
    {
        return $this->requestId;
    }

    /**
     * @param string $requestId
     */
    public function setRequestId($requestId)
    {
        $this->requestId = $requestId;
    }

    /**
     * @return string
     */
    public function getResponseContext()
    {
        return $this->responseContext;
    }

    /**
     * @param string $responseContext
     */
    public function setResponseContext($responseContext)
    {
        $this->responseContext = $responseContext;
    }

    /**
     * @return string
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param string $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return int
     */
    public function getQuotaMax()
    {
        return $this->quotaMax;
    }

    /**
     * @param int $quotaMax
     */
    public function setQuotaMax($quotaMax)
    {
        $this->quotaMax = $quotaMax;
    }

    /**
     * @return int
     */
    public function getQuotaRemaining()
    {
        return $this->quotaRemaining;
    }

    /**
     * @param int $quotaRemaining
     */
    public function setQuotaRemaining($quotaRemaining)
    {
        $this->quotaRemaining = $quotaRemaining;
    }

    /**
     * @return string
     */
    public function getQuotaResetsAt()
    {
        return $this->quotaResetsAt;
    }

    /**
     * @param string $quotaResetsAt
     */
    public function setQuotaResetsAt($quotaResetsAt)
    {
        $this->quotaResetsAt = $quotaResetsAt;
    }
}
