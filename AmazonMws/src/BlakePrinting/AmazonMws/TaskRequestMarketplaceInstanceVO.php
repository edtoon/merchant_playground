<?php

namespace BlakePrinting\AmazonMws;

use BlakePrinting\Db\ValueObject;

class TaskRequestMarketplaceInstanceVO implements ValueObject {
    /** @var int */
    private $taskRequestId = null;
    /** @var int */
    private $marketplaceInstanceId = null;

    /**
     * @return int
     */
    public function getTaskRequestId()
    {
        return $this->taskRequestId;
    }

    /**
     * @param int $taskRequestId
     */
    public function setTaskRequestId($taskRequestId)
    {
        $this->taskRequestId = $taskRequestId;
    }

    /**
     * @return int
     */
    public function getMarketplaceInstanceId()
    {
        return $this->marketplaceInstanceId;
    }

    /**
     * @param int $marketplaceInstanceId
     */
    public function setMarketplaceInstanceId($marketplaceInstanceId)
    {
        $this->marketplaceInstanceId = $marketplaceInstanceId;
    }
}
