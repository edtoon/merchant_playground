<?php

namespace BlakePrinting\AmazonMws;

class MwsTaskConfig {
    /** @var int */
    private $maximumRequestQuota = null;
    /** @var int */
    private $restoreRate = null;
    /** @var bool */
    private $hasNextToken = null;
    /** @var bool */
    private $nextTokenSharesQuota = null;
    /** @var int */
    private $nextTokenMaximumRequestQuota = null;
    /** @var int */
    private $nextTokenRestoreRate = null;

    /**
     * @return int
     */
    public function getMaximumRequestQuota()
    {
        return $this->maximumRequestQuota;
    }

    /**
     * @param int $maximumRequestQuota
     */
    public function setMaximumRequestQuota($maximumRequestQuota)
    {
        $this->maximumRequestQuota = $maximumRequestQuota;
    }

    /**
     * @return int
     */
    public function getRestoreRate()
    {
        return $this->restoreRate;
    }

    /**
     * @param int $restoreRate
     */
    public function setRestoreRate($restoreRate)
    {
        $this->restoreRate = $restoreRate;
    }

    /**
     * @return boolean
     */
    public function isHasNextToken()
    {
        return $this->hasNextToken;
    }

    /**
     * @param boolean $hasNextToken
     */
    public function setHasNextToken($hasNextToken)
    {
        $this->hasNextToken = $hasNextToken;
    }

    /**
     * @return boolean
     */
    public function isNextTokenSharesQuota()
    {
        return $this->nextTokenSharesQuota;
    }

    /**
     * @param boolean $nextTokenSharesQuota
     */
    public function setNextTokenSharesQuota($nextTokenSharesQuota)
    {
        $this->nextTokenSharesQuota = $nextTokenSharesQuota;
    }

    /**
     * @return int
     */
    public function getNextTokenMaximumRequestQuota()
    {
        return $this->nextTokenMaximumRequestQuota;
    }

    /**
     * @param int $nextTokenMaximumRequestQuota
     */
    public function setNextTokenMaximumRequestQuota($nextTokenMaximumRequestQuota)
    {
        $this->nextTokenMaximumRequestQuota = $nextTokenMaximumRequestQuota;
    }

    /**
     * @return int
     */
    public function getNextTokenRestoreRate()
    {
        return $this->nextTokenRestoreRate;
    }

    /**
     * @param int $nextTokenRestoreRate
     */
    public function setNextTokenRestoreRate($nextTokenRestoreRate)
    {
        $this->nextTokenRestoreRate = $nextTokenRestoreRate;
    }
}
