<?php

namespace BlakePrinting\AmazonMws;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static EndpointDAO instance(PDO $connection = null, $schemaName = null)
 * @method EndpointVO findById($id, $options = null, $useCache = false)
 * @method EndpointVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EndpointVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EndpointVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EndpointVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EndpointVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EndpointVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class EndpointDAO extends AbstractDataAccessObject {
    const ENDPOINT_COM = "https://mws.amazonservices.com";
    const ENDPOINT_EU = "https://mws-eu.amazonservices.com";
    const ENDPOINT_JP = "https://mws.amazonservices.jp";
    const ENDPOINT_IN = "https://mws.amazonservices.in";
    const ENDPOINT_CN = "https://mws.amazonservices.com.cn";
}
