<?php

namespace BlakePrinting\AmazonMws;

use BlakePrinting\Fields\FieldTypeDAO;

class AmazonMwsFields {
    const DOMAIN = "amazon_mws";
    const SCHEMA = "api";
    const TABLE_REQUEST_METADATA = [ "table" => "request", "fields" => [
        "developer_account_id" => FieldTypeDAO::TYPE_STRING,
        "mws_auth_token" => FieldTypeDAO::TYPE_STRING,
        "seller_id" => FieldTypeDAO::TYPE_STRING,
        "next_token" => FieldTypeDAO::TYPE_TEXT,
    ]];
    const TABLE_RESPONSE_METADATA = [ "table" => "response", "fields" => [
        "next_token" => FieldTypeDAO::TYPE_TEXT,
        "request_id" => FieldTypeDAO::TYPE_STRING,
        "response_context" => FieldTypeDAO::TYPE_STRING,
        "quota_max" => FieldTypeDAO::TYPE_UNSIGNED,
        "quota_remaining" => FieldTypeDAO::TYPE_UNSIGNED,
        "quota_resets_at" => FieldTypeDAO::TYPE_STRING
    ]];

    private function __construct() { }
}
