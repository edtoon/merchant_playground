<?php

namespace BlakePrinting\AmazonMws;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static TaskRequestDAO instance(PDO $connection = null, $schemaName = null)
 * @method TaskRequestVO findById($id, $options = null, $useCache = false)
 * @method TaskRequestVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskRequestVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskRequestVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaskRequestVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskRequestVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskRequestVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class TaskRequestDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, "task_request_id");
    }
}
