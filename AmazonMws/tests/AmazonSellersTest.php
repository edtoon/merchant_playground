<?php

use BlakePrinting\AmazonMws\EndpointVO;
use BlakePrinting\AmazonMws\Sellers\AmazonMwsUtil;
use BlakePrinting\AmazonMws\Sellers\SellersClientFactory;
use BlakePrinting\Test\AbstractTestCase;

class AmazonSellersTest extends AbstractTestCase {
    /** @var MarketplaceWebServiceSellers_Interface */
    private static $client = null;

    public static function setUpBeforeClass() {
        parent::setUpBeforeClass();

        $sellersClientFactory = new SellersClientFactory();
        $endpointVo = new EndpointVO();

        $endpointVo->setId(AmazonMwsUtil::MOCK_ENDPOINT_ID);
        $endpointVo->setEndpoint(AmazonMwsUtil::MOCK_ENDPOINT);
        $sellersClientFactory->setEndpointVo($endpointVo);

        self::$client =& $sellersClientFactory->createClient(self::class, "0.0.1");
    }

    public function testListMarketplaceParticipation() {
        $request = new MarketplaceWebServiceSellers_Model_ListMarketplaceParticipationsRequest();
        $request->setSellerId(AmazonMwsUtil::MOCK_SELLER_ID);
        $response = self::$client->listMarketplaceParticipations($request);

        AmazonMwsUtil::logResponse($response);

        $this->assertNotEmpty($response);

        $responseMetadata = $response->getResponseMetadata();

        $this->assertNotEmpty($responseMetadata);
        $this->assertEquals("String", $responseMetadata->getRequestId());

        $result = $response->getListMarketplaceParticipationsResult();

        $this->assertNotEmpty($result);
        $this->assertEquals("String", $result->getNextToken());

        $marketplaceList = $result->getListMarketplaces();

        $this->assertNotNull($marketplaceList);

        $marketplaces = $marketplaceList->getMarketplace();

        $this->assertNotEmpty($marketplaces);
        $this->assertEquals(1, count($marketplaces));

        foreach($marketplaces as $marketplace) {
            $this->assertEquals("String", $marketplace->getMarketplaceId());
            $this->assertEquals("String", $marketplace->getName());
            $this->assertEquals("String", $marketplace->getDefaultCountryCode());
            $this->assertEquals("String", $marketplace->getDefaultCurrencyCode());
            $this->assertEquals("String", $marketplace->getDefaultLanguageCode());
            $this->assertEquals("String", $marketplace->getDomainName());
        }

        $participationList = $result->getListParticipations();

        $this->assertNotNull($participationList);

        $participation = $participationList->getParticipation();

        $this->assertNotEmpty($participation);
        $this->assertEquals(1, count($participation));

        foreach($participation as $participationEntry) {
            $this->assertEquals("String", $participationEntry->getMarketplaceId());
            $this->assertEquals("String", $participationEntry->getSellerId());
            $this->assertEquals("String", $participationEntry->getHasSellerSuspendedListings());
        }
    }
}
