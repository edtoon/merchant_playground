#!/bin/bash
#
# Download non-Packagist dependencies
#

set -o errexit
set -o nounset

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

VEND_DIR="${VEND_DIR:=${BASE_DIR}/vendor}"
DL_DIR="${DL_DIR:=${BASE_DIR}/downloads}"

declare -a VENDOR_DOWNLOADS=(
  "amazon_mws_sellers:https://images-na.ssl-images-amazon.com/images/G/01/mwsportal/clientlib/Sellers/2011-07-01/MWSSellersPHPClientLibrary-2011-07-01._V316922799_.zip"
)

unzip -v > /dev/null 2>&1 || sudo apt-get install unzip

mkdir -p "${DL_DIR}" "${VEND_DIR}"

for vendor_download in ${VENDOR_DOWNLOADS[@]}
do
  vendor_dir="${VEND_DIR}/${vendor_download%%:*}"
  vendor_url="${vendor_download#*:}"

  if [ ! -d "${vendor_dir}" ];
  then
    download_filename="${vendor_url##*/}"
    download_path="${DL_DIR}/${download_filename}"

    if [ ! -f "${download_path}" ];
    then
      curl -o "${download_path}" "${vendor_url}"
    fi

    if [ "${download_path: -4}" = ".zip" ];
    then
      mkdir -p "${vendor_dir}"
      unzip "${download_path}" -d "${vendor_dir}"
    else
      echo "Downloaded: ${download_path: -4}"
    fi
  fi
done

for patch_php_file in `find ${VEND_DIR}/amazon_mws_sellers/src/MarketplaceWebServiceSellers/ -name "*.php"`
do
  backup_file="${patch_php_file}.bak"

  if [ ! -f "${backup_file}" ] || [ "${patch_php_file}" -ot "${backup_file}" ];
  then
    sed -i.bak "s/iconv_set_encoding/ @iconv_set_encoding/g" "${patch_php_file}"
    sed -i.bak "s/@return ResponseMetadata/@return MarketplaceWebServiceSellers_Model_ResponseMetadata/g" "${patch_php_file}"
    sed -i.bak "s/@return ListMarketplaceParticipationsResult/@return MarketplaceWebServiceSellers_Model_ListMarketplaceParticipationsResult/g" "${patch_php_file}"
    sed -i.bak "s/@return ListMarketplaces/@return MarketplaceWebServiceSellers_Model_ListMarketplaces/g" "${patch_php_file}"
    sed -i.bak "s/@return List<Marketplace>/@return MarketplaceWebServiceSellers_Model_Marketplace[]/g" "${patch_php_file}"
    sed -i.bak "s/@return ListParticipations/@return MarketplaceWebServiceSellers_Model_ListParticipations/g" "${patch_php_file}"
    sed -i.bak "s/@return List<Participation>/@return MarketplaceWebServiceSellers_Model_Participation[]/g" "${patch_php_file}"
    sed -i.bak "s/@return ListMarketplaceParticipationsByNextTokenResult/@return MarketplaceWebServiceSellers_Model_ListMarketplaceParticipationsByNextTokenResult/g" "${patch_php_file}"
    sed -i.bak "s/@return ResponseHeaderMetadata/@return MarketplaceWebServiceSellers_Model_ResponseHeaderMetadata/g" "${patch_php_file}"
  fi
done
