<?php

use BlakePrinting\AmazonMws\EndpointDAO;
use BlakePrinting\AmazonMws\EndpointVO;
use BlakePrinting\AmazonMws\MarketplaceDAO;
use BlakePrinting\AmazonMws\MarketplaceVO;
use BlakePrinting\AmazonMws\RegionDAO;
use BlakePrinting\AmazonMws\RegionVO;
use BlakePrinting\Db\DbSeed;
use BlakePrinting\Util\Utils;

class AmazonMwsSeeder extends DbSeed {
    public function run() {
        $connection =& $this->getConnection();
        $regionDao =& RegionDAO::instance($connection);
        $endpointDao =& EndpointDAO::instance($connection);
        $marketplaceDao =& MarketplaceDAO::instance($connection);

        Utils::forEachConstantInObjectClass(
            $marketplaceDao,
            function($_, $constantName, $constantValue) use (&$regionDao, &$endpointDao, &$marketplaceDao) {
                $regionVo =& $regionDao->findByColumn("name", $constantValue["region"], PDO::PARAM_STR, null, null, "FOR UPDATE");
                $regionId = ($regionVo === null ? null : $regionVo->getId());

                if($regionId === null) {
                    $regionVo = new RegionVO();

                    $regionVo->setName($constantValue["region"]);

                    $regionId =& $regionDao->insert($regionVo);
                }

                $endpointVo =& $endpointDao->findByColumn("endpoint", $constantValue["endpoint"], PDO::PARAM_STR, null, null, "FOR UPDATE");
                $endpointId = ($endpointVo === null ? null : $endpointVo->getId());

                if($endpointId === null) {
                    $endpointVo = new EndpointVO();

                    $endpointVo->setEndpoint($constantValue["endpoint"]);

                    $endpointId = $endpointDao->insert($endpointVo);
                }

                $marketplaceProtoVo = new MarketplaceVO();

                $marketplaceProtoVo->setRegionId($regionId);
                $marketplaceProtoVo->setEndpointId($endpointId);
                $marketplaceProtoVo->setCountryCode($constantValue["country_code"]);
                $marketplaceProtoVo->setMwsMarketplaceId($constantValue["mws_marketplace_id"]);

                $marketplaceVo =& $marketplaceDao->findByPrototype($marketplaceProtoVo, null, null, "FOR UPDATE");

                if($marketplaceVo === null) {
                    $marketplaceDao->insert($marketplaceProtoVo);
                }
            }, "MARKETPLACE_"
        );
    }
}
