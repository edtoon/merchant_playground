<?php

use BlakePrinting\AmazonMws\EndpointDAO;
use BlakePrinting\AmazonMws\Sellers\CredentialDAO;
use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceDAO;
use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceParticipationDAO;
use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceListDAO;
use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceListResultDAO;
use BlakePrinting\Db\DbMigration;
use BlakePrinting\Users\UserDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class AmazonMwsSellers extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $credentialDao =& CredentialDAO::instance($connection);
        $credentialTbl = $credentialDao->formatSchemaAndTableName();
        $endpointDao =& EndpointDAO::instance($connection);
        $userDao =& UserDao::instance($connection);
        $userTbl = $userDao->formatSchemaAndTableName();
        $this->tableDao($credentialDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("endpoint_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("aws_access_key_id", "string", ["limit" => 50])
            ->addColumn("aws_secret_access_key", "string", ["limit" => 50])
            ->addColumn("seller_id", "string", ["limit" => 50])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("endpoint_id", $endpointDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("creator")
            ->addIndex("updated")
            ->addIndex("updater")
            ->addIndex(["endpoint_id", "aws_access_key_id", "seller_id"], ["unique" => true])
            ->addIndex("aws_access_key_id")
            ->addIndex("aws_secret_access_key")
            ->addIndex("seller_id")
            ->save()
        ;

        $marketplaceInstanceDao =& MarketplaceInstanceDAO::instance($connection);
        $this->tableDao($marketplaceInstanceDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["null" => true, "signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("credential_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("mws_marketplace_id", "string", ["limit" => 15])
            ->addColumn("name", "string", ["limit" => 50])
            ->addColumn("default_country_code", "string", ["limit" => 2])
            ->addColumn("default_currency_code", "string", ["limit" => 3])
            ->addColumn("default_language_code", "string", ["limit" => 10])
            ->addColumn("domain_name", "string", ["limit" => 191])
            ->addColumn("active", "boolean", ["default" => 0])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("credential_id", $credentialTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex(["credential_id", "mws_marketplace_id"], ["unique" => true])
            ->addIndex("mws_marketplace_id")
            ->addIndex("name")
            ->addIndex("default_country_code")
            ->addIndex("default_currency_code")
            ->addIndex("default_language_code")
            ->addIndex("domain_name")
            ->addIndex("active")
            ->save()
        ;

        $marketplaceInstanceParticipationDao =& MarketplaceInstanceParticipationDAO::instance($connection);
        $marketplaceInstanceTbl = $marketplaceInstanceDao->formatSchemaAndTableName();
        $this->tableDao($marketplaceInstanceParticipationDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["marketplace_instance_id"]])
            ->addColumn("marketplace_instance_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("has_seller_suspended_listings", "boolean", ["null" => true])
            ->addForeignKey("marketplace_instance_id", $marketplaceInstanceTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("has_seller_suspended_listings")
            ->save()
        ;

        $marketplaceInstanceListDao =& MarketplaceInstanceListDAO::instance($connection);
        $this->tableDao($marketplaceInstanceListDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("credential_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("next_token", "text", ["null" => true])
            ->addColumn("response_header_metadata", "text")
            ->addForeignKey("credential_id", $credentialTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $marketplaceInstanceListResultDao =& MarketplaceInstanceListResultDAO::instance($connection);
        $this->tableDao($marketplaceInstanceListResultDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["marketplace_instance_list_id", "marketplace_instance_id"]])
            ->addColumn("marketplace_instance_list_id", "biginteger", ["signed" => false])
            ->addColumn("marketplace_instance_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addForeignKey("marketplace_instance_list_id", $marketplaceInstanceListDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("marketplace_instance_id", $marketplaceInstanceTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(MarketplaceInstanceListResultDAO::instance($connection));
        $this->dropDao(MarketplaceInstanceListDAO::instance($connection));
        $this->dropDao(MarketplaceInstanceParticipationDAO::instance($connection));
        $this->dropDao(MarketplaceInstanceDAO::instance($connection));
        $this->dropDao(CredentialDAO::instance($connection));
    }
}
