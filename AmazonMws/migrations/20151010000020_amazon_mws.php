<?php

use BlakePrinting\AmazonMws\AddressDAO;
use BlakePrinting\AmazonMws\EndpointDAO;
use BlakePrinting\AmazonMws\MarketplaceDAO;
use BlakePrinting\AmazonMws\RegionDAO;
use BlakePrinting\Db\DbMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class AmazonMws extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $regionDao =& RegionDAO::instance($connection);
        $this->tableDao($regionDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("name", "string", ["limit" => 2])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $endpointDao =& EndpointDAO::instance($connection);
        $this->tableDao($endpointDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("endpoint", "string", ["limit" => 191])
            ->addIndex("endpoint", ["unique" => true])
            ->save()
        ;

        $marketplaceDao =& MarketplaceDAO::instance($connection);
        $this->tableDao($marketplaceDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("region_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("endpoint_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("mws_marketplace_id", "string", ["limit" => 15])
            ->addColumn("country_code", "string", ["limit" => 2])
            ->addForeignKey("region_id", $regionDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("endpoint_id", $endpointDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex(["region_id", "endpoint_id", "country_code"], ["unique" => true])
            ->addIndex("mws_marketplace_id", ["unique" => true])
            ->addIndex("country_code", ["unique" => true])
            ->save()
        ;

        $addressDao =& AddressDAO::instance($connection);
        $this->tableDao($addressDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("name", "string", ["limit" => 191, "null" => true])
            ->addColumn("line1", "string", ["limit" => 191, "null" => true])
            ->addColumn("line2", "string", ["limit" => 191, "null" => true])
            ->addColumn("line3", "string", ["limit" => 191, "null" => true])
            ->addColumn("city", "string", ["limit" => 191, "null" => true])
            ->addColumn("district", "string", ["limit" => 191, "null" => true])
            ->addColumn("state_or_region", "string", ["limit" => 191, "null" => true])
            ->addColumn("postal_code", "string", ["limit" => 191, "null" => true])
            ->addColumn("country_code", "string", ["limit" => 2, "null" => true])
            ->addColumn("phone", "string", ["limit" => 50, "null" => true])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name")
            ->addIndex("line1")
            ->addIndex("city")
            ->addIndex("district")
            ->addIndex("state_or_region")
            ->addIndex("postal_code")
            ->addIndex("country_code")
            ->addIndex("phone")
            ->save()
        ;
        $this->alterDao($addressDao, "ADD sha256 BINARY(32) NOT NULL");
        $this->alterDao($addressDao, "ADD UNIQUE (sha256)");
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(AddressDAO::instance($connection));
        $this->dropDao(MarketplaceDAO::instance($connection));
        $this->dropDao(EndpointDAO::instance($connection));
        $this->dropDao(RegionDAO::instance($connection));
    }
}
