<?php

use BlakePrinting\AmazonMws\Sellers\CredentialDAO;
use BlakePrinting\AmazonMws\TaskRequestDAO;
use BlakePrinting\AmazonMws\TaskResponseDAO;
use BlakePrinting\Db\DbMigration;
use BlakePrinting\Tasks\TaskDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class AmazonMwsTasks extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $taskRequestDao =& TaskRequestDAO::instance($connection);
        $taskDao =& TaskDAO::instance($connection);
        $credentialDao =& CredentialDAO::instance($connection);
        $this->tableDao($taskRequestDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("task_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("credential_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addForeignKey("task_id", $taskDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("credential_id", $credentialDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->save()
        ;

        $taskResponseDao =& TaskResponseDAO::instance($connection);
        $this->tableDao($taskResponseDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["task_request_id"]])
            ->addColumn("task_request_id", "biginteger", ["signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("next_token", "text", ["null" => true])
            ->addColumn("request_id", "string", ["null" => true, "limit" => 191])
            ->addColumn("response_context", "string", ["null" => true, "limit" => 191])
            ->addColumn("timestamp", "string", ["null" => true, "limit" => 191])
            ->addColumn("quota_max", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("quota_remaining", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("quota_resets_at", "string", ["null" => true, "limit" => 191])
            ->addForeignKey("task_request_id", $taskRequestDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("request_id")
            ->addIndex("quota_max")
            ->addIndex("quota_remaining")
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(TaskResponseDAO::instance($connection));
        $this->dropDao(TaskRequestDAO::instance($connection));
    }
}
