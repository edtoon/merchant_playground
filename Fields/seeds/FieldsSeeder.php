<?php

use BlakePrinting\Db\DbSeed;
use BlakePrinting\Fields\FieldTypeDAO;
use BlakePrinting\Fields\FieldTypeVO;
use BlakePrinting\Util\Utils;

class FieldsSeeder extends DbSeed {
    public function run() {
        $connection =& $this->getConnection();
        $fieldTypeDao =& FieldTypeDAO::instance($connection);

        Utils::forEachConstantInObjectClass(
            $fieldTypeDao,
            function($_, $constantName, $constantValue) use (&$fieldTypeDao) {
                $fieldTypeVo = new FieldTypeVO();

                $fieldTypeVo->setName($constantValue);

                $fieldTypeDao->findOrCreate($fieldTypeVo);
            }, "TYPE_"
        );
    }
}
