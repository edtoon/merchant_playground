<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\Fields\FieldDAO;
use BlakePrinting\Fields\FieldDomainDAO;
use BlakePrinting\Fields\FieldSchemaDAO;
use BlakePrinting\Fields\FieldTableDAO;
use BlakePrinting\Fields\FieldTypeDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class Fields extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $fieldDomainDao =& FieldDomainDAO::instance($connection);
        $this->tableDao($fieldDomainDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $fieldSchemaDao =& FieldSchemaDAO::instance($connection);
        $this->tableDao($fieldSchemaDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("field_domain_id", "integer", ["signed" => false])
            ->addColumn("name", "string", ["limit" => 191])
            ->addForeignKey("field_domain_id", $fieldDomainDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex(["field_domain_id", "name"], ["unique" => true])
            ->addIndex("name")
            ->save()
        ;

        $fieldTableDao =& FieldTableDAO::instance($connection);
        $this->tableDao($fieldTableDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("field_schema_id", "integer", ["signed" => false])
            ->addColumn("name", "string", ["limit" => 191])
            ->addForeignKey("field_schema_id", $fieldSchemaDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex(["field_schema_id", "name"], ["unique" => true])
            ->addIndex("name")
            ->save()
        ;

        $fieldTypeDao =& FieldTypeDAO::instance($connection);
        $this->tableDao($fieldTypeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $fieldDao =& FieldDAO::instance($connection);
        $this->tableDao($fieldDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("field_table_id", "integer", ["signed" => false])
            ->addColumn("field_type_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("name", "string", ["limit" => 191])
            ->addForeignKey("field_table_id", $fieldTableDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_type_id", $fieldTypeDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex(["field_table_id", "name"], ["unique" => true])
            ->addIndex("name")
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(FieldDAO::instance($connection));
        $this->dropDao(FieldTypeDAO::instance($connection));
        $this->dropDao(FieldTableDAO::instance($connection));
        $this->dropDao(FieldSchemaDAO::instance($connection));
        $this->dropDao(FieldDomainDAO::instance($connection));
    }
}
