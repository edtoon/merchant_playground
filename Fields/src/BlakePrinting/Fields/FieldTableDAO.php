<?php

namespace BlakePrinting\Fields;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static FieldTableDAO instance(PDO $connection = null, $schemaName = null)
 * @method FieldTableVO findById($id, $options = null, $useCache = false)
 * @method FieldTableVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FieldTableVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FieldTableVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method FieldTableVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FieldTableVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FieldTableVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method FieldTableVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class FieldTableDAO extends AbstractDataAccessObject {
}
