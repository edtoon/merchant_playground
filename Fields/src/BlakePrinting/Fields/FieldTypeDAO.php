<?php

namespace BlakePrinting\Fields;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static FieldTypeDAO instance(PDO $connection = null, $schemaName = null)
 * @method FieldTypeVO findById($id, $options = null, $useCache = false)
 * @method FieldTypeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FieldTypeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FieldTypeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method FieldTypeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FieldTypeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FieldTypeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method FieldTypeVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class FieldTypeDAO extends AbstractDataAccessObject {
    const TYPE_STRING = "string";
    const TYPE_TEXT = "text";
    const TYPE_UNSIGNED = "unsigned";
    const TYPE_SIGNED = "signed";
}
