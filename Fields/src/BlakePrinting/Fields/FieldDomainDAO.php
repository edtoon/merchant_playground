<?php

namespace BlakePrinting\Fields;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static FieldDomainDAO instance(PDO $connection = null, $schemaName = null)
 * @method FieldDomainVO findById($id, $options = null, $useCache = false)
 * @method FieldDomainVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FieldDomainVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FieldDomainVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method FieldDomainVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FieldDomainVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FieldDomainVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method FieldDomainVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class FieldDomainDAO extends AbstractDataAccessObject {
}
