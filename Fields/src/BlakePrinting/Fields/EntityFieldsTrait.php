<?php

namespace BlakePrinting\Fields;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\AbstractExtensibleEntity;
use BlakePrinting\Db\ExtensibleEntity;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use Exception;
use Stringy\StaticStringy as S;

trait EntityFieldsTrait {
    /** @var array */
    private $signedFields = [];
    /** @var array */
    private $unsignedFields = [];
    /** @var array */
    private $textFields = [];
    /** @var array */
    private $stringFields = [];
    /** @var AbstractDataAccessObject */
    private $signedFieldDao = null;
    /** @var AbstractDataAccessObject */
    private $unsignedFieldDao = null;
    /** @var AbstractDataAccessObject */
    private $textFieldDao = null;
    /** @var AbstractDataAccessObject */
    private $stringFieldDao = null;

    public function replaceFields() {
        if($this instanceof AbstractExtensibleEntity) {
            if($this->signedFieldDao === null) {
                $this->signedFieldDao = $this->getExtensionDAO("FieldSigned");
            }

            $this->replaceTypeFields($this->signedFieldDao, $this->signedFields);

            if($this->unsignedFieldDao === null) {
                $this->unsignedFieldDao = $this->getExtensionDAO("FieldUnsigned");
            }

            $this->replaceTypeFields($this->unsignedFieldDao, $this->unsignedFields);

            if($this->textFieldDao === null) {
                $this->textFieldDao = $this->getExtensionDAO("FieldText");
            }

            $this->replaceTypeFields($this->textFieldDao, $this->textFields);

            if($this->stringFieldDao === null) {
                $this->stringFieldDao = $this->getExtensionDAO("FieldString");
            }

            $this->replaceTypeFields($this->stringFieldDao, $this->stringFields);
        }
    }

    private function replaceTypeFields(AbstractDataAccessObject &$fieldDao, array &$typeFields) {
        if(!$this instanceof ExtensibleEntity) {
            throw new Exception("Tried to replace fields of object that's not an extensible entity: " . get_class($this));
        } else {
            $entityColumnName = $this->getEntityColumnName();
            $entityColumnValue = $this->getEntityColumnValue();

            PDOQuery::delete()
                ->from($fieldDao)
                ->where($entityColumnName, $this->getEntityColumnValue(), $this->getEntityColumnType())
                ->executeGetRowCount($fieldDao->getConnection())
            ;

            if(!empty($typeFields)) {
                $voReflectionClass = $fieldDao->getVoReflectionClass();
                $setEntityMethod = $voReflectionClass->getMethod("set" . S::upperCamelize($entityColumnName));
                $setFieldIdMethod = $voReflectionClass->getMethod("setFieldId");
                $setValueMethod = $voReflectionClass->getMethod("setValue");

                foreach($typeFields as $fieldId => &$fieldValues) {
                    foreach($fieldValues as &$fieldValue) {
                        $vo = $voReflectionClass->newInstance();

                        if($vo instanceof ValueObject) {
                            $setEntityMethod->invoke($vo, $entityColumnValue);
                            $setFieldIdMethod->invoke($vo, $fieldId);
                            $setValueMethod->invoke($vo, $fieldValue);

                            $fieldDao->insert($vo);
                        }
                    }

                    unset($fieldValue);
                }

                unset($fieldValues);
            }
        }
    }

    /**
     * @param int $fieldId
     * @param int $value
     */
    public function addSignedFieldValue($fieldId, $value) {
        if(!array_key_exists($fieldId, $this->signedFields)) {
            $this->signedFields[$fieldId] = [];
        }

        $this->signedFields[$fieldId][] = $value;
    }

    /**
     * @return array
     */
    public function getSignedFields() {
        return $this->signedFields;
    }

    /**
     * @param int $fieldId
     * @param int $value
     */
    public function addUnsignedFieldValue($fieldId, $value) {
        if(!array_key_exists($fieldId, $this->unsignedFields)) {
            $this->unsignedFields[$fieldId] = [];
        }

        $this->unsignedFields[$fieldId][] = $value;
    }

    /**
     * @return array
     */
    public function getUnsignedFields() {
        return $this->unsignedFields;
    }

    /**
     * @param int $fieldId
     * @param string $value
     */
    public function addTextFieldValue($fieldId, $value) {
        if(!array_key_exists($fieldId, $this->textFields)) {
            $this->textFields[$fieldId] = [];
        }

        $this->textFields[$fieldId][] = $value;
    }

    /**
     * @return array
     */
    public function getTextFields() {
        return $this->textFields;
    }

    /**
     * @param int $fieldId
     * @param string $value
     */
    public function addStringFieldValue($fieldId, $value) {
        if(!array_key_exists($fieldId, $this->stringFields)) {
            $this->stringFields[$fieldId] = [];
        }

        $this->stringFields[$fieldId][] = $value;
    }

    /**
     * @return array
     */
    public function getStringFields() {
        return $this->stringFields;
    }

    /**
     * @return AbstractDataAccessObject
     */
    public function getSignedFieldDao()
    {
        return $this->signedFieldDao;
    }

    /**
     * @param AbstractDataAccessObject $signedFieldDao
     */
    public function setSignedFieldDao(AbstractDataAccessObject $signedFieldDao)
    {
        $this->signedFieldDao = $signedFieldDao;
    }

    /**
     * @return AbstractDataAccessObject
     */
    public function getUnsignedFieldDao()
    {
        return $this->unsignedFieldDao;
    }

    /**
     * @param AbstractDataAccessObject $unsignedFieldDao
     */
    public function setUnsignedFieldDao(AbstractDataAccessObject $unsignedFieldDao)
    {
        $this->unsignedFieldDao = $unsignedFieldDao;
    }

    /**
     * @return AbstractDataAccessObject
     */
    public function getTextFieldDao()
    {
        return $this->textFieldDao;
    }

    /**
     * @param AbstractDataAccessObject $textFieldDao
     */
    public function setTextFieldDao(AbstractDataAccessObject $textFieldDao)
    {
        $this->textFieldDao = $textFieldDao;
    }

    /**
     * @return AbstractDataAccessObject
     */
    public function getStringFieldDao()
    {
        return $this->stringFieldDao;
    }

    /**
     * @param AbstractDataAccessObject $stringFieldDao
     */
    public function setStringFieldDao(AbstractDataAccessObject $stringFieldDao)
    {
        $this->stringFieldDao = $stringFieldDao;
    }
}
