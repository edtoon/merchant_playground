<?php

namespace BlakePrinting\Fields;

use BlakePrinting\Db\ValueObject;

class FieldTableVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $fieldSchemaId = null;
    /** @var string */
    private $name = null;

    /**
     * @param int $fieldSchemaId
     * @param string $name
     */
    public function __construct($fieldSchemaId = null, $name = null) {
        $this->fieldSchemaId = $fieldSchemaId;
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getFieldSchemaId()
    {
        return $this->fieldSchemaId;
    }

    /**
     * @param int $fieldSchemaId
     */
    public function setFieldSchemaId($fieldSchemaId)
    {
        $this->fieldSchemaId = $fieldSchemaId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
