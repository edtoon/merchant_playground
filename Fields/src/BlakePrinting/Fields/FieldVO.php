<?php

namespace BlakePrinting\Fields;

use BlakePrinting\Db\ValueObject;

class FieldVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $fieldTableId = null;
    /** @var int */
    private $fieldTypeId = null;
    /** @var string */
    private $name = null;

    /**
     * @param int $fieldTableId
     * @param int $fieldTypeId
     * @param string $name
     */
    public function __construct($fieldTableId = null, $fieldTypeId = null, $name = null) {
        $this->fieldTableId = $fieldTableId;
        $this->fieldTypeId = $fieldTypeId;
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getFieldTableId()
    {
        return $this->fieldTableId;
    }

    /**
     * @param int $fieldTableId
     */
    public function setFieldTableId($fieldTableId)
    {
        $this->fieldTableId = $fieldTableId;
    }

    /**
     * @return int
     */
    public function getFieldTypeId()
    {
        return $this->fieldTypeId;
    }

    /**
     * @param int $fieldTypeId
     */
    public function setFieldTypeId($fieldTypeId)
    {
        $this->fieldTypeId = $fieldTypeId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
