<?php

namespace BlakePrinting\Fields;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static FieldSchemaDAO instance(PDO $connection = null, $schemaName = null)
 * @method FieldSchemaVO findById($id, $options = null, $useCache = false)
 * @method FieldSchemaVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FieldSchemaVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FieldSchemaVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method FieldSchemaVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FieldSchemaVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FieldSchemaVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method FieldSchemaVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class FieldSchemaDAO extends AbstractDataAccessObject {
}
