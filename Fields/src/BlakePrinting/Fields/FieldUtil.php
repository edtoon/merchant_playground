<?php

namespace BlakePrinting\Fields;

use PDO;

class FieldUtil {
    private function __construct() { }

    /**
     * @param array $fieldSpec
     * @param PDO $connection
     * @param bool $create
     * @return FieldVO
     */
    public static function &findFieldVoBySpec(array $fieldSpec, PDO $connection = null, $create = false) {
        $fieldDomainDao =& FieldDomainDAO::instance($connection);
        $fieldDomainVo = new FieldDomainVO($fieldSpec["domain"]);

        if($create) {
            $fieldDomainVo =& $fieldDomainDao->findOrCreate($fieldDomainVo);
        } else {
            $fieldDomainVo =& $fieldDomainDao->findByPrototype($fieldDomainVo);
        }

        $fieldSchemaDao =& FieldSchemaDAO::instance($connection);
        $fieldSchemaVo = new FieldSchemaVO($fieldDomainVo->getId(), $fieldSpec["schema"]);

        if($create) {
            $fieldSchemaVo =& $fieldSchemaDao->findOrCreate($fieldSchemaVo);
        } else {
            $fieldSchemaVo =& $fieldSchemaDao->findByPrototype($fieldSchemaVo);
        }

        $fieldTableDao =& FieldTableDAO::instance($connection);
        $fieldTableVo = new FieldTableVO($fieldSchemaVo->getId(), $fieldSpec["table"]);

        if($create) {
            $fieldTableVo =& $fieldTableDao->findOrCreate($fieldTableVo);
        } else {
            $fieldTableVo =& $fieldTableDao->findByPrototype($fieldTableVo);
        }

        $fieldTypeVo =& FieldTypeDAO::instance($connection)->findByColumn(
            "name", $fieldSpec["type"], PDO::PARAM_STR, null, null, null, true
        );

        $fieldDao =& FieldDAO::instance($connection);
        $fieldVoProto = new FieldVO($fieldTableVo->getId(), $fieldTypeVo->getId(), $fieldSpec["field"]);

        if($create) {
            return $fieldDao->findOrCreate($fieldVoProto);
        } else {
            return $fieldDao->findByPrototype($fieldVoProto);
        }
    }
}
