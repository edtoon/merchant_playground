<?php

namespace BlakePrinting\Fields;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static FieldDAO instance(PDO $connection = null, $schemaName = null)
 * @method FieldVO findById($id, $options = null, $useCache = false)
 * @method FieldVO findByColumn($column, $value, $pdo = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FieldVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FieldVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method FieldVO[] findAllByColumn($column, $value, $pdo = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FieldVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FieldVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method FieldVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class FieldDAO extends AbstractDataAccessObject {
}
