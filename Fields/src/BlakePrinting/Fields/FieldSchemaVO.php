<?php

namespace BlakePrinting\Fields;

use BlakePrinting\Db\ValueObject;

class FieldSchemaVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $fieldDomainId = null;
    /** @var string */
    private $name = null;

    /**
     * @param int $fieldDomainId
     * @param string $name
     */
    public function __construct($fieldDomainId = null, $name = null) {
        $this->fieldDomainId = $fieldDomainId;
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getFieldDomainId()
    {
        return $this->fieldDomainId;
    }

    /**
     * @param int $fieldDomainId
     */
    public function setFieldDomainId($fieldDomainId)
    {
        $this->fieldDomainId = $fieldDomainId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
