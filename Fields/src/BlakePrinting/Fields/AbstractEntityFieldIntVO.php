<?php

namespace BlakePrinting\Fields;

use BlakePrinting\Db\ValueObject;

abstract class AbstractEntityFieldIntVO implements ValueObject {
    /** @var int */
    private $fieldId = null;
    /** @var int */
    private $value = null;

    public function __construct($fieldId = null, $value = null) {
        $this->fieldId = $fieldId;
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getFieldId()
    {
        return $this->fieldId;
    }

    /**
     * @param int $fieldId
     */
    public function setFieldId($fieldId)
    {
        $this->fieldId = $fieldId;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}
