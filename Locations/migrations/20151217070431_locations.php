<?php

use BlakePrinting\Contacts\ContactAddressDAO;
use BlakePrinting\Db\DbMigration;
use BlakePrinting\Locations\LocationContactAddressDAO;
use BlakePrinting\Locations\LocationOrganizationDAO;
use BlakePrinting\Locations\LocationDAO;
use BlakePrinting\Locations\LocationTypeDAO;
use BlakePrinting\Organizations\OrganizationDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class Locations extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $locationTypeDao = new LocationTypeDAO($connection);
        $locationTypeTbl = $locationTypeDao->formatSchemaAndTableName();
        $this->tableDao($locationTypeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("parent_location_type_id", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("name", "string", ["limit" => 191])
            ->addForeignKey("parent_location_type_id", $locationTypeTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $locationDao = new LocationDAO($connection);
        $locationTbl = $locationDao->formatSchemaAndTableName();
        $this->tableDao($locationDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("parent_location_id", "integer", ["null" => true, "signed" => false])
            ->addColumn("location_type_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("code", "string", ["limit" => 191])
            ->addColumn("name", "string", ["limit" => 191])
            ->addForeignKey("parent_location_id", $locationTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("location_type_id", $locationTypeTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("code", ["unique" => true])
            ->addIndex("name")
            ->save()
        ;

        $locationContactAddressDao = new LocationContactAddressDAO($connection);
        $contactAddressDao = new ContactAddressDAO($connection);
        $contactAddressTbl = $contactAddressDao->formatSchemaAndTableName();
        $this->tableDao($locationContactAddressDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["location_id", "contact_address_id"]])
            ->addColumn("location_id", "integer", ["signed" => false])
            ->addColumn("contact_address_id", "integer", ["signed" => false])
            ->addForeignKey("location_id", $locationTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("contact_address_id", $contactAddressTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $locationOrganizationDao = new LocationOrganizationDAO($connection);
        $organizationDao = new OrganizationDAO($connection);
        $organizationTbl = $organizationDao->formatSchemaAndTableName();
        $this->tableDao($locationOrganizationDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["location_id", "organization_id"]])
            ->addColumn("location_id", "integer", ["signed" => false])
            ->addColumn("organization_id", "integer", ["signed" => false])
            ->addForeignKey("location_id", $locationTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("organization_id", $organizationTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(new LocationOrganizationDAO($connection));
        $this->dropDao(new LocationContactAddressDAO($connection));
        $this->dropDao(new LocationDAO($connection));
        $this->dropDao(new LocationTypeDAO($connection));
    }
}
