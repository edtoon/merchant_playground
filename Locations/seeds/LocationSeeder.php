<?php

use BlakePrinting\Db\DbSeed;
use BlakePrinting\Locations\LocationTypeDAO;
use BlakePrinting\Locations\LocationTypeVO;

class LocationSeeder extends DbSeed {
    public final function run() {
        $locationTypeDao =& LocationTypeDAO::instance($this->getConnection());

        $locationTypeDao->findOrCreate(new LocationTypeVO(LocationTypeDAO::LOCATION_TYPE_VIRTUAL));

        $physicalLocationTypeVo =& $locationTypeDao->findOrCreate(new LocationTypeVO(LocationTypeDAO::LOCATION_TYPE_PHYSICAL));
        $physicalLocationTypeId = $physicalLocationTypeVo->getId();

        $locationTypeDao->findOrCreate(new LocationTypeVO(LocationTypeDAO::LOCATION_TYPE_TRANSIT));
        $locationTypeDao->findOrCreate(new LocationTypeVO(LocationTypeDAO::LOCATION_TYPE_PARTNER));

        $buildingLocationTypeVo =& $locationTypeDao->findOrCreate(new LocationTypeVO(LocationTypeDAO::LOCATION_TYPE_BUILDING, $physicalLocationTypeId));
        $buildingLocationTypeId = $buildingLocationTypeVo->getId();

        $locationTypeDao->findOrCreate(new LocationTypeVO(LocationTypeDAO::LOCATION_TYPE_AREA, $buildingLocationTypeId));

        $roomLocationTypeVo =& $locationTypeDao->findOrCreate(new LocationTypeVO(LocationTypeDAO::LOCATION_TYPE_ROOM, $buildingLocationTypeId));
        $roomLocationTypeId = $roomLocationTypeVo->getId();

        $locationTypeDao->findOrCreate(new LocationTypeVO(LocationTypeDAO::LOCATION_TYPE_OFFICE, $roomLocationTypeId));

        $cabinetLocationTypeVo =& $locationTypeDao->findOrCreate(new LocationTypeVO(LocationTypeDAO::LOCATION_TYPE_CABINET, $physicalLocationTypeId));
        $cabinetLocationTypeId = $cabinetLocationTypeVo->getId();

        $locationTypeDao->findOrCreate(new LocationTypeVO(LocationTypeDAO::LOCATION_TYPE_DOOR, $cabinetLocationTypeId));

        $locationTypeDao->findOrCreate(new LocationTypeVO(LocationTypeDAO::LOCATION_TYPE_BENCH, $physicalLocationTypeId));
        $locationTypeDao->findOrCreate(new LocationTypeVO(LocationTypeDAO::LOCATION_TYPE_BOX, $physicalLocationTypeId));
        $locationTypeDao->findOrCreate(new LocationTypeVO(LocationTypeDAO::LOCATION_TYPE_DESK, $physicalLocationTypeId));
        $locationTypeDao->findOrCreate(new LocationTypeVO(LocationTypeDAO::LOCATION_TYPE_DRAWER, $physicalLocationTypeId));
        $locationTypeDao->findOrCreate(new LocationTypeVO(LocationTypeDAO::LOCATION_TYPE_PALLET, $physicalLocationTypeId));
        $locationTypeDao->findOrCreate(new LocationTypeVO(LocationTypeDAO::LOCATION_TYPE_RACK, $physicalLocationTypeId));
        $locationTypeDao->findOrCreate(new LocationTypeVO(LocationTypeDAO::LOCATION_TYPE_SAFE, $physicalLocationTypeId));
        $locationTypeDao->findOrCreate(new LocationTypeVO(LocationTypeDAO::LOCATION_TYPE_SHELF, $physicalLocationTypeId));
        $locationTypeDao->findOrCreate(new LocationTypeVO(LocationTypeDAO::LOCATION_TYPE_TABLE, $physicalLocationTypeId));
        $locationTypeDao->findOrCreate(new LocationTypeVO(LocationTypeDAO::LOCATION_TYPE_TRAY, $physicalLocationTypeId));
    }
}
