<?php

namespace BlakePrinting\Locations;

use BlakePrinting\Db\ValueObject;

abstract class AbstractEntityLocationVO implements ValueObject {
    /** @var int */
    private $locationId = null;

    /**
     * @param int $locationId
     */
    public function __construct($locationId = null) {
        $this->locationId = $locationId;
    }

    /**
     * @return int
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * @param int $locationId
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;
    }
}
