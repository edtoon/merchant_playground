<?php

namespace BlakePrinting\Locations;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static LocationDAO instance(PDO $connection = null, $schemaName = null)
 * @method LocationVO findById($id, $options = null, $useCache = false)
 * @method LocationVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method LocationVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method LocationVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method LocationVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method LocationVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method LocationVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method LocationVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class LocationDAO extends AbstractDataAccessObject {
    /**
     * @param int $childLocationId
     * @param int $parentLocationId
     * @param bool $useCache
     * @return bool
     */
    public function checkLocationDescendsFrom($childLocationId, $parentLocationId, $useCache = true) {
        do {
            $childLocationVo = $this->findById($childLocationId, null, $useCache);
            $nextParentLocationId = $childLocationVo->getParentLocationId();

            if($nextParentLocationId == $parentLocationId) {
                return true;
            }

            $childLocationId = $nextParentLocationId;
        } while(!empty($childLocationVo->getParentLocationId()));

        return false;
    }
}
