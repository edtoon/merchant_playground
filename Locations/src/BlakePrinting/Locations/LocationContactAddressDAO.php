<?php

namespace BlakePrinting\Locations;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static LocationContactAddressDAO instance(PDO $connection = null, $schemaName = null)
 * @method LocationContactAddressVO findById($id, $options = null, $useCache = false)
 * @method LocationContactAddressVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method LocationContactAddressVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method LocationContactAddressVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method LocationContactAddressVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method LocationContactAddressVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method LocationContactAddressVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class LocationContactAddressDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $locationId
     * @param int $contactAddressId
     * @return int
     */
    public function &store($locationId, $contactAddressId) {
        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value("location_id", $locationId, PDO::PARAM_INT)
            ->value("contact_address_id", $contactAddressId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
