<?php

namespace BlakePrinting\Locations;

use BlakePrinting\Contacts\AbstractEntityContactAddressVO;

class LocationContactAddressVO extends AbstractEntityContactAddressVO {
    /** @var int */
    private $locationId = null;

    /**
     * @return int
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * @param int $locationId
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;
    }
}
