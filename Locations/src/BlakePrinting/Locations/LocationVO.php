<?php

namespace BlakePrinting\Locations;

use BlakePrinting\Db\ValueObject;
use JsonSerializable;

class LocationVO implements JsonSerializable, ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $parentLocationId = null;
    /** @var int */
    private $locationTypeId = null;
    /** @var string */
    private $code = null;
    /** @var string */
    private $name = null;

    public function __construct($code = null, $name = null, $locationTypeId = null, $parentLocationId = null) {
        $this->code = $code;
        $this->name = $name;
        $this->locationTypeId = $locationTypeId;
        $this->parentLocationId = $parentLocationId;
    }

    public function jsonSerialize() {
        return [
            "id" => $this->id,
            "created" => $this->created,
            "updated" => $this->updated,
            "parent_location_id" => $this->parentLocationId,
            "location_type_id" => $this->locationTypeId,
            "code" => $this->code,
            "name" => $this->name
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getParentLocationId()
    {
        return $this->parentLocationId;
    }

    /**
     * @param int $parentLocationId
     */
    public function setParentLocationId($parentLocationId)
    {
        $this->parentLocationId = $parentLocationId;
    }

    /**
     * @return int
     */
    public function getLocationTypeId()
    {
        return $this->locationTypeId;
    }

    /**
     * @param int $locationTypeId
     */
    public function setLocationTypeId($locationTypeId)
    {
        $this->locationTypeId = $locationTypeId;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
