<?php

namespace BlakePrinting\Locations;

use BlakePrinting\Db\ValueObject;

class LocationTypeVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $parentLocationTypeId = null;
    /** @var string */
    private $name = null;

    public function __construct($name = null, $parentLocationTypeId = null) {
        $this->name = $name;
        $this->parentLocationTypeId = $parentLocationTypeId;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getParentLocationTypeId()
    {
        return $this->parentLocationTypeId;
    }

    /**
     * @param int $parentLocationTypeId
     */
    public function setParentLocationTypeId($parentLocationTypeId)
    {
        $this->parentLocationTypeId = $parentLocationTypeId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
