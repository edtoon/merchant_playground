<?php

namespace BlakePrinting\Locations;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static LocationTypeDAO instance(PDO $connection = null, $schemaName = null)
 * @method LocationTypeVO findById($id, $options = null, $useCache = false)
 * @method LocationTypeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method LocationTypeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method LocationTypeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method LocationTypeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method LocationTypeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method LocationTypeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method LocationTypeVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class LocationTypeDAO extends AbstractDataAccessObject {
    const LOCATION_TYPE_VIRTUAL = "virtual";
    const LOCATION_TYPE_PHYSICAL = "physical";
    const LOCATION_TYPE_TRANSIT = "transit";
    const LOCATION_TYPE_PARTNER = "partner";

    const LOCATION_TYPE_BUILDING = "building";
    const LOCATION_TYPE_AREA = "area";
    const LOCATION_TYPE_OFFICE = "office";
    const LOCATION_TYPE_ROOM = "room";

    const LOCATION_TYPE_CABINET = "cabinet";
    const LOCATION_TYPE_DOOR = "door";

    const LOCATION_TYPE_BENCH = "bench";
    const LOCATION_TYPE_BOX = "box";
    const LOCATION_TYPE_DESK = "desk";
    const LOCATION_TYPE_DRAWER = "drawer";
    const LOCATION_TYPE_PALLET = "pallet";
    const LOCATION_TYPE_RACK = "rack";
    const LOCATION_TYPE_SAFE = "safe";
    const LOCATION_TYPE_SHELF = "shelf";
    const LOCATION_TYPE_TABLE = "table";
    const LOCATION_TYPE_TRAY = "tray";
}
