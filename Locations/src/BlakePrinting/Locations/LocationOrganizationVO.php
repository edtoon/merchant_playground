<?php

namespace BlakePrinting\Locations;

use BlakePrinting\Organizations\AbstractEntityOrganizationVO;

class LocationOrganizationVO extends AbstractEntityOrganizationVO {
    /** @var int */
    private $locationId = null;

    /**
     * @return int
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * @param int $locationId
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;
    }
}
