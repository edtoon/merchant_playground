<?php

namespace BlakePrinting\Locations;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static LocationOrganizationDAO instance(PDO $connection = null, $schemaName = null)
 * @method LocationOrganizationVO findById($id, $options = null, $useCache = false)
 * @method LocationOrganizationVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method LocationOrganizationVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method LocationOrganizationVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method LocationOrganizationVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method LocationOrganizationVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method LocationOrganizationVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class LocationOrganizationDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $locationId
     * @param int $organizationId
     * @return int
     */
    public function &store($locationId, $organizationId) {
        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value("location_id", $locationId, PDO::PARAM_INT)
            ->value("organization_id", $organizationId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
