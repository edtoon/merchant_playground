<?php

namespace BlakePrinting\Locations;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use PDO;
use Stringy\StaticStringy as S;

class AbstractEntityLocationDAO extends AbstractDataAccessObject {
    /** @var string */
    private $entityColumnName = null;

    public function __construct(PDO $connection = null, $schemaName = null, $entityColumnName = null) {
        parent::__construct($connection, $schemaName, null, null, null);

        $this->entityColumnName = $entityColumnName;
    }

    public function store(AbstractEntityLocationVO $abstractEntityLocationVo) {
        $voReflectionClass =& $this->getVoReflectionClass();
        $getEntityPrimaryKeyMethod = $voReflectionClass->getMethod("get" . S::upperCamelize($this->entityColumnName));

        return PDOQuery::insert()
            ->into($this)
            ->value($this->entityColumnName, $getEntityPrimaryKeyMethod->invoke($abstractEntityLocationVo), PDO::PARAM_INT)
            ->value("location_id", $abstractEntityLocationVo->getLocationId(), PDO::PARAM_INT)
            ->onDuplicateValue("location_id", "VALUES(location_id)")
            ->executeGetRowCount($this->getConnection())
        ;
    }

}
