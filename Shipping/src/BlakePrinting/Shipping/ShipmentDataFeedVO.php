<?php

namespace BlakePrinting\Shipping;

use BlakePrinting\DataSources\AbstractEntityDataFeedVO;

class ShipmentDataFeedVO extends AbstractEntityDataFeedVO {
    /** @var int */
    private $shipmentId = null;

    public function __construct($shipmentId = null, $dataFeedId = null) {
        parent::__construct($dataFeedId);

        $this->shipmentId = $shipmentId;
    }

    /**
     * @return int
     */
    public function getShipmentId()
    {
        return $this->shipmentId;
    }

    /**
     * @param int $shipmentId
     */
    public function setShipmentId($shipmentId)
    {
        $this->shipmentId = $shipmentId;
    }
}
