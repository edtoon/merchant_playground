<?php

namespace BlakePrinting\Shipping;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ShipmentFieldSignedDAO instance(PDO $connection = null, $schemaName = null)
 * @method ShipmentFieldSignedVO findById($id, $options = null, $useCache = false)
 * @method ShipmentFieldSignedVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ShipmentFieldSignedVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ShipmentFieldSignedVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ShipmentFieldSignedVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ShipmentFieldSignedVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ShipmentFieldSignedVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ShipmentFieldSignedDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
