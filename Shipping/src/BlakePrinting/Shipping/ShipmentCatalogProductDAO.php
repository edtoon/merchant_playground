<?php

namespace BlakePrinting\Shipping;

use BlakePrinting\Catalog\AbstractEntityCatalogProductDAO;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ShipmentCatalogProductDAO instance(PDO $connection = null, $schemaName = null)
 * @method ShipmentCatalogProductVO findById($id, $options = null, $useCache = false)
 * @method ShipmentCatalogProductVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ShipmentCatalogProductVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ShipmentCatalogProductVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ShipmentCatalogProductVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ShipmentCatalogProductVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ShipmentCatalogProductVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method void store(ShipmentCatalogProductVO $vo)
 */
class ShipmentCatalogProductDAO extends AbstractEntityCatalogProductDAO {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, "shipment_id");
    }
}
