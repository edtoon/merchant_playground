<?php

namespace BlakePrinting\Shipping;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ShipmentDAO instance(PDO $connection = null, $schemaName = null)
 * @method ShipmentVO findById($id, $options = null, $useCache = false)
 * @method ShipmentVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ShipmentVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ShipmentVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ShipmentVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ShipmentVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ShipmentVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ShipmentVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class ShipmentDAO extends AbstractDataAccessObject {
}
