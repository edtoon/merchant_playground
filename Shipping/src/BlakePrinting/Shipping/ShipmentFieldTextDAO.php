<?php

namespace BlakePrinting\Shipping;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ShipmentFieldTextDAO instance(PDO $connection = null, $schemaName = null)
 * @method ShipmentFieldTextVO findById($id, $options = null, $useCache = false)
 * @method ShipmentFieldTextVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ShipmentFieldTextVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ShipmentFieldTextVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ShipmentFieldTextVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ShipmentFieldTextVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ShipmentFieldTextVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ShipmentFieldTextDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
