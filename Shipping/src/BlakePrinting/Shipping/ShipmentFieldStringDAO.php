<?php

namespace BlakePrinting\Shipping;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ShipmentFieldStringDAO instance(PDO $connection = null, $schemaName = null)
 * @method ShipmentFieldStringVO findById($id, $options = null, $useCache = false)
 * @method ShipmentFieldStringVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ShipmentFieldStringVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ShipmentFieldStringVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ShipmentFieldStringVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ShipmentFieldStringVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ShipmentFieldStringVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ShipmentFieldStringDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
