<?php

namespace BlakePrinting\Shipping;

use BlakePrinting\Db\ValueObject;

abstract class AbstractEntityShipmentVO implements ValueObject {
    /** @var int */
    private $shipmentId = null;

    /**
     * @param int $shipmentId
     */
    public function __construct($shipmentId = null) {
        $this->shipmentId = $shipmentId;
    }

    /**
     * @return int
     */
    public function getShipmentId()
    {
        return $this->shipmentId;
    }

    /**
     * @param int $shipmentId
     */
    public function setShipmentId($shipmentId)
    {
        $this->shipmentId = $shipmentId;
    }
}
