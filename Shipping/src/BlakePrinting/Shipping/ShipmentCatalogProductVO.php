<?php

namespace BlakePrinting\Shipping;

use BlakePrinting\Catalog\AbstractEntityCatalogProductVO;

class ShipmentCatalogProductVO extends AbstractEntityCatalogProductVO {
    /** @var int */
    private $shipmentId = null;

    public function __construct($shipmentId = null, $catalogProductId = null, $quantity = null) {
        parent::__construct($catalogProductId, $quantity);

        $this->shipmentId = $shipmentId;
    }

    /**
     * @return int
     */
    public function getShipmentId()
    {
        return $this->shipmentId;
    }

    /**
     * @param int $shipmentId
     */
    public function setShipmentId($shipmentId)
    {
        $this->shipmentId = $shipmentId;
    }
}
