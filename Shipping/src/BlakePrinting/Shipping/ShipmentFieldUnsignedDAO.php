<?php

namespace BlakePrinting\Shipping;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ShipmentFieldUnsignedDAO instance(PDO $connection = null, $schemaName = null)
 * @method ShipmentFieldUnsignedVO findById($id, $options = null, $useCache = false)
 * @method ShipmentFieldUnsignedVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ShipmentFieldUnsignedVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ShipmentFieldUnsignedVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ShipmentFieldUnsignedVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ShipmentFieldUnsignedVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ShipmentFieldUnsignedVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ShipmentFieldUnsignedDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
