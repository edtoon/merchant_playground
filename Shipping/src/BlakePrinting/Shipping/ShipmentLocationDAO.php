<?php

namespace BlakePrinting\Shipping;

use BlakePrinting\Db\ValueObject;
use BlakePrinting\Locations\AbstractEntityLocationDAO;
use PDO;

/**
 * @method static ShipmentLocationDAO instance(PDO $connection = null, $schemaName = null)
 * @method ShipmentLocationVO findById($id, $options = null, $useCache = false)
 * @method ShipmentLocationVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ShipmentLocationVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ShipmentLocationVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ShipmentLocationVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ShipmentLocationVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ShipmentLocationVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method void store(ShipmentLocationVO $vo)
 */
class ShipmentLocationDAO extends AbstractEntityLocationDAO {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, "shipment_id");
    }
}
