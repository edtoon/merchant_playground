<?php

namespace BlakePrinting\Shipping;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ShippingCarrierDAO instance(PDO $connection = null, $schemaName = null)
 * @method ShippingCarrierVO findById($id, $options = null, $useCache = false)
 * @method ShippingCarrierVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ShippingCarrierVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ShippingCarrierVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ShippingCarrierVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ShippingCarrierVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ShippingCarrierVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ShippingCarrierVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class ShippingCarrierDAO extends AbstractDataAccessObject {
    const SHIPPING_CARRIER_USPS = "USPS";
    const SHIPPING_CARRIER_UPS = "UPS";
    const SHIPPING_CARRIER_FEDEX = "Fedex";
    const SHIPPING_CARRIER_DHL_DE = "DHL Germany";
    const SHIPPING_CARRIER_DHL_GLOBAL = "DHL Global Mail";
    const SHIPPING_CARRIER_DHL_EXPRESS = "DHL Express";
    const SHIPPING_CARRIER_DHL_FREIGHT = "DHL Freight";
    const SHIPPING_CARRIER_LASER = "LaserShip";
    const SHIPPING_CARRIER_ONTRAC = "OnTrac";
    const SHIPPING_CARRIER_GSO = "GSO";
    const SHIPPING_CARRIER_CANADA = "Canada Post";
    const SHIPPING_CARRIER_CHINA = "China Post";
    const SHIPPING_CARRIER_DE_POST = "Deutsche Post";
    const SHIPPING_CARRIER_GLS = "GLS";
    const SHIPPING_CARRIER_DPD = "DPD";
    const SHIPPING_CARRIER_JAPAN = "JP Post";
    const SHIPPING_CARRIER_UK = "Royal Mail";
}
