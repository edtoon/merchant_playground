<?php

namespace BlakePrinting\Shipping;

use BlakePrinting\Db\ValueObject;
use JsonSerializable;

class ShipmentVO implements ValueObject, JsonSerializable {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $creator = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $updater = null;
    /** @var int */
    private $parentShipmentId = null;
    /** @var int */
    private $senderOrganizationId = null;
    /** @var int */
    private $recipientOrganizationId = null;
    /** @var int */
    private $contactPhoneId = null;
    /** @var int */
    private $contactPersonId = null;
    /** @var int */
    private $contactAddressId = null;
    /** @var int */
    private $shippingCarrierId = null;
    /** @var int */
    private $shipped = null;
    /** @var int */
    private $shipmentStatusId = null;

    public function jsonSerialize() {
        return [
            "id" => $this->id,
            "created" => $this->created,
            "creator" => $this->creator,
            "updated" => $this->updated,
            "updater" => $this->updater,
            "parent_shipment_id" => $this->parentShipmentId,
            "sender_organization_id" => $this->senderOrganizationId,
            "recipient_organization_id" => $this->recipientOrganizationId,
            "contact_phone_id" => $this->contactPhoneId,
            "contact_person_id" => $this->contactPersonId,
            "contact_address_id" => $this->contactAddressId,
            "shipping_carrier_id" => $this->shippingCarrierId,
            "shipped" => $this->shipped,
            "shipment_status_id" => $this->shipmentStatusId,
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param int $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getUpdater()
    {
        return $this->updater;
    }

    /**
     * @param int $updater
     */
    public function setUpdater($updater)
    {
        $this->updater = $updater;
    }

    /**
     * @return int
     */
    public function getParentShipmentId()
    {
        return $this->parentShipmentId;
    }

    /**
     * @param int $parentShipmentId
     */
    public function setParentShipmentId($parentShipmentId)
    {
        $this->parentShipmentId = $parentShipmentId;
    }

    /**
     * @return int
     */
    public function getSenderOrganizationId()
    {
        return $this->senderOrganizationId;
    }

    /**
     * @param int $senderOrganizationId
     */
    public function setSenderOrganizationId($senderOrganizationId)
    {
        $this->senderOrganizationId = $senderOrganizationId;
    }

    /**
     * @return int
     */
    public function getRecipientOrganizationId()
    {
        return $this->recipientOrganizationId;
    }

    /**
     * @param int $recipientOrganizationId
     */
    public function setRecipientOrganizationId($recipientOrganizationId)
    {
        $this->recipientOrganizationId = $recipientOrganizationId;
    }

    /**
     * @return int
     */
    public function getContactPhoneId()
    {
        return $this->contactPhoneId;
    }

    /**
     * @param int $contactPhoneId
     */
    public function setContactPhoneId($contactPhoneId)
    {
        $this->contactPhoneId = $contactPhoneId;
    }

    /**
     * @return int
     */
    public function getContactPersonId()
    {
        return $this->contactPersonId;
    }

    /**
     * @param int $contactPersonId
     */
    public function setContactPersonId($contactPersonId)
    {
        $this->contactPersonId = $contactPersonId;
    }

    /**
     * @return int
     */
    public function getContactAddressId()
    {
        return $this->contactAddressId;
    }

    /**
     * @param int $contactAddressId
     */
    public function setContactAddressId($contactAddressId)
    {
        $this->contactAddressId = $contactAddressId;
    }

    /**
     * @return int
     */
    public function getShippingCarrierId()
    {
        return $this->shippingCarrierId;
    }

    /**
     * @param int $shippingCarrierId
     */
    public function setShippingCarrierId($shippingCarrierId)
    {
        $this->shippingCarrierId = $shippingCarrierId;
    }

    /**
     * @return int
     */
    public function getShipped()
    {
        return $this->shipped;
    }

    /**
     * @param int $shipped
     */
    public function setShipped($shipped)
    {
        $this->shipped = $shipped;
    }

    /**
     * @return int
     */
    public function getShipmentStatusId()
    {
        return $this->shipmentStatusId;
    }

    /**
     * @param int $shipmentStatusId
     */
    public function setShipmentStatusId($shipmentStatusId)
    {
        $this->shipmentStatusId = $shipmentStatusId;
    }
}
