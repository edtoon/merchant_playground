<?php

namespace BlakePrinting\Shipping;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ShipmentStatusDAO instance(PDO $connection = null, $schemaName = null)
 * @method ShipmentStatusVO findById($id, $options = null, $useCache = false)
 * @method ShipmentStatusVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ShipmentStatusVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ShipmentStatusVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ShipmentStatusVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ShipmentStatusVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ShipmentStatusVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ShipmentStatusVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class ShipmentStatusDAO extends AbstractDataAccessObject {
    const SHIPMENT_STATUS_CANCELLED = "cancelled";
    const SHIPMENT_STATUS_PENDING = "pending";
    const SHIPMENT_STATUS_UNKNOWN = "unknown";
    const SHIPMENT_STATUS_TRANSIT = "transit";
    const SHIPMENT_STATUS_DELIVERY = "delivery";
    const SHIPMENT_STATUS_DELIVERED = "delivered";
    const SHIPMENT_STATUS_PICKUP = "pickup";
    const SHIPMENT_STATUS_RETURNING = "returning";
    const SHIPMENT_STATUS_RETURNED = "returned";
    const SHIPMENT_STATUS_REFUSED = "refused";
    const SHIPMENT_STATUS_FAILED = "failed";
    const SHIPMENT_STATUS_ERROR = "error";
}
