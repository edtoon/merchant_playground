<?php

namespace BlakePrinting\Shipping;

use BlakePrinting\Fields\AbstractEntityFieldStringVO;

class ShipmentFieldTextVO extends AbstractEntityFieldStringVO {
    /** @var int */
    private $shipmentId = null;

    public function __construct($shipmentId = null, $fieldId = null, $value = null) {
        parent::__construct($fieldId, $value);

        $this->shipmentId = $shipmentId;
    }

    /**
     * @return int
     */
    public function getShipmentId()
    {
        return $this->shipmentId;
    }

    /**
     * @param int $shipmentId
     */
    public function setShipmentId($shipmentId)
    {
        $this->shipmentId = $shipmentId;
    }
}
