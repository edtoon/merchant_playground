<?php

namespace BlakePrinting\Shipping;

use BlakePrinting\Db\PDOQuery;
use PDO;

class ShippingUtil {
    private function __construct() { }

    /**
     * @param int $shipmentId
     * @param PDO $connection
     * @param string|string[] $options
     * @return ShipmentStatusVO
     */
    public static function getShipmentStatusVoByShipmentId($shipmentId, PDO &$connection = null, $options = null) {
        $shipmentStatusId =& PDOQuery::select()
            ->column("shipment_status_id")
            ->from(ShipmentDAO::instance($connection))
            ->where("id", $shipmentId, PDO::PARAM_INT)
            ->options($options)
            ->getSingleValue($connection)
        ;

        if($shipmentStatusId === null) {
            return null;
        }

        return ShipmentStatusDAO::instance($connection)->findById($shipmentStatusId, null, true);
    }
}
