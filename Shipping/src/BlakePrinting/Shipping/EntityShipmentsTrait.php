<?php

namespace BlakePrinting\Shipping;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\AbstractExtensibleEntity;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use Exception;
use PDO;
use Stringy\StaticStringy as S;

trait EntityShipmentsTrait {
    /** @var ShipmentVO[] */
    private $shipments = [];
    /** @var AbstractDataAccessObject */
    private $entityShipmentDao = null;

    public function replaceShipments() {
        $this->updateShipments(true);
    }

    /**
     * @param bool $cancelPrevious
     * @throws Exception
     */
    public function updateShipments($cancelPrevious = true) {
        if(!$this instanceof AbstractExtensibleEntity) {
            throw new Exception("Tried to replace shipments of object that's not an extensible entity: " . get_class($this));
        } else {
            $entityColumnName = $this->getEntityColumnName();
            $entityColumnValue = $this->getEntityColumnValue();
            $shipmentIds = [];

            if($this->entityShipmentDao === null) {
                $this->entityShipmentDao = $this->getExtensionDAO("Shipment");
            }

            if(!empty($this->shipments)) {
                $voReflectionClass = $this->entityShipmentDao->getVoReflectionClass();
                $setEntityMethod = $voReflectionClass->getMethod("set" . S::upperCamelize($entityColumnName));
                $setShipmentIdMethod = $voReflectionClass->getMethod("setShipmentId");

                foreach($this->shipments as &$shipmentVo) {
                    $shipmentId = $shipmentVo->getId();
                    $shipmentIds[] = $shipmentId;
                    $vo = $voReflectionClass->newInstance();

                    if($vo instanceof ValueObject) {
                        $setEntityMethod->invoke($vo, $entityColumnValue);
                        $setShipmentIdMethod->invoke($vo, $shipmentId);

                        $this->entityShipmentDao->insert($vo, null, true);
                    }
                }

                unset($shipmentVo);
            }

            if($cancelPrevious) {
                $connection = $this->entityShipmentDao->getConnection();
                $shipmentStatusDao =& ShipmentStatusDAO::instance($connection);
                $shipmentDao =& ShipmentDAO::instance($connection);
                $pendingStatusId = $shipmentStatusDao->findByColumn("name", ShipmentStatusDAO::SHIPMENT_STATUS_PENDING, PDO::PARAM_STR, null, null, null, true)->getId();
                $cancelledStatusId = $shipmentStatusDao->findByColumn("name", ShipmentStatusDAO::SHIPMENT_STATUS_CANCELLED, PDO::PARAM_STR, null, null, null, true)->getId();
                $update = PDOQuery::update()
                    ->table($this->entityShipmentDao)
                    ->table($shipmentDao)
                    ->set("shipment_status_id", $cancelledStatusId, PDO::PARAM_INT)
                    ->set("updated", "UNIX_TIMESTAMP()")
                    ->where($entityColumnName, $entityColumnValue, $this->getEntityColumnType())
                    ->where("shipment_id", "id")
                    ->where("shipment_status_id", $pendingStatusId, PDO::PARAM_INT)
                    ->whereNull("shipped")
                ;

                if(!empty($shipmentIds)) {
                    $update->where("shipment_id NOT IN (" . implode(",", $shipmentIds) . ")");
                }

                $update->executeGetRowCount($connection);
            }
        }
    }

    /**
     * @param ShipmentVO $shipmentVo
     */
    public function addShipment(ShipmentVO $shipmentVo) {
        $this->shipments[] = $shipmentVo;
    }

    /**
     * @return ShipmentVO[]
     */
    public function getShipments() {
        return $this->shipments;
    }

    /**
     * @return AbstractDataAccessObject
     */
    public function getEntityShipmentDao()
    {
        return $this->entityShipmentDao;
    }

    /**
     * @param AbstractDataAccessObject $entityShipmentDao
     */
    public function setEntityShipmentDao(AbstractDataAccessObject $entityShipmentDao)
    {
        $this->entityShipmentDao = $entityShipmentDao;
    }
}
