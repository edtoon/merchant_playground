<?php

namespace BlakePrinting\Shipping;

use BlakePrinting\Locations\AbstractEntityLocationVO;

class ShipmentLocationVO extends AbstractEntityLocationVO {
    /** @var int */
    private $shipmentId = null;

    /**
     * @param int $locationId
     */
    public function __construct($shipmentId = null, $locationId = null) {
        parent::__construct($locationId);

        $this->shipmentId = $shipmentId;
    }

    /**
     * @return int
     */
    public function getShipmentId()
    {
        return $this->shipmentId;
    }

    /**
     * @param int $shipmentId
     */
    public function setShipmentId($shipmentId)
    {
        $this->shipmentId = $shipmentId;
    }
}
