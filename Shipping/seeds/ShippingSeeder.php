<?php

use BlakePrinting\Db\DbSeed;
use BlakePrinting\Shipping\ShipmentStatusDAO;
use BlakePrinting\Shipping\ShipmentStatusVO;
use BlakePrinting\Shipping\ShippingCarrierDAO;
use BlakePrinting\Shipping\ShippingCarrierVO;
use BlakePrinting\Util\Utils;

class ShippingSeeder extends DbSeed {
    public final function run() {
        $connection =& $this->getConnection();
        $shipmentStatusDao =& ShipmentStatusDAO::instance($connection);
        $shippingCarrierDao =& ShippingCarrierDAO::instance($connection);

        Utils::forEachConstantInObjectClass(
            $shipmentStatusDao,
            function($_, $constantName, $constantValue) use (&$shipmentStatusDao) {
                $shipmentStatusDao->findOrCreate(new ShipmentStatusVO($constantValue));
            },
            "SHIPMENT_STATUS_"
        );

        Utils::forEachConstantInObjectClass(
            $shippingCarrierDao,
            function($_, $constantName, $constantValue) use (&$shippingCarrierDao) {
                $shippingCarrierDao->findOrCreate(new ShippingCarrierVO($constantValue));
            },
            "SHIPPING_CARRIER_"
        );
    }
}
