<?php

use BlakePrinting\Catalog\CatalogProductDAO;
use BlakePrinting\Contacts\ContactAddressDAO;
use BlakePrinting\Contacts\ContactPersonDAO;
use BlakePrinting\Contacts\ContactPhoneDAO;
use BlakePrinting\DataSources\DataFeedDAO;
use BlakePrinting\Db\DbMigration;
use BlakePrinting\Fields\FieldDAO;
use BlakePrinting\Locations\LocationDAO;
use BlakePrinting\Organizations\OrganizationDAO;
use BlakePrinting\Shipping\ShipmentCatalogProductDAO;
use BlakePrinting\Shipping\ShipmentDAO;
use BlakePrinting\Shipping\ShipmentDataFeedDAO;
use BlakePrinting\Shipping\ShipmentFieldSignedDAO;
use BlakePrinting\Shipping\ShipmentFieldStringDAO;
use BlakePrinting\Shipping\ShipmentFieldTextDAO;
use BlakePrinting\Shipping\ShipmentFieldUnsignedDAO;
use BlakePrinting\Shipping\ShipmentLocationDAO;
use BlakePrinting\Shipping\ShipmentStatusDAO;
use BlakePrinting\Shipping\ShippingCarrierDAO;
use BlakePrinting\Users\UserDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class Shipping extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();
        $userDao =& UserDAO::instance($connection);
        $userTbl = $userDao->formatSchemaAndTableName();
        $fieldDao =& FieldDAO::instance($connection);
        $fieldTbl = $fieldDao->formatSchemaAndTableName();

        $shippingCarrierDao =& ShippingCarrierDAO::instance($connection);
        $this->tableDao($shippingCarrierDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("name", "string", ["limit" => 191, "null" => true])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $shipmentStatusDao =& ShipmentStatusDAO::instance($connection);
        $this->tableDao($shipmentStatusDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("name", "string", ["limit" => 191, "null" => true])
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $shipmentDao =& ShipmentDAO::instance($connection);
        $shipmentTbl = $shipmentDao->formatSchemaAndTableName();
        $organizationDao =& OrganizationDAO::instance($connection);
        $organizationTbl = $organizationDao->formatSchemaAndTableName();
        $contactPhoneDao =& ContactPhoneDAO::instance($connection);
        $contactPersonDao =& ContactPersonDAO::instance($connection);
        $contactAddressDao =& ContactAddressDAO::instance($connection);
        $this->tableDao($shipmentDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("parent_shipment_id", "biginteger", ["null" => true, "signed" => false])
            ->addColumn("sender_organization_id", "integer", ["null" => true, "signed" => false])
            ->addColumn("recipient_organization_id", "integer", ["null" => true, "signed" => false])
            ->addColumn("contact_phone_id", "integer", ["null" => true, "signed" => false])
            ->addColumn("contact_person_id", "integer", ["null" => true, "signed" => false])
            ->addColumn("contact_address_id", "integer", ["signed" => false])
            ->addColumn("shipping_carrier_id", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("shipped", "biginteger", ["null" => true, "signed" => false])
            ->addColumn("shipment_status_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("parent_shipment_id", $shipmentTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("sender_organization_id", $organizationTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("recipient_organization_id", $organizationTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("contact_phone_id", $contactPhoneDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("contact_person_id", $contactPersonDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("contact_address_id", $contactAddressDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("shipping_carrier_id", $shippingCarrierDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("shipment_status_id", $shipmentStatusDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("shipped")
            ->save()
        ;

        $shipmentFieldStringDao =& ShipmentFieldStringDAO::instance($connection);
        $this->tableDao($shipmentFieldStringDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("shipment_id", "biginteger", ["signed" => false])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "string", ["null" => true, "limit" => 191])
            ->addForeignKey("shipment_id", $shipmentTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $shipmentFieldTextDao =& ShipmentFieldTextDAO::instance($connection);
        $this->tableDao($shipmentFieldTextDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("shipment_id", "biginteger", ["signed" => false])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "text", ["null" => true])
            ->addForeignKey("shipment_id", $shipmentTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $shipmentFieldUnsignedDao =& ShipmentFieldUnsignedDAO::instance($connection);
        $this->tableDao($shipmentFieldUnsignedDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("shipment_id", "biginteger", ["signed" => false])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "biginteger", ["null" => true, "signed" => false])
            ->addForeignKey("shipment_id", $shipmentTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $shipmentFieldSignedDao =& ShipmentFieldSignedDAO::instance($connection);
        $this->tableDao($shipmentFieldSignedDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("shipment_id", "biginteger", ["signed" => false])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "biginteger", ["null" => true])
            ->addForeignKey("shipment_id", $shipmentTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $shipmentCatalogProductDao =& ShipmentCatalogProductDAO::instance($connection);
        $catalogProductDao =& CatalogProductDAO::instance($connection);
        $this->tableDao($shipmentCatalogProductDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["shipment_id", "catalog_product_id"]])
            ->addColumn("shipment_id", "biginteger", ["signed" => false])
            ->addColumn("catalog_product_id", "biginteger", ["signed" => false])
            ->addColumn("quantity", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addForeignKey("shipment_id", $shipmentTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("catalog_product_id", $catalogProductDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $shipmentLocationDao =& ShipmentLocationDAO::instance($connection);
        $locationDao =& LocationDAO::instance($connection);
        $this->tableDao($shipmentLocationDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["shipment_id"]])
            ->addColumn("shipment_id", "biginteger", ["signed" => false])
            ->addColumn("location_id", "integer", ["signed" => false])
            ->addForeignKey("shipment_id", $shipmentTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("location_id", $locationDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $shipmentDataFeedDao =& ShipmentDataFeedDAO::instance($connection);
        $dataFeedDao =& DataFeedDAO::instance($connection);
        $this->tableDao($shipmentDataFeedDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["shipment_id", "data_feed_id"]])
            ->addColumn("shipment_id", "biginteger", ["signed" => false])
            ->addColumn("data_feed_id", "integer", ["signed" => false])
            ->addForeignKey("shipment_id", $shipmentTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("data_feed_id", $dataFeedDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(ShipmentDataFeedDAO::instance($connection));
        $this->dropDao(ShipmentLocationDAO::instance($connection));
        $this->dropDao(ShipmentCatalogProductDAO::instance($connection));
        $this->dropDao(ShipmentFieldSignedDAO::instance($connection));
        $this->dropDao(ShipmentFieldUnsignedDAO::instance($connection));
        $this->dropDao(ShipmentFieldTextDAO::instance($connection));
        $this->dropDao(ShipmentFieldStringDAO::instance($connection));
        $this->dropDao(ShipmentDAO::instance($connection));
        $this->dropDao(ShipmentStatusDAO::instance($connection));
    }
}
