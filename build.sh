#!/bin/bash
#
# Build the entire project at once
#

set -o errexit

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${BASE_DIR}/lib/lib-blake-environment.sh"
. "${BASE_DIR}/lib/lib-composer.sh"
. "${BASE_DIR}/lib/lib-config.sh"
. "${BASE_DIR}/lib/lib-docker.sh"

export DL_DIR="${DL_DIR:=${BASE_DIR}/downloads}"
export VEND_DIR="${VEND_DIR:=${BASE_DIR}/vendor}"

function build_subprojects {
  local build_file=""

  for build_file in `ls ${BASE_DIR}/*/build.sh`
  do
    "${build_file}"
  done
}

function start_mysql {
  if [ "${OPERATING_SYSTEM_KERNEL}" = "Linux" ] && [ "${BLAKE_ENVIRONMENT}" = "dev" ];
  then
    DB_ID="${DB_ID:=$(config_get database.default)}"
    DB_DOCKER_NAME="${DB_DOCKER_NAME:=$(config_get database.${DB_ID}.docker)}"

    if [ -z "$(docker_exec ps --filter name=${DB_DOCKER_NAME} --filter status=running --format "{{.ID}}")" ];
    then
      "${BASE_DIR}/bin/run_mysql.sh"
    fi
  fi
}

function build_composer_packages {
  mkdir -p "${VEND_DIR}/composer"

  if [ ! -d "${VEND_DIR}/composer/composer" ];
  then
    COMPOSER_VERSION="$(composer_exec --version | awk '{print $3}')"
    TMPDIR="$(mktemp -d)"
    curl -L -o "${TMPDIR}/composer-${COMPOSER_VERSION}.zip" "https://github.com/composer/composer/archive/${COMPOSER_VERSION}.zip"
    unzip "${TMPDIR}/composer-${COMPOSER_VERSION}.zip" -d "${TMPDIR}"
    rm "${TMPDIR}/composer-${COMPOSER_VERSION}.zip"
    mv -f "${TMPDIR}/composer-${COMPOSER_VERSION}" "${VEND_DIR}/composer/composer"
    rmdir "${TMPDIR}"
  fi

  if [ ! -f "${VEND_DIR}/autoload.php" ];
  then
    composer_exec install --no-interaction --prefer-dist
  elif [ "${HAS_DOCKER}" != "true" ];
  then
    composer_exec update --no-interaction --prefer-stable
  fi

  if [ "${HAS_DOCKER}" = "true" ];
  then
    "${BASE_DIR}/bin/run_php.sh" "${BLAKE_APP_DIR}/bin/composer.php"
    composer_chown
  fi
}

function download_external {
  local download_file=""

  for download_file in `ls ${BASE_DIR}/*/download.sh`
  do
    VEND_DIR="${VEND_DIR}" DL_DIR="${DL_DIR}" "${download_file}"
  done
}

function migrate_schemas {
  if [ "${OPERATING_SYSTEM_KERNEL}" = "Linux" ];
  then
    "${BASE_DIR}/bin/run_php.sh" "${BLAKE_APP_DIR}/bin/phinx.php" migrate
  fi
}

function seed_schemas {
  if [ "${OPERATING_SYSTEM_KERNEL}" = "Linux" ];
  then
    "${BASE_DIR}/bin/run_php.sh" "${BLAKE_APP_DIR}/bin/phinx.php" seed
  fi
}

function sync_resources {
  mkdir -p "${BASE_DIR}/Admin/web/cldr/supplemental"
  rsync -av ${BASE_DIR}/vendor/unicode_cldr_core/*/supplemental/ "${BASE_DIR}/Admin/web/cldr/supplemental/"
  mkdir -p "${BASE_DIR}/Admin/web/cldr/main"
  rsync -av ${BASE_DIR}/vendor/unicode_cldr_numbers/*/main/ "${BASE_DIR}/Admin/web/cldr/main/"
}

build_subprojects
start_mysql
build_composer_packages
download_external
migrate_schemas
seed_schemas
sync_resources
