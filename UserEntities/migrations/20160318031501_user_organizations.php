<?php

use BlakePrinting\Organizations\OrganizationDAO;
use BlakePrinting\Users\Entities\UserOrganizationDAO;
use BlakePrinting\Users\UserDAO;
use BlakePrinting\Db\DbMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class UserOrganizations extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $userDao = UserDAO::instance($connection);
        $organizationDao = OrganizationDAO::instance($connection);
        $userOrganizationDao = UserOrganizationDAO::instance($connection);
        $userOrganizationTbl = $this->tableDao($userOrganizationDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["user_id", "organization_id"]]);

        if(!$userOrganizationTbl->exists()) {
            $userOrganizationTbl
                ->addColumn("user_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
                ->addColumn("organization_id", "integer", ["signed" => false])
                ->addForeignKey("user_id", $userDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
                ->addForeignKey("organization_id", $organizationDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
                ->save()
            ;
        }
    }

    public function down() {
        $connection = $this->getConnection();

        $this->dropDao(UserOrganizationDAO::instance($connection));
    }
}
