<?php

use BlakePrinting\Facilities\FacilityDAO;
use BlakePrinting\Users\Entities\UserFacilityDAO;
use BlakePrinting\Users\UserDAO;
use BlakePrinting\Db\DbMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class UserFacilities extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $userDao = UserDAO::instance($connection);
        $facilityDao = FacilityDAO::instance($connection);
        $userFacilityDao = UserFacilityDAO::instance($connection);
        $userFacilityTbl = $this->tableDao($userFacilityDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["user_id", "facility_id"]]);

        if(!$userFacilityTbl->exists()) {
            $userFacilityTbl
                ->addColumn("user_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
                ->addColumn("facility_id", "integer", ["signed" => false])
                ->addForeignKey("user_id", $userDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
                ->addForeignKey("facility_id", $facilityDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
                ->save()
            ;
        }
    }

    public function down() {
        $connection = $this->getConnection();

        $this->dropDao(UserFacilityDAO::instance($connection));
    }
}
