<?php

namespace BlakePrinting\Users\Entities;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static UserOrganizationDAO instance(PDO $connection = null, $schemaName = null)
 * @method UserOrganizationVO findById($id, $options = null, $useCache = false)
 * @method UserOrganizationVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method UserOrganizationVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method UserOrganizationVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method UserOrganizationVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method UserOrganizationVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method UserOrganizationVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class UserOrganizationDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $userId
     * @param int $organizationId
     * @return int
     */
    public function addOrganization($userId, $organizationId) {
        return PDOQuery::insert()->ignore()->into($this)
            ->value("user_id", $userId, PDO::PARAM_INT)
            ->value("organization_id", $organizationId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }

    /**
     * @param int $userId
     * @param int $organizationId
     * @return int
     */
    public function removeOrganization($userId, $organizationId) {
        return PDOQuery::delete()->from($this)
            ->where("user_id", $userId, PDO::PARAM_INT)
            ->where("organization_id", $organizationId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
