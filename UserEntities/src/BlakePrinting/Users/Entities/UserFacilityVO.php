<?php

namespace BlakePrinting\Users\Entities;

use BlakePrinting\Facilities\AbstractEntityFacilityVO;

class UserFacilityVO extends AbstractEntityFacilityVO {
    /** @var int */
    private $userId = null;

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
}
