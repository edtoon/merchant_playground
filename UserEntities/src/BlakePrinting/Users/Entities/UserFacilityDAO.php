<?php

namespace BlakePrinting\Users\Entities;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static UserFacilityDAO instance(PDO $connection = null, $schemaName = null)
 * @method UserFacilityVO findById($id, $options = null, $useCache = false)
 * @method UserFacilityVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method UserFacilityVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method UserFacilityVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method UserFacilityVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method UserFacilityVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method UserFacilityVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class UserFacilityDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $userId
     * @param int $facilityId
     * @return int
     */
    public function &addFacility($userId, $facilityId) {
        return PDOQuery::insert()->ignore()->into($this)
            ->value("user_id", $userId, PDO::PARAM_INT)
            ->value("facility_id", $facilityId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }

    /**
     * @param int $userId
     * @param int $facilityId
     * @return int
     */
    public function &removeFacility($userId, $facilityId) {
        return PDOQuery::delete()->from($this)
            ->where("user_id", $userId, PDO::PARAM_INT)
            ->where("facility_id", $facilityId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
