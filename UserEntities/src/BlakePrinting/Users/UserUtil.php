<?php

namespace BlakePrinting\Users;

use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Facilities\FacilityDAO;
use BlakePrinting\Facilities\FacilityLocationDAO;
use BlakePrinting\Facilities\FacilityVO;
use BlakePrinting\Locations\LocationDAO;
use BlakePrinting\Organizations\OrganizationDAO;
use BlakePrinting\Organizations\OrganizationOrganizationTypeDAO;
use BlakePrinting\Organizations\OrganizationTypeDAO;
use BlakePrinting\Organizations\OrganizationVO;
use BlakePrinting\Users\Entities\UserFacilityDAO;
use BlakePrinting\Users\Entities\UserOrganizationDAO;
use PDO;
use ReflectionClass;

class UserUtil {
    private function __construct() { }

    /**
     * @param int $userId
     * @param int $locationId
     * @param PDO $connection
     * @return bool
     */
    public static function &checkUserAuthorizedForLocation($userId, $locationId, PDO $connection) {
        $userFacilityLocationIds =& self::getUserFacilityLocations($userId, $connection);
        $locationIsAuthorized = false;

        if(!empty($userFacilityLocationIds)) {
            $locationDao =& LocationDAO::instance($connection);

            foreach($userFacilityLocationIds as &$userFacilityLocationId) {
                if($locationId == $userFacilityLocationId || $locationDao->checkLocationDescendsFrom($locationId, $userFacilityLocationId)) {
                    $locationIsAuthorized = true;

                    break;
                }
            }

            unset($userFacilityLocationId);
        }

        return $locationIsAuthorized;
    }

    /**
     * @param int $userId
     * @param PDO $connection
     * @param string|string[] $orderBy
     * @param string|string[]|int|int[] $limit
     * @param string|string[] $options
     * @return FacilityVO[]
     */
    public static function &getUserFacilities($userId, PDO $connection = null, $orderBy = null, $limit = null, $options = null) {
        $facilityDao =& FacilityDAO::instance($connection);

        return $facilityDao->findAll(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$userId, &$connection, &$orderBy, &$limit, &$options) {
                $userFacilityDao =& UserFacilityDAO::instance($connection);

                $pdoQuery
                    ->join($userFacilityDao, ["facility.id = user_facility.facility_id"])
                    ->where("user_id", $userId, PDO::PARAM_INT)
                ;
            },
            $orderBy, $limit, $options
        );
    }

    /**
     * @param int $userId
     * @param PDO $connection
     * @param string|string[] $orderBy
     * @param string|string[]|int|int[] $limit
     * @param string|string[] $options
     * @return int[]
     */
    public static function &getUserFacilityLocations($userId, PDO $connection = null, $orderBy = null, $limit = null, $options = null) {
        $facilityLocationDao =& FacilityLocationDAO::instance($connection);
        $userFacilityDao =& UserFacilityDAO::instance($connection);

        return PDOQuery::select()
            ->column("location_id")
            ->from($facilityLocationDao)
            ->join($userFacilityDao, ["facility_location.facility_id = user_facility.facility_id"])
            ->where("user_id", $userId, PDO::PARAM_INT)
            ->addOrderByLimitAndOptions($orderBy, $limit, $options)
            ->getColumnValues($connection)
        ;
    }

    /**
     * @param int $userId
     * @param PDO $connection
     * @param string|string[] $orderBy
     * @param string|string[]|int|int[] $limit
     * @param string|string[] $options
     * @return OrganizationVO[]
     */
    public static function &getUserOrganizations($userId, PDO $connection = null, $orderBy = null, $limit = null, $options = null) {
        $internalOrganizationTypeVo =& OrganizationTypeDAO::instance($connection)->findByColumn(
            "name", OrganizationTypeDAO::ORGANIZATION_TYPE_INTERNAL, PDO::PARAM_STR, null, null, null, true
        );
        $organizationDao =& OrganizationDAO::instance($connection);

        return $organizationDao->findAll(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$userId, &$internalOrganizationTypeVo, &$organizationDao, &$connection) {
                $organizationOrganizationTypeTbl = OrganizationOrganizationTypeDAO::instance($connection)->formatSchemaAndTableName();
                $userOrganizationTbl = UserOrganizationDAO::instance($connection)->formatSchemaAndTableName();
                $organizationTbl = $organizationDao->formatSchemaAndTableName();

                $pdoQuery
                    ->distinct()
                    ->columns($organizationTbl . ".*")
                    ->join($userOrganizationTbl, [$organizationTbl . ".id = " . $userOrganizationTbl . ".organization_id"])
                    ->join($organizationOrganizationTypeTbl, [$organizationTbl . ".id = " . $organizationOrganizationTypeTbl . ".organization_id"])
                    ->where("user_id", $userId, PDO::PARAM_INT)
                    ->where("organization_type_id", $internalOrganizationTypeVo->getId(), PDO::PARAM_INT)
                ;
            }, $orderBy, $limit, $options
        );
    }
}
