<?php

namespace BlakePrinting\TransformAmazonReports;

use BlakePrinting\AmazonMws\Reports\ReportTypes;
use BlakePrinting\Db\DbUtil;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use Stringy\StaticStringy as S;
use Exception;
use PDO;

class AmazonReportTransformer {
    const VALID_REPORT_TYPES = [
        ReportTypes::REPORT_TYPE_FBA_INV_HIST_DAILY,
        ReportTypes::REPORT_TYPE_FBA_INV_HIST_MONTHLY,
        ReportTypes::REPORT_TYPE_FBA_INV_RECEIVED,
        ReportTypes::REPORT_TYPE_FBA_SHIPMENT_SALES,
        ReportTypes::REPORT_TYPE_FULFILLED_SHIPMENTS
    ];

    private function __construct() { }

    public static function transformReport($marketplaceInstanceId, $reportType, $reportId, PDO $connection) {
        if(!in_array($reportType, self::VALID_REPORT_TYPES, true)) {
            throw new Exception("Don't know how to transform report type: " . $reportType);
        }

        $logger =& LogUtil::getLogger();
        $reportFilePath = "/data/marketplace/" . $marketplaceInstanceId . "/" . $reportType . "/" . $reportId;
        $reportFile = fopen($reportFilePath, "rb");

        try {
            if($reportFile === null) {
                throw new Exception("Failed to open report file: " . $reportFilePath);
            }

            $transformBatchId =& PDOQuery::insert()
                ->into("amazon_mws.report_transform_batch")
                ->value("created", "UNIX_TIMESTAMP()")
                ->value("marketplace_instance_id", $marketplaceInstanceId, PDO::PARAM_INT)
                ->value("report_id", $reportId, PDO::PARAM_STR)
                ->executeEnsuringLastInsertId($connection)
            ;
            $reportColumns = [];
            $reportColumnCount = 0;

            while(($reportLine = fgets($reportFile)) !== false) {
                if(!mb_check_encoding($reportLine, "UTF-8")) {
                    $reportLine = mb_convert_encoding($reportLine, "UTF-8",
                        "Shift-JIS, EUC-JP, JIS, SJIS, JIS-ms, eucJP-win, SJIS-win, " .
                        "ISO-2022-JP, ISO-2022-JP-MS, SJIS-mac, SJIS-Mobile#DOCOMO, SJIS-Mobile#KDDI, " .
                        "SJIS-Mobile#SOFTBANK, UTF-8-Mobile#DOCOMO, UTF-8-Mobile#KDDI-A, " .
                        "UTF-8-Mobile#KDDI-B, UTF-8-Mobile#SOFTBANK, ISO-2022-JP-MOBILE#KDDI"
                    );
                }

                $columns = mb_split("\t", (string)S::trim($reportLine));
                $columnCount = count($columns);

                if(empty($reportColumns)) {
                    $reportColumnCount = $columnCount;
                    $reportColumns = array_map(function($columnName) {
                        return str_replace("-", "_", $columnName);
                    }, $columns);

                    continue;
                }

                $row = [];

                for($i = 0; $i < min($reportColumnCount, $columnCount); $i++) {
                    $columnName =& $reportColumns[$i];
                    $columnValue =& $columns[$i];

                    if($columnName !== null && $columnValue !== null) {
                        $columnName = (string)S::trim($columnName);
                        $columnValue = (string)S::trim($columnValue);

                        if(!S::isBlank($columnName) && !S::isBlank($columnValue)) {
                            $row[$columnName] =& $columnValue;
                        }
                    }
                }

                if(empty($row)) {
                    continue;
                }

                switch($reportType) {
                    case ReportTypes::REPORT_TYPE_FBA_INV_HIST_DAILY:
                        $insertQuery = PDOQuery::insert()
                            ->into("amazon_mws.report_fba_inv_hist_daily")
                            ->value("report_transform_batch_id", $transformBatchId, PDO::PARAM_INT)
                        ;

                        foreach($row as $columnName => &$columnValue) {
                            if($columnName == "snapshot_date") {
                                $insertQuery->value($columnName, $columnValue, PDO::PARAM_STR, DbUtil::formatFunction(DbUtil::FUNC_FROM_ISO8601_SUBSET));
                            } else if($columnName == "quantity") {
                                $insertQuery->value($columnName, (0 + $columnValue), PDO::PARAM_INT);
                            } else {
                                $insertQuery->value($columnName, $columnValue, PDO::PARAM_STR);
                            }
                        }

                        unset($columnValue);

                        $insertQuery->execute($connection);
                    break;
                    case ReportTypes::REPORT_TYPE_FBA_INV_HIST_MONTHLY:
                        $insertQuery = PDOQuery::insert()
                            ->into("amazon_mws.report_fba_inv_hist_monthly")
                            ->value("report_transform_batch_id", $transformBatchId, PDO::PARAM_INT)
                        ;

                        foreach($row as $columnName => &$columnValue) {
                            if($columnName == "month") {
                                $parts = explode("/", $columnValue);

                                $insertQuery->value("year", (0 + $parts[1]), PDO::PARAM_INT);
                                $insertQuery->value("month", (0 + $parts[0]), PDO::PARAM_INT);
                            } else if($columnName == "end_quantity") {
                                $insertQuery->value($columnName, (0 + $columnValue), PDO::PARAM_INT);
                            } else {
                                $insertQuery->value($columnName, $columnValue, PDO::PARAM_STR);
                            }
                        }

                        unset($columnValue);

                        $insertQuery->execute($connection);
                    break;
                    case ReportTypes::REPORT_TYPE_FBA_INV_RECEIVED:
                        $insertQuery = PDOQuery::insert()
                            ->into("amazon_mws.report_fba_inv_received")
                            ->value("report_transform_batch_id", $transformBatchId, PDO::PARAM_INT)
                        ;

                        foreach($row as $columnName => &$columnValue) {
                            if($columnName == "received_date") {
                                $insertQuery->value($columnName, $columnValue, PDO::PARAM_STR, DbUtil::formatFunction(DbUtil::FUNC_FROM_ISO8601_SUBSET));
                            } else if($columnName == "quantity") {
                                $quantity = 0;

                                if($columnValue !== null && S::length($columnValue) > 0) {
                                    $quantity = (0 + $columnValue);

                                    if($quantity < 0) {
                                        $logger->warn("Found negative quantity: " . $quantity . ", changing to zero but that's probably not good - columns: " . Utils::getVarDump($columns));

                                        $quantity = 0;
                                    }
                                }

                                $insertQuery->value($columnName, $quantity, PDO::PARAM_INT);
                            } else {
                                $insertQuery->value($columnName, $columnValue, PDO::PARAM_STR);
                            }
                        }

                        unset($columnValue);

                        $insertQuery->execute($connection);
                    break;
                    case ReportTypes::REPORT_TYPE_FBA_SHIPMENT_SALES:
                        $insertQuery = PDOQuery::insert()
                            ->into("amazon_mws.report_fba_shipment_sales")
                            ->value("report_transform_batch_id", $transformBatchId, PDO::PARAM_INT)
                        ;
                        $seenPointsGranted = false;

                        foreach($row as $columnName => &$columnValue) {
                            if($columnName == "shipment_date") {
                                $insertQuery->value($columnName, $columnValue, PDO::PARAM_STR, DbUtil::formatFunction(DbUtil::FUNC_FROM_ISO8601_SUBSET));
                            } else if($columnName == "quantity" || $columnName == "points_granted") {
                                if($columnName == "points_granted") {
                                    $seenPointsGranted = true;
                                }

                                $insertQuery->value($columnName, (0 + $columnValue), PDO::PARAM_INT);
                            } else {
                                $insertQuery->value($columnName, $columnValue, PDO::PARAM_STR);
                            }
                        }

                        unset($columnValue);

                        if(!$seenPointsGranted) {
                            $insertQuery->value("points_granted", 0, PDO::PARAM_INT);
                        }

                        $insertQuery->execute($connection);
                    break;
                    case ReportTypes::REPORT_TYPE_FULFILLED_SHIPMENTS:
                        $insertQuery = PDOQuery::insert()
                            ->into("amazon_mws.report_fulfilled_shipments")
                            ->value("report_transform_batch_id", $transformBatchId, PDO::PARAM_INT)
                        ;
                        $seenPointsGranted = false;

                        foreach($row as $columnName => &$columnValue) {
                            if(in_array($columnName, ["purchase_date", "payments_date", "shipment_date", "reporting_date", "estimated_arrival_date"])) {
                                $insertQuery->value($columnName, $columnValue, PDO::PARAM_STR, DbUtil::formatFunction(DbUtil::FUNC_FROM_ISO8601_SUBSET));
                            } else if($columnName == "quantity_shipped" || $columnName == "points_granted") {
                                if($columnName == "points_granted") {
                                    $seenPointsGranted = true;
                                }

                                $insertQuery->value($columnName, (0 + $columnValue), PDO::PARAM_INT);
                            } else {
                                $insertQuery->value($columnName, $columnValue, PDO::PARAM_STR);
                            }
                        }

                        unset($columnValue);

                        if(!$seenPointsGranted) {
                            $insertQuery->value("points_granted", 0, PDO::PARAM_INT);
                        }

                        $insertQuery->execute($connection);
                    break;
                }
            }

            if(!feof($reportFile)) {
                $logger->error("Finished reading report before the end of file: " . $reportFilePath);
            }
        } finally {
            if($reportFile !== null) {
                fclose($reportFile);
            }
        }
    }
}
