<?php

namespace BlakePrinting\TransformAmazonReports;

use BlakePrinting\AmazonMws\Sellers\AmazonMwsUtil;
use BlakePrinting\AmazonMws\Sellers\CredentialVO;
use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceVO;
use BlakePrinting\CLI\CLICommand;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Util\LogUtil;
use Monolog\Logger;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Exception;
use PDO;

class TransformReportCommand extends CLICommand {
    protected function configure() {
        $this
            ->setName("transform:amazon:report")
            ->setDescription("Transform a retrieved report")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        try {
            AmazonMwsUtil::forEachActiveMarketplace(function(MarketplaceInstanceVO &$marketplaceInstanceVo, CredentialVO &$credentialVo, PDO $connection) {
                $this->transformReportForInstance($marketplaceInstanceVo, $connection);

                PDOUtil::commitConnection($connection);
            }, function(PDO $connection) {
                PDOUtil::rollbackConnection($connection);
            });
        } catch(Exception $e) {
            LogUtil::logException(Logger::ERROR, $e, "Error transforming reports");
        }
    }

    private function transformReportForInstance(MarketplaceInstanceVO &$marketplaceInstanceVo, PDO $connection) {
        $logger =& LogUtil::getLogger();
        $reportId = null;
        $mwsMarketplaceId = $marketplaceInstanceVo->getMwsMarketplaceId();
        $reportRequest =& self::getRequestForTransformation($marketplaceInstanceVo, $connection);

        try {
            if($reportRequest !== null) {
                $reportRequestId = $reportRequest["report_request_id"];
                $reportId = $reportRequest["report_id"];
                $reportType = $reportRequest["report_type"];

                $logger->debug(
                    "Transforming request #" . $reportRequestId . ", report id: " . $reportId . ", report type: " . $reportType
                );

                PDOUtil::pauseSqlLogging();

                try {
                    AmazonReportTransformer::transformReport($marketplaceInstanceVo->getId(), $reportType, $reportId, $connection);
                } finally {
                    PDOUtil::resumeSqlLogging();
                }

                PDOQuery::update()
                    ->table("amazon_mws.report_request_transform")
                    ->set("updated", "UNIX_TIMESTAMP()")
                    ->where("report_request_id", $reportRequestId, PDO::PARAM_INT)
                    ->executeEnsuringRowCount($connection, 1)
                ;
                PDOQuery::insert()
                    ->ignore()
                    ->into("amazon_mws.report_request_acknowledge")
                    ->value("report_request_id", $reportRequestId, PDO::PARAM_INT)
                    ->value("created", "UNIX_TIMESTAMP()")
                    ->executeGetRowCount($connection)
                ;

                PDOUtil::commitConnection($connection);

                $logger->info("Transformed Amazon report id: " . $reportId . ", type: " . $reportType);
            }
        } catch(Exception $e) {
            LogUtil::logException(Logger::EMERGENCY, $e, "Failed to transform Amazon report id: " . $reportId . ", MWS marketplace id: " . $mwsMarketplaceId);
        } finally {
            PDOUtil::rollbackConnection($connection);
        }
    }

    private static function &getRequestForTransformation(MarketplaceInstanceVO &$marketplaceInstanceVo, PDO $connection) {
        return PDOQuery::select()
            ->columns(
                "request_transform.report_request_id",
                "request.report_type",
                "COALESCE(request.generated_report_id, report.report_id) as report_id"
            )
            ->from([
                "amazon_mws.report_request_transform" => "request_transform",
                "amazon_mws.report_request" => "request",
                "amazon_mws.report_request_marketplace_instance" => "request_mktpl"
            ])
            ->leftJoin("amazon_mws.report report", [
                "request.mws_report_request_id = report.mws_report_request_id",
                "EXISTS (" .
                    "SELECT 1 FROM amazon_mws.report_marketplace_instance report_mktpl " .
                    "WHERE report_mktpl.report_id = report.id " .
                    "AND report_mktpl.marketplace_instance_id = request_mktpl.marketplace_instance_id" .
                ")"
            ])
            ->leftJoin("amazon_mws.report_marketplace_instance report_mktpl", [
                "report.id = report_mktpl.report_id",
                "report_mktpl.marketplace_instance_id = request_mktpl.marketplace_instance_id"
            ])
            ->where("request_transform.report_request_id", "request.id")
            ->whereNotNull("request_transform.created")
            ->where("(" .
                "request_transform.updated IS NULL" .
                " OR " .
                "request_transform.updated <= request.updated" .
            ")")
            ->where("request.id", "request_mktpl.report_request_id")
            ->where(
                "request.report_type",
                "(" . implode(",", array_map(
                    function($reportType) {
                        return "'" . $reportType . "'";
                    },
                    AmazonReportTransformer::VALID_REPORT_TYPES
                )) . ")",
                null, "IN"
            )
            ->where("request_mktpl.marketplace_instance_id", $marketplaceInstanceVo->getId(), PDO::PARAM_INT)
            ->where("(" .
                "request.generated_report_id IS NOT NULL OR (report.report_id IS NOT NULL AND report_mktpl.report_id IS NOT NULL)" .
            ")")
            ->orderBy("request.completed_date asc")
            ->limit(1)
            ->options("FOR UPDATE")
            ->getSingleRow($connection)
        ;
    }
}
