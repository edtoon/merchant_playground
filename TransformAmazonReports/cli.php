<?php

require_once(__DIR__ . "/../vendor/autoload.php");

$app = new BlakePrinting\TransformAmazonReports\Main;
$app->run();
