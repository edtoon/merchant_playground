<?php

namespace BlakePrinting\WalmartMarketplace\Orders;

class WalmartOrderLineRefundChargeVO extends AbstractWalmartOrderChargeVO {
    /** @var int */
    private $walmartOrderLineRefundId = null;

    /**
     * @return int
     */
    public function getWalmartOrderLineRefundId()
    {
        return $this->walmartOrderLineRefundId;
    }

    /**
     * @param int $walmartOrderLineRefundId
     */
    public function setWalmartOrderLineRefundId($walmartOrderLineRefundId)
    {
        $this->walmartOrderLineRefundId = $walmartOrderLineRefundId;
    }
}
