<?php

namespace BlakePrinting\WalmartMarketplace\Orders;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static WalmartOrderLineStatusTrackingDAO instance(PDO $connection = null, $schemaName = null)
 * @method WalmartOrderLineStatusTrackingVO findById($id, $options = null, $useCache = false)
 * @method WalmartOrderLineStatusTrackingVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartOrderLineStatusTrackingVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartOrderLineStatusTrackingVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartOrderLineStatusTrackingVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartOrderLineStatusTrackingVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartOrderLineStatusTrackingVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartOrderLineStatusTrackingVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class WalmartOrderLineStatusTrackingDAO extends AbstractDataAccessObject {
}
