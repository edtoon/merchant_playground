<?php

namespace BlakePrinting\WalmartMarketplace\Orders;

class WalmartOrderLineChargeVO extends AbstractWalmartOrderChargeVO {
    /** @var int */
    private $walmartOrderLineId = null;

    /**
     * @return int
     */
    public function getWalmartOrderLineId()
    {
        return $this->walmartOrderLineId;
    }

    /**
     * @param int $walmartOrderLineId
     */
    public function setWalmartOrderLineId($walmartOrderLineId)
    {
        $this->walmartOrderLineId = $walmartOrderLineId;
    }
}
