<?php

namespace BlakePrinting\WalmartMarketplace\Orders;

use BlakePrinting\Db\ValueObject;

class WalmartOrderListRequestVO implements ValueObject {
    /** @var int */
    private $walmartOrderListId = null;
    /** @var int */
    private $created = null;
    /** @var string */
    private $uuid = null;

    /**
     * @return int
     */
    public function getWalmartOrderListId()
    {
        return $this->walmartOrderListId;
    }

    /**
     * @param int $walmartOrderListId
     */
    public function setWalmartOrderListId($walmartOrderListId)
    {
        $this->walmartOrderListId = $walmartOrderListId;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }
}
