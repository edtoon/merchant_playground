<?php

namespace BlakePrinting\WalmartMarketplace\Orders;

use BlakePrinting\Db\ValueObject;

abstract class AbstractWalmartOrderChargeVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $walmartChargeTypeId = null;
    /** @var int */
    private $walmartChargeNameId = null;
    /** @var string */
    private $currency = null;
    /** @var string */
    private $amount = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getWalmartChargeTypeId()
    {
        return $this->walmartChargeTypeId;
    }

    /**
     * @param int $walmartChargeTypeId
     */
    public function setWalmartChargeTypeId($walmartChargeTypeId)
    {
        $this->walmartChargeTypeId = $walmartChargeTypeId;
    }

    /**
     * @return int
     */
    public function getWalmartChargeNameId()
    {
        return $this->walmartChargeNameId;
    }

    /**
     * @param int $walmartChargeNameId
     */
    public function setWalmartChargeNameId($walmartChargeNameId)
    {
        $this->walmartChargeNameId = $walmartChargeNameId;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }
}
