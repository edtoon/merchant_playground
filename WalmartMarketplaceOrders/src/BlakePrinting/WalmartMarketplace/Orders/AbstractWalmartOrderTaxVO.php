<?php

namespace BlakePrinting\WalmartMarketplace\Orders;

use BlakePrinting\Db\ValueObject;

class AbstractWalmartOrderTaxVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $walmartTaxId = null;
    /** @var string */
    private $currency = null;
    /** @var string */
    private $amount = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getWalmartTaxId()
    {
        return $this->walmartTaxId;
    }

    /**
     * @param int $walmartTaxId
     */
    public function setWalmartTaxId($walmartTaxId)
    {
        $this->walmartTaxId = $walmartTaxId;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }
}
