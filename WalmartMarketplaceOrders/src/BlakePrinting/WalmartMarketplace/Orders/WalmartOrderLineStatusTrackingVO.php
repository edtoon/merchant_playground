<?php

namespace BlakePrinting\WalmartMarketplace\Orders;

use BlakePrinting\Db\ValueObject;

class WalmartOrderLineStatusTrackingVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $walmartOrderLineStatusId = null;
    /** @var int */
    private $walmartShippingCarrierId = null;
    /** @var int */
    private $walmartShippingMethodId = null;
    /** @var int */
    private $shipped = null;
    /** @var string */
    private $trackingNumber = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getWalmartOrderLineStatusId()
    {
        return $this->walmartOrderLineStatusId;
    }

    /**
     * @param int $walmartOrderLineStatusId
     */
    public function setWalmartOrderLineStatusId($walmartOrderLineStatusId)
    {
        $this->walmartOrderLineStatusId = $walmartOrderLineStatusId;
    }

    /**
     * @return int
     */
    public function getWalmartShippingCarrierId()
    {
        return $this->walmartShippingCarrierId;
    }

    /**
     * @param int $walmartShippingCarrierId
     */
    public function setWalmartShippingCarrierId($walmartShippingCarrierId)
    {
        $this->walmartShippingCarrierId = $walmartShippingCarrierId;
    }

    /**
     * @return int
     */
    public function getWalmartShippingMethodId()
    {
        return $this->walmartShippingMethodId;
    }

    /**
     * @param int $walmartShippingMethodId
     */
    public function setWalmartShippingMethodId($walmartShippingMethodId)
    {
        $this->walmartShippingMethodId = $walmartShippingMethodId;
    }

    /**
     * @return int
     */
    public function getShipped()
    {
        return $this->shipped;
    }

    /**
     * @param int $shipped
     */
    public function setShipped($shipped)
    {
        $this->shipped = $shipped;
    }

    /**
     * @return string
     */
    public function getTrackingNumber()
    {
        return $this->trackingNumber;
    }

    /**
     * @param string $trackingNumber
     */
    public function setTrackingNumber($trackingNumber)
    {
        $this->trackingNumber = $trackingNumber;
    }
}
