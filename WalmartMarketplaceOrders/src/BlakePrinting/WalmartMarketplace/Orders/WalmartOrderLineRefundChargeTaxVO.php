<?php

namespace BlakePrinting\WalmartMarketplace\Orders;

class WalmartOrderLineRefundChargeTaxVO extends AbstractWalmartOrderTaxVO {
    /** @var int */
    private $walmartOrderLineRefundChargeId = null;

    /**
     * @return int
     */
    public function getWalmartOrderLineRefundChargeId()
    {
        return $this->walmartOrderLineRefundChargeId;
    }

    /**
     * @param int $walmartOrderLineRefundChargeId
     */
    public function setWalmartOrderLineRefundChargeId($walmartOrderLineRefundChargeId)
    {
        $this->walmartOrderLineRefundChargeId = $walmartOrderLineRefundChargeId;
    }
}
