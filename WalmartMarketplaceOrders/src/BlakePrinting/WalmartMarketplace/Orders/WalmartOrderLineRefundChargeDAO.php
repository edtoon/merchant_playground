<?php

namespace BlakePrinting\WalmartMarketplace\Orders;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static WalmartOrderLineRefundChargeDAO instance(PDO $connection = null, $schemaName = null)
 * @method WalmartOrderLineRefundChargeVO findById($id, $options = null, $useCache = false)
 * @method WalmartOrderLineRefundChargeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartOrderLineRefundChargeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartOrderLineRefundChargeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartOrderLineRefundChargeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartOrderLineRefundChargeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartOrderLineRefundChargeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartOrderLineRefundChargeVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class WalmartOrderLineRefundChargeDAO extends AbstractDataAccessObject {
}
