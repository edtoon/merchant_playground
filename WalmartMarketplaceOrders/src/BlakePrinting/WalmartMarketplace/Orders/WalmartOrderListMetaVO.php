<?php

namespace BlakePrinting\WalmartMarketplace\Orders;

use BlakePrinting\Db\ValueObject;

class WalmartOrderListMetaVO implements ValueObject {
    /** @var int */
    private $walmartOrderListId = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $totalCount = null;
    /** @var int */
    private $itemLimit = null;
    /** @var string */
    private $nextCursor = null;

    /**
     * @return int
     */
    public function getWalmartOrderListId()
    {
        return $this->walmartOrderListId;
    }

    /**
     * @param int $walmartOrderListId
     */
    public function setWalmartOrderListId($walmartOrderListId)
    {
        $this->walmartOrderListId = $walmartOrderListId;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getTotalCount()
    {
        return $this->totalCount;
    }

    /**
     * @param int $totalCount
     */
    public function setTotalCount($totalCount)
    {
        $this->totalCount = $totalCount;
    }

    /**
     * @return int
     */
    public function getItemLimit()
    {
        return $this->itemLimit;
    }

    /**
     * @param int $itemLimit
     */
    public function setItemLimit($itemLimit)
    {
        $this->itemLimit = $itemLimit;
    }

    /**
     * @return string
     */
    public function getNextCursor()
    {
        return $this->nextCursor;
    }

    /**
     * @param string $nextCursor
     */
    public function setNextCursor($nextCursor)
    {
        $this->nextCursor = $nextCursor;
    }
}
