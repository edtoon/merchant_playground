<?php

namespace BlakePrinting\WalmartMarketplace\Orders\Tasks;

use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Tasks\AbstractTask;
use BlakePrinting\Util\ConfigUtil;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use BlakePrinting\WalmartMarketplace\Orders\Extract\ExtractOrderListUtil;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderListDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderListMetaDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderListRequestDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderListRequestVO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderListVO;
use BlakePrinting\WalmartMarketplace\WalmartMarketplaceCredentialDAO;
use BlakePrinting\WalmartMarketplace\WalmartUtil;
use PDO;
use Ramsey\Uuid\Uuid;
use ReflectionClass;
use Symfony\Component\HttpFoundation\Response;
use XMLReader;

class ImportWalmartOrdersTask extends AbstractTask {
    const FIRST_EXTRACT_MONTH = 8;
    const FIRST_EXTRACT_DAY = 1;
    const FIRST_EXTRACT_YEAR = 2016;

    /** @var int */
    private $walmartOrderListId = null;
    /** @var ExtractOrderListUtil */
    private $extractOrderListUtil = null;

    public function execute() {
        $this->extractOrderListUtil = new ExtractOrderListUtil();

        $this->ensureCredential();
        $this->ensureContent();

        if($this->extractOrderListUtil->getXmlReader() === null) {
            $logger =& LogUtil::getLogger();

            $logger->error("No XML content");

            return 1;
        }

        PDOUtil::pauseSqlLogging();

        try {
            $this->extractOrderListUtil->extractOrderList();
        } finally {
            PDOUtil::resumeSqlLogging();
        }

        $walmartOrderListMetaVo =& $this->extractOrderListUtil->getWalmartOrderListMetaVo();

        if($walmartOrderListMetaVo === null) {
            $logger =& LogUtil::getLogger();

            $logger->error("No metadata");

            return 1;
        }

        $walmartOrderListMetaDao =& WalmartOrderListMetaDAO::instance();

        $walmartOrderListMetaVo->setWalmartOrderListId($this->walmartOrderListId);

        $existingListMetaVo =& $walmartOrderListMetaDao->findByColumn(
            "walmart_order_list_id", $this->walmartOrderListId, PDO::PARAM_INT, null, null, "FOR UPDATE"
        );

        if($existingListMetaVo === null) {
            $walmartOrderListMetaDao->insert($walmartOrderListMetaVo);
        } else {
            if($walmartOrderListMetaVo->getNextCursor() === $existingListMetaVo->getNextCursor()) {
                $walmartOrderListMetaVo->setNextCursor(null);
            }

            PDOQuery::update()
                ->table($walmartOrderListMetaDao)
                ->set("updated", "UNIX_TIMESTAMP()")
                ->set("total_count", $walmartOrderListMetaVo->getTotalCount(), PDO::PARAM_INT)
                ->set("item_limit", $walmartOrderListMetaVo->getItemLimit(), PDO::PARAM_INT)
                ->set("next_cursor", $walmartOrderListMetaVo->getNextCursor(), PDO::PARAM_INT)
                ->where("walmart_order_list_id", $this->walmartOrderListId, PDO::PARAM_INT)
                ->executeGetRowCount()
            ;
        }

        return 0;
    }

    private function ensureCredential() {
        $config =& ConfigUtil::getConfig();
        $consumerId = $config->retrieve(WalmartUtil::CONFIG_CATEGORY, WalmartUtil::CONFIG_CONSUMER_ID);

        $this->extractOrderListUtil->setWalmartMarketplaceCredentialVo(
            WalmartMarketplaceCredentialDAO::instance()->findByColumn(
                "consumer_id", $consumerId, PDO::PARAM_STR
            )
        );
    }

    private function ensureContent() {
        $uuid = Uuid::uuid4()->toString();
        $walmartOrderListMetaDao =& WalmartOrderListMetaDAO::instance();
        $walmartMarketplaceCredentialVo = $this->extractOrderListUtil->getWalmartMarketplaceCredentialVo();
        $existingListMetaVo =& $walmartOrderListMetaDao->findSingle(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$walmartOrderListMetaDao, &$walmartMarketplaceCredentialVo) {
                $pdoQuery
                    ->columns($walmartOrderListMetaDao->formatSchemaAndTableName() . ".*")
                    ->join(WalmartOrderDAO::instance(), [
                        "walmart_order_list_id = id"
                    ])
                    ->where("walmart_marketplace_credential_id", $walmartMarketplaceCredentialVo->getId(), PDO::PARAM_INT)
                    ->where("next_cursor", "NULL", null, "IS NOT")
                ;
            }, "id DESC", 1
        );
        $response = null;
        $logger =& LogUtil::getLogger();
        $requestPath = "/v2/orders";

        if($existingListMetaVo !== null) {
            $logger->debug("Using cursor from meta: " . $existingListMetaVo->getNextCursor());

            $requestPath .= $existingListMetaVo->getNextCursor();
        } else {
            $logger->debug("Using start date: " . self::FIRST_EXTRACT_YEAR . "-" . self::FIRST_EXTRACT_MONTH . "-" . self::FIRST_EXTRACT_DAY);

            $requestPath .= "?createdStartDate=" .
                sprintf("%04d-%02d-%02d", self::FIRST_EXTRACT_YEAR, self::FIRST_EXTRACT_MONTH, self::FIRST_EXTRACT_DAY)
            ;
        }

        $response = WalmartUtil::executeSignedRequest($requestPath, $walmartMarketplaceCredentialVo, $uuid);

        if($response->getStatusCode() !== Response::HTTP_OK) {
            $logger->error("Error processing request ID: " . $uuid . ", response was: " . Utils::getVarDump($response));
        } else {
            $xmlReader = new XMLReader();

            $xmlReader->XML((string)$response->getBody());

            $this->extractOrderListUtil->setXmlReader($xmlReader);

            if($existingListMetaVo !== null) {
                $this->walmartOrderListId = $existingListMetaVo->getWalmartOrderListId();
            } else {
                $listVo = new WalmartOrderListVO();

                $listVo->setWalmartMarketplaceCredentialId($walmartMarketplaceCredentialVo->getId());

                $this->walmartOrderListId =& WalmartOrderListDAO::instance()->insert($listVo);
            }

            $walmartOrderListRequestVo = new WalmartOrderListRequestVO();

            $walmartOrderListRequestVo->setWalmartOrderListId($this->walmartOrderListId);
            $walmartOrderListRequestVo->setUuid($uuid);

            WalmartOrderListRequestDAO::instance()->insert($walmartOrderListRequestVo);
        }
    }

    /**
     * @return ExtractOrderListUtil
     */
    public function &getExtractOrderListUtil()
    {
        return $this->extractOrderListUtil;
    }
}
