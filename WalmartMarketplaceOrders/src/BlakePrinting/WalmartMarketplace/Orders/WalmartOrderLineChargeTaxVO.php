<?php

namespace BlakePrinting\WalmartMarketplace\Orders;

class WalmartOrderLineChargeTaxVO extends AbstractWalmartOrderTaxVO {
    /** @var int */
    private $walmartOrderLineChargeId = null;

    /**
     * @return int
     */
    public function getWalmartOrderLineChargeId()
    {
        return $this->walmartOrderLineChargeId;
    }

    /**
     * @param int $walmartOrderLineChargeId
     */
    public function setWalmartOrderLineChargeId($walmartOrderLineChargeId)
    {
        $this->walmartOrderLineChargeId = $walmartOrderLineChargeId;
    }
}
