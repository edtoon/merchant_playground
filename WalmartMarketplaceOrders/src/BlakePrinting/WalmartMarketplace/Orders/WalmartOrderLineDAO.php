<?php

namespace BlakePrinting\WalmartMarketplace\Orders;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static WalmartOrderLineDAO instance(PDO $connection = null, $schemaName = null)
 * @method WalmartOrderLineVO findById($id, $options = null, $useCache = false)
 * @method WalmartOrderLineVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartOrderLineVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartOrderLineVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartOrderLineVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartOrderLineVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartOrderLineVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartOrderLineVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class WalmartOrderLineDAO extends AbstractDataAccessObject {
}
