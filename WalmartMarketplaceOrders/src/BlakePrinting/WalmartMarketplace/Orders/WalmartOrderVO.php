<?php

namespace BlakePrinting\WalmartMarketplace\Orders;

use BlakePrinting\Db\ValueObject;

class WalmartOrderVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $walmartMarketplaceCredentialId = null;
    /** @var int */
    private $walmartEmailId = null;
    /** @var string */
    private $purchaseOrderId = null;
    /** @var string */
    private $customerOrderId = null;
    /** @var int */
    private $orderDate = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getWalmartMarketplaceCredentialId()
    {
        return $this->walmartMarketplaceCredentialId;
    }

    /**
     * @param int $walmartMarketplaceCredentialId
     */
    public function setWalmartMarketplaceCredentialId($walmartMarketplaceCredentialId)
    {
        $this->walmartMarketplaceCredentialId = $walmartMarketplaceCredentialId;
    }

    /**
     * @return int
     */
    public function getWalmartEmailId()
    {
        return $this->walmartEmailId;
    }

    /**
     * @param int $walmartEmailId
     */
    public function setWalmartEmailId($walmartEmailId)
    {
        $this->walmartEmailId = $walmartEmailId;
    }

    /**
     * @return string
     */
    public function getPurchaseOrderId()
    {
        return $this->purchaseOrderId;
    }

    /**
     * @param string $purchaseOrderId
     */
    public function setPurchaseOrderId($purchaseOrderId)
    {
        $this->purchaseOrderId = $purchaseOrderId;
    }

    /**
     * @return string
     */
    public function getCustomerOrderId()
    {
        return $this->customerOrderId;
    }

    /**
     * @param string $customerOrderId
     */
    public function setCustomerOrderId($customerOrderId)
    {
        $this->customerOrderId = $customerOrderId;
    }

    /**
     * @return int
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * @param int $orderDate
     */
    public function setOrderDate($orderDate)
    {
        $this->orderDate = $orderDate;
    }
}
