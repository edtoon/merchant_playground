<?php

namespace BlakePrinting\WalmartMarketplace\Orders\CLI;

use BlakePrinting\CLI\CLIApplication;

class Main extends CLIApplication {
    const APP_NAME = "WalmartMarketplaceOrders";
    const APP_VERSION = "0.0.1";

    public function __construct() {
        parent::__construct(self::APP_NAME, self::APP_VERSION);

        $this->add(new GetOrdersCommand());
    }
}
