<?php

namespace BlakePrinting\WalmartMarketplace\Orders\Extract;

use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Util\Utils;
use BlakePrinting\WalmartMarketplace\Orders\AbstractWalmartOrderChargeVO;
use BlakePrinting\WalmartMarketplace\Orders\AbstractWalmartOrderTaxVO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineChargeDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineChargeTaxDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineChargeTaxVO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineChargeVO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineRefundChargeDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineRefundChargeTaxDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineRefundChargeTaxVO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineRefundChargeVO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineRefundDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineRefundVO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineStatusDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineStatusTrackingDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineStatusTrackingVO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineStatusVO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineVO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderStatusDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderStatusVO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderVO;
use BlakePrinting\WalmartMarketplace\WalmartChargeNameDAO;
use BlakePrinting\WalmartMarketplace\WalmartChargeNameVO;
use BlakePrinting\WalmartMarketplace\WalmartChargeTypeDAO;
use BlakePrinting\WalmartMarketplace\WalmartChargeTypeVO;
use BlakePrinting\WalmartMarketplace\WalmartItemDAO;
use BlakePrinting\WalmartMarketplace\WalmartItemVO;
use BlakePrinting\WalmartMarketplace\WalmartRefundReasonDAO;
use BlakePrinting\WalmartMarketplace\WalmartRefundReasonVO;
use BlakePrinting\WalmartMarketplace\WalmartShippingCarrierDAO;
use BlakePrinting\WalmartMarketplace\WalmartShippingCarrierVO;
use BlakePrinting\WalmartMarketplace\WalmartShippingMethodDAO;
use BlakePrinting\WalmartMarketplace\WalmartShippingMethodVO;
use BlakePrinting\WalmartMarketplace\WalmartTaxDAO;
use BlakePrinting\WalmartMarketplace\WalmartTaxVO;
use BlakePrinting\WalmartMarketplace\WalmartUnitOfMeasurementDAO;
use BlakePrinting\WalmartMarketplace\WalmartUnitOfMeasurementVO;
use DateTime;
use Exception;
use PDO;
use ReflectionClass;
use XMLReader;

class ExtractOrderLineUtil {
    /** @var XMLReader */
    private $xmlReader = null;
    /** @var WalmartOrderLineVO */
    private $orderLineVo = null;
    private $lineCharges = [];
    private $lineStatuses = [];
    private $lineRefunds = [];

    public function extractOrderLine() {
        $this->orderLineVo = new WalmartOrderLineVO();

        while($this->xmlReader->read()) {
            if($this->xmlReader->nodeType === XMLReader::END_ELEMENT && $this->xmlReader->localName === "orderLine") {
                break;
            } else if($this->xmlReader->nodeType === XMLReader::ELEMENT) {
                switch($this->xmlReader->localName) {
                    case "lineNumber":
                    case "statusDate":
                    case "unitOfMeasurement":
                    case "amount":
                        $elementText = Utils::getTrimmedXmlOrNull($this->xmlReader);

                        if($elementText !== null) {
                            switch($this->xmlReader->localName) {
                                case "lineNumber":
                                    $this->orderLineVo->setLineNumber($elementText);
                                break;
                                case "statusDate":
                                    $dateTime = new DateTime($elementText);

                                    $this->orderLineVo->setStatusDate($dateTime->getTimestamp());
                                break;
                                case "unitOfMeasurement":
                                    $unitOfMeasurementVo =& WalmartUnitOfMeasurementDAO::instance()->findOrCreate(
                                        new WalmartUnitOfMeasurementVO($elementText)
                                    );

                                    $this->orderLineVo->setWalmartUnitOfMeasurementId($unitOfMeasurementVo->getId());
                                break;
                                case "amount":
                                    $this->orderLineVo->setQuantity($elementText);
                                break;
                            }
                        }
                    break;
                    case "item":
                        $itemVo =& $this->readItem();

                        $this->orderLineVo->setWalmartItemId($itemVo->getId());
                    break;
                    case "charge":
                        $lineChargeVo = new WalmartOrderLineChargeVO();
                        $lineChargeTaxes = [];

                        $this->readCharge($lineChargeVo, $lineChargeTaxes, WalmartOrderLineChargeTaxVO::class);

                        $this->lineCharges[] = [$lineChargeVo, $lineChargeTaxes];
                    break;
                    case "orderLineStatus":
                        $lineStatusVo = new WalmartOrderLineStatusVO();
                        $lineStatusTrackers = [];

                        $this->readStatus($lineStatusVo, $lineStatusTrackers);

                        $this->lineStatuses[] = [$lineStatusVo, $lineStatusTrackers];
                    break;
                    case "refund":
                        $lineRefundVo = new WalmartOrderLineRefundVO();
                        $lineRefundCharges = [];

                        $this->readRefund($lineRefundVo, $lineRefundCharges);

                        $this->lineRefunds[] = [$lineRefundVo, $lineRefundCharges];
                    break;
                }
            }
        }
    }

    /**
     * @param WalmartOrderVO $walmartOrderVo
     * @return WalmartOrderLineVO
     * @throws Exception
     */
    public function store(WalmartOrderVO &$walmartOrderVo) {
        $this->orderLineVo->setWalmartOrderId($walmartOrderVo->getId());

        $orderLineDao =& WalmartOrderLineDAO::instance();
        $existingOrderLineVo =& $orderLineDao->findSingle(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) {
                $pdoQuery
                    ->where("walmart_order_id", $this->orderLineVo->getWalmartOrderId(), PDO::PARAM_INT)
                    ->whereValueOrNull("line_number", $this->orderLineVo->getLineNumber(), PDO::PARAM_INT)
                ;
            }, null, null, "FOR UPDATE"
        );

        if($existingOrderLineVo === null) {
            $this->orderLineVo->setId($orderLineDao->insert($this->orderLineVo));
        } else {
            $this->orderLineVo->setId($existingOrderLineVo->getId());

            $differences = Utils::propertiesDifference($existingOrderLineVo, $this->orderLineVo);

            if($differences !== null) {
                $rowCount = $orderLineDao->update($this->orderLineVo);

                if($rowCount !== 1) {
                    throw new Exception(
                        "Too " . ($rowCount > 1 ? "many" : "few") . " results updating " .
                        "value object: " . Utils::getVarDump($this->orderLineVo) . ", " .
                        "differences: " . Utils::getVarDump($differences)
                    );
                }
            }
        }

        $this->storeCharges();
        $this->storeStatuses();
        $this->storeRefunds();

        return $this->orderLineVo;
    }

    private function storeCharges() {
        $orderLineId = $this->orderLineVo->getId();
        $lineChargeDao =& WalmartOrderLineChargeDAO::instance();
        $lineChargeTbl = $lineChargeDao->formatSchemaAndTableName();
        $lineChargeTaxDao =& WalmartOrderLineChargeTaxDAO::instance();
        $keepLineChargeIds = [];

        foreach($this->lineCharges as &$chargeData) {
            /** @var WalmartOrderLineChargeVO $lineChargeVo */
            $lineChargeVo =& $chargeData[0];
            /** @var WalmartOrderLineChargeTaxVO[] $taxes */
            $taxes =& $chargeData[1];
            $lineChargeId = null;

            $lineChargeVo->setWalmartOrderLineId($orderLineId);

            $existingChargeVo =& $lineChargeDao->findSingle(
                function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$orderLineId, &$lineChargeVo, &$keepLineChargeIds) {
                    $pdoQuery
                        ->where("walmart_order_line_id", $orderLineId, PDO::PARAM_INT)
                        ->whereValueOrNull("walmart_charge_type_id", $lineChargeVo->getWalmartChargeTypeId(), PDO::PARAM_INT)
                        ->whereValueOrNull("walmart_charge_name_id", $lineChargeVo->getWalmartChargeNameId(), PDO::PARAM_INT)
                        ->whereValueOrNull("currency", $lineChargeVo->getCurrency(), PDO::PARAM_INT)
                        ->whereValueOrNull("amount", $lineChargeVo->getAmount(), PDO::PARAM_INT)
                    ;

                    if(!empty($keepLineChargeIds)) {
                        $pdoQuery->where("id NOT IN (" . implode(",", $keepLineChargeIds) . ")");
                    }
                }, null, null, "FOR UPDATE"
            );

            if($existingChargeVo !== null) {
                $lineChargeId = $existingChargeVo->getId();
            } else {
                $lineChargeId = $lineChargeDao->insert($lineChargeVo);
            }

            $keepLineChargeIds[] = $lineChargeId;
            $keepLineChargeTaxIds = [];

            foreach($taxes as &$lineChargeTaxVo) {
                $lineChargeTaxId = null;

                $lineChargeTaxVo->setWalmartOrderLineChargeId($lineChargeId);

                $existingTaxVo =& $lineChargeTaxDao->findSingle(
                    function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$lineChargeId, &$lineChargeTaxVo, &$keepLineChargeTaxIds) {
                        $pdoQuery
                            ->where("walmart_order_line_charge_id", $lineChargeId, PDO::PARAM_INT)
                            ->whereValueOrNull("walmart_tax_id", $lineChargeTaxVo->getWalmartTaxId(), PDO::PARAM_INT)
                            ->whereValueOrNull("currency", $lineChargeTaxVo->getCurrency(), PDO::PARAM_INT)
                            ->whereValueOrNull("amount", $lineChargeTaxVo->getAmount(), PDO::PARAM_INT)
                        ;

                        if(!empty($keepLineChargeTaxIds)) {
                            $pdoQuery->where("id NOT IN (" . implode(",", $keepLineChargeTaxIds) . ")");
                        }
                    }, null, null, "FOR UPDATE"
                );

                if($existingTaxVo !== null) {
                    $lineChargeTaxId = $existingTaxVo->getId();
                } else {
                    $lineChargeTaxId = $lineChargeTaxDao->insert($lineChargeTaxVo);
                }

                $keepLineChargeTaxIds[] = $lineChargeTaxId;
            }

            unset($lineChargeTaxVo);

            $deleteOldTaxesQuery =& PDOQuery::delete()
                ->from($lineChargeTaxDao)
                ->where("walmart_order_line_charge_id", $lineChargeId, PDO::PARAM_INT)
            ;

            if(!empty($keepLineChargeTaxIds)) {
                $deleteOldTaxesQuery->where("id NOT IN (" . implode(",", $keepLineChargeTaxIds) . ")");
            }

            $deleteOldTaxesQuery->executeGetRowCount();
        }

        unset($chargeData);

        $deleteTaxesFromOldChargesQuery =& PDOQuery::delete()
            ->from($lineChargeTaxDao)
            ->where("walmart_order_line_charge_id IN (" .
                "SELECT id " .
                "FROM " . $lineChargeTbl . " " .
                "WHERE walmart_order_line_id = " . $orderLineId .
            ")")
        ;

        if(!empty($keepLineChargeIds)) {
            $deleteTaxesFromOldChargesQuery->where("walmart_order_line_charge_id NOT IN (" . implode(",", $keepLineChargeIds) . ")");
        }

        $deleteTaxesFromOldChargesQuery->executeGetRowCount();

        $deleteOldChargesQuery =& PDOQuery::delete()
            ->from($lineChargeDao)
            ->where("walmart_order_line_id", $orderLineId, PDO::PARAM_INT)
        ;

        if(!empty($keepLineChargeIds)) {
            $deleteOldChargesQuery->where("id NOT IN (" . implode(",", $keepLineChargeIds) . ")");
        }

        $deleteOldChargesQuery->executeGetRowCount();
    }

    private function storeStatuses() {
        $orderLineId = $this->orderLineVo->getId();
        $lineStatusDao =& WalmartOrderLineStatusDAO::instance();
        $lineStatusTbl = $lineStatusDao->formatSchemaAndTableName();
        $lineStatusTrackingDao =& WalmartOrderLineStatusTrackingDAO::instance();
        $keepLineStatusIds = [];

        foreach($this->lineStatuses as &$statusData) {
            /** @var WalmartOrderLineStatusVO $lineStatusVo */
            $lineStatusVo =& $statusData[0];
            /** @var WalmartOrderLineStatusTrackingVO[] $trackers */
            $trackers =& $statusData[1];
            $lineStatusId = null;

            $lineStatusVo->setWalmartOrderLineId($orderLineId);

            $existingStatusVo =& $lineStatusDao->findSingle(
                function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$orderLineId, &$lineStatusVo, &$keepLineStatusIds) {
                    $pdoQuery
                        ->where("walmart_order_line_id", $orderLineId, PDO::PARAM_INT)
                        ->whereValueOrNull("walmart_unit_of_measurement_id", $lineStatusVo->getWalmartUnitOfMeasurementId(), PDO::PARAM_INT)
                        ->whereValueOrNull("quantity", $lineStatusVo->getQuantity(), PDO::PARAM_INT)
                        ->whereValueOrNull("cancellation_reason", $lineStatusVo->getCancellationReason(), PDO::PARAM_INT)
                    ;

                    if(!empty($keepLineStatusIds)) {
                        $pdoQuery->where("id NOT IN (" . implode(",", $keepLineStatusIds) . ")");
                    }
                }, null, null, "FOR UPDATE"
            );

            if($existingStatusVo !== null) {
                $lineStatusId = $existingStatusVo->getId();
            } else {
                $lineStatusId = $lineStatusDao->insert($lineStatusVo);
            }

            $keepLineStatusIds[] = $lineStatusId;
            $keepLineStatusTrackingIds = [];

            foreach($trackers as &$lineStatusTrackingVo) {
                $lineStatusTrackingId = null;

                $lineStatusTrackingVo->setWalmartOrderLineStatusId($lineStatusId);

                $existingTrackingVo =& $lineStatusTrackingDao->findSingle(
                    function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$lineStatusId, &$lineStatusTrackingVo, &$keepLineStatusTrackingIds) {
                        $pdoQuery
                            ->where("walmart_order_line_status_id", $lineStatusId, PDO::PARAM_INT)
                            ->whereValueOrNull("walmart_shipping_carrier_id", $lineStatusTrackingVo->getWalmartShippingCarrierId(), PDO::PARAM_INT)
                            ->whereValueOrNull("walmart_shipping_method_id", $lineStatusTrackingVo->getWalmartShippingMethodId(), PDO::PARAM_INT)
                            ->whereValueOrNull("shipped", $lineStatusTrackingVo->getShipped(), PDO::PARAM_INT)
                            ->whereValueOrNull("tracking_number", $lineStatusTrackingVo->getTrackingNumber(), PDO::PARAM_INT)
                        ;

                        if(!empty($keepLineStatusTrackingIds)) {
                            $pdoQuery->where("id NOT IN (" . implode(",", $keepLineStatusTrackingIds) . ")");
                        }
                    }, null, null, "FOR UPDATE"
                );

                if($existingTrackingVo !== null) {
                    $lineStatusTrackingId = $existingTrackingVo->getId();
                } else {
                    $lineStatusTrackingId = $lineStatusTrackingDao->insert($lineStatusTrackingVo);
                }

                $keepLineStatusTrackingIds[] = $lineStatusTrackingId;
            }

            unset($lineStatusTrackingVo);

            $deleteOldTrackersQuery =& PDOQuery::delete()
                ->from($lineStatusTrackingDao)
                ->where("walmart_order_line_status_id", $lineStatusId, PDO::PARAM_INT)
            ;

            if(!empty($keepLineStatusTrackingIds)) {
                $deleteOldTrackersQuery->where("id NOT IN (" . implode(",", $keepLineStatusTrackingIds) . ")");
            }

            $deleteOldTrackersQuery->executeGetRowCount();
        }

        unset($statusData);

        $deleteTrackersFromOldStatusesQuery =& PDOQuery::delete()
            ->from($lineStatusTrackingDao)
            ->where("walmart_order_line_status_id IN (" .
                "SELECT id " .
                "FROM " . $lineStatusTbl . " " .
                "WHERE walmart_order_line_id = " . $orderLineId .
            ")")
        ;

        if(!empty($keepLineStatusIds)) {
            $deleteTrackersFromOldStatusesQuery->where("walmart_order_line_status_id NOT IN (" . implode(",", $keepLineStatusIds) . ")");
        }

        $deleteTrackersFromOldStatusesQuery->executeGetRowCount();

        $deleteOldStatusesQuery =& PDOQuery::delete()
            ->from($lineStatusDao)
            ->where("walmart_order_line_id", $orderLineId, PDO::PARAM_INT)
        ;

        if(!empty($keepLineStatusIds)) {
            $deleteOldStatusesQuery->where("id NOT IN (" . implode(",", $keepLineStatusIds) . ")");
        }

        $deleteOldStatusesQuery->executeGetRowCount();
    }

    private function storeRefunds() {
        $orderLineId = $this->orderLineVo->getId();
        $lineRefundDao =& WalmartOrderLineRefundDAO::instance();
        $lineRefundChargeDao =& WalmartOrderLineRefundChargeDAO::instance();
        $lineRefundChargeTbl = $lineRefundChargeDao->formatSchemaAndTableName();
        $lineRefundChargeTaxDao =& WalmartOrderLineRefundChargeTaxDAO::instance();
        $keepLineRefundIds = [];

        foreach($this->lineRefunds as &$refundData) {
            /** @var WalmartOrderLineRefundVO $lineRefundVo */
            $lineRefundVo =& $refundData[0];
            $lineRefundCharges =& $refundData[1];
            $lineRefundId = null;

            $lineRefundVo->setWalmartOrderLineId($orderLineId);

            $existingRefundVo =& $lineRefundDao->findSingle(
                function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$lineRefundVo, &$keepLineRefundIds) {
                    $pdoQuery
                        ->where("walmart_order_line_id", $lineRefundVo->getWalmartOrderLineId(), PDO::PARAM_INT)
                        ->whereValueOrNull("walmart_refund_reason_id", $lineRefundVo->getWalmartRefundReasonId(), PDO::PARAM_INT)
                        ->whereValueOrNull("refund_id", $lineRefundVo->getRefundId(), PDO::PARAM_STR)
                        ->whereValueOrNull("comments", $lineRefundVo->getComments(), PDO::PARAM_STR)
                    ;

                    if(!empty($keepLineRefundIds)) {
                        $pdoQuery->where("id NOT IN (" . implode(",", $keepLineRefundIds) . ")");
                    }
                }, null, null, "FOR UPDATE"
            );

            if($existingRefundVo !== null) {
                $lineRefundId = $existingRefundVo->getId();
            } else {
                $lineRefundId = $lineRefundDao->insert($lineRefundVo);
            }

            $keepLineRefundIds[] = $lineRefundId;
            $keepLineRefundChargeIds = [];

            foreach($lineRefundCharges as &$refundChargeData) {
                /** @var WalmartOrderLineRefundChargeVO $lineRefundChargeVo */
                $lineRefundChargeVo =& $refundChargeData[0];
                /** @var WalmartOrderLineRefundChargeTaxVO[] $taxes */
                $taxes =& $refundChargeData[1];
                $lineRefundChargeId = null;

                $lineRefundChargeVo->setWalmartOrderLineRefundId($lineRefundId);

                $existingRefundChargeVo =& $lineRefundChargeDao->findSingle(
                    function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$lineRefundId, &$lineRefundChargeVo, &$keepLineRefundChargeIds) {
                        $pdoQuery
                            ->where("walmart_order_line_refund_id", $lineRefundId, PDO::PARAM_INT)
                            ->whereValueOrNull("walmart_charge_type_id", $lineRefundChargeVo->getWalmartChargeTypeId(), PDO::PARAM_INT)
                            ->whereValueOrNull("walmart_charge_name_id", $lineRefundChargeVo->getWalmartChargeNameId(), PDO::PARAM_INT)
                            ->whereValueOrNull("currency", $lineRefundChargeVo->getCurrency(), PDO::PARAM_INT)
                            ->whereValueOrNull("amount", $lineRefundChargeVo->getAmount(), PDO::PARAM_INT)
                        ;

                        if(!empty($keepLineRefundChargeIds)) {
                            $pdoQuery->where("id NOT IN (" . implode(",", $keepLineRefundChargeIds) . ")");
                        }
                    }, null, null, "FOR UPDATE"
                );

                if($existingRefundChargeVo !== null) {
                    $lineRefundChargeId = $existingRefundChargeVo->getId();
                } else {
                    $lineRefundChargeId = $lineRefundChargeDao->insert($lineRefundChargeVo);
                }

                $keepLineRefundChargeIds[] = $lineRefundChargeId;
                $keepLineRefundChargeTaxIds = [];

                foreach($taxes as &$lineRefundChargeTaxVo) {
                    $lineRefundChargeTaxId = null;

                    $lineRefundChargeTaxVo->setWalmartOrderLineRefundChargeId($lineRefundChargeId);

                    $existingTaxVo =& $lineRefundChargeTaxDao->findSingle(
                        function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$lineRefundChargeId, &$lineRefundChargeTaxVo, &$keepLineRefundChargeTaxIds) {
                            $pdoQuery
                                ->where("walmart_order_line_refund_charge_id", $lineRefundChargeId, PDO::PARAM_INT)
                                ->whereValueOrNull("walmart_tax_id", $lineRefundChargeTaxVo->getWalmartTaxId(), PDO::PARAM_INT)
                                ->whereValueOrNull("currency", $lineRefundChargeTaxVo->getCurrency(), PDO::PARAM_INT)
                                ->whereValueOrNull("amount", $lineRefundChargeTaxVo->getAmount(), PDO::PARAM_INT)
                            ;

                            if(!empty($keepLineRefundChargeTaxIds)) {
                                $pdoQuery->where("id NOT IN (" . implode(",", $keepLineRefundChargeTaxIds) . ")");
                            }
                        }, null, null, "FOR UPDATE"
                    );

                    if($existingTaxVo !== null) {
                        $lineRefundChargeTaxId = $existingTaxVo->getId();
                    } else {
                        $lineRefundChargeTaxId = $lineRefundChargeTaxDao->insert($lineRefundChargeTaxVo);
                    }

                    $keepLineRefundChargeTaxIds[] = $lineRefundChargeTaxId;
                }

                unset($lineRefundChargeTaxVo);

                $deleteOldTaxesQuery =& PDOQuery::delete()
                    ->from($lineRefundChargeTaxDao)
                    ->where("walmart_order_line_refund_charge_id", $lineRefundChargeId, PDO::PARAM_INT)
                ;

                if(!empty($keepLineRefundChargeTaxIds)) {
                    $deleteOldTaxesQuery->where("id NOT IN (" . implode(",", $keepLineRefundChargeTaxIds) . ")");
                }

                $deleteOldTaxesQuery->executeGetRowCount();
            }

            unset($refundChargeData);

            $deleteTaxesFromOldRefundChargesQuery =& PDOQuery::delete()
                ->from($lineRefundChargeTaxDao)
                ->where(
                    "walmart_order_line_refund_charge_id IN (" .
                        "SELECT id " .
                        "FROM " . $lineRefundChargeTbl . " " .
                        "WHERE walmart_order_line_refund_id = " . $lineRefundId .
                    ")"
                )
            ;

            if(!empty($keepLineRefundChargeIds)) {
                $deleteTaxesFromOldRefundChargesQuery->where("walmart_order_line_refund_charge_id NOT IN (" . implode(",", $keepLineRefundChargeIds) . ")");
            }

            $deleteTaxesFromOldRefundChargesQuery->executeGetRowCount();

            $deleteOldRefundChargesQuery =& PDOQuery::delete()
                ->from($lineRefundChargeDao)
                ->where("walmart_order_line_refund_id", $lineRefundId, PDO::PARAM_INT)
            ;

            if(!empty($keepLineRefundChargeIds)) {
                $deleteOldRefundChargesQuery->where("id NOT IN (" . implode(",", $keepLineRefundChargeIds) . ")");
            }

            $deleteOldRefundChargesQuery->executeGetRowCount();

            $deleteOldRefundsQuery =& PDOQuery::delete()
                ->from($lineRefundDao)
                ->where("walmart_order_line_id", $orderLineId, PDO::PARAM_INT)
            ;

            if(!empty($keepLineRefundIds)) {
                $deleteOldRefundsQuery->where("id NOT IN (" . implode(",", $keepLineRefundIds) . ")");
            }

            $deleteOldRefundsQuery->executeGetRowCount();
        }

        unset($refundData);
    }

    /**
     * @return WalmartItemVO
     */
    private function &readItem() {
        $itemVo = new WalmartItemVO();
        $addressTypeVo = null;

        while($this->xmlReader->read()) {
            if($this->xmlReader->nodeType === XMLReader::END_ELEMENT && $this->xmlReader->localName === "item") {
                break;
            } else if($this->xmlReader->nodeType === XMLReader::ELEMENT) {
                switch($this->xmlReader->localName) {
                    case "sku":
                        $itemVo->setSku(Utils::getTrimmedXmlOrNull($this->xmlReader));
                    break;
                    case "productName":
                        $itemVo->setProductName(Utils::getTrimmedXmlOrNull($this->xmlReader));
                    break;
                }
            }
        }

        return WalmartItemDAO::instance()->findOrCreate($itemVo, "FOR UPDATE", ["sku", "product_name"], ["created"]);
    }

    /**
     * @param AbstractWalmartOrderChargeVO $chargeVo
     * @param array $chargeTaxes
     * @param string $taxClassName
     */
    private function readCharge(AbstractWalmartOrderChargeVO &$chargeVo, array &$chargeTaxes, $taxClassName) {
        while($this->xmlReader->read()) {
            if($this->xmlReader->nodeType === XMLReader::END_ELEMENT && $this->xmlReader->localName === "charge") {
                break;
            } else if($this->xmlReader->nodeType === XMLReader::ELEMENT) {
                switch($this->xmlReader->localName) {
                    case "chargeType":
                    case "chargeName":
                    case "currency":
                    case "amount":
                        $elementText = Utils::getTrimmedXmlOrNull($this->xmlReader);

                        if($elementText !== null) {
                            switch($this->xmlReader->localName) {
                                case "chargeType":
                                    $chargeTypeVo =& WalmartChargeTypeDAO::instance()->findOrCreate(
                                        new WalmartChargeTypeVO($elementText)
                                    );

                                    $chargeVo->setWalmartChargeTypeId($chargeTypeVo->getId());
                                break;
                                case "chargeName":
                                    $chargeNameVo =& WalmartChargeNameDAO::instance()->findOrCreate(
                                        new WalmartChargeNameVO($elementText)
                                    );

                                    $chargeVo->setWalmartChargeNameId($chargeNameVo->getId());
                                break;
                                case "currency":
                                    $chargeVo->setCurrency($elementText);
                                break;
                                case "amount":
                                    $chargeVo->setAmount($elementText);
                                break;
                            }
                        }
                    break;
                    case "tax":
                        $taxVo = new $taxClassName();

                        $this->readTax($taxVo);

                        $chargeTaxes[] = $taxVo;
                    break;
                }
            }
        }
    }

    /**
     * @param WalmartOrderLineRefundVO $refundVo
     * @param array $refundCharges
     */
    private function readRefund(WalmartOrderLineRefundVO &$refundVo, array &$refundCharges) {
        while($this->xmlReader->read()) {
            if($this->xmlReader->nodeType === XMLReader::END_ELEMENT && $this->xmlReader->localName === "refund") {
                break;
            } else if($this->xmlReader->nodeType === XMLReader::ELEMENT) {
                switch($this->xmlReader->localName) {
                    case "refundId":
                        $refundVo->setRefundId(Utils::getXmlReaderText($this->xmlReader));
                    break;
                    case "refundReason":
                        $walmartRefundReasonVo =& WalmartRefundReasonDAO::instance()->findOrCreate(
                            new WalmartRefundReasonVO(Utils::getXmlReaderText($this->xmlReader))
                        );

                        $refundVo->setWalmartRefundReasonId($walmartRefundReasonVo->getId());
                    break;
                    case "comments":
                        $refundVo->setComments(Utils::getXmlReaderText($this->xmlReader));
                    break;
                    case "charges":
                        $this->readRefundCharge($refundVo, $refundCharges);
                    break;
                }
            }
        }
    }

    /**
     * @param WalmartOrderLineRefundVO $refundVo
     * @param array $refundCharges
     */
    private function readRefundCharge(WalmartOrderLineRefundVO &$refundVo, array &$refundCharges) {
        while($this->xmlReader->read()) {
            if($this->xmlReader->nodeType === XMLReader::END_ELEMENT && $this->xmlReader->localName === "charges") {
                break;
            } else if($this->xmlReader->nodeType === XMLReader::ELEMENT) {
                switch($this->xmlReader->localName) {
                    case "charge":
                        $lineRefundChargeVo = new WalmartOrderLineRefundChargeVO();
                        $lineRefundChargeTaxes = [];

                        $this->readCharge($lineRefundChargeVo, $lineRefundChargeTaxes, WalmartOrderLineRefundChargeTaxVO::class);

                        $refundCharges[] = [$lineRefundChargeVo, $lineRefundChargeTaxes];
                    break;
                }
            }
        }
    }

    /**
     * @param AbstractWalmartOrderTaxVO $taxVo
     */
    private function readTax(AbstractWalmartOrderTaxVO &$taxVo) {
        while($this->xmlReader->read()) {
            if($this->xmlReader->nodeType === XMLReader::END_ELEMENT && $this->xmlReader->localName === "tax") {
                break;
            } else if($this->xmlReader->nodeType === XMLReader::ELEMENT) {
                switch($this->xmlReader->localName) {
                    case "taxName":
                        $taxVo->setWalmartTaxId(WalmartTaxDAO::instance()->findOrCreate(
                            new WalmartTaxVO(Utils::getXmlReaderText($this->xmlReader))
                        )->getId());
                    break;
                    case "currency":
                        $taxVo->setCurrency(Utils::getXmlReaderText($this->xmlReader));
                    break;
                    case "amount":
                        $taxVo->setAmount(Utils::getXmlReaderText($this->xmlReader));
                    break;
                }
            }
        }
    }

    /**
     * @param WalmartOrderLineStatusVO $orderLineStatusVo
     * @param array $trackers
     */
    private function readStatus(WalmartOrderLineStatusVO &$orderLineStatusVo, array &$trackers) {
        while($this->xmlReader->read()) {
            if($this->xmlReader->nodeType === XMLReader::END_ELEMENT && $this->xmlReader->localName === "orderLineStatus") {
                break;
            } else if($this->xmlReader->nodeType === XMLReader::ELEMENT) {
                switch($this->xmlReader->localName) {
                    case "status":
                        $orderStatus = Utils::getTrimmedStringOrNull(Utils::getXmlReaderText($this->xmlReader));

                        if($orderStatus !== null) {
                            $orderStatusVo =& WalmartOrderStatusDAO::instance()->findOrCreate(
                                new WalmartOrderStatusVO($orderStatus)
                            );

                            $orderLineStatusVo->setWalmartOrderStatusId($orderStatusVo->getId());
                        }
                    break;
                    case "unitOfMeasurement":
                        $unitOfMeasurement = Utils::getTrimmedStringOrNull(Utils::getXmlReaderText($this->xmlReader));

                        if($unitOfMeasurement !== null) {
                            $unitOfMeasurementVo =& WalmartUnitOfMeasurementDAO::instance()->findOrCreate(
                                new WalmartUnitOfMeasurementVO($unitOfMeasurement)
                            );

                            $orderLineStatusVo->setWalmartUnitOfMeasurementId($unitOfMeasurementVo->getId());
                        }
                    break;
                    case "amount":
                        $quantity = Utils::getTrimmedStringOrNull(Utils::getXmlReaderText($this->xmlReader));

                        if($quantity !== null) {
                            $orderLineStatusVo->setQuantity($quantity);
                        }
                    break;
                    case "cancellationReason":
                        $cancellationReason = Utils::getTrimmedStringOrNull(Utils::getXmlReaderText($this->xmlReader));

                        if($cancellationReason !== null) {
                            $orderLineStatusVo->setCancellationReason($cancellationReason);
                        }
                    break;
                    case "trackingInfo":
                        $trackingVo = new WalmartOrderLineStatusTrackingVO();

                        $this->readTrackingInfo($trackingVo);

                        $trackers[] = $trackingVo;
                    break;
                }
            }
        }
    }

    /**
     * @param WalmartOrderLineStatusTrackingVO $trackingVo
     */
    private function readTrackingInfo(WalmartOrderLineStatusTrackingVO &$trackingVo) {
        while($this->xmlReader->read()) {
            if($this->xmlReader->nodeType === XMLReader::END_ELEMENT && $this->xmlReader->localName === "trackingInfo") {
                break;
            } else if($this->xmlReader->nodeType === XMLReader::ELEMENT) {
                switch($this->xmlReader->localName) {
                    case "carrier":
                    case "otherCarrier":
                        $carrierName = Utils::getTrimmedStringOrNull(Utils::getXmlReaderText($this->xmlReader));

                        if($carrierName !== null) {
                            $trackingVo->setWalmartShippingCarrierId(
                                WalmartShippingCarrierDAO::instance()->findOrCreate(
                                    new WalmartShippingCarrierVO($carrierName)
                                )->getId()
                            );
                        }
                    break;
                    case "methodCode":
                        $methodCode = Utils::getTrimmedStringOrNull(Utils::getXmlReaderText($this->xmlReader));

                        if($methodCode !== null) {
                            $trackingVo->setWalmartShippingMethodId(
                                WalmartShippingMethodDAO::instance()->findOrCreate(
                                    new WalmartShippingMethodVO($methodCode)
                                )->getId()
                            );
                        }
                    break;
                    case "shipDateTime":
                        $shipDateTime = Utils::getTrimmedStringOrNull(Utils::getXmlReaderText($this->xmlReader));

                        if($shipDateTime !== null) {
                            $dateTime = new DateTime($shipDateTime);
                            $trackingVo->setShipped($dateTime->getTimestamp());
                        }
                    break;
                    case "trackingNumber":
                        $trackingNumber = Utils::getTrimmedStringOrNull(Utils::getXmlReaderText($this->xmlReader));

                        if($trackingNumber !== null) {
                            $trackingVo->setTrackingNumber($trackingNumber);
                        }
                    break;
                }
            }
        }
    }

    /**
     * @return XMLReader
     */
    public function &getXmlReader()
    {
        return $this->xmlReader;
    }

    /**
     * @param XMLReader $xmlReader
     */
    public function setXmlReader(XMLReader &$xmlReader)
    {
        $this->xmlReader =& $xmlReader;
    }
}
