<?php

namespace BlakePrinting\WalmartMarketplace\Orders\Extract;

use BlakePrinting\Util\Utils;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderListMetaVO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderShippingDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderShippingVO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderVO;
use BlakePrinting\WalmartMarketplace\WalmartAddressDAO;
use BlakePrinting\WalmartMarketplace\WalmartAddressTypeDAO;
use BlakePrinting\WalmartMarketplace\WalmartAddressTypeVO;
use BlakePrinting\WalmartMarketplace\WalmartAddressVO;
use BlakePrinting\WalmartMarketplace\WalmartEmailDAO;
use BlakePrinting\WalmartMarketplace\WalmartEmailVO;
use BlakePrinting\WalmartMarketplace\WalmartMarketplaceCredentialVO;
use BlakePrinting\WalmartMarketplace\WalmartPhoneDAO;
use BlakePrinting\WalmartMarketplace\WalmartPhoneVO;
use BlakePrinting\WalmartMarketplace\WalmartShippingMethodDAO;
use BlakePrinting\WalmartMarketplace\WalmartShippingMethodVO;
use DateTime;
use Exception;
use PDO;
use XMLReader;

class ExtractOrderListUtil {
    /** @var XMLReader */
    private $xmlReader = null;
    /** @var WalmartMarketplaceCredentialVO */
    private $walmartMarketplaceCredentialVo = null;
    /** @var WalmartOrderListMetaVO */
    private $walmartOrderListMetaVo = null;
    /** @var WalmartOrderVO[] */
    private $walmartOrders = [];

    /**
     * @throws Exception
     */
    public function extractOrderList() {
        $this->walmartOrders = [];

        try {
            $this->walmartOrderListMetaVo = new WalmartOrderListMetaVO();

            /*
             * localName, name, namespaceURI, nodeType, prefix, value, hasValue
             */
            while($this->xmlReader->read()) {
                if($this->xmlReader->nodeType === XMLReader::ELEMENT) {
                    switch($this->xmlReader->localName) {
                        case "meta":
                            $this->readListMeta();
                        break;
                        case "order":
                            $orders[] = self::readOrder();
                        break;
                    }
                }
            }
        } finally {
            $this->xmlReader->close();
        }
    }

    private function readListMeta() {
        while($this->xmlReader->read()) {
            if($this->xmlReader->nodeType === XMLReader::END_ELEMENT && $this->xmlReader->localName === "meta") {
                break;
            } else if($this->xmlReader->nodeType === XMLReader::ELEMENT) {
                switch($this->xmlReader->localName) {
                    case "totalCount":
                    case "limit":
                    case "nextCursor":
                        $elementText = Utils::getTrimmedStringOrNull(Utils::getXmlReaderText($this->xmlReader));

                        if($elementText !== null) {
                            switch($this->xmlReader->localName) {
                                case "totalCount":
                                    $this->walmartOrderListMetaVo->setTotalCount($elementText);
                                break;
                                case "limit":
                                    $this->walmartOrderListMetaVo->setItemLimit($elementText);
                                break;
                                case "nextCursor":
                                    $this->walmartOrderListMetaVo->setNextCursor($elementText);
                                break;
                            }
                        }
                    break;
                }
            }
        }
    }

    /**
     * @return WalmartOrderVO
     */
    private function &readOrder() {
        $orderVo = new WalmartOrderVO();
        /** @var ExtractOrderLineUtil[] $extractedOrderLineUtils */
        $extractedOrderLineUtils = [];
        $shippingVo = null;

        if($this->walmartMarketplaceCredentialVo !== null) {
            $orderVo->setWalmartMarketplaceCredentialId($this->walmartMarketplaceCredentialVo->getId());
        }

        while($this->xmlReader->read()) {
            if($this->xmlReader->nodeType === XMLReader::END_ELEMENT && $this->xmlReader->localName === "order") {
                break;
            } else if($this->xmlReader->nodeType === XMLReader::ELEMENT) {
                switch($this->xmlReader->localName) {
                    case "purchaseOrderId":
                    case "customerOrderId":
                    case "orderDate":
                    case "customerEmailId":
                        $elementText = Utils::getTrimmedStringOrNull(Utils::getXmlReaderText($this->xmlReader));

                        if($elementText !== null) {
                            switch($this->xmlReader->localName) {
                                case "purchaseOrderId":
                                    $orderVo->setPurchaseOrderId($elementText);
                                break;
                                case "customerOrderId":
                                    $orderVo->setCustomerOrderId($elementText);
                                break;
                                case "orderDate":
                                    $dateTime = new DateTime($elementText);

                                    $orderVo->setOrderDate($dateTime->getTimestamp());
                                break;
                                case "customerEmailId":
                                    $emailVo =& WalmartEmailDAO::instance()->findOrCreate(
                                        new WalmartEmailVO($elementText)
                                    );

                                    $orderVo->setWalmartEmailId($emailVo->getId());
                                break;
                            }
                        }
                    break;
                    case "shippingInfo":
                        $shippingVo =& $this->readShipping();
                    break;
                    case "orderLine":
                        $extractOrderLineUtil = new ExtractOrderLineUtil();

                        $extractOrderLineUtil->setXmlReader($this->xmlReader);
                        $extractOrderLineUtil->extractOrderLine();

                        $extractedOrderLineUtils[] = $extractOrderLineUtil;
                    break;
                }
            }
        }

        $orderVo =& WalmartOrderDAO::instance()->findOrCreate($orderVo);
        $orderId = $orderVo->getId();

        if($shippingVo !== null) {
            $shippingVo->setWalmartOrderId($orderId);

            $shippingDao =& WalmartOrderShippingDAO::instance();
            $existingShippingVo =& $shippingDao->findByColumn("walmart_order_id", $orderId, PDO::PARAM_INT, null, null, "FOR UPDATE");

            if($existingShippingVo === null) {
                $shippingDao->insert($shippingVo);
            } else {
                $differences = Utils::propertiesDifference($existingShippingVo, $shippingVo);

                if($differences !== null) {
                    $rowCount = $shippingDao->update(
                        $shippingVo, null,
                        ["walmart_address_id", "walmart_shipping_method_id", "walmart_phone_id", "estimated_ship_date", "estimated_delivery_date"]
                    );

                    if($rowCount !== 1) {
                        throw new Exception(
                            "Too " . ($rowCount > 1 ? "many" : "few") . " results updating " .
                            "value object: " . Utils::getVarDump($shippingVo) . ", " .
                            "differences: " . Utils::getVarDump($differences)
                        );
                    }
                }
            }
        }

        foreach($extractedOrderLineUtils as &$extractedOrderLineUtil) {
            $extractedOrderLineUtil->store($orderVo);
        }

        unset($extractedOrderLineUtil);

        return $orderVo;
    }

    /**
     * @return WalmartOrderShippingVO
     */
    private function &readShipping() {
        $shippingVo = new WalmartOrderShippingVO();

        while($this->xmlReader->read()) {
            if($this->xmlReader->nodeType === XMLReader::END_ELEMENT && $this->xmlReader->localName === "shippingInfo") {
                break;
            } else if($this->xmlReader->nodeType === XMLReader::ELEMENT) {
                switch($this->xmlReader->localName) {
                    case "phone":
                    case "estimatedShipDate":
                    case "estimatedDeliveryDate":
                    case "methodCode":
                        $elementText = Utils::getTrimmedStringOrNull(Utils::getXmlReaderText($this->xmlReader));

                        if($elementText !== null) {
                            switch($this->xmlReader->localName) {
                                case "phone":
                                    $phoneVo =& WalmartPhoneDAO::instance()->findOrCreate(
                                        new WalmartPhoneVO($elementText)
                                    );

                                    $shippingVo->setWalmartPhoneId($phoneVo->getId());
                                break;
                                case "estimatedShipDate":
                                    $dateTime = new DateTime($elementText);

                                    $shippingVo->setEstimatedShipDate($dateTime->getTimestamp());
                                break;
                                case "estimatedDeliveryDate":
                                    $dateTime = new DateTime($elementText);

                                    $shippingVo->setEstimatedDeliveryDate($dateTime->getTimestamp());
                                break;
                                case "methodCode":
                                    $shippingMethodVo =& WalmartShippingMethodDAO::instance()->findOrCreate(
                                        new WalmartShippingMethodVO($elementText)
                                    );

                                    $shippingVo->setWalmartShippingMethodId($shippingMethodVo->getId());
                                break;
                            }
                        }
                    break;
                    case "postalAddress":
                        $addressVo =& $this->readPostalAddress();

                        $shippingVo->setWalmartAddressId($addressVo->getId());
                    break;
                }
            }
        }

        return $shippingVo;
    }

    /**
     * @return WalmartAddressVO
     */
    private function &readPostalAddress() {
        $addressVo = new WalmartAddressVO();
        $addressTypeVo = null;

        while($this->xmlReader->read()) {
            if($this->xmlReader->nodeType === XMLReader::END_ELEMENT && $this->xmlReader->localName === "postalAddress") {
                break;
            } else if($this->xmlReader->nodeType === XMLReader::ELEMENT) {
                switch($this->xmlReader->localName) {
                    case "name":
                    case "address1":
                    case "address2":
                    case "city":
                    case "state":
                    case "postalCode":
                    case "country":
                    case "addressType":
                        $elementText = Utils::getTrimmedStringOrNull(Utils::getXmlReaderText($this->xmlReader));

                        if($elementText !== null) {
                            switch($this->xmlReader->localName) {
                                case "name":
                                    $addressVo->setName($elementText);
                                break;
                                case "address1":
                                    $addressVo->setAddress1($elementText);
                                break;
                                case "address2":
                                    $addressVo->setAddress2($elementText);
                                break;
                                case "city":
                                    $addressVo->setCity($elementText);
                                break;
                                case "state":
                                    $addressVo->setState($elementText);
                                break;
                                case "postalCode":
                                    $addressVo->setPostalCode($elementText);
                                break;
                                case "country":
                                    $addressVo->setCountry($elementText);
                                break;
                                case "addressType":
                                    $addressTypeVo =& WalmartAddressTypeDAO::instance()->findOrCreate(
                                        new WalmartAddressTypeVO($elementText)
                                    );

                                    $addressVo->setWalmartAddressTypeId($addressTypeVo->getId());
                                break;
                            }
                        }
                    break;
                }
            }
        }

        return WalmartAddressDAO::instance()->findOrCreate(
            $addressVo, "FOR UPDATE",
            ["walmart_address_type_id", "name", "address1", "address2", "city", "state", "postal_code", "country"],
            ["created"]
        );
    }

    /**
     * @return XMLReader
     */
    public function &getXmlReader()
    {
        return $this->xmlReader;
    }

    /**
     * @param XMLReader $xmlReader
     */
    public function setXmlReader(XMLReader &$xmlReader)
    {
        $this->xmlReader =& $xmlReader;
    }

    /**
     * @return WalmartMarketplaceCredentialVO
     */
    public function &getWalmartMarketplaceCredentialVo()
    {
        return $this->walmartMarketplaceCredentialVo;
    }

    /**
     * @param WalmartMarketplaceCredentialVO $walmartMarketplaceCredentialVo
     */
    public function setWalmartMarketplaceCredentialVo(WalmartMarketplaceCredentialVO &$walmartMarketplaceCredentialVo)
    {
        $this->walmartMarketplaceCredentialVo =& $walmartMarketplaceCredentialVo;
    }

    /**
     * @return WalmartOrderListMetaVO
     */
    public function &getWalmartOrderListMetaVo()
    {
        return $this->walmartOrderListMetaVo;
    }

    /**
     * @return WalmartOrderVO[]
     */
    public function &getWalmartOrders()
    {
        return $this->walmartOrders;
    }
}
