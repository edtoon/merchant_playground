<?php

namespace BlakePrinting\WalmartMarketplace\Orders;

use BlakePrinting\Db\ValueObject;

class WalmartOrderShippingVO implements ValueObject {
    /** @var int */
    private $walmartOrderId = null;
    /** @var int */
    private $walmartAddressId = null;
    /** @var int */
    private $walmartShippingMethodId = null;
    /** @var int */
    private $walmartPhoneId = null;
    /** @var int */
    private $estimatedShipDate = null;
    /** @var int */
    private $estimatedDeliveryDate = null;

    /**
     * @return int
     */
    public function getWalmartOrderId()
    {
        return $this->walmartOrderId;
    }

    /**
     * @param int $walmartOrderId
     */
    public function setWalmartOrderId($walmartOrderId)
    {
        $this->walmartOrderId = $walmartOrderId;
    }

    /**
     * @return int
     */
    public function getWalmartAddressId()
    {
        return $this->walmartAddressId;
    }

    /**
     * @param int $walmartAddressId
     */
    public function setWalmartAddressId($walmartAddressId)
    {
        $this->walmartAddressId = $walmartAddressId;
    }

    /**
     * @return int
     */
    public function getWalmartShippingMethodId()
    {
        return $this->walmartShippingMethodId;
    }

    /**
     * @param int $walmartShippingMethodId
     */
    public function setWalmartShippingMethodId($walmartShippingMethodId)
    {
        $this->walmartShippingMethodId = $walmartShippingMethodId;
    }

    /**
     * @return int
     */
    public function getWalmartPhoneId()
    {
        return $this->walmartPhoneId;
    }

    /**
     * @param int $walmartPhoneId
     */
    public function setWalmartPhoneId($walmartPhoneId)
    {
        $this->walmartPhoneId = $walmartPhoneId;
    }

    /**
     * @return int
     */
    public function getEstimatedShipDate()
    {
        return $this->estimatedShipDate;
    }

    /**
     * @param int $estimatedShipDate
     */
    public function setEstimatedShipDate($estimatedShipDate)
    {
        $this->estimatedShipDate = $estimatedShipDate;
    }

    /**
     * @return int
     */
    public function getEstimatedDeliveryDate()
    {
        return $this->estimatedDeliveryDate;
    }

    /**
     * @param int $estimatedDeliveryDate
     */
    public function setEstimatedDeliveryDate($estimatedDeliveryDate)
    {
        $this->estimatedDeliveryDate = $estimatedDeliveryDate;
    }
}
