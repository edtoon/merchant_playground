<?php

namespace BlakePrinting\WalmartMarketplace\Orders;

use BlakePrinting\Db\ValueObject;

class WalmartOrderListVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $walmartMarketplaceCredentialId = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getWalmartMarketplaceCredentialId()
    {
        return $this->walmartMarketplaceCredentialId;
    }

    /**
     * @param int $walmartMarketplaceCredentialId
     */
    public function setWalmartMarketplaceCredentialId($walmartMarketplaceCredentialId)
    {
        $this->walmartMarketplaceCredentialId = $walmartMarketplaceCredentialId;
    }
}
