<?php

namespace BlakePrinting\WalmartMarketplace\Orders;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static WalmartOrderLineChargeDAO instance(PDO $connection = null, $schemaName = null)
 * @method WalmartOrderLineChargeVO findById($id, $options = null, $useCache = false)
 * @method WalmartOrderLineChargeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartOrderLineChargeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartOrderLineChargeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartOrderLineChargeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartOrderLineChargeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartOrderLineChargeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartOrderLineChargeVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class WalmartOrderLineChargeDAO extends AbstractDataAccessObject {
}
