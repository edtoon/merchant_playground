<?php

namespace BlakePrinting\WalmartMarketplace\Orders;

use BlakePrinting\Db\ValueObject;

class WalmartOrderLineVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $walmartOrderId = null;
    /** @var int */
    private $lineNumber = null;
    /** @var int */
    private $walmartItemId = null;
    /** @var int */
    private $walmartUnitOfMeasurementId = null;
    /** @var int */
    private $quantity = null;
    /** @var int */
    private $statusDate = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getWalmartOrderId()
    {
        return $this->walmartOrderId;
    }

    /**
     * @param int $walmartOrderId
     */
    public function setWalmartOrderId($walmartOrderId)
    {
        $this->walmartOrderId = $walmartOrderId;
    }

    /**
     * @return int
     */
    public function getLineNumber()
    {
        return $this->lineNumber;
    }

    /**
     * @param int $lineNumber
     */
    public function setLineNumber($lineNumber)
    {
        $this->lineNumber = $lineNumber;
    }

    /**
     * @return int
     */
    public function getWalmartItemId()
    {
        return $this->walmartItemId;
    }

    /**
     * @param int $walmartItemId
     */
    public function setWalmartItemId($walmartItemId)
    {
        $this->walmartItemId = $walmartItemId;
    }

    /**
     * @return int
     */
    public function getWalmartUnitOfMeasurementId()
    {
        return $this->walmartUnitOfMeasurementId;
    }

    /**
     * @param int $walmartUnitOfMeasurementId
     */
    public function setWalmartUnitOfMeasurementId($walmartUnitOfMeasurementId)
    {
        $this->walmartUnitOfMeasurementId = $walmartUnitOfMeasurementId;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return int
     */
    public function getStatusDate()
    {
        return $this->statusDate;
    }

    /**
     * @param int $statusDate
     */
    public function setStatusDate($statusDate)
    {
        $this->statusDate = $statusDate;
    }
}
