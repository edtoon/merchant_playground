<?php

namespace BlakePrinting\WalmartMarketplace\Orders;

use BlakePrinting\Db\ValueObject;

class WalmartOrderLineStatusVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $walmartOrderLineId = null;
    /** @var int */
    private $walmartOrderStatusId = null;
    /** @var int */
    private $walmartUnitOfMeasurementId = null;
    /** @var int */
    private $quantity = null;
    /** @var string */
    private $cancellationReason = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getWalmartOrderLineId()
    {
        return $this->walmartOrderLineId;
    }

    /**
     * @param int $walmartOrderLineId
     */
    public function setWalmartOrderLineId($walmartOrderLineId)
    {
        $this->walmartOrderLineId = $walmartOrderLineId;
    }

    /**
     * @return int
     */
    public function getWalmartOrderStatusId()
    {
        return $this->walmartOrderStatusId;
    }

    /**
     * @param int $walmartOrderStatusId
     */
    public function setWalmartOrderStatusId($walmartOrderStatusId)
    {
        $this->walmartOrderStatusId = $walmartOrderStatusId;
    }

    /**
     * @return int
     */
    public function getWalmartUnitOfMeasurementId()
    {
        return $this->walmartUnitOfMeasurementId;
    }

    /**
     * @param int $walmartUnitOfMeasurementId
     */
    public function setWalmartUnitOfMeasurementId($walmartUnitOfMeasurementId)
    {
        $this->walmartUnitOfMeasurementId = $walmartUnitOfMeasurementId;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return string
     */
    public function getCancellationReason()
    {
        return $this->cancellationReason;
    }

    /**
     * @param string $cancellationReason
     */
    public function setCancellationReason($cancellationReason)
    {
        $this->cancellationReason = $cancellationReason;
    }
}
