<?php

namespace BlakePrinting\WalmartMarketplace\Orders;

use BlakePrinting\Db\ValueObject;

class WalmartOrderLineRefundVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $walmartOrderLineId = null;
    /** @var int */
    private $walmartRefundReasonId = null;
    /** @var string */
    private $refundId = null;
    /** @var string */
    private $comments = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getWalmartOrderLineId()
    {
        return $this->walmartOrderLineId;
    }

    /**
     * @param int $walmartOrderLineId
     */
    public function setWalmartOrderLineId($walmartOrderLineId)
    {
        $this->walmartOrderLineId = $walmartOrderLineId;
    }

    /**
     * @return int
     */
    public function getWalmartRefundReasonId()
    {
        return $this->walmartRefundReasonId;
    }

    /**
     * @param int $walmartRefundReasonId
     */
    public function setWalmartRefundReasonId($walmartRefundReasonId)
    {
        $this->walmartRefundReasonId = $walmartRefundReasonId;
    }

    /**
     * @return string
     */
    public function getRefundId()
    {
        return $this->refundId;
    }

    /**
     * @param string $refundId
     */
    public function setRefundId($refundId)
    {
        $this->refundId = $refundId;
    }

    /**
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param string $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }
}
