<?php

namespace BlakePrinting\WalmartMarketplace\Orders;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static WalmartOrderStatusDAO instance(PDO $connection = null, $schemaName = null)
 * @method WalmartOrderLineStatusVO findById($id, $options = null, $useCache = false)
 * @method WalmartOrderLineStatusVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartOrderLineStatusVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartOrderLineStatusVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartOrderLineStatusVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartOrderLineStatusVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartOrderLineStatusVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartOrderLineStatusVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class WalmartOrderStatusDAO extends AbstractDataAccessObject {
    const WALMART_ORDER_STATUS_CREATED = "Created";
    const WALMART_ORDER_STATUS_ACKNOWLEDGED = "Acknowledged";
    const WALMART_ORDER_STATUS_SHIPPED = "Shipped";
    const WALMART_ORDER_STATUS_CANCELLED = "Cancelled";
    const WALMART_ORDER_STATUS_REFUND = "Refund";
}
