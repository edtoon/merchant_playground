<?php

namespace BlakePrinting\WalmartMarketplace\Orders;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static WalmartOrderLineChargeTaxDAO instance(PDO $connection = null, $schemaName = null)
 * @method WalmartOrderLineChargeTaxVO findById($id, $options = null, $useCache = false)
 * @method WalmartOrderLineChargeTaxVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartOrderLineChargeTaxVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartOrderLineChargeTaxVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartOrderLineChargeTaxVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartOrderLineChargeTaxVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartOrderLineChargeTaxVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartOrderLineChargeTaxVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class WalmartOrderLineChargeTaxDAO extends AbstractDataAccessObject {
}
