<?php

use BlakePrinting\Db\DbSeed;
use BlakePrinting\Util\Utils;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderStatusDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderStatusVO;

class WalmartOrderSchemaSeeder extends DbSeed {
    public function run() {
        $connection =& $this->getConnection();

        $orderStatusDao =& WalmartOrderStatusDAO::instance($connection);
        Utils::forEachConstantInObjectClass(
            $orderStatusDao,
            function($_, $constantName, $constantValue) use (&$orderStatusDao) {
                $orderStatusDao->findOrCreate(new WalmartOrderStatusVO($constantValue));
            }, "WALMART_ORDER_STATUS_"
        );
    }
}
