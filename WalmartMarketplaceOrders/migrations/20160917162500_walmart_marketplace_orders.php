<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineChargeDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineChargeTaxDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineRefundChargeDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineRefundChargeTaxDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineRefundDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineStatusDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderLineStatusTrackingDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderListDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderListMetaDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderListRequestDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderShippingDAO;
use BlakePrinting\WalmartMarketplace\Orders\WalmartOrderStatusDAO;
use BlakePrinting\WalmartMarketplace\WalmartAddressDAO;
use BlakePrinting\WalmartMarketplace\WalmartChargeNameDAO;
use BlakePrinting\WalmartMarketplace\WalmartChargeTypeDAO;
use BlakePrinting\WalmartMarketplace\WalmartEmailDAO;
use BlakePrinting\WalmartMarketplace\WalmartItemDAO;
use BlakePrinting\WalmartMarketplace\WalmartMarketplaceCredentialDAO;
use BlakePrinting\WalmartMarketplace\WalmartPhoneDAO;
use BlakePrinting\WalmartMarketplace\WalmartRefundReasonDAO;
use BlakePrinting\WalmartMarketplace\WalmartShippingCarrierDAO;
use BlakePrinting\WalmartMarketplace\WalmartShippingMethodDAO;
use BlakePrinting\WalmartMarketplace\WalmartTaxDAO;
use BlakePrinting\WalmartMarketplace\WalmartUnitOfMeasurementDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class WalmartMarketplaceOrders extends DbMigration {
    /**
     * @throws Exception
     */
    public function up() {
        $connection =& $this->getConnection();
        $credentialTbl = WalmartMarketplaceCredentialDAO::instance($connection)->formatSchemaAndTableName();

        $orderListDao =& WalmartOrderListDAO::instance($connection);
        $orderListTbl = $orderListDao->formatSchemaAndTableName();
        $this->tableDao($orderListDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("walmart_marketplace_credential_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addForeignKey("walmart_marketplace_credential_id", $credentialTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->save()
        ;

        $orderListRequestDao =& WalmartOrderListRequestDAO::instance($connection);
        $this->tableDao($orderListRequestDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("walmart_order_list_id", "biginteger", ["signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("uuid", "string", ["limit" => 191])
            ->addForeignKey("walmart_order_list_id", $orderListTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("uuid", ["unique" => true])
            ->save()
        ;

        $orderListMetaDao =& WalmartOrderListMetaDAO::instance($connection);
        $this->tableDao($orderListMetaDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["walmart_order_list_id"]])
            ->addColumn("walmart_order_list_id", "biginteger", ["signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["null" => true, "signed" => false])
            ->addColumn("total_count", "biginteger", ["signed" => false])
            ->addColumn("item_limit", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("next_cursor", "text", ["null" => true])
            ->addForeignKey("walmart_order_list_id", $orderListTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("total_count")
            ->save()
        ;

        $orderStatusDao =& WalmartOrderStatusDAO::instance($connection);
        $orderStatusTbl = WalmartOrderStatusDAO::instance($connection)->formatSchemaAndTableName();
        $this->tableDao($orderStatusDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("code", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("code", ["unique" => true])
            ->save()
        ;

        $orderDao =& WalmartOrderDAO::instance($connection);
        $orderTbl = $orderDao->formatSchemaAndTableName();
        $emailTbl = WalmartEmailDAO::instance($connection)->formatSchemaAndTableName();
        $this->tableDao($orderDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["null" => true, "signed" => false])
            ->addColumn("walmart_marketplace_credential_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("walmart_email_id", "integer", ["signed" => false])
            ->addColumn("purchase_order_id", "string", ["limit" => 191])
            ->addColumn("customer_order_id", "string", ["limit" => 191])
            ->addColumn("order_date", "biginteger", ["signed" => false])
            ->addForeignKey("walmart_marketplace_credential_id", $credentialTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("walmart_email_id", $emailTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex(["walmart_marketplace_credential_id", "purchase_order_id"], ["unique" => true])
            ->addIndex("purchase_order_id")
            ->addIndex("customer_order_id")
            ->addIndex("order_date")
            ->save()
        ;

        $orderShippingDao =& WalmartOrderShippingDAO::instance($connection);
        $walmartAddressTbl = WalmartAddressDAO::instance($connection)->formatSchemaAndTableName();
        $walmartShippingMethodTbl = WalmartShippingMethodDAO::instance($connection)->formatSchemaAndTableName();
        $walmartPhoneTbl = WalmartPhoneDAO::instance($connection)->formatSchemaAndTableName();
        $this->tableDao($orderShippingDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["walmart_order_id"]])
            ->addColumn("walmart_order_id", "biginteger", ["signed" => false])
            ->addColumn("walmart_address_id", "integer", ["signed" => false, "null" => true])
            ->addColumn("walmart_shipping_method_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY, "null" => true])
            ->addColumn("walmart_phone_id", "integer", ["signed" => false, "null" => true])
            ->addColumn("estimated_ship_date", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("estimated_delivery_date", "biginteger", ["signed" => false, "null" => true])
            ->addForeignKey("walmart_order_id", $orderTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("walmart_address_id", $walmartAddressTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("walmart_shipping_method_id", $walmartShippingMethodTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("walmart_phone_id", $walmartPhoneTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("estimated_ship_date")
            ->addIndex("estimated_delivery_date")
            ->save()
        ;

        $orderLineDao =& WalmartOrderLineDAO::instance($connection);
        $orderLineTbl = $orderLineDao->formatSchemaAndTableName();
        $walmartItemTbl = WalmartItemDAO::instance($connection)->formatSchemaAndTableName();
        $walmartUnitOfMeasurementTbl = WalmartUnitOfMeasurementDAO::instance($connection)->formatSchemaAndTableName();
        $this->tableDao($orderLineDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("line_number", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("walmart_order_id", "biginteger", ["signed" => false])
            ->addColumn("walmart_item_id", "integer", ["signed" => false, "null" => true])
            ->addColumn("walmart_unit_of_measurement_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY, "null" => true])
            ->addColumn("quantity", "integer", ["signed" => false, "null" => true])
            ->addColumn("status_date", "biginteger", ["signed" => false, "null" => true])
            ->addForeignKey("walmart_order_id", $orderTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("walmart_item_id", $walmartItemTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("walmart_unit_of_measurement_id", $walmartUnitOfMeasurementTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex(["walmart_order_id", "line_number"], ["unique" => true])
            ->addIndex("quantity")
            ->addIndex("status_date")
            ->save()
        ;

        $orderLineChargeDao =& WalmartOrderLineChargeDAO::instance($connection);
        $orderLineChargeTbl = $orderLineChargeDao->formatSchemaAndTableName();
        $walmartChargeTypeTbl = WalmartChargeTypeDAO::instance($connection)->formatSchemaAndTableName();
        $walmartChargeNameTbl = WalmartChargeNameDAO::instance($connection)->formatSchemaAndTableName();
        $this->tableDao($orderLineChargeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("walmart_order_line_id", "biginteger", ["signed" => false])
            ->addColumn("walmart_charge_type_id", "integer", ["signed" => false, "null" => true, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("walmart_charge_name_id", "integer", ["signed" => false, "null" => true, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("currency", "string", ["limit" => 191, "null" => true])
            ->addColumn("amount", "decimal", ["null" => true, "precision" => 11, "scale" => 4])
            ->addForeignKey("walmart_order_line_id", $orderLineTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("walmart_charge_type_id", $walmartChargeTypeTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("walmart_charge_name_id", $walmartChargeNameTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("currency")
            ->addIndex("amount")
            ->save()
        ;

        $orderLineChargeTaxDao =& WalmartOrderLineChargeTaxDAO::instance($connection);
        $walmartTaxTbl = WalmartTaxDAO::instance($connection)->formatSchemaAndTableName();
        $this->tableDao($orderLineChargeTaxDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("walmart_order_line_charge_id", "biginteger", ["signed" => false])
            ->addColumn("walmart_tax_id", "integer", ["signed" => false, "null" => true, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("currency", "string", ["limit" => 191, "null" => true])
            ->addColumn("amount", "decimal", ["null" => true, "precision" => 11, "scale" => 4])
            ->addForeignKey("walmart_order_line_charge_id", $orderLineChargeTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("walmart_tax_id", $walmartTaxTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("currency")
            ->addIndex("amount")
            ->save()
        ;

        $orderLineStatusDao =& WalmartOrderLineStatusDAO::instance($connection);
        $orderLineStatusTbl = $orderLineStatusDao->formatSchemaAndTableName();
        $this->tableDao($orderLineStatusDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("walmart_order_line_id", "biginteger", ["signed" => false])
            ->addColumn("walmart_order_status_id", "integer", ["signed" => false, "null" => true, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("walmart_unit_of_measurement_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY, "null" => true])
            ->addColumn("quantity", "integer", ["signed" => false, "null" => true])
            ->addColumn("cancellation_reason", "text", ["null" => true])
            ->addForeignKey("walmart_order_line_id", $orderLineTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("walmart_order_status_id", $orderStatusTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("walmart_unit_of_measurement_id", $walmartUnitOfMeasurementTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("quantity")
            ->save()
        ;
        $this->alterDao($orderLineStatusDao, "ADD INDEX (cancellation_reason(191))");

        $orderLineStatusTrackingDao =& WalmartOrderLineStatusTrackingDAO::instance($connection);
        $walmartShippingCarrierTbl = WalmartShippingCarrierDAO::instance($connection)->formatSchemaAndTableName();
        $walmartShippingMethodTbl = WalmartShippingMethodDAO::instance($connection)->formatSchemaAndTableName();
        $this->tableDao($orderLineStatusTrackingDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("walmart_order_line_status_id", "biginteger", ["signed" => false])
            ->addColumn("walmart_shipping_carrier_id", "integer", ["signed" => false, "null" => true, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("walmart_shipping_method_id", "integer", ["signed" => false, "null" => true, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("shipped", "biginteger", ["null" => true, "signed" => false])
            ->addColumn("tracking_number", "text", ["null" => true])
            ->addForeignKey("walmart_order_line_status_id", $orderLineStatusTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("walmart_shipping_carrier_id", $walmartShippingCarrierTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("walmart_shipping_method_id", $walmartShippingMethodTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("shipped")
            ->save()
        ;
        $this->alterDao($orderLineStatusTrackingDao, "ADD INDEX (tracking_number(191))");

        $orderLineRefundDao =& WalmartOrderLineRefundDAO::instance($connection);
        $orderLineRefundTbl = $orderLineRefundDao->formatSchemaAndTableName();
        $walmartRefundReasonTbl = WalmartRefundReasonDAO::instance($connection)->formatSchemaAndTableName();
        $this->tableDao($orderLineRefundDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("walmart_order_line_id", "biginteger", ["signed" => false])
            ->addColumn("walmart_refund_reason_id", "integer", ["signed" => false, "null" => true, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("refund_id", "string", ["limit" => 191, "null" => true])
            ->addColumn("comments", "text", ["null" => true])
            ->addForeignKey("walmart_order_line_id", $orderLineTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("walmart_refund_reason_id", $walmartRefundReasonTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("refund_id")
            ->save()
        ;
        $this->alterDao($orderLineRefundDao, "ADD INDEX (comments(191))");

        $orderLineRefundChargeDao =& WalmartOrderLineRefundChargeDAO::instance($connection);
        $orderLineRefundChargeTbl = $orderLineRefundChargeDao->formatSchemaAndTableName();
        $this->tableDao($orderLineRefundChargeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("walmart_order_line_refund_id", "biginteger", ["signed" => false])
            ->addColumn("walmart_charge_type_id", "integer", ["signed" => false, "null" => true, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("walmart_charge_name_id", "integer", ["signed" => false, "null" => true, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("currency", "string", ["limit" => 191, "null" => true])
            ->addColumn("amount", "decimal", ["null" => true, "precision" => 11, "scale" => 4])
            ->addForeignKey("walmart_order_line_refund_id", $orderLineRefundTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("walmart_charge_type_id", $walmartChargeTypeTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("walmart_charge_name_id", $walmartChargeNameTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("currency")
            ->addIndex("amount")
            ->save()
        ;

        $orderLineRefundChargeTaxDao =& WalmartOrderLineRefundChargeTaxDAO::instance($connection);
        $this->tableDao($orderLineRefundChargeTaxDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("walmart_order_line_refund_charge_id", "biginteger", ["signed" => false])
            ->addColumn("walmart_tax_id", "integer", ["signed" => false, "null" => true, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("currency", "string", ["limit" => 191, "null" => true])
            ->addColumn("amount", "decimal", ["null" => true, "precision" => 11, "scale" => 4])
            ->addForeignKey("walmart_order_line_refund_charge_id", $orderLineRefundChargeTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("walmart_tax_id", $walmartTaxTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("currency")
            ->addIndex("amount")
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(WalmartOrderLineRefundChargeTaxDAO::instance($connection));
        $this->dropDao(WalmartOrderLineRefundChargeDAO::instance($connection));
        $this->dropDao(WalmartOrderLineRefundDAO::instance($connection));
        $this->dropDao(WalmartOrderLineStatusTrackingDAO::instance($connection));
        $this->dropDao(WalmartOrderLineStatusDAO::instance($connection));
        $this->dropDao(WalmartOrderLineChargeTaxDAO::instance($connection));
        $this->dropDao(WalmartOrderLineChargeDAO::instance($connection));
        $this->dropDao(WalmartOrderLineDAO::instance($connection));
        $this->dropDao(WalmartOrderShippingDAO::instance($connection));
        $this->dropDao(WalmartOrderDAO::instance($connection));
        $this->dropDao(WalmartOrderStatusDAO::instance($connection));
        $this->dropDao(WalmartOrderListMetaDAO::instance($connection));
        $this->dropDao(WalmartOrderListRequestDAO::instance($connection));
        $this->dropDao(WalmartOrderListDAO::instance($connection));
    }
}
