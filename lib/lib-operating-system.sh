#!/bin/bash

if [ -z "${LIB_DIR}" ];
then
  SOURCE="${BASH_SOURCE[0]}"
  while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
  LIB_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"
fi

. "${LIB_DIR}/lib-command.sh"

function operating_system_detect {
  local os="$(2>/dev/null uname -o)"

  os="${os:=${OPERATING_SYSTEM_KERNEL}}"

  if [ "GNU/Linux" = "${os}" ];
  then
    local dist=""

    if command_exists lsb_release;
    then
      dist="$(lsb_release -si)"
    fi

    if [ -z "${dist}" ] && [ -r /etc/lsb-release ];
    then
      dist="$(. /etc/lsb-release && echo "${DISTRIB_ID}")"
    fi

    if [ -z "${dist}" ] && [ -r /etc/debian_version ];
    then
      dist="debian"
    fi

    if [ -z "${dist}" ] && [ -r /etc/fedora-release ];
    then
      dist="fedora"
    fi

    if [ -z "${dist}" ] && [ -r /etc/os-release ];
    then
      dist="$(. /etc/os-release && echo "${ID}")"
    fi

   if [ ! -z "${dist}" ];
   then
     os="${dist}"
   fi
  fi

  echo "${os}" | tr '[:upper:]' '[:lower:]'
}

if [ -z "${_LIB_OPERATING_SYSTEM_}" ];
then
  _LIB_OPERATING_SYSTEM_=1

  export OPERATING_SYSTEM_KERNEL="$(uname -s)"
  export OPERATING_SYSTEM="$(operating_system_detect)"
fi
