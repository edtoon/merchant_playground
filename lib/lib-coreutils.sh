#!/bin/bash

if [ -z "${LIB_DIR}" ];
then
  SOURCE="${BASH_SOURCE[0]}"
  while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
  LIB_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"
fi

. "${LIB_DIR}/lib-brew.sh"
. "${LIB_DIR}/lib-command.sh"
. "${LIB_DIR}/lib-operating-system.sh"

function readlink_exec {
  if [ "${OPERATING_SYSTEM}" = "darwin" ];
  then
    command_exists greadlink || brew install coreutils
    greadlink $*
  else
    readlink $*
  fi
}
