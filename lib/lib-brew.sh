#!/bin/bash

if [ -z "${LIB_DIR}" ];
then
  SOURCE="${BASH_SOURCE[0]}"
  while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
  LIB_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"
fi

. "${LIB_DIR}/lib-operating-system.sh"

if [ -z "${_LIB_BREW_}" ];
then
  _LIB_BREW_=1

  if [ "${OPERATING_SYSTEM}" = "darwin" ];
  then
    BREW_HOME="${BREW_HOME:=/usr/local}"
    BREW_EXE="${BREW_EXE:=${BREW_HOME}/bin/brew}"

    [ -x "${BREW_EXE}" ] || {
      /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    }

    export BREW_HOME BREW_EXE
  fi
fi
