#!/bin/bash
# https://github.com/0k/shyaml

if [ -z "${LIB_DIR}" ];
then
  SOURCE="${BASH_SOURCE[0]}"
  while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
  LIB_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"
fi

. "${LIB_DIR}/lib-pip.sh"

if [ -z "${_LIB_YAML_}" ];
then
  _LIB_YAML_=1

  2>/dev/null python -c "import yaml" || pip_install pyyaml
  command_exists shyaml || pip_install shyaml
fi

function yaml_get {
  file="$1"

  shift
  cat "${file}" | shyaml get-value $*
}

function yaml_type {
  file="$1"

  shift
  cat "${file}" | shyaml get-type $*
}
