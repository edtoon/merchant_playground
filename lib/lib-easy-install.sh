#!/bin/bash

if [ -z "${LIB_DIR}" ];
then
  SOURCE="${BASH_SOURCE[0]}"
  while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
  LIB_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"
fi

. "${LIB_DIR}/lib-command.sh"
. "${LIB_DIR}/lib-python.sh"

function easy_install_exec {
  if command_exists easy_install;
  then
    command_sudo_or_reg easy_install $@
  elif command_exists easy_install-2.7;
  then
    command_sudo_or_reg easy_install-2.7 $@
  elif command_exists easy_install-2.7;
  then
    command_sudo_or_reg easy_install-2.7 $@
  elif command_exists python3;
  then
    if command_exists python3.5;
    then
      command_exists easy_install || apt_install python-setuptools
      command_sudo_or_reg easy_install $@
    elif command_exists easy_install-3.4;
    then
      command_sudo_or_reg easy_install-3.4 $@
    fi
  else
    >&2 echo "Could not find easy_install to install $@"
    exit 1
  fi
}
