#!/bin/bash

if [ -z "${LIB_DIR}" ];
then
  SOURCE="${BASH_SOURCE[0]}"
  while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
  LIB_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"
fi

. "${LIB_DIR}/lib-command.sh"
. "${LIB_DIR}/lib-easy-install.sh"

if [ -z "${_LIB_PIP_}" ];
then
  _LIB_PIP_=1

  command_exists pip || easy_install_exec pip
fi

function pip_install {
  command_sudo_or_reg pip install $*
}
