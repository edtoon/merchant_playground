#!/bin/bash

if [ -z "${LIB_DIR}" ];
then
  SOURCE="${BASH_SOURCE[0]}"
  while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
  LIB_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"
fi

. "${LIB_DIR}/lib-command.sh"
. "${LIB_DIR}/lib-operating-system.sh"

if [ -z "${_LIB_DOCKER_}" ];
then
  _LIB_DOCKER_=1

  HAS_DOCKER="true"

  if ! command_exists docker;
  then
    HAS_DOCKER="false"
  elif [ "${OPERATING_SYSTEM_KERNEL}" != "Linux" ];
  then
    if command_exists docker-machine;
    then
      machine_state="$(docker-machine ls --filter name=default --format "{{.State}}")"

      if [ "${machine_state}" = "Stopped" ];
      then
        docker-machine start default
      elif [ "${machine_state}" != "Running" ];
      then
        echo "Unrecognized state for default Docker machine: ${machine_state}"
        exit 1
      fi

      eval "$(docker-machine env default)"
    else
      HAS_DOCKER="false"
    fi
  fi

  export HAS_DOCKER
fi

function docker_exec {
  if [ "${OPERATING_SYSTEM_KERNEL}" = "Linux" ];
  then
    command_sudo_or_reg docker $*
  else
    docker $*
  fi
}
