#!/bin/bash

if [ -z "${LIB_DIR}" ];
then
  SOURCE="${BASH_SOURCE[0]}"
  while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
  LIB_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"
fi

. "${LIB_DIR}/lib-command.sh"
. "${LIB_DIR}/lib-docker.sh"

if [ -z "${_LIB_BLAKE_ENVIRONMENT_}" ];
then
  _LIB_BLAKE_ENVIRONMENT_=1

  BLAKE_ENVIRONMENT="dev"
  BLAKE_APP_DIR="${LIB_DIR}/../"

  if [ "${HAS_DOCKER}" = "true" ];
  then
    BLAKE_APP_DIR="/app"

    if command_exists ec2metadata;
    then
      if [ "$(ec2metadata --instance-type)" = "m4.xlarge" ];
      then
        BLAKE_ENVIRONMENT="prod"
      fi
    fi
  fi

  export BLAKE_APP_DIR BLAKE_ENVIRONMENT
fi
