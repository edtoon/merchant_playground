#!/bin/bash

if [ -z "${LIB_DIR}" ];
then
  SOURCE="${BASH_SOURCE[0]}"
  while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
  LIB_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"
fi

. "${LIB_DIR}/lib-pip.sh"

if [ -z "${_LIB_AWS_}" ];
then
  _LIB_AWS_=1

  command_exists aws || pip install awscli
fi
