#!/bin/bash

if [ -z "${LIB_DIR}" ];
then
  SOURCE="${BASH_SOURCE[0]}"
  while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
  LIB_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"
fi

. "${LIB_DIR}/lib-blake-environment.sh"
. "${LIB_DIR}/lib-yaml.sh"

if [ -z "${_LIB_CONFIG_}" ];
then
  _LIB_CONFIG_=1

  export CONFIG_YML_ENV="${LIB_DIR}/../config.${BLAKE_ENVIRONMENT}.yml"
  export CONFIG_YML_BASE="${LIB_DIR}/../config.yml"
fi

function config_get {
  env_value="$(2>/dev/null yaml_get "${CONFIG_YML_ENV}" $*)"

  if [ "$?" = "0" ];
  then
    echo "${env_value}"
  else
    2>/dev/null yaml_get "${CONFIG_YML_BASE}" $*
  fi
}

function config_type {
  env_value="$(2>/dev/null yaml_type "${CONFIG_YML_ENV}" $*)"

  if [ "$?" = "0" ];
  then
    echo "${env_value}"
  else
    2>/dev/null yaml_type "${CONFIG_YML_BASE}" $*
  fi
}
