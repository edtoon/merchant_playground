#!/bin/bash

if [ -z "${LIB_DIR}" ];
then
  SOURCE="${BASH_SOURCE[0]}"
  while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
  LIB_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"
fi

. "${LIB_DIR}/lib-command.sh"

function apt_install {
  if command_exists apt-get;
  then
    command_sudo_or_reg apt-get install -y $@
  elif command_exists apt-cyg;
  then
    apt-cyg install $@
  else
    >&2 echo "Could not find apt to install $@"
    exit 1
  fi
}
