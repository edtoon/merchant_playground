#!/bin/bash

if [ -z "${LIB_DIR}" ];
then
  SOURCE="${BASH_SOURCE[0]}"
  while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
  LIB_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"
fi

. "${LIB_DIR}/lib-blake-environment.sh"
. "${LIB_DIR}/lib-command.sh"
. "${LIB_DIR}/lib-docker.sh"

if [ -z "${_LIB_COMPOSER_}" ];
then
  _LIB_COMPOSER_=1

  if [ "${HAS_DOCKER}" != "true" ];
  then
    command_exists composer || {
      curl -sS https://getcomposer.org/installer | command_sudo_or_reg php -- \
        --install-dir=/usr/local/bin --filename=composer
    }
  fi
fi

function composer_exec {
  local cmd="${1}"

  shift

  "${LIB_DIR}/../bin/run_php.sh" -c composer "${cmd}" --working-dir="${BLAKE_APP_DIR}" "$@"

  if [ "${HAS_DOCKER}" = "true" ];
  then
    composer_chown
  fi
}

function composer_chown {
    command_sudo_or_reg chown -R "$(id -un):$(id -gn)" "${LIB_DIR}/../vendor"
}
