<?php

use BlakePrinting\Db\DbSeed;
use BlakePrinting\Sales\FulfillmentStatusDAO;
use BlakePrinting\Sales\FulfillmentStatusVO;
use BlakePrinting\Sales\PaymentMethodDAO;
use BlakePrinting\Sales\PaymentMethodVO;
use BlakePrinting\Sales\PaymentStatusDAO;
use BlakePrinting\Sales\PaymentStatusVO;
use BlakePrinting\Sales\SalesChannelDAO;
use BlakePrinting\Sales\SalesChannelVO;
use BlakePrinting\Util\Utils;

class SalesSeeder extends DbSeed {
    public final function run() {
        $connection =& $this->getConnection();
        $paymentMethodDao =& PaymentMethodDAO::instance($connection);
        $paymentStatusDao =& PaymentStatusDAO::instance($connection);
        $fulfillmentStatusDao =& FulfillmentStatusDAO::instance($connection);
        $salesChannelDao =& SalesChannelDAO::instance($connection);

        Utils::forEachConstantInObjectClass(
            $paymentMethodDao,
            function($_, $constantName, $constantValue) use (&$paymentMethodDao) {
                $paymentMethodDao->findOrCreate(new PaymentMethodVO($constantValue));
            },
            "PAYMENT_METHOD_"
        );

        Utils::forEachConstantInObjectClass(
            $paymentStatusDao,
            function($_, $constantName, $constantValue) use (&$paymentStatusDao) {
                $paymentStatusDao->findOrCreate(new PaymentStatusVO($constantValue));
            },
            "PAYMENT_STATUS_"
        );

        Utils::forEachConstantInObjectClass(
            $fulfillmentStatusDao,
            function($_, $constantName, $constantValue) use (&$fulfillmentStatusDao) {
                $fulfillmentStatusDao->findOrCreate(new FulfillmentStatusVO($constantValue));
            },
            "FULFILLMENT_STATUS_"
        );

        Utils::forEachConstantInObjectClass(
            $salesChannelDao,
            function($_, $constantName, $constantValue) use (&$salesChannelDao) {
                $salesChannelDao->findOrCreate(new SalesChannelVO($constantValue));
            },
            "SALES_CHANNEL_"
        );
    }
}
