<?php

use BlakePrinting\Catalog\CatalogProductDAO;
use BlakePrinting\Contacts\ContactAddressDAO;
use BlakePrinting\Contacts\ContactPersonDAO;
use BlakePrinting\DataSources\DataFeedDAO;
use BlakePrinting\Db\DbMigration;
use BlakePrinting\Fields\FieldDAO;
use BlakePrinting\Globalization\CurrencyDAO;
use BlakePrinting\Organizations\OrganizationDAO;
use BlakePrinting\Sales\FulfillmentStatusDAO;
use BlakePrinting\Sales\Orders\SalesOrderCatalogProductDAO;
use BlakePrinting\Sales\Orders\SalesOrderShipmentDAO;
use BlakePrinting\Sales\PaymentMethodDAO;
use BlakePrinting\Sales\PaymentStatusDAO;
use BlakePrinting\Sales\SalesChannelDAO;
use BlakePrinting\Sales\Orders\SalesOrderContactAddressDAO;
use BlakePrinting\Sales\Orders\SalesOrderContactPersonDAO;
use BlakePrinting\Sales\Orders\SalesOrderDAO;
use BlakePrinting\Sales\Orders\SalesOrderDataFeedDAO;
use BlakePrinting\Sales\Orders\SalesOrderFieldSignedDAO;
use BlakePrinting\Sales\Orders\SalesOrderFieldStringDAO;
use BlakePrinting\Sales\Orders\SalesOrderFieldTextDAO;
use BlakePrinting\Sales\Orders\SalesOrderFieldUnsignedDAO;
use BlakePrinting\Sales\Orders\SalesOrderTaxDAO;
use BlakePrinting\Shipping\ShipmentDAO;
use BlakePrinting\Taxes\TaxIdentifierTypeDAO;
use BlakePrinting\Taxes\TaxRateDAO;
use BlakePrinting\Users\UserDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class Sales extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();
        $userDao =& UserDAO::instance($connection);
        $userTbl = $userDao->formatSchemaAndTableName();
        $fieldDao =& FieldDAO::instance($connection);
        $fieldTbl = $fieldDao->formatSchemaAndTableName();

        $salesChannelDao =& SalesChannelDAO::instance($connection);
        $this->tableDao($salesChannelDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("name", "string", ["limit" => 191, "null" => true])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $paymentMethodDao =& PaymentMethodDAO::instance($connection);
        $this->tableDao($paymentMethodDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("name", "string", ["limit" => 191, "null" => true])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $paymentStatusDao =& PaymentStatusDAO::instance($connection);
        $this->tableDao($paymentStatusDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("name", "string", ["limit" => 191, "null" => true])
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $fulfillmentStatusDao =& FulfillmentStatusDAO::instance($connection);
        $this->tableDao($fulfillmentStatusDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("name", "string", ["limit" => 191, "null" => true])
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $salesOrderDao =& SalesOrderDAO::instance($connection);
        $salesOrderTbl = $salesOrderDao->formatSchemaAndTableName();
        $organizationDao =& OrganizationDAO::instance($connection);
        $organizationTbl = $organizationDao->formatSchemaAndTableName();
        $currencyDao =& CurrencyDAO::instance($connection);
        $currencyTbl = $currencyDao->formatSchemaAndTableName();
        $this->tableDao($salesOrderDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("seller_organization_id", "integer", ["signed" => false])
            ->addColumn("purchaser_organization_id", "integer", ["null" => true, "signed" => false])
            ->addColumn("sales_channel_id", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addColumn("title", "string", ["null" => true, "limit" => 191])
            ->addColumn("ordered", "biginteger", ["signed" => false])
            ->addColumn("purchaser_currency_id", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("sale_currency_id", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("currency_exchange_rate", "decimal", ["null" => true, "precision" => 16, "scale" => 8])
            ->addColumn("payment_method_id", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addColumn("shipping_amount", "decimal", ["null" => true, "precision" => 19, "scale" => 4])
            ->addColumn("subtotal_amount", "decimal", ["null" => true, "precision" => 19, "scale" => 4])
            ->addColumn("total_amount", "decimal", ["null" => true, "precision" => 19, "scale" => 4])
            ->addColumn("purchase_order_number", "string", ["null" => true, "limit" => 191])
            ->addColumn("fulfillment_status_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("seller_organization_id", $organizationTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("purchaser_organization_id", $organizationTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("sales_channel_id", $salesChannelDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("purchaser_currency_id", $currencyTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("sale_currency_id", $currencyTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("payment_method_id", $paymentMethodDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("fulfillment_status_id", $fulfillmentStatusDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("title")
            ->addIndex("ordered")
            ->addIndex("currency_exchange_rate")
            ->addIndex("shipping_amount")
            ->addIndex("subtotal_amount")
            ->addIndex("total_amount")
            ->addIndex("purchase_order_number")
            ->save()
        ;

        $salesOrderFieldStringDao =& SalesOrderFieldStringDAO::instance($connection);
        $this->tableDao($salesOrderFieldStringDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("sales_order_id", "biginteger", ["signed" => false])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "string", ["null" => true, "limit" => 191])
            ->addForeignKey("sales_order_id", $salesOrderTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $salesOrderFieldTextDao =& SalesOrderFieldTextDAO::instance($connection);
        $this->tableDao($salesOrderFieldTextDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("sales_order_id", "biginteger", ["signed" => false])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "text", ["null" => true])
            ->addForeignKey("sales_order_id", $salesOrderTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $salesOrderFieldUnsignedDao =& SalesOrderFieldUnsignedDAO::instance($connection);
        $this->tableDao($salesOrderFieldUnsignedDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("sales_order_id", "biginteger", ["signed" => false])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "biginteger", ["null" => true, "signed" => false])
            ->addForeignKey("sales_order_id", $salesOrderTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $salesOrderFieldSignedDao =& SalesOrderFieldSignedDAO::instance($connection);
        $this->tableDao($salesOrderFieldSignedDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("sales_order_id", "biginteger", ["signed" => false])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "biginteger", ["null" => true])
            ->addForeignKey("sales_order_id", $salesOrderTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $salesOrderContactPersonDao =& SalesOrderContactPersonDAO::instance($connection);
        $contactPersonDao =& ContactPersonDAO::instance($connection);
        $this->tableDao($salesOrderContactPersonDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["sales_order_id", "contact_person_id"]])
            ->addColumn("sales_order_id", "biginteger", ["signed" => false])
            ->addColumn("contact_person_id", "integer", ["signed" => false])
            ->addForeignKey("sales_order_id", $salesOrderTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("contact_person_id", $contactPersonDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $salesOrderContactAddressDao =& SalesOrderContactAddressDAO::instance($connection);
        $contactAddressDao =& ContactAddressDAO::instance($connection);
        $this->tableDao($salesOrderContactAddressDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["sales_order_id", "contact_address_id"]])
            ->addColumn("sales_order_id", "biginteger", ["signed" => false])
            ->addColumn("contact_address_id", "integer", ["signed" => false])
            ->addForeignKey("sales_order_id", $salesOrderTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("contact_address_id", $contactAddressDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $salesOrderTaxDao =& SalesOrderTaxDAO::instance($connection);
        $taxRateDao =& TaxRateDAO::instance($connection);
        $taxIdentifierTypeDao =& TaxIdentifierTypeDAO::instance($connection);
        $this->tableDao($salesOrderTaxDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("sales_order_id", "biginteger", ["signed" => false])
            ->addColumn("tax_rate_id", "integer", ["null" => true, "signed" => false])
            ->addColumn("tax_identifier_type_id", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("tax_identifier", "string", ["null" => true, "limit" => 191])
            ->addColumn("variable_percentage", "decimal", ["null" => true, "precision" => 10, "scale" => 8])
            ->addColumn("tax_amount", "decimal", ["null" => true, "precision" => 19, "scale" => 4])
            ->addForeignKey("sales_order_id", $salesOrderTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("tax_rate_id", $taxRateDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("tax_identifier_type_id", $taxIdentifierTypeDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("tax_identifier")
            ->addIndex("variable_percentage")
            ->addIndex("tax_amount")
            ->save()
        ;

        $salesOrderShipmentDao =& SalesOrderShipmentDAO::instance($connection);
        $shipmentDao =& ShipmentDAO::instance($connection);
        $this->tableDao($salesOrderShipmentDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["sales_order_id", "shipment_id"]])
            ->addColumn("sales_order_id", "biginteger", ["signed" => false])
            ->addColumn("shipment_id", "biginteger", ["signed" => false])
            ->addForeignKey("sales_order_id", $salesOrderTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("shipment_id", $shipmentDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("shipment_id", ["unique" => true])
            ->save()
        ;

        $salesOrderCatalogProductDao =& SalesOrderCatalogProductDAO::instance($connection);
        $catalogProductDao =& CatalogProductDAO::instance($connection);
        $this->tableDao($salesOrderCatalogProductDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["sales_order_id", "catalog_product_id"]])
            ->addColumn("sales_order_id", "biginteger", ["signed" => false])
            ->addColumn("catalog_product_id", "biginteger", ["signed" => false])
            ->addColumn("quantity", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addForeignKey("sales_order_id", $salesOrderTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("catalog_product_id", $catalogProductDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $salesOrderDataFeedDao =& SalesOrderDataFeedDAO::instance($connection);
        $dataFeedDao =& DataFeedDAO::instance($connection);
        $this->tableDao($salesOrderDataFeedDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["sales_order_id", "data_feed_id"]])
            ->addColumn("sales_order_id", "biginteger", ["signed" => false])
            ->addColumn("data_feed_id", "integer", ["signed" => false])
            ->addForeignKey("sales_order_id", $salesOrderTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("data_feed_id", $dataFeedDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(SalesOrderDataFeedDAO::instance($connection));
        $this->dropDao(SalesOrderCatalogProductDAO::instance($connection));
        $this->dropDao(SalesOrderShipmentDAO::instance($connection));
        $this->dropDao(SalesOrderTaxDAO::instance($connection));
        $this->dropDao(SalesOrderContactAddressDAO::instance($connection));
        $this->dropDao(SalesOrderContactPersonDAO::instance($connection));
        $this->dropDao(SalesOrderFieldSignedDAO::instance($connection));
        $this->dropDao(SalesOrderFieldUnsignedDAO::instance($connection));
        $this->dropDao(SalesOrderFieldTextDAO::instance($connection));
        $this->dropDao(SalesOrderFieldStringDAO::instance($connection));
        $this->dropDao(SalesOrderDAO::instance($connection));
        $this->dropDao(FulfillmentStatusDAO::instance($connection));
        $this->dropDao(PaymentStatusDAO::instance($connection));
        $this->dropDao(PaymentMethodDAO::instance($connection));
        $this->dropDao(SalesChannelDAO::instance($connection));
    }
}
