<?php

namespace BlakePrinting\Sales\Orders;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static SalesOrderContactPersonDAO instance(PDO $connection = null, $schemaName = null)
 * @method SalesOrderContactPersonVO findById($id, $options = null, $useCache = false)
 * @method SalesOrderContactPersonVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesOrderContactPersonVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesOrderContactPersonVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method SalesOrderContactPersonVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesOrderContactPersonVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesOrderContactPersonVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class SalesOrderContactPersonDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
