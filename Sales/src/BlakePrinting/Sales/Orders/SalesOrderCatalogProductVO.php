<?php

namespace BlakePrinting\Sales\Orders;

use BlakePrinting\Catalog\AbstractEntityCatalogProductVO;

class SalesOrderCatalogProductVO extends AbstractEntityCatalogProductVO {
    /** @var int */
    private $salesOrderId = null;

    public function __construct($salesOrderId = null, $catalogProductId = null, $quantity = null) {
        parent::__construct($catalogProductId, $quantity);

        $this->salesOrderId = $salesOrderId;
    }

    /**
     * @return int
     */
    public function getSalesOrderId()
    {
        return $this->salesOrderId;
    }

    /**
     * @param int $salesOrderId
     */
    public function setSalesOrderId($salesOrderId)
    {
        $this->salesOrderId = $salesOrderId;
    }
}
