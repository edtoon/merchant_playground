<?php

namespace BlakePrinting\Sales\Orders;

use BlakePrinting\Shipping\AbstractEntityShipmentVO;

class SalesOrderShipmentVO extends AbstractEntityShipmentVO {
    /** @var int */
    private $salesOrderId = null;

    public function __construct($salesOrderId = null, $shipmentId = null) {
        parent::__construct($shipmentId);

        $this->salesOrderId = $salesOrderId;
    }

    /**
     * @return int
     */
    public function getSalesOrderId()
    {
        return $this->salesOrderId;
    }

    /**
     * @param int $salesOrderId
     */
    public function setSalesOrderId($salesOrderId)
    {
        $this->salesOrderId = $salesOrderId;
    }
}
