<?php

namespace BlakePrinting\Sales\Orders;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static SalesOrderDAO instance(PDO $connection = null, $schemaName = null)
 * @method SalesOrderVO findById($id, $options = null, $useCache = false)
 * @method SalesOrderVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesOrderVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesOrderVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method SalesOrderVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesOrderVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesOrderVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method SalesOrderVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class SalesOrderDAO extends AbstractDataAccessObject {
}
