<?php

namespace BlakePrinting\Sales\Orders;

use BlakePrinting\Catalog\AbstractEntityCatalogProductDAO;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static SalesOrderCatalogProductDAO instance(PDO $connection = null, $schemaName = null)
 * @method SalesOrderCatalogProductVO findById($id, $options = null, $useCache = false)
 * @method SalesOrderCatalogProductVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesOrderCatalogProductVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesOrderCatalogProductVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method SalesOrderCatalogProductVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesOrderCatalogProductVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesOrderCatalogProductVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method void store(SalesOrderCatalogProductVO $vo)
 */
class SalesOrderCatalogProductDAO extends AbstractEntityCatalogProductDAO {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, "sales_order_id");
    }
}
