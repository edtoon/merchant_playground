<?php

namespace BlakePrinting\Sales\Orders;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static SalesOrderFieldSignedDAO instance(PDO $connection = null, $schemaName = null)
 * @method SalesOrderFieldSignedVO findById($id, $options = null, $useCache = false)
 * @method SalesOrderFieldSignedVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesOrderFieldSignedVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesOrderFieldSignedVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method SalesOrderFieldSignedVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesOrderFieldSignedVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesOrderFieldSignedVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class SalesOrderFieldSignedDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
