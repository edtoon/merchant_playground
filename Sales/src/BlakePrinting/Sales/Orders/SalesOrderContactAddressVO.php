<?php

namespace BlakePrinting\Sales\Orders;

use BlakePrinting\Contacts\AbstractEntityContactAddressVO;

class SalesOrderContactAddressVO extends AbstractEntityContactAddressVO {
    /** @var int */
    private $salesOrderId = null;

    public function __construct($salesOrderId = null, $contactAddressId = null) {
        parent::__construct($contactAddressId);

        $this->salesOrderId = $salesOrderId;
    }

    /**
     * @return int
     */
    public function getSalesOrderId()
    {
        return $this->salesOrderId;
    }

    /**
     * @param int $salesOrderId
     */
    public function setSalesOrderId($salesOrderId)
    {
        $this->salesOrderId = $salesOrderId;
    }
}
