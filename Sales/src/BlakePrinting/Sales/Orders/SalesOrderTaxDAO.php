<?php

namespace BlakePrinting\Sales\Orders;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static SalesOrderTaxDAO instance(PDO $connection = null, $schemaName = null)
 * @method SalesOrderTaxVO findById($id, $options = null, $useCache = false)
 * @method SalesOrderTaxVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesOrderTaxVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesOrderTaxVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method SalesOrderTaxVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesOrderTaxVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesOrderTaxVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class SalesOrderTaxDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
