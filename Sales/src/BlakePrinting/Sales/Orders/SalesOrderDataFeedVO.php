<?php

namespace BlakePrinting\Sales\Orders;

use BlakePrinting\DataSources\AbstractEntityDataFeedVO;

class SalesOrderDataFeedVO extends AbstractEntityDataFeedVO {
    /** @var int */
    private $salesOrderId = null;

    public function __construct($salesOrderId = null, $dataFeedId = null) {
        parent::__construct($dataFeedId);

        $this->salesOrderId = $salesOrderId;
    }

    /**
     * @return int
     */
    public function getSalesOrderId()
    {
        return $this->salesOrderId;
    }

    /**
     * @param int $salesOrderId
     */
    public function setSalesOrderId($salesOrderId)
    {
        $this->salesOrderId = $salesOrderId;
    }
}
