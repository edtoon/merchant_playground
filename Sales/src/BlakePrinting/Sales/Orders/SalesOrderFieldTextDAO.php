<?php

namespace BlakePrinting\Sales\Orders;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static SalesOrderFieldTextDAO instance(PDO $connection = null, $schemaName = null)
 * @method SalesOrderFieldTextVO findById($id, $options = null, $useCache = false)
 * @method SalesOrderFieldTextVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesOrderFieldTextVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesOrderFieldTextVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method SalesOrderFieldTextVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesOrderFieldTextVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesOrderFieldTextVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class SalesOrderFieldTextDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
