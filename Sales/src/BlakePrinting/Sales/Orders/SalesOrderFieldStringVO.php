<?php

namespace BlakePrinting\Sales\Orders;

use BlakePrinting\Fields\AbstractEntityFieldStringVO;

class SalesOrderFieldStringVO extends AbstractEntityFieldStringVO {
    /** @var int */
    private $salesOrderId = null;

    public function __construct($shipmentId = null, $fieldId = null, $value = null) {
        parent::__construct($fieldId, $value);

        $this->salesOrderId = $shipmentId;
    }

    /**
     * @return int
     */
    public function getSalesOrderId()
    {
        return $this->salesOrderId;
    }

    /**
     * @param int $salesOrderId
     */
    public function setSalesOrderId($salesOrderId)
    {
        $this->salesOrderId = $salesOrderId;
    }
}
