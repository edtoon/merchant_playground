<?php

namespace BlakePrinting\Sales\Orders;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static SalesOrderShipmentDAO instance(PDO $connection = null, $schemaName = null)
 * @method SalesOrderShipmentVO findById($id, $options = null, $useCache = false)
 * @method SalesOrderShipmentVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesOrderShipmentVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesOrderShipmentVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method SalesOrderShipmentVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesOrderShipmentVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesOrderShipmentVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class SalesOrderShipmentDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
