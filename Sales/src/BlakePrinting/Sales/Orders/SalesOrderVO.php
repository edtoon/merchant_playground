<?php

namespace BlakePrinting\Sales\Orders;

use BlakePrinting\Db\ValueObject;

class SalesOrderVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $creator = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $updater = null;
    /** @var int */
    private $sellerOrganizationId = null;
    /** @var int */
    private $purchaserOrganizationId = null;
    /** @var int */
    private $salesChannelId = null;
    /** @var string */
    private $title = null;
    /** @var int */
    private $ordered = null;
    /** @var int */
    private $purchaserCurrencyId = null;
    /** @var int */
    private $saleCurrencyId = null;
    /** @var string */
    private $currencyExchangeRate = null;
    /** @var int */
    private $paymentMethodId = null;
    /** @var string */
    private $shippingAmount = null;
    /** @var string */
    private $subtotalAmount = null;
    /** @var string */
    private $totalAmount = null;
    /** @var string */
    private $purchaseOrderNumber = null;
    /** @var int */
    private $fulfillmentStatusId = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param int $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getUpdater()
    {
        return $this->updater;
    }

    /**
     * @param int $updater
     */
    public function setUpdater($updater)
    {
        $this->updater = $updater;
    }

    /**
     * @return int
     */
    public function getSellerOrganizationId()
    {
        return $this->sellerOrganizationId;
    }

    /**
     * @param int $sellerOrganizationId
     */
    public function setSellerOrganizationId($sellerOrganizationId)
    {
        $this->sellerOrganizationId = $sellerOrganizationId;
    }

    /**
     * @return int
     */
    public function getPurchaserOrganizationId()
    {
        return $this->purchaserOrganizationId;
    }

    /**
     * @param int $purchaserOrganizationId
     */
    public function setPurchaserOrganizationId($purchaserOrganizationId)
    {
        $this->purchaserOrganizationId = $purchaserOrganizationId;
    }

    /**
     * @return int
     */
    public function getSalesChannelId()
    {
        return $this->salesChannelId;
    }

    /**
     * @param int $salesChannelId
     */
    public function setSalesChannelId($salesChannelId)
    {
        $this->salesChannelId = $salesChannelId;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getOrdered()
    {
        return $this->ordered;
    }

    /**
     * @param int $ordered
     */
    public function setOrdered($ordered)
    {
        $this->ordered = $ordered;
    }

    /**
     * @return int
     */
    public function getPurchaserCurrencyId()
    {
        return $this->purchaserCurrencyId;
    }

    /**
     * @param int $purchaserCurrencyId
     */
    public function setPurchaserCurrencyId($purchaserCurrencyId)
    {
        $this->purchaserCurrencyId = $purchaserCurrencyId;
    }

    /**
     * @return int
     */
    public function getSaleCurrencyId()
    {
        return $this->saleCurrencyId;
    }

    /**
     * @param int $saleCurrencyId
     */
    public function setSaleCurrencyId($saleCurrencyId)
    {
        $this->saleCurrencyId = $saleCurrencyId;
    }

    /**
     * @return string
     */
    public function getCurrencyExchangeRate()
    {
        return $this->currencyExchangeRate;
    }

    /**
     * @param string $currencyExchangeRate
     */
    public function setCurrencyExchangeRate($currencyExchangeRate)
    {
        $this->currencyExchangeRate = $currencyExchangeRate;
    }

    /**
     * @return int
     */
    public function getPaymentMethodId()
    {
        return $this->paymentMethodId;
    }

    /**
     * @param int $paymentMethodId
     */
    public function setPaymentMethodId($paymentMethodId)
    {
        $this->paymentMethodId = $paymentMethodId;
    }

    /**
     * @return string
     */
    public function getShippingAmount()
    {
        return $this->shippingAmount;
    }

    /**
     * @param string $shippingAmount
     */
    public function setShippingAmount($shippingAmount)
    {
        $this->shippingAmount = $shippingAmount;
    }

    /**
     * @return string
     */
    public function getSubtotalAmount()
    {
        return $this->subtotalAmount;
    }

    /**
     * @param string $subtotalAmount
     */
    public function setSubtotalAmount($subtotalAmount)
    {
        $this->subtotalAmount = $subtotalAmount;
    }

    /**
     * @return string
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * @param string $totalAmount
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;
    }

    /**
     * @return string
     */
    public function getPurchaseOrderNumber()
    {
        return $this->purchaseOrderNumber;
    }

    /**
     * @param string $purchaseOrderNumber
     */
    public function setPurchaseOrderNumber($purchaseOrderNumber)
    {
        $this->purchaseOrderNumber = $purchaseOrderNumber;
    }

    /**
     * @return int
     */
    public function getFulfillmentStatusId()
    {
        return $this->fulfillmentStatusId;
    }

    /**
     * @param int $fulfillmentStatusId
     */
    public function setFulfillmentStatusId($fulfillmentStatusId)
    {
        $this->fulfillmentStatusId = $fulfillmentStatusId;
    }
}
