<?php

namespace BlakePrinting\Sales\Orders;

use BlakePrinting\DataSources\AbstractEntityDataFeedDAO;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static SalesOrderDataFeedDAO instance(PDO $connection = null, $schemaName = null)
 * @method SalesOrderDataFeedVO findById($id, $options = null, $useCache = false)
 * @method SalesOrderDataFeedVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesOrderDataFeedVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesOrderDataFeedVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method SalesOrderDataFeedVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesOrderDataFeedVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesOrderDataFeedVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method void store(SalesOrderDataFeedVO $vo)
 */
class SalesOrderDataFeedDAO extends AbstractEntityDataFeedDAO {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, "sales_order_id");
    }
}
