<?php

namespace BlakePrinting\Sales\Orders;

use BlakePrinting\Taxes\AbstractEntityTaxVO;

class SalesOrderTaxVO extends AbstractEntityTaxVO {
    /** @var int */
    private $salesOrderId = null;

    /**
     * @param int $salesOrderId
     * @param int $taxRateId
     * @param int $taxIdentifierTypeId
     * @param string $taxIdentifier
     * @param string $variablePercentage
     * @param string $taxAmount
     */
    public function __construct($salesOrderId = null, $taxRateId = null, $taxIdentifierTypeId = null, $taxIdentifier = null, $variablePercentage = null, $taxAmount = null) {
        parent::__construct($taxRateId, $taxIdentifierTypeId, $taxIdentifier, $variablePercentage, $taxAmount);

        $this->salesOrderId = $salesOrderId;
    }

    /**
     * @return int
     */
    public function getSalesOrderId()
    {
        return $this->salesOrderId;
    }

    /**
     * @param int $salesOrderId
     */
    public function setSalesOrderId($salesOrderId)
    {
        $this->salesOrderId = $salesOrderId;
    }
}
