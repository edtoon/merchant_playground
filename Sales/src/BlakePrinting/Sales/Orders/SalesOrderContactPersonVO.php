<?php

namespace BlakePrinting\Sales\Orders;

use BlakePrinting\Contacts\AbstractEntityContactPersonVO;

class SalesOrderContactPersonVO extends AbstractEntityContactPersonVO {
    /** @var int */
    private $salesOrderId = null;

    public function __construct($salesOrderId = null, $contactPersonId = null) {
        parent::__construct($contactPersonId);

        $this->salesOrderId = $salesOrderId;
    }

    /**
     * @return int
     */
    public function getSalesOrderId()
    {
        return $this->salesOrderId;
    }

    /**
     * @param int $salesOrderId
     */
    public function setSalesOrderId($salesOrderId)
    {
        $this->salesOrderId = $salesOrderId;
    }
}
