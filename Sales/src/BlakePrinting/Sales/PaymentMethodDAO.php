<?php

namespace BlakePrinting\Sales;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static PaymentMethodDAO instance(PDO $connection = null, $schemaName = null)
 * @method PaymentMethodVO findById($id, $options = null, $useCache = false)
 * @method PaymentMethodVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method PaymentMethodVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method PaymentMethodVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method PaymentMethodVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method PaymentMethodVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method PaymentMethodVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method PaymentMethodVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class PaymentMethodDAO extends AbstractDataAccessObject {
    const PAYMENT_METHOD_AFTER_PAY = "AfterPay.nl"; // 1293 invoices, postpaid
    const PAYMENT_METHOD_AUTHORIZE_NET = "Authorize.net"; // 1 invoice
    const PAYMENT_METHOD_BILL_INK = "Bill Ink"; // 16 invoices
    const PAYMENT_METHOD_BOL_COM = "bol.com"; // 7636 invoices
    const PAYMENT_METHOD_BUCKAROO = "Buckaroo"; // 10 invoices
    const PAYMENT_METHOD_CASH_ON_DELIVERY = "C.O.D."; // 11 invoices, postpaid
    const PAYMENT_METHOD_FRENCH_CREDIT_CARD = "Societe Generale (Credit Card - France)"; // 7945 invoices, prepaid
    const PAYMENT_METHOD_IDEAL = "iDEAL"; // 1950 invoices, "Check or Money Order (will call)", prepaid
    const PAYMENT_METHOD_PAYPAL = "PayPal"; // 3031 invoices, prepaid
    const PAYMENT_METHOD_PREPAY = "Pre-Paid"; // 991 invoices, postpaid
    const PAYMENT_METHOD_PRICE_MINISTER = "Price Minister"; // 168 invoices
    const PAYMENT_METHOD_SOFORT = "SOFORT"; // 6 invoices, prepaid
    const PAYMENT_METHOD_STRIPE = "Stripe"; // 6 invoices, prepaid
    const PAYMENT_METHOD_PAYPAL_CREDIT_CARD = "PayPal Credit Card"; // 21 invoices
    const PAYMENT_METHOD_WORLDPAY = "WorldPay"; // 1 invoice
}
