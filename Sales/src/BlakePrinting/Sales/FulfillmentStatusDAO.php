<?php

namespace BlakePrinting\Sales;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static FulfillmentStatusDAO instance(PDO $connection = null, $schemaName = null)
 * @method FulfillmentStatusVO findById($id, $options = null, $useCache = false)
 * @method FulfillmentStatusVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FulfillmentStatusVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FulfillmentStatusVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method FulfillmentStatusVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FulfillmentStatusVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FulfillmentStatusVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method FulfillmentStatusVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class FulfillmentStatusDAO extends AbstractDataAccessObject {
    const FULFILLMENT_STATUS_PENDING = "pending";
    const FULFILLMENT_STATUS_PARTIAL = "partial";
    const FULFILLMENT_STATUS_PROCESSED = "processed";
    const FULFILLMENT_STATUS_CANCELLED = "cancelled";
    const FULFILLMENT_STATUS_UNKNOWN = "unknown";
}
