<?php

namespace BlakePrinting\Sales;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static SalesChannelDAO instance(PDO $connection = null, $schemaName = null)
 * @method SalesChannelVO findById($id, $options = null, $useCache = false)
 * @method SalesChannelVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesChannelVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesChannelVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method SalesChannelVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesChannelVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesChannelVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method SalesChannelVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class SalesChannelDAO extends AbstractDataAccessObject {
    const SALES_CHANNEL_INKT_MAXX = "InktMaxx.nl";
    const SALES_CHANNEL_BOL_NL = "bol.com NL";
    const SALES_CHANNEL_BOL_BE = "bol.com BE";
    const SALES_CHANNEL_TINTEN_TONER = "Tinten-Toner-und-Co.de"; // aka patronenland.de
    const SALES_CHANNEL_PRICE_MINISTER = "PriceMinister.com";
    const SALES_CHANNEL_PARADISE_CARTOUCHE = "ParadiseCartouche.fr";
    const SALES_CHANNEL_ALL_INKJETS = "AllInkjets.co.uk";
}
