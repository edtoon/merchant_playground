<?php

namespace BlakePrinting\Sales;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static PaymentStatusDAO instance(PDO $connection = null, $schemaName = null)
 * @method PaymentStatusVO findById($id, $options = null, $useCache = false)
 * @method PaymentStatusVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method PaymentStatusVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method PaymentStatusVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method PaymentStatusVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method PaymentStatusVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method PaymentStatusVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method PaymentStatusVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class PaymentStatusDAO extends AbstractDataAccessObject {
    const PAYMENT_STATUS_CANCELLED = "cancelled";
    const PAYMENT_STATUS_SCHEDULED = "scheduled";
    const PAYMENT_STATUS_AUTH = "auth";
    const PAYMENT_STATUS_CAPTURE = "capture";
    const PAYMENT_STATUS_VERIFY = "verify";
    const PAYMENT_STATUS_HOLD = "hold";
    const PAYMENT_STATUS_PAID = "paid";
    const PAYMENT_STATUS_REFUNDING = "refunding";
    const PAYMENT_STATUS_REFUNDED = "refunded";
    const PAYMENT_STATUS_TIMEOUT = "timeout";
    const PAYMENT_STATUS_DECLINED = "declined";
    const PAYMENT_STATUS_ERROR = "error";
}
