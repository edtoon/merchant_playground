<?php

namespace BlakePrinting\Sales\Invoices;

use BlakePrinting\DataSources\AbstractEntityDataFeedVO;

class SalesInvoiceDataFeedVO extends AbstractEntityDataFeedVO {
    /** @var int */
    private $salesInvoiceId = null;

    public function __construct($salesInvoiceId = null, $dataFeedId = null) {
        parent::__construct($dataFeedId);

        $this->salesInvoiceId = $salesInvoiceId;
    }

    /**
     * @return int
     */
    public function getSalesInvoiceId()
    {
        return $this->salesInvoiceId;
    }

    /**
     * @param int $salesInvoiceId
     */
    public function setSalesInvoiceId($salesInvoiceId)
    {
        $this->salesInvoiceId = $salesInvoiceId;
    }
}
