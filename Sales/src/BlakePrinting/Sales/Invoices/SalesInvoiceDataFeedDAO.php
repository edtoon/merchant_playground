<?php

namespace BlakePrinting\Sales\Invoices;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static SalesInvoiceDataFeedDAO instance(PDO $connection = null, $schemaName = null)
 * @method SalesInvoiceDataFeedVO findById($id, $options = null, $useCache = false)
 * @method SalesInvoiceDataFeedVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesInvoiceDataFeedVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesInvoiceDataFeedVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method SalesInvoiceDataFeedVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesInvoiceDataFeedVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesInvoiceDataFeedVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class SalesInvoiceDataFeedDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
