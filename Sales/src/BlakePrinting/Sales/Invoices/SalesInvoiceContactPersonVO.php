<?php

namespace BlakePrinting\Sales\Invoices;

use BlakePrinting\Contacts\AbstractEntityContactPersonVO;

class SalesInvoiceContactPersonVO extends AbstractEntityContactPersonVO {
    /** @var int */
    private $salesInvoiceId = null;

    public function __construct($salesInvoiceId = null, $contactPersonId = null) {
        parent::__construct($contactPersonId);

        $this->salesInvoiceId = $salesInvoiceId;
    }

    /**
     * @return int
     */
    public function getSalesInvoiceId()
    {
        return $this->salesInvoiceId;
    }

    /**
     * @param int $salesInvoiceId
     */
    public function setSalesInvoiceId($salesInvoiceId)
    {
        $this->salesInvoiceId = $salesInvoiceId;
    }
}
