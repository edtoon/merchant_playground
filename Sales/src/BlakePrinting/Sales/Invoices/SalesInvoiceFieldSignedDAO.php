<?php

namespace BlakePrinting\Sales\Invoices;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static SalesInvoiceFieldSignedDAO instance(PDO $connection = null, $schemaName = null)
 * @method SalesInvoiceFieldSignedVO findById($id, $options = null, $useCache = false)
 * @method SalesInvoiceFieldSignedVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesInvoiceFieldSignedVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesInvoiceFieldSignedVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method SalesInvoiceFieldSignedVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesInvoiceFieldSignedVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesInvoiceFieldSignedVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class SalesInvoiceFieldSignedDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
