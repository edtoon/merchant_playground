<?php

namespace BlakePrinting\Sales\Invoices;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static SalesInvoiceFieldUnsignedDAO instance(PDO $connection = null, $schemaName = null)
 * @method SalesInvoiceFieldUnsignedVO findById($id, $options = null, $useCache = false)
 * @method SalesInvoiceFieldUnsignedVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesInvoiceFieldUnsignedVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesInvoiceFieldUnsignedVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method SalesInvoiceFieldUnsignedVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesInvoiceFieldUnsignedVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesInvoiceFieldUnsignedVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class SalesInvoiceFieldUnsignedDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
