<?php

namespace BlakePrinting\Sales\Invoices;

use BlakePrinting\Contacts\AbstractEntityContactAddressVO;

class SalesInvoiceContactAddressVO extends AbstractEntityContactAddressVO {
    /** @var int */
    private $salesInvoiceId = null;
    /** @var int */
    private $contactAddressId = null;

    public function __construct($salesInvoiceId = null, $contactAddressId = null) {
        parent::__construct($contactAddressId);

        $this->salesInvoiceId = $salesInvoiceId;
    }

    /**
     * @return int
     */
    public function getSalesInvoiceId()
    {
        return $this->salesInvoiceId;
    }

    /**
     * @param int $salesInvoiceId
     */
    public function setSalesInvoiceId($salesInvoiceId)
    {
        $this->salesInvoiceId = $salesInvoiceId;
    }
}
