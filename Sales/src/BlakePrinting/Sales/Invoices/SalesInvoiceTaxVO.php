<?php

namespace BlakePrinting\Sales\Invoices;

use BlakePrinting\Taxes\AbstractEntityTaxVO;

class SalesInvoiceTaxVO extends AbstractEntityTaxVO {
    /** @var int */
    private $salesInvoiceId = null;

    /**
     * @param int $salesOrderId
     * @param int $taxRateId
     * @param int $taxIdentifierTypeId
     * @param string $taxIdentifier
     * @param string $variablePercentage
     * @param string $taxAmount
     */
    public function __construct($salesInvoiceId = null, $taxRateId = null, $taxIdentifierTypeId = null, $taxIdentifier = null, $variablePercentage = null, $taxAmount = null) {
        parent::__construct($taxRateId, $taxIdentifierTypeId, $taxIdentifier, $variablePercentage, $taxAmount);

        $this->salesInvoiceId = $salesInvoiceId;
    }

    /**
     * @return int
     */
    public function getSalesInvoiceId()
    {
        return $this->salesInvoiceId;
    }

    /**
     * @param int $salesInvoiceId
     */
    public function setSalesInvoiceId($salesInvoiceId)
    {
        $this->salesInvoiceId = $salesInvoiceId;
    }
}
