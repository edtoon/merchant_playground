<?php

namespace BlakePrinting\Sales\Invoices;

use BlakePrinting\Fields\AbstractEntityFieldIntVO;

class SalesInvoiceFieldSignedVO extends AbstractEntityFieldIntVO {
    /** @var int */
    private $salesInvoiceId = null;

    public function __construct($salesInvoiceId = null, $fieldId = null, $value = null) {
        parent::__construct($fieldId, $value);

        $this->salesInvoiceId = $salesInvoiceId;
    }

    /**
     * @return int
     */
    public function getSalesInvoiceId()
    {
        return $this->salesInvoiceId;
    }

    /**
     * @param int $salesInvoiceId
     */
    public function setSalesInvoiceId($salesInvoiceId)
    {
        $this->salesInvoiceId = $salesInvoiceId;
    }
}
