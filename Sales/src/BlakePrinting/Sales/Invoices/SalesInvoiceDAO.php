<?php

namespace BlakePrinting\Sales\Invoices;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static SalesInvoiceDAO instance(PDO $connection = null, $schemaName = null)
 * @method SalesInvoiceVO findById($id, $options = null, $useCache = false)
 * @method SalesInvoiceVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesInvoiceVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesInvoiceVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method SalesInvoiceVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesInvoiceVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesInvoiceVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method SalesInvoiceVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class SalesInvoiceDAO extends AbstractDataAccessObject {
}
