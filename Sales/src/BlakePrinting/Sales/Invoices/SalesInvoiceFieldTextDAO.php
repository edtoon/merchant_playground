<?php

namespace BlakePrinting\Sales\Invoices;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static SalesInvoiceFieldTextDAO instance(PDO $connection = null, $schemaName = null)
 * @method SalesInvoiceFieldTextVO findById($id, $options = null, $useCache = false)
 * @method SalesInvoiceFieldTextVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesInvoiceFieldTextVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesInvoiceFieldTextVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method SalesInvoiceFieldTextVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SalesInvoiceFieldTextVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SalesInvoiceFieldTextVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class SalesInvoiceFieldTextDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
