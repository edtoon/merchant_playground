<?php

namespace BlakePrinting\Sales\Invoices;

use BlakePrinting\Sales\Orders\SalesOrderVO;

class SalesInvoiceVO extends SalesOrderVO {
    /** @var int */
    private $salesOrderId = null;
    /** @var int */
    private $invoiced = null;
    /** @var int */
    private $shipped = null;
}
