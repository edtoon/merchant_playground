<?php

namespace BlakePrinting\Sales\Load;

use BlakePrinting\Catalog\EntityCatalogProductsTrait;
use BlakePrinting\Contacts\EntityContactAddressesTrait;
use BlakePrinting\Contacts\EntityContactPersonsTrait;
use BlakePrinting\DataSources\EntityDataFeedsTrait;
use BlakePrinting\Db\AbstractExtensibleEntity;
use BlakePrinting\Db\ConnectionProvider;
use BlakePrinting\Db\ValueObject;
use BlakePrinting\Fields\EntityFieldsTrait;
use BlakePrinting\Globalization\CurrencyVO;
use BlakePrinting\Organizations\OrganizationVO;
use BlakePrinting\Sales\FulfillmentStatusDAO;
use BlakePrinting\Sales\FulfillmentStatusVO;
use BlakePrinting\Sales\Orders\SalesOrderVO;
use BlakePrinting\Sales\PaymentMethodVO;
use BlakePrinting\Sales\SalesChannelVO;
use BlakePrinting\Shipping\EntityShipmentsTrait;
use BlakePrinting\Taxes\EntityTaxesTrait;
use BlakePrinting\Util\Utils;
use PDO;

class SalesOrderLoader extends AbstractExtensibleEntity implements ConnectionProvider {
    use EntityCatalogProductsTrait;
    use EntityContactAddressesTrait;
    use EntityContactPersonsTrait;
    use EntityDataFeedsTrait;
    use EntityFieldsTrait;
    use EntityShipmentsTrait;
    use EntityTaxesTrait;

    /** @var int */
    private $id = null;
    /** @var OrganizationVO */
    private $sellerOrganizationVo = null;
    /** @var OrganizationVO */
    private $purchaserOrganizationVo = null;
    /** @var SalesChannelVO */
    private $salesChannelVo = null;
    /** @var CurrencyVO */
    private $purchaserCurrencyVo = null;
    /** @var CurrencyVO */
    private $saleCurrencyVo = null;
    /** @var PaymentMethodVO */
    private $paymentMethodVo = null;
    /** @var FulfillmentStatusVO */
    private $fulfillmentStatusVo = null;
    /** @var PDO */
    private $connection = null;

    public function __construct(PDO &$connection) {
        $this->setConnection($connection);
        $this->setEntityClassName(SalesOrderVO::class);
        $this->setEntityColumnName("sales_order_id");
        $this->setEntityColumnType(PDO::PARAM_INT);
    }

    /**
     * @param string $title
     * @param int $orderedTimestamp
     * @param string $exchangeRate
     * @param string $shippingAmount
     * @param string $subtotalAmount
     * @param string $totalAmount
     * @param string $purchaseOrderNumber
     * @return ValueObject
     */
    public function &loadSalesOrder($title, $orderedTimestamp, $exchangeRate, $shippingAmount, $subtotalAmount, $totalAmount, $purchaseOrderNumber) {
        /** @var SalesOrderVO $valueObject */
        $valueObject = Utils::getReflectionClassByName($this->getEntityClassName())->newInstance();

        $valueObject->setId($this->id);
        $valueObject->setTitle($title);
        $valueObject->setOrdered($orderedTimestamp);
        $valueObject->setCurrencyExchangeRate($exchangeRate);
        $valueObject->setShippingAmount($shippingAmount);
        $valueObject->setSubtotalAmount($subtotalAmount);
        $valueObject->setTotalAmount($totalAmount);
        $valueObject->setPurchaseOrderNumber($purchaseOrderNumber);

        if($this->sellerOrganizationVo !== null) {
            $valueObject->setSellerOrganizationId($this->sellerOrganizationVo->getId());
        }

        if($this->purchaserOrganizationVo !== null) {
            $valueObject->setPurchaserOrganizationId($this->purchaserOrganizationVo->getId());
        }

        if($this->salesChannelVo !== null) {
            $valueObject->setSalesChannelId($this->salesChannelVo->getId());
        }

        if($this->purchaserCurrencyVo !== null) {
            $valueObject->setPurchaserCurrencyId($this->purchaserCurrencyVo->getId());
        }

        if($this->saleCurrencyVo !== null) {
            $valueObject->setSaleCurrencyId($this->saleCurrencyVo->getId());
        }

        if($this->paymentMethodVo !== null) {
            $valueObject->setPaymentMethodId($this->paymentMethodVo->getId());
        }

        if($this->fulfillmentStatusVo === null) {
            $this->fulfillmentStatusVo =& FulfillmentStatusDAO::instance($this->getConnection())->findByColumn(
                "name", FulfillmentStatusDAO::FULFILLMENT_STATUS_UNKNOWN, PDO::PARAM_STR, null, null, null, true
            );
        }

        $valueObject->setFulfillmentStatusId($this->fulfillmentStatusVo->getId());

        $dao = $this->getEntityDAO();

        if($this->id === null) {
            $insertedId =& $dao->insert($valueObject);
            $valueObject =& $dao->findById($insertedId);
        } else {
            $reflectedColumns = $dao->getReflectedColumns();
            $entityColumns = [];

            foreach($reflectedColumns as $propertyName => &$reflectedColumnData) {
                $entityColumns[] = $reflectedColumnData[0];
            }

            $includeNullColumns = array_diff($entityColumns, ["id", "created", "creator", "updated", "updater"]);

            $dao->update($valueObject, null, $includeNullColumns);

            $valueObject =& $dao->findById($this->id);
        }

        $this->setEntityColumnValue($valueObject->getId());
        $this->replaceCatalogProducts();
        $this->replaceContactAddresses();
        $this->replaceContactPersons();
        $this->insertDataFeeds();
        $this->replaceFields();
        $this->replaceShipments();
        $this->replaceTaxes();

        return $valueObject;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return OrganizationVO
     */
    public function getSellerOrganizationVo()
    {
        return $this->sellerOrganizationVo;
    }

    /**
     * @param OrganizationVO $sellerOrganizationVo
     */
    public function setSellerOrganizationVo($sellerOrganizationVo)
    {
        $this->sellerOrganizationVo = $sellerOrganizationVo;
    }

    /**
     * @return OrganizationVO
     */
    public function getPurchaserOrganizationVo()
    {
        return $this->purchaserOrganizationVo;
    }

    /**
     * @param OrganizationVO $purchaserOrganizationVo
     */
    public function setPurchaserOrganizationVo($purchaserOrganizationVo)
    {
        $this->purchaserOrganizationVo = $purchaserOrganizationVo;
    }

    /**
     * @return SalesChannelVO
     */
    public function getSalesChannelVo()
    {
        return $this->salesChannelVo;
    }

    /**
     * @param SalesChannelVO $salesChannelVo
     */
    public function setSalesChannelVo($salesChannelVo)
    {
        $this->salesChannelVo = $salesChannelVo;
    }

    /**
     * @return CurrencyVO
     */
    public function getPurchaserCurrencyVo()
    {
        return $this->purchaserCurrencyVo;
    }

    /**
     * @param CurrencyVO $purchaserCurrencyVo
     */
    public function setPurchaserCurrencyVo($purchaserCurrencyVo)
    {
        $this->purchaserCurrencyVo = $purchaserCurrencyVo;
    }

    /**
     * @return CurrencyVO
     */
    public function getSaleCurrencyVo()
    {
        return $this->saleCurrencyVo;
    }

    /**
     * @param CurrencyVO $saleCurrencyVo
     */
    public function setSaleCurrencyVo($saleCurrencyVo)
    {
        $this->saleCurrencyVo = $saleCurrencyVo;
    }

    /**
     * @return PaymentMethodVO
     */
    public function getPaymentMethodVo()
    {
        return $this->paymentMethodVo;
    }

    /**
     * @param PaymentMethodVO $paymentMethodVo
     */
    public function setPaymentMethodVo($paymentMethodVo)
    {
        $this->paymentMethodVo = $paymentMethodVo;
    }

    /**
     * @return FulfillmentStatusVO
     */
    public function getFulfillmentStatusVo()
    {
        return $this->fulfillmentStatusVo;
    }

    /**
     * @param FulfillmentStatusVO $fulfillmentStatusVo
     */
    public function setFulfillmentStatusVo($fulfillmentStatusVo)
    {
        $this->fulfillmentStatusVo = $fulfillmentStatusVo;
    }

    /**
     * @return PDO
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @param PDO $connection
     */
    public function setConnection(PDO $connection)
    {
        $this->connection = $connection;
    }
}
