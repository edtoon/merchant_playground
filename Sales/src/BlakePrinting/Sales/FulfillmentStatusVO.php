<?php

namespace BlakePrinting\Sales;

use BlakePrinting\Db\ValueObject;

class FulfillmentStatusVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var string */
    private $name = null;

    public function __construct($name = null) {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
