<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\Facilities\FacilityDAO;
use BlakePrinting\Printers\GatewayApiKeyDAO;
use BlakePrinting\Printers\GatewayDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class Gateways extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $gatewayDao = new GatewayDAO($connection);
        $facilityDao = new FacilityDAO($connection);
        $this->tableDao($gatewayDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("facility_id", "integer", ["signed" => false])
            ->addForeignKey("facility_id", $facilityDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->save()
        ;

        $gatewayApiKeyDao = new GatewayApiKeyDAO($connection);
        $this->tableDao($gatewayApiKeyDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("gateway_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("api_key", "string", ["limit" => 191])
            ->addColumn("activated", "biginteger", ["signed" => false, "null" => true])
            ->addForeignKey("gateway_id", $gatewayDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("api_key", ["unique" => true])
            ->addIndex("activated")
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(new GatewayApiKeyDAO($connection));
        $this->dropDao(new GatewayDAO($connection));
    }
}
