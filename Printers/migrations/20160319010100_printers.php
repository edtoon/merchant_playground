<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\Locations\LocationDAO;
use BlakePrinting\Organizations\OrganizationDAO;
use BlakePrinting\Printers\PrinterAttributeDAO;
use BlakePrinting\Printers\PrinterConnectionIpv4DAO;
use BlakePrinting\Printers\PrinterConnectionIpv6DAO;
use BlakePrinting\Printers\PrinterDAO;
use BlakePrinting\Printers\PrinterStatusDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class Printers extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $organizationDao = new OrganizationDAO($connection);
        $organizationTbl = $organizationDao->formatSchemaAndTableName();
        $printerDao = new PrinterDAO($connection);
        $this->tableDao($printerDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("owner_organization_id", "integer", ["null" => true, "signed" => false])
            ->addColumn("manufacturer_organization_id", "integer", ["signed" => false])
            ->addColumn("product_name", "string", ["limit" => 191])
            ->addColumn("product_number", "string", ["limit" => 50])
            ->addColumn("serial_number", "string", ["limit" => 191])
            ->addColumn("system_name", "string", ["limit" => 191])
            ->addForeignKey("owner_organization_id", $organizationTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("manufacturer_organization_id", $organizationTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("product_name")
            ->addIndex("product_number")
            ->addIndex("serial_number")
            ->addIndex("system_name")
            ->save()
        ;

        $printerTbl = $printerDao->formatSchemaAndTableName();
        $printerAttributeDao = new PrinterAttributeDAO($connection);
        $this->tableDao($printerAttributeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["printer_id", "name"]])
            ->addColumn("printer_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("name", "string", ["limit" => 191])
            ->addColumn("value", "text")
            ->addForeignKey("printer_id", $printerTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("name")
            ->save()
        ;
        $this->alterDao($printerAttributeDao, "ADD INDEX (value(191))");

        $printerConnectionIpv4Dao = new PrinterConnectionIpv4DAO($connection);
        $this->tableDao($printerConnectionIpv4Dao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("printer_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("port", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addForeignKey("printer_id", $printerTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("port")
            ->save()
        ;
        $this->alterDao($printerConnectionIpv4Dao, "ADD ipv4 BINARY(4) NOT NULL, ADD KEY (ipv4)");
        $this->alterDao($printerConnectionIpv4Dao, "ADD default_gateway BINARY(4), ADD KEY (default_gateway)");
        $this->alterDao($printerConnectionIpv4Dao, "ADD subnet_mask BINARY(4), ADD KEY (subnet_mask)");

        $printerConnectionIpv6Dao = new PrinterConnectionIpv6DAO($connection);
        $this->tableDao($printerConnectionIpv6Dao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("printer_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("port", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("prefix_length", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addForeignKey("printer_id", $printerTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("port")
            ->save()
        ;
        $this->alterDao($printerConnectionIpv6Dao, "ADD ipv6 BINARY(16) NOT NULL, ADD KEY (ipv6)");
        $this->alterDao($printerConnectionIpv6Dao, "ADD default_gateway BINARY(16), ADD KEY (default_gateway)");

        $printerStatusDao = new PrinterStatusDAO($connection);
        $locationDao = new LocationDAO($connection);
        $this->tableDao($printerStatusDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["printer_id"]])
            ->addColumn("printer_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("location_id", "integer", ["null" => true, "signed" => false])
            ->addColumn("status_description", "string", ["limit" => 191])
            ->addColumn("use_network", "integer", ["limit" => MysqlAdapter::INT_TINY, "precision" => 1])
            ->addColumn("ready", "integer", ["limit" => MysqlAdapter::INT_TINY, "precision" => 1])
            ->addForeignKey("printer_id", $printerTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("location_id", $locationDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("updated")
            ->addIndex("status_description")
            ->addIndex("use_network")
            ->addIndex("ready")
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(new PrinterStatusDAO($connection));
        $this->dropDao(new PrinterConnectionIpv6DAO($connection));
        $this->dropDao(new PrinterConnectionIpv4DAO($connection));
        $this->dropDao(new PrinterAttributeDAO($connection));
        $this->dropDao(new PrinterDAO($connection));
    }
}
