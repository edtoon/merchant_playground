<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\Printers\GatewayDAO;
use BlakePrinting\Printers\GatewayPrinterDAO;
use BlakePrinting\Printers\PrinterDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class GatewayPrinters extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $gatewayDao = new GatewayDAO($connection);
        $printerDao = new PrinterDAO($connection);
        $gatewayPrinterDao = new GatewayPrinterDAO($connection);
        $this->tableDao($gatewayPrinterDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["gateway_id", "printer_id"]])
            ->addColumn("gateway_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("printer_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addForeignKey("gateway_id", $gatewayDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("printer_id", $printerDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(new GatewayPrinterDAO($connection));
    }
}
