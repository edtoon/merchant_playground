<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\Labels\LabelDAO;
use BlakePrinting\Printers\GatewayDAO;
use BlakePrinting\Printers\GatewayPrinterLabelQueueDAO;
use BlakePrinting\Printers\PrinterDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class GatewayPrinterLabelQueue extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $gatewayDao = new GatewayDAO($connection);
        $printerDao = new PrinterDAO($connection);
        $labelDao = new LabelDAO($connection);
        $gatewayPrinterLabelQueueDao = new GatewayPrinterLabelQueueDAO($connection);
        $this->tableDao($gatewayPrinterLabelQueueDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["gateway_id", "printer_id", "label_id"]])
            ->addColumn("gateway_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("printer_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("label_id", "biginteger", ["signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("completed", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("cancelled", "biginteger", ["signed" => false, "null" => true])
            ->addForeignKey("gateway_id", $gatewayDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("printer_id", $printerDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("label_id", $labelDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("completed")
            ->addIndex("cancelled")
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(new GatewayPrinterLabelQueueDAO($connection));
    }
}
