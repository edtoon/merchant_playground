<?php

namespace BlakePrinting\Printers;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static PrinterConnectionIpv6DAO instance(PDO $connection = null, $schemaName = null)
 * @method PrinterConnectionIpv6VO findById($id, $options = null, $useCache = false)
 * @method PrinterConnectionIpv6VO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method PrinterConnectionIpv6VO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method PrinterConnectionIpv6VO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method PrinterConnectionIpv6VO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method PrinterConnectionIpv6VO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method PrinterConnectionIpv6VO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class PrinterConnectionIpv6DAO extends AbstractDataAccessObject {
}
