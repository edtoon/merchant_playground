<?php

namespace BlakePrinting\Printers;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static PrinterAttributeDAO instance(PDO $connection = null, $schemaName = null)
 * @method PrinterAttributeVO findById($id, $options = null, $useCache = false)
 * @method PrinterAttributeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method PrinterAttributeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method PrinterAttributeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method PrinterAttributeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method PrinterAttributeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method PrinterAttributeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class PrinterAttributeDAO extends AbstractDataAccessObject {
    const ATTR_DATE_CODE = "dateCode";
    const ATTR_DISCOVERY_VERSION = "discoveryVersion";
    const ATTR_GET_COMMUNITY_NAME = "encryptedGetCommunityName";
    const ATTR_SET_COMMUNITY_NAME = "encryptedSetCommunityName";
    const ATTR_FIRMWARE_VERSION = "firmwareVersion";
    const ATTR_HARDWARE_ADDRESS = "hardwareAddress";
    const ATTR_PORT_NAME = "portName";

    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $printerId
     * @param string $name
     * @param string $value
     * @return int
     */
    public function &storeAttribute($printerId, $name, $value) {
        return PDOQuery::insert()
            ->into($this)
            ->value("printer_id", $printerId, PDO::PARAM_INT)
            ->value("name", $name, PDO::PARAM_STR)
            ->value("value", $value, PDO::PARAM_STR)
            ->onDuplicateValue("value", "VALUES(value)")
            ->executeGetRowCount($this->getConnection())
        ;
    }

    /**
     * @param int $printerId
     * @return int
     */
    public function &deleteAttributes($printerId) {
        return PDOQuery::delete()
            ->from($this)
            ->where("printer_id", $printerId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
