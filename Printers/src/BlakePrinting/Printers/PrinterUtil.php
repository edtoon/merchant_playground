<?php

namespace BlakePrinting\Printers;

use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Users\UserDAO;
use BlakePrinting\Users\Entities\UserFacilityDAO;
use PDO;
use ReflectionClass;

class PrinterUtil {
    private function __construct() { }

    /**
     * @param int $userId
     * @param string|string[] $orderBy
     * @param string|string[]|int|int[] $limit
     * @param string|string[] $options
     * @return PrinterVO[]
     */
    public static function &getUserFacilityPrinters($userId, PDO $connection = null, $orderBy = null, $limit = null, $options = null) {
        return PrinterDAO::instance($connection)->findAll(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$userId, &$connection) {
                $gatewayPrinterDao =& GatewayPrinterDAO::instance($connection);
                $gatewayDao =& GatewayDAO::instance($connection);
                $userFacilityDao =& UserFacilityDAO::instance($connection);
                $userDao =& UserDAO::instance($connection);
                $pdoQuery
                    ->columns("printer.*")
                    ->join($gatewayPrinterDao, ["printer.id = gateway_printer.printer_id"])
                    ->join($gatewayDao, ["gateway_printer.gateway_id = gateway.id"])
                    ->join($userFacilityDao, ["gateway.facility_id = user_facility.facility_id"])
                    ->join($userDao, ["user.id = user_facility.user_id"])
                    ->where("user.id", $userId, PDO::PARAM_INT)
                ;
            },
            $orderBy, $limit, $options
        );
    }
}
