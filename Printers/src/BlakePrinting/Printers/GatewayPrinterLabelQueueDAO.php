<?php

namespace BlakePrinting\Printers;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static GatewayPrinterLabelQueueDAO instance(PDO $connection = null, $schemaName = null)
 * @method GatewayPrinterLabelQueueVO findById($id, $options = null, $useCache = false)
 * @method GatewayPrinterLabelQueueVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method GatewayPrinterLabelQueueVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method GatewayPrinterLabelQueueVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method GatewayPrinterLabelQueueVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method GatewayPrinterLabelQueueVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method GatewayPrinterLabelQueueVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class GatewayPrinterLabelQueueDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $gatewayId
     * @param int $printerId
     * @param int $labelId
     * @param bool $completed
     * @param bool $cancelled
     * @return int
     */
    public function &store($gatewayId, $printerId, $labelId, $completed = false, $cancelled = false) {
        $insert =& PDOQuery::insert()
            ->into($this)
            ->value("gateway_id", $gatewayId, PDO::PARAM_INT)
            ->value("printer_id", $printerId, PDO::PARAM_INT)
            ->value("label_id", $labelId, PDO::PARAM_INT)
            ->value("created", "UNIX_TIMESTAMP()")
            ->onDuplicateValue("updated", "UNIX_TIMESTAMP()")
        ;

        if($completed) {
            $insert
                ->value("completed", "UNIX_TIMESTAMP()")
                ->onDuplicateValue("completed", "UNIX_TIMESTAMP()")
            ;
        }

        if($cancelled) {
            $insert
                ->value("cancelled", "UNIX_TIMESTAMP()")
                ->onDuplicateValue("cancelled", "UNIX_TIMESTAMP()")
            ;
        }

        return $insert->executeGetRowCount($this->getConnection());
    }
}
