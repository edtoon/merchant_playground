<?php

namespace BlakePrinting\Printers;

use BlakePrinting\Db\ValueObject;
use JsonSerializable;

class GatewayPrinterVO implements ValueObject, JsonSerializable {
    /** @var int */
    private $gatewayId = null;
    /** @var int */
    private $printerId = null;
    /** @var int */
    private $created = null;

    public function jsonSerialize() {
        return [
            "gateway_id" => $this->gatewayId,
            "printer_id" => $this->printerId,
            "created" => $this->created
        ];
    }

    /**
     * @return int
     */
    public function getGatewayId()
    {
        return $this->gatewayId;
    }

    /**
     * @param int $gatewayId
     */
    public function setGatewayId($gatewayId)
    {
        $this->gatewayId = $gatewayId;
    }

    /**
     * @return int
     */
    public function getPrinterId()
    {
        return $this->printerId;
    }

    /**
     * @param int $printerId
     */
    public function setPrinterId($printerId)
    {
        $this->printerId = $printerId;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }
}
