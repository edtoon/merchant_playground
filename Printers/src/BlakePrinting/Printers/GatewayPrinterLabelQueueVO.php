<?php

namespace BlakePrinting\Printers;

use BlakePrinting\Db\ValueObject;
use JsonSerializable;

class GatewayPrinterLabelQueueVO implements JsonSerializable, ValueObject {
    /** @var int */
    private $gatewayId = null;
    /** @var int */
    private $printerId = null;
    /** @var int */
    private $labelId = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $completed = null;
    /** @var int */
    private $cancelled = null;

    public function jsonSerialize() {
        return [
            "gateway_id" => $this->gatewayId,
            "printer_id" => $this->printerId,
            "label_id" => $this->labelId,
            "created" => $this->created,
            "updated" => $this->updated,
            "completed" => $this->completed,
            "cancelled" => $this->cancelled
        ];
    }

    /**
     * @return int
     */
    public function getGatewayId()
    {
        return $this->gatewayId;
    }

    /**
     * @param int $gatewayId
     */
    public function setGatewayId($gatewayId)
    {
        $this->gatewayId = $gatewayId;
    }

    /**
     * @return int
     */
    public function getPrinterId()
    {
        return $this->printerId;
    }

    /**
     * @param int $printerId
     */
    public function setPrinterId($printerId)
    {
        $this->printerId = $printerId;
    }

    /**
     * @return int
     */
    public function getLabelId()
    {
        return $this->labelId;
    }

    /**
     * @param int $labelId
     */
    public function setLabelId($labelId)
    {
        $this->labelId = $labelId;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getCompleted()
    {
        return $this->completed;
    }

    /**
     * @param int $completed
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;
    }

    /**
     * @return int
     */
    public function getCancelled()
    {
        return $this->cancelled;
    }

    /**
     * @param int $cancelled
     */
    public function setCancelled($cancelled)
    {
        $this->cancelled = $cancelled;
    }
}
