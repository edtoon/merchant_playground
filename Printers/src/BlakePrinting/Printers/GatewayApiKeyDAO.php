<?php

namespace BlakePrinting\Printers;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static GatewayApiKeyDAO instance(PDO $connection = null, $schemaName = null)
 * @method GatewayApiKeyVO findById($id, $options = null, $useCache = false)
 * @method GatewayApiKeyVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method GatewayApiKeyVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method GatewayApiKeyVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method GatewayApiKeyVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method GatewayApiKeyVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method GatewayApiKeyVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method GatewayApiKeyVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class GatewayApiKeyDAO extends AbstractDataAccessObject {
}
