<?php

namespace BlakePrinting\Printers;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static PrinterDAO instance(PDO $connection = null, $schemaName = null)
 * @method PrinterVO findById($id, $options = null, $useCache = false)
 * @method PrinterVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method PrinterVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method PrinterVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method PrinterVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method PrinterVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method PrinterVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class PrinterDAO extends AbstractDataAccessObject {
}
