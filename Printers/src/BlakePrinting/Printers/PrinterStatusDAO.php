<?php

namespace BlakePrinting\Printers;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static PrinterStatusDAO instance(PDO $connection = null, $schemaName = null)
 * @method PrinterStatusVO findById($id, $options = null, $useCache = false)
 * @method PrinterStatusVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method PrinterStatusVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method PrinterStatusVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method PrinterStatusVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method PrinterStatusVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method PrinterStatusVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class PrinterStatusDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, "printer_id");
    }

    /**
     * @param int $printerId
     * @param int $locationId
     * @param string $statusDescription
     * @param bool $useNetwork
     * @param bool $ready
     * @return int
     */
    public function &store($printerId, $locationId, $statusDescription, $useNetwork, $ready) {
        return PDOQuery::insert()
            ->into($this)
            ->value("printer_id", $printerId, PDO::PARAM_INT)
            ->value("location_id", $locationId, PDO::PARAM_INT)
            ->value("status_description", $statusDescription, PDO::PARAM_STR)
            ->value("use_network", $useNetwork, PDO::PARAM_BOOL)
            ->value("ready", $ready, PDO::PARAM_BOOL)
            ->onDuplicateValue("updated", "UNIX_TIMESTAMP()")
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
