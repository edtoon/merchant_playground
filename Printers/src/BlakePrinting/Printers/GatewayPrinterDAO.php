<?php

namespace BlakePrinting\Printers;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static GatewayPrinterDAO instance(PDO $connection = null, $schemaName = null)
 * @method GatewayPrinterVO findById($id, $options = null, $useCache = false)
 * @method GatewayPrinterVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method GatewayPrinterVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method GatewayPrinterVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method GatewayPrinterVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method GatewayPrinterVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method GatewayPrinterVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class GatewayPrinterDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $gatewayId
     * @param int $printerId
     * @return int
     */
    public function &store($gatewayId, $printerId) {
        $this->deleteOtherGateways($printerId, $gatewayId);

        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value("gateway_id", $gatewayId, PDO::PARAM_INT)
            ->value("printer_id", $printerId, PDO::PARAM_INT)
            ->value("created", "UNIX_TIMESTAMP()")
            ->executeGetRowCount($this->getConnection())
        ;
    }

    /**
     * @param int $gatewayId
     * @return int
     */
    public function &deletePrintersNotIn($gatewayId, array $printerIds = []) {
        $delete =& PDOQuery::delete()
            ->from($this)
            ->where("gateway_id", $gatewayId, PDO::PARAM_INT)
        ;

        if(!empty($printerIds)) {
            $delete->where("printer_id NOT IN (" . implode(",", $printerIds) . ")");
        }

        return $delete->executeGetRowCount($this->getConnection());
    }

    /**
     * @param int $printerId
     * @param int $gatewayId
     * @return int
     */
    public function &deleteOtherGateways($printerId, $gatewayId) {
        return PDOQuery::delete()
            ->from($this)
            ->where("printer_id", $printerId, PDO::PARAM_INT)
            ->where("gateway_id", $gatewayId, PDO::PARAM_INT, "<>")
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
