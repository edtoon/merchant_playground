<?php

namespace BlakePrinting\Printers;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static GatewayDAO instance(PDO $connection = null, $schemaName = null)
 * @method GatewayVO findById($id, $options = null, $useCache = false)
 * @method GatewayVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method GatewayVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method GatewayVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method GatewayVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method GatewayVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method GatewayVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class GatewayDAO extends AbstractDataAccessObject {
}
