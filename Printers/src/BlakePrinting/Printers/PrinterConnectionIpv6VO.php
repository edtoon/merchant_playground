<?php

namespace BlakePrinting\Printers;

use BlakePrinting\Db\ValueObject;
use JsonSerializable;

class PrinterConnectionIpv6VO implements ValueObject, JsonSerializable {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $printerId = null;
    /** @var int */
    private $port = null;
    /** @var string */
    private $ipv6 = null;
    /** @var string */
    private $defaultGateway = null;
    /** @var int */
    private $prefixLength = null;

    public function jsonSerialize() {
        return [
            "id" => $this->id,
            "created" => $this->created,
            "updated" => $this->updated,
            "printer_id" => $this->printerId,
            "port" => $this->port,
            "ipv6" => $this->ipv6,
            "default_gateway" => $this->defaultGateway,
            "prefix_length" => $this->prefixLength
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getPrinterId()
    {
        return $this->printerId;
    }

    /**
     * @param int $printerId
     */
    public function setPrinterId($printerId)
    {
        $this->printerId = $printerId;
    }

    /**
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param int $port
     */
    public function setPort($port)
    {
        $this->port = $port;
    }

    /**
     * @return string
     */
    public function getIpv6()
    {
        return $this->ipv6;
    }

    /**
     * @param string $ipv6
     */
    public function setIpv6($ipv6)
    {
        $this->ipv6 = $ipv6;
    }

    /**
     * @return string
     */
    public function getDefaultGateway()
    {
        return $this->defaultGateway;
    }

    /**
     * @param string $defaultGateway
     */
    public function setDefaultGateway($defaultGateway)
    {
        $this->defaultGateway = $defaultGateway;
    }

    /**
     * @return int
     */
    public function getPrefixLength()
    {
        return $this->prefixLength;
    }

    /**
     * @param int $prefixLength
     */
    public function setPrefixLength($prefixLength)
    {
        $this->prefixLength = $prefixLength;
    }
}
