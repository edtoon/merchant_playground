<?php

namespace BlakePrinting\Printers;

use BlakePrinting\Db\ValueObject;
use JsonSerializable;

class PrinterVO implements ValueObject, JsonSerializable {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $ownerOrganizationId = null;
    /** @var int */
    private $manufacturerOrganizationId = null;
    /** @var string */
    private $productName = null;
    /** @var string */
    private $productNumber = null;
    /** @var string */
    private $serialNumber = null;
    /** @var string */
    private $systemName = null;

    public function jsonSerialize() {
        return [
            "id" => $this->id,
            "created" => $this->created,
            "updated" => $this->updated,
            "owner_organization_id" => $this->ownerOrganizationId,
            "manufacturer_organization_id" => $this->manufacturerOrganizationId,
            "product_name" => $this->productName,
            "product_number" => $this->productNumber,
            "serial_number" => $this->serialNumber,
            "system_name" => $this->systemName
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getOwnerOrganizationId()
    {
        return $this->ownerOrganizationId;
    }

    /**
     * @param int $ownerOrganizationId
     */
    public function setOwnerOrganizationId($ownerOrganizationId)
    {
        $this->ownerOrganizationId = $ownerOrganizationId;
    }

    /**
     * @return int
     */
    public function getManufacturerOrganizationId()
    {
        return $this->manufacturerOrganizationId;
    }

    /**
     * @param int $manufacturerOrganizationId
     */
    public function setManufacturerOrganizationId($manufacturerOrganizationId)
    {
        $this->manufacturerOrganizationId = $manufacturerOrganizationId;
    }

    /**
     * @return string
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * @param string $productName
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;
    }

    /**
     * @return string
     */
    public function getProductNumber()
    {
        return $this->productNumber;
    }

    /**
     * @param string $productNumber
     */
    public function setProductNumber($productNumber)
    {
        $this->productNumber = $productNumber;
    }

    /**
     * @return string
     */
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * @param string $serialNumber
     */
    public function setSerialNumber($serialNumber)
    {
        $this->serialNumber = $serialNumber;
    }

    /**
     * @return string
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

    /**
     * @param string $systemName
     */
    public function setSystemName($systemName)
    {
        $this->systemName = $systemName;
    }
}
