<?php

namespace BlakePrinting\Printers;

use BlakePrinting\Db\ValueObject;
use JsonSerializable;

class PrinterStatusVO implements ValueObject, JsonSerializable {
    /** @var int */
    private $printerId = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $locationId = null;
    /** @var string */
    private $statusDescription = null;
    /** @var bool */
    private $useNetwork = null;
    /** @var bool */
    private $ready = null;

    public function jsonSerialize() {
        return [
            "printer_id" => $this->printerId,
            "updated" => $this->updated,
            "location_id" => $this->locationId,
            "status_description" => $this->statusDescription,
            "use_network" => $this->useNetwork,
            "ready" => $this->ready
        ];
    }

    /**
     * @return int
     */
    public function getPrinterId()
    {
        return $this->printerId;
    }

    /**
     * @param int $printerId
     */
    public function setPrinterId($printerId)
    {
        $this->printerId = $printerId;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * @param int $locationId
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;
    }

    /**
     * @return string
     */
    public function getStatusDescription()
    {
        return $this->statusDescription;
    }

    /**
     * @param string $statusDescription
     */
    public function setStatusDescription($statusDescription)
    {
        $this->statusDescription = $statusDescription;
    }

    /**
     * @return boolean
     */
    public function isUseNetwork()
    {
        return $this->useNetwork;
    }

    /**
     * @param boolean $useNetwork
     */
    public function setUseNetwork($useNetwork)
    {
        $this->useNetwork = $useNetwork;
    }

    /**
     * @return boolean
     */
    public function isReady()
    {
        return $this->ready;
    }

    /**
     * @param boolean $ready
     */
    public function setReady($ready)
    {
        $this->ready = $ready;
    }
}
