<?php

namespace BlakePrinting\Printers;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static PrinterConnectionIpv4DAO instance(PDO $connection = null, $schemaName = null)
 * @method PrinterConnectionIpv4VO findById($id, $options = null, $useCache = false)
 * @method PrinterConnectionIpv4VO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method PrinterConnectionIpv4VO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method PrinterConnectionIpv4VO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method PrinterConnectionIpv4VO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method PrinterConnectionIpv4VO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method PrinterConnectionIpv4VO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class PrinterConnectionIpv4DAO extends AbstractDataAccessObject {
    /**
     * @param int $id
     * @return int
     */
    public function &delete($id) {
        return PDOQuery::delete()
            ->from($this)
            ->where("id", $id, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }

    /**
     * @param int $printerId
     * @param int $port
     * @param string $ipv4
     * @param string $defaultGateway
     * @param string $subnetMask
     * @return int
     */
    public function create($printerId, $port, $ipv4, $defaultGateway = null, $subnetMask = null) {
        $insert =& PDOQuery::insert()
            ->into($this)
            ->value("created", "UNIX_TIMESTAMP()")
            ->value("printer_id", $printerId, PDO::PARAM_INT)
            ->value("port", $port, PDO::PARAM_INT)
            ->value("ipv4", $ipv4, PDO::PARAM_STR, "INET6_ATON")
        ;

        if($defaultGateway !== null) {
            $insert->value("default_gateway", $defaultGateway, PDO::PARAM_STR, "INET6_ATON");
        }

        if($subnetMask !== null) {
            $insert->value("subnet_mask", $subnetMask, PDO::PARAM_STR, "INET6_ATON");
        }

        return $insert->executeEnsuringLastInsertId($this->getConnection());
    }
}
