<?php

namespace BlakePrinting\Printers;

use BlakePrinting\Db\ValueObject;
use JsonSerializable;

class PrinterAttributeVO implements JsonSerializable, ValueObject {
    /** @var int */
    private $printerId = null;
    /** @var string */
    private $name = null;
    /** @var string */
    private $value = null;

    public function jsonSerialize() {
        return [
            "printer_id" => $this->printerId,
            "name" => $this->name,
            "value" => $this->value
        ];
    }

    /**
     * @return int
     */
    public function getPrinterId()
    {
        return $this->printerId;
    }

    /**
     * @param int $printerId
     */
    public function setPrinterId($printerId)
    {
        $this->printerId = $printerId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}
