<?php

namespace BlakePrinting\AmazonMws\Orders\CLI;

use BlakePrinting\AmazonMws\AddressDAO;
use BlakePrinting\AmazonMws\AddressVO;
use BlakePrinting\AmazonMws\Orders\OrdersClientFactory;
use BlakePrinting\AmazonMws\Sellers\AmazonMwsUtil;
use BlakePrinting\AmazonMws\Sellers\CredentialVO;
use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceVO;
use BlakePrinting\CLI\CLICommand;
use BlakePrinting\Db\DbUtil;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use Monolog\Logger;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use MarketplaceWebServiceOrders_Model_ListOrdersByNextTokenRequest;
use MarketplaceWebServiceOrders_Model_ListOrdersRequest;
use MarketplaceWebServiceOrders_Model_Order;
use MarketplaceWebServiceOrders_Exception;
use DateTime;
use Exception;
use PDO;

class ListOrdersCommand extends CLICommand {
    const FIRST_EXTRACT_MONTH = 1;
    const FIRST_EXTRACT_DAY = 1;
    const FIRST_EXTRACT_YEAR = 2016;

    private $connection = null;

    protected function configure() {
        $this
            ->setName("amazon:mws:orders:list-orders")
            ->setDescription("List orders")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->connection =& PDOUtil::getMysqlConnection();

        try {
            AmazonMwsUtil::forEachActiveMarketplace(function(MarketplaceInstanceVO &$marketplaceInstanceVo, CredentialVO $credentialVo, PDO $connection) {
                $this->connection =& $connection;

                $this->listOrdersForInstanceAndConfig($marketplaceInstanceVo, $credentialVo);

                PDOUtil::commitConnection($this->connection);
            }, function(PDO $connection) {
                PDOUtil::rollbackConnection($connection);
            });

            return true;
        } catch(MarketplaceWebServiceOrders_Exception $e) {
            LogUtil::logException(Logger::ERROR, $e,
                "MWS service call failed with status code: " . $e->getStatusCode() .
                ", error code: " . $e->getErrorCode() .
                ", error type: " . $e->getErrorType() .
                ", request ID: " . $e->getRequestId() .
                ", responseHeaderMetadata: " . $e->getResponseHeaderMetadata() .
                ", xml: " . $e->getXML()
            );
        }

        return false;
    }

    private function listOrdersForInstanceAndConfig(MarketplaceInstanceVO &$marketplaceInstanceVo, CredentialVO &$credentialVo) {
        $logger =& LogUtil::getLogger();
        $marketplaceInstanceId = $marketplaceInstanceVo->getId();
        $orderListResult =& PDOQuery::select()->columns("last_updated_after", "next_token")
            ->from("amazon_mws.order_list")
            ->where("marketplace_instance_id", $marketplaceInstanceId, PDO::PARAM_INT)
            ->getSingleRow($this->connection);
        ;
        $orderListResultIsEmpty = empty($orderListResult);
        $lastUpdatedAfter = ($orderListResultIsEmpty === true ? null : $orderListResult["last_updated_after"]);
        $nextToken = ($orderListResultIsEmpty === true ? null : $orderListResult["next_token"]);
        $ordersClientFactory = new OrdersClientFactory();
        $ordersClient =& $ordersClientFactory->createClient(Main::APP_NAME, Main::APP_VERSION, $credentialVo, $this->connection);
        $storedOrderCount = 0;
        $response = null;
        $result = null;

        if(!empty($lastUpdatedAfter)) {
            $lastUpdatedAfter = DateTime::createFromFormat(DbUtil::MYSQL_DATE_FORMAT, $lastUpdatedAfter)->format(DateTime::ATOM);
        }

        if(empty($nextToken)) {
            $lastUpdateDate = (empty($lastUpdatedAfter) ? null :
                PDOQuery::select()->columns("last_update_date")->from("amazon_mws.orders")
                    ->where("marketplace_instance_id", $marketplaceInstanceId, PDO::PARAM_INT)
                    ->orderBy("id desc")->limit(1)->getSingleValue($this->connection)
            );
            $dt = null;

            if(empty($lastUpdateDate)) {
                $dt = new DateTime("@" . mktime(0, 0, 0, self::FIRST_EXTRACT_MONTH, self::FIRST_EXTRACT_DAY, self::FIRST_EXTRACT_YEAR));
            } else {
                $dt = DateTime::createFromFormat(DbUtil::MYSQL_DATE_FORMAT, $lastUpdateDate);
            }

            $lastUpdatedAfter = $dt->format(DateTime::ATOM);

            $logger->debug("Listing orders updated after: " . $lastUpdatedAfter);

            $request = new MarketplaceWebServiceOrders_Model_ListOrdersRequest();
            $request->setSellerId($credentialVo->getSellerId());
            $request->setMarketplaceId($marketplaceInstanceVo->getMwsMarketplaceId());
            $request->setLastUpdatedAfter($lastUpdatedAfter);
            $response = $ordersClient->listOrders($request);
            $result = $response->getListOrdersResult();
        } else {
            $logger->debug("Listing orders by next token: " . $nextToken);

            $request = new MarketplaceWebServiceOrders_Model_ListOrdersByNextTokenRequest();
            $request->setSellerId($credentialVo->getSellerId());
            $request->setNextToken($nextToken);
            $response = $ordersClient->listOrdersByNextToken($request);
            $result = $response->getListOrdersByNextTokenResult();
        }

        AmazonMwsUtil::logResponse($response);

        if(empty($result)) {
            $logger->critical("No result received - seller id: " . $credentialVo->getSellerId() . ", MWS marketplace id: " . $marketplaceInstanceVo->getMwsMarketplaceId() . ", next token: " . $nextToken . ", response: " . Utils::getVarDump($response));
        } else {
            $nextToken = $result->getNextToken();
            $createdBefore = $result->getCreatedBefore();
            $lastUpdatedBefore = $result->getLastUpdatedBefore();
            $orders = $result->getOrders();

            if(!empty($nextToken)) {
                $logger->debug("Next token: " . $nextToken);
            }

            if(!empty($createdBefore)) {
                $logger->debug("Created before: " . $createdBefore);
            }

            if(!empty($lastUpdatedBefore)) {
                $logger->debug("Last updated before: " . $lastUpdatedBefore);
            }

            foreach($orders as &$order) {
                $logger->debug("-----------------------------------------------------------------------------");

                if($this->storeOrder($marketplaceInstanceVo, $order)) {
                    $storedOrderCount++;
                }
            }

            unset($order);

            $rowCount =& PDOQuery::insert()
                ->into("amazon_mws.order_list")
                ->value("marketplace_instance_id", $marketplaceInstanceId, PDO::PARAM_INT)
                ->value("last_updated_after", $lastUpdatedAfter, PDO::PARAM_STR, DbUtil::formatFunction(DbUtil::FUNC_FROM_ISO8601_SUBSET))
                ->value("next_token", $nextToken, PDO::PARAM_STR)
                ->value("response_header_metadata", $response->getResponseHeaderMetadata(), PDO::PARAM_STR)
                ->onDuplicateValue("last_updated_after", "VALUES(last_updated_after)")
                ->onDuplicateValue("next_token", "VALUES(next_token)")
                ->onDuplicateValue("response_header_metadata", "VALUES(response_header_metadata)")
                ->executeGetRowCount($this->connection)
            ;

            if($rowCount < 1) {
                $logger->critical("Failed to update Amazon order extract record for seller id: " . $credentialVo->getSellerId() . ", MWS marketplace id: " . $marketplaceInstanceVo->getMwsMarketplaceId() . ", last updated after: " . $lastUpdatedAfter . ", next token: " . $nextToken);
            } else {
                $logger->debug(($rowCount > 1 ? "Updated" : "Inserted") . " Amazon order extract record for seller id: " . $credentialVo->getSellerId() . ", MWS marketplace id: " . $marketplaceInstanceVo->getMwsMarketplaceId() . ", last updated after: " . $lastUpdatedAfter . ", next token: " . $nextToken);

                PDOUtil::commitConnection($this->connection);
            }
        }

        $logger->debug("=============================================================================");
        $logger->log(
            ($storedOrderCount > 0 ? Logger::INFO : Logger::DEBUG),
            "Stored " . $storedOrderCount . " order changes"
        );
    }

    private function storeOrder(MarketplaceInstanceVO &$marketplaceInstanceVo, MarketplaceWebServiceOrders_Model_Order &$order) {
        $logger =& LogUtil::getLogger();
        $marketplaceInstanceId = $marketplaceInstanceVo->getId();
        $amazonOrderId = $order->getAmazonOrderId();
        $lastUpdateDate = $order->getLastUpdateDate();
        $orderStatus = $order->getOrderStatus();

        $logger->debug("Storing order xml: " . $order->_toXMLFragment());

        try {
            if(empty($amazonOrderId)) {
                $logger->alert("No Amazon order ID set");
                return false;
            }

            $missingRequiredFields = [];

            if(empty($order->getPurchaseDate())) {
                $missingRequiredFields[] = "purchase date";
            }

            if(empty($lastUpdateDate)) {
                $missingRequiredFields[] = "last update date";
            }

            if(empty($orderStatus)) {
                $missingRequiredFields[] = "order status";
            }

            if($order->getIsPrime() === null) {
                $missingRequiredFields[] = "prime flag";
            }

            if($order->getIsPremiumOrder() === null) {
                $missingRequiredFields[] = "premium order flag";
            }

            if(!empty($missingRequiredFields)) {
                $logger->critical("Amazon order #" . $amazonOrderId . " is missing required fields: " . implode(", ", $missingRequiredFields));
                return false;
            }

            if($marketplaceInstanceVo->getMwsMarketplaceId() != $order->getMarketplaceId()) {
                throw new Exception("Order marketplace id: " . $order->getMarketplaceId() . " did not match instance MWS marketplace id: " . $marketplaceInstanceVo->getMwsMarketplaceId());
            }

            $shippingAddressVo =& $this->getShippingAddressFromOrder($order);
            $shippingAddressId = ($shippingAddressVo === null ? null : $shippingAddressVo->getId());
            $orderResult =& PDOQuery::select()->columns("id", "last_update_date")
                ->from("amazon_mws.orders")
                ->where("marketplace_instance_id", $marketplaceInstanceId, PDO::PARAM_INT)
                ->where("amazon_order_id", $amazonOrderId, PDO::PARAM_STR)
                ->options("FOR UPDATE")
                ->getSingleRow($this->connection)
            ;
            $orderResultIsEmpty = empty($orderResult);
            $orderId = ($orderResultIsEmpty === true ? null : $orderResult["id"]);
            $existingLastUpdateDate = ($orderResultIsEmpty === true ? null : $orderResult["last_update_date"]);
            $existingLastUpdateTime = (empty($existingLastUpdateDate) ? null : DateTime::createFromFormat(DbUtil::MYSQL_DATE_FORMAT, $existingLastUpdateDate)->getTimestamp());
            $lastUpdateTime = (empty($lastUpdateDate) ? null : DateTime::createFromFormat(DateTime::ATOM, $lastUpdateDate)->getTimestamp());

            if(empty($orderId) || empty($lastUpdateTime) || empty($existingLastUpdateTime) || $lastUpdateTime > $existingLastUpdateTime) {
                $orderTotal = $order->getOrderTotal();
                $orderTotalCurrencyCode = ($orderTotal === null ? null : $orderTotal->getCurrencyCode());
                $orderTotalAmount = ($orderTotal === null ? null : $orderTotal->getAmount());
                $fromIso8601SubsetFunc = DbUtil::formatFunction(DbUtil::FUNC_FROM_ISO8601_SUBSET);
                $orderId =& PDOQuery::insert()
                    ->into("amazon_mws.orders")
                    ->value("marketplace_instance_id", $marketplaceInstanceId, PDO::PARAM_INT)
                    ->value("amazon_order_id", $amazonOrderId, PDO::PARAM_STR)
                    ->value("seller_order_id", $order->getSellerOrderId(), PDO::PARAM_STR)
                    ->value("purchase_date", $order->getPurchaseDate(), PDO::PARAM_STR, $fromIso8601SubsetFunc)
                    ->value("last_update_date", $lastUpdateDate, PDO::PARAM_STR, $fromIso8601SubsetFunc)
                    ->value("order_status", $orderStatus, PDO::PARAM_STR)
                    ->value("fulfillment_channel", $order->getFulfillmentChannel(), PDO::PARAM_STR)
                    ->value("sales_channel", $order->getSalesChannel(), PDO::PARAM_STR)
                    ->value("order_channel", $order->getOrderChannel(), PDO::PARAM_STR)
                    ->value("ship_service_level", $order->getShipServiceLevel(), PDO::PARAM_STR)
                    ->value("address_id", $shippingAddressId, PDO::PARAM_INT)
                    ->value("order_total_currency_code", $orderTotalCurrencyCode, PDO::PARAM_STR)
                    ->value("order_total_amount", $orderTotalAmount, PDO::PARAM_STR)
                    ->value("number_of_items_shipped", $order->getNumberOfItemsShipped(), PDO::PARAM_INT)
                    ->value("number_of_items_unshipped", $order->getNumberOfItemsUnshipped(), PDO::PARAM_INT)
                    ->value("payment_method", $order->getPaymentMethod(), PDO::PARAM_STR)
                    ->value("buyer_email", $order->getBuyerEmail(), PDO::PARAM_STR)
                    ->value("buyer_name", $order->getBuyerName(), PDO::PARAM_STR)
                    ->value("shipment_service_level_category", $order->getShipmentServiceLevelCategory(), PDO::PARAM_STR)
                    ->value("shipped_by_amazon_tfm", $order->getShippedByAmazonTFM(), PDO::PARAM_BOOL)
                    ->value("tfm_shipment_status", $order->getTFMShipmentStatus(), PDO::PARAM_STR)
                    ->value("cba_displayable_shipping_label", $order->getCbaDisplayableShippingLabel(), PDO::PARAM_STR)
                    ->value("order_type", $order->getOrderType(), PDO::PARAM_STR)
                    ->value("earliest_ship_date", $order->getEarliestShipDate(), PDO::PARAM_STR, $fromIso8601SubsetFunc)
                    ->value("latest_ship_date", $order->getLatestShipDate(), PDO::PARAM_STR, $fromIso8601SubsetFunc)
                    ->value("earliest_delivery_date", $order->getEarliestDeliveryDate(), PDO::PARAM_STR, $fromIso8601SubsetFunc)
                    ->value("latest_delivery_date", $order->getLatestDeliveryDate(), PDO::PARAM_STR, $fromIso8601SubsetFunc)
                    ->value("business_order", $order->getIsBusinessOrder(), PDO::PARAM_BOOL)
                    ->value("purchase_order_number", $order->getPurchaseOrderNumber(), PDO::PARAM_STR)
                    ->value("prime", $order->getIsPrime(), PDO::PARAM_BOOL)
                    ->value("premium_order", $order->getIsPremiumOrder(), PDO::PARAM_BOOL)
                    ->onDuplicateValue("id", "LAST_INSERT_ID(id)")
                    ->onDuplicateValue("seller_order_id", "COALESCE(VALUES(seller_order_id), seller_order_id)")
                    ->onDuplicateValue("purchase_date", "COALESCE(VALUES(purchase_date), purchase_date)")
                    ->onDuplicateValue("last_update_date", "COALESCE(VALUES(last_update_date), last_update_date)")
                    ->onDuplicateValue("order_status", "COALESCE(VALUES(order_status), order_status)")
                    ->onDuplicateValue("fulfillment_channel", "COALESCE(VALUES(fulfillment_channel), fulfillment_channel)")
                    ->onDuplicateValue("sales_channel", "COALESCE(VALUES(sales_channel), sales_channel)")
                    ->onDuplicateValue("order_channel", "COALESCE(VALUES(order_channel), order_channel)")
                    ->onDuplicateValue("ship_service_level", "COALESCE(VALUES(ship_service_level), ship_service_level)")
                    ->onDuplicateValue("address_id", "COALESCE(VALUES(address_id), address_id)")
                    ->onDuplicateValue("order_total_currency_code", "COALESCE(VALUES(order_total_currency_code), order_total_currency_code)")
                    ->onDuplicateValue("order_total_amount", "COALESCE(VALUES(order_total_amount), order_total_amount)")
                    ->onDuplicateValue("number_of_items_shipped", "COALESCE(VALUES(number_of_items_shipped), number_of_items_shipped)")
                    ->onDuplicateValue("number_of_items_unshipped", "COALESCE(VALUES(number_of_items_unshipped), number_of_items_unshipped)")
                    ->onDuplicateValue("payment_method", "COALESCE(VALUES(payment_method), payment_method)")
                    ->onDuplicateValue("buyer_email", "COALESCE(VALUES(buyer_email), buyer_email)")
                    ->onDuplicateValue("buyer_name", "COALESCE(VALUES(buyer_name), buyer_name)")
                    ->onDuplicateValue("shipment_service_level_category", "COALESCE(VALUES(shipment_service_level_category), shipment_service_level_category)")
                    ->onDuplicateValue("shipped_by_amazon_tfm", "COALESCE(VALUES(shipped_by_amazon_tfm), shipped_by_amazon_tfm)")
                    ->onDuplicateValue("tfm_shipment_status", "COALESCE(VALUES(tfm_shipment_status), tfm_shipment_status)")
                    ->onDuplicateValue("cba_displayable_shipping_label", "COALESCE(VALUES(cba_displayable_shipping_label), cba_displayable_shipping_label)")
                    ->onDuplicateValue("order_type", "COALESCE(VALUES(order_type), order_type)")
                    ->onDuplicateValue("earliest_ship_date", "COALESCE(VALUES(earliest_ship_date), earliest_ship_date)")
                    ->onDuplicateValue("latest_ship_date", "COALESCE(VALUES(latest_ship_date), latest_ship_date)")
                    ->onDuplicateValue("earliest_delivery_date", "COALESCE(VALUES(earliest_delivery_date), earliest_delivery_date)")
                    ->onDuplicateValue("latest_delivery_date", "COALESCE(VALUES(latest_delivery_date), latest_delivery_date)")
                    ->onDuplicateValue("business_order", "COALESCE(VALUES(business_order), business_order)")
                    ->onDuplicateValue("purchase_order_number", "COALESCE(VALUES(purchase_order_number), purchase_order_number)")
                    ->onDuplicateValue("prime", "COALESCE(VALUES(prime), prime)")
                    ->onDuplicateValue("premium_order", "COALESCE(VALUES(premium_order), premium_order)")
                    ->executeEnsuringLastInsertId($this->connection);
                ;
            }

            $this->storeOrderPayments($orderId, $order);

            PDOUtil::commitConnection($this->connection);

            return true;
        } catch(Exception $e) {
            LogUtil::logException(Logger::EMERGENCY, $e,
                "Failed to store Amazon order id: " . $amazonOrderId . ", status: " . $orderStatus . ", last update date: " . $lastUpdateDate
            );
        }

        PDOUtil::rollbackConnection($this->connection);

        return false;
    }

    private function storeOrderPayments($orderId, MarketplaceWebServiceOrders_Model_Order &$order) {
        $logger =& LogUtil::getLogger();
        $existingPayments =& PDOQuery::select()->columns(["id", "method", "amount", "currency_code"])
            ->from("amazon_mws.order_payment")
            ->where("order_id", $orderId, PDO::PARAM_INT)
            ->options("FOR UPDATE")
            ->getResults($this->connection)
        ;

        foreach($order->getPaymentExecutionDetail() as &$paymentExecutionDetailItem) {
            $paymentObject = $paymentExecutionDetailItem->getPayment();
            $paymentMethod = $paymentExecutionDetailItem->getPaymentMethod();
            $paymentAmount = ($paymentObject === null ? null : $paymentObject->getAmount());
            $paymentCurrencyCode = ($paymentObject === null ? null : $paymentObject->getCurrencyCode());
            $paymentId = null;

            foreach($existingPayments as $existingPaymentIdx => &$existingPayment) {
                if(Utils::valuesMatch($existingPayment["method"], $paymentMethod) &&
                    Utils::valuesMatch($existingPayment["amount"], $paymentAmount) &&
                    Utils::valuesMatch($existingPayment["currency_code"], $paymentCurrencyCode)) {

                    $paymentId = $existingPayment["id"];

                    unset($existingPayments[$existingPaymentIdx]);

                    break;
                }
            }

            unset($existingPayment);

            if($paymentId === null) {
                PDOQuery::insert()
                    ->into("amazon_mws.order_payment")
                    ->value("order_id", $orderId, PDO::PARAM_INT)
                    ->value("method", $paymentMethod, PDO::PARAM_STR)
                    ->value("amount", $paymentAmount, PDO::PARAM_STR)
                    ->value("currency_code", $paymentCurrencyCode, PDO::PARAM_STR)
                    ->executeEnsuringRowCount($this->connection, 1)
                ;
            }
        }

        unset($paymentExecutionDetailItem);

        foreach($existingPayments as &$removedPayment) {
            $removedPaymentId = $removedPayment["id"];

            $logger->notice("Removing payment #" . $removedPaymentId . ", method: " . $removedPayment["method"] . ", amount: " . $removedPayment["amount"] . ", currency code: " . $removedPayment["currency_code"]);

            PDOQuery::delete()->from("amazon_mws.order_payment")->where("id", $removedPaymentId, PDO::PARAM_INT)->executeEnsuringRowCount($this->connection, 1);
        }

        unset($removedPayment);
    }

    private function &getShippingAddressFromOrder(MarketplaceWebServiceOrders_Model_Order &$order) {
        $shippingAddressObject = $order->getShippingAddress();
        $addressVo = null;

        if(!empty($shippingAddressObject)) {
            $addressDao =& AddressDAO::instance($this->connection);
            $addressProtoVo = new AddressVO();

            $addressProtoVo->setName($shippingAddressObject->getName());
            $addressProtoVo->setLine1($shippingAddressObject->getAddressLine1());
            $addressProtoVo->setLine2($shippingAddressObject->getAddressLine2());
            $addressProtoVo->setLine3($shippingAddressObject->getAddressLine3());
            $addressProtoVo->setCity($shippingAddressObject->getCity());
            $addressProtoVo->setDistrict($shippingAddressObject->getDistrict());
            $addressProtoVo->setStateOrRegion($shippingAddressObject->getStateOrRegion());
            $addressProtoVo->setPostalCode($shippingAddressObject->getPostalCode());
            $addressProtoVo->setCountryCode($shippingAddressObject->getCountryCode());
            $addressProtoVo->setPhone($shippingAddressObject->getPhone());
            $addressProtoVo->setSha256(AddressDAO::createSha256($addressProtoVo));

            $addressVo =& $addressDao->findByColumn("sha256", $addressProtoVo->getSha256(), PDO::PARAM_STR, null, null, "FOR UPDATE");

            if($addressVo === null) {
                $addressId =& $addressDao->insert($addressProtoVo);

                if($addressId !== null) {
                    $addressVo =& $addressProtoVo;

                    $addressVo->setId($addressId);
                }
            }
        }

        return $addressVo;
    }
}
