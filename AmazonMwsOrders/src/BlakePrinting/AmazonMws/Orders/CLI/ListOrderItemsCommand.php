<?php

namespace BlakePrinting\AmazonMws\Orders\CLI;

use BlakePrinting\AmazonMws\Orders\OrdersClientFactory;
use BlakePrinting\AmazonMws\Sellers\AmazonMwsUtil;
use BlakePrinting\AmazonMws\Sellers\CredentialVO;
use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceVO;
use BlakePrinting\CLI\CLICommand;
use BlakePrinting\Db\DbUtil;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use Monolog\Logger;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use MarketplaceWebServiceOrders_Model_ListOrderItemsByNextTokenRequest;
use MarketplaceWebServiceOrders_Model_ListOrderItemsByNextTokenResponse;
use MarketplaceWebServiceOrders_Model_ListOrderItemsByNextTokenResult;
use MarketplaceWebServiceOrders_Model_ListOrderItemsRequest;
use MarketplaceWebServiceOrders_Model_ListOrderItemsResponse;
use MarketplaceWebServiceOrders_Model_ListOrderItemsResult;
use MarketplaceWebServiceOrders_Model_OrderItem;
use MarketplaceWebServiceOrders_Exception;
use MarketplaceWebServiceOrders_Interface;
use DateTime;
use Exception;
use PDO;

class ListOrderItemsCommand extends CLICommand {
    private $connection = null;

    protected function configure() {
        $this
            ->setName("amazon:mws:orders:list-order-items")
            ->setDescription("List order items")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        AmazonMwsUtil::forEachActiveMarketplace(function(MarketplaceInstanceVO &$marketplaceInstanceVo, CredentialVO $credentialVo, PDO $connection) {
            $this->connection =& $connection;
            $this->listOrderItemsForInstanceAndCredential($marketplaceInstanceVo, $credentialVo);

            PDOUtil::commitConnection($connection);
        }, function(PDO $connection) {
            PDOUtil::rollbackConnection($connection);
        });

        return true;
    }

    private function listOrderItemsForInstanceAndCredential(MarketplaceInstanceVO &$marketplaceInstanceVo, CredentialVO &$credentialVo) {
        $logger =& LogUtil::getLogger();
        $marketplaceInstanceId = $marketplaceInstanceVo->getId();
        $ordersClientFactory = new OrdersClientFactory();
        $ordersClient =& $ordersClientFactory->createClient(Main::APP_NAME, Main::APP_VERSION, $credentialVo, $this->connection);
        $nextToken =& PDOQuery::select()->columns("next_token")
            ->from("amazon_mws.order_item_list")
            ->where("marketplace_instance_id", $marketplaceInstanceId, PDO::PARAM_INT)
            ->getSingleValue($this->connection);
        ;
        $processedNextTokenOrderCount = (empty($nextToken) ? 0 : $this->processOrderItemsByNextToken($marketplaceInstanceVo, $credentialVo, $ordersClient, $nextToken));

        if($processedNextTokenOrderCount > 0) {
            $logger->info("Extracted " . $processedNextTokenOrderCount . " order items from next token");

            return true;
        }

        $amazonOrderId = PDOQuery::select()->columns("amazon_order_id")
            ->from("amazon_mws.orders")
            ->where("marketplace_instance_id", $marketplaceInstanceId, PDO::PARAM_INT)
            ->where("NOT EXISTS (SELECT 1 FROM amazon_mws.order_item_extract WHERE order_id = id)")
            ->limit(1)->getSingleValue($this->connection)
        ;
        $processedPendingOrderCount = (empty($amazonOrderId) ? 0 : $this->processOrderItemsByAmazonOrderId($marketplaceInstanceVo, $credentialVo, $ordersClient, $amazonOrderId));

        if($processedPendingOrderCount > 0) {
            $logger->info("Extracted " . $processedPendingOrderCount . " new order items");

            return true;
        }

        $amazonOrderId = PDOQuery::select()->columns("amazon_order_id")
            ->from("amazon_mws.orders", "amazon_mws.order_item_extract")
            ->where("marketplace_instance_id", $marketplaceInstanceId, PDO::PARAM_INT)
            ->where("orders.id", "order_item_extract.order_id")
            ->whereNotNull("orders.updated")
            ->where(
                "COALESCE(order_item_extract.updated, order_item_extract.created)",
                "orders.updated",
                null, "<="
            )
            ->limit(1)->getSingleValue($this->connection)
        ;
        $processedUpdatedOrderCount = (empty($amazonOrderId) ? 0 : $this->processOrderItemsByAmazonOrderId($marketplaceInstanceVo, $credentialVo, $ordersClient, $amazonOrderId));

        if($processedUpdatedOrderCount > 0) {
            $logger->info("Extracted " . $processedUpdatedOrderCount . " updated order items");
        }

        return true;
    }

    private function processOrderItemsByNextToken(MarketplaceInstanceVO &$marketplaceInstanceVo, CredentialVO &$credentialVo, MarketplaceWebServiceOrders_Interface &$ordersClient, $nextToken) {
        $logger =& LogUtil::getLogger();

        $logger->debug("Listing order items by next token: " . $nextToken);

        $request = new MarketplaceWebServiceOrders_Model_ListOrderItemsByNextTokenRequest();
        $request->setSellerId($credentialVo->getSellerId());
        $request->setNextToken($nextToken);
        $response = null;

        try {
            $response = $ordersClient->listOrderItemsByNextToken($request);
        } catch(MarketplaceWebServiceOrders_Exception $e) {
            LogUtil::logException(Logger::ERROR, $e,
                "MWS service call failed with status code: " . $e->getStatusCode() .
                ", error code: " . $e->getErrorCode() .
                ", error type: " . $e->getErrorType() .
                ", request ID: " . $e->getRequestId() .
                ", responseHeaderMetadata: " . $e->getResponseHeaderMetadata() .
                ", xml: " . $e->getXML()
            );

            return 0;
        }

        AmazonMwsUtil::logResponse($response);

        $result = $response->getListOrderItemsByNextTokenResult();

        return $this->processOrderItemsResponse($marketplaceInstanceVo, $credentialVo, $response, $result);
    }

    private function processOrderItemsByAmazonOrderId(MarketplaceInstanceVO &$marketplaceInstanceVo, CredentialVO &$credentialVo, MarketplaceWebServiceOrders_Interface &$ordersClient, $amazonOrderId) {
        $logger =& LogUtil::getLogger();

        $logger->debug("Listing order items for Amazon order id: " . $amazonOrderId);

        $request = new MarketplaceWebServiceOrders_Model_ListOrderItemsRequest();
        $request->setSellerId($credentialVo->getSellerId());
        $request->setAmazonOrderId($amazonOrderId);
        $response = null;

        try {
            $response = $ordersClient->listOrderItems($request);
        } catch(MarketplaceWebServiceOrders_Exception $e) {
            LogUtil::logException(Logger::ERROR, $e,
                "MWS service call failed with status code: " . $e->getStatusCode() .
                ", error code: " . $e->getErrorCode() .
                ", error type: " . $e->getErrorType() .
                ", request ID: " . $e->getRequestId() .
                ", responseHeaderMetadata: " . $e->getResponseHeaderMetadata() .
                ", xml: " . $e->getXML()
            );

            return 0;
        }

        AmazonMwsUtil::logResponse($response);

        $result = $response->getListOrderItemsResult();

        return $this->processOrderItemsResponse($marketplaceInstanceVo, $credentialVo, $response, $result);
    }

    /**
     * @param MarketplaceWebServiceOrders_Model_ListOrderItemsResponse|MarketplaceWebServiceOrders_Model_ListOrderItemsByNextTokenResponse $response
     * @param MarketplaceWebServiceOrders_Model_ListOrderItemsResult|MarketplaceWebServiceOrders_Model_ListOrderItemsByNextTokenResult $result
     */
    private function processOrderItemsResponse(MarketplaceInstanceVO &$marketplaceInstanceVo, CredentialVO &$credentialVo, &$response, &$result) {
        $logger =& LogUtil::getLogger();

        if(empty($result)) {
            $logger->critical("No result received - merchant id: " . $credentialVo->getSellerId() . ", MWS marketplace id: " . $marketplaceInstanceVo->getMwsMarketplaceId() . ", response: " . Utils::getVarDump($response));

            return 0;
        }

        $nextToken = $result->getNextToken();
        $amazonOrderId = $result->getAmazonOrderId();
        $orderItems = $result->getOrderItems();

        if(!empty($nextToken)) {
            $logger->debug("Next token: " . $nextToken);
        }

        if(empty($amazonOrderId)) {
            $logger->critical("No Amazon order id received in result - merchant id: " . $credentialVo->getSellerId() . ", MWS marketplace id: " . $marketplaceInstanceVo->getMwsMarketplaceId() . ", next token: " . $nextToken . ", response: " . Utils::getVarDump($response));

            return 0;
        }

        $logger->debug("Amazon order id: " . $amazonOrderId);

        try {
            $storedOrderCount = $this->storeOrderItems($marketplaceInstanceVo, $credentialVo, $amazonOrderId, $orderItems);
            $rowCount =& PDOQuery::insert()
                ->into("amazon_mws.order_item_list")
                ->value("marketplace_instance_id", $marketplaceInstanceVo->getId(), PDO::PARAM_INT)
                ->value("next_token", $nextToken, PDO::PARAM_STR)
                ->value("response_header_metadata", $response->getResponseHeaderMetadata(), PDO::PARAM_STR)
                ->onDuplicateValue("next_token", "VALUES(next_token)")
                ->onDuplicateValue("response_header_metadata", "VALUES(response_header_metadata)")
                ->executeGetRowCount($this->connection)
            ;

            if($rowCount < 1) {
                $logger->emergency("Failed to update Amazon order item extract status record for merchant id: " . $credentialVo->getSellerId() . ", MWS marketplace id: " . $marketplaceInstanceVo->getMwsMarketplaceId() . ", next token: " . $nextToken);
            } else {
                $logger->debug(($rowCount > 1 ? "Updated" : "Inserted") . " Amazon order item extract status record for merchant id: " . $credentialVo->getSellerId() . ", MWS marketplace id: " . $marketplaceInstanceVo->getMwsMarketplaceId() . ", next token: " . $nextToken);

                PDOUtil::commitConnection($this->connection);

                return $storedOrderCount;
            }
        } catch(Exception $e) {
            LogUtil::logException(Logger::EMERGENCY, $e,
                "Failed to store order items for Amazon order id: " . $amazonOrderId
            );
        }

        PDOUtil::rollbackConnection($this->connection);

        return 0;
    }

    /**
     * @param MarketplaceWebServiceOrders_Model_OrderItem[] $orderItems
     */
    private function storeOrderItems(MarketplaceInstanceVO &$marketplaceInstanceVo, CredentialVO &$credentialVo, $amazonOrderId, &$orderItems) {
        $logger =& LogUtil::getLogger();
        $orderId = PDOQuery::select()->columns("id")->from("amazon_mws.orders")
            ->where("marketplace_instance_id", $marketplaceInstanceVo->getId(), PDO::PARAM_INT)
            ->where("amazon_order_id", $amazonOrderId, PDO::PARAM_STR)
            ->getSingleValue($this->connection)
        ;

        if(empty($orderId)) {
            $logger->alert("Could not find entry for Amazon order id: " . $amazonOrderId . ", merchant id: " . $credentialVo->getSellerId() . ", MWS marketplace id: " . $marketplaceInstanceVo->getMwsMarketplaceId());

            return 0;
        }

        $existingOrderItems =& PDOQuery::select()
            ->from("amazon_mws.order_item")
            ->where("order_id", $orderId, PDO::PARAM_INT)
            ->options("FOR UPDATE")
            ->getResults($this->connection)
        ;
        $orderItemsUpdatedCount = 0;

        foreach($orderItems as &$orderItem) {
            $logger->debug("-----------------------------------------------------------------------------");

            $orderItemXml = $orderItem->_toXMLFragment();
            $asin = $orderItem->getASIN();
            $mwsOrderItemId = $orderItem->getOrderItemId();
            $priceObject = $orderItem->getItemPrice();

            if(empty($asin)) {
                $logger->alert("Missing ASIN in order item results for Amazon order id: " . $amazonOrderId . ", merchant id: " . $credentialVo->getSellerId() . ", MWS marketplace id: " . $marketplaceInstanceVo->getMwsMarketplaceId() . ", xml: " . $orderItemXml);

                continue;
            }

            if(empty($mwsOrderItemId)) {
                $logger->alert("Missing MWS order item id in order item results for Amazon order id: " . $amazonOrderId . ", merchant id: " . $credentialVo->getSellerId() . ", MWS marketplace id: " . $marketplaceInstanceVo->getMwsMarketplaceId() . ", xml: " . $orderItemXml);

                continue;
            }

            if(empty($priceObject)) {
                $logger->warn("Missing price in order item results for Amazon order id: " . $amazonOrderId . ", merchant id: " . $credentialVo->getSellerId() . ", MWS marketplace id: " . $marketplaceInstanceVo->getMwsMarketplaceId() . ", xml " . $orderItemXml);

                continue;
            }

            $logger->debug("Storing order item: " . $orderItem->_toXMLFragment());

            $sellerSku = $orderItem->getSellerSKU();
            $buyerCustomizedInfo = $orderItem->getBuyerCustomizedInfo();
            $customizedUrl = ($buyerCustomizedInfo === null ? null : $buyerCustomizedInfo->getCustomizedURL());
            $title = $orderItem->getTitle();
            $quantityOrdered = $orderItem->getQuantityOrdered();
            $quantityShipped = $orderItem->getQuantityShipped();
            $pointsGrantedObject = $orderItem->getPointsGranted();
            $pointsGranted = ($pointsGrantedObject === null ? null : $pointsGrantedObject->getPointsNumber());
            $pointsMonetaryValue = ($pointsGrantedObject === null ? null : $pointsGrantedObject->getPointsMonetaryValue());
            $pointsCurrencyCode = ($pointsMonetaryValue === null ? null : $pointsMonetaryValue->getCurrencyCode());
            $pointsAmount = ($pointsMonetaryValue === null ? null : $pointsMonetaryValue->getAmount());
            $priceCurrencyCode = ($priceObject === null ? null : $priceObject->getCurrencyCode());
            $priceAmount = ($priceObject === null ? null : $priceObject->getAmount());
            $shippingPriceObject = $orderItem->getShippingPrice();
            $shippingPriceCurrencyCode = ($shippingPriceObject === null ? null : $shippingPriceObject->getCurrencyCode());
            $shippingPriceAmount = ($shippingPriceObject === null ? null : $shippingPriceObject->getAmount());
            $giftWrapPriceObject = $orderItem->getGiftWrapPrice();
            $giftWrapPriceCurrencyCode = ($giftWrapPriceObject === null ? null : $giftWrapPriceObject->getCurrencyCode());
            $giftWrapPriceAmount = ($giftWrapPriceObject === null ? null : $giftWrapPriceObject->getAmount());
            $taxObject = $orderItem->getItemTax();
            $taxCurrencyCode = ($taxObject === null ? null : $taxObject->getCurrencyCode());
            $taxAmount = ($taxObject === null ? null : $taxObject->getAmount());
            $shippingTaxObject = $orderItem->getShippingTax();
            $shippingTaxCurrencyCode = ($shippingTaxObject === null ? null : $shippingTaxObject->getCurrencyCode());
            $shippingTaxAmount = ($shippingTaxObject === null ? null : $shippingTaxObject->getAmount());
            $giftWrapTaxObject = $orderItem->getGiftWrapTax();
            $giftWrapTaxCurrencyCode = ($giftWrapTaxObject === null ? null : $giftWrapTaxObject->getCurrencyCode());
            $giftWrapTaxAmount = ($giftWrapTaxObject === null ? null : $giftWrapTaxObject->getAmount());
            $shippingDiscountObject = $orderItem->getShippingDiscount();
            $shippingDiscountCurrencyCode = ($shippingDiscountObject === null ? null : $shippingDiscountObject->getCurrencyCode());
            $shippingDiscountAmount = ($shippingDiscountObject === null ? null : $shippingDiscountObject->getAmount());
            $promotionDiscountObject = $orderItem->getPromotionDiscount();
            $promotionDiscountCurrencyCode = ($promotionDiscountObject === null ? null : $promotionDiscountObject->getCurrencyCode());
            $promotionDiscountAmount = ($promotionDiscountObject === null ? null : $promotionDiscountObject->getAmount());
            $codFeeObject = $orderItem->getCodFee();
            $codFeeCurrencyCode = ($codFeeObject === null ? null : $codFeeObject->getCurrencyCode());
            $codFeeAmount = ($codFeeObject === null ? null : $codFeeObject->getAmount());
            $codFeeDiscountObject = $orderItem->getCodFeeDiscount();
            $codFeeDiscountCurrencyCode = ($codFeeDiscountObject === null ? null : $codFeeDiscountObject->getCurrencyCode());
            $codFeeDiscountAmount = ($codFeeDiscountObject === null ? null : $codFeeDiscountObject->getAmount());
            $giftMessageText = $orderItem->getGiftMessageText();
            $giftWrapLevel = $orderItem->getGiftWrapLevel();
            $conditionNote = $orderItem->getConditionNote();
            $conditionId = $orderItem->getConditionId();
            $conditionSubtypeId = $orderItem->getConditionSubtypeId();
            $scheduledDeliveryStartDate = $orderItem->getScheduledDeliveryStartDate();
            $scheduledDeliveryStartDate = ($scheduledDeliveryStartDate === null ? null : DateTime::createFromFormat(DateTime::ATOM, $scheduledDeliveryStartDate)->format(DbUtil::MYSQL_DATE_FORMAT));
            $scheduledDeliveryEndDate = $orderItem->getScheduledDeliveryEndDate();
            $scheduledDeliveryEndDate = ($scheduledDeliveryEndDate === null ? null : DateTime::createFromFormat(DateTime::ATOM, $scheduledDeliveryEndDate)->format(DbUtil::MYSQL_DATE_FORMAT));
            $priceDesignation = $orderItem->getPriceDesignation();
            $orderItemId = null;
            $existingQuantityShipped = null;

            foreach($existingOrderItems as $existingOrderItemIdx => &$existingOrderItem) {
                if(Utils::valuesMatch($existingOrderItem["asin"], $asin) &&
                    Utils::valuesMatch($existingOrderItem["mws_order_item_id"], $mwsOrderItemId) &&
                    Utils::valuesMatch($existingOrderItem["seller_sku"], $sellerSku) &&
                    Utils::valuesMatch($existingOrderItem["customized_url"], $customizedUrl) &&
                    Utils::valuesMatch($existingOrderItem["title"], $title) &&
                    Utils::valuesMatch($existingOrderItem["quantity_ordered"], $quantityOrdered) &&
                    Utils::valuesMatch($existingOrderItem["points_granted"], $pointsGranted) &&
                    Utils::valuesMatch($existingOrderItem["points_currency_code"], $pointsCurrencyCode) &&
                    Utils::valuesMatch($existingOrderItem["points_amount"], $pointsAmount) &&
                    Utils::valuesMatch($existingOrderItem["price_currency_code"], $priceCurrencyCode) &&
                    Utils::valuesMatch($existingOrderItem["price_amount"], $priceAmount) &&
                    Utils::valuesMatch($existingOrderItem["shipping_price_currency_code"], $shippingPriceCurrencyCode) &&
                    Utils::valuesMatch($existingOrderItem["shipping_price_amount"], $shippingPriceAmount) &&
                    Utils::valuesMatch($existingOrderItem["gift_wrap_price_currency_code"], $giftWrapPriceCurrencyCode) &&
                    Utils::valuesMatch($existingOrderItem["gift_wrap_price_amount"], $giftWrapPriceAmount) &&
                    Utils::valuesMatch($existingOrderItem["tax_currency_code"], $taxCurrencyCode) &&
                    Utils::valuesMatch($existingOrderItem["tax_amount"], $taxAmount) &&
                    Utils::valuesMatch($existingOrderItem["shipping_tax_currency_code"], $shippingTaxCurrencyCode) &&
                    Utils::valuesMatch($existingOrderItem["shipping_tax_amount"], $shippingTaxAmount) &&
                    Utils::valuesMatch($existingOrderItem["gift_wrap_tax_currency_code"], $giftWrapTaxCurrencyCode) &&
                    Utils::valuesMatch($existingOrderItem["gift_wrap_tax_amount"], $giftWrapTaxAmount) &&
                    Utils::valuesMatch($existingOrderItem["shipping_discount_currency_code"], $shippingDiscountCurrencyCode) &&
                    Utils::valuesMatch($existingOrderItem["shipping_discount_amount"], $shippingDiscountAmount) &&
                    Utils::valuesMatch($existingOrderItem["promotion_discount_currency_code"], $promotionDiscountCurrencyCode) &&
                    Utils::valuesMatch($existingOrderItem["promotion_discount_amount"], $promotionDiscountAmount) &&
                    Utils::valuesMatch($existingOrderItem["cod_fee_currency_code"], $codFeeCurrencyCode) &&
                    Utils::valuesMatch($existingOrderItem["cod_fee_amount"], $codFeeAmount) &&
                    Utils::valuesMatch($existingOrderItem["cod_fee_discount_currency_code"], $codFeeDiscountCurrencyCode) &&
                    Utils::valuesMatch($existingOrderItem["cod_fee_discount_amount"], $codFeeDiscountAmount) &&
                    Utils::valuesMatch($existingOrderItem["gift_message_text"], $giftMessageText) &&
                    Utils::valuesMatch($existingOrderItem["gift_wrap_level"], $giftWrapLevel) &&
                    Utils::valuesMatch($existingOrderItem["condition_note"], $conditionNote) &&
                    Utils::valuesMatch($existingOrderItem["condition_id"], $conditionId) &&
                    Utils::valuesMatch($existingOrderItem["condition_subtype_id"], $conditionSubtypeId) &&
                    Utils::valuesMatch($existingOrderItem["scheduled_delivery_start_date"], $scheduledDeliveryStartDate) &&
                    Utils::valuesMatch($existingOrderItem["scheduled_delivery_end_date"], $scheduledDeliveryEndDate) &&
                    Utils::valuesMatch($existingOrderItem["price_designation"], $priceDesignation)) {

                    $orderItemId = $existingOrderItem["id"];
                    $existingQuantityShipped = $existingOrderItem["quantity_shipped"];

                    unset($existingOrderItems[$existingOrderItemIdx]);

                    break;
                }
            }

            unset($existingOrderItem);

            if($orderItemId === null) {
                $orderItemId =& PDOQuery::insert()
                    ->into("amazon_mws.order_item")
                    ->value("order_id", $orderId, PDO::PARAM_INT)
                    ->value("asin", $asin, PDO::PARAM_STR)
                    ->value("mws_order_item_id", $mwsOrderItemId, PDO::PARAM_STR)
                    ->value("seller_sku", $sellerSku, PDO::PARAM_STR)
                    ->value("customized_url", $customizedUrl, PDO::PARAM_STR)
                    ->value("title", $title, PDO::PARAM_STR)
                    ->value("quantity_ordered", $quantityOrdered, PDO::PARAM_INT)
                    ->value("quantity_shipped", $quantityShipped, PDO::PARAM_INT)
                    ->value("points_granted", $pointsGranted, PDO::PARAM_INT)
                    ->value("points_currency_code", $pointsCurrencyCode, PDO::PARAM_STR)
                    ->value("points_amount", $pointsAmount, PDO::PARAM_STR)
                    ->value("price_currency_code", $priceCurrencyCode, PDO::PARAM_STR)
                    ->value("price_amount", $priceAmount, PDO::PARAM_STR)
                    ->value("shipping_price_currency_code", $shippingPriceCurrencyCode, PDO::PARAM_STR)
                    ->value("shipping_price_amount", $shippingPriceAmount, PDO::PARAM_STR)
                    ->value("gift_wrap_price_currency_code", $giftWrapPriceCurrencyCode, PDO::PARAM_STR)
                    ->value("gift_wrap_price_amount", $giftWrapPriceAmount, PDO::PARAM_STR)
                    ->value("tax_currency_code", $taxCurrencyCode, PDO::PARAM_STR)
                    ->value("tax_amount", $taxAmount, PDO::PARAM_STR)
                    ->value("shipping_tax_currency_code", $shippingTaxCurrencyCode, PDO::PARAM_STR)
                    ->value("shipping_tax_amount", $shippingTaxAmount, PDO::PARAM_STR)
                    ->value("gift_wrap_tax_currency_code", $giftWrapTaxCurrencyCode, PDO::PARAM_STR)
                    ->value("gift_wrap_tax_amount", $giftWrapTaxAmount, PDO::PARAM_STR)
                    ->value("shipping_discount_currency_code", $shippingDiscountCurrencyCode, PDO::PARAM_STR)
                    ->value("shipping_discount_amount", $shippingDiscountAmount, PDO::PARAM_STR)
                    ->value("promotion_discount_currency_code", $promotionDiscountCurrencyCode, PDO::PARAM_STR)
                    ->value("promotion_discount_amount", $promotionDiscountAmount, PDO::PARAM_STR)
                    ->value("cod_fee_currency_code", $codFeeCurrencyCode, PDO::PARAM_STR)
                    ->value("cod_fee_amount", $codFeeAmount, PDO::PARAM_STR)
                    ->value("cod_fee_discount_currency_code", $codFeeDiscountCurrencyCode, PDO::PARAM_STR)
                    ->value("cod_fee_discount_amount", $codFeeDiscountAmount, PDO::PARAM_STR)
                    ->value("gift_message_text", $giftMessageText, PDO::PARAM_STR)
                    ->value("gift_wrap_level", $giftWrapLevel, PDO::PARAM_STR)
                    ->value("condition_note", $conditionNote, PDO::PARAM_STR)
                    ->value("condition_id", $conditionId, PDO::PARAM_STR)
                    ->value("condition_subtype_id", $conditionSubtypeId, PDO::PARAM_STR)
                    ->value("scheduled_delivery_start_date", $scheduledDeliveryStartDate, PDO::PARAM_STR)
                    ->value("scheduled_delivery_end_date", $scheduledDeliveryEndDate, PDO::PARAM_STR)
                    ->value("price_designation", $priceDesignation, PDO::PARAM_STR)
                    ->executeEnsuringLastInsertId($this->connection)
                ;

                $orderItemsUpdatedCount++;
            } else if(!Utils::valuesMatch($existingQuantityShipped, $quantityShipped)) {
                PDOQuery::update()
                    ->table("amazon_mws.order_item")
                    ->set("quantity_shipped", $quantityShipped, PDO::PARAM_INT)
                    ->where("id", $orderItemId, PDO::PARAM_INT)
                    ->executeEnsuringRowCount($this->connection, 1)
                ;

                $orderItemsUpdatedCount++;
            }

            $this->storeOrderItemPromotions($orderItemId, $orderItem);
            $this->storeOrderItemInvoiceData($orderItemId, $orderItem);
        }

        unset($orderItem);

        foreach($existingOrderItems as &$removedOrderItem) {
            $removedId = $removedOrderItem["id"];

            $logger->notice(
                "Removing order item #" . $removedId . ", ASIN: " . $removedOrderItem["asin"] .
                ", MWS order item id: " . $removedOrderItem["mws_order_item_id"] .
                ", seller sku: " . $removedOrderItem["seller_sku"] .
                ", currency code: " . $removedOrderItem["price_currency_code"] .
                ", amount: " . $removedOrderItem["price_amount"]
            );
            PDOQuery::delete()->from("amazon_mws.order_item_promotion")
                ->where("order_item_id", $removedId, PDO::PARAM_INT)
                ->execute($this->connection)
            ;
            $this->removeOrderItemInvoiceData($removedId);

            PDOQuery::delete()->from("amazon_mws.order_item")->where("id", $removedId, PDO::PARAM_INT)->executeEnsuringRowCount($this->connection, 1);

            $orderItemsUpdatedCount++;
        }

        unset($removedOrderItem);

        $rowCount =& PDOQuery::insert()
            ->into("amazon_mws.order_item_extract")
            ->value("order_id", $orderId, PDO::PARAM_INT)
            ->onDuplicateValue("updated", "CURRENT_TIMESTAMP")
            ->executeGetRowCount($this->connection)
        ;

        if($rowCount < 1) {
            throw new Exception("Failed to update Amazon order item extract record for order id: " . $orderId);
        }

        $logger->debug(($rowCount > 1 ? "Updated" : "Inserted") . " Amazon order item extract record for order id: " . $orderId);

        return $orderItemsUpdatedCount;
    }

    private function storeOrderItemPromotions($orderItemId, MarketplaceWebServiceOrders_Model_OrderItem &$orderItem) {
        $logger =& LogUtil::getLogger();
        $existingPromotions =& PDOQuery::select()
            ->from("amazon_mws.order_item_promotion")
            ->where("order_item_id", $orderItemId, PDO::PARAM_INT)
            ->options("FOR UPDATE")
            ->getResults($this->connection)
        ;

        foreach($orderItem->getPromotionIds() as &$promotionId) {
            $promotionAlreadyExists = false;

            foreach($existingPromotions as $existingPromotionIdx => &$existingPromotion) {
                if(Utils::valuesMatch($existingPromotion["promotion_id"], $promotionId)) {
                    $promotionAlreadyExists = true;
                    break;
                }
            }

            unset($existingPromotion);

            if(!$promotionAlreadyExists) {
                PDOQuery::insert()
                    ->into("amazon_mws.order_item_promotion")
                    ->value("order_item_id", $orderItemId, PDO::PARAM_INT)
                    ->value("promotion_id", $promotionId, PDO::PARAM_STR)
                    ->executeEnsuringRowCount($this->connection, 1)
                ;
            }
        }

        unset($promotionId);

        foreach($existingPromotions as &$existingPromotion) {
            $promotionStillExists = false;

            foreach($orderItem->getPromotionIds() as &$promotionId) {
                if(Utils::valuesMatch($existingPromotion["promotion_id"], $promotionId)) {
                    $promotionStillExists = true;
                    break;
                }
            }

            unset($promotionId);

            if(!$promotionStillExists) {
                $logger->notice("Removing promotion id: " . $existingPromotion["promotion_id"] . " for Amazon MWS order item id: " . $orderItemId);

                PDOQuery::delete()->from("amazon_mws.order_item_promotion")
                    ->where("order_item_id", $orderItemId, PDO::PARAM_INT)
                    ->where("promotion_id", $existingPromotion["promotion_id"], PDO::PARAM_STR)
                    ->executeEnsuringRowCount($this->connection, 1)
                ;
            }
        }

        unset($existingPromotion);
    }

    private function storeOrderItemInvoiceData($orderItemId, MarketplaceWebServiceOrders_Model_OrderItem &$orderItem) {
        $invoiceData = $orderItem->getInvoiceData();

        if(empty($invoiceData)) {
            $this->removeOrderItemInvoiceData($orderItemId);

            return;
        }

        PDOQuery::insert()
            ->into("amazon_mws.order_item_invoice_data")
            ->value("order_item_id", $orderItemId, PDO::PARAM_INT)
            ->value("requirement", $invoiceData->getInvoiceRequirement(), PDO::PARAM_STR)
            ->value("category", $invoiceData->getBuyerSelectedInvoiceCategory(), PDO::PARAM_STR)
            ->value("title", $invoiceData->getInvoiceTitle(), PDO::PARAM_STR)
            ->value("information", $invoiceData->getInvoiceInformation(), PDO::PARAM_STR)
            ->onDuplicateValue("requirement", "VALUES(requirement)")
            ->onDuplicateValue("category", "VALUES(category)")
            ->onDuplicateValue("title", "VALUES(title)")
            ->onDuplicateValue("information", "VALUES(information)")
            ->execute($this->connection)
        ;
    }

    private function removeOrderItemInvoiceData($orderItemId) {
        PDOQuery::delete()->from("amazon_mws.order_item_invoice_data")
            ->where("order_item_id", $orderItemId, PDO::PARAM_INT)
            ->execute($this->connection)
        ;
    }
}
