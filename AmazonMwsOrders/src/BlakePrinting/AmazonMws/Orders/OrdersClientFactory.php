<?php

namespace BlakePrinting\AmazonMws\Orders;

use BlakePrinting\AmazonMws\Sellers\AmazonMwsClientFactory;
use BlakePrinting\AmazonMws\Sellers\CredentialVO;
use MarketplaceWebServiceOrders_Client;
use MarketplaceWebServiceOrders_Interface;
use MarketplaceWebServiceOrders_Mock;
use PDO;

/**
 * @method MarketplaceWebServiceOrders_Interface createClient($applicationName, $applicationVersion, CredentialVO &$credentialVo = null, PDO $connection = null)
 */
class OrdersClientFactory extends AmazonMwsClientFactory {
    public function __construct() {
        parent::__construct(
            AmazonMwsClientFactory::ORDER_API, "amazon_mws_orders/src",
            MarketplaceWebServiceOrders_Client::class, MarketplaceWebServiceOrders_Mock::class,
            "/Orders/2013-09-01"
        );
    }
}
