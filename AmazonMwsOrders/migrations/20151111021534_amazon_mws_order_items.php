<?php

use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceDAO;
use BlakePrinting\Db\DbMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class AmazonMwsOrderItems extends DbMigration {
    public function up() {
        $this->table("order_item", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "datetime", ["default" => "CURRENT_TIMESTAMP"])
            ->addColumn("updated", "datetime", ["null" => true, "update" => "CURRENT_TIMESTAMP"])
            ->addColumn("order_id", "biginteger", ["signed" => false])
            ->addColumn("asin", "string", ["limit" => 10])
            ->addColumn("mws_order_item_id", "string", ["limit" => 20])
            ->addColumn("seller_sku", "string", ["limit" => 191, "null" => true])
            ->addColumn("customized_url", "string", ["limit" => 191, "null" => true])
            ->addColumn("title", "text", ["null" => true])
            ->addColumn("quantity_ordered", "integer", ["signed" => false, "null" => true, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("quantity_shipped", "integer", ["signed" => false, "null" => true, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("points_granted", "integer", ["signed" => false, "null" => true, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("points_currency_code", "string", ["limit" => 3, "null" => true])
            ->addColumn("points_amount", "string", ["limit" => 10, "null" => true])
            ->addColumn("price_currency_code", "string", ["limit" => 3, "null" => true])
            ->addColumn("price_amount", "string", ["limit" => 10, "null" => true])
            ->addColumn("shipping_price_currency_code", "string", ["limit" => 3, "null" => true])
            ->addColumn("shipping_price_amount", "string", ["limit" => 10, "null" => true])
            ->addColumn("gift_wrap_price_currency_code", "string", ["limit" => 3, "null" => true])
            ->addColumn("gift_wrap_price_amount", "string", ["limit" => 10, "null" => true])
            ->addColumn("tax_currency_code", "string", ["limit" => 3, "null" => true])
            ->addColumn("tax_amount", "string", ["limit" => 10, "null" => true])
            ->addColumn("shipping_tax_currency_code", "string", ["limit" => 3, "null" => true])
            ->addColumn("shipping_tax_amount", "string", ["limit" => 10, "null" => true])
            ->addColumn("gift_wrap_tax_currency_code", "string", ["limit" => 3, "null" => true])
            ->addColumn("gift_wrap_tax_amount", "string", ["limit" => 10, "null" => true])
            ->addColumn("shipping_discount_currency_code", "string", ["limit" => 3, "null" => true])
            ->addColumn("shipping_discount_amount", "string", ["limit" => 10, "null" => true])
            ->addColumn("promotion_discount_currency_code", "string", ["limit" => 3, "null" => true])
            ->addColumn("promotion_discount_amount", "string", ["limit" => 10, "null" => true])
            ->addColumn("cod_fee_currency_code", "string", ["limit" => 3, "null" => true])
            ->addColumn("cod_fee_amount", "string", ["limit" => 10, "null" => true])
            ->addColumn("cod_fee_discount_currency_code", "string", ["limit" => 3, "null" => true])
            ->addColumn("cod_fee_discount_amount", "string", ["limit" => 10, "null" => true])
            ->addColumn("gift_message_text", "text", ["null" => true])
            ->addColumn("gift_wrap_level", "string", ["limit" => 191, "null" => true])
            ->addColumn("condition_note", "string", ["limit" => 191, "null" => true])
            ->addColumn("condition_id", "string", ["limit" => 20, "null" => true])
            ->addColumn("condition_subtype_id", "string", ["limit" => 20, "null" => true])
            ->addColumn("scheduled_delivery_start_date", "datetime", ["null" => true])
            ->addColumn("scheduled_delivery_end_date", "datetime", ["null" => true])
            ->addColumn("price_designation", "string", ["limit" => 20, "null" => true])
            ->addForeignKey("order_id", "orders", "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("asin")
            ->addIndex("mws_order_item_id")
            ->addIndex("seller_sku")
            ->addIndex("customized_url")
            ->addIndex("quantity_ordered")
            ->addIndex("quantity_shipped")
            ->addIndex("points_granted")
            ->addIndex("points_currency_code")
            ->addIndex("points_amount")
            ->addIndex("price_currency_code")
            ->addIndex("price_amount")
            ->addIndex("shipping_price_currency_code")
            ->addIndex("shipping_price_amount")
            ->addIndex("gift_wrap_price_currency_code")
            ->addIndex("gift_wrap_price_amount")
            ->addIndex("tax_currency_code")
            ->addIndex("tax_amount")
            ->addIndex("shipping_tax_currency_code")
            ->addIndex("shipping_tax_amount")
            ->addIndex("gift_wrap_tax_currency_code")
            ->addIndex("gift_wrap_tax_amount")
            ->addIndex("shipping_discount_currency_code")
            ->addIndex("shipping_discount_amount")
            ->addIndex("promotion_discount_currency_code")
            ->addIndex("promotion_discount_amount")
            ->addIndex("cod_fee_currency_code")
            ->addIndex("cod_fee_amount")
            ->addIndex("cod_fee_discount_currency_code")
            ->addIndex("cod_fee_discount_amount")
            ->addIndex("gift_wrap_level")
            ->addIndex("condition_id")
            ->addIndex("condition_subtype_id")
            ->addIndex("scheduled_delivery_start_date")
            ->addIndex("scheduled_delivery_end_date")
            ->addIndex("price_designation")
            ->save()
        ;
        $this->execute("ALTER TABLE order_item ADD INDEX (title(191))");
        $this->execute("ALTER TABLE order_item ADD INDEX (gift_message_text(191))");

        $this->table("order_item_promotion", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["order_item_id", "promotion_id"]])
            ->addColumn("order_item_id", "biginteger", ["signed" => false])
            ->addColumn("promotion_id", "string", ["limit" => 191])
            ->addForeignKey("order_item_id", "order_item", "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("promotion_id")
            ->save()
        ;

        $this->table("order_item_invoice_data", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["order_item_id"]])
            ->addColumn("order_item_id", "biginteger", ["signed" => false])
            ->addColumn("requirement", "string", ["limit" => 15, "null" => true])
            ->addColumn("category", "text", ["null" => true])
            ->addColumn("title", "text", ["null" => true])
            ->addColumn("information", "string", ["limit" => 30, "null" => true])
            ->addForeignKey("order_item_id", "order_item", "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("requirement")
            ->addIndex("information")
            ->save()
        ;
        $this->execute("ALTER TABLE order_item_invoice_data ADD INDEX (category(191))");
        $this->execute("ALTER TABLE order_item_invoice_data ADD INDEX (title(191))");

        $this->table("order_item_extract", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["order_id"]])
            ->addColumn("order_id", "biginteger", ["signed" => false])
            ->addColumn("created", "datetime", ["default" => "CURRENT_TIMESTAMP"])
            ->addColumn("updated", "datetime", ["null" => true, "update" => "CURRENT_TIMESTAMP"])
            ->addForeignKey("order_id", "orders", "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->save()
        ;

        $marketplaceInstanceDao = new MarketplaceInstanceDAO($this->getConnection());
        $this->table("order_item_list", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["marketplace_instance_id"]])
            ->addColumn("marketplace_instance_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("next_token", "text", ["null" => true])
            ->addColumn("response_header_metadata", "text")
            ->addForeignKey("marketplace_instance_id", $marketplaceInstanceDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;
    }

    public function down() {
        $this->execute("DROP TABLE IF EXISTS order_item_list");
        $this->execute("DROP TABLE IF EXISTS order_item_extract");
        $this->execute("DROP TABLE IF EXISTS order_item_invoice_data");
        $this->execute("DROP TABLE IF EXISTS order_item_promotion");
        $this->execute("DROP TABLE IF EXISTS order_item");
    }
}
