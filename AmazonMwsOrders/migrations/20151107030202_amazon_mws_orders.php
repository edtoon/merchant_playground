<?php

use BlakePrinting\AmazonMws\AddressDAO;
use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceDAO;
use BlakePrinting\Db\DbMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class AmazonMwsOrders extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();
        $marketplaceInstanceDao = new MarketplaceInstanceDAO($connection);
        $addressDao = new AddressDAO($connection);
        $this->table("orders", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "datetime", ["default" => "CURRENT_TIMESTAMP"])
            ->addColumn("updated", "datetime", ["null" => true, "update" => "CURRENT_TIMESTAMP"])
            ->addColumn("marketplace_instance_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("amazon_order_id", "string", ["limit" => 50])
            ->addColumn("seller_order_id", "string", ["limit" => 191, "null" => true])
            ->addColumn("purchase_date", "datetime")
            ->addColumn("last_update_date", "datetime")
            ->addColumn("order_status", "string", ["limit" => 191])
            ->addColumn("fulfillment_channel", "string", ["limit" => 191, "null" => true])
            ->addColumn("sales_channel", "string", ["limit" => 191, "null" => true])
            ->addColumn("order_channel", "string", ["limit" => 191, "null" => true])
            ->addColumn("ship_service_level", "string", ["limit" => 191, "null" => true])
            ->addColumn("address_id", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("order_total_currency_code", "string", ["limit" => 3, "null" => true])
            ->addColumn("order_total_amount", "string", ["limit" => 10, "null" => true])
            ->addColumn("number_of_items_shipped", "integer", ["signed" => false, "null" => true, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("number_of_items_unshipped", "integer", ["signed" => false, "null" => true, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("payment_method", "string", ["limit" => 5, "null" => true])
            ->addColumn("buyer_email", "string", ["limit" => 191, "null" => true])
            ->addColumn("buyer_name", "string", ["limit" => 191, "null" => true])
            ->addColumn("shipment_service_level_category", "string", ["limit" => 15, "null" => true])
            ->addColumn("shipped_by_amazon_tfm", "boolean", ["null" => true])
            ->addColumn("tfm_shipment_status", "string", ["limit" => 20, "null" => true])
            ->addColumn("cba_displayable_shipping_label", "string", ["limit" => 15, "null" => true])
            ->addColumn("order_type", "string", ["limit" => 15, "null" => true])
            ->addColumn("earliest_ship_date", "datetime", ["null" => true])
            ->addColumn("latest_ship_date", "datetime", ["null" => true])
            ->addColumn("earliest_delivery_date", "datetime", ["null" => true])
            ->addColumn("latest_delivery_date", "datetime", ["null" => true])
            ->addColumn("business_order", "boolean", ["null" => true])
            ->addColumn("purchase_order_number", "string", ["limit" => 191, "null" => true])
            ->addColumn("prime", "boolean")
            ->addColumn("premium_order", "boolean")
            ->addForeignKey("marketplace_instance_id", $marketplaceInstanceDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("address_id", $addressDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex(["marketplace_instance_id", "amazon_order_id"], ["unique" => true])
            ->addIndex("amazon_order_id")
            ->addIndex("seller_order_id")
            ->addIndex("purchase_date")
            ->addIndex("last_update_date")
            ->addIndex("order_status")
            ->addIndex("fulfillment_channel")
            ->addIndex("sales_channel")
            ->addIndex("order_channel")
            ->addIndex("order_total_currency_code")
            ->addIndex("order_total_amount")
            ->addIndex("number_of_items_shipped")
            ->addIndex("number_of_items_unshipped")
            ->addIndex("payment_method")
            ->addIndex("buyer_email")
            ->addIndex("buyer_name")
            ->addIndex("shipment_service_level_category")
            ->addIndex("shipped_by_amazon_tfm")
            ->addIndex("tfm_shipment_status")
            ->addIndex("cba_displayable_shipping_label")
            ->addIndex("order_type")
            ->addIndex("earliest_ship_date")
            ->addIndex("latest_ship_date")
            ->addIndex("earliest_delivery_date")
            ->addIndex("latest_delivery_date")
            ->addIndex("business_order")
            ->addIndex("purchase_order_number")
            ->addIndex("prime")
            ->addIndex("premium_order")
            ->save()
        ;

        $this->table("order_payment", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "datetime", ["default" => "CURRENT_TIMESTAMP"])
            ->addColumn("updated", "datetime", ["null" => true, "update" => "CURRENT_TIMESTAMP"])
            ->addColumn("order_id", "biginteger", ["signed" => false])
            ->addColumn("currency_code", "string", ["limit" => 3])
            ->addColumn("amount", "string", ["limit" => 10])
            ->addColumn("method", "string", ["limit" => 15])
            ->addForeignKey("order_id", "orders", "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("currency_code")
            ->addIndex("amount")
            ->addIndex("method")
            ->save()
        ;

        $this->table("order_list", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["marketplace_instance_id"]])
            ->addColumn("marketplace_instance_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("last_updated_after", "datetime")
            ->addColumn("next_token", "text", ["null" => true])
            ->addColumn("response_header_metadata", "text")
            ->addForeignKey("marketplace_instance_id", $marketplaceInstanceDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;
    }

    public function down() {
        $this->execute("DROP TABLE IF EXISTS order_list");
        $this->execute("DROP TABLE IF EXISTS order_payment");
        $this->execute("DROP TABLE IF EXISTS orders");
    }
}
