#!/bin/bash
#
# Download non-Packagist dependencies
#

set -o errexit
set -o nounset

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

VEND_DIR="${VEND_DIR:=${BASE_DIR}/vendor}"
DL_DIR="${DL_DIR:=${BASE_DIR}/downloads}"

declare -a VENDOR_DOWNLOADS=(
  "amazon_mws_orders:https://images-na.ssl-images-amazon.com/images/G/01/mwsportal/clientlib/Orders/2013-09-01/MWSOrdersPHPClientLibrary-2013-09-01._V293335039_.zip"
)

unzip -v > /dev/null 2>&1 || sudo apt-get install unzip

mkdir -p "${DL_DIR}" "${VEND_DIR}"

for vendor_download in ${VENDOR_DOWNLOADS[@]}
do
  vendor_dir="${VEND_DIR}/${vendor_download%%:*}"
  vendor_url="${vendor_download#*:}"

  if [ ! -d "${vendor_dir}" ];
  then
    download_filename="${vendor_url##*/}"
    download_path="${DL_DIR}/${download_filename}"

    if [ ! -f "${download_path}" ];
    then
      echo "Downloading: ${download_path: -4}"
      curl -o "${download_path}" "${vendor_url}"
    fi

    if [ "${download_path: -4}" = ".zip" ];
    then
      mkdir -p "${vendor_dir}"
      unzip "${download_path}" -d "${vendor_dir}"
    else
      echo "Downloaded: ${download_path: -4}"
    fi
  fi
done

for patch_php_file in `find ${VEND_DIR}/amazon_mws_orders/src/MarketplaceWebServiceOrders/ -name "*.php"`
do
  backup_file="${patch_php_file}.bak"

  if [ ! -f "${backup_file}" ] || [ "${patch_php_file}" -ot "${backup_file}" ];
  then
    sed -i.bak "s/ toXMLFragment/ _toXMLFragment/g" "${patch_php_file}"
    sed -i.bak "s/>toXMLFragment/>_toXMLFragment/g" "${patch_php_file}"
    sed -i.bak "s/protected function _toXMLFragment/public function _toXMLFragment/g" "${patch_php_file}"
    sed -i.bak "s/iconv_set_encoding/ @iconv_set_encoding/g" "${patch_php_file}"
    sed -i.bak "s/@return Address/@return MarketplaceWebServiceOrders_Model_Address/g" "${patch_php_file}"
    sed -i.bak "s/@return Buyer/@return MarketplaceWebServiceOrders_Model_Buyer/g" "${patch_php_file}"
    sed -i.bak "s/@return Get/@return MarketplaceWebServiceOrders_Model_Get/g" "${patch_php_file}"
    sed -i.bak "s/@return InvoiceData/@return MarketplaceWebServiceOrders_Model_InvoiceData/g" "${patch_php_file}"
    sed -i.bak "s/@return ListOrder/@return MarketplaceWebServiceOrders_Model_ListOrder/g" "${patch_php_file}"
    sed -i.bak "s/@return Money/@return MarketplaceWebServiceOrders_Model_Money/g" "${patch_php_file}"
    sed -i.bak "s/@return Points/@return MarketplaceWebServiceOrders_Model_Points/g" "${patch_php_file}"
    sed -i.bak "s/@return Response/@return MarketplaceWebServiceOrders_Model_Response/g" "${patch_php_file}"
    sed -i.bak "s/@return List<Message>/@return MarketplaceWebServiceOrders_Model_Message[]/g" "${patch_php_file}"
    sed -i.bak "s/@return List<Order>/@return MarketplaceWebServiceOrders_Model_Order[]/g" "${patch_php_file}"
    sed -i.bak "s/@return List<OrderItem>/@return MarketplaceWebServiceOrders_Model_OrderItem[]/g" "${patch_php_file}"
    sed -i.bak "s/@return List<PaymentExecutionDetailItem>/@return MarketplaceWebServiceOrders_Model_PaymentExecutionDetailItem[]/g" "${patch_php_file}"
    sed -i.bak "s/@return List<String>/@return string[]/g" "${patch_php_file}"
    sed -i.bak "s/@return XMLGregorianCalendar/@return string/g" "${patch_php_file}"
  fi
done
