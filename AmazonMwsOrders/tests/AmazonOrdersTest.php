<?php

use BlakePrinting\AmazonMws\EndpointVO;
use BlakePrinting\AmazonMws\Orders\OrdersClientFactory;
use BlakePrinting\AmazonMws\Sellers\AmazonMwsUtil;
use BlakePrinting\Test\AbstractTestCase;
use BlakePrinting\Util\Utils;

class AmazonOrdersTest extends AbstractTestCase {
    /** @var MarketplaceWebServiceOrders_Interface */
    private static $client = null;

    public static function setUpBeforeClass() {
        parent::setUpBeforeClass();

        $ordersClientFactory = new OrdersClientFactory();
        $endpointVo = new EndpointVO();

        $endpointVo->setId(AmazonMwsUtil::MOCK_ENDPOINT_ID);
        $endpointVo->setEndpoint(AmazonMwsUtil::MOCK_ENDPOINT);
        $ordersClientFactory->setEndpointVo($endpointVo);

        self::$client =& $ordersClientFactory->createClient(self::class, "0.0.1");
    }

    public function testListOrders() {
        $request = new MarketplaceWebServiceOrders_Model_ListOrdersRequest();
        $response = self::$client->listOrders($request);

        AmazonMwsUtil::logResponse($response);

        $this->assertNotEmpty($response);

        $responseMetadata = $response->getResponseMetadata();

        $this->assertNotEmpty($responseMetadata);
        $this->assertEquals("String", $responseMetadata->getRequestId());

        $result = $response->getListOrdersResult();

        $this->assertNotEmpty($result);
        $this->assertEquals("String", $result->getNextToken());
        $this->assertEquals("1969-07-21T02:56:03Z", $result->getCreatedBefore());
        $this->assertEquals("1969-07-21T02:56:03Z", $result->getLastUpdatedBefore());

        $orders = $result->getOrders();

        $this->assertNotEmpty($orders);
        $this->assertEquals(1, count($orders));

        foreach($orders as $order) {
            $this->assertEquals("String", $order->getAmazonOrderId());
            $this->assertEquals("String", $order->getSellerOrderId());
            $this->assertEquals("1969-07-21T02:56:03Z", $order->getPurchaseDate());
            $this->assertEquals("1969-07-21T02:56:03Z", $order->getLastUpdateDate());
            $this->assertEquals("String", $order->getOrderStatus());
            $this->assertEquals("String", $order->getFulfillmentChannel());
            $this->assertEquals("String", $order->getSalesChannel());
            $this->assertEquals("String", $order->getOrderChannel());
            $this->assertEquals("String", $order->getShipServiceLevel());
            $this->assertEquals(1, $order->getNumberOfItemsShipped());
            $this->assertEquals(1, $order->getNumberOfItemsUnshipped());
            $this->assertEquals("String", $order->getPaymentMethod());
            $this->assertEquals("String", $order->getMarketplaceId());
            $this->assertEquals("String", $order->getBuyerEmail());
            $this->assertEquals("String", $order->getBuyerName());
            $this->assertEquals("String", $order->getShipmentServiceLevelCategory());
            $this->assertTrue(Utils::getBooleanOrNull($order->getShippedByAmazonTFM()));
            $this->assertEquals("String", $order->getTFMShipmentStatus());
            $this->assertEquals("String", $order->getCbaDisplayableShippingLabel());
            $this->assertEquals("String", $order->getOrderType());
            $this->assertEquals("1969-07-21T02:56:03Z", $order->getEarliestShipDate());
            $this->assertEquals("1969-07-21T02:56:03Z", $order->getLatestShipDate());
            $this->assertEquals("1969-07-21T02:56:03Z", $order->getEarliestDeliveryDate());
            $this->assertEquals("1969-07-21T02:56:03Z", $order->getLatestDeliveryDate());
            $this->assertTrue(Utils::getBooleanOrNull($order->getIsBusinessOrder()));
            $this->assertEquals("String", $order->getPurchaseOrderNumber());
            $this->assertTrue(Utils::getBooleanOrNull($order->getIsPrime()));
            $this->assertTrue(Utils::getBooleanOrNull($order->getIsPremiumOrder()));

            $shippingAddress = $order->getShippingAddress();

            $this->assertNotEmpty($shippingAddress);
            $this->assertEquals("String", $shippingAddress->getName());
            $this->assertEquals("String", $shippingAddress->getAddressLine1());
            $this->assertEquals("String", $shippingAddress->getAddressLine2());
            $this->assertEquals("String", $shippingAddress->getAddressLine3());
            $this->assertEquals("String", $shippingAddress->getCity());
            $this->assertEquals("String", $shippingAddress->getStateOrRegion());
            $this->assertEquals("String", $shippingAddress->getPostalCode());
            $this->assertEquals("String", $shippingAddress->getPhone());

            $orderTotal = $order->getOrderTotal();

            $this->assertNotEmpty($orderTotal);
            $this->assertEquals("String", $orderTotal->getCurrencyCode());
            $this->assertEquals("String", $orderTotal->getAmount());

            $paymentExecutionDetails = $order->getPaymentExecutionDetail();

            $this->assertNotEmpty($paymentExecutionDetails);

            foreach($paymentExecutionDetails as $paymentExecutionDetail) {
                $payment = $paymentExecutionDetail->getPayment();

                $this->assertNotEmpty($payment);
                $this->assertEquals("String", $payment->getCurrencyCode());
                $this->assertEquals("String", $payment->getAmount());
            }
        }
    }
}
