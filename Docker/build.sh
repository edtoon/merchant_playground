#!/bin/bash
#
# Build the sub-project
#

set -o errexit

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${BASE_DIR}/../lib/lib-blake-environment.sh"
. "${BASE_DIR}/../lib/lib-docker.sh"

if [ "${HAS_DOCKER}" = "true" ];
then
  images="php-cli php-supervisor php-web"

  if [ "${BLAKE_ENVIRONMENT}" = "dev" ];
  then
    images="mysql ${images}"
  fi

  for dir in ${images}
  do
    docker_exec build -t blakeprinting/${dir} ${BASE_DIR}/${dir}
  done
fi
