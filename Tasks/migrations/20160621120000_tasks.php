<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\Fields\FieldDAO;
use BlakePrinting\Tasks\TaskAttemptDAO;
use BlakePrinting\Tasks\TaskAttemptFieldSignedDAO;
use BlakePrinting\Tasks\TaskAttemptFieldStringDAO;
use BlakePrinting\Tasks\TaskAttemptFieldTextDAO;
use BlakePrinting\Tasks\TaskAttemptFieldUnsignedDAO;
use BlakePrinting\Tasks\TaskDAO;
use BlakePrinting\Tasks\TaskFieldSignedDAO;
use BlakePrinting\Tasks\TaskFieldStringDAO;
use BlakePrinting\Tasks\TaskFieldTextDAO;
use BlakePrinting\Tasks\TaskFieldUnsignedDAO;
use BlakePrinting\Tasks\TaskLockDAO;
use BlakePrinting\Tasks\TaskResultDAO;
use BlakePrinting\Tasks\TaskResultFieldSignedDAO;
use BlakePrinting\Tasks\TaskResultFieldStringDAO;
use BlakePrinting\Tasks\TaskResultFieldTextDAO;
use BlakePrinting\Tasks\TaskResultFieldUnsignedDAO;
use BlakePrinting\Tasks\TaskStatusDAO;
use BlakePrinting\Users\UserDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class Tasks extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();
        $userDao =& UserDao::instance($connection);
        $userTbl = $userDao->formatSchemaAndTableName();
        $fieldDao =& FieldDAO::instance($connection);
        $fieldTbl = $fieldDao->formatSchemaAndTableName();

        $taskDao =& TaskDAO::instance($connection);
        $taskTbl = $taskDao->formatSchemaAndTableName();
        $this->tableDao($taskDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("name", "string", ["limit" => 191, "null" => true])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $taskStatusDao =& TaskStatusDAO::instance($connection);
        $this->tableDao($taskStatusDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["task_id"]])
            ->addColumn("task_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("code", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addForeignKey("task_id", $taskTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("updated")
            ->addIndex("code")
            ->save()
        ;

        $taskLockDao =& TaskLockDAO::instance($connection);
        $this->tableDao($taskLockDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["task_id"]])
            ->addColumn("task_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("lock_uuid", "varbinary", ["limit" => 256])
            ->addColumn("expiration", "biginteger", ["signed" => false])
            ->addForeignKey("task_id", $taskTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("lock_uuid")
            ->addIndex("expiration")
            ->save()
        ;

        $taskFieldStringDao =& TaskFieldStringDAO::instance($connection);
        $this->tableDao($taskFieldStringDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("task_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "string", ["null" => true, "limit" => 191])
            ->addForeignKey("task_id", $taskTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $taskFieldTextDao =& TaskFieldTextDAO::instance($connection);
        $this->tableDao($taskFieldTextDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("task_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "text", ["null" => true])
            ->addForeignKey("task_id", $taskTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $taskFieldUnsignedDao =& TaskFieldUnsignedDAO::instance($connection);
        $this->tableDao($taskFieldUnsignedDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("task_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "biginteger", ["null" => true, "signed" => false])
            ->addForeignKey("task_id", $taskTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $taskFieldSignedDao =& TaskFieldSignedDAO::instance($connection);
        $this->tableDao($taskFieldSignedDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("task_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "biginteger", ["null" => true])
            ->addForeignKey("task_id", $taskTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $taskAttemptDao =& TaskAttemptDAO::instance($connection);
        $taskAttemptTbl = $taskAttemptDao->formatSchemaAndTableName();
        $this->tableDao($taskAttemptDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("task_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addForeignKey("task_id", $taskTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->save()
        ;

        $taskAttemptFieldStringDao =& TaskAttemptFieldStringDAO::instance($connection);
        $this->tableDao($taskAttemptFieldStringDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("task_attempt_id", "biginteger", ["signed" => false])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "string", ["null" => true, "limit" => 191])
            ->addForeignKey("task_attempt_id", $taskAttemptTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $taskAttemptFieldTextDao =& TaskAttemptFieldTextDAO::instance($connection);
        $this->tableDao($taskAttemptFieldTextDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("task_attempt_id", "biginteger", ["signed" => false])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "text", ["null" => true])
            ->addForeignKey("task_attempt_id", $taskAttemptTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $taskAttemptFieldUnsignedDao =& TaskAttemptFieldUnsignedDAO::instance($connection);
        $this->tableDao($taskAttemptFieldUnsignedDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("task_attempt_id", "biginteger", ["signed" => false])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "biginteger", ["null" => true, "signed" => false])
            ->addForeignKey("task_attempt_id", $taskAttemptTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $taskAttemptFieldSignedDao =& TaskAttemptFieldSignedDAO::instance($connection);
        $this->tableDao($taskAttemptFieldSignedDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("task_attempt_id", "biginteger", ["signed" => false])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "biginteger", ["null" => true])
            ->addForeignKey("task_attempt_id", $taskAttemptTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $taskResultDao =& TaskResultDAO::instance($connection);
        $this->tableDao($taskResultDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["task_attempt_id"]])
            ->addColumn("task_attempt_id", "biginteger", ["signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("code", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addForeignKey("task_attempt_id", $taskAttemptTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $taskResultFieldStringDao =& TaskResultFieldStringDAO::instance($connection);
        $this->tableDao($taskResultFieldStringDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("task_attempt_id", "biginteger", ["signed" => false])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "string", ["null" => true, "limit" => 191])
            ->addForeignKey("task_attempt_id", $taskAttemptTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $taskResultFieldTextDao =& TaskResultFieldTextDAO::instance($connection);
        $this->tableDao($taskResultFieldTextDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("task_attempt_id", "biginteger", ["signed" => false])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "text", ["null" => true])
            ->addForeignKey("task_attempt_id", $taskAttemptTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $taskResultFieldUnsignedDao =& TaskResultFieldUnsignedDAO::instance($connection);
        $this->tableDao($taskResultFieldUnsignedDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("task_attempt_id", "biginteger", ["signed" => false])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "biginteger", ["null" => true, "signed" => false])
            ->addForeignKey("task_attempt_id", $taskAttemptTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $taskResultFieldSignedDao =& TaskResultFieldSignedDAO::instance($connection);
        $this->tableDao($taskResultFieldSignedDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("task_attempt_id", "biginteger", ["signed" => false])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "biginteger", ["null" => true])
            ->addForeignKey("task_attempt_id", $taskAttemptTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(TaskResultFieldSignedDAO::instance($connection));
        $this->dropDao(TaskResultFieldUnsignedDAO::instance($connection));
        $this->dropDao(TaskResultFieldTextDAO::instance($connection));
        $this->dropDao(TaskResultFieldStringDAO::instance($connection));
        $this->dropDao(TaskResultDAO::instance($connection));
        $this->dropDao(TaskAttemptFieldSignedDAO::instance($connection));
        $this->dropDao(TaskAttemptFieldUnsignedDAO::instance($connection));
        $this->dropDao(TaskAttemptFieldTextDAO::instance($connection));
        $this->dropDao(TaskAttemptFieldStringDAO::instance($connection));
        $this->dropDao(TaskAttemptDAO::instance($connection));
        $this->dropDao(TaskFieldSignedDAO::instance($connection));
        $this->dropDao(TaskFieldUnsignedDAO::instance($connection));
        $this->dropDao(TaskFieldTextDAO::instance($connection));
        $this->dropDao(TaskFieldStringDAO::instance($connection));
        $this->dropDao(TaskLockDAO::instance($connection));
        $this->dropDao(TaskStatusDAO::instance($connection));
        $this->dropDao(TaskDAO::instance($connection));
    }
}
