<?php

use BlakePrinting\Db\DbSeed;
use BlakePrinting\Fields\FieldDAO;
use BlakePrinting\Fields\FieldDomainDAO;
use BlakePrinting\Fields\FieldDomainVO;
use BlakePrinting\Fields\FieldSchemaDAO;
use BlakePrinting\Fields\FieldSchemaVO;
use BlakePrinting\Fields\FieldTableDAO;
use BlakePrinting\Fields\FieldTableVO;
use BlakePrinting\Fields\FieldTypeDAO;
use BlakePrinting\Fields\FieldVO;
use BlakePrinting\Tasks\TaskFields;
use BlakePrinting\Util\Utils;

class TaskFieldsSeeder extends DbSeed {
    public function run() {
        $connection =& $this->getConnection();
        $fieldDomainDao =& FieldDomainDAO::instance($connection);
        $fieldSchemaDao =& FieldSchemaDAO::instance($connection);
        $fieldTableDao =& FieldTableDAO::instance($connection);
        $fieldTypeDao =& FieldTypeDAO::instance($connection);
        $fieldDao =& FieldDAO::instance($connection);

        Utils::forEachConstantInObjectClass(
            TaskFields::class,
            function($_, $constantName, $constantValue) use (&$fieldDomainDao, &$fieldSchemaDao, &$fieldTableDao, &$fieldTypeDao, &$fieldDao) {
                $fieldSpec =& $constantValue;
                $fieldDomainVo =& $fieldDomainDao->findOrCreate(new FieldDomainVO($fieldSpec["domain"]));
                $fieldSchemaVo =& $fieldSchemaDao->findOrCreate(new FieldSchemaVO(
                    $fieldDomainVo->getId(), $fieldSpec["schema"]
                ));
                $fieldTableVo =& $fieldTableDao->findOrCreate(new FieldTableVO(
                    $fieldSchemaVo->getId(), $fieldSpec["table"]
                ));
                $fieldTypeVo =& $fieldTypeDao->findByColumn(
                    "name", $fieldSpec["type"], PDO::PARAM_STR, null, null, null, true
                );

                $fieldDao->findOrCreate(new FieldVO(
                    $fieldTableVo->getId(), $fieldTypeVo->getId(), $fieldSpec["field"]
                ));
            }, "FIELD_"
        );
    }
}
