<?php

namespace BlakePrinting\Tasks;

use Exception;

interface Task {
    /**
     * @return string
     */
    public function getName();

    /**
     * @return bool
     * @throws Exception
     */
    public function lock();

    /**
     * @return void
     * @throws Exception
     */
    public function unlock();

    /**
     * @return int
     * @throws Exception
     */
    public function run();
}
