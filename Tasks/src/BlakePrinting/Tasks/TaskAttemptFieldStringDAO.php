<?php

namespace BlakePrinting\Tasks;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static TaskAttemptFieldStringDAO instance(PDO $connection = null, $schemaName = null)
 * @method TaskAttemptFieldStringVO findById($id, $options = null, $useCache = false)
 * @method TaskAttemptFieldStringVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskAttemptFieldStringVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskAttemptFieldStringVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaskAttemptFieldStringVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskAttemptFieldStringVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskAttemptFieldStringVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class TaskAttemptFieldStringDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
