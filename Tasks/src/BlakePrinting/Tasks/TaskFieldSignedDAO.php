<?php

namespace BlakePrinting\Tasks;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static TaskFieldSignedDAO instance(PDO $connection = null, $schemaName = null)
 * @method TaskFieldSignedVO findById($id, $options = null, $useCache = false)
 * @method TaskFieldSignedVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskFieldSignedVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskFieldSignedVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaskFieldSignedVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskFieldSignedVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskFieldSignedVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class TaskFieldSignedDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
