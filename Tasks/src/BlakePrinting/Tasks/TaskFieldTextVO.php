<?php

namespace BlakePrinting\Tasks;

use BlakePrinting\Fields\AbstractEntityFieldStringVO;

class TaskFieldTextVO extends AbstractEntityFieldStringVO {
    /** @var int */
    private $taskId = null;

    /**
     * @return int
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * @param int $taskId
     */
    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;
    }
}
