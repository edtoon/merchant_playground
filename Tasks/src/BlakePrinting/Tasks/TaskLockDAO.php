<?php

namespace BlakePrinting\Tasks;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static TaskLockDAO instance(PDO $connection = null, $schemaName = null)
 * @method TaskLockVO findById($id, $options = null, $useCache = false)
 * @method TaskLockVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskLockVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskLockVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaskLockVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskLockVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskLockVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class TaskLockDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, "task_id");
    }

    /**
     * @param int $taskId
     * @param string $lockUuid
     * @param int $seconds
     * @return int
     */
    public function &acquire($taskId, $lockUuid, $seconds) {
        return PDOQuery::insert()
            ->into($this)
            ->value("created", "UNIX_TIMESTAMP()")
            ->value("task_id", $taskId, PDO::PARAM_INT)
            ->value("lock_uuid", $lockUuid, PDO::PARAM_STR)
            ->value("expiration", "UNIX_TIMESTAMP() + " . $seconds)
            ->executeGetRowCount($this->getConnection())
        ;
    }

    /**
     * @param TaskLockVO $taskLockVo
     * @param int $lockExpirationSeconds
     * @return int
     */
    public function &refresh(TaskLockVO &$taskLockVo, $lockExpirationSeconds) {
        return PDOQuery::update()
            ->table($this)
            ->set("updated", "UNIX_TIMESTAMP()")
            ->set("expiration", "UNIX_TIMESTAMP() + " . $lockExpirationSeconds)
            ->where("task_id", $taskLockVo->getTaskId(), PDO::PARAM_INT)
            ->where("lock_uuid", $taskLockVo->getLockUuid(), PDO::PARAM_STR)
            ->executeGetRowCount($this->getConnection())
        ;
    }

    /**
     * @return int
     */
    public function deleteExpired() {
        return PDOQuery::delete()
            ->from($this)
            ->where("expiration", "UNIX_TIMESTAMP()", null, "<")
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
