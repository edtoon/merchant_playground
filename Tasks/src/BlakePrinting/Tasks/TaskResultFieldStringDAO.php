<?php

namespace BlakePrinting\Tasks;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static TaskResultFieldStringDAO instance(PDO $connection = null, $schemaName = null)
 * @method TaskResultFieldStringVO findById($id, $options = null, $useCache = false)
 * @method TaskResultFieldStringVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskResultFieldStringVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskResultFieldStringVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaskResultFieldStringVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskResultFieldStringVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskResultFieldStringVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class TaskResultFieldStringDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
