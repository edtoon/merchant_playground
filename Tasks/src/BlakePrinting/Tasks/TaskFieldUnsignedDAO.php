<?php

namespace BlakePrinting\Tasks;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static TaskFieldUnsignedDAO instance(PDO $connection = null, $schemaName = null)
 * @method TaskFieldUnsignedVO findById($id, $options = null, $useCache = false)
 * @method TaskFieldUnsignedVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskFieldUnsignedVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskFieldUnsignedVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaskFieldUnsignedVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskFieldUnsignedVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskFieldUnsignedVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class TaskFieldUnsignedDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
