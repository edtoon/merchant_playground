<?php

namespace BlakePrinting\Tasks;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static TaskAttemptDAO instance(PDO $connection = null, $schemaName = null)
 * @method TaskAttemptVO findById($id, $options = null, $useCache = false)
 * @method TaskAttemptVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskAttemptVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskAttemptVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaskAttemptVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskAttemptVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskAttemptVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class TaskAttemptDAO extends AbstractDataAccessObject {
    /**
     * @param int $taskId
     * @return int
     */
    public function &store($taskId) {
        return PDOQuery::insert()
            ->into($this)
            ->value("created", "UNIX_TIMESTAMP()")
            ->value("task_id", $taskId, PDO::PARAM_INT)
            ->executeEnsuringLastInsertId($this->getConnection())
        ;
    }
}
