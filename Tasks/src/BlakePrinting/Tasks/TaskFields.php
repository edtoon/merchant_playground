<?php

namespace BlakePrinting\Tasks;

use BlakePrinting\Fields\FieldTypeDAO;

class TaskFields {
    const FIELD_LAST_INSERT_ID = [
        "domain" => "sys",
        "schema" => "virtual",
        "table" => "abstract",
        "field" => "last_insert_id",
        "type" => FieldTypeDAO::TYPE_UNSIGNED
    ];

    private function __construct() { }
}
