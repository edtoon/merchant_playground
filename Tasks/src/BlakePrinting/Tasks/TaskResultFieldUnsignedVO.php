<?php

namespace BlakePrinting\Tasks;

use BlakePrinting\Fields\AbstractEntityFieldIntVO;

class TaskResultFieldUnsignedVO extends AbstractEntityFieldIntVO {
    /** @var int */
    private $taskAttemptId = null;

    /**
     * @return int
     */
    public function getTaskAttemptId()
    {
        return $this->taskAttemptId;
    }

    /**
     * @param int $taskAttemptId
     */
    public function setTaskAttemptId($taskAttemptId)
    {
        $this->taskAttemptId = $taskAttemptId;
    }
}
