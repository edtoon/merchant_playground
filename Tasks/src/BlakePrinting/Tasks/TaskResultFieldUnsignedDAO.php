<?php

namespace BlakePrinting\Tasks;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static TaskResultFieldUnsignedDAO instance(PDO $connection = null, $schemaName = null)
 * @method TaskResultFieldUnsignedVO findById($id, $options = null, $useCache = false)
 * @method TaskResultFieldUnsignedVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskResultFieldUnsignedVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskResultFieldUnsignedVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaskResultFieldUnsignedVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskResultFieldUnsignedVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskResultFieldUnsignedVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class TaskResultFieldUnsignedDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
