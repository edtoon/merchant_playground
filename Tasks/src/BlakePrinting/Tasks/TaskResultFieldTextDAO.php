<?php

namespace BlakePrinting\Tasks;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static TaskResultFieldTextDAO instance(PDO $connection = null, $schemaName = null)
 * @method TaskResultFieldTextVO findById($id, $options = null, $useCache = false)
 * @method TaskResultFieldTextVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskResultFieldTextVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskResultFieldTextVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaskResultFieldTextVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskResultFieldTextVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskResultFieldTextVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class TaskResultFieldTextDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
