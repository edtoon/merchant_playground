<?php

namespace BlakePrinting\Tasks;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static TaskResultDAO instance(PDO $connection = null, $schemaName = null)
 * @method TaskResultVO findById($id, $options = null, $useCache = false)
 * @method TaskResultVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskResultVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskResultVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaskResultVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskResultVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskResultVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class TaskResultDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, "task_attempt_id");
    }

    /**
     * @param int $taskAttemptId
     * @param int $code
     * @return int
     */
    public function &store($taskAttemptId, $code) {
        return PDOQuery::insert()
            ->into($this)
            ->value("created", "UNIX_TIMESTAMP()")
            ->value("task_attempt_id", $taskAttemptId, PDO::PARAM_INT)
            ->value("code", $code, PDO::PARAM_INT)
            ->executeEnsuringRowCount($this->getConnection(), 1)
        ;
    }
}
