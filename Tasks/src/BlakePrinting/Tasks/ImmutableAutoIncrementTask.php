<?php

namespace BlakePrinting\Tasks;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Db\ValueObject;
use BlakePrinting\Fields\FieldUtil;
use BlakePrinting\Fields\FieldVO;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use Exception;
use Monolog\Logger;
use PDO;
use ReflectionClass;
use ReflectionMethod;

abstract class ImmutableAutoIncrementTask extends AbstractTask {
    /** @var FieldVO */
    private $lastInsertIdFieldVo = null;
    /** @var AbstractDataAccessObject */
    private $dataAccessObject = null;
    /** @var int */
    private $batchSize = 1000;
    /** @var int */
    private $lastRecordedId = null;
    /** @var int */
    private $currentMaxId = null;

    public function evaluate() {
        PDOUtil::pauseSqlLogging();

        try {
            $connection =& $this->getConnection();
            $taskFieldUnsignedDao =& TaskFieldUnsignedDAO::instance($connection);
            $taskId = $this->getTaskVo()->getId();
            $this->lastRecordedId =& PDOQuery::select()
                ->column("value")
                ->from($taskFieldUnsignedDao)
                ->where("task_id", $taskId, PDO::PARAM_INT)
                ->where("field_id", $this->getLastInsertIdFieldVo()->getId(), PDO::PARAM_INT)
                ->options("FOR UPDATE")
                ->getSingleValue($connection)
            ;

            if($this->lastRecordedId !== null) {
                $this->currentMaxId =& $this->getLastId($this->dataAccessObject);

                if($this->currentMaxId !== null && $this->currentMaxId <= $this->lastRecordedId) {
                    return false;
                }
            }
        } finally {
            PDOUtil::resumeSqlLogging();
        }

        return true;
    }

    /**
     * @param ValueObject $valueObject
     * @return int
     */
    protected abstract function executeForValueObject(ValueObject &$valueObject);

    public final function execute() {
        $logger =& LogUtil::getLogger();
        $idColumnName = $this->dataAccessObject->getIdColumnName();
        $iterator = null;
        $lastVo = null;
        $code = 0;

        PDOUtil::pauseSqlLogging();

        try {
            $logger->debug("Checking from id: " . ($this->lastRecordedId ?: 0));

            $iterator =& $this->dataAccessObject->iterate(
                function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$idColumnName) {
                    $pdoQuery->where($idColumnName, ($this->lastRecordedId ?: 0), PDO::PARAM_INT, ">");
                },
                $idColumnName . " ASC", $this->batchSize
            );
        } finally {
            PDOUtil::resumeSqlLogging();
        }

        if($iterator !== null) {
            $executedCount = 0;

            foreach($iterator as $valueObject) {
                try {
                    $code = $this->executeForValueObject($valueObject);
                    $lastVo =& $valueObject;

                    $executedCount++;
                } catch(Exception $e) {
                    LogUtil::logException(Logger::ERROR, $e, "Error executing " . $this->getName() . " for value object: " . Utils::getVarDump($valueObject));

                    $code = 1;
                }

                if($code !== 0) {
                    break;
                }
            }

            if($code === 0 && $executedCount > 0) {
                $logger->debug("Executed for " . $executedCount . " value objects");
            } else if($code !== 0) {
                $logger->debug("Failed after " . $executedCount . " . executions");
            }
        }

        if($lastVo !== null) {
            $reflectedColumns = $this->dataAccessObject->getReflectedColumns();

            foreach($reflectedColumns as $propertyName => &$reflectedColumnData) {
                $columnName = $reflectedColumnData[0];

                if($columnName === $idColumnName) {
                    PDOUtil::pauseSqlLogging();

                    try {
                        $taskFieldUnsignedDao =& TaskFieldUnsignedDAO::instance($this->getConnection());
                        /** @var ReflectionMethod $getMethod */
                        $getMethod = $reflectedColumnData[2];
                        $id = $getMethod->invoke($lastVo);
                        $taskFieldUnsignedVo = new TaskFieldUnsignedVO();
                        $taskId = $this->getTaskVo()->getId();
                        $fieldId = $this->getLastInsertIdFieldVo()->getId();

                        $taskFieldUnsignedVo->setTaskId($taskId);
                        $taskFieldUnsignedVo->setFieldId($fieldId);
                        $taskFieldUnsignedVo->setValue($id);

                        if($this->lastRecordedId === null) {
                            $logger->debug("Inserting " . $columnName . " field #" . $fieldId . ", task #" . $taskId . ", value: " . $id);
                            $taskFieldUnsignedDao->insert($taskFieldUnsignedVo);
                        } else {
                            $logger->debug("Updating " . $columnName . " field #" . $fieldId . ", task #" . $taskId . ", value: " . $id);
                            $taskFieldUnsignedDao->update($taskFieldUnsignedVo, null, ["value"], ["task_id", "field_id"]);
                        }
                    } finally {
                        PDOUtil::resumeSqlLogging();
                    }

                    break;
                }
            }

            unset($reflectedColumnData);
        }

        return $code;
    }

    /**
     * @return FieldVO
     */
    private function getLastInsertIdFieldVo() {
        if($this->lastInsertIdFieldVo === null) {
            $this->lastInsertIdFieldVo =& FieldUtil::findFieldVoBySpec(
                TaskFields::FIELD_LAST_INSERT_ID, $this->getConnection()
            );
        }

        return $this->lastInsertIdFieldVo;
    }

    private function &getLastId(AbstractDataAccessObject &$dataAccessObject) {
        $idColumnName = $dataAccessObject->getIdColumnName();

        return PDOQuery::select()
            ->column($idColumnName)
            ->from($dataAccessObject)
            ->orderBy($idColumnName . " DESC")
            ->limit(1)
            ->getSingleValue($dataAccessObject->getConnection())
        ;
    }

    /**
     * @return AbstractDataAccessObject
     */
    public function getDataAccessObject()
    {
        return $this->dataAccessObject;
    }

    /**
     * @param AbstractDataAccessObject $dataAccessObject
     */
    public function setDataAccessObject($dataAccessObject)
    {
        $this->dataAccessObject = $dataAccessObject;
    }

    /**
     * @return int
     */
    public function getBatchSize()
    {
        return $this->batchSize;
    }

    /**
     * @param int $batchSize
     */
    public function setBatchSize($batchSize)
    {
        $this->batchSize = $batchSize;
    }

    /**
     * @return int
     */
    public function getLastRecordedId()
    {
        return $this->lastRecordedId;
    }

    /**
     * @param int $lastRecordedId
     */
    public function setLastRecordedId($lastRecordedId)
    {
        $this->lastRecordedId = $lastRecordedId;
    }

    /**
     * @return int
     */
    public function getCurrentMaxId()
    {
        return $this->currentMaxId;
    }

    /**
     * @param int $currentMaxId
     */
    public function setCurrentMaxId($currentMaxId)
    {
        $this->currentMaxId = $currentMaxId;
    }
}
