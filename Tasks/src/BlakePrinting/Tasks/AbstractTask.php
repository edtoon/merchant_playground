<?php

namespace BlakePrinting\Tasks;

use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Fields\FieldVO;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use Exception;
use Monolog\Logger;
use PDO;

abstract class AbstractTask implements Task {
    /** @var string */
    private $name = null;
    /** @var string */
    private $appName = null;
    /** @var string */
    private $appVersion = null;
    /** @var int */
    private $lockExpirationSeconds = null;
    /** @var PDO */
    private $connection = null;
    /** @var int */
    private $minIntervalSeconds = null;
    /** @var TaskVO */
    private $taskVo = null;
    /** @var string */
    private $lockUuid = null;
    /** @var int */
    private $taskAttemptId = null;
    /** @var array */
    private $stringResultFields = [];
    /** @var array */
    private $textResultFields = [];
    /** @var array */
    private $unsignedResultFields = [];
    /** @var array */
    private $signedResultFields = [];

    /**
     * @param string $name
     * @param string $appName
     * @param string $appVersion
     * @param int $lockExpirationSeconds
     * @param PDO $connection
     * @param int $minIntervalSeconds
     */
    public function __construct($name, $appName, $appVersion, $lockExpirationSeconds, PDO $connection, $minIntervalSeconds = 60) {
        $this->name = $name;
        $this->appName = $appName;
        $this->appVersion = $appVersion;
        $this->lockExpirationSeconds = $lockExpirationSeconds;
        $this->connection = $connection;
        $this->minIntervalSeconds = $minIntervalSeconds;
    }

    /**
     * @return bool
     */
    public function evaluate() {
        return true;
    }

    /**
     * @return int
     */
    protected abstract function execute();

    /**
     * @param FieldVO $fieldVo
     * @param string $value
     */
    public final function addTaskFieldString(FieldVO &$fieldVo, $value) {
        $connection =& $this->getConnection();
        $dao =& TaskFieldStringDAO::instance($connection);
        $vo = new TaskFieldStringVO();

        $vo->setTaskId($this->getTaskVo()->getId());
        $vo->setFieldId($fieldVo->getId());
        $vo->setValue($value);

        $dao->insert($vo);
    }

    /**
     * @param FieldVO $fieldVo
     * @param string $value
     */
    public final function addTaskFieldText(FieldVO &$fieldVo, $value) {
        $connection =& $this->getConnection();
        $dao =& TaskFieldTextDAO::instance($connection);
        $vo = new TaskFieldTextVO();

        $vo->setTaskId($this->getTaskVo()->getId());
        $vo->setFieldId($fieldVo->getId());
        $vo->setValue($value);

        $dao->insert($vo);
    }

    /**
     * @param FieldVO $fieldVo
     * @param int $value
     */
    public final function addTaskFieldUnsigned(FieldVO &$fieldVo, $value) {
        $connection =& $this->getConnection();
        $dao =& TaskFieldUnsignedDAO::instance($connection);
        $vo = new TaskFieldUnsignedVO();

        $vo->setTaskId($this->getTaskVo()->getId());
        $vo->setFieldId($fieldVo->getId());
        $vo->setValue($value);

        $dao->insert($vo);
    }

    /**
     * @param FieldVO $fieldVo
     * @param int $value
     */
    public final function addTaskFieldSigned(FieldVO &$fieldVo, $value) {
        $connection =& $this->getConnection();
        $dao =& TaskFieldSignedDAO::instance($connection);
        $vo = new TaskFieldSignedVO();

        $vo->setTaskId($this->getTaskVo()->getId());
        $vo->setFieldId($fieldVo->getId());
        $vo->setValue($value);

        $dao->insert($vo);
    }

    /**
     * @param FieldVO $fieldVo
     * @param string $value
     */
    public final function addAttemptFieldString(FieldVO &$fieldVo, $value) {
        $connection =& $this->getConnection();
        $dao =& TaskAttemptFieldStringDAO::instance($connection);
        $vo = new TaskAttemptFieldStringVO();

        $vo->setTaskAttemptId($this->taskAttemptId);
        $vo->setFieldId($fieldVo->getId());
        $vo->setValue($value);

        $dao->insert($vo);
    }

    /**
     * @param FieldVO $fieldVo
     * @param string $value
     */
    public final function addAttemptFieldText(FieldVO &$fieldVo, $value) {
        $connection =& $this->getConnection();
        $dao =& TaskAttemptFieldTextDAO::instance($connection);
        $vo = new TaskAttemptFieldTextVO();

        $vo->setTaskAttemptId($this->taskAttemptId);
        $vo->setFieldId($fieldVo->getId());
        $vo->setValue($value);

        $dao->insert($vo);
    }

    /**
     * @param FieldVO $fieldVo
     * @param int $value
     */
    public final function addAttemptFieldUnsigned(FieldVO &$fieldVo, $value) {
        $connection =& $this->getConnection();
        $vo = new TaskAttemptFieldUnsignedVO();

        $vo->setTaskAttemptId($this->taskAttemptId);
        $vo->setFieldId($fieldVo->getId());
        $vo->setValue($value);

        TaskAttemptFieldUnsignedDAO::instance($connection)->insert($vo);
    }

    /**
     * @param FieldVO $fieldVo
     * @param int $value
     */
    public final function addAttemptFieldSigned(FieldVO &$fieldVo, $value) {
        $connection =& $this->getConnection();
        $vo = new TaskAttemptFieldSignedVO();

        $vo->setTaskAttemptId($this->taskAttemptId);
        $vo->setFieldId($fieldVo->getId());
        $vo->setValue($value);

        TaskAttemptFieldSignedDAO::instance($connection)->insert($vo);
    }

    /**
     * @param FieldVO $fieldVo
     * @param string $value
     */
    public final function addResultFieldString(FieldVO &$fieldVo, $value) {
        $fieldId = $fieldVo->getId();

        if(array_key_exists($fieldId, $this->stringResultFields)) {
            $this->stringResultFields[$fieldId][] = $value;
        } else {
            $this->stringResultFields[$fieldId] = [$value];
        }
    }

    /**
     * @param FieldVO $fieldVo
     * @param string $value
     */
    public final function addResultFieldText(FieldVO &$fieldVo, $value) {
        $fieldId = $fieldVo->getId();

        if(array_key_exists($fieldId, $this->textResultFields)) {
            $this->textResultFields[$fieldId][] = $value;
        } else {
            $this->textResultFields[$fieldId] = [$value];
        }
    }

    /**
     * @param FieldVO $fieldVo
     * @param int $value
     */
    public final function addResultFieldUnsigned(FieldVO &$fieldVo, $value) {
        $fieldId = $fieldVo->getId();

        if(array_key_exists($fieldId, $this->unsignedResultFields)) {
            $this->unsignedResultFields[$fieldId][] = $value;
        } else {
            $this->unsignedResultFields[$fieldId] = [$value];
        }
    }

    /**
     * @param FieldVO $fieldVo
     * @param int $value
     */
    public final function addResultFieldSigned(FieldVO &$fieldVo, $value) {
        $fieldId = $fieldVo->getId();

        if(array_key_exists($fieldId, $this->signedResultFields)) {
            $this->signedResultFields[$fieldId][] = $value;
        } else {
            $this->signedResultFields[$fieldId] = [$value];
        }
    }

    /**
     * @inheritdoc
     */
    public final function run() {
        if($this->lockUuid === null) {
            throw new Exception("Must acquire task lock before running");
        }

        $logger =& LogUtil::getLogger();
        $connection =& $this->getConnection();
        $taskStatusDao =& TaskStatusDAO::instance($connection);
        $taskId = $this->getTaskVo()->getId();
        $taskName = $this->getName();
        $attemptEvaluate = true;
        $code = 0;

        if(!empty($this->minIntervalSeconds)) {
            PDOUtil::pauseSqlLogging();

            try {
                $taskStatusVo =& $taskStatusDao->findByColumn("task_id", $taskId, PDO::PARAM_INT, null, null, "FOR UPDATE");

                if($taskStatusVo !== null) {
                    $lastUpdated = $taskStatusVo->getUpdated();
                    $currentTime = PDOUtil::getCurrentTimestamp($connection);

                    if(($currentTime - $lastUpdated) < $this->minIntervalSeconds) {
                        $attemptEvaluate = false;
                    }
                }
            } finally {
                PDOUtil::resumeSqlLogging();
            }
        }

        if($attemptEvaluate) {
            $attemptExecute = false;

            try {
                $attemptExecute = $this->evaluate();
            } catch(Exception $e) {
                LogUtil::logException(Logger::ERROR, $e, "Error evaluating task: " . $taskName);

                $code = 1;
            }

            if($attemptExecute) {
                $logger->info("Task: " . $taskName . " attempting");

                PDOUtil::pauseSqlLogging();

                try {
                    $this->taskAttemptId =& TaskAttemptDAO::instance($connection)->store($taskId);
                } finally {
                    PDOUtil::resumeSqlLogging();
                }

                try {
                    $code = $this->execute();
                } catch(Exception $e) {
                    LogUtil::logException(Logger::ERROR, $e, "Error executing task: " . $taskName);

                    $code = 1;
                }

                PDOUtil::pauseSqlLogging();

                try {
                    $taskStatusDao->store($taskId, $code);

                    TaskResultDAO::instance($connection)->store($this->taskAttemptId, $code);

                    $this->storeResultStringFields();
                    $this->storeResultTextFields();
                    $this->storeResultUnsignedFields();
                    $this->storeResultSignedFields();
                } finally {
                    PDOUtil::resumeSqlLogging();
                }

                $logger->info("Task: " . $taskName . " result code: " . $code);
            }
        }

        return $code;
    }

    private final function storeResultStringFields() {
        if(!empty($this->stringResultFields)) {
            $dao =& TaskResultFieldStringDAO::instance($this->getConnection());

            foreach($this->stringResultFields as $fieldId => &$fieldValues) {
                foreach($fieldValues as &$value) {
                    $vo = new TaskResultFieldStringVO();

                    $vo->setTaskAttemptId($this->taskAttemptId);
                    $vo->setFieldId($fieldId);
                    $vo->setValue($value);

                    $dao->insert($vo);
                }

                unset($value);
            }

            unset($fieldValues);
        }
    }

    private final function storeResultTextFields() {
        if(!empty($this->textResultFields)) {
            $dao =& TaskResultFieldTextDAO::instance($this->getConnection());

            foreach($this->textResultFields as $fieldId => &$fieldValues) {
                foreach($fieldValues as &$value) {
                    $vo = new TaskResultFieldTextVO();

                    $vo->setTaskAttemptId($this->taskAttemptId);
                    $vo->setFieldId($fieldId);
                    $vo->setValue($value);

                    $dao->insert($vo);
                }

                unset($value);
            }

            unset($fieldValues);
        }
    }

    private final function storeResultUnsignedFields() {
        if(!empty($this->unsignedResultFields)) {
            $dao =& TaskResultFieldUnsignedDAO::instance($this->getConnection());

            foreach($this->unsignedResultFields as $fieldId => &$fieldValues) {
                foreach($fieldValues as &$value) {
                    $vo = new TaskResultFieldUnsignedVO();

                    $vo->setTaskAttemptId($this->taskAttemptId);
                    $vo->setFieldId($fieldId);
                    $vo->setValue($value);

                    $dao->insert($vo);
                }

                unset($value);
            }

            unset($fieldValues);
        }
    }

    private final function storeResultSignedFields() {
        if(!empty($this->signedResultFields)) {
            $dao =& TaskResultFieldSignedDAO::instance($this->getConnection());

            foreach($this->signedResultFields as $fieldId => &$fieldValues) {
                foreach($fieldValues as &$value) {
                    $vo = new TaskResultFieldSignedVO();

                    $vo->setTaskAttemptId($this->taskAttemptId);
                    $vo->setFieldId($fieldId);
                    $vo->setValue($value);

                    $dao->insert($vo);
                }

                unset($value);
            }

            unset($fieldValues);
        }
    }

    /**
     * @inheritdoc
     */
    public final function lock() {
        if($this->lockUuid === null) {
            PDOUtil::pauseSqlLogging();

            try {
                $taskName = $this->getName();
                $connection =& $this->getConnection();
                $taskDao =& TaskDAO::instance($connection);
                $taskVo = new TaskVO();

                $taskVo->setName($taskName);

                $taskVo =& $taskDao->findOrCreate($taskVo);
                $taskId = ($taskVo === null ? null : $taskVo->getId());

                $this->taskVo =& $taskVo;

                $lockUuid = Utils::getDockerContainerId();

                if($lockUuid === null) {
                    throw new Exception("Can't find docker container ID, aborting task");
                }

                $taskLockDao =& TaskLockDAO::instance($connection);
                $taskLockVo =& $taskLockDao->findByColumn("task_id", $taskId, PDO::PARAM_INT, null, null, "FOR UPDATE");
                $locked = false;

                if($taskLockVo === null) {
                    $locked = ($taskLockDao->acquire($taskId, $lockUuid, $this->getLockExpirationSeconds()) > 0);
                } else if($lockUuid === $taskLockVo->getLockUuid()) {
                    $locked = ($taskLockDao->refresh($taskLockVo, $this->getLockExpirationSeconds()) > 0);

                    if(!$locked) {
                        $logger =& LogUtil::getLogger();

                        $logger->error("Task: " . $taskName . " already locked but expiration could not be extended");
                    }
                }

                if($locked) {
                    $this->lockUuid = $lockUuid;
                }
            } finally {
                PDOUtil::resumeSqlLogging();
            }
        }

        return ($this->lockUuid !== null);
    }

    /**
     * @inheritdoc
     */
    public final function unlock() {
        if($this->lockUuid !== null) {
            PDOUtil::pauseSqlLogging();

            try {
                $connection =& $this->getConnection();
                $taskLockDao =& TaskLockDAO::instance($connection);

                PDOQuery::delete()
                    ->from($taskLockDao)
                    ->where("task_id", $this->getTaskVo()->getId(), PDO::PARAM_INT)
                    ->where("lock_uuid", $this->lockUuid, PDO::PARAM_INT)
                    ->executeGetRowCount($connection)
                ;

                $this->lockUuid = null;
            } finally {
                PDOUtil::resumeSqlLogging();
            }
        }
    }

    /**
     * @return TaskVO
     */
    public function getTaskVo()
    {
        return $this->taskVo;
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAppName()
    {
        return $this->appName;
    }

    /**
     * @param string $appName
     */
    public function setAppName($appName)
    {
        $this->appName = $appName;
    }

    /**
     * @return string
     */
    public function getAppVersion()
    {
        return $this->appVersion;
    }

    /**
     * @param string $appVersion
     */
    public function setAppVersion($appVersion)
    {
        $this->appVersion = $appVersion;
    }

    /**
     * @return int
     */
    public function getLockExpirationSeconds()
    {
        return $this->lockExpirationSeconds;
    }

    /**
     * @param int $lockExpirationSeconds
     */
    public function setLockExpirationSeconds($lockExpirationSeconds)
    {
        $this->lockExpirationSeconds = $lockExpirationSeconds;
    }

    /**
     * @return int
     */
    public function getMinIntervalSeconds()
    {
        return $this->minIntervalSeconds;
    }

    /**
     * @param int $minIntervalSeconds
     */
    public function setMinIntervalSeconds($minIntervalSeconds)
    {
        $this->minIntervalSeconds = $minIntervalSeconds;
    }

    /**
     * @return PDO
     */
    public function &getConnection()
    {
        return $this->connection;
    }

    /**
     * @param PDO $connection
     */
    public function setConnection(PDO $connection)
    {
        $this->connection = $connection;
    }
}
