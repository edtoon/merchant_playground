<?php

namespace BlakePrinting\Tasks;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static TaskStatusDAO instance(PDO $connection = null, $schemaName = null)
 * @method TaskStatusVO findById($id, $options = null, $useCache = false)
 * @method TaskStatusVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskStatusVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskStatusVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaskStatusVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskStatusVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskStatusVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class TaskStatusDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, "task_id");
    }

    /**
     * @param int $taskId
     * @param int $code
     * @return int
     */
    public function &store($taskId, $code) {
        return PDOQuery::insert()
            ->into($this)
            ->value("task_id", $taskId, PDO::PARAM_INT)
            ->value("updated", "UNIX_TIMESTAMP()")
            ->value("code", $code, PDO::PARAM_INT)
            ->onDuplicateAllValues()
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
