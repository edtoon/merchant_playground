<?php

namespace BlakePrinting\Tasks;

use BlakePrinting\Db\ValueObject;

class TaskLockVO implements ValueObject {
    /** @var int */
    private $taskId = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var string */
    private $lockUuid = null;
    /** @var int */
    private $expiration = null;

    /**
     * @return int
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * @param int $taskId
     */
    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getLockUuid()
    {
        return $this->lockUuid;
    }

    /**
     * @param string $lockUuid
     */
    public function setLockUuid($lockUuid)
    {
        $this->lockUuid = $lockUuid;
    }

    /**
     * @return int
     */
    public function getExpiration()
    {
        return $this->expiration;
    }

    /**
     * @param int $expiration
     */
    public function setExpiration($expiration)
    {
        $this->expiration = $expiration;
    }
}
