<?php

namespace BlakePrinting\Tasks;

use BlakePrinting\Db\ValueObject;

class TaskResultVO implements ValueObject {
    /** @var int */
    private $taskAttemptId = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $code = null;

    /**
     * @return int
     */
    public function getTaskAttemptId()
    {
        return $this->taskAttemptId;
    }

    /**
     * @param int $taskAttemptId
     */
    public function setTaskAttemptId($taskAttemptId)
    {
        $this->taskAttemptId = $taskAttemptId;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }
}
