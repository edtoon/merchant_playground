<?php

namespace BlakePrinting\Tasks;

abstract class MutableAutoIncrementTask extends ImmutableAutoIncrementTask {
    /** @var int */
    private $catchupBatchSize = null;

    public function evaluate() {
        $immutableResult = parent::evaluate();

        if(!$immutableResult) {
            $currentMaxId = $this->getCurrentMaxId();
            $lastRecordedId = $this->getLastRecordedId();

            if($currentMaxId !== null && $currentMaxId <= $lastRecordedId) {
                $this->setLastRecordedId(0);

                if($this->catchupBatchSize !== null) {
                    $this->setBatchSize($this->catchupBatchSize);
                }

                return true;
            }
        }

        return $immutableResult;
    }

    /**
     * @return int
     */
    public function getCatchupBatchSize()
    {
        return $this->catchupBatchSize;
    }

    /**
     * @param int $catchupBatchSize
     */
    public function setCatchupBatchSize($catchupBatchSize)
    {
        $this->catchupBatchSize = $catchupBatchSize;
    }
}
