<?php

namespace BlakePrinting\Tasks;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static TaskFieldStringDAO instance(PDO $connection = null, $schemaName = null)
 * @method TaskFieldStringVO findById($id, $options = null, $useCache = false)
 * @method TaskFieldStringVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskFieldStringVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskFieldStringVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaskFieldStringVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaskFieldStringVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaskFieldStringVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class TaskFieldStringDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
