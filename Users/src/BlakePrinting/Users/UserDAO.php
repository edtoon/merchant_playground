<?php

namespace BlakePrinting\Users;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use Exception;
use PDO;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;

/**
 * @method static UserDAO instance(PDO $connection = null, $schemaName = null)
 * @method UserVO findById($id, $options = null, $useCache = false)
 * @method UserVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method UserVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method UserVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method UserVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method UserVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method UserVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class UserDAO extends AbstractDataAccessObject {
    const BCRYPT_COST = 14;

    /** @var BCryptPasswordEncoder */
    private static $encoder = null;

    /**
     * @return BCryptPasswordEncoder
     */
    public static function getPasswordEncoder() {
        if(self::$encoder === null) {
            self::$encoder = new BCryptPasswordEncoder(self::BCRYPT_COST);
        }

        return self::$encoder;
    }

    /**
     * @param string $username
     * @return UserVO
     */
    public function &findByUsername($username) {
        return $this->findByColumn("name", $username, PDO::PARAM_STR, "id DESC", 1);
    }

    /**
     * @param int $userId
     * @param string $password
     * @return int
     * @throws Exception
     */
    public function setPassword($userId, $password) {
        if(empty($password)) {
            throw new Exception("Password must not be empty");
        } else if(strlen($password) < 6) {
            throw new Exception("Password too short");
        }

        return PDOQuery::update()
            ->table($this)
            ->set("password", self::getPasswordEncoder()->encodePassword($password, ""), PDO::PARAM_STR)
            ->where("id", $userId, PDO::PARAM_INT)
            ->executeEnsuringRowCount($this->getConnection(), 1)
        ;
    }

    /**
     * @param string $username
     * @param string $password
     * @return int
     * @throws Exception
     */
    public function create($username, $password) {
        if(empty($username)) {
            throw new Exception("Username must not be empty");
        }

        if(empty($password)) {
            throw new Exception("Password must not be empty");
        } else if(strlen($password) < 6) {
            throw new Exception("Password too short");
        }

        return PDOQuery::insert()
            ->into($this)
            ->value("created", "UNIX_TIMESTAMP()")
            ->value("name", $username, PDO::PARAM_INT)
            ->value("password", self::getPasswordEncoder()->encodePassword($password, ""), PDO::PARAM_STR)
            ->executeEnsuringLastInsertId($this->getConnection())
        ;
    }
}
