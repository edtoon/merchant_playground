<?php

namespace BlakePrinting\Users;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static UserRoleDAO instance(PDO $connection = null, $schemaName = null)
 * @method UserRoleVO findById($id, $options = null, $useCache = false)
 * @method UserRoleVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method UserRoleVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method UserRoleVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method UserRoleVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method UserRoleVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method UserRoleVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class UserRoleDAO extends AbstractDataAccessObject {
    const ROLE_ADMIN = "ROLE_ADMIN";
    const ROLE_AUTH = "ROLE_AUTH";
    const ROLE_CATALOG = "ROLE_CATALOG";
    const ROLE_FINANCE = "ROLE_FINANCE";
    const ROLE_INVENTORY = "ROLE_INVENTORY";
    const ROLE_SUPERADMIN = "ROLE_SUPERADMIN";
    const ROLE_TRIAL = "ROLE_TRIAL";

    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $userId
     * @param string $role
     * @return int
     */
    public function addRole($userId, $role) {
        return PDOQuery::insert()->ignore()->into($this)
            ->value("user_id", $userId, PDO::PARAM_INT)
            ->value("role", $role, PDO::PARAM_STR)
            ->executeGetRowCount($this->getConnection())
        ;
    }

    /**
     * @param int $userId
     * @return string[]
     */
    public function &listRoles($userId) {
        $results =& PDOQuery::select()->distinct()->column("role")
            ->from($this)
            ->where("user_id", $userId, PDO::PARAM_INT)
            ->getResults($this->getConnection())
        ;
        $roles = [];

        foreach($results as &$result) {
            $roles[] = $result["role"];
        }

        unset($result);

        return $roles;
    }

    /**
     * @param int $userId
     * @param string $role
     * @return int
     */
    public function removeRole($userId, $role) {
        return PDOQuery::delete()->from($this)
            ->where("user_id", $userId, PDO::PARAM_INT)
            ->where("role", $role, PDO::PARAM_STR)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
