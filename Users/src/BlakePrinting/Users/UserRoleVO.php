<?php

namespace BlakePrinting\Users;

use BlakePrinting\Db\ValueObject;

class UserRoleVO implements ValueObject {
    /** @var int */
    private $userId = null;
    /** @var string */
    private $role = null;

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }
}
