<?php

namespace BlakePrinting\Users;

use BlakePrinting\Db\ByteLimit;
use BlakePrinting\Db\ValueObject;
use Phinx\Db\Adapter\MysqlAdapter;
use Stringy\StaticStringy as S;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class UserVO implements ValueObject {
    const INVALID_NAMES = [
        "3pl", "amazon", "amz", "amzn", "apple", "about", "abuse", "account", "accounts", "accounting", "accountant",
        "adm", "accountants", "admin", "administrator", "ad", "ads", "advertising", "affiliate", "affiliates", "add",
        "atom", "address", "ajax", "analytics", "android", "archive", "avatar", "all", "anon", "anonymous", "api",
        "apis", "asset", "assets", "app", "apps", "auth", "authentication", "access", "backup", "backups", "beta",
        "bin", "banner", "banners", "billing", "blog", "blogs", "board", "boards", "bot", "bots", "business", "book",
        "books", "calendar", "calendars", "catalog", "catalogs", "campaign", "campaigns", "career", "careers", "cgi",
        "choice", "choices", "client", "clients", "cliente", "config", "connect", "contact", "contacts", "crm",
        "contest", "contests", "content", "contents", "chat", "cache", "code", "commercial", "commercials", "comercial",
        "compare", "create", "compras", "css", "cdn", "conference", "conferences", "data", "dir", "dirs", "directory",
        "directories", "delete", "dash", "dashboard", "demo", "demos", "demonstration", "demonstrations", "design",
        "designs", "designer", "designers", "dl", "dev", "devs", "devel", "developer", "developers", "development",
        "doc", "docs", "document", "documents", "documentation", "domain", "domains", "download", "downloads", "ebay",
        "edit", "edits", "editor", "editors", "email", "emails", "ecommerce", "e-commerce", "faq", "faqs", "feed",
        "feedback", "feeds", "file", "files", "forum", "forums", "facebook", "fb", "free", "ftp", "forward", "freight",
        "forwarding", "google", "group", "groups", "guest", "guests", "hacks", "hacking", "help", "home", "homepage",
        "homepages", "host", "hosting", "hostmaster", "hr", "hostname", "html", "http", "httpd", "https", "info",
        "information", "image", "index", "it", "images", "imap", "img", "invite", "invites", "intranet", "internal",
        "ipad", "iphone", "irc", "job", "jobs", "javascript", "js", "kb", "knowledgebase", "log", "label", "labels",
        "logs", "login", "logins", "logout", "logoff", "logistics", "list", "lists", "mail", "mail1", "mail2", "mail3",
        "mail4", "mail5", "mail6", "mail7", "mail8", "mail9", "mailer", "mailers", "mailing", "mailings", "majordomo",
        "manager", "marketing", "master", "me", "media", "msg", "message", "messages", "mx", "mysql", "messenger",
        "mob", "mobile", "my", "mine", "name", "named", "net", "network", "networking", "new", "news", "newsletter",
        "newsletters", "nickname", "nicknames", "noc", "note", "notes", "noticias", "ns", "ns1", "ns2", "ns3", "ns4",
        "ns5", "ns6", "ns7", "ns8", "ns9", "old", "online", "operator", "operators", "order", "orders", "option",
        "options", "page", "pager", "pagers", "pages", "panel", "password", "passwords", "pic", "pics", "picture",
        "pictures", "photo", "photos", "php", "plugin", "plugins", "pop", "pop3", "post", "postage", "posts", "pub",
        "public", "postmaster", "postfix", "profile", "project", "projects", "promo", "promos", "price", "prices",
        "pricing", "prize", "prizes", "quit", "quote", "quotes", "random", "reset", "root", "register", "remove",
        "removal", "removals", "registration", "registrations", "rss", "sale", "sales", "sample", "samples", "script",
        "scripts", "send", "sender", "senders", "secure", "security", "service", "services", "ssl", "shop", "shops",
        "shopping", "ship", "shipping", "sql", "signup", "signups", "signin", "search", "searches", "setup", "setting",
        "settings", "site", "sites", "sitemap", "sitemaps", "smtp", "soporte", "ssh", "stage", "staging", "start",
        "subscribe", "subscribers", "suporte", "stat", "stats", "status", "subscription", "subscriptions", "subdomain",
        "subdomains", "static", "statistic", "statistics", "sys", "store", "stores", "stop", "sysadmin", "sysadmins",
        "system", "systems", "sysop", "sysops", "staff", "staffs", "staffer", "staffers", "staffing", "skype", "signon",
        "sso", "tablet", "tablets", "tech", "technology", "telnet", "test", "tests", "tmp", "testing", "test1", "test2",
        "test3", "test4", "test5", "test6", "test7", "test8", "test9", "theme", "themes", "temp", "temporary", "todo",
        "todos", "twitter", "task", "tasks", "tool", "tools", "talk", "tv", "unsub", "uri", "url", "urls", "urn",
        "unsubscribe", "update", "updates", "upload", "uploads", "user", "users", "username", "usernames", "usenet",
        "usuario", "usuarios", "usage", "uucp", "vendas", "ventas", "video", "videos", "visitor", "visitors", "win",
        "winner", "winners", "webmaster", "webstore", "webstores", "website", "websites", "web", "webmail", "www",
        "www1", "www2", "www3", "www4", "www5", "www6", "www7", "www", "www9", "wwww", "wws", "wwws", "workshop",
        "workshops", "you", "yourname", "yourusername", "yoursite", "yourdomain", "zshop", "zshops"
    ];

    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var string */
    private $name = null;
    /** @var string */
    private $password = null;
    /** @var bool */
    private $enabled = null;
    /** @var bool */
    private $locked = null;
    /** @var int */
    private $expiration = null;
    /** @var int */
    private $timezoneId = null;

    public static function loadValidatorMetadata(ClassMetadata $metadata) {
        $metadata->addGetterConstraints(
            "id", [new Assert\Type("digit"), new Assert\Range(["min" => 1, "max" => MysqlAdapter::INT_SMALL])]
        );
        $metadata->addGetterConstraints(
            "created", [new Assert\Type("digit"), new Assert\Range(["min" => 1, "max" => MysqlAdapter::INT_BIG])]
        );
        $metadata->addGetterConstraints(
            "updated", [new Assert\Type("digit"), new Assert\Range(["min" => 1, "max" => MysqlAdapter::INT_BIG])]
        );
        $metadata->addGetterConstraints(
            "name", [
                new Assert\NotNull(), new Assert\NotBlank(), new Assert\Length(["min" => 2, "max" => 25]), new ByteLimit(191),
                new Assert\Callback([self::class, "validateName"])
            ]
        );
        $metadata->addGetterConstraints(
            "password", [new Assert\NotNull(), new Assert\NotBlank(), new Assert\Length(["min" => 6, "max" => 50]), new ByteLimit(50)]
        );
        $metadata->addGetterConstraints(
            "enabled", [new Assert\Type("bool")]
        );
        $metadata->addGetterConstraints(
            "locked", [new Assert\Type("bool")]
        );
        $metadata->addGetterConstraints(
            "expiration", [new Assert\Type("digit"), new Assert\Range(["min" => 0, "max" => MysqlAdapter::INT_BIG])]
        );
        $metadata->addGetterConstraints(
            "timezoneId", [new Assert\Type("digit"), new Assert\Range(["min" => 1, "max" => MysqlAdapter::INT_SMALL])]
        );
    }

    public static function validateName($value, ExecutionContextInterface $context, $payload) {
        $lowerName = (string)S::toLowerCase($value);

        if(!S::isAlphanumeric($value) || S::contains($lowerName, "support") || S::contains($lowerName, "apoyo") ||
            S::contains($lowerName, "soutien") || in_array($lowerName, self::INVALID_NAMES)
        ) {
            $context
                ->buildViolation(
                    "Username rejected - must contain only alphanumeric characters and " .
                    "may not be a commonly used administrative account name."
                )
                ->setInvalidValue($value)
                ->addViolation()
            ;
        }
    }

    public function __construct($name = null, $password = null, $enabled = null, $locked = null, $expiration = null, $timezoneId = null) {
        $this->name = $name;
        $this->password = $password;
        $this->enabled = $enabled;
        $this->locked = $locked;
        $this->expiration = $expiration;
        $this->timezoneId = $timezoneId;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return bool
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * @param bool $locked
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;
    }

    /**
     * @return int
     */
    public function getExpiration()
    {
        return $this->expiration;
    }

    /**
     * @param int $expiration
     */
    public function setExpiration($expiration)
    {
        $this->expiration = $expiration;
    }

    /**
     * @return int
     */
    public function getTimezoneId()
    {
        return $this->timezoneId;
    }

    /**
     * @param int $timezoneId
     */
    public function setTimezoneId($timezoneId)
    {
        $this->timezoneId = $timezoneId;
    }
}
