<?php

use BlakePrinting\Users\UserDAO;
use BlakePrinting\Db\DbMigration;

class UsersIndexEnabledLockedExpiration extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();
        $userDao = new UserDAO($connection);

        $this->alterDao($userDao, "ADD INDEX (enabled), ADD INDEX (locked), ADD INDEX (expiration)");
    }

    public function down() {
        $connection = $this->getConnection();
        $userDao = new UserDAO($connection);

        $this->alterDao($userDao, "DROP INDEX expiration, DROP INDEX locked, DROP INDEX enabled");
    }
}
