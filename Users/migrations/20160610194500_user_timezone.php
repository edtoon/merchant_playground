<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\Globalization\TimezoneDAO;
use BlakePrinting\Users\UserDAO;

class UserTimezone extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();
        $userDao = UserDAO::instance($connection);
        $timezoneDao = TimezoneDAO::instance($connection);

        $this->alterDao(
            $userDao,
            "ADD timezone_id SMALLINT(5) UNSIGNED, " .
            "ADD CONSTRAINT user_ibfk_1 FOREIGN KEY (timezone_id) REFERENCES " . $timezoneDao->formatSchemaAndTableName() . " (id)"
        );
    }

    public function down() {
        $connection =& $this->getConnection();
        $userDao = UserDAO::instance($connection);

        $this->alterDao($userDao, "DROP FOREIGN KEY user_ibfk_1, DROP COLUMN timezone_id");
    }
}
