<?php

use BlakePrinting\Users\UserDAO;
use BlakePrinting\Users\UserRoleDAO;
use BlakePrinting\Db\DbMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class Users extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $userDao = new UserDAO($connection);
        $this->tableDao($userDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("name", "string", ["limit" => 191])
            ->addColumn("password", "binary", ["limit" => 60])
            ->addColumn("enabled", "integer", ["limit" => MysqlAdapter::INT_TINY, "precision" => 1, "default" => 1])
            ->addColumn("locked", "integer", ["limit" => MysqlAdapter::INT_TINY, "precision" => 1, "default" => 0])
            ->addColumn("expiration", "biginteger", ["signed" => false, "null" => true])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $userRoleDao = new UserRoleDAO($connection);
        $this->tableDao($userRoleDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["user_id", "role"]])
            ->addColumn("user_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("role", "string", ["limit" => 191])
            ->addForeignKey("user_id", $userDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("role")
            ->save()
        ;
    }

    public function down() {
        $connection = $this->getConnection();

        $this->dropDao(new UserRoleDAO($connection));
        $this->dropDao(new UserDAO($connection));
    }
}
