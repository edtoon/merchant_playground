<?php

use BlakePrinting\Db\DbMigration;

class RenameMfnShipmentsToFulfilledShipments extends DbMigration {
    public function up() {
        $this->execute("RENAME TABLE report_mfn_shipments TO report_fulfilled_shipments");
    }

    public function down() {
        $this->execute("RENAME TABLE report_fulfilled_shipments TO report_mfn_shipments");
    }
}
