<?php

use BlakePrinting\AmazonMws\Reports\ReportDAO;
use BlakePrinting\AmazonMws\Reports\ReportMarketplaceInstanceDAO;
use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceDAO;
use BlakePrinting\Db\DbMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class AmazonMwsReports extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();
        $reportDao =& ReportDAO::instance($connection);
        $this->tableDao($reportDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("report_id", "string", ["limit" => 50, "null" => true])
            ->addColumn("report_type", "string", ["limit" => 60])
            ->addColumn("mws_report_request_id", "string", ["limit" => 50, "null" => true])
            ->addColumn("available_date", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("acknowledged", "boolean", ["null" => true])
            ->addColumn("acknowledged_date", "biginteger", ["signed" => false, "null" => true])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("report_id")
            ->addIndex("report_type")
            ->addIndex("mws_report_request_id")
            ->addIndex("available_date")
            ->addIndex("acknowledged")
            ->addIndex("acknowledged_date")
            ->save()
        ;

        $marketplaceInstanceDao =& MarketplaceInstanceDAO::instance($connection);
        $reportMarketplaceInstanceDao =& ReportMarketplaceInstanceDAO::instance($connection);
        $this->tableDao($reportMarketplaceInstanceDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["report_id", "marketplace_instance_id"]])
            ->addColumn("report_id", "biginteger", ["signed" => false])
            ->addColumn("marketplace_instance_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addForeignKey("report_id", "report", "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("marketplace_instance_id", $marketplaceInstanceDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $this->table("report_list", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("max_count", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY, "null" => true])
            ->addColumn("available_from_date", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("available_to_date", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("acknowledged", "boolean", ["null" => true])
            ->addColumn("next_token", "text", ["null" => true])
            ->addColumn("response_header_metadata", "text")
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("max_count")
            ->addIndex("available_from_date")
            ->addIndex("available_to_date")
            ->addIndex("acknowledged")
            ->save()
        ;

        $this->table("report_list_marketplace_instance", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["report_list_id", "marketplace_instance_id"]])
            ->addColumn("report_list_id", "biginteger", ["signed" => false])
            ->addColumn("marketplace_instance_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addForeignKey("report_list_id", "report_list", "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("marketplace_instance_id", $marketplaceInstanceDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $this->table("report_list_report_type", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["report_list_id", "report_type"]])
            ->addColumn("report_list_id", "biginteger", ["signed" => false])
            ->addColumn("report_type", "string", ["limit" => 60])
            ->addForeignKey("report_list_id", "report_list", "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("report_type")
            ->save()
        ;

        $this->table("report_list_mws_report_request_id", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["report_list_id", "mws_report_request_id"]])
            ->addColumn("report_list_id", "biginteger", ["signed" => false])
            ->addColumn("mws_report_request_id", "string", ["limit" => 50])
            ->addForeignKey("report_list_id", "report_list", "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("mws_report_request_id")
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->execute("DROP TABLE IF EXISTS report_list_mws_report_request_id");
        $this->execute("DROP TABLE IF EXISTS report_list_report_type");
        $this->execute("DROP TABLE IF EXISTS report_list_marketplace_instance");
        $this->execute("DROP TABLE IF EXISTS report_list");
        $this->dropDao(ReportMarketplaceInstanceDAO::instance($connection));
        $this->dropDao(ReportDAO::instance($connection));
    }
}
