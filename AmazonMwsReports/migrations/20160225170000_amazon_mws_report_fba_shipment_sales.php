<?php

use Phinx\Db\Adapter\MysqlAdapter;
use Phinx\Migration\AbstractMigration;

class AmazonMwsReportFbaShipmentSales extends AbstractMigration {
    public function up() {
        $this->table("report_fba_shipment_sales", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("report_transform_batch_id", "biginteger", ["signed" => false])
            ->addColumn("shipment_date", "datetime")
            ->addColumn("sku", "string", ["limit" => 50])
            ->addColumn("fnsku", "string", ["limit" => 10])
            ->addColumn("asin", "string", ["limit" => 10])
            ->addColumn("fulfillment_center_id", "string", ["limit" => 5])
            ->addColumn("quantity", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("amazon_order_id", "string", ["limit" => 50])
            ->addColumn("currency", "string", ["limit" => 3, "null" => true])
            ->addColumn("item_price_per_unit", "string", ["limit" => 10])
            ->addColumn("shipping_price", "string", ["limit" => 10])
            ->addColumn("gift_wrap_price", "string", ["limit" => 10])
            ->addColumn("ship_city", "string", ["limit" => 191, "null" => true])
            ->addColumn("ship_state", "string", ["limit" => 191, "null" => true])
            ->addColumn("ship_postal_code", "string", ["limit" => 191, "null" => true])
            ->addColumn("points_granted", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addForeignKey("report_transform_batch_id", "report_transform_batch", "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("shipment_date")
            ->addIndex("sku")
            ->addIndex("fnsku")
            ->addIndex("asin")
            ->addIndex("fulfillment_center_id")
            ->addIndex("quantity")
            ->addIndex("amazon_order_id")
            ->addIndex("currency")
            ->addIndex("item_price_per_unit")
            ->addIndex("shipping_price")
            ->addIndex("gift_wrap_price")
            ->addIndex("ship_city")
            ->addIndex("ship_state")
            ->addIndex("ship_postal_code")
            ->addIndex("points_granted")
            ->save()
        ;
    }

    public function down() {
        $this->execute("DROP TABLE IF EXISTS report_fba_shipment_sales");
    }
}
