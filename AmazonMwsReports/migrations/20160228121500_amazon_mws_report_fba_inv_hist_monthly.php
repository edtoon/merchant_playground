<?php

use Phinx\Db\Adapter\MysqlAdapter;
use Phinx\Migration\AbstractMigration;

class AmazonMwsReportFbaInvHistMonthly extends AbstractMigration {
    public function up() {
        $this->table("report_fba_inv_hist_monthly", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("report_transform_batch_id", "biginteger", ["signed" => false])
            ->addColumn("year", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("month", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("fnsku", "string", ["limit" => 10])
            ->addColumn("sku", "string", ["limit" => 50])
            ->addColumn("product_name", "text")
            ->addColumn("average_quantity", "string", ["limit" => 10])
            ->addColumn("end_quantity", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("fulfillment_center_id", "string", ["limit" => 5])
            ->addColumn("detailed_disposition", "string", ["limit" => 50])
            ->addColumn("country", "string", ["limit" => 2])
            ->addForeignKey("report_transform_batch_id", "report_transform_batch", "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("year")
            ->addIndex("month")
            ->addIndex("fnsku")
            ->addIndex("sku")
            ->addIndex("average_quantity")
            ->addIndex("end_quantity")
            ->addIndex("fulfillment_center_id")
            ->addIndex("detailed_disposition")
            ->addIndex("country")
            ->save()
        ;
        $this->execute("ALTER TABLE report_fba_inv_hist_monthly ADD INDEX (product_name(191))");
    }

    public function down() {
        $this->execute("DROP TABLE IF EXISTS report_fba_inv_hist_monthly");
    }
}
