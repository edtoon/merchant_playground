<?php

use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceDAO;
use BlakePrinting\Db\DbMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class AddMarketplaceInstanceToBatch extends DbMigration {
    public function up() {
        $marketplaceInstanceDao =& MarketplaceInstanceDAO::instance($this->getConnection());

        $this->table("report_transform_batch", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["report_transform_batch_id"]])
            ->addColumn("marketplace_instance_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addForeignKey("marketplace_instance_id", $marketplaceInstanceDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;
    }

    public function down() {
        $this->table("report_transform_batch")
            ->dropForeignKey("marketplace_instance_id")
            ->removeColumn("marketplace_instance_id")
            ->save()
        ;
    }
}
