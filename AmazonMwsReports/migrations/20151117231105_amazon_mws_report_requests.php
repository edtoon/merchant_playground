<?php

use BlakePrinting\AmazonMws\Reports\ReportRequestDAO;
use BlakePrinting\AmazonMws\Reports\ReportRequestMarketplaceInstanceDAO;
use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceDAO;
use BlakePrinting\Db\DbMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class AmazonMwsReportRequests extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();
        $reportRequestDao =& ReportRequestDAO::instance($connection);
        $this->tableDao($reportRequestDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("mws_report_request_id", "string", ["limit" => 50, "null" => true])
            ->addColumn("report_type", "string", ["limit" => 60])
            ->addColumn("start_date", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("end_date", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("options", "text", ["null" => true])
            ->addColumn("scheduled", "boolean", ["null" => true])
            ->addColumn("submitted_date", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("report_processing_status", "string", ["limit" => 50, "null" => true])
            ->addColumn("generated_report_id", "string", ["limit" => 50, "null" => true])
            ->addColumn("started_processing_date", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("completed_date", "biginteger", ["signed" => false, "null" => true])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("mws_report_request_id")
            ->addIndex("report_type")
            ->addIndex("start_date")
            ->addIndex("end_date")
            ->addIndex("scheduled")
            ->addIndex("submitted_date")
            ->addIndex("report_processing_status")
            ->addIndex("generated_report_id")
            ->addIndex("started_processing_date")
            ->addIndex("completed_date")
            ->save()
        ;
        $this->alterDao($reportRequestDao, "ADD INDEX (options(191))");

        $reportRequestMarketplaceInstanceDao =& ReportRequestMarketplaceInstanceDAO::instance($connection);
        $marketplaceInstanceDao =& MarketplaceInstanceDAO::instance($connection);
        $marketplaceInstanceTbl = $marketplaceInstanceDao->formatSchemaAndTableName();
        $this->tableDao($reportRequestMarketplaceInstanceDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["report_request_id", "marketplace_instance_id"]])
            ->addColumn("report_request_id", "biginteger", ["signed" => false])
            ->addColumn("marketplace_instance_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addForeignKey("report_request_id", "report_request", "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("marketplace_instance_id", $marketplaceInstanceTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $this->table("report_request_retrieve", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["report_request_id"]])
            ->addColumn("report_request_id", "biginteger", ["signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addForeignKey("report_request_id", "report_request", "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->save()
        ;

        $this->table("report_request_transform", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["report_request_id"]])
            ->addColumn("report_request_id", "biginteger", ["signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addForeignKey("report_request_id", "report_request", "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->save()
        ;

        $this->table("report_request_acknowledge", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["report_request_id"]])
            ->addColumn("report_request_id", "biginteger", ["signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addForeignKey("report_request_id", "report_request", "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->save()
        ;

        $this->table("report_request_list", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("max_count", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY, "null" => true])
            ->addColumn("requested_from_date", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("requested_to_date", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("next_token", "text", ["null" => true])
            ->addColumn("response_header_metadata", "text")
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("max_count")
            ->addIndex("requested_from_date")
            ->addIndex("requested_to_date")
            ->save()
        ;

        $this->table("report_request_list_marketplace_instance", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["report_request_list_id", "marketplace_instance_id"]])
            ->addColumn("report_request_list_id", "biginteger", ["signed" => false])
            ->addColumn("marketplace_instance_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addForeignKey("report_request_list_id", "report_request_list", "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("marketplace_instance_id", $marketplaceInstanceTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $this->table("report_request_list_mws_report_request_id", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["report_request_list_id", "mws_report_request_id"]])
            ->addColumn("report_request_list_id", "biginteger", ["signed" => false])
            ->addColumn("mws_report_request_id", "string", ["limit" => 50])
            ->addForeignKey("report_request_list_id", "report_request_list", "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex(["report_request_list_id", "mws_report_request_id"], ["unique" => true])
            ->addIndex("mws_report_request_id")
            ->save()
        ;

        $this->table("report_request_list_report_type", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["report_request_list_id"]])
            ->addColumn("report_request_list_id", "biginteger", ["signed" => false])
            ->addColumn("report_type", "string", ["limit" => 60])
            ->addForeignKey("report_request_list_id", "report_request_list", "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex(["report_request_list_id", "report_type"], ["unique" => true])
            ->addIndex("report_type")
            ->save()
        ;

        $this->table("report_request_list_report_processing_status", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["report_request_list_id"]])
            ->addColumn("report_request_list_id", "biginteger", ["signed" => false])
            ->addColumn("report_processing_status", "string", ["limit" => 50])
            ->addForeignKey("report_request_list_id", "report_request_list", "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex(["report_request_list_id", "report_processing_status"], ["unique" => true])
            ->addIndex("report_processing_status")
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();
        $this->execute("DROP TABLE IF EXISTS report_request_list_report_processing_status");
        $this->execute("DROP TABLE IF EXISTS report_request_list_report_type");
        $this->execute("DROP TABLE IF EXISTS report_request_list_mws_report_request_id");
        $this->execute("DROP TABLE IF EXISTS report_request_list_marketplace_instance");
        $this->execute("DROP TABLE IF EXISTS report_request_list");
        $this->execute("DROP TABLE IF EXISTS report_request_acknowledge");
        $this->execute("DROP TABLE IF EXISTS report_request_transform");
        $this->execute("DROP TABLE IF EXISTS report_request_retrieve");
        $this->dropDao(ReportRequestMarketplaceInstanceDAO::instance($connection));
        $this->dropDao(ReportRequestDAO::instance($connection));
    }
}
