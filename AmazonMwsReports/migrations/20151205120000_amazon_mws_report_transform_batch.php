<?php

use Phinx\Migration\AbstractMigration;

class AmazonMwsReportTransformBatch extends AbstractMigration {
    public function up() {
        $this->table("report_transform_batch", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("report_id", "string", ["limit" => 50])
            ->addIndex("created")
            ->addIndex("report_id")
            ->save()
        ;
    }

    public function down() {
        $this->execute("DROP TABLE IF EXISTS report_transform_batch");
    }
}
