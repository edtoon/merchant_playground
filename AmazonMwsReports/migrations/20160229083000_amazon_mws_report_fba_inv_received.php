<?php

use Phinx\Db\Adapter\MysqlAdapter;
use Phinx\Migration\AbstractMigration;

class AmazonMwsReportFbaInvReceived extends AbstractMigration {
    public function up() {
        $this->table("report_fba_inv_received", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("report_transform_batch_id", "biginteger", ["signed" => false])
            ->addColumn("received_date", "datetime")
            ->addColumn("fnsku", "string", ["limit" => 10])
            ->addColumn("sku", "string", ["limit" => 50])
            ->addColumn("product_name", "text")
            ->addColumn("quantity", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addColumn("fba_shipment_id", "string", ["limit" => 10])
            ->addColumn("fulfillment_center_id", "string", ["limit" => 5])
            ->addForeignKey("report_transform_batch_id", "report_transform_batch", "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("received_date")
            ->addIndex("fnsku")
            ->addIndex("sku")
            ->addIndex("quantity")
            ->addIndex("fba_shipment_id")
            ->addIndex("fulfillment_center_id")
            ->save()
        ;
        $this->execute("ALTER TABLE report_fba_inv_received ADD INDEX (product_name(191))");
    }

    public function down() {
        $this->execute("DROP TABLE IF EXISTS report_fba_inv_received");
    }
}
