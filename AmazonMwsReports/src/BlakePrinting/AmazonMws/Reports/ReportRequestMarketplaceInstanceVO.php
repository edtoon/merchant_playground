<?php

namespace BlakePrinting\AmazonMws\Reports;

use BlakePrinting\Db\ValueObject;

class ReportRequestMarketplaceInstanceVO implements ValueObject {
    /** @var int */
    private $reportRequestId = null;
    /** @var int */
    private $marketplaceInstanceId = null;

    /**
     * @return int
     */
    public function getReportRequestId()
    {
        return $this->reportRequestId;
    }

    /**
     * @param int $reportRequestId
     */
    public function setReportRequestId($reportRequestId)
    {
        $this->reportRequestId = $reportRequestId;
    }

    /**
     * @return int
     */
    public function getMarketplaceInstanceId()
    {
        return $this->marketplaceInstanceId;
    }

    /**
     * @param int $marketplaceInstanceId
     */
    public function setMarketplaceInstanceId($marketplaceInstanceId)
    {
        $this->marketplaceInstanceId = $marketplaceInstanceId;
    }
}
