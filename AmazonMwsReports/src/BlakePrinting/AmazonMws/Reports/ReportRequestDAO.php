<?php

namespace BlakePrinting\AmazonMws\Reports;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ReportRequestDAO instance(PDO $connection = null, $schemaName = null)
 * @method ReportRequestVO findById($id, $options = null, $useCache = false)
 * @method ReportRequestVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ReportRequestVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ReportRequestVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ReportRequestVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ReportRequestVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ReportRequestVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ReportRequestDAO extends AbstractDataAccessObject {
}
