<?php

namespace BlakePrinting\AmazonMws\Reports;

use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceVO;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use MarketplaceWebService_Model_ReportInfo;
use MarketplaceWebService_Model_ReportRequestInfo;
use PDO;
use ReflectionClass;

class ReportsUtil {
    private function __construct() { }

    /**
     * @return int
     */
    public static function storeRequestInfo(MarketplaceInstanceVO &$marketplaceInstanceVo, MarketplaceWebService_Model_ReportRequestInfo &$requestInfo) {
        $marketplaceInstanceId = $marketplaceInstanceVo->getId();
        $logger =& LogUtil::getLogger();
        $connection =& PDOUtil::getMysqlConnection();
        $mwsReportRequestId = $requestInfo->getReportRequestId();
        $reportType = $requestInfo->getReportType();

        if(empty($mwsReportRequestId)) {
            $logger->error("No report request id set");
            return null;
        }

        if(empty($reportType)) {
            $logger->alert("Report request id: " . $mwsReportRequestId . " has no report type");
            return null;
        }

        $reportRequestProtoVo = new ReportRequestVO();

        $reportRequestProtoVo->setMwsReportRequestId($mwsReportRequestId);
        $reportRequestProtoVo->setReportType($requestInfo->getReportType());
        $reportRequestProtoVo->setStartDate(
            $requestInfo->getStartDate() === null ? null : $requestInfo->getStartDate()->getTimestamp()
        );
        $reportRequestProtoVo->setEndDate(
            $requestInfo->getEndDate() === null ? null : $requestInfo->getEndDate()->getTimestamp()
        );
        $reportRequestProtoVo->setScheduled($requestInfo->getScheduled() ?: false);
        $reportRequestProtoVo->setSubmittedDate(
            $requestInfo->getSubmittedDate() == null ? null : $requestInfo->getSubmittedDate()->getTimestamp()
        );
        $reportRequestProtoVo->setReportProcessingStatus($requestInfo->getReportProcessingStatus());
        $reportRequestProtoVo->setGeneratedReportId($requestInfo->getGeneratedReportId());
        $reportRequestProtoVo->setStartedProcessingDate(
            $requestInfo->getStartedProcessingDate() == null ? null : $requestInfo->getStartedProcessingDate()->getTimestamp()
        );
        $reportRequestProtoVo->setCompletedDate(
            $requestInfo->getCompletedDate() == null ? null : $requestInfo->getCompletedDate()->getTimestamp()
        );

        $reportRequestDao =& ReportRequestDAO::instance($connection);
        $reportRequestMarketplaceInstanceDao =& ReportRequestMarketplaceInstanceDAO::instance($connection);
        $reportRequestVo =& $reportRequestDao->findSingle(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$reportRequestMarketplaceInstanceDao, &$reportRequestProtoVo, &$marketplaceInstanceId) {
                $reportRequestMarketplaceInstanceTbl = $reportRequestMarketplaceInstanceDao->formatSchemaAndTableName();
                $pdoQuery
                    ->join(
                        $reportRequestMarketplaceInstanceTbl . " mktpl", [
                            "id = report_request_id",
                            "marketplace_instance_id = " . $marketplaceInstanceId
                        ]
                    )
                    ->where("mws_report_request_id", $reportRequestProtoVo->getMwsReportRequestId(), PDO::PARAM_STR)
                    ->where("report_type", $reportRequestProtoVo->getReportType(), PDO::PARAM_STR)
                    ->whereValueOrNull("start_date", $reportRequestProtoVo->getStartDate(), PDO::PARAM_INT)
                    ->whereValueOrNull("end_date", $reportRequestProtoVo->getEndDate(), PDO::PARAM_INT)
                    ->whereValueOrNull("scheduled", $reportRequestProtoVo->isScheduled(), PDO::PARAM_BOOL)
                    ->where("NOT EXISTS (" .
                        "SELECT 1 " .
                        "FROM " . $reportRequestMarketplaceInstanceTbl . " wrong_mktpl " .
                        "WHERE wrong_mktpl.report_request_id = id " .
                        "AND wrong_mktpl.marketplace_instance_id <> mktpl.marketplace_instance_id " .
                        "LIMIT 1" .
                    ")")
                ;
            }, null, null, "FOR UPDATE"
        );
        $reportRequestId = null;

        if($reportRequestVo === null) {
            /** @var ReportRequestVO $reportRequestVo */
            $reportRequestId =& $reportRequestDao->insert($reportRequestProtoVo);

            $reportRequestMarketplaceInstanceVo = new ReportRequestMarketplaceInstanceVO();

            $reportRequestMarketplaceInstanceVo->setReportRequestId($reportRequestId);
            $reportRequestMarketplaceInstanceVo->setMarketplaceInstanceId($marketplaceInstanceId);

            $reportRequestMarketplaceInstanceDao->insert($reportRequestMarketplaceInstanceVo);
        } else {
            $requestReportProcessingStatus = $reportRequestVo->getReportProcessingStatus();

            if($requestReportProcessingStatus == "_CANCELED_" || $requestReportProcessingStatus == "_DONE_" || $requestReportProcessingStatus == "_DONE_NO_DATA_") {
                return null;
            }

            if(Utils::propertiesDiffer($reportRequestVo, $reportRequestProtoVo, ["id", "created", "updated"])) {
                $reportRequestVo->setSubmittedDate($reportRequestProtoVo->getSubmittedDate());
                $reportRequestVo->setReportProcessingStatus($reportRequestProtoVo->getReportProcessingStatus());
                $reportRequestVo->setGeneratedReportId($reportRequestProtoVo->getGeneratedReportId());
                $reportRequestVo->setStartedProcessingDate($reportRequestProtoVo->getStartedProcessingDate());
                $reportRequestVo->setCompletedDate($reportRequestVo->getCompletedDate());

                $reportRequestDao->update($reportRequestVo);
            }
        }

        return $reportRequestId;
    }

    /**
     * @return int
     */
    public static function storeReportInfo(MarketplaceInstanceVO &$marketplaceInstanceVo, MarketplaceWebService_Model_ReportInfo &$reportInfo) {
        $logger =& LogUtil::getLogger();
        $mysql =& PDOUtil::getMysqlConnection();
        $reportId = $reportInfo->getReportId();
        $mwsReportRequestId = $reportInfo->getReportRequestId();
        $reportType = $reportInfo->getReportType();
        $availableDate = ($reportInfo->getAvailableDate() == null ? null : $reportInfo->getAvailableDate()->getTimestamp());
        $acknowledged = $reportInfo->getAcknowledged();
        $acknowledgedDate = ($reportInfo->getAcknowledgedDate() == null ? null : $reportInfo->getAcknowledgedDate()->getTimestamp());

        if(empty($reportId)) {
            $logger->error("No report id set");
            return null;
        }

        if(empty($reportType)) {
            $logger->alert("Report id: " . $reportId . " has no report type");
            return null;
        }

        $report =& PDOQuery::select()
            ->columns("id", "available_date", "acknowledged", "acknowledged_date")
            ->from("amazon_mws.report")
            ->from("amazon_mws.report_marketplace_instance mktpl")
            ->where("report.id", $reportId, PDO::PARAM_STR)
            ->where("report.id", "mktpl.report_id")
            ->where("mktpl.marketplace_instance_id", $marketplaceInstanceVo->getId(), PDO::PARAM_INT)
            ->where("NOT EXISTS (" .
                "SELECT 1 FROM amazon_mws.report_marketplace_instance wrong_mktpl " .
                "WHERE wrong_mktpl.report_id = report.id " .
                "AND wrong_mktpl.marketplace_instance_id <> mktpl.marketplace_instance_id LIMIT 1" .
            ")")
            ->where("report.report_type", $reportType, PDO::PARAM_STR)
            ->where("report.mws_report_request_id", $mwsReportRequestId, PDO::PARAM_STR)
            ->options("FOR UPDATE")
            ->getSingleRow($mysql)
        ;
        $reportId = null;

        if($report === null) {
            $reportId =& PDOQuery::insert()
                ->into("amazon_mws.report")
                ->value("created", "UNIX_TIMESTAMP()")
                ->value("report_id", $reportId, PDO::PARAM_STR)
                ->value("report_type", $reportType, PDO::PARAM_STR)
                ->value("mws_report_request_id", $mwsReportRequestId, PDO::PARAM_STR)
                ->value("available_date", $availableDate, PDO::PARAM_INT)
                ->value("acknowledged", $acknowledged, PDO::PARAM_BOOL)
                ->value("acknowledged_date", $acknowledgedDate, PDO::PARAM_INT)
                ->executeEnsuringLastInsertId($mysql)
            ;

            PDOQuery::insert()
                ->into("amazon_mws.report_marketplace_instance")
                ->value("report_id", $reportId, PDO::PARAM_INT)
                ->value("marketplace_instance_id", $marketplaceInstanceVo->getId(), PDO::PARAM_INT)
                ->executeEnsuringRowCount($mysql, 1)
            ;
        } else {
            $reportAcknowledgedDate = $report["acknowledged_date"];

            if(!empty($reportAcknowledgedDate)) {
                return null;
            }

            if(!Utils::valuesMatch($availableDate, $report["available_date"]) ||
                !Utils::valuesMatch($acknowledged, $report["acknowledged"]) ||
                !Utils::valuesMatch($acknowledgedDate, $reportAcknowledgedDate)) {
                $reportId = $report["id"];

                PDOQuery::update()
                    ->table("amazon_mws.report")
                    ->set("updated", "UNIX_TIMESTAMP()")
                    ->set("available_date", $availableDate, PDO::PARAM_INT)
                    ->set("acknowledged", $acknowledged, PDO::PARAM_STR)
                    ->set("acknowledged_date", $acknowledgedDate, PDO::PARAM_INT)
                    ->where("id", $reportId, PDO::PARAM_INT)
                    ->executeEnsuringRowCount($mysql, 1)
                ;
            }
        }

        return $reportId;
    }
}
