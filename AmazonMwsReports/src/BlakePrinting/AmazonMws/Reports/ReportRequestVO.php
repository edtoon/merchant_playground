<?php

namespace BlakePrinting\AmazonMws\Reports;

use BlakePrinting\Db\ValueObject;

class ReportRequestVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var string */
    private $mwsReportRequestId = null;
    /** @var string */
    private $reportType = null;
    /** @var int */
    private $startDate = null;
    /** @var int */
    private $endDate = null;
    /** @var string */
    private $options = null;
    /** @var bool */
    private $scheduled = null;
    /** @var int */
    private $submittedDate = null;
    /** @var string */
    private $reportProcessingStatus = null;
    /** @var string */
    private $generatedReportId = null;
    /** @var int */
    private $startedProcessingDate = null;
    /** @var int */
    private $completedDate = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getMwsReportRequestId()
    {
        return $this->mwsReportRequestId;
    }

    /**
     * @param string $mwsReportRequestId
     */
    public function setMwsReportRequestId($mwsReportRequestId)
    {
        $this->mwsReportRequestId = $mwsReportRequestId;
    }

    /**
     * @return string
     */
    public function getReportType()
    {
        return $this->reportType;
    }

    /**
     * @param string $reportType
     */
    public function setReportType($reportType)
    {
        $this->reportType = $reportType;
    }

    /**
     * @return int
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param int $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return int
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param int $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @return string
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param string $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * @return boolean
     */
    public function isScheduled()
    {
        return $this->scheduled;
    }

    /**
     * @param boolean $scheduled
     */
    public function setScheduled($scheduled)
    {
        $this->scheduled = $scheduled;
    }

    /**
     * @return int
     */
    public function getSubmittedDate()
    {
        return $this->submittedDate;
    }

    /**
     * @param int $submittedDate
     */
    public function setSubmittedDate($submittedDate)
    {
        $this->submittedDate = $submittedDate;
    }

    /**
     * @return string
     */
    public function getReportProcessingStatus()
    {
        return $this->reportProcessingStatus;
    }

    /**
     * @param string $reportProcessingStatus
     */
    public function setReportProcessingStatus($reportProcessingStatus)
    {
        $this->reportProcessingStatus = $reportProcessingStatus;
    }

    /**
     * @return string
     */
    public function getGeneratedReportId()
    {
        return $this->generatedReportId;
    }

    /**
     * @param string $generatedReportId
     */
    public function setGeneratedReportId($generatedReportId)
    {
        $this->generatedReportId = $generatedReportId;
    }

    /**
     * @return int
     */
    public function getStartedProcessingDate()
    {
        return $this->startedProcessingDate;
    }

    /**
     * @param int $startedProcessingDate
     */
    public function setStartedProcessingDate($startedProcessingDate)
    {
        $this->startedProcessingDate = $startedProcessingDate;
    }

    /**
     * @return int
     */
    public function getCompletedDate()
    {
        return $this->completedDate;
    }

    /**
     * @param int $completedDate
     */
    public function setCompletedDate($completedDate)
    {
        $this->completedDate = $completedDate;
    }
}
