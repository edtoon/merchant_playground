<?php

namespace BlakePrinting\AmazonMws\Reports;

class ReportTypes {
    private function __construct() { }

    const REPORT_TYPE_INVENTORY = '_GET_FLAT_FILE_OPEN_LISTINGS_DATA_';
    const REPORT_TYPE_ACTIVE_LISTINGS = '_GET_MERCHANT_LISTINGS_DATA_';
    const REPORT_TYPE_OPEN_LISTINGS = '_GET_MERCHANT_LISTINGS_DATA_BACK_COMPAT_';
    const REPORT_TYPE_OPEN_LISTINGS_LITE = '_GET_MERCHANT_LISTINGS_DATA_LITE_';
    const REPORT_TYPE_OPEN_LISTINGS_LITER = '_GET_MERCHANT_LISTINGS_DATA_LITER_';
    const REPORT_TYPE_CANCELED_LISTINGS = '_GET_MERCHANT_CANCELLED_LISTINGS_DATA_';
    const REPORT_TYPE_SOLD_LISTINGS = '_GET_CONVERGED_FLAT_FILE_SOLD_LISTINGS_DATA_';
    const REPORT_TYPE_DEFECTS = '_GET_MERCHANT_LISTINGS_DEFECT_DATA_';

    const REPORT_TYPE_ORDERS_UNSHIPPED = '_GET_FLAT_FILE_ACTIONABLE_ORDER_DATA_';
    const REPORT_TYPE_ORDERS_SCHEDULED_XML = '_GET_ORDERS_DATA_';
    const REPORT_TYPE_ORDERS_SCHEDULED = '_GET_FLAT_FILE_ORDERS_DATA_';
    const REPORT_TYPE_ORDERS_CONV = '_GET_CONVERGED_FLAT_FILE_ORDER_REPORT_DATA_';

    const REPORT_TYPE_ORDERS_BY_LAST_UPDATE = '_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_';
    const REPORT_TYPE_ORDERS_BY_ORDER_DATE = '_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_ORDER_DATE_';
    const REPORT_TYPE_ORDERS_BY_LAST_UPDATE_XML = '_GET_XML_ALL_ORDERS_DATA_BY_LAST_UPDATE_';
    const REPORT_TYPE_ORDERS_BY_ORDER_DATE_XML = '_GET_XML_ALL_ORDERS_DATA_BY_ORDER_DATE_';

    const REPORT_TYPE_PENDING_ORDERS = '_GET_FLAT_FILE_PENDING_ORDERS_DATA_';
    const REPORT_TYPE_PENDING_ORDERS_XML = '_GET_PENDING_ORDERS_DATA_';
    const REPORT_TYPE_PENDING_ORDERS_CONV = '_GET_CONVERGED_FLAT_FILE_PENDING_ORDERS_DATA_';

    const REPORT_TYPE_FEEDBACK = '_GET_SELLER_FEEDBACK_DATA_';

    const REPORT_TYPE_SETTLEMENT = '_GET_V2_SETTLEMENT_REPORT_DATA_FLAT_FILE_';
    const REPORT_TYPE_SETTLEMENT_XML = '_GET_V2_SETTLEMENT_REPORT_DATA_XML_';
    const REPORT_TYPE_SETTLEMENT_V2 = '_GET_V2_SETTLEMENT_REPORT_DATA_FLAT_FILE_V2_';

    const REPORT_TYPE_FBA_SHIPMENT_SALES = '_GET_FBA_FULFILLMENT_CUSTOMER_SHIPMENT_SALES_DATA_';
    const REPORT_TYPE_FBA_PROMOTIONS = '_GET_FBA_FULFILLMENT_CUSTOMER_SHIPMENT_PROMOTION_DATA_';
    const REPORT_TYPE_FBA_US_TAXES = '_GET_FBA_FULFILLMENT_CUSTOMER_TAXES_DATA_';
    const REPORT_TYPE_FBA_INV_FULFILLED = '_GET_AFN_INVENTORY_DATA_';
    const REPORT_TYPE_FBA_INV_MULTI_COUNTRY = '_GET_AFN_INVENTORY_DATA_BY_COUNTRY_';
    const REPORT_TYPE_FBA_INV_HIST_DAILY = '_GET_FBA_FULFILLMENT_CURRENT_INVENTORY_DATA_';
    const REPORT_TYPE_FBA_INV_HIST_MONTHLY = '_GET_FBA_FULFILLMENT_MONTHLY_INVENTORY_DATA_';
    const REPORT_TYPE_FBA_INV_RECEIVED = '_GET_FBA_FULFILLMENT_INVENTORY_RECEIPTS_DATA_';
    const REPORT_TYPE_FBA_INV_RESERVED = '_GET_RESERVED_INVENTORY_DATA_';
    const REPORT_TYPE_FBA_INV_EVENT_DETAIL = '_GET_FBA_FULFILLMENT_INVENTORY_SUMMARY_DATA_';
    const REPORT_TYPE_FBA_INV_ADJUSTMENTS = '_GET_FBA_FULFILLMENT_INVENTORY_ADJUSTMENTS_DATA_';
    const REPORT_TYPE_FBA_INV_HEALTH = '_GET_FBA_FULFILLMENT_INVENTORY_HEALTH_DATA_';
    const REPORT_TYPE_FBA_INV_MANAGE = '_GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA_';
    const REPORT_TYPE_FBA_INV_MANAGE_ARCHIVE = '_GET_FBA_MYI_ALL_INVENTORY_DATA_';
    const REPORT_TYPE_FBA_INV_MOVEMENT = '_GET_FBA_FULFILLMENT_CROSS_BORDER_INVENTORY_MOVEMENT_DATA_';
    const REPORT_TYPE_FBA_INBOUND_PERF = '_GET_FBA_FULFILLMENT_INBOUND_NONCOMPLIANCE_DATA_';
    const REPORT_TYPE_FBA_INV_HAZMAT = '_GET_FBA_HAZMAT_STATUS_CHANGE_DATA_';
    const REPORT_TYPE_FBA_FEE_PREVIEW = '_GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA_';
    const REPORT_TYPE_FBA_REIMBURSEMENTS = '_GET_FBA_REIMBURSEMENTS_DATA_';
    const REPORT_TYPE_FBA_RETURNS = '_GET_FBA_FULFILLMENT_CUSTOMER_RETURNS_DATA_';
    const REPORT_TYPE_FBA_REPLACEMENTS = '_GET_FBA_FULFILLMENT_CUSTOMER_SHIPMENT_REPLACEMENT_DATA_';
    const REPORT_TYPE_FBA_REMOVAL_RECOMMEND = '_GET_FBA_RECOMMENDED_REMOVAL_DATA_';
    const REPORT_TYPE_FBA_REMOVAL_DETAIL = '_GET_FBA_FULFILLMENT_REMOVAL_ORDER_DETAIL_DATA_';
    const REPORT_TYPE_FBA_REMOVAL_SHIPPING = '_GET_FBA_FULFILLMENT_REMOVAL_SHIPMENT_DETAIL_DATA_';

    const REPORT_TYPE_FULFILLED_SHIPMENTS = '_GET_AMAZON_FULFILLED_SHIPMENTS_DATA_';

    const REPORT_TYPE_US_SALES_TAX = '_GET_FLAT_FILE_SALES_TAX_DATA_';
}
