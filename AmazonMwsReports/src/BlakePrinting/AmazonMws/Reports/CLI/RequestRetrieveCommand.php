<?php

namespace BlakePrinting\AmazonMws\Reports\CLI;

use BlakePrinting\AmazonMws\Reports\AmazonReportResultReader;
use BlakePrinting\AmazonMws\Reports\ReportsClientFactory;
use BlakePrinting\AmazonMws\Sellers\AmazonMwsUtil;
use BlakePrinting\AmazonMws\Sellers\CredentialVO;
use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceVO;
use BlakePrinting\CLI\CLICommand;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Util\LogUtil;
use Monolog\Logger;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use MarketplaceWebService_Model_GetReportRequest;
use MarketplaceWebService_Exception;
use Exception;
use PDO;

class RequestRetrieveCommand extends CLICommand {
    protected function configure() {
        $this
            ->setName("amazon:mws:reports:retrieve-request")
            ->setDescription("Retrieve a requested report")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        AmazonMwsUtil::forEachActiveMarketplace(function(MarketplaceInstanceVO &$marketplaceInstanceVo, CredentialVO $credentialVo, PDO $connection) {
            $this->retrieveRequestsForInstanceAndConfig($marketplaceInstanceVo, $credentialVo, $connection);

            PDOUtil::commitConnection($connection);
        }, function(PDO $connection) {
            PDOUtil::rollbackConnection($connection);
        });
    }

    private function retrieveRequestsForInstanceAndConfig(MarketplaceInstanceVO &$marketplaceInstanceVo, CredentialVO &$credentialVo, PDO $connection) {
        $logger =& LogUtil::getLogger();
        $mwsMarketplaceId = $marketplaceInstanceVo->getMwsMarketplaceId();
        $sellerId = $credentialVo->getSellerId();
        $reportRequest =& self::getRequestForRetrieval($marketplaceInstanceVo, $connection);

        try {
            if($reportRequest === null) {
                $logger->debug("No requests ready to retrieve");

                return false;
            }

            $reportRequestId = $reportRequest["report_request_id"];
            $reportProcessingStatus = $reportRequest["report_processing_status"];
            $reportType = $reportRequest["report_type"];
            $reportId = $reportRequest["report_id"];

            if($reportProcessingStatus == "_CANCELED_" || $reportProcessingStatus == "_DONE_NO_DATA_") {
                self::updateRequest($reportRequestId, (empty($reportId) ? null : "amazon_mws.report_request_acknowledge"), $connection);

                $logger->debug("Received processing status: " . $reportProcessingStatus . " for Amazon report request #" . $reportRequestId . ", id: " . $reportId . ", type: " . $reportType);

                return true;
            }

            $reportDir = "/data/marketplace/" . $marketplaceInstanceVo->getId() . "/" . $reportType;

            if(!is_dir($reportDir)) {
                if(file_exists($reportDir)) {
                    throw new Exception("A file exists where our report directory should be: " . $reportDir);
                } else {
                    mkdir($reportDir, 0644, true);
                }
            } else if(!is_writable($reportDir)) {
                throw new Exception("Can't write to report directory: " . $reportDir);
            }

            $reportFilePath = $reportDir . "/" . $reportId;
            $reportFile = fopen($reportFilePath, "wb");

            try {
                if($reportFile === null) {
                    $logger->critical("Failed to open report file: " . $reportFilePath);

                    return false;
                }

                $reportsClientFactory = new ReportsClientFactory();
                $reportsClient =& $reportsClientFactory->createClient(Main::APP_NAME, Main::APP_VERSION, $credentialVo, $connection);

                $request = new MarketplaceWebService_Model_GetReportRequest();
                $request->setMarketplace($mwsMarketplaceId);
                $request->setMerchant($sellerId);
                $request->setReport($reportFile);
                $request->setReportId($reportId);

                try {
                    $response = $reportsClient->getReport($request);

                    AmazonMwsUtil::logResponse($response, false);

                    $result = $response->getGetReportResult();

                    if(empty($result)) {
                        $resultReader = new AmazonReportResultReader($response);
                        $result = $resultReader->getResult();
                    }

                    if(empty($result)) {
                        $logger->critical("No result received - merchant id: " . $sellerId . ", marketplace id: " . $mwsMarketplaceId);

                        return false;
                    }

                    $reportMd5 = $result->getContentMd5();

                    if(empty($reportMd5)) {
                        $logger->critical("No report md5 received - merchant id: " . $sellerId . ", marketplace id: " . $mwsMarketplaceId);

                        return false;
                    }

                    fflush($reportFile);

                    $reportFileMd5 = base64_encode(md5_file($reportFilePath, true));

                    if($reportMd5 != $reportFileMd5) {
                        $logger->critical("Md5: " . $reportFileMd5 . " for file: " . $reportFilePath . " does not match the md5 from the response: " . $reportMd5);

                        return false;
                    }

                    self::updateRequest($reportRequestId, "amazon_mws.report_request_transform", $connection);

                    $logger->info("Ready to transform Amazon report: " . $reportFilePath);

                    return true;
                } catch(MarketplaceWebService_Exception $e) {
                    LogUtil::logException(Logger::ERROR, $e,
                        "MWS service call failed with status code: " . $e->getStatusCode() .
                        ", error code: " . $e->getErrorCode() .
                        ", error type: " . $e->getErrorType() .
                        ", request ID: " . $e->getRequestId() .
                        ", responseHeaderMetadata: " . $e->getResponseHeaderMetadata() .
                        ", xml: " . $e->getXML()
                    );
                } catch(Exception $e) {
                    LogUtil::logException(Logger::EMERGENCY, $e, "Failed to retrieve Amazon report id: " . $reportId);
                }
            } finally {
                if($reportFile !== null) {
                    fclose($reportFile);
                }
            }
        } finally {
            PDOUtil::rollbackConnection($connection);
        }

        return false;
    }

    private static function updateRequest($reportRequestId, $nextStepTable = null, PDO $connection) {
        PDOQuery::update()
            ->table("amazon_mws.report_request_retrieve")
            ->set("updated", "UNIX_TIMESTAMP()")
            ->where("report_request_id", $reportRequestId, PDO::PARAM_INT)
            ->executeEnsuringRowCount($connection, 1)
        ;

        if($nextStepTable !== null) {
            PDOQuery::insert()
                ->ignore()
                ->into($nextStepTable)
                ->value("report_request_id", $reportRequestId, PDO::PARAM_INT)
                ->value("created", "UNIX_TIMESTAMP()")
                ->execute($connection)
            ;
        }

        PDOUtil::commitConnection($connection);
    }

    private static function &getRequestForRetrieval(MarketplaceInstanceVO &$marketplaceInstanceVo, PDO $connection) {
        return PDOQuery::select()
            ->columns(
                "retrieve.report_request_id",
                "request.report_type",
                "request.report_processing_status",
                "COALESCE(request.generated_report_id, report.report_id) as report_id"
            )
            ->from([
                "amazon_mws.report_request" => "request",
                "amazon_mws.report_request_retrieve" => "retrieve",
                "amazon_mws.report_request_marketplace_instance" => "request_mktpl"
            ])
            ->leftJoin("amazon_mws.report report", [
                "request.mws_report_request_id = report.mws_report_request_id",
                "report.acknowledged_date IS NULL",
                "(report.acknowledged IS NULL OR report.acknowledged = 0)"
            ])
            ->leftJoin("amazon_mws.report_marketplace_instance report_mktpl", [
                "report.id = report_mktpl.report_id",
                "report_mktpl.marketplace_instance_id = request_mktpl.marketplace_instance_id"
            ])
            ->whereNotNull("request.updated")
            ->where("request.report_processing_status", "('_CANCELED_', '_DONE_', '_DONE_NO_DATA_')", null, "IN")
            ->where("request.id", "request_mktpl.report_request_id")
            ->where("request_mktpl.marketplace_instance_id", $marketplaceInstanceVo->getId(), PDO::PARAM_INT)
            ->where("request.id", "retrieve.report_request_id")
            ->where("(" .
                "retrieve.updated IS NULL " .
                "OR " .
                "retrieve.updated <= request.updated " .
                "OR " .
                "(report.id IS NOT NULL AND retrieve.updated <= COALESCE(report.updated, report.created))" .
            ")")
            ->where("(" .
                "request.report_processing_status IN ('_CANCELED_', '_DONE_NO_DATA_')" .
                " OR " .
                "request.generated_report_id IS NOT NULL" .
                " OR " .
                "(report.report_id IS NOT NULL AND report_mktpl.report_id IS NOT NULL)" .
            ")")
            ->limit(1)
            ->options("FOR UPDATE")
            ->getSingleRow($connection)
        ;
    }
}
