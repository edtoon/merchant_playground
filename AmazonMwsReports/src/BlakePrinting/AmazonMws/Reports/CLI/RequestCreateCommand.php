<?php

namespace BlakePrinting\AmazonMws\Reports\CLI;

use BlakePrinting\AmazonMws\Reports\AmazonReportResultReader;
use BlakePrinting\AmazonMws\Reports\ReportsClientFactory;
use BlakePrinting\AmazonMws\Reports\ReportsUtil;
use BlakePrinting\AmazonMws\Reports\ReportTypes;
use BlakePrinting\AmazonMws\Sellers\AmazonMwsUtil;
use BlakePrinting\AmazonMws\Sellers\CredentialDAO;
use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceDAO;
use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceVO;
use BlakePrinting\CLI\CLICommand;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use Monolog\Logger;
use Stringy\StaticStringy as S;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use MarketplaceWebService_Model_RequestReportRequest;
use MarketplaceWebService_Exception;
use Exception;
use PDO;

class RequestCreateCommand extends CLICommand {
    protected function configure() {
        $this
            ->setName("amazon:mws:reports:create-request")
            ->setDescription("Request a new report")
            ->addArgument("domain", InputArgument::REQUIRED, "Amazon domain to report on")
            ->addArgument("reportType", InputArgument::REQUIRED, "Type of report")
            ->addOption("start_date", "s", InputOption::VALUE_OPTIONAL, "Report start date in UTC (2015-04-23 or 2015-04-23 23:59:59)", "2016-01-01")
            ->addOption("end_date", "e", InputOption::VALUE_OPTIONAL, "Report end date in UTC (2015-04-23 or 2015-04-23 23:59:59)")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $domain = $input->getArgument("domain");

        if(empty($domain)) {
            $output->writeln("No domain specified to request");
            return false;
        }

        $reportType = $input->getArgument("reportType");

        if(empty($reportType)) {
            $output->writeln("No report type specified to request");
            return false;
        }

        $reportTypeValue = constant(ReportTypes::class . "::REPORT_TYPE_" . $reportType);

        if($reportTypeValue === null) {
            $output->writeln("Unrecognized report type: " . $reportType);
            return false;
        }

        $connection =& PDOUtil::getMysqlConnection();

        try {
            $marketplaceInstanceDao =& MarketplaceInstanceDAO::instance($connection);
            $marketplaceInstanceVo =& $marketplaceInstanceDao->findByColumn("domain_name", $domain, PDO::PARAM_STR);

            if($marketplaceInstanceVo === null) {
                $marketplaceDomains = array_map(
                    function(MarketplaceInstanceVO $marketplaceInstanceVo) {
                        return $marketplaceInstanceVo->getDomainName();
                    },
                    $marketplaceInstanceDao->findAll()
                );

                $output->writeln("No marketplace instances found for domain: " . $domain . ", existing domains are:\n    " . implode("\n    ", $marketplaceDomains));
                return false;
            }

            $logger =& LogUtil::getLogger();
            $mwsMarketplaceId = $marketplaceInstanceVo->getMwsMarketplaceId();
            $credentialDao =& CredentialDAO::instance($connection);
            $credentialVo =& $credentialDao->findById($marketplaceInstanceVo->getCredentialId());
            $sellerId = $credentialVo->getSellerId();
            $reportsClientFactory = new ReportsClientFactory();
            $reportsClient =& $reportsClientFactory->createClient(Main::APP_NAME, Main::APP_VERSION, $credentialVo, $connection);
            $startDateTime = self::convertDateOptionToDateTime($input->getOption("start_date"), "start");
            $endDateTime = self::convertDateOptionToDateTime($input->getOption("end_date"), "end");

            $logger->debug("Requesting report type: " . $reportType . " starting at: " . $startDateTime->format(DATE_ISO8601) . ", ending at: " . $endDateTime->format(DATE_ISO8601));

            $request = new MarketplaceWebService_Model_RequestReportRequest();
            $request->setMerchant($sellerId);
            $request->setReportType($reportTypeValue);
            if($startDateTime !== null) {
                $request->setStartDate($startDateTime);
            }
            if($endDateTime !== null) {
                $request->setEndDate($endDateTime);
            }
            $request->setMarketplaceIdList(["Id" => [$mwsMarketplaceId]]);
            $response = $reportsClient->requestReport($request);

            AmazonMwsUtil::logResponse($response);

            $result = $response->getRequestReportResult();

            if(empty($result)) {
                $resultReader = new AmazonReportResultReader($response);
                $result = $resultReader->getResult();
            }

            if(empty($result)) {
                $logger->critical("No result received - merchant id: " . $sellerId . ", MWS marketplace id: " . $mwsMarketplaceId);
            } else {
                $requestInfo = $result->getReportRequestInfo();

                if(empty($requestInfo)) {
                    $logger->critical("No report request info received - merchant id: " . $sellerId . ", MWS marketplace id: " . $mwsMarketplaceId);
                } else {
                    $reportRequestId = ReportsUtil::storeRequestInfo($marketplaceInstanceVo, $requestInfo);

                    if($reportRequestId !== null) {
                        PDOQuery::insert()
                            ->into("amazon_mws.report_request_retrieve")
                            ->value("created", "UNIX_TIMESTAMP()")
                            ->value("report_request_id", $reportRequestId, PDO::PARAM_INT)
                            ->executeEnsuringRowCount($connection, 1)
                        ;

                        PDOUtil::commitConnection($connection);

                        $logger->info("Created Amazon report request for MWS marketplace id: " . $mwsMarketplaceId);
                    } else {
                        $logger->error("Failed to create Amazon report request for MWS marketplace id: " . $mwsMarketplaceId);
                    }
                }

                return true;
            }
        } catch(MarketplaceWebService_Exception $e) {
            LogUtil::logException(Logger::ERROR, $e,
                "MWS service call failed with status code: " . $e->getStatusCode() .
                ", error code: " . $e->getErrorCode() .
                ", error type: " . $e->getErrorType() .
                ", request ID: " . $e->getRequestId() .
                ", responseHeaderMetadata: " . $e->getResponseHeaderMetadata() .
                ", xml: " . $e->getXML()
            );
        } catch(Exception $e) {
            LogUtil::logException(Logger::CRITICAL, $e, "Failed to store Amazon report request details");
        }

        PDOUtil::rollbackConnection($connection);

        return false;
    }

    private static function convertDateOptionToDateTime($date, $type) {
        if($date !== null) {
            $date = (string)S::trim($date);

            if(!S::isBlank($date)) {
                if(S::length($date) == 10) {
                    $date = $date . " " . ($type == "end" ? "23:59:59" : "00:00:00");
                }

                return Utils::convertUtcDateToDateTime($date);
            }
        }

        return null;
    }
}
