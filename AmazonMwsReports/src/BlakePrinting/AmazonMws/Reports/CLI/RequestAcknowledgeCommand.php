<?php

namespace BlakePrinting\AmazonMws\Reports\CLI;

use BlakePrinting\AmazonMws\Reports\AmazonReportResultReader;
use BlakePrinting\AmazonMws\Reports\ReportsClientFactory;
use BlakePrinting\AmazonMws\Reports\ReportsUtil;
use BlakePrinting\AmazonMws\Sellers\AmazonMwsUtil;
use BlakePrinting\AmazonMws\Sellers\CredentialVO;
use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceVO;
use BlakePrinting\CLI\CLICommand;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Util\LogUtil;
use Monolog\Logger;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use MarketplaceWebService_Model_IdList;
use MarketplaceWebService_Model_UpdateReportAcknowledgementsRequest;
use MarketplaceWebService_Exception;
use Exception;
use PDO;

class RequestAcknowledgeCommand extends CLICommand {
    protected function configure() {
        $this
            ->setName("amazon:mws:reports:acknowledge-request")
            ->setDescription("Acknowledge a retrieved report")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        AmazonMwsUtil::forEachActiveMarketplace(function(MarketplaceInstanceVO &$marketplaceInstanceVo, CredentialVO $credentialVo, PDO $connection) {
            $this->acknowledgeRequestsForInstanceAndConfig($marketplaceInstanceVo, $credentialVo, $connection);

            PDOUtil::commitConnection($connection);
        }, function(PDO $connection) {
            PDOUtil::rollbackConnection($connection);
        });
    }

    private function acknowledgeRequestsForInstanceAndConfig(MarketplaceInstanceVO &$marketplaceInstanceVo, CredentialVO &$credentialVo, PDO $connection) {
        $reportsClientFactory = new ReportsClientFactory();
        $reportsClient =& $reportsClientFactory->createClient(Main::APP_NAME, Main::APP_VERSION, $credentialVo, $connection);
        $logger =& LogUtil::getLogger();
        $mwsMarketplaceId = $marketplaceInstanceVo->getMwsMarketplaceId();
        $sellerId = $credentialVo->getSellerId();
        $reportRequests =& self::getRequestsForAcknowledgement($marketplaceInstanceVo, $connection);

        try {
            if(empty($reportRequests)) {
                $logger->debug("No reports ready to acknowledge");
            } else {
                $reportIdList = new MarketplaceWebService_Model_IdList();
                $requestIds = [];
                $requestCount = count($reportRequests);

                foreach($reportRequests as &$reportRequest) {
                    $reportIdList->withId($reportRequest["report_id"]);

                    $requestIds[] = $reportRequest["report_request_id"];
                }

                unset($reportRequest);

                $logger->debug("Acknowledging " . $requestCount . " report(s) - report ids: " . implode(", ", $reportIdList->getId()));

                $request = new MarketplaceWebService_Model_UpdateReportAcknowledgementsRequest();
                $request->setMarketplace($mwsMarketplaceId);
                $request->setMerchant($sellerId);
                $request->setReportIdList($reportIdList);

                try {
                    $response = $reportsClient->updateReportAcknowledgements($request);

                    AmazonMwsUtil::logResponse($response);

                    $result = $response->getUpdateReportAcknowledgementsResult();

                    if(empty($result)) {
                        $resultReader = new AmazonReportResultReader($response);
                        $result = $resultReader->getResult();
                    }

                    if(empty($result)) {
                        $logger->critical("No result received - merchant id: " . $sellerId . ", MWS marketplace id: " . $mwsMarketplaceId);
                    } else {
                        $resultCount = $result->getCount();
                        $reportInfoList = $result->getReportInfoList();

                        if($requestCount != $resultCount) {
                            $logger->error("Requested acknowledgement of " . $requestCount . " requests but only received results for " . $resultCount);
                        }

                        foreach($reportInfoList as &$reportInfo) {
                            ReportsUtil::storeReportInfo($marketplaceInstanceVo, $reportInfo);
                        }

                        unset($reportInfo);

                        PDOQuery::update()
                            ->table("amazon_mws.report_request_acknowledge")
                            ->set("updated", "UNIX_TIMESTAMP()")
                            ->where("report_request_id", "(" . implode(",", $requestIds) . ")", null, "IN")
                            ->executeEnsuringRowCount($connection, $requestCount)
                        ;

                        PDOUtil::commitConnection($connection);

                        $logger->info("Acknowledged " . $requestCount . " Amazon report(s)");

                        return true;
                    }
                } catch(MarketplaceWebService_Exception $e) {
                    LogUtil::logException(Logger::ERROR, $e,
                        "MWS service call failed with status code: " . $e->getStatusCode() .
                        ", error code: " . $e->getErrorCode() .
                        ", error type: " . $e->getErrorType() .
                        ", request ID: " . $e->getRequestId() .
                        ", responseHeaderMetadata: " . $e->getResponseHeaderMetadata() .
                        ", xml: " . $e->getXML()
                    );
                } catch(Exception $e) {
                    LogUtil::logException(Logger::EMERGENCY, $e, "Failed to acknowledge " . $requestCount . " Amazon report(s)");
                }
            }
        } finally {
            PDOUtil::rollbackConnection($connection);
        }

        return false;
    }

    private static function &getRequestsForAcknowledgement(MarketplaceInstanceVO &$marketplaceInstanceVo, PDO $connection) {
        return PDOQuery::select()
            ->columns(
                "acknowledge.report_request_id",
                "COALESCE(request.generated_report_id, report.report_id) as report_id"
            )
            ->from("(" .
                "amazon_mws.report_request_acknowledge acknowledge, " .
                "amazon_mws.report_request request, " .
                "amazon_mws.report_request_marketplace_instance request_mktpl" .
            ")")
            ->leftJoin("amazon_mws.report report", [
                "request.mws_report_request_id = report.mws_report_request_id",
                "EXISTS (" .
                "SELECT 1 FROM amazon_mws.report_marketplace_instance report_mktpl " .
                "WHERE report_mktpl.report_id = report.id " .
                "AND report_mktpl.marketplace_instance_id = request_mktpl.marketplace_instance_id" .
                ")"
            ])
            ->leftJoin("amazon_mws.report_marketplace_instance report_mktpl", [
                "report.id = report_mktpl.report_id",
                "report_mktpl.marketplace_instance_id = request_mktpl.marketplace_instance_id"
            ])
            ->whereNull("acknowledge.updated")
            ->whereNotNull("acknowledge.created")
            ->where("acknowledge.report_request_id", "request.id")
            ->where("(" .
                "request.generated_report_id IS NOT NULL OR (report.report_id IS NOT NULL AND report_mktpl.report_id IS NOT NULL)" .
            ")")
            ->where("request.id", "request_mktpl.report_request_id")
            ->where("request_mktpl.marketplace_instance_id", $marketplaceInstanceVo->getId(), PDO::PARAM_INT)
            ->options("FOR UPDATE")
            ->getResults($connection)
        ;
    }
}
