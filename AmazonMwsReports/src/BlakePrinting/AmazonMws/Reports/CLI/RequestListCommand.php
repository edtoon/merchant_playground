<?php

namespace BlakePrinting\AmazonMws\Reports\CLI;

use BlakePrinting\AmazonMws\Reports\AmazonReportResultReader;
use BlakePrinting\AmazonMws\Reports\ReportsClientFactory;
use BlakePrinting\AmazonMws\Reports\ReportsUtil;
use BlakePrinting\AmazonMws\Sellers\AmazonMwsUtil;
use BlakePrinting\AmazonMws\Sellers\CredentialVO;
use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceVO;
use BlakePrinting\CLI\CLICommand;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Util\LogUtil;
use Monolog\Logger;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use MarketplaceWebService_Model_GetReportRequestListByNextTokenRequest;
use MarketplaceWebService_Model_GetReportRequestListRequest;
use MarketplaceWebService_Exception;
use DateTime;
use Exception;
use PDO;

class RequestListCommand extends CLICommand {
    const MAX_COUNT = 100;

    protected function configure() {
        $this
            ->setName("amazon:mws:reports:list-requests")
            ->setDescription("List report requests")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        AmazonMwsUtil::forEachActiveMarketplace(function(MarketplaceInstanceVO &$marketplaceInstanceVo, CredentialVO $credentialVo, PDO $connection) {
            $this->listRequestsForInstanceAndConfig($marketplaceInstanceVo, $credentialVo, $connection);

            PDOUtil::commitConnection($connection);
        }, function(PDO $connection) {
            PDOUtil::rollbackConnection($connection);
        });

        return true;
    }

    private function listRequestsForInstanceAndConfig(MarketplaceInstanceVO &$marketplaceInstanceVo, CredentialVO &$credentialVo, PDO $connection) {
        $mwsMarketplaceId = $marketplaceInstanceVo->getMwsMarketplaceId();
        $sellerId = $credentialVo->getSellerId();
        $reportsClientFactory = new ReportsClientFactory();
        $reportsClient =& $reportsClientFactory->createClient(Main::APP_NAME, Main::APP_VERSION, $credentialVo, $connection);
        $logger =& LogUtil::getLogger();
        $connection =& PDOUtil::getMysqlConnection();
        $listStatus =& $this->getListStatus($marketplaceInstanceVo, $connection);
        $listStatusIsEmpty = empty($listStatus);
        $nextToken = ($listStatusIsEmpty ? null : $listStatus["next_token"]);
        $storedRequestInfoCount = 0;
        $requestedFromTimestamp = null;
        $response = null;
        $result = null;

        try {
            if(empty($nextToken)) {
                $recentCompletedDate = $this->getRecentCompletedDate($marketplaceInstanceVo, $connection);
                $request = new MarketplaceWebService_Model_GetReportRequestListRequest();
                $request->setMaxCount(self::MAX_COUNT);
                $request->setMarketplace($mwsMarketplaceId);
                $request->setMerchant($sellerId);
                if($recentCompletedDate !== null) {
                    $requestedFromTimestamp = $recentCompletedDate;

                    $request->setRequestedFromDate((new DateTime("@" . $recentCompletedDate))->format(DateTime::ATOM));
                }
                $response = $reportsClient->getReportRequestList($request);
                $result = $response->getGetReportRequestListResult();
            } else {
                $request = new MarketplaceWebService_Model_GetReportRequestListByNextTokenRequest();
                $request->setMarketplace($mwsMarketplaceId);
                $request->setMerchant($sellerId);
                $request->setNextToken($nextToken);
                $response = $reportsClient->getReportRequestListByNextToken($request);
                $result = $response->getGetReportRequestListByNextTokenResult();
            }

            AmazonMwsUtil::logResponse($response);

            if(empty($result)) {
                $resultReader = new AmazonReportResultReader($response);
                $result = $resultReader->getResult();
            }

            if(empty($result)) {
                $logger->critical("No result received - seller id: " . $sellerId . ", MWS marketplace id: " . $mwsMarketplaceId . ", next token: " . $nextToken);
            } else {
                $nextToken = $result->getNextToken();
                $requestInfoList = $result->getReportRequestInfoList();

                foreach($requestInfoList as &$requestInfo) {
                    if(ReportsUtil::storeRequestInfo($marketplaceInstanceVo, $requestInfo)) {
                        $storedRequestInfoCount++;
                    }
                }

                unset($requestInfo);

                if($listStatusIsEmpty) {
                    $reportRequestListId =& PDOQuery::insert()
                        ->into("amazon_mws.report_request_list")
                        ->value("created", "UNIX_TIMESTAMP()")
                        ->value("max_count", self::MAX_COUNT, PDO::PARAM_INT)
                        ->value("requested_from_date", $requestedFromTimestamp, PDO::PARAM_INT)
                        ->value("next_token", $nextToken, PDO::PARAM_STR)
                        ->value("response_header_metadata", $response->getResponseHeaderMetadata(), PDO::PARAM_STR)
                        ->executeEnsuringLastInsertId($connection)
                    ;
                    PDOQuery::insert()
                        ->into("amazon_mws.report_request_list_marketplace_instance")
                        ->value("report_request_list_id", $reportRequestListId, PDO::PARAM_INT)
                        ->value("marketplace_instance_id", $marketplaceInstanceVo->getId(), PDO::PARAM_INT)
                        ->executeEnsuringRowCount($connection, 1)
                    ;
                    $logger->debug("Inserted Amazon report request record for MWS marketplace id: " . $mwsMarketplaceId . ", next token: " . $nextToken);
                } else {
                    PDOQuery::update()
                        ->table("amazon_mws.report_request_list")
                        ->set("updated", "UNIX_TIMESTAMP()")
                        ->set("requested_from_date", $requestedFromTimestamp, PDO::PARAM_INT)
                        ->set("next_token", $nextToken, PDO::PARAM_STR)
                        ->set("response_header_metadata", $response->getResponseHeaderMetadata(), PDO::PARAM_STR)
                        ->where("id", $listStatus["id"], PDO::PARAM_INT)
                        ->executeEnsuringRowCount($connection, 1)
                    ;
                    $logger->debug("Updated Amazon report request record for MWS marketplace id: " . $mwsMarketplaceId . ", next token: " . $nextToken);
                }

                PDOUtil::commitConnection($connection);
            }

            $logger->log(
                ($storedRequestInfoCount > 0 ? Logger::INFO : Logger::DEBUG),
                "Stored " . $storedRequestInfoCount . " report request changes"
            );

            return true;
        } catch(MarketplaceWebService_Exception $e) {
            LogUtil::logException(Logger::ERROR, $e,
                "MWS service call failed with status code: " . $e->getStatusCode() .
                ", error code: " . $e->getErrorCode() .
                ", error type: " . $e->getErrorType() .
                ", request ID: " . $e->getRequestId() .
                ", responseHeaderMetadata: " . $e->getResponseHeaderMetadata() .
                ", xml: " . $e->getXML()
            );
        } catch(Exception $e) {
            LogUtil::logException(Logger::CRITICAL, $e, "Failed to store Amazon report request info");
        }

        PDOUtil::rollbackConnection($connection);

        return false;
    }

    private function &getListStatus(MarketplaceInstanceVO &$marketplaceInstanceVo, PDO $connection) {
        return PDOQuery::select()->columns("list.id", "list.next_token")
            ->from("amazon_mws.report_request_list list")
            ->from("amazon_mws.report_request_list_marketplace_instance mktpl")
            ->where("list.id", "mktpl.report_request_list_id")
            ->where("mktpl.marketplace_instance_id", $marketplaceInstanceVo->getId(), PDO::PARAM_INT)
            ->where("list.max_count", self::MAX_COUNT, PDO::PARAM_INT)
            ->whereNull("list.requested_to_date")
            ->where("NOT EXISTS (" .
                "SELECT 1 FROM amazon_mws.report_request_list_marketplace_instance wrong_mktpl " .
                "WHERE wrong_mktpl.report_request_list_id = list.id " .
                "AND wrong_mktpl.marketplace_instance_id <> mktpl.marketplace_instance_id LIMIT 1" .
            ")")
            ->where("NOT EXISTS (" .
                "SELECT 1 FROM amazon_mws.report_request_list_mws_report_request_id param " .
                "WHERE param.report_request_list_id = list.id LIMIT 1" .
            ")")
            ->where("NOT EXISTS (" .
                "SELECT 1 FROM amazon_mws.report_request_list_report_type param " .
                "WHERE param.report_request_list_id = list.id LIMIT 1" .
            ")")
            ->where("NOT EXISTS (" .
                "SELECT 1 FROM amazon_mws.report_request_list_report_processing_status param " .
                "WHERE param.report_request_list_id = list.id LIMIT 1" .
            ")")
            ->options("FOR UPDATE")
            ->getSingleRow($connection)
        ;
    }

    private function getRecentCompletedDate(MarketplaceInstanceVO &$marketplaceInstanceVo, PDO $connection) {
        return PDOQuery::select()->columns("completed_date")
            ->from("amazon_mws.report_request request")
            ->from("amazon_mws.report_request_marketplace_instance mktpl")
            ->where("request.id", "mktpl.report_request_id")
            ->where("mktpl.marketplace_instance_id", $marketplaceInstanceVo->getId(), PDO::PARAM_INT)
            ->whereNotNull("request.completed_date")
            ->where("NOT EXISTS (" .
                "SELECT 1 FROM amazon_mws.report_request_list_marketplace_instance wrong_mktpl " .
                "WHERE wrong_mktpl.report_request_list_id = request.id " .
                "AND wrong_mktpl.marketplace_instance_id <> mktpl.marketplace_instance_id LIMIT 1" .
            ")")
            ->orderBy("request.completed_date desc")
            ->limit(1)
            ->getSingleValue($connection)
        ;
    }
}
