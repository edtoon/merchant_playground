<?php

namespace BlakePrinting\AmazonMws\Reports\CLI;

use BlakePrinting\CLI\CLIApplication;

class Main extends CLIApplication {
    const APP_NAME = "AmazonMwsReports";
    const APP_VERSION = "0.0.1";

    public function __construct() {
        parent::__construct(self::APP_NAME, self::APP_VERSION);

        $this->add(new ReportListCommand());
        $this->add(new RequestAcknowledgeCommand());
        $this->add(new RequestListCommand());
        $this->add(new RequestCreateCommand());
        $this->add(new RequestRetrieveCommand());
        $this->add(new RequestCancelCommand());
    }
}
