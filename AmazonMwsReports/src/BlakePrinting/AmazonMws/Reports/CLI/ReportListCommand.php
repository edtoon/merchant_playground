<?php

namespace BlakePrinting\AmazonMws\Reports\CLI;

use BlakePrinting\AmazonMws\Reports\AmazonReportResultReader;
use BlakePrinting\AmazonMws\Reports\ReportsClientFactory;
use BlakePrinting\AmazonMws\Reports\ReportsUtil;
use BlakePrinting\AmazonMws\Sellers\AmazonMwsUtil;
use BlakePrinting\AmazonMws\Sellers\CredentialVO;
use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceVO;
use BlakePrinting\CLI\CLICommand;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Util\LogUtil;
use Monolog\Logger;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use MarketplaceWebService_Model_GetReportListByNextTokenRequest;
use MarketplaceWebService_Model_GetReportListRequest;
use MarketplaceWebService_Exception;
use DateTime;
use Exception;
use PDO;

class ReportListCommand extends CLICommand {
    const MAX_COUNT = 100;

    protected function configure() {
        $this
            ->setName("amazon:mws:reports:list-reports")
            ->setDescription("List reports")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        AmazonMwsUtil::forEachActiveMarketplace(function(MarketplaceInstanceVO &$marketplaceInstanceVo, CredentialVO $credentialVo, PDO $connection) {
            $this->listReportsForInstanceAndConfig($marketplaceInstanceVo, $credentialVo, $connection);

            PDOUtil::commitConnection($connection);
        }, function(PDO $connection) {
            PDOUtil::rollbackConnection($connection);
        });

        return true;
    }

    private function listReportsForInstanceAndConfig(MarketplaceInstanceVO &$marketplaceInstanceVo, CredentialVO &$credentialVo, PDO $connection) {
        $mwsMarketplaceId = $marketplaceInstanceVo->getMwsMarketplaceId();
        $reportsClientFactory = new ReportsClientFactory();
        $reportsClient =& $reportsClientFactory->createClient(Main::APP_NAME, Main::APP_VERSION, $credentialVo, $connection);
        $logger =& LogUtil::getLogger();
        $listStatus =& $this->getListStatus($marketplaceInstanceVo, $connection);
        $listStatusIsEmpty = empty($listStatus);
        $nextToken = ($listStatusIsEmpty ? null : $listStatus["next_token"]);
        $storedReportInfoCount = 0;
        $availableFromDate = null;
        $response = null;
        $result = null;

        try {
            if(empty($nextToken)) {
                $recentAvailableDate = $this->getRecentAvailableDate($marketplaceInstanceVo, $connection);
                $request = new MarketplaceWebService_Model_GetReportListRequest();
                $request->setMaxCount(self::MAX_COUNT);
                $request->setMarketplace($mwsMarketplaceId);
                $request->setMerchant($credentialVo->getSellerId());
                if($recentAvailableDate !== null) {
                    $availableFromDate = $recentAvailableDate;

                    $request->setAvailableFromDate((new DateTime("@" . $recentAvailableDate))->format(DateTime::ATOM));
                }
                $response = $reportsClient->getReportList($request);
                $result = $response->getGetReportListResult();
            } else {
                $request = new MarketplaceWebService_Model_GetReportListByNextTokenRequest();
                $request->setMarketplace($mwsMarketplaceId);
                $request->setMerchant($credentialVo->getSellerId());
                $request->setNextToken($nextToken);
                $response = $reportsClient->getReportListByNextToken($request);
                $result = $response->getGetReportListByNextTokenResult();
            }

            AmazonMwsUtil::logResponse($response);

            if(empty($result)) {
                $resultReader = new AmazonReportResultReader($response);
                $result = $resultReader->getResult();
            }

            if(empty($result)) {
                $logger->critical("No result received - seller id: " . $credentialVo->getSellerId() . ", MWS marketplace id: " . $mwsMarketplaceId . ", next token: " . $nextToken);
            } else {
                $nextToken = $result->getNextToken();
                $reportInfoList = $result->getReportInfoList();

                foreach($reportInfoList as &$reportInfo) {
                    if(ReportsUtil::storeReportInfo($marketplaceInstanceVo, $reportInfo)) {
                        $storedReportInfoCount++;
                    }
                }

                unset($reportInfo);

                if($listStatusIsEmpty) {
                    $reportListId =& PDOQuery::insert()
                        ->into("amazon_mws.report_list")
                        ->value("created", "UNIX_TIMESTAMP()")
                        ->value("max_count", self::MAX_COUNT, PDO::PARAM_INT)
                        ->value("available_from_date", $availableFromDate, PDO::PARAM_INT)
                        ->value("next_token", $nextToken, PDO::PARAM_STR)
                        ->value("response_header_metadata", $response->getResponseHeaderMetadata(), PDO::PARAM_STR)
                        ->executeEnsuringLastInsertId($connection)
                    ;
                    PDOQuery::insert()
                        ->into("amazon_mws.report_list_marketplace_instance")
                        ->value("report_list_id", $reportListId, PDO::PARAM_INT)
                        ->value("marketplace_instance_id", $marketplaceInstanceVo->getId(), PDO::PARAM_INT)
                        ->executeEnsuringRowCount($connection, 1)
                    ;
                    $logger->debug("Inserted Amazon report record for MWS marketplace id: " . $mwsMarketplaceId . ", next token: " . $nextToken);
                } else {
                    PDOQuery::update()
                        ->table("amazon_mws.report_list")
                        ->set("updated", "UNIX_TIMESTAMP()")
                        ->set("available_from_date", $availableFromDate, PDO::PARAM_INT)
                        ->set("next_token", $nextToken, PDO::PARAM_STR)
                        ->set("response_header_metadata", $response->getResponseHeaderMetadata(), PDO::PARAM_STR)
                        ->where("id", $listStatus["id"], PDO::PARAM_INT)
                        ->executeEnsuringRowCount($connection, 1)
                    ;
                    $logger->debug("Updated Amazon report record for MWS marketplace id: " . $mwsMarketplaceId . ", next token: " . $nextToken);
                }

                PDOUtil::commitConnection($connection);
            }

            $logger->log(
                ($storedReportInfoCount > 0 ? Logger::INFO : Logger::DEBUG),
                "Stored " . $storedReportInfoCount . " report changes"
            );

            return true;
        } catch(MarketplaceWebService_Exception $e) {
            LogUtil::logException(Logger::ERROR, $e,
                "MWS service call failed with status code: " . $e->getStatusCode() .
                ", error code: " . $e->getErrorCode() .
                ", error type: " . $e->getErrorType() .
                ", request ID: " . $e->getRequestId() .
                ", responseHeaderMetadata: " . $e->getResponseHeaderMetadata() .
                ", xml: " . $e->getXML()
            );
        } catch(Exception $e) {
            LogUtil::logException(Logger::CRITICAL, $e, "Failed to store Amazon report info");
        }

        PDOUtil::rollbackConnection($connection);

        return false;
    }

    private function &getListStatus(MarketplaceInstanceVO &$marketplaceInstanceVo, PDO $connection) {
        return PDOQuery::select()->columns("list.id", "list.next_token")
            ->from("amazon_mws.report_list list")
            ->from("amazon_mws.report_list_marketplace_instance mktpl")
            ->where("list.id", "report_list_id")
            ->where("mktpl.marketplace_instance_id", $marketplaceInstanceVo->getId(), PDO::PARAM_INT)
            ->where("list.max_count", self::MAX_COUNT, PDO::PARAM_INT)
            ->whereNull("list.available_to_date")
            ->whereNull("list.acknowledged")
            ->where("NOT EXISTS (" .
                "SELECT 1 FROM amazon_mws.report_list_marketplace_instance wrong_mktpl " .
                "WHERE wrong_mktpl.report_list_id = list.id " .
                "AND wrong_mktpl.marketplace_instance_id <> mktpl.marketplace_instance_id LIMIT 1" .
            ")")
            ->where("NOT EXISTS (" .
                "SELECT 1 FROM amazon_mws.report_list_mws_report_request_id param " .
                "WHERE param.report_list_id = list.id LIMIT 1" .
            ")")
            ->where("NOT EXISTS (" .
                "SELECT 1 FROM amazon_mws.report_list_report_type param " .
                "WHERE param.report_list_id = list.id LIMIT 1" .
            ")")
            ->options("FOR UPDATE")
            ->getSingleRow($connection)
        ;
    }

    private function getRecentAvailableDate(MarketplaceInstanceVO &$marketplaceInstanceVo, PDO $connection) {
        return PDOQuery::select()->columns("available_date")
            ->from("amazon_mws.report report")
            ->from("amazon_mws.report_marketplace_instance mktpl")
            ->where("report.id", "mktpl.report_id")
            ->where("mktpl.marketplace_instance_id", $marketplaceInstanceVo->getId(), PDO::PARAM_INT)
            ->whereNotNull("report.available_date")
            ->where("NOT EXISTS (" .
                "SELECT 1 FROM amazon_mws.report_list_marketplace_instance wrong_mktpl " .
                "WHERE wrong_mktpl.report_list_id = report.id " .
                "AND wrong_mktpl.marketplace_instance_id <> mktpl.marketplace_instance_id LIMIT 1" .
            ")")
            ->orderBy("report.available_date desc")
            ->limit(1)
            ->getSingleValue($connection)
        ;
    }
}
