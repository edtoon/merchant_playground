<?php

namespace BlakePrinting\AmazonMws\Reports\CLI;

use BlakePrinting\AmazonMws\Reports\AmazonReportResultReader;
use BlakePrinting\AmazonMws\Reports\ReportsClientFactory;
use BlakePrinting\AmazonMws\Reports\ReportsUtil;
use BlakePrinting\AmazonMws\Sellers\AmazonMwsUtil;
use BlakePrinting\AmazonMws\Sellers\CredentialDAO;
use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceDAO;
use BlakePrinting\AmazonMws\Sellers\MarketplaceInstanceVO;
use BlakePrinting\CLI\CLICommand;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Util\LogUtil;
use MarketplaceWebService_Model_IdList;
use Monolog\Logger;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use MarketplaceWebService_Model_CancelReportRequestsRequest;
use MarketplaceWebService_Exception;
use Exception;
use PDO;

class RequestCancelCommand extends CLICommand {
    protected function configure() {
        $this
            ->setName("amazon:mws:reports:cancel-request")
            ->setDescription("Cancel report requests")
            ->addArgument("domain", InputArgument::REQUIRED, "Amazon domain to report on")
            ->addOption("mws_request_id", "id", InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, "MWS report request id (can specify multiple)")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $domain = $input->getArgument("domain");

        if(empty($domain)) {
            $output->writeln("No domain specified to request");
            return false;
        }

        $mwsRequestIdOption = $input->getOption("mws_request_id");
        $mwsRequestIds = ($mwsRequestIdOption == null ? null : (is_array($mwsRequestIdOption) ? $mwsRequestIdOption : [$mwsRequestIdOption]));

        if(empty($mwsRequestIds)) {
            $output->writeln("No --id specified");
            return false;
        } else {
            $invalidIds = [];

            foreach($mwsRequestIds as &$mwsRequestId) {
                if(!ctype_digit($mwsRequestId)) {
                    $invalidIds[] = $mwsRequestId;
                }
            }

            unset($mwsRequestId);

            if(!empty($invalidIds)) {
                $output->writeln("Invalid MWS report request ids specified: " . implode(",", $invalidIds));
                return false;
            }
        }

        $connection =& PDOUtil::getMysqlConnection();

        try {
            $marketplaceInstanceDao =& MarketplaceInstanceDAO::instance($connection);
            $marketplaceInstanceVo =& $marketplaceInstanceDao->findByColumn("domain_name", $domain, PDO::PARAM_STR);

            if($marketplaceInstanceVo === null) {
                $marketplaceDomains = array_map(
                    function(MarketplaceInstanceVO $marketplaceInstanceVo) {
                        return $marketplaceInstanceVo->getDomainName();
                    },
                    $marketplaceInstanceDao->findAll()
                );

                $output->writeln("No marketplace instances found for domain: " . $domain . ", existing domains are:\n    " . implode("\n    ", $marketplaceDomains));
                return false;
            }

            $logger =& LogUtil::getLogger();
            $mwsMarketplaceId = $marketplaceInstanceVo->getMwsMarketplaceId();
            $credentialDao =& CredentialDAO::instance($connection);
            $credentialVo =& $credentialDao->findById($marketplaceInstanceVo->getCredentialId());
            $sellerId = $credentialVo->getSellerId();
            $reportsClientFactory = new ReportsClientFactory();
            $reportsClient =& $reportsClientFactory->createClient(Main::APP_NAME, Main::APP_VERSION, $credentialVo, $connection);
            $requestIdList = new MarketplaceWebService_Model_IdList();

            foreach($mwsRequestIds as &$mwsRequestId) {
                $requestIdList->withId($mwsRequestId);
            }

            unset($mwsRequestId);

            $logger->debug("Requesting cancellation of report ids: " . implode(", ", $mwsRequestIds));

            $request = new MarketplaceWebService_Model_CancelReportRequestsRequest();
            $request->setMerchant($sellerId);
            $request->setReportRequestIdList($requestIdList);
            $request->setMarketplace($mwsMarketplaceId);
            $response = $reportsClient->cancelReportRequests($request);

            AmazonMwsUtil::logResponse($response);

            $result = $response->getCancelReportRequestsResult();

            if(empty($result)) {
                $resultReader = new AmazonReportResultReader($response);
                $result = $resultReader->getResult();
            }

            if(empty($result)) {
                $logger->critical("No result received - merchant id: " . $sellerId . ", MWS marketplace id: " . $mwsMarketplaceId);
            } else {
                $requestInfoList = $result->getReportRequestInfoList();

                if(empty($requestInfoList)) {
                    $logger->critical("No report request info received - merchant id: " . $sellerId . ", MWS marketplace id: " . $mwsMarketplaceId);
                } else {
                    $updatedRequestIds = [];

                    foreach($requestInfoList as &$requestInfo) {
                        $requestId = ReportsUtil::storeRequestInfo($marketplaceInstanceVo, $requestInfo);

                        if(!empty($requestId)) {
                            $updatedRequestIds[] = $requestId;
                        }
                    }

                    unset($requestInfo);

                    $logger->info("Cancelled " . count($updatedRequestIds) . " Amazon report requests out of " . count($mwsRequestIds) . " requested for MWS marketplace id: " . $mwsMarketplaceId);
                }

                PDOUtil::commitConnection($connection);

                return true;
            }
        } catch(MarketplaceWebService_Exception $e) {
            LogUtil::logException(Logger::ERROR, $e,
                "MWS service call failed with status code: " . $e->getStatusCode() .
                ", error code: " . $e->getErrorCode() .
                ", error type: " . $e->getErrorType() .
                ", request ID: " . $e->getRequestId() .
                ", responseHeaderMetadata: " . $e->getResponseHeaderMetadata() .
                ", xml: " . $e->getXML()
            );
        } catch(Exception $e) {
            LogUtil::logException(Logger::CRITICAL, $e, "Failed to store Amazon report request details");
        }

        PDOUtil::rollbackConnection($connection);

        return false;
    }
}
