<?php

namespace BlakePrinting\AmazonMws\Reports;

use BlakePrinting\Db\ValueObject;

class ReportTransformBatchVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var string */
    private $created = null;
    /** @var string */
    private $reportId = null;
    /** @var int */
    private $marketplaceInstanceId = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getReportId()
    {
        return $this->reportId;
    }

    /**
     * @param string $reportId
     */
    public function setReportId($reportId)
    {
        $this->reportId = $reportId;
    }

    /**
     * @return int
     */
    public function getMarketplaceInstanceId()
    {
        return $this->marketplaceInstanceId;
    }

    /**
     * @param int $marketplaceInstanceId
     */
    public function setMarketplaceInstanceId($marketplaceInstanceId)
    {
        $this->marketplaceInstanceId = $marketplaceInstanceId;
    }
}
