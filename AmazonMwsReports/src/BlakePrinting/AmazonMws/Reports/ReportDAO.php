<?php

namespace BlakePrinting\AmazonMws\Reports;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ReportDAO instance(PDO $connection = null, $schemaName = null)
 * @method ReportVO findById($id, $options = null, $useCache = false)
 * @method ReportVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ReportVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ReportVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ReportVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ReportVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ReportVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ReportDAO extends AbstractDataAccessObject {
}
