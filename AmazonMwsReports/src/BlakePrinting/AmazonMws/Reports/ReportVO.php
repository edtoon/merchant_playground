<?php

namespace BlakePrinting\AmazonMws\Reports;

use BlakePrinting\Db\ValueObject;

class ReportVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var string */
    private $reportId = null;
    /** @var string */
    private $reportType = null;
    /** @var string */
    private $mwsReportRequestId = null;
    /** @var int */
    private $availableDate = null;
    /** @var bool */
    private $acknowledged = null;
    /** @var int */
    private $acknowledgedDate = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getReportId()
    {
        return $this->reportId;
    }

    /**
     * @param string $reportId
     */
    public function setReportId($reportId)
    {
        $this->reportId = $reportId;
    }

    /**
     * @return string
     */
    public function getReportType()
    {
        return $this->reportType;
    }

    /**
     * @param string $reportType
     */
    public function setReportType($reportType)
    {
        $this->reportType = $reportType;
    }

    /**
     * @return string
     */
    public function getMwsReportRequestId()
    {
        return $this->mwsReportRequestId;
    }

    /**
     * @param string $mwsReportRequestId
     */
    public function setMwsReportRequestId($mwsReportRequestId)
    {
        $this->mwsReportRequestId = $mwsReportRequestId;
    }

    /**
     * @return int
     */
    public function getAvailableDate()
    {
        return $this->availableDate;
    }

    /**
     * @param int $availableDate
     */
    public function setAvailableDate($availableDate)
    {
        $this->availableDate = $availableDate;
    }

    /**
     * @return boolean
     */
    public function isAcknowledged()
    {
        return $this->acknowledged;
    }

    /**
     * @param boolean $acknowledged
     */
    public function setAcknowledged($acknowledged)
    {
        $this->acknowledged = $acknowledged;
    }

    /**
     * @return int
     */
    public function getAcknowledgedDate()
    {
        return $this->acknowledgedDate;
    }

    /**
     * @param int $acknowledgedDate
     */
    public function setAcknowledgedDate($acknowledgedDate)
    {
        $this->acknowledgedDate = $acknowledgedDate;
    }
}
