<?php

namespace BlakePrinting\AmazonMws\Reports;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ReportTransformBatchDAO instance(PDO $connection = null, $schemaName = null)
 * @method ReportTransformBatchVO findById($id, $options = null, $useCache = false)
 * @method ReportTransformBatchVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ReportTransformBatchVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ReportTransformBatchVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ReportTransformBatchVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ReportTransformBatchVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ReportTransformBatchVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ReportTransformBatchDAO extends AbstractDataAccessObject {
}
