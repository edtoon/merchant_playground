<?php

namespace BlakePrinting\AmazonMws\Reports;

use BlakePrinting\Db\ValueObject;

class ReportMarketplaceInstanceVO implements ValueObject {
    /** @var int */
    private $reportId = null;
    /** @var int */
    private $marketplaceInstanceId = null;

    /**
     * @return int
     */
    public function getReportId()
    {
        return $this->reportId;
    }

    /**
     * @param int $reportId
     */
    public function setReportId($reportId)
    {
        $this->reportId = $reportId;
    }

    /**
     * @return int
     */
    public function getMarketplaceInstanceId()
    {
        return $this->marketplaceInstanceId;
    }

    /**
     * @param int $marketplaceInstanceId
     */
    public function setMarketplaceInstanceId($marketplaceInstanceId)
    {
        $this->marketplaceInstanceId = $marketplaceInstanceId;
    }
}
