<?php

namespace BlakePrinting\AmazonMws\Reports;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ReportMarketplaceInstanceDAO instance(PDO $connection = null, $schemaName = null)
 * @method ReportMarketplaceInstanceVO findById($id, $options = null, $useCache = false)
 * @method ReportMarketplaceInstanceVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ReportMarketplaceInstanceVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ReportMarketplaceInstanceVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ReportMarketplaceInstanceVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ReportMarketplaceInstanceVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ReportMarketplaceInstanceVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ReportMarketplaceInstanceDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
