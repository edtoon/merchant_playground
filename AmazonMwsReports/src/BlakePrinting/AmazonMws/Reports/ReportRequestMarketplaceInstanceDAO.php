<?php

namespace BlakePrinting\AmazonMws\Reports;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ReportRequestMarketplaceInstanceDAO instance(PDO $connection = null, $schemaName = null)
 * @method ReportRequestMarketplaceInstanceVO findById($id, $options = null, $useCache = false)
 * @method ReportRequestMarketplaceInstanceVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ReportRequestMarketplaceInstanceVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ReportRequestMarketplaceInstanceVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ReportRequestMarketplaceInstanceVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ReportRequestMarketplaceInstanceVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ReportRequestMarketplaceInstanceVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ReportRequestMarketplaceInstanceDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
