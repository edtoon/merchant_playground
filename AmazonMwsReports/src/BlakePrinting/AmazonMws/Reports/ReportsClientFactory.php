<?php

namespace BlakePrinting\AmazonMws\Reports;

use BlakePrinting\AmazonMws\Sellers\AmazonMwsClientFactory;
use BlakePrinting\AmazonMws\Sellers\CredentialVO;
use MarketplaceWebService_Client;
use MarketplaceWebService_Interface;
use MarketplaceWebService_Mock;
use PDO;

/**
 * @method MarketplaceWebService_Interface createClient($applicationName, $applicationVersion, CredentialVO &$credentialVo = null, PDO $connection = null)
 */
class ReportsClientFactory extends AmazonMwsClientFactory {
    public function __construct() {
        parent::__construct(
            AmazonMwsClientFactory::REPORT_API, "amazon_mws_reports/src",
            MarketplaceWebService_Client::class, MarketplaceWebService_Mock::class,
            ""
        );
    }
}
