<?php

namespace BlakePrinting\AmazonMws\Reports;

use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use Exception;

class AmazonReportResultReader {
    private $response = null;
    private $result = null;

    public function __construct($response) {
        if(empty($response->domElement)) {
            throw new Exception("Tried to read a response without a dom element");
        }

        $this->response =& $response;
    }

    public function getResult() {
        if($this->result === null) {
            self::processNode($this->response->domElement);
        }

        return $this->result;
    }

    private function processNode($domNode) {
        $logger =& LogUtil::getLogger();

        $attributeCount = ($domNode->attributes === null ? 0 : $domNode->attributes->length);
        $childCount = $domNode->childNodes->length;

        $logger->debug("Node: " . $domNode->nodeName .
            ", children: " . $childCount .
            ", attributes: " . $attributeCount
        );

        if(substr($domNode->nodeName, -6) == "Result") {
            $className = "MarketplaceWebService_Model_" . $domNode->nodeName;
            $this->result = new $className($domNode);
        } else {
            foreach($domNode->childNodes as $childNode) {
                if($childNode->nodeType == XML_ELEMENT_NODE) {
                    self::processNode($childNode);
                } else if($childNode->nodeType == XML_TEXT_NODE) {
                    if(!empty(trim($childNode->wholeText))) {
                        $logger->debug("Text: " . $childNode->wholeText);
                    }
                } else if($childNode->nodeType == XML_COMMENT_NODE) {
                } else {
                    $logger->critical("I don't know what this is: " . Utils::getVarDump($childNode));
                }
            }
        }
    }
}
