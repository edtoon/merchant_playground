<?php

use BlakePrinting\AmazonMws\EndpointVO;
use BlakePrinting\AmazonMws\Reports\AmazonReportResultReader;
use BlakePrinting\AmazonMws\Reports\ReportsClientFactory;
use BlakePrinting\AmazonMws\Reports\ReportTypes;
use BlakePrinting\AmazonMws\Sellers\AmazonMwsUtil;
use BlakePrinting\AmazonMws\Sellers\CredentialVO;
use BlakePrinting\Test\AbstractTestCase;

class AmazonReportResultReaderTest extends AbstractTestCase {
    /** @var MarketplaceWebService_Interface */
    private static $client = null;

    public static function setUpBeforeClass() {
        parent::setUpBeforeClass();

        $reportsClientFactory = new ReportsClientFactory();
        $endpointVo = new EndpointVO();
        $credentialVo = new CredentialVO();

        $endpointVo->setId(AmazonMwsUtil::MOCK_ENDPOINT_ID);
        $endpointVo->setEndpoint(AmazonMwsUtil::MOCK_ENDPOINT);
        $credentialVo->setSellerId(AmazonMwsUtil::MOCK_SELLER_ID);
        $reportsClientFactory->setEndpointVo($endpointVo);
        $reportsClientFactory->setCredentialVo($credentialVo);

        self::$client =& $reportsClientFactory->createClient(self::class, "0.0.1");
    }

    public function testGetRequestList() {
        $request = new MarketplaceWebService_Model_GetReportRequestListRequest();
        $request->setMerchant(AmazonMwsUtil::MOCK_SELLER_ID);
        $response = self::$client->getReportRequestList($request);

        AmazonMwsUtil::logResponse($response);

        $result = $response->getGetReportRequestListResult();

        if(empty($result)) {
            $resultReader = new AmazonReportResultReader($response);
            $result = $resultReader->getResult();
        }

        $this->assertNotEmpty($result);
        $this->assertEquals("string", $result->getNextToken());
        $this->assertFalse($result->getHasNext());

        $reportRequestInfoList = $result->getReportRequestInfoList();

        $this->assertNotEmpty($reportRequestInfoList);
        $this->assertEquals(1, count($reportRequestInfoList));

        foreach($reportRequestInfoList as $requestInfo) {
            $this->assertNotEmpty($requestInfo);
            $this->assertEquals("string", $requestInfo->getReportRequestId());
            $this->assertEquals("string", $requestInfo->getReportType());
            $this->assertEquals("2007-10-25T23:36:28+00:00", $requestInfo->getStartDate()->format(DateTime::ATOM));
            $this->assertEquals("2004-02-14T10:44:14+00:00", $requestInfo->getEndDate()->format(DateTime::ATOM));
            $this->assertFalse($requestInfo->getScheduled());
            $this->assertEquals("2018-10-31T22:36:46-07:00", $requestInfo->getSubmittedDate()->format(DateTime::ATOM));
            $this->assertEquals("string", $requestInfo->getReportProcessingStatus());
            $this->assertEquals("2008-04-22T10:44:23+00:00", $requestInfo->getStartedProcessingDate()->format(DateTime::ATOM));
            $this->assertEquals("2012-01-12T03:16:16+00:00", $requestInfo->getCompletedDate()->format(DateTime::ATOM));
        }
    }

    public function testRequestReport() {
        $request = new MarketplaceWebService_Model_RequestReportRequest();
        $request->setMerchant(AmazonMwsUtil::MOCK_SELLER_ID);
        $request->setReportType(ReportTypes::REPORT_TYPE_FBA_INV_MANAGE_ARCHIVE);
        $request->setStartDate(date("c"));
        $request->setMarketplaceIdList(["Id" => [AmazonMwsUtil::MOCK_MARKETPLACE_ID]]);
        $response = self::$client->requestReport($request);

        AmazonMwsUtil::logResponse($response);

        $result = $response->getRequestReportResult();

        if(empty($result)) {
            $resultReader = new AmazonReportResultReader($response);
            $result = $resultReader->getResult();
        }

        $this->assertNotEmpty($result);

        $requestInfo = $result->getReportRequestInfo();

        $this->assertNotEmpty($requestInfo);
        $this->assertEquals("string", $requestInfo->getReportRequestId());
        $this->assertEquals("string", $requestInfo->getReportType());
        $this->assertEquals("2008-09-28T18:49:45+00:00", $requestInfo->getStartDate()->format(DateTime::ATOM));
        $this->assertEquals("2014-09-18T16:18:33+00:00", $requestInfo->getEndDate()->format(DateTime::ATOM));
        $this->assertFalse($requestInfo->getScheduled());
        $this->assertEquals("2006-08-19T10:27:14-07:00", $requestInfo->getSubmittedDate()->format(DateTime::ATOM));
        $this->assertEquals("string", $requestInfo->getReportProcessingStatus());
        $this->assertEquals("2008-04-22T10:44:23+00:00", $requestInfo->getStartedProcessingDate()->format(DateTime::ATOM));
        $this->assertEquals("2012-01-12T03:16:16+00:00", $requestInfo->getCompletedDate()->format(DateTime::ATOM));
    }

    public function testGetReportList() {
        $request = new MarketplaceWebService_Model_GetReportListRequest();
        $request->setMaxCount(100);
        $request->setMerchant(AmazonMwsUtil::MOCK_SELLER_ID);
        $response = self::$client->getReportList($request);

        AmazonMwsUtil::logResponse($response);

        $result = $response->getGetReportListResult();

        if(empty($result)) {
            $resultReader = new AmazonReportResultReader($response);
            $result = $resultReader->getResult();
        }

        $this->assertNotEmpty($result);
        $this->assertEquals("string", $result->getNextToken());
        $this->assertFalse($result->getHasNext());

        $reportInfoList = $result->getReportInfoList();

        $this->assertNotEmpty($reportInfoList);
        $this->assertEquals(1, count($reportInfoList));

        foreach($reportInfoList as $reportInfo) {
            $this->assertNotEmpty($reportInfo);
            $this->assertEquals("string", $reportInfo->getReportId());
            $this->assertEquals("string", $reportInfo->getReportType());
            $this->assertEquals("string", $reportInfo->getReportRequestId());
            $this->assertEquals("2007-10-25T23:36:28+00:00", $reportInfo->getAvailableDate()->format(DateTime::ATOM));
            $this->assertTrue($reportInfo->getAcknowledged());
            $this->assertEquals("2014-06-09T08:15:04-07:00", $reportInfo->getAcknowledgedDate()->format(DateTime::ATOM));
        }
    }
}
