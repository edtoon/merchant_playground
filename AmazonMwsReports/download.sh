#!/bin/bash
#
# Download non-Packagist dependencies
#

set -o errexit
set -o nounset

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

VEND_DIR="${VEND_DIR:=${BASE_DIR}/vendor}"
DL_DIR="${DL_DIR:=${BASE_DIR}/downloads}"

declare -a VENDOR_DOWNLOADS=(
  "amazon_mws_reports:https://images-na.ssl-images-amazon.com/images/G/01/mwsportal/clientlib/feeds/amazon-mws-v20090101-php-2015-06-18._V316922310_.zip"
)

unzip -v > /dev/null 2>&1 || sudo apt-get install unzip

mkdir -p "${DL_DIR}" "${VEND_DIR}"

for vendor_download in ${VENDOR_DOWNLOADS[@]}
do
  vendor_dir="${VEND_DIR}/${vendor_download%%:*}"
  vendor_url="${vendor_download#*:}"

  if [ ! -d "${vendor_dir}" ];
  then
    download_filename="${vendor_url##*/}"
    download_path="${DL_DIR}/${download_filename}"

    if [ ! -f "${download_path}" ];
    then
      curl -o "${download_path}" "${vendor_url}"
    fi

    if [ "${download_path: -4}" = ".zip" ];
    then
      mkdir -p "${vendor_dir}"
      unzip "${download_path}" -d "${vendor_dir}"
    else
      echo "Downloaded: ${download_path: -4}"
    fi
  fi
done

for patch_php_file in `find ${VEND_DIR}/amazon_mws_reports/src/MarketplaceWebService/ -name "*.php"`
do
  backup_file="${patch_php_file}.bak"

  if [ ! -f "${backup_file}" ] || [ "${patch_php_file}" -ot "${backup_file}" ];
  then
    sed -i.bak "s/ toXMLFragment/ _toXMLFragment/g" "${patch_php_file}"
    sed -i.bak "s/>toXMLFragment/>_toXMLFragment/g" "${patch_php_file}"
    sed -i.bak "s/protected function _toXMLFragment/public function _toXMLFragment/g" "${patch_php_file}"
    sed -i.bak "s/iconv_set_encoding/ @iconv_set_encoding/g" "${patch_php_file}"
    sed -i.bak "s/@return Cancel/@return MarketplaceWebService_Model_Cancel/g" "${patch_php_file}"
    sed -i.bak "s/@return Get/@return MarketplaceWebService_Model_Get/g" "${patch_php_file}"
    sed -i.bak "s/@return Report/@return MarketplaceWebService_Model_Report/g" "${patch_php_file}"
    sed -i.bak "s/@return Request/@return MarketplaceWebService_Model_Request/g" "${patch_php_file}"
    sed -i.bak "s/@return Update/@return MarketplaceWebService_Model_Update/g" "${patch_php_file}"
    sed -i.bak "s/@return array of ReportRequestInfo ReportRequestInfo/@return MarketplaceWebService_Model_ReportRequestInfo[]/g" "${patch_php_file}"
    sed -i.bak "s/@return array of ReportInfo ReportInfo/@return MarketplaceWebService_Model_ReportInfo[]/g" "${patch_php_file}"
  fi
done

for patch_php_file in `ls ${VEND_DIR}/amazon_mws_reports/src/MarketplaceWebService/*.php`
do
  backup_file="${patch_php_file}.bak"

  sed -i.bak "s/'MarketplaceWebService\//'/g" "${patch_php_file}"
done
for patch_php_file in `ls ${VEND_DIR}/amazon_mws_reports/src/MarketplaceWebService/Model/*.php`
do
  backup_file="${patch_php_file}.bak"

  sed -i.bak "s/'MarketplaceWebService\//__DIR__ . '\/\.\.\//g" "${patch_php_file}"
done

sed -i.bak "s/CURLOPT_VERBOSE => true/CURLOPT_VERBOSE => false/g" "${VEND_DIR}/amazon_mws_reports/src/MarketplaceWebService/Client.php"
sed -i.bak "s/fields = array ();/fields = array(); public \$domElement = null;/g" "${VEND_DIR}/amazon_mws_reports/src/MarketplaceWebService/Model.php"
sed -i.bak "s/this->fromDOMElement(\$data);/this->fromDOMElement( \$data ); \$this->domElement =\& \$data;/g" "${VEND_DIR}/amazon_mws_reports/src/MarketplaceWebService/Model.php"
sed -i.bak "s/require_once (str_replace/require_once (dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . str_replace/g" "${VEND_DIR}/amazon_mws_reports/src/MarketplaceWebService/Model.php"
sed -i.bak "s/@return string StartDate/@return DateTime StartDate/g" "${VEND_DIR}/amazon_mws_reports/src/MarketplaceWebService/Model/ReportRequestInfo.php"
sed -i.bak "s/@return string EndDate/@return DateTime EndDate/g" "${VEND_DIR}/amazon_mws_reports/src/MarketplaceWebService/Model/ReportRequestInfo.php"
sed -i.bak "s/@return string SubmittedDate/@return DateTime SubmittedDate/g" "${VEND_DIR}/amazon_mws_reports/src/MarketplaceWebService/Model/ReportRequestInfo.php"
sed -i.bak "s/@return string StartedProcessingDate/@return DateTime StartedProcessingDate/g" "${VEND_DIR}/amazon_mws_reports/src/MarketplaceWebService/Model/ReportRequestInfo.php"
sed -i.bak "s/@return string CompletedDate/@return DateTime CompletedDate/g" "${VEND_DIR}/amazon_mws_reports/src/MarketplaceWebService/Model/ReportRequestInfo.php"
sed -i.bak "s/@return string AvailableDate/@return DateTime AvailableDate/g" "${VEND_DIR}/amazon_mws_reports/src/MarketplaceWebService/Model/ReportInfo.php"
sed -i.bak "s/@return string AcknowledgedDate/@return DateTime AcknowledgedDate/g" "${VEND_DIR}/amazon_mws_reports/src/MarketplaceWebService/Model/ReportInfo.php"
