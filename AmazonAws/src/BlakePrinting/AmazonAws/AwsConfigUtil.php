<?php

namespace BlakePrinting\AmazonAws;

use Aws\Ec2\Ec2Client;
use Aws\Rds\RdsClient;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Aws\Sdk;
use BlakePrinting\Util\ConfigUtil;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use Stringy\StaticStringy as S;
use Symfony\Component\Console\Output\OutputInterface;
use Exception;

final class AwsConfigUtil {
    const CONFIG_AWS_ACCOUNT_ID = "###########################";
    const CONFIG_AWS_ACCOUNT_ID_CANONICAL = "###################";
    const CONFIG_AWS_GROUP_NAME = "default";
    const CONFIG_AWS_REGION = "us-east-1";
    const CONFIG_KEY_PRIVATE_FILE = "config.pem";
    const CONFIG_KEY_PUBLIC_FILE = "config.pub";
    const CONFIG_KEY_SIZE = 4096;
    const CONFIG_RDS_CLUSTER_ID = "aurora-prod-####################";
    const CONFIG_RDS_INSTANCE_ID_PREFIX = self::CONFIG_RDS_CLUSTER_ID . "-";
    const CONFIG_RDS_USERNAME = "########################";
    const CONFIG_S3_BUCKET_CONFIG = "########################";
    const CONFIG_S3_KEY_PROD_EIP = "########";
    const CONFIG_S3_KEY_PROD_RDS = "#########";
    const CONFIG_S3_KEY_PROD_PUB_KEY = "####.pub";

    /** @var Sdk */
    private static $awsSdk = null;
    /** @var Ec2Client */
    private static $ec2Client = null;
    /** @var RdsClient */
    private static $rdsClient = null;
    /** @var S3Client */
    private static $s3Client = null;
    /** @var string */
    private static $storedPublicKey = null;
    /** @var StoredRdsConfig */
    private static $storedRdsConfig = null;
    /** @var StoredElasticIP */
    private static $storedElasticIp = null;
    /** @var bool */
    private static $bucketEnsured = false;

    private function __construct() { }

    /**
     * @param string $env
     * @param string $publicKey
     * @return StoredElasticIP
     */
    public static function &getStoredElasticIp($env, $publicKey = null) {
        if($env == ConfigUtil::ENV_PROD && self::$storedElasticIp === null) {
            $encodedText = self::getObjectAnonymous(AwsConfigUtil::CONFIG_S3_KEY_PROD_EIP);

            if(!empty($encodedText)) {
                $publicKey = (empty($publicKey) ? self::getStoredPublicKey($env) : $publicKey);
                $decodedText = base64_decode($encodedText);
                $decryptedText = Utils::decryptWithKey($decodedText, $publicKey);
                $foundVersion = null;
                $foundAllocationId = null;
                $foundPublicIp = null;

                if(S::startsWith($decryptedText, "v1\n")) {
                    $decryptedParts = explode("\n", $decryptedText);

                    if(count($decryptedParts) == 4) {
                        list($foundVersion, $foundAllocationId, $foundPublicIp, $_) = $decryptedParts;
                    }
                }

                if(empty($foundVersion) || empty($foundAllocationId) || empty($foundPublicIp)) {
                    throw new Exception("Could not parse stored elastic IP configuration: " . $encodedText);
                }

                $elasticIp = new StoredElasticIP();

                $elasticIp->setConfigVersion($foundVersion);
                $elasticIp->setAllocationId($foundAllocationId);
                $elasticIp->setIpAddress($foundPublicIp);

                self::$storedElasticIp =& $elasticIp;
            }
        }

        return self::$storedElasticIp;
    }

    /**
     * @param string $env
     * @param string $publicKey
     * @return StoredRdsConfig
     */
    public static function &getStoredRdsConfig($env, $publicKey = null) {
        if($env == ConfigUtil::ENV_PROD && self::$storedRdsConfig === null) {
            $encodedText = self::getObjectAnonymous(AwsConfigUtil::CONFIG_S3_KEY_PROD_RDS);

            if(!empty($encodedText)) {
                $publicKey = (empty($publicKey) ? self::getStoredPublicKey($env) : $publicKey);
                $decodedText = base64_decode($encodedText);
                $decryptedText = Utils::decryptWithKey($decodedText, $publicKey);
                $foundVersion = null;
                $foundUsername = null;
                $foundPassword = null;
                $foundEndpoint = null;
                $foundPort = null;

                if(S::startsWith($decryptedText, "v1\n")) {
                    $decryptedParts = explode("\n", $decryptedText);

                    if(count($decryptedParts) == 6) {
                        list($foundVersion, $foundUsername, $foundPassword, $foundEndpoint, $foundPort, $_) = $decryptedParts;
                    }
                }

                if(empty($foundVersion) || empty($foundUsername) || empty($foundPassword) || empty($foundEndpoint) || empty($foundPort)) {
                    throw new Exception("Could not parse stored RDS configuration: " . $encodedText);
                }

                $rdsConfig = new StoredRdsConfig();

                $rdsConfig->setConfigVersion($foundVersion);
                $rdsConfig->setUsername($foundUsername);
                $rdsConfig->setPassword($foundPassword);
                $rdsConfig->setEndpoint($foundEndpoint);
                $rdsConfig->setPort($foundPort);

                self::$storedRdsConfig =& $rdsConfig;
            }
        }

        return self::$storedRdsConfig;
    }

    /**
     * @param string $env
     * @return string
     */
    public static function &getStoredPublicKey($env) {
        if($env == ConfigUtil::ENV_PROD && empty(self::$storedPublicKey)) {
            self::$storedPublicKey = self::getObjectAnonymous(AwsConfigUtil::CONFIG_S3_KEY_PROD_PUB_KEY);
        }

        return self::$storedPublicKey;
    }

    /**
     * @param string $keyDir
     * @param OutputInterface $output
     * @param bool $forCreate
     * @return bool
     */
    public static function validateLocalKeys($keyDir, OutputInterface $output = null, $forCreate = false) {
        $realKeyDir = null;

        if(empty($keyDir)) {
            self::writeToOutput($output, "No key directory specified");
        } else {
            $realKeyDir = realpath($keyDir);

            if(empty($realKeyDir) || !is_dir($realKeyDir)) {
                self::writeToOutput($output, "Key directory does not exist: " . $realKeyDir);
            } else {
                $privateKeyFilePath = AwsConfigUtil::getPrivateKeyPath($keyDir);
                $privateKeyFileExists = is_readable($privateKeyFilePath);
                $publicKeyFilePath = AwsConfigUtil::getPublicKeyPath($keyDir);
                $publicKeyFileExists = is_readable($publicKeyFilePath);
                $keysMatch = false;

                self::writeToOutput($output, "Private key " . ($privateKeyFileExists ? "exists" : "does not exist") . " locally: " . $privateKeyFilePath);
                self::writeToOutput($output, "Public key " . ($publicKeyFileExists ? "exists" : "does not exist") . " locally: " . $publicKeyFilePath);

                if($privateKeyFileExists && $publicKeyFileExists) {
                    $keysMatch = self::validateKeyFilesMatch($privateKeyFilePath, $publicKeyFilePath, $output);
                }

                if($forCreate) {
                    if(!$privateKeyFileExists && !$publicKeyFileExists) {
                        return true;
                    }
                } else if($privateKeyFileExists && $publicKeyFileExists && $keysMatch) {
                    return true;
                }
            }
        }

        return false;
    }

    public static function validateRemoteKey($keyDir, OutputInterface $output = null, $forCreate = false) {
        $publicKeyFilePath = AwsConfigUtil::getPublicKeyPath($keyDir);
        $publicKeyS3Path = AwsConfigUtil::CONFIG_S3_KEY_PROD_PUB_KEY;
        $publicKeyS3Url = AwsConfigUtil::getS3Url($publicKeyS3Path);
        $remotePublicKey = AwsConfigUtil::getObjectAnonymous($publicKeyS3Path);
        $remotePublicKeyExists = (!empty($remotePublicKey));

        self::ensureBucket();
        self::writeToOutput($output, "Stored public key " . ($remotePublicKeyExists ? ("exists at: " . $publicKeyS3Url) : "does not exist"));

        if($remotePublicKeyExists && is_readable($publicKeyFilePath)) {
            $localPublicKey = file_get_contents($publicKeyFilePath);
            $keysMatch = (base64_decode($localPublicKey) == base64_decode($remotePublicKey));

            self::writeToOutput($output, "The local public key at path: " . $publicKeyFilePath . " " . ($keysMatch ? "matches" : "differs from") . " the key at " . $publicKeyS3Url);

            if($keysMatch && !$forCreate) {
                return true;
            }
        } else if($forCreate && !$remotePublicKeyExists) {
            return true;
        }

        return false;
    }

    /**
     * @param string $keyDir
     * @return string
     */
    public static function getPrivateKeyPath($keyDir) {
        return realpath($keyDir) . DIRECTORY_SEPARATOR . self::CONFIG_KEY_PRIVATE_FILE;
    }

    /**
     * @param string $keyDir
     * @return string
     */
    public static function getPublicKeyPath($keyDir) {
        return realpath($keyDir) . DIRECTORY_SEPARATOR . self::CONFIG_KEY_PUBLIC_FILE;
    }

    /**
     * @param string $s3Key
     * @return string
     */
    public static function getS3Url($s3Key) {
        return "s3://" . AwsConfigUtil::CONFIG_S3_BUCKET_CONFIG . "/" . $s3Key;
    }

    /**
     * @param string $key
     * @param string $value
     * @param string $contentType
     */
    public static function putObject($key, $value, $contentType = null) {
        $config = [
            "ACL" => "public-read",
            "Body" => $value,
            "Bucket" => self::CONFIG_S3_BUCKET_CONFIG,
            "Key" => $key
        ];

        if($contentType !== null) {
            $config["ContentType"] = $contentType;
        }

        self::getS3Client()->putObject($config);
    }

    /**
     * @param string $key
     * @return string
     */
    public static function getObject($key) {
        $body = null;

        try {
            $result = self::getS3Client()->getObject([
                "Bucket" => self::CONFIG_S3_BUCKET_CONFIG,
                "Key" => $key
            ]);

            $body =& $result["Body"];
        } catch(S3Exception $e) {
            if($e->getAwsErrorCode() != "NoSuchKey") {
                throw $e;
            }
        }

        return $body;
    }

    /**
     * @param string $key
     * @return string
     */
    public static function getObjectAnonymous($key) {
        return file_get_contents("http://" . self::CONFIG_S3_BUCKET_CONFIG . ".s3.amazonaws.com/" . $key);
    }

    public static function ensureBucket() {
        if(self::$bucketEnsured === false) {
            try {
                $result = self::getS3Client()->createBucket([
                    "ACL" => "private",
                    "Bucket" => self::CONFIG_S3_BUCKET_CONFIG,
                    "CreateBucketConfiguration" => [
                        "LocationConstraint" => self::CONFIG_AWS_REGION
                    ]
                ]);
                $logger =& LogUtil::getLogger();

                $logger->debug(Utils::getVarDump($result));
            } catch(S3Exception $e) {
                if($e->getAwsErrorCode() != "BucketAlreadyOwnedByYou") {
                    throw $e;
                }
            }

            self::$bucketEnsured = true;
        }

        return self::$bucketEnsured;
    }

    /**
     * @return string[]
     */
    public static function getAvailabilityZones() {
        $availabilityZonesResult = self::getEc2Client()->describeAvailabilityZones();

        return array_map(
            function($zoneResult) {
                $zoneName =& $zoneResult["ZoneName"];
                $regionName =& $zoneResult["RegionName"];
                $state =& $zoneResult["State"];

                if($state != "available") {
                    throw new Exception("Zone: " . $zoneName . " is in the wrong state: " . $state);
                }

                if($regionName != AwsConfigUtil::CONFIG_AWS_REGION) {
                    throw new Exception("Zone: " . $zoneName . " is in the wrong region: " . $regionName);
                }

                return $zoneName;
            },
            $availabilityZonesResult["AvailabilityZones"]
        );
    }

    /**
     * @return S3Client
     */
    public static function getS3Client() {
        if(self::$s3Client === null) {
            self::$s3Client = self::getAwsSdk()->createS3();
        }

        return self::$s3Client;
    }

    /**
     * @return RdsClient
     */
    public static function getRdsClient() {
        if(self::$rdsClient === null) {
            self::$rdsClient = self::getAwsSdk()->createRds();
        }

        return self::$rdsClient;
    }

    /**
     * @return Ec2Client
     */
    public static function getEc2Client() {
        if(self::$ec2Client === null) {
            self::$ec2Client = self::getAwsSdk()->createEc2();
        }

        return self::$ec2Client;
    }

    /**
     * @return Sdk
     */
    public static function getAwsSdk() {
        if(self::$awsSdk === null) {
            $config = [
                "version" => "latest",
                "region" => AwsConfigUtil::CONFIG_AWS_REGION,
            ];

            if(ConfigUtil::getEnvironmentName() == ConfigUtil::ENV_PROD) {
                $config["credentials"] = false;
            }

            self::$awsSdk = new Sdk($config);
        }

        return self::$awsSdk;
    }

    /**
     * @param string $privateKeyFilePath
     * @param string $publicKeyFilePath
     * @return bool
     */
    private static function validateKeyFilesMatch($privateKeyFilePath, $publicKeyFilePath, OutputInterface $output = null) {
        $keysMatch = self::validateKeysMatch(file_get_contents($privateKeyFilePath), file_get_contents($publicKeyFilePath));

        self::writeToOutput($output, "Private key: " . $privateKeyFilePath . " and public key: " . $publicKeyFilePath . " " . ($keysMatch ? "" : "do not ") . "match");

        return $keysMatch;
    }

    /**
     * @param string $privateKey
     * @param string $publicKey
     * @return bool
     */
    private static function validateKeysMatch($privateKey, $publicKey) {
        $length = random_int(1000, 2000);
        $plaintext = "";

        for($i = 0; $i < $length; $i++) {
            if(random_int(0, 5) == 3) {
                $plaintext .= "\n";
            } else {
                $plaintext .= Utils::getRandomPrintableAsciiCharacter();
            }
        }

        $ciphertext = Utils::encryptWithKey($plaintext, $privateKey);

        return ($plaintext === Utils::decryptWithKey($ciphertext, $publicKey));
    }

    /**
     * @param string $msg
     */
    private static function writeToOutput(OutputInterface $output = null, $msg) {
        if($output !== null) {
            $output->writeln($msg);
        }
    }
}
