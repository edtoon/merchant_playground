<?php

namespace BlakePrinting\AmazonAws\CLI;

use Aws\Ec2\Exception\Ec2Exception;
use Aws\Rds\Exception\RdsException;
use BlakePrinting\AmazonAws\AwsConfigUtil;
use BlakePrinting\CLI\CLICommand;
use BlakePrinting\Util\ConfigUtil;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use Exception;
use Monolog\Logger;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LaunchRdsAndWebCommand extends CLICommand {
    /** @var Logger */
    private $logger = null;
    /** @var OutputInterface */
    private $output = null;
    /** @var string */
    private $keyDir = null;
    /** @var string */
    private $publicIp = null;
    /** @var bool */
    private $force = false;
    /** @var string[] */
    private $availabilityZones = [];
    /** @var string */
    private $instance = null;

    protected function configure() {
        $this
            ->setName("aws:rds:launch")
            ->setDescription("Launch the central Amazon RDS instance and a web server")
            ->addOption("key_dir", "d", InputOption::VALUE_REQUIRED, "Path keys are stored in")
            ->addOption("public_ip", "a", InputOption::VALUE_REQUIRED, "Elastic IP public address of web server")
            ->addOption("force", "f", InputOption::VALUE_NONE, "Force change of elastic IP even if one is already assigned")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        try {
            $this->logger =& LogUtil::getLogger();
            $this->output =& $output;
            $this->keyDir = $input->getOption("key_dir");
            $this->publicIp = $input->getOption("public_ip");
            $this->force = $input->getOption("force");

            if(empty($this->keyDir)) {
                $output->writeln("No key directory specified");
            } else if(!is_dir($this->keyDir)) {
                $output->writeln("Key directory does not exist: " . $this->keyDir);
            } else {
                $localValid = AwsConfigUtil::validateLocalKeys($this->keyDir, $output);
                $remoteValid = ($localValid ? AwsConfigUtil::validateRemoteKey($this->keyDir, $output) : false);

                if($remoteValid) {
                    $this->availabilityZones = AwsConfigUtil::getAvailabilityZones();

                    if($this->ensureRdsInstance()) {
                        if($this->ensureEc2Instance()) {
                            if($this->ensureEc2InstanceTags()) {
                                if($this->ensureEc2Ip()) {
                                    $this->authorizePortOnDefaultGroup(22);
                                    $this->authorizePortOnDefaultGroup(80);
                                    $this->authorizePortOnDefaultGroup(443);

                                    return 0;
                                }
                            }
                        }
                    }
                }
            }
        } catch(Exception $e) {
            LogUtil::logException(Logger::ERROR, $e);
        }

        return 1;
    }

    private function authorizePortOnDefaultGroup($port, $protocol = "tcp") {
        try {
            AwsConfigUtil::getEc2Client()->authorizeSecurityGroupIngress([
                "GroupName" => AwsConfigUtil::CONFIG_AWS_GROUP_NAME,
                "CidrIp" => "0.0.0.0/0",
                "FromPort" => $port,
                "ToPort" => $port,
                "IpProtocol" => $protocol
            ]);
        } catch(Ec2Exception $e) {
            if($e->getAwsErrorCode() != "InvalidPermission.Duplicate") {
                throw $e;
            }
        }
    }

    private function ensureEc2Ip() {
        $ec2Client = AwsConfigUtil::getEc2Client();
        $publicKey = file_get_contents(AwsConfigUtil::getPublicKeyPath($this->keyDir));
        $storedElasticIp =& AwsConfigUtil::getStoredElasticIp(ConfigUtil::ENV_PROD, $publicKey);
        $publicIp = $this->publicIp;
        $allocationId = null;

        if($storedElasticIp !== null) {
            $storedPublicIp = $storedElasticIp->getIpAddress();

            $this->output->writeln(
                "Parsed stored elastic IP connection config - " .
                "version: " . $storedElasticIp->getConfigVersion() . ", " .
                "allocationId: " . $storedElasticIp->getAllocationId() . ", " .
                "public IP: " . $storedPublicIp
            );

            if(empty($publicIp)) {
                $publicIp = $storedPublicIp;
            } else if($publicIp != $storedPublicIp && $this->force !== true) {
                throw new Exception("Passed-in IP: " . $this->publicIp . " does not match stored configuration");
            }
        }

        if(!empty($publicIp)) {
            $addresses = $ec2Client->describeAddresses()["Addresses"];

            if(!empty($addresses)) {
                foreach($addresses as &$address) {
                    if($address["PublicIp"] == $publicIp) {
                        $allocationId = $address["AllocationId"];
                    }
                }

                unset($address);
            }
        }

        if(empty($allocationId) || empty($publicIp)) {
            if($storedElasticIp !== null) {
                throw new Exception("No elastic IP addresses are available but configuration is stored for one");
            } else {
                $allocationResult = $ec2Client->allocateAddress(["Domain" => "vpc"]);
                $allocationId =& $allocationResult["AllocationId"];
                $publicIp =& $allocationResult["PublicIp"];
            }
        }

        if(empty($allocationId) || empty($publicIp)) {
            throw new Exception("Could not find an appropriate elastic IP");
        }

        $decryptedEipConfig = "v1\n" . $allocationId . "\n" . $publicIp . "\n";
        $privateKey = file_get_contents(AwsConfigUtil::getPrivateKeyPath($this->keyDir));
        $encryptedEipConfig = Utils::encryptWithKey($decryptedEipConfig, $privateKey);
        $encodedEipConfig = base64_encode($encryptedEipConfig);

        AwsConfigUtil::putObject(AwsConfigUtil::CONFIG_S3_KEY_PROD_EIP, $encodedEipConfig);

        $this->output->writeln("Stored elastic IP configuration - allocationId: " . $allocationId . ", public IP: " . $publicIp);

        $this->output->writeln(Utils::getVarDump($ec2Client->associateAddress([
            "AllocationId" => $allocationId,
            "InstanceId" => $this->instance["InstanceId"]
        ])));

        return true;
    }

    private function ensureEc2InstanceTags() {
        $ec2Client = AwsConfigUtil::getEc2Client();
        $instanceId =& $this->instance["InstanceId"];
        $tags =& $ec2Client->describeTags([
            "Filters" => [
                ["Name" => "resource-id", "Values" => [$instanceId]],
                ["Name" => "resource-type", "Values" => ["instance"]]
            ]
        ])["Tags"];

        if(empty($tags)) {
            $ec2Client->createTags([
                "Resources" => [$instanceId],
                "Tags" => [
                    ["Key" => "Name", "Value" => "ED"],
                    ["Key" => "merchant-env", "Value" => ConfigUtil::ENV_PROD],
                    ["Key" => "merchant-role", "Value" => "web"]
                ]
            ]);
        }

        return true;
    }

    private function ensureEc2Instance() {
        $reservations = AwsConfigUtil::getEc2Client()->describeInstances([
            "Filters" => [
                ["Name" => "architecture", "Values" => ["x86_64"]],
                ["Name" => "instance-state-name", "Values" => ["pending", "running", "stopping", "stopped"]],
                ["Name" => "instance-type", "Values" => ["m4.xlarge"]],
                ["Name" => "instance.group-name", "Values" => [AwsConfigUtil::CONFIG_AWS_GROUP_NAME]],
                ["Name" => "key-name", "Values" => ["oregon"]],
                ["Name" => "root-device-type", "Values" => ["ebs"]],
                ["Name" => "virtualization-type", "Values" => ["hvm"]]
            ]
        ])["Reservations"];

        if(!empty($reservations)) {
            foreach($reservations as &$reservation) {
                $runningInstances =& $reservation["Instances"];

                if(count($runningInstances) == 1) {
                    $this->instance =& $runningInstances[0];
                }
            }

            unset($reservation);
        }

        if(empty($this->instance)) {
            $this->instance =& $this->runInstance();
        }

        return (!empty($this->instance["InstanceId"]));
    }

    private function &runInstance() {
        $latestImage =& $this->findLatestMachineImage();

        if(empty($latestImage)) {
            throw new Exception("Could not find an appropriate machine image to launch");
        }

        $runResult = AwsConfigUtil::getEc2Client()->runInstances([
            "BlockDeviceMappings" => [
                [
                    "DeviceName" => "/dev/sdb",
                    "VirtualName" => "ephemeral0"
                ],
                [
                    "DeviceName" => "/dev/sdc",
                    "VirtualName" => "ephemeral1"
                ],
                [
                    "DeviceName" => "/dev/sdf",
                    "Ebs" => [
                        "DeleteOnTermination" => true,
                        "VolumeSize" => 120,
                        "VolumeType" => "gp2"
                    ]
                ]
            ],
            "ImageId" => $latestImage["ImageId"],
            "InstanceType" => "m4.xlarge",
            "KeyName" => "oregon",
            "MinCount" => 1,
            "MaxCount" => 1,
            "Monitoring" => [
                "Enabled" => false
            ],
            "NetworkInterfaces" => [
                [
                    "AssociatePublicIpAddress" => false,
                    "DeleteOnTermination" => true,
                    "DeviceIndex" => 0,
                ]
            ]
        ]);
        $instances =& $runResult["Instances"];
        $instance =& $instances[0];

        $this->output->writeln("Launched EC2 instance: " . Utils::getVarDump($runResult));

        return $instance;
    }

    private function &findLatestMachineImage() {
        $imagesResult = AwsConfigUtil::getEc2Client()->describeImages([
            "Filters" => [
                ["Name" => "architecture", "Values" => ["x86_64"]],
                ["Name" => "image-type", "Values" => ["machine"]],
                ["Name" => "is-public", "Values" => ["true"]],
                ["Name" => "root-device-type", "Values" => ["ebs"]],
                ["Name" => "virtualization-type", "Values" => ["hvm"]],
                ["Name" => "name", "Values" => ["ubuntu/images-milestone/hvm-ssd/ubuntu-xenial*"]]
            ],
            "Owners" => [AwsConfigUtil::CONFIG_AWS_ACCOUNT_ID_CANONICAL]
        ]);
        $images =& $imagesResult["Images"];
        $latestImage = null;
        $latestTimestamp = null;

        foreach($images as &$image) {
            if($image["State"] == "available") {
                $timestamp = strtotime($image["CreationDate"]);

                if($latestImage === null || $timestamp > $latestTimestamp) {
                    $latestImage = $image;
                    $latestTimestamp = $timestamp;
                }
            }
        }

        unset($image);

        return $latestImage;
    }

    private function ensureRdsInstance() {
        $rdsClient = AwsConfigUtil::getRdsClient();
        $rdsS3Path = AwsConfigUtil::CONFIG_S3_KEY_PROD_RDS;
        $publicKey = file_get_contents(AwsConfigUtil::getPublicKeyPath($this->keyDir));
        $storedRdsConfig = AwsConfigUtil::getStoredRdsConfig(ConfigUtil::ENV_PROD, $publicKey);
        $dbClusterIdentifier = AwsConfigUtil::CONFIG_RDS_CLUSTER_ID;
        $masterUsername = AwsConfigUtil::CONFIG_RDS_USERNAME;
        $dbClustersResult = null;
        $dbClusters = [];
        $dbCluster = null;
        $endpoint = null;
        $port = null;

        try {
            $dbClustersResult = $rdsClient->describeDBClusters([
                "DBClusterIdentifier" => $dbClusterIdentifier
            ]);
            $dbClusters =& $dbClustersResult["DBClusters"];
        } catch(RdsException $e) {
            if($e->getAwsErrorCode() != "DBClusterNotFoundFault") {
                throw $e;
            }
        }

        if(empty($dbClusters)) {
            if($storedRdsConfig !== null) {
                throw new Exception("No RDS cluster exists but a configuration is stored for one");
            } else {
                $masterPassword = self::createRandomPassword();
                $createClusterResult = $rdsClient->createDBCluster([
                    "AvailabilityZones" => $this->availabilityZones,
                    "BackupRetentionPeriod" => 35,
                    "DBClusterIdentifier" => $dbClusterIdentifier,
                    "Engine" => "aurora",
                    "MasterUsername" => $masterUsername,
                    "MasterUserPassword" => $masterPassword
                ]);
                $dbCluster =& $createClusterResult["DBCluster"];

                if(empty($dbCluster)) {
                    throw new Exception("Could not create DB cluster: " . Utils::getVarDump($createClusterResult));
                } else {
                    $this->output->writeln("Created DB cluster: " . Utils::getVarDump($createClusterResult));

                    $endpoint = $dbCluster["Endpoint"];
                    $port = $dbCluster["Port"];
                    $decryptedRdsConfig = "v1\n" . $masterUsername . "\n" . $masterPassword . "\n" . $endpoint . "\n" . $port . "\n";
                    $privateKey = file_get_contents(AwsConfigUtil::getPrivateKeyPath($this->keyDir));
                    $encryptedRdsConfig = Utils::encryptWithKey($decryptedRdsConfig, $privateKey);
                    $encodedRdsConfig = base64_encode($encryptedRdsConfig);

                    AwsConfigUtil::putObject($rdsS3Path, $encodedRdsConfig);

                    $this->output->writeln(
                        "Stored RDS connection config - version: 1, username: " . $masterUsername . ", " .
                        "endpoint: " . $endpoint . ", port: " . $port
                    );

                    sleep(1);
                }
            }
        } else {
            foreach($dbClusters as &$existingDbCluster) {
                if($existingDbCluster["DBClusterIdentifier"] == $dbClusterIdentifier) {
                    $dbCluster = $existingDbCluster;
                }
            }

            unset($existingDbCluster);

            if(empty($dbCluster)) {
                throw new Exception("No DB cluster available: " . Utils::getVarDump($dbClustersResult));
            } else {
                $endpoint =& $dbCluster["Endpoint"];
                $port =& $dbCluster["Port"];

                if($masterUsername != $dbCluster["MasterUsername"]) {
                    throw new Exception("Hard-coded config is for a different username: " . $masterUsername . ", found: " . $dbCluster["MasterUsername"]);
                }

                if($storedRdsConfig === null) {
                    throw new Exception("DB cluster exists but no configuration is stored");
                } else {
                    $storedUsername = $storedRdsConfig->getUsername();
                    $storedEndpoint = $storedRdsConfig->getEndpoint();
                    $storedPort = $storedRdsConfig->getPort();

                    $this->output->writeln(
                        "Parsed stored RDS connection config - " .
                        "version: " . $storedRdsConfig->getConfigVersion() . ", " .
                        "username: " . $storedUsername . ", " .
                        "endpoint: " . $storedEndpoint . ", " .
                        "port: " . $storedPort
                    );

                    if($storedUsername != $masterUsername) {
                        throw new Exception("Stored config is for a different username: " . $storedUsername . ", found: " . $masterUsername);
                    }

                    if($storedEndpoint != $endpoint) {
                        throw new Exception("Stored config is for a different endpoint: " . $storedEndpoint . ", found: " . $endpoint);
                    }

                    if($storedPort != $port) {
                        throw new Exception("Stored config is for a different port: " . $storedPort . ", found: " . $port);
                    }
                }
            }
        }

        if(!empty($dbCluster)) {
            $dbClusterStatus =& $dbCluster["Status"];

            $this->output->writeln("Cluster status is currently: " . $dbClusterStatus);

            if($dbClusterStatus != "available") {
                $this->output->writeln("Retry in a few moments");
            } else {
                $dbClusterMembers =& $dbCluster["DBClusterMembers"];

                if(empty($dbClusterMembers)) {
                    $this->output->writeln("Creating cluster member...");

                    $createMasterResult = $rdsClient->createDBInstance([
                        "DBClusterIdentifier" => $dbClusterIdentifier,
                        "DBInstanceClass" => "db.r3.large",
                        "DBInstanceIdentifier" => AwsConfigUtil::CONFIG_RDS_INSTANCE_ID_PREFIX . "master",
                        "Engine" => "aurora",
                        "MonitoringInterval" => 0,
                        "PubliclyAccessible" => false
                    ]);

                    $this->output->writeln("Created DB master instance: " . Utils::getVarDump($createMasterResult));
                }

                $vpcSecurityGroups =& $dbCluster["VpcSecurityGroups"];

                foreach($vpcSecurityGroups as &$vpcSecurityGroup) {
                    try {
                        AwsConfigUtil::getEc2Client()->authorizeSecurityGroupIngress([
                            "GroupId" => $vpcSecurityGroup["VpcSecurityGroupId"],
                            "SourceSecurityGroupName" => AwsConfigUtil::CONFIG_AWS_GROUP_NAME,
                            "SourceSecurityGroupOwnerId" => AwsConfigUtil::CONFIG_AWS_ACCOUNT_ID
                        ]);
                    } catch(Ec2Exception $e) {
                        if($e->getAwsErrorCode() != "InvalidPermission.Duplicate") {
                            throw $e;
                        }
                    }
                }

                unset($vpcSecurityGroup);

                return true;
            }
        }

        return false;
    }

    private static function createRandomPassword() {
        $passwordLength = random_int(37, 41);
        $pass = "";

        for($i = 0; $i < $passwordLength; $i++) {
            $pass .= Utils::getRandomPrintableAsciiCharacter(['@', '\\', '"', '\'', '/'], 33);
        }

        return $pass;
    }
}
