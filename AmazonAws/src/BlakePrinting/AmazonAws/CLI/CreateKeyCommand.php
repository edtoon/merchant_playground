<?php

namespace BlakePrinting\AmazonAws\CLI;

use BlakePrinting\AmazonAws\AwsConfigUtil;
use BlakePrinting\CLI\CLICommand;
use phpseclib\Crypt\RSA;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CreateKeyCommand extends CLICommand {
    protected function configure() {
        $this
            ->setName("aws:config:create-key")
            ->setDescription("Create a private key to manage servers with (only if none exists unless force is specified)")
            ->addOption("key_dir", "d", InputOption::VALUE_REQUIRED, "Path to store keys in")
            ->addOption("force", "f", InputOption::VALUE_NONE, "Force creation even if a public key already exists")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $keyDirOption = $input->getOption("key_dir");

        if(empty($keyDirOption)) {
            $output->writeln("No key directory specified");
        } else if(!is_dir($keyDirOption)) {
            $output->writeln("Please create the directory first: " . $keyDirOption);
        } else {
            $localValid = AwsConfigUtil::validateLocalKeys($keyDirOption, $output, true);
            $remoteValid = ($localValid ? AwsConfigUtil::validateRemoteKey($keyDirOption, $output, true) : false);
            $force = ($input->getOption("force") === true);

            if(!$remoteValid && !$force) {
                $output->writeln("Use the --force option to overwrite existing keys");
            } else {
                $privateKeyFilePath = AwsConfigUtil::getPrivateKeyPath($keyDirOption);
                $publicKeyFilePath = AwsConfigUtil::getPublicKeyPath($keyDirOption);
                $publicKeyS3Path = AwsConfigUtil::CONFIG_S3_KEY_PROD_PUB_KEY;
                $rsa = new RSA();
                $keypair = $rsa->createKey(AwsConfigUtil::CONFIG_KEY_SIZE);
                $privateKey = $keypair['privatekey'];
                $publicKey = $keypair['publickey'];

                $output->writeln("Saving new public key file to: " . $publicKeyFilePath);
                file_put_contents($publicKeyFilePath, $publicKey);

                $output->writeln("Saving new private key file to: " . $privateKeyFilePath);
                file_put_contents($privateKeyFilePath, $privateKey);

                $output->writeln("Copying public key to " . AwsConfigUtil::getS3Url($publicKeyS3Path));
                AwsConfigUtil::putObject($publicKeyS3Path, $publicKey);

                return 0;
            }
        }

        return 1;
    }
}
