<?php

namespace BlakePrinting\AmazonAws;

class StoredElasticIP {
    /** @var string */
    private $configVersion = null;
    /** @var string */
    private $allocationId = null;
    /** @var string */
    private $ipAddress = null;

    /**
     * @return string
     */
    public function getConfigVersion()
    {
        return $this->configVersion;
    }

    /**
     * @param string $configVersion
     */
    public function setConfigVersion($configVersion)
    {
        $this->configVersion = $configVersion;
    }

    /**
     * @return string
     */
    public function getAllocationId()
    {
        return $this->allocationId;
    }

    /**
     * @param string $allocationId
     */
    public function setAllocationId($allocationId)
    {
        $this->allocationId = $allocationId;
    }

    /**
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * @param string $ipAddress
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;
    }
}
