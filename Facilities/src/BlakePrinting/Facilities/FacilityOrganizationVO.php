<?php

namespace BlakePrinting\Facilities;

use BlakePrinting\Organizations\AbstractEntityOrganizationVO;
use JsonSerializable;

class FacilityOrganizationVO extends AbstractEntityOrganizationVO implements JsonSerializable {
    /** @var int */
    private $facilityId = null;

    public function jsonSerialize() {
        return [
            "facility_id" => $this->facilityId,
            "organization_id" => $this->getOrganizationId()
        ];
    }

    /**
     * @return int
     */
    public function getFacilityId()
    {
        return $this->facilityId;
    }

    /**
     * @param int $facilityId
     */
    public function setFacilityId($facilityId)
    {
        $this->facilityId = $facilityId;
    }
}
