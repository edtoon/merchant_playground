<?php

namespace BlakePrinting\Facilities;

use BlakePrinting\Contacts\AbstractEntityContactAddressVO;
use JsonSerializable;

class FacilityContactAddressVO extends AbstractEntityContactAddressVO implements JsonSerializable {
    /** @var int */
    private $facilityId = null;

    public function jsonSerialize() {
        return [
            "facility_id" => $this->facilityId,
            "contact_address_id" => $this->getContactAddressId()
        ];
    }

    /**
     * @return int
     */
    public function getFacilityId()
    {
        return $this->facilityId;
    }

    /**
     * @param int $facilityId
     */
    public function setFacilityId($facilityId)
    {
        $this->facilityId = $facilityId;
    }
}
