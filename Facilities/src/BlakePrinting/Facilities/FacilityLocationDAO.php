<?php

namespace BlakePrinting\Facilities;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static FacilityLocationDAO instance(PDO $connection = null, $schemaName = null)
 * @method FacilityLocationVO findById($id, $options = null, $useCache = false)
 * @method FacilityLocationVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FacilityLocationVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FacilityLocationVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method FacilityLocationVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FacilityLocationVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FacilityLocationVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class FacilityLocationDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $facilityId
     * @param int $locationId
     * @return int
     */
    public function &store($facilityId, $locationId) {
        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value("facility_id", $facilityId, PDO::PARAM_INT)
            ->value("location_id", $locationId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
