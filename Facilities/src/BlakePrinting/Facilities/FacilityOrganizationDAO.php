<?php

namespace BlakePrinting\Facilities;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static FacilityOrganizationDAO instance(PDO $connection = null, $schemaName = null)
 * @method FacilityOrganizationVO findById($id, $options = null, $useCache = false)
 * @method FacilityOrganizationVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FacilityOrganizationVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FacilityOrganizationVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method FacilityOrganizationVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FacilityOrganizationVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FacilityOrganizationVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class FacilityOrganizationDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $facilityId
     * @param int $organizationId
     * @return int
     */
    public function &store($facilityId, $organizationId) {
        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value("facility_id", $facilityId, PDO::PARAM_INT)
            ->value("organization_id", $organizationId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
