<?php

namespace BlakePrinting\Facilities;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use BlakePrinting\Organizations\OrganizationDAO;
use PDO;

/**
 * @method static FacilityDAO instance(PDO $connection = null, $schemaName = null)
 * @method FacilityVO findById($id, $options = null, $useCache = false)
 * @method FacilityVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FacilityVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FacilityVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method FacilityVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FacilityVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FacilityVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method FacilityVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class FacilityDAO extends AbstractDataAccessObject {
    const FAC_DARTFORD = ["DAR", "Dartford", OrganizationDAO::ORGANIZATION_BLAKE_61];
    const FAC_EASTLAKE = ["ELK", "Eastlake", OrganizationDAO::ORGANIZATION_BLAKE_B_MARKETING];
    const FAC_FRANCE = ["FRA", "France", OrganizationDAO::ORGANIZATION_BLAKE_ROUTIQUE];
    const FAC_HUNTINGTON_BEACH = ["HB", "Huntington Beach", OrganizationDAO::ORGANIZATION_BLAKE_CALI_ELECTRONIC_CONSUMABLES];
    const FAC_JAPAN = ["JPN", "Japan", OrganizationDAO::ORGANIZATION_BLAKE_WWL];
    const FAC_NETHERLANDS = ["NLD", "Netherlands", OrganizationDAO::ORGANIZATION_BLAKE_KRB];
}
