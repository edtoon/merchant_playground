<?php

namespace BlakePrinting\Facilities;

use BlakePrinting\Locations\AbstractEntityLocationVO;
use JsonSerializable;

class FacilityLocationVO extends AbstractEntityLocationVO implements JsonSerializable {
    /** @var int */
    private $facilityId = null;

    public function jsonSerialize() {
        return [
            "facility_id" => $this->facilityId,
            "location_id" => $this->getLocationId()
        ];
    }

    /**
     * @return int
     */
    public function getFacilityId()
    {
        return $this->facilityId;
    }

    /**
     * @param int $facilityId
     */
    public function setFacilityId($facilityId)
    {
        $this->facilityId = $facilityId;
    }
}
