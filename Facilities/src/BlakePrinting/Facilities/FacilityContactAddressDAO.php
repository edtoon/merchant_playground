<?php

namespace BlakePrinting\Facilities;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static FacilityContactAddressDAO instance(PDO $connection = null, $schemaName = null)
 * @method FacilityContactAddressVO findById($id, $options = null, $useCache = false)
 * @method FacilityContactAddressVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FacilityContactAddressVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FacilityContactAddressVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method FacilityContactAddressVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FacilityContactAddressVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FacilityContactAddressVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class FacilityContactAddressDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $facilityId
     * @param int $contactAddressId
     * @return int
     */
    public function &store($facilityId, $contactAddressId) {
        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value("facility_id", $facilityId, PDO::PARAM_INT)
            ->value("contact_address_id", $contactAddressId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
