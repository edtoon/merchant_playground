<?php

namespace BlakePrinting\Facilities;

use BlakePrinting\Db\ValueObject;

abstract class AbstractEntityFacilityVO implements ValueObject {
    /** @var int */
    private $facilityId = null;

    /**
     * @param int $facilityId
     */
    public function __construct($facilityId = null) {
        $this->facilityId = $facilityId;
    }

    /**
     * @return int
     */
    public function getFacilityId()
    {
        return $this->facilityId;
    }

    /**
     * @param int $facilityId
     */
    public function setFacilityId($facilityId)
    {
        $this->facilityId = $facilityId;
    }
}
