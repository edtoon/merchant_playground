<?php

use BlakePrinting\Contacts\ContactAddressDAO;
use BlakePrinting\Db\DbMigration;
use BlakePrinting\Facilities\FacilityContactAddressDAO;
use BlakePrinting\Facilities\FacilityOrganizationDAO;
use BlakePrinting\Facilities\FacilityDAO;
use BlakePrinting\Facilities\FacilityLocationDAO;
use BlakePrinting\Locations\LocationDAO;
use BlakePrinting\Organizations\OrganizationDAO;

class Facilities extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $facilityDao = new FacilityDAO($connection);
        $this->tableDao($facilityDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("code", "string", ["limit" => 191])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("code", ["unique" => true])
            ->addIndex("name")
            ->save()
        ;

        $facilityContactAddressDao = new FacilityContactAddressDAO($connection);
        $contactAddressDao = new ContactAddressDAO($connection);
        $facilityTbl = $facilityDao->formatSchemaAndTableName();
        $contactAddressTbl = $contactAddressDao->formatSchemaAndTableName();
        $this->tableDao($facilityContactAddressDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["facility_id", "contact_address_id"]])
            ->addColumn("facility_id", "integer", ["signed" => false])
            ->addColumn("contact_address_id", "integer", ["signed" => false])
            ->addForeignKey("facility_id", $facilityTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("contact_address_id", $contactAddressTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $facilityOrganizationDao = new FacilityOrganizationDAO($connection);
        $organizationDao = new OrganizationDAO($connection);
        $organizationTbl = $organizationDao->formatSchemaAndTableName();
        $this->tableDao($facilityOrganizationDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["facility_id", "organization_id"]])
            ->addColumn("facility_id", "integer", ["signed" => false])
            ->addColumn("organization_id", "integer", ["signed" => false])
            ->addForeignKey("facility_id", $facilityTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("organization_id", $organizationTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $facilityLocationDao = new FacilityLocationDAO($connection);
        $locationDao = new LocationDAO($connection);
        $locationTbl = $locationDao->formatSchemaAndTableName();
        $this->tableDao($facilityLocationDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["facility_id", "location_id"]])
            ->addColumn("facility_id", "integer", ["signed" => false])
            ->addColumn("location_id", "integer", ["signed" => false])
            ->addForeignKey("facility_id", $facilityTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("location_id", $locationTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(new FacilityLocationDAO($connection));
        $this->dropDao(new FacilityOrganizationDAO($connection));
        $this->dropDao(new FacilityContactAddressDAO($connection));
        $this->dropDao(new FacilityDAO($connection));
    }
}
