<?php

use BlakePrinting\Accounting\ChartAccountDAO;
use BlakePrinting\Accounting\ChartAccountTypeDAO;
use BlakePrinting\Accounting\ChartAccountVO;
use BlakePrinting\Accounting\ChartOfAccountsDAO;
use BlakePrinting\Accounting\ChartOfAccountsVO;
use BlakePrinting\AmazonMws\EndpointDAO;
use BlakePrinting\AmazonMws\Sellers\CredentialDAO;
use BlakePrinting\AmazonMws\Sellers\CredentialVO;
use BlakePrinting\Db\DbSeed;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\EasyPost\EasyPostAccountDAO;
use BlakePrinting\EasyPost\EasyPostAccountVO;
use BlakePrinting\Facilities\FacilityDAO;
use BlakePrinting\Facilities\FacilityLocationDAO;
use BlakePrinting\Facilities\FacilityOrganizationDAO;
use BlakePrinting\Facilities\FacilityVO;
use BlakePrinting\Globalization\CldrUtil;
use BlakePrinting\Globalization\CountryDAO;
use BlakePrinting\Globalization\CountryVO;
use BlakePrinting\Labels\LabelTypeDAO;
use BlakePrinting\Labels\LabelTypeVO;
use BlakePrinting\Locations\LocationDAO;
use BlakePrinting\Locations\LocationOrganizationDAO;
use BlakePrinting\Locations\LocationTypeDAO;
use BlakePrinting\Locations\LocationVO;
use BlakePrinting\Organizations\OrganizationDAO;
use BlakePrinting\Organizations\OrganizationUtil;
use BlakePrinting\Printers\GatewayApiKeyDAO;
use BlakePrinting\Printers\GatewayApiKeyVO;
use BlakePrinting\Printers\GatewayDAO;
use BlakePrinting\Printers\GatewayVO;
use BlakePrinting\Taxes\TaxRateAreaDAO;
use BlakePrinting\Taxes\TaxRateAreaVO;
use BlakePrinting\Taxes\TaxDAO;
use BlakePrinting\Taxes\TaxRateDAO;
use BlakePrinting\Taxes\TaxRateVO;
use BlakePrinting\Taxes\TaxTagDAO;
use BlakePrinting\Taxes\TaxTagVO;
use BlakePrinting\Taxes\TaxTaxTagDAO;
use BlakePrinting\Taxes\TaxTaxTagVO;
use BlakePrinting\Taxes\TaxTypeDAO;
use BlakePrinting\Taxes\TaxVO;
use BlakePrinting\Users\Entities\UserFacilityDAO;
use BlakePrinting\Users\Entities\UserOrganizationDAO;
use BlakePrinting\Users\UserDAO;
use BlakePrinting\Users\UserRoleDAO;
use BlakePrinting\Util\Utils;
use Stringy\StaticStringy as S;

class BlakeSeeder extends DbSeed {
    const TAX_TAG_PREFIX_ODOO_NL = "odoo_nl_";

    private $blakeOrganizationNameToId = [];
    private $eastlakeFacilityId = null;
    private $eastlakeLocationId = null;

    public function run() {
        $this->populateAmazonMwsCredentials();
        $this->populateEasypostAccounts();
        $this->populateOrganizations();
        $this->populateFacilities();
        $this->populateEastlakeLocation();
        $this->populateGateways();
        $this->populateLabelTypes();
        $this->populateUsers();
        $this->populateNetherlandsChartsOfAccounts();
    }

    private function populateAmazonMwsCredentials() {
        $this->addMwsCredential(
            EndpointDAO::ENDPOINT_COM, "##########", "##############################", "###########"
        );
    }

    private function addMwsCredential($endpoint, $awsAccessKeyId, $awsSecretAccessKey, $sellerId) {
        $connection =& $this->getConnection();
        $endpointDao =& EndpointDAO::instance($connection);
        $endpointVo =& $endpointDao->findByColumn("endpoint", $endpoint, PDO::PARAM_STR, null, null, null, true);
        $credentialDao =& CredentialDAO::instance($connection);
        $credentialProtoVo = new CredentialVO();

        $credentialProtoVo->setEndpointId($endpointVo->getId());
        $credentialProtoVo->setAwsAccessKeyId($awsAccessKeyId);
        $credentialProtoVo->setSellerId($sellerId);

        $credentialVo =& $credentialDao->findByPrototype($credentialProtoVo, null, null, "FOR UPDATE");

        if($credentialVo === null) {
            $credentialProtoVo->setAwsSecretAccessKey($awsSecretAccessKey);

            $credentialDao->insert($credentialProtoVo);
        }
    }

    private function populateEasypostAccounts() {
        $connection =& $this->getConnection();
        $easypostAccountDao =& EasyPostAccountDAO::instance($connection);

        $easypostAccountDao->findOrCreate(new EasyPostAccountVO("blake", "EasyPost - Blake Companies"));
    }

    private function populateOrganizations() {
        $connection =& $this->getConnection();
        $organizationDao =& OrganizationDAO::instance($connection);

        Utils::forEachConstantInObjectClass(
            $organizationDao,
            function($_, $constantName, $constantValue) use (&$connection) {
                $this->blakeOrganizationNameToId[$constantValue["name"]] =
                    OrganizationUtil::addOrganizationFromConstant($constantValue, $connection)->getId()
                ;
            }, "ORGANIZATION_BLAKE_"
        );
    }

    private function populateFacilities() {
        $connection =& $this->getConnection();

        $facilityDao =& FacilityDAO::instance($connection);

        Utils::forEachConstantInObjectClass(
            $facilityDao,
            function($_, $constantName, $constantValue) use (&$connection, &$facilityDao) {
                $facilityCode = $constantValue[0];
                $facilityName = $constantValue[1];
                $organizationConstant = $constantValue[2];
                $organizationId = $this->blakeOrganizationNameToId[$organizationConstant["name"]];
                $locationOrganizationDao =& LocationOrganizationDAO::instance($connection);
                $locationTypeDao =& LocationTypeDAO::instance($connection);
                $locationDao =& LocationDAO::instance($connection);
                $facilityOrganizationDao =& FacilityOrganizationDAO::instance($connection);
                $facilityLocationDao =& FacilityLocationDAO::instance($connection);
                $buildingLocationTypeId = $locationTypeDao->findByColumn("name", LocationTypeDAO::LOCATION_TYPE_BUILDING, PDO::PARAM_STR, null, null, null, true)->getId();
                $locationVo =& $locationDao->findOrCreate(new LocationVO("BLAKE_" . $facilityCode, $facilityName, $buildingLocationTypeId));
                $locationId = $locationVo->getId();

                $locationOrganizationDao->store($locationId, $organizationId);

                $facilityVo =& $facilityDao->findOrCreate(new FacilityVO($facilityCode, $facilityName));
                $facilityId = $facilityVo->getId();

                $facilityOrganizationDao->store($facilityId, $organizationId);
                $facilityLocationDao->store($facilityId, $locationId);

                if($facilityCode == FacilityDAO::FAC_EASTLAKE[0]) {
                    $this->eastlakeFacilityId = $facilityId;
                    $this->eastlakeLocationId = $locationId;
                }
            },
            "FAC_"
        );
    }

    private function populateEastlakeLocation() {
        $facilityCode = FacilityDAO::FAC_EASTLAKE[0];
        $connection =& $this->getConnection();

        $locationTypeDao =& LocationTypeDAO::instance($connection);

        $areaLocationTypeId = $locationTypeDao->findByColumn("name", LocationTypeDAO::LOCATION_TYPE_AREA, PDO::PARAM_STR)->getId();
        $roomLocationTypeId = $locationTypeDao->findByColumn("name", LocationTypeDAO::LOCATION_TYPE_ROOM, PDO::PARAM_STR)->getId();
        $officeLocationTypeId = $locationTypeDao->findByColumn("name", LocationTypeDAO::LOCATION_TYPE_OFFICE, PDO::PARAM_STR)->getId();

        $cabinetLocationTypeId = $locationTypeDao->findByColumn("name", LocationTypeDAO::LOCATION_TYPE_CABINET, PDO::PARAM_STR)->getId();
        $deskLocationTypeId = $locationTypeDao->findByColumn("name", LocationTypeDAO::LOCATION_TYPE_DESK, PDO::PARAM_STR)->getId();
        $doorLocationTypeId = $locationTypeDao->findByColumn("name", LocationTypeDAO::LOCATION_TYPE_DOOR, PDO::PARAM_STR)->getId();
        $drawerLocationTypeId = $locationTypeDao->findByColumn("name", LocationTypeDAO::LOCATION_TYPE_DRAWER, PDO::PARAM_STR)->getId();
        $shelfLocationTypeId = $locationTypeDao->findByColumn("name", LocationTypeDAO::LOCATION_TYPE_SHELF, PDO::PARAM_STR)->getId();
        $tableLocationTypeId = $locationTypeDao->findByColumn("name", LocationTypeDAO::LOCATION_TYPE_TABLE, PDO::PARAM_STR)->getId();
        $trayLocationTypeId = $locationTypeDao->findByColumn("name", LocationTypeDAO::LOCATION_TYPE_TRAY, PDO::PARAM_STR)->getId();

        $frontRoomId = $this->storeLocationAndGetId($facilityCode, $roomLocationTypeId, "Front Room", $this->eastlakeLocationId);
        $utilityAreaId = $this->storeLocationAndGetId($facilityCode, $areaLocationTypeId, "Utility Area", $frontRoomId);
        $receptionAreaId = $this->storeLocationAndGetId($facilityCode, $areaLocationTypeId, "Reception Area", $frontRoomId);
        $windowedOfficeId = $this->storeLocationAndGetId($facilityCode, $officeLocationTypeId, "Windowed Office", $this->eastlakeLocationId);
        $windowlessOfficeId = $this->storeLocationAndGetId($facilityCode, $officeLocationTypeId, "Windowless Office", $this->eastlakeLocationId);

        $utilityDeskId = $this->storeLocationAndGetId($facilityCode, $deskLocationTypeId, "Utility Desk", $utilityAreaId);
        $utilityDeskTopLeftDrawerId = $this->storeLocationAndGetId($facilityCode, $drawerLocationTypeId, "Top Left Drawer", $utilityDeskId);
        $utilityDeskBottomLeftDrawerId = $this->storeLocationAndGetId($facilityCode, $drawerLocationTypeId, "Bottom Left Drawer", $utilityDeskId);
        $utilityDeskTopRightDrawerId = $this->storeLocationAndGetId($facilityCode, $drawerLocationTypeId, "Top Right Drawer", $utilityDeskId);
        $utilityDeskBottomRightDrawerId = $this->storeLocationAndGetId($facilityCode, $drawerLocationTypeId, "Bottom Right Drawer", $utilityDeskId);
        $utilityDeskTopMiddleShelfId = $this->storeLocationAndGetId($facilityCode, $shelfLocationTypeId, "Top Middle Shelf", $utilityDeskId);
        $utilityDeskBottomMiddleShelfId = $this->storeLocationAndGetId($facilityCode, $shelfLocationTypeId, "Bottom Middle Shelf", $utilityDeskId);

        $utilityCabinetId = $this->storeLocationAndGetId($facilityCode, $cabinetLocationTypeId, "Utility Cabinet", $utilityAreaId);
        $utilityCabinetTopLeftDrawerId = $this->storeLocationAndGetId($facilityCode, $drawerLocationTypeId, "Top Left Drawer", $utilityCabinetId);
        $utilityCabinetTopRightDrawerId = $this->storeLocationAndGetId($facilityCode, $drawerLocationTypeId, "Top Left Drawer", $utilityCabinetId);
        $utilityCabinetLeftDoorId = $this->storeLocationAndGetId($facilityCode, $doorLocationTypeId, "Left Door", $utilityCabinetId);
        $utilityCabinetLeftDoorTopShelfId = $this->storeLocationAndGetId($facilityCode, $shelfLocationTypeId, "Top Shelf", $utilityCabinetLeftDoorId);
        $utilityCabinetLeftDoorTopMiddleShelfId = $this->storeLocationAndGetId($facilityCode, $shelfLocationTypeId, "Top Middle Shelf", $utilityCabinetLeftDoorId);
        $utilityCabinetLeftDoorBottomMiddleShelfId = $this->storeLocationAndGetId($facilityCode, $shelfLocationTypeId, "Bottom Middle Shelf", $utilityCabinetLeftDoorId);
        $utilityCabinetLeftDoorBottomShelfId = $this->storeLocationAndGetId($facilityCode, $shelfLocationTypeId, "Bottom Shelf", $utilityCabinetLeftDoorId);
        $utilityCabinetRightDoorId = $this->storeLocationAndGetId($facilityCode, $doorLocationTypeId, "Right Door", $utilityCabinetId);
        $utilityCabinetRightDoorTopShelfId = $this->storeLocationAndGetId($facilityCode, $shelfLocationTypeId, "Top Shelf", $utilityCabinetRightDoorId);
        $utilityCabinetRightDoorTopMiddleShelfId = $this->storeLocationAndGetId($facilityCode, $shelfLocationTypeId, "Top Middle Shelf", $utilityCabinetRightDoorId);
        $utilityCabinetRightDoorBottomMiddleShelfId = $this->storeLocationAndGetId($facilityCode, $shelfLocationTypeId, "Bottom Middle Shelf", $utilityCabinetRightDoorId);
        $utilityCabinetRightDoorBottomShelfId = $this->storeLocationAndGetId($facilityCode, $shelfLocationTypeId, "Bottom Shelf", $utilityCabinetRightDoorId);

        $entryTableId = $this->storeLocationAndGetId($facilityCode, $tableLocationTypeId, "Entry Table", $frontRoomId);
        $entryTableBeneathId = $this->storeLocationAndGetId($facilityCode, $areaLocationTypeId, "Beneath", $entryTableId);

        $frontDeskId = $this->storeLocationAndGetId($facilityCode, $deskLocationTypeId, "Front Desk", $receptionAreaId);
        $frontDeskKeyboardId = $this->storeLocationAndGetId($facilityCode, $trayLocationTypeId, "Keyboard Tray", $frontDeskId);
        $frontDeskWorkId = $this->storeLocationAndGetId($facilityCode, $areaLocationTypeId, "Work Side", $frontDeskId);
        $frontDeskMiddleId = $this->storeLocationAndGetId($facilityCode, $areaLocationTypeId, "Middle", $frontDeskId);
        $frontDeskDrawerId = $this->storeLocationAndGetId($facilityCode, $areaLocationTypeId, "Drawer Side", $frontDeskId);
        $frontDeskTopDrawerId = $this->storeLocationAndGetId($facilityCode, $shelfLocationTypeId, "Top Drawer", $frontDeskDrawerId);
        $frontDeskMiddleDrawerId = $this->storeLocationAndGetId($facilityCode, $shelfLocationTypeId, "Middle Drawer", $frontDeskDrawerId);
        $frontDeskBottomDrawerId = $this->storeLocationAndGetId($facilityCode, $shelfLocationTypeId, "Bottom Drawer", $frontDeskDrawerId);

        $windowedOfficeGuestId = $this->storeLocationAndGetId($facilityCode, $areaLocationTypeId, "Guest Area", $windowedOfficeId);
        $windowedOfficeGuestId = $this->storeLocationAndGetId($facilityCode, $areaLocationTypeId, "Storage Area", $windowedOfficeId);
        $windowedOfficeDeskId = $this->storeLocationAndGetId($facilityCode, $deskLocationTypeId, "Windowed Desk", $windowedOfficeId);
        $windowedOfficeDeskKeyboardId = $this->storeLocationAndGetId($facilityCode, $trayLocationTypeId, "Keyboard Tray", $windowedOfficeDeskId);
        $windowedOfficeDeskWorkId = $this->storeLocationAndGetId($facilityCode, $areaLocationTypeId, "Work Side", $windowedOfficeDeskId);
        $windowedOfficeDeskMiddleId = $this->storeLocationAndGetId($facilityCode, $areaLocationTypeId, "Middle", $windowedOfficeDeskId);
        $windowedOfficeDeskDrawerId = $this->storeLocationAndGetId($facilityCode, $areaLocationTypeId, "Drawer Side", $windowedOfficeDeskId);
        $windowedOfficeDeskTopDrawerId = $this->storeLocationAndGetId($facilityCode, $shelfLocationTypeId, "Top Drawer", $windowedOfficeDeskDrawerId);
        $windowedOfficeDeskMiddleDrawerId = $this->storeLocationAndGetId($facilityCode, $shelfLocationTypeId, "Middle Drawer", $windowedOfficeDeskDrawerId);
        $windowedOfficeDeskBottomDrawerId = $this->storeLocationAndGetId($facilityCode, $shelfLocationTypeId, "Bottom Drawer", $windowedOfficeDeskDrawerId);

        $windowlessOfficeGuestId = $this->storeLocationAndGetId($facilityCode, $areaLocationTypeId, "Guest Area", $windowlessOfficeId);
        $windowlessOfficeGuestId = $this->storeLocationAndGetId($facilityCode, $areaLocationTypeId, "Storage Area", $windowlessOfficeId);
        $windowlessOfficeDeskId = $this->storeLocationAndGetId($facilityCode, $deskLocationTypeId, "Windowless Desk", $windowlessOfficeId);
        $windowlessOfficeDeskKeyboardId = $this->storeLocationAndGetId($facilityCode, $trayLocationTypeId, "Keyboard Tray", $windowlessOfficeDeskId);
        $windowlessOfficeDeskWorkId = $this->storeLocationAndGetId($facilityCode, $areaLocationTypeId, "Work Side", $windowlessOfficeDeskId);
        $windowlessOfficeDeskMiddleId = $this->storeLocationAndGetId($facilityCode, $areaLocationTypeId, "Middle", $windowlessOfficeDeskId);
        $windowlessOfficeDeskDrawerId = $this->storeLocationAndGetId($facilityCode, $areaLocationTypeId, "Drawer Side", $windowlessOfficeDeskId);
        $windowlessOfficeDeskTopDrawerId = $this->storeLocationAndGetId($facilityCode, $shelfLocationTypeId, "Top Drawer", $windowlessOfficeDeskDrawerId);
        $windowlessOfficeDeskMiddleDrawerId = $this->storeLocationAndGetId($facilityCode, $shelfLocationTypeId, "Middle Drawer", $windowlessOfficeDeskDrawerId);
        $windowlessOfficeDeskBottomDrawerId = $this->storeLocationAndGetId($facilityCode, $shelfLocationTypeId, "Bottom Drawer", $windowlessOfficeDeskDrawerId);
    }

    private function storeLocationAndGetId($facilityCode, $locationTypeId, $locationName, $parentLocationId) {
        $locationCode = "BLAKE_" . $facilityCode . "_" . $parentLocationId . "_" . S::replace(((string)S::toUpperCase($locationName)), " ", "_");
        $locationDao =& LocationDAO::instance($this->getConnection());
        $locationVo =& $locationDao->findOrCreate(new LocationVO($locationCode, $locationName, $locationTypeId, $parentLocationId));

        return $locationVo->getId();
    }

    private function populateGateways() {
        $connection =& $this->getConnection();
        $facilityDao =& FacilityDAO::instance($connection);
        $facilityVo =& $facilityDao->findOrCreate(new FacilityVO(
            FacilityDAO::FAC_EASTLAKE[0], FacilityDAO::FAC_EASTLAKE[1]
        ));
        $facilityId = $facilityVo->getId();
        $gatewayDao =& GatewayDAO::instance($connection);
        $gatewayVo =& $gatewayDao->findByColumn("facility_id", $facilityId, PDO::PARAM_INT, null, null, "FOR UPDATE");
        $gatewayId = ($gatewayVo === null ? null : $gatewayVo->getId());

        if($gatewayId === null) {
            $gatewayVo = new GatewayVO();

            $gatewayVo->setFacilityId($facilityId);

            $gatewayId =& $gatewayDao->insert($gatewayVo);
        }

        $gatewayApiKeyDao =& GatewayApiKeyDAO::instance($connection);

        $gatewayApiKeyDao->findOrCreate(new GatewayApiKeyVO($gatewayId, "testKey"));
    }

    private function populateLabelTypes() {
        $connection =& $this->getConnection();
        $labelTypeDao =& LabelTypeDAO::instance($connection);
        $reflectionClass =& Utils::getReflectionClassByName(get_class($labelTypeDao));
        $constants = $reflectionClass->getConstants();

        foreach($constants as $constantName => &$constantValue) {
            if(strpos($constantName, "LABEL_TYPE_") === 0) {
                $labelTypeDao->findOrCreate(new LabelTypeVO($constantValue));
            }
        }

        unset($constantValue);
    }

    private function populateUsers() {
        $connection =& $this->getConnection();
        $userDao =& UserDAO::instance($connection);
        $userRoleDao =& UserRoleDAO::instance($connection);
        $userFacilityDao =& UserFacilityDAO::instance($connection);
        $userOrganizationDao =& UserOrganizationDAO::instance($connection);
        $facilityOrganizationDao =& FacilityOrganizationDAO::instance($connection);
        $reflectionClass =& Utils::getReflectionClassByName(OrganizationDAO::class);
        $constants = $reflectionClass->getConstants();

        foreach(["#####" => "################", "######" => "################"] as $username => $password) {
            $userVo =& $userDao->findByColumn("name", $username, PDO::PARAM_STR, null, null, "FOR UPDATE");
            $userId = ($userVo === null ? null : $userVo->getId());

            if($userId === null) {
                $userId = $userDao->create($username, $password);
            }

            $userRoleDao->addRole($userId, UserRoleDAO::ROLE_SUPERADMIN);

            foreach($constants as $constantName => &$constantValue) {
                if(strpos($constantName, "ORGANIZATION_BLAKE_") === 0) {
                    $organizationId =& $this->blakeOrganizationNameToId[$constantValue["name"]];
                    $userOrganizationDao->addOrganization($userId, $organizationId);
                    $organizationFacilities =& $facilityOrganizationDao->findAll(
                        function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$organizationId) {
                            $pdoQuery->where("organization_id", $organizationId, PDO::PARAM_INT);
                        }
                    );

                    if(!empty($organizationFacilities)) {
                        foreach($organizationFacilities as &$facility) {
                            $userFacilityDao->addFacility($userId, $facility->getFacilityId());
                        }

                        unset($facility);
                    }
                }
            }

            unset($constantValue);
        }
    }

    private function populateNetherlandsChartsOfAccounts() {
        $connection =& $this->getConnection();
        $rawJson = Utils::decodeJson(file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . "accounts_netherlands.json"));
        $chartOfAccountsJson = null;

        if($rawJson !== null && !empty($rawJson["chart_of_accounts"])) {
            $chartOfAccountsJson =& $rawJson["chart_of_accounts"];

            if(!empty($chartOfAccountsJson["tags"])) {
                $this->populateTaxTagsFromJson($chartOfAccountsJson["tags"], self::TAX_TAG_PREFIX_ODOO_NL);
            }

            if(!empty($chartOfAccountsJson["name"])) {
                $chartOfAccountsVo =& ChartOfAccountsDAO::instance($connection)->findOrCreate(new ChartOfAccountsVO($chartOfAccountsJson["name"]));

                if(!empty($chartOfAccountsJson["accounts"])) {
                    $this->populateChartAccountsFromJson($chartOfAccountsVo, $chartOfAccountsJson["accounts"]);
                }
            }

            if(!empty($chartOfAccountsJson["taxes"])) {
                $this->populateTaxCategoriesFromJson($chartOfAccountsJson["taxes"], self::TAX_TAG_PREFIX_ODOO_NL);
            }
        }
    }

    private function populateTaxTagsFromJson($tagsJson, $tagPrefix = null) {
        $taxTagDao =& TaxTagDAO::instance($this->getConnection());

        foreach($tagsJson as &$tagJson) {
            $tagName = (($tagPrefix ?: "") . $tagJson["tag"]);
            $tagDescription = $tagJson["english"];

            $taxTagDao->findOrCreate(new TaxTagVO($tagName, $tagDescription));
        }

        unset($tagJson);
    }

    private function populateChartAccountsFromJson(ChartOfAccountsVO $chartOfAccountsVo, $accountsJson) {
        $connection =& $this->getConnection();
        $chartAccountTypeDao =& ChartAccountTypeDAO::instance($connection);
        $chartAccountDao =& ChartAccountDAO::instance($connection);

        foreach($accountsJson as &$accountJson) {
            $chartAccountTypeId = null;

            Utils::forEachConstantInObjectClass(
                $chartAccountTypeDao,
                function($_, $constantName, $constantValue) use (&$chartAccountTypeId, &$chartAccountTypeDao, &$chartAccountTypeVo, &$accountJson) {
                    if($chartAccountTypeId === null && $constantValue[0] == $accountJson["type"]) {
                        $chartAccountTypeVo =& $chartAccountTypeDao->findByColumn("name", $constantValue[0], PDO::PARAM_STR, null, null, null, true);
                        $chartAccountTypeId = $chartAccountTypeVo->getId();
                    }
                },
                "CHART_ACCOUNT_TYPE_"
            );

            if($chartAccountTypeId === null) {
                throw new Exception("Couldn't find chart account type named: " . $accountJson["type"]);
            }

            $chartAccountVo =& $chartAccountDao->findSingle(
                function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$chartOfAccountsVo, &$accountJson) {
                    $pdoQuery
                        ->where("chart_of_accounts_id", $chartOfAccountsVo->getId(), PDO::PARAM_INT)
                        ->where("code", $accountJson["code"], PDO::PARAM_STR)
                    ;
                }, null, null, "FOR UPDATE"
            );

            if($chartAccountVo === null) {
                $chartAccountVo = new ChartAccountVO();

                $chartAccountVo->setChartOfAccountsId($chartOfAccountsVo->getId());
                $chartAccountVo->setChartAccountTypeId($chartAccountTypeId);
                $chartAccountVo->setCode($accountJson["code"]);
                $chartAccountVo->setName($accountJson["english"]);

                $chartAccountDao->insert($chartAccountVo);
            }
        }

        unset($accountJson);
    }

    private function populateTaxCategoriesFromJson($taxesJson, $tagPrefix = null) {
        $connection =& $this->getConnection();
        $countryDao =& CountryDAO::instance($connection);
        $taxTypeDao =& TaxTypeDAO::instance($connection);

        foreach($taxesJson as &$taxZoneJson) {
            if(!empty($taxZoneJson["categories"])) {
                $zoneName = $taxZoneJson["zone"];
                $taxCountries = [];

                if($zoneName === "NL") {
                    $taxCountries[] = $countryDao->findByColumn("iso_code", $zoneName, PDO::PARAM_STR, null, null, null, true);
                } else if($zoneName === "EU") {
                    $taxCountries = CldrUtil::getEuCountries($connection);
                }

                foreach($taxZoneJson["categories"] as &$taxCategory) {
                    $taxTypeName = $taxCategory["english"];
                    $taxTypeId = null;

                    Utils::forEachConstantInObjectClass(
                        $taxTypeDao,
                        function($_, $constantName, $constantValue) use (&$taxTypeId, &$taxTypeName, &$taxTypeDao) {
                            if($taxTypeId === null && $constantValue === $taxTypeName) {
                                $taxTypeVo =& $taxTypeDao->findByColumn("name", $constantValue, PDO::PARAM_STR, null, null, null, true);
                                $taxTypeId = $taxTypeVo->getId();
                            }
                        },
                        "TAX_TYPE_"
                    );

                    if($taxTypeId === null) {
                        throw new Exception("Couldn't find tax type named: " . $taxTypeName);
                    }

                    if(!empty($taxCategory["taxes"])) {
                        $this->populateTaxesFromJson($taxCountries, $taxTypeId, $taxCategory["taxes"], $tagPrefix);
                    }
                }

                unset($taxCategory);
            }
        }

        unset($taxZoneJson);
    }

    private function populateTaxesFromJson(array $taxCountries, $taxTypeId, $taxesJson, $tagPrefix = null, TaxVO $parentTaxVo = null) {
        $connection =& $this->getConnection();
        $taxDao =& TaxDAO::instance($connection);
        $taxRateDao =& TaxRateDAO::instance($connection);
        $taxRateAreaDao =& TaxRateAreaDAO::instance($connection);

        foreach($taxesJson as &$tax) {
            $taxCode = (string)S::toUpperCase($tax["id"]);
            $taxVo =& $taxDao->findByColumn("code", $taxCode, PDO::PARAM_STR, null, null, "FOR UPDATE");
            $taxId = ($taxVo === null ? null : $taxVo->getId());

            if($taxVo === null) {
                $taxVo = new TaxVO();

                if($parentTaxVo !== null) {
                    $taxVo->setParentTaxId($parentTaxVo->getId());
                }

                $taxVo->setTaxTypeId($taxTypeId);
                $taxVo->setCode($taxCode);
                $taxVo->setName($tax["name"]["english"]);
                $taxVo->setDescription($tax["desc"]["english"]);

                $taxId =& $taxDao->insert($taxVo);
                $taxVo =& $taxDao->findById($taxId);
            }

            $taxRateVo = new TaxRateVO();

            $taxRateVo->setTaxId($taxVo->getId());

            if(isset($tax["amount"])) {
                $taxRateVo->setPercentage((string)S::replace($tax["amount"], "%", ""));
            }

            if(isset($tax["variable"])) {
                $taxRateVo->setVariable($tax["variable"]);
            }

            if(isset($tax["included"])) {
                $taxRateVo->setIncludeInPrice($tax["included"]);
            }

            $taxRateVo =& $taxRateDao->findOrCreate($taxRateVo);

            if(!empty($taxCountries)) {
                foreach($taxCountries as &$countryVo) {
                    if($countryVo instanceof CountryVO) {
                        $taxRateAreaVo = new TaxRateAreaVO();

                        $taxRateAreaVo->setTaxRateId($taxRateVo->getId());
                        $taxRateAreaVo->setCountryId($countryVo->getId());

                        $taxRateAreaDao->findOrCreate($taxRateAreaVo, "FOR UPDATE", ["start", "end", "city", "district", "state_or_region", "postal_code"]);
                    }
                }

                unset($countryVo);
            }

            if(!empty($tax["tags"])) {
                $this->populateTaxTaxTagsFromJson($taxVo, $tax["tags"], $tagPrefix);
            }

            if(!empty($tax["taxes"])) {
                $this->populateTaxesFromJson($taxCountries, $taxTypeId, $tax["taxes"], $tagPrefix, $taxVo);
            }
        }

        unset($tax);
    }

    private function populateTaxTaxTagsFromJson(TaxVO $taxVo, $tagsJson, $tagPrefix = null) {
        $connection =& $this->getConnection();
        $taxTagDao =& TaxTagDAO::instance($connection);
        $taxTaxTagDao =& TaxTaxTagDAO::instance($connection);

        foreach($tagsJson as $rawTagName) {
            $tagName = (($tagPrefix ?: "") . $rawTagName);
            $taxTagVo =& $taxTagDao->findByColumn("name", $tagName, PDO::PARAM_STR, null, null, null, true);

            $taxTaxTagDao->insert(new TaxTaxTagVO($taxVo->getId(), $taxTagVo->getId()), null, true);
        }
    }
}
