<?php

use BlakePrinting\Accounting\ChartAccountTypeDAO;
use BlakePrinting\Accounting\ChartAccountTypeVO;
use BlakePrinting\Accounting\FiscalPeriodTypeDAO;
use BlakePrinting\Accounting\FiscalPeriodTypeVO;
use BlakePrinting\Db\DbSeed;
use BlakePrinting\Util\Utils;

class AccountingSeeder extends DbSeed {
    public final function run() {
        $connection =& $this->getConnection();
        $fiscalPeriodTypeDao =& FiscalPeriodTypeDAO::instance($connection);

        Utils::forEachConstantInObjectClass(
            $fiscalPeriodTypeDao,
            function($_, $constantName, $constantValue) use (&$fiscalPeriodTypeDao) {
                $fiscalPeriodTypeDao->findOrCreate(new FiscalPeriodTypeVO($constantValue));
            },
            "FISCAL_PERIOD_TYPE_"
        );

        $chartAccountTypeDao =& ChartAccountTypeDAO::instance($connection);

        Utils::forEachConstantInObjectClass(
            $chartAccountTypeDao,
            function($_, $constantName, $constantValue) use (&$chartAccountTypeDao) {
                $chartAccountTypeDao->findOrCreate(new ChartAccountTypeVO($constantValue[0], $constantValue[1], $constantValue[2]));
            },
            "CHART_ACCOUNT_TYPE_"
        );
    }
}
