<?php

namespace BlakePrinting\Accounting;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static GeneralLedgerDAO instance(PDO $connection = null, $schemaName = null)
 * @method GeneralLedgerVO findById($id, $options = null, $useCache = false)
 * @method GeneralLedgerVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method GeneralLedgerVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method GeneralLedgerVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method GeneralLedgerVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method GeneralLedgerVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method GeneralLedgerVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class GeneralLedgerDAO extends AbstractDataAccessObject {
}
