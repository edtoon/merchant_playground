<?php

namespace BlakePrinting\Accounting;

use BlakePrinting\Db\ValueObject;

class GeneralLedgerAccountVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $creator = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $updater = null;
    /** @var int */
    private $generalLedgerId = null;
    /** @var int */
    private $chartAccountId = null;
    /** @var int */
    private $currencyId = null;
    /** @var int */
    private $start = null;
    /** @var float */
    private $openingBalance = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param int $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getUpdater()
    {
        return $this->updater;
    }

    /**
     * @param int $updater
     */
    public function setUpdater($updater)
    {
        $this->updater = $updater;
    }

    /**
     * @return int
     */
    public function getGeneralLedgerId()
    {
        return $this->generalLedgerId;
    }

    /**
     * @param int $generalLedgerId
     */
    public function setGeneralLedgerId($generalLedgerId)
    {
        $this->generalLedgerId = $generalLedgerId;
    }

    /**
     * @return int
     */
    public function getChartAccountId()
    {
        return $this->chartAccountId;
    }

    /**
     * @param int $chartAccountId
     */
    public function setChartAccountId($chartAccountId)
    {
        $this->chartAccountId = $chartAccountId;
    }

    /**
     * @return int
     */
    public function getCurrencyId()
    {
        return $this->currencyId;
    }

    /**
     * @param int $currencyId
     */
    public function setCurrencyId($currencyId)
    {
        $this->currencyId = $currencyId;
    }

    /**
     * @return int
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param int $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return float
     */
    public function getOpeningBalance()
    {
        return $this->openingBalance;
    }

    /**
     * @param float $openingBalance
     */
    public function setOpeningBalance($openingBalance)
    {
        $this->openingBalance = $openingBalance;
    }
}
