<?php

namespace BlakePrinting\Accounting;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static GeneralLedgerAccountDAO instance(PDO $connection = null, $schemaName = null)
 * @method GeneralLedgerAccountVO findById($id, $options = null, $useCache = false)
 * @method GeneralLedgerAccountVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method GeneralLedgerAccountVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method GeneralLedgerAccountVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method GeneralLedgerAccountVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method GeneralLedgerAccountVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method GeneralLedgerAccountVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class GeneralLedgerAccountDAO extends AbstractDataAccessObject {
}
