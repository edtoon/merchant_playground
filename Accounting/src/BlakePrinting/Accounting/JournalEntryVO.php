<?php

namespace BlakePrinting\Accounting;

use BlakePrinting\Db\ValueObject;

class JournalEntryVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $creator = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $updater = null;
    /** @var int */
    private $generalLedgerId = null;
    /** @var int */
    private $occurred = null;
    /** @var string */
    private $memo = null;
    /** @var bool */
    private $recurring = null;
    /** @var bool */
    private $reversing = null;
    /** @var int */
    private $reverseJournalEntryId = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param int $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getUpdater()
    {
        return $this->updater;
    }

    /**
     * @param int $updater
     */
    public function setUpdater($updater)
    {
        $this->updater = $updater;
    }

    /**
     * @return int
     */
    public function getGeneralLedgerId()
    {
        return $this->generalLedgerId;
    }

    /**
     * @param int $generalLedgerId
     */
    public function setGeneralLedgerId($generalLedgerId)
    {
        $this->generalLedgerId = $generalLedgerId;
    }

    /**
     * @return int
     */
    public function getOccurred()
    {
        return $this->occurred;
    }

    /**
     * @param int $occurred
     */
    public function setOccurred($occurred)
    {
        $this->occurred = $occurred;
    }

    /**
     * @return string
     */
    public function getMemo()
    {
        return $this->memo;
    }

    /**
     * @param string $memo
     */
    public function setMemo($memo)
    {
        $this->memo = $memo;
    }

    /**
     * @return boolean
     */
    public function isRecurring()
    {
        return $this->recurring;
    }

    /**
     * @param boolean $recurring
     */
    public function setRecurring($recurring)
    {
        $this->recurring = $recurring;
    }

    /**
     * @return boolean
     */
    public function isReversing()
    {
        return $this->reversing;
    }

    /**
     * @param boolean $reversing
     */
    public function setReversing($reversing)
    {
        $this->reversing = $reversing;
    }

    /**
     * @return int
     */
    public function getReverseJournalEntryId()
    {
        return $this->reverseJournalEntryId;
    }

    /**
     * @param int $reverseJournalEntryId
     */
    public function setReverseJournalEntryId($reverseJournalEntryId)
    {
        $this->reverseJournalEntryId = $reverseJournalEntryId;
    }
}
