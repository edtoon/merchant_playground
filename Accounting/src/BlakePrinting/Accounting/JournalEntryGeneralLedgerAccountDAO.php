<?php

namespace BlakePrinting\Accounting;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static JournalEntryGeneralLedgerAccountDAO instance(PDO $connection = null, $schemaName = null)
 * @method JournalEntryGeneralLedgerAccountVO findById($id, $options = null, $useCache = false)
 * @method JournalEntryGeneralLedgerAccountVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method JournalEntryGeneralLedgerAccountVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method JournalEntryGeneralLedgerAccountVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method JournalEntryGeneralLedgerAccountVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method JournalEntryGeneralLedgerAccountVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method JournalEntryGeneralLedgerAccountVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class JournalEntryGeneralLedgerAccountDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
