<?php

namespace BlakePrinting\Accounting;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static SetOfBooksDAO instance(PDO $connection = null, $schemaName = null)
 * @method SetOfBooksVO findById($id, $options = null, $useCache = false)
 * @method SetOfBooksVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SetOfBooksVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SetOfBooksVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method SetOfBooksVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method SetOfBooksVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method SetOfBooksVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class SetOfBooksDAO extends AbstractDataAccessObject {
}
