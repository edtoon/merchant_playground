<?php

namespace BlakePrinting\Accounting;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static FiscalPeriodDAO instance(PDO $connection = null, $schemaName = null)
 * @method FiscalPeriodVO findById($id, $options = null, $useCache = false)
 * @method FiscalPeriodVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FiscalPeriodVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FiscalPeriodVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method FiscalPeriodVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FiscalPeriodVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FiscalPeriodVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class FiscalPeriodDAO extends AbstractDataAccessObject {
}
