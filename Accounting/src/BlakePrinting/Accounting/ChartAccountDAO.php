<?php

namespace BlakePrinting\Accounting;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ChartAccountDAO instance(PDO $connection = null, $schemaName = null)
 * @method ChartAccountVO findById($id, $options = null, $useCache = false)
 * @method ChartAccountVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ChartAccountVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ChartAccountVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ChartAccountVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ChartAccountVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ChartAccountVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ChartAccountVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class ChartAccountDAO extends AbstractDataAccessObject {
    /**
     * @param int $id
     * @return int
     */
    public function &delete($id) {
        return PDOQuery::delete()
            ->from($this)
            ->where("id", $id, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
