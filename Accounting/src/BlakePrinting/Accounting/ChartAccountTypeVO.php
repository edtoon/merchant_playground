<?php

namespace BlakePrinting\Accounting;

use BlakePrinting\Db\ValueObject;
use JsonSerializable;

class ChartAccountTypeVO implements JsonSerializable, ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var string */
    private $name = null;
    /** @var int */
    private $debitDirection = null;
    /** @var int */
    private $creditDirection = null;

    public function __construct($name = null, $debitDirection = null, $creditDirection = null) {
        $this->name = $name;
        $this->debitDirection = $debitDirection;
        $this->creditDirection = $creditDirection;
    }

    public function jsonSerialize() {
        return [
            "id" => $this->id,
            "created" => $this->created,
            "updated" => $this->updated,
            "name" => $this->name,
            "debit_direction" => $this->debitDirection,
            "credit_direction" => $this->creditDirection
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getDebitDirection()
    {
        return $this->debitDirection;
    }

    /**
     * @param int $debitDirection
     */
    public function setDebitDirection($debitDirection)
    {
        $this->debitDirection = $debitDirection;
    }

    /**
     * @return int
     */
    public function getCreditDirection()
    {
        return $this->creditDirection;
    }

    /**
     * @param int $creditDirection
     */
    public function setCreditDirection($creditDirection)
    {
        $this->creditDirection = $creditDirection;
    }
}
