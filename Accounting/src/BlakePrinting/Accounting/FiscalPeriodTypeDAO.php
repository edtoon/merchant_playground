<?php

namespace BlakePrinting\Accounting;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static FiscalPeriodTypeDAO instance(PDO $connection = null, $schemaName = null)
 * @method FiscalPeriodTypeVO findById($id, $options = null, $useCache = false)
 * @method FiscalPeriodTypeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FiscalPeriodTypeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FiscalPeriodTypeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method FiscalPeriodTypeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method FiscalPeriodTypeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method FiscalPeriodTypeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class FiscalPeriodTypeDAO extends AbstractDataAccessObject {
    const FISCAL_PERIOD_TYPE_MONTH = "month";
    const FISCAL_PERIOD_TYPE_QUARTER = "quarter";
    const FISCAL_PERIOD_TYPE_YEAR = "year";
}
