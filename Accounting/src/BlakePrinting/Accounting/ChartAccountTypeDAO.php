<?php

namespace BlakePrinting\Accounting;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ChartAccountTypeDAO instance(PDO $connection = null, $schemaName = null)
 * @method ChartAccountTypeVO findById($id, $options = null, $useCache = false)
 * @method ChartAccountTypeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ChartAccountTypeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ChartAccountTypeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ChartAccountTypeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ChartAccountTypeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ChartAccountTypeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ChartAccountTypeDAO extends AbstractDataAccessObject {
    const CHART_ACCOUNT_TYPE_ASSET = ["asset", 1, -1];
    const CHART_ACCOUNT_TYPE_EXPENSE = ["expense", 1, -1];
    const CHART_ACCOUNT_TYPE_DIVIDEND = ["dividend", 1, -1]; // aka drawing account
    const CHART_ACCOUNT_TYPE_LIABILITY = ["liability", -1, 1];
    const CHART_ACCOUNT_TYPE_REVENUE = ["revenue", -1, 1]; // aka income account
    const CHART_ACCOUNT_TYPE_EQUITY = ["equity", -1, 1]; // aka capital account
}
