<?php

namespace BlakePrinting\Accounting;

use BlakePrinting\Db\ValueObject;

class JournalEntryGeneralLedgerAccountVO implements ValueObject {
    /** @var int */
    private $journalEntryId = null;
    /** @var int */
    private $generalLedgerAccountId = null;
    /** @var string */
    private $amount = null;

    /**
     * @return int
     */
    public function getJournalEntryId()
    {
        return $this->journalEntryId;
    }

    /**
     * @param int $journalEntryId
     */
    public function setJournalEntryId($journalEntryId)
    {
        $this->journalEntryId = $journalEntryId;
    }

    /**
     * @return int
     */
    public function getGeneralLedgerAccountId()
    {
        return $this->generalLedgerAccountId;
    }

    /**
     * @param int $generalLedgerAccountId
     */
    public function setGeneralLedgerAccountId($generalLedgerAccountId)
    {
        $this->generalLedgerAccountId = $generalLedgerAccountId;
    }

    /**
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }
}
