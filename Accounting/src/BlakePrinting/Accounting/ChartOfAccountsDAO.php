<?php

namespace BlakePrinting\Accounting;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ChartOfAccountsDAO instance(PDO $connection = null, $schemaName = null)
 * @method ChartOfAccountsVO findById($id, $options = null, $useCache = false)
 * @method ChartOfAccountsVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ChartOfAccountsVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ChartOfAccountsVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ChartOfAccountsVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ChartOfAccountsVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ChartOfAccountsVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ChartOfAccountsVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class ChartOfAccountsDAO extends AbstractDataAccessObject {
}
