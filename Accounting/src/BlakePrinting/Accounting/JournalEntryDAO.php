<?php

namespace BlakePrinting\Accounting;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static JournalEntryDAO instance(PDO $connection = null, $schemaName = null)
 * @method JournalEntryVO findById($id, $options = null, $useCache = false)
 * @method JournalEntryVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method JournalEntryVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method JournalEntryVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method JournalEntryVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method JournalEntryVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method JournalEntryVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class JournalEntryDAO extends AbstractDataAccessObject {
}
