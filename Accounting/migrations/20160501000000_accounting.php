<?php

use BlakePrinting\Accounting\ChartAccountDAO;
use BlakePrinting\Accounting\ChartAccountTypeDAO;
use BlakePrinting\Accounting\ChartOfAccountsDAO;
use BlakePrinting\Accounting\GeneralLedgerDAO;
use BlakePrinting\Accounting\JournalEntryGeneralLedgerAccountDAO;
use BlakePrinting\Accounting\GeneralLedgerAccountDAO;
use BlakePrinting\Accounting\SetOfBooksDAO;
use BlakePrinting\Accounting\JournalEntryDAO;
use BlakePrinting\Accounting\FiscalPeriodDAO;
use BlakePrinting\Accounting\FiscalPeriodTypeDAO;
use BlakePrinting\Db\DbMigration;
use BlakePrinting\Globalization\CurrencyDAO;
use BlakePrinting\Organizations\OrganizationDAO;
use BlakePrinting\Users\UserDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class Accounting extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $chartOfAccountsDao =& ChartOfAccountsDAO::instance($connection);
        $userDao =& UserDao::instance($connection);
        $userTbl = $userDao->formatSchemaAndTableName();
        $this->tableDao($chartOfAccountsDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("name", "string", ["limit" => 191])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $chartAccountTypeDao =& ChartAccountTypeDAO::instance($connection);
        $this->tableDao($chartAccountTypeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("name", "string", ["limit" => 191])
            ->addColumn("debit_direction", "integer", ["limit" => MysqlAdapter::INT_TINY, "precision" => 1])
            ->addColumn("credit_direction", "integer", ["limit" => MysqlAdapter::INT_TINY, "precision" => 1])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->addIndex("debit_direction")
            ->addIndex("credit_direction")
            ->save()
        ;

        $chartAccountDao =& ChartAccountDAO::instance($connection);
        $chartOfAccountsTbl = $chartOfAccountsDao->formatSchemaAndTableName();
        $this->tableDao($chartAccountDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("chart_of_accounts_id", "integer", ["signed" => false])
            ->addColumn("chart_account_type_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("code", "string", ["limit" => 191])
            ->addColumn("name", "string", ["limit" => 191])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("chart_of_accounts_id", $chartOfAccountsTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("chart_account_type_id", $chartAccountTypeDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex(["chart_of_accounts_id", "code"], ["unique" => true])
            ->addIndex("code")
            ->addIndex("name")
            ->save()
        ;

        $fiscalPeriodTypeDao =& FiscalPeriodTypeDAO::instance($connection);
        $this->tableDao($fiscalPeriodTypeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $fiscalPeriodDao =& FiscalPeriodDAO::instance($connection);
        $this->tableDao($fiscalPeriodDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("fiscal_period_type_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("name", "string", ["limit" => 191])
            ->addColumn("start", "biginteger", ["signed" => false])
            ->addColumn("end", "biginteger", ["signed" => false])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("fiscal_period_type_id", $fiscalPeriodTypeDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->addIndex("start")
            ->addIndex("end")
            ->save()
        ;

        $setOfBooksDao =& SetOfBooksDAO::instance($connection);
        $this->tableDao($setOfBooksDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("name", "string", ["limit" => 191])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $generalLedgerDao =& GeneralLedgerDAO::instance($connection);
        $organizationDao =& OrganizationDAO::instance($connection);
        $currencyDao =& CurrencyDAO::instance($connection);
        $currencyTbl = $currencyDao->formatSchemaAndTableName();
        $this->tableDao($generalLedgerDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("set_of_books_id", "integer", ["signed" => false])
            ->addColumn("organization_id", "integer", ["signed" => false])
            ->addColumn("chart_of_accounts_id", "integer", ["signed" => false])
            ->addColumn("name", "string", ["limit" => 191])
            ->addColumn("currency_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("set_of_books_id", $setOfBooksDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("organization_id", $organizationDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("chart_of_accounts_id", $chartOfAccountsTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("currency_id", $currencyTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex(["set_of_books_id", "name"], ["unique" => true])
            ->addIndex("name")
            ->save()
        ;

        $generalLedgerAccountDao =& GeneralLedgerAccountDAO::instance($connection);
        $generalLedgerTbl = $generalLedgerDao->formatSchemaAndTableName();
        $this->tableDao($generalLedgerAccountDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("general_ledger_id", "integer", ["signed" => false])
            ->addColumn("chart_account_id", "integer", ["signed" => false])
            ->addColumn("currency_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("start", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("opening_balance", "decimal", ["null" => true, "precision" => 19, "scale" => 4])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("general_ledger_id", $generalLedgerTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("chart_account_id", $chartAccountDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("currency_id", $currencyTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex(["general_ledger_id", "chart_account_id"], ["unique" => true])
            ->addIndex("start")
            ->addIndex("opening_balance")
            ->save()
        ;

        $journalEntryDao =& JournalEntryDAO::instance($connection);
        $journalEntryTbl = $journalEntryDao->formatSchemaAndTableName();
        $this->tableDao($journalEntryDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("general_ledger_id", "integer", ["signed" => false])
            ->addColumn("occurred", "biginteger", ["signed" => false])
            ->addColumn("memo", "text", ["null" => true])
            ->addColumn("recurring", "integer", ["null" => true, "limit" => MysqlAdapter::INT_TINY, "precision" => 1])
            ->addColumn("reversing", "integer", ["null" => true, "limit" => MysqlAdapter::INT_TINY, "precision" => 1])
            ->addColumn("reverse_journal_entry_id", "biginteger", ["null" => true, "signed" => false])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("general_ledger_id", $generalLedgerTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("reverse_journal_entry_id", $journalEntryTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("occurred")
            ->addIndex("recurring")
            ->addIndex("reversing")
            ->save()
        ;
        $this->alterDao($journalEntryDao, "ADD INDEX (memo(191))");

        $journalEntryGeneralLedgerAccountDao =& JournalEntryGeneralLedgerAccountDAO::instance($connection);
        $this->tableDao($journalEntryGeneralLedgerAccountDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["journal_entry_id", "general_ledger_account_id"]])
            ->addColumn("journal_entry_id", "biginteger", ["signed" => false])
            ->addColumn("general_ledger_account_id", "integer", ["signed" => false])
            ->addColumn("amount", "decimal", ["precision" => 19, "scale" => 4])
            ->addForeignKey("journal_entry_id", $journalEntryTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("general_ledger_account_id", $generalLedgerAccountDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("amount")
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(JournalEntryGeneralLedgerAccountDAO::instance($connection));
        $this->dropDao(JournalEntryDAO::instance($connection));
        $this->dropDao(GeneralLedgerAccountDAO::instance($connection));
        $this->dropDao(SetOfBooksDAO::instance($connection));
        $this->dropDao(FiscalPeriodDAO::instance($connection));
        $this->dropDao(FiscalPeriodTypeDAO::instance($connection));
        $this->dropDao(ChartAccountDAO::instance($connection));
        $this->dropDao(ChartAccountTypeDAO::instance($connection));
        $this->dropDao(ChartOfAccountsDAO::instance($connection));
    }
}
