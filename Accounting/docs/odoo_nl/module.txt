# - * - Encoding: utf-8 - * -
# Part of ODOO. See LICENSE file for full copyright and licensing details.

# Copyright (c) 2009 Veritos - Jan Verlaan - www.veritos.nl

#
# This module works in OpenERP 5.0.0 (and probably higher).
# This module does not work in OpenERP version 4 and lower.
#
# Status 1.0 - tested OpenERP 5.0.3
#
# Version 5.0.0.1
# account.account.type
# Foundation laid for all account types
#
# account.account.template
# Basic submitted with all the required general ledger accounts which via a menu
# Structure are linked to items 1/9.
# The ledger accounts linked to the account.account.type
# These links should be another good checked.
#
# account.chart.template
# Foundation laid for linking accounts receivable, accounts payable,
# Bank books procurement and sales and the VAT configuration.
#
# Version 5.0.0.2
# account.tax.code.template
# Foundation laid for the VAT configuration (structure)
# Have basis using the tax return form. Whether this works?
#
# account.tax.template
# The VAT accounts created and linked it to the relevant
# Ledger accounts
#
# Version 5.0.0.3
# Clean up the code and disposal of used components.
# Version 5.0.0.4
# Adjust a_expense 3000 -> 7000
# Record id = 'btw_code_5b' put on negative
# Version 5.0.0.5
# Tax bills have received type designation receivers for purchase or sale
# Version 5.0.0.6
# Cleanup module.
# Version 5.0.0.7
# Cleanup module.
# Version 5.0.0.8
Mistake # l10n_nl_wizard.xml corrected in which the module is not installed completely.
# Version 5.0.0.9
# Account Receivable and Payable defined.
# Version 5.0.1.0
# All user_type_xxx fields defined.
# Specific construction and car-related ledgers removed to create a standard module.
# This module can then be used as the basis for specific target group to create modules.
# Version 5.0.1.1
# Correction of account 7010 (which was double in 7014 installation went wrong)
# Version 5.0.1.2
# Correction in various account types user_type_asset -> user_type_liability and user_type_equity
# Version 5.0.1.3
# Receivable Small correction to high VAT, ID was the same for both, which was highly regarded by other #. Clarify definitions in tax codes receivers for summary declaration.
# Version 5.0.1.4
# VAT descriptions modified to reports look better. 2a and 5b and the like removed and added some descriptions.
# Version 5.0.1.5 - Switch to English
# Added properties_stock_xxx properly accounts for stock valuation, changed 7000 accounts from type to type cash expense
# Changed naming of 7020 and 7030 to Cost of sales xxxx

{
    "Name": "Netherlands - Accounting '
    'Version' '2.0'
    'Category': 'Localization / Account Charts "
    "Description": "" "
This is the module to manage the accounting chart for Netherlands in OpenERP.
================================================== ===========================

Read changelog for version information in file __openerp__.py.
This is a basic module to a comprehensive general ledger and tax schedule
To install Dutch companies in OpenERP version 7.0.

VAT accounts where necessary coupled to generate the correct reporting,
think, for example to intra-Community acquisitions in which you 21% VAT should increase,
but also 21% as tax may be deducted again.

After installation of this module will be called the Configuration Wizard 'Accounting'.
    * You will be offered a list of ledger templates which are also
      Dutch ledger chart located.

    * If the configuration wizard starts, you are prompted for the name of your company
      To enter, install the ledger schedule, how many digits
      ledger account may exist, the account number of your bank and the currency
      to create Journals.

Pay attention!! -> The template of the Dutch chart of accounts is made up of 4
digits. This is the minimum number which you need to complete, you may increase the number.
The extra digits are then being behind the account fills with zeros.

    "" ",
    'Author' 'Veritos - Jan Verlaan "
    'Website' 'http://www.veritos.nl "
    'Depends': [ 'Account'
                 'Base_vat',
                 'Base_iban',
    ],
    'Data': [ 'account_chart_netherlands.xml "
              "Account_fiscal_position_template.xml",
              "Account_fiscal_position_tax_template.xml",
              "Account_fiscal_position_account_template.xml",
              "L10n_nl_account_chart.yml",
    ],
    "Demo": [],
    'Installable': True,
}
