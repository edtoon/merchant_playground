<?php

namespace BlakePrinting\AmazonMws\Subscriptions;

use BlakePrinting\AmazonMws\Sellers\AmazonMwsClientFactory;
use BlakePrinting\AmazonMws\Sellers\CredentialVO;
use MWSSubscriptionsService_Client;
use MWSSubscriptionsService_Interface;
use MWSSubscriptionsService_Mock;
use PDO;

/**
 * @method MWSSubscriptionsService_Interface createClient($applicationName, $applicationVersion, CredentialVO &$credentialVo = null, PDO $connection = null)
 */
class SubscriptionsClientFactory extends AmazonMwsClientFactory {
    public function __construct() {
        parent::__construct(
            AmazonMwsClientFactory::SUBSCRIPTION_API, "amazon_mws_subscriptions/src",
            MWSSubscriptionsService_Client::class, MWSSubscriptionsService_Mock::class,
            "/Subscriptions/2013-07-01"
        );
    }
}
