<?php

use BlakePrinting\AmazonMws\EndpointVO;
use BlakePrinting\AmazonMws\Sellers\AmazonMwsUtil;
use BlakePrinting\AmazonMws\Subscriptions\SubscriptionsClientFactory;
use BlakePrinting\Test\AbstractTestCase;

class AmazonSubscriptionsTest extends AbstractTestCase {
    /** @var MWSSubscriptionsService_Interface */
    private static $client = null;

    public static function setUpBeforeClass() {
        parent::setUpBeforeClass();

        $subscriptionsClientFactory = new SubscriptionsClientFactory();
        $endpointVo = new EndpointVO();

        $endpointVo->setId(AmazonMwsUtil::MOCK_ENDPOINT_ID);
        $endpointVo->setEndpoint(AmazonMwsUtil::MOCK_ENDPOINT);
        $subscriptionsClientFactory->setEndpointVo($endpointVo);

        self::$client =& $subscriptionsClientFactory->createClient(self::class, "0.0.1");
    }

    public function testListRegisteredDestinations() {
        $request = new MWSSubscriptionsService_Model_ListRegisteredDestinationsInput();
        $request->setMarketplaceId(AmazonMwsUtil::MOCK_MARKETPLACE_ID);
        $request->setSellerId(AmazonMwsUtil::MOCK_SELLER_ID);
        $response = self::$client->listRegisteredDestinations($request);

        AmazonMwsUtil::logResponse($response);

        $this->assertNotEmpty($response);

        $responseMetadata = $response->getResponseMetadata();

        $this->assertNotEmpty($responseMetadata);
        $this->assertEquals("String", $responseMetadata->getRequestId());

        $result = $response->getListRegisteredDestinationsResult();

        $this->assertNotEmpty($result);

        $destinationList = $result->getDestinationList();

        $this->assertNotNull($destinationList);

        $destinationMembers = $destinationList->getmember();

        $this->assertNotEmpty($destinationMembers);
        $this->assertEquals(1, count($destinationMembers));

        foreach($destinationMembers as $destinationMember) {
            $this->assertEquals("String", $destinationMember->getDeliveryChannel());

            $attributeList = $destinationMember->getAttributeList();

            $this->assertNotEmpty($attributeList);

            $attributeMembers = $attributeList->getmember();

            $this->assertNotEmpty($attributeMembers);
            $this->assertEquals(1, count($attributeMembers));

            foreach($attributeMembers as $attributeMember) {
                $this->assertEquals("String", $attributeMember->getKey());
                $this->assertEquals("String", $attributeMember->getValue());
            }
        }
    }
}
