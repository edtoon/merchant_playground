#!/bin/bash
#
# Download non-Packagist dependencies
#

set -o errexit
set -o nounset

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

VEND_DIR="${VEND_DIR:=${BASE_DIR}/vendor}"
DL_DIR="${DL_DIR:=${BASE_DIR}/downloads}"

declare -a VENDOR_DOWNLOADS=(
  "amazon_mws_subscriptions:https://images-na.ssl-images-amazon.com/images/G/01/mwsportal/clientlib/Subscriptions/MWSSubscriptionsServicePHPClientLibrary-2013-07-01._V316922762_.zip"
)

unzip -v > /dev/null 2>&1 || sudo apt-get install unzip

mkdir -p "${DL_DIR}" "${VEND_DIR}"

for vendor_download in ${VENDOR_DOWNLOADS[@]}
do
  vendor_dir="${VEND_DIR}/${vendor_download%%:*}"
  vendor_url="${vendor_download#*:}"

  if [ ! -d "${vendor_dir}" ];
  then
    download_filename="${vendor_url##*/}"
    download_path="${DL_DIR}/${download_filename}"

    if [ ! -f "${download_path}" ];
    then
      curl -o "${download_path}" "${vendor_url}"
    fi

    if [ "${download_path: -4}" = ".zip" ];
    then
      mkdir -p "${vendor_dir}"
      unzip "${download_path}" -d "${vendor_dir}"
    else
      echo "Downloaded: ${download_path: -4}"
    fi
  fi
done

for patch_php_file in `find ${VEND_DIR}/amazon_mws_subscriptions/src/MWSSubscriptionsService/ -name "*.php"`
do
  backup_file="${patch_php_file}.bak"

  if [ ! -f "${backup_file}" ] || [ "${patch_php_file}" -ot "${backup_file}" ];
  then
    sed -i.bak "s/iconv_set_encoding/ @iconv_set_encoding/g" "${patch_php_file}"
    sed -i.bak "s/@return ResponseMetadata/@return MWSSubscriptionsService_Model_ResponseMetadata/g" "${patch_php_file}"
    sed -i.bak "s/@return ListRegisteredDestinationsResult/@return MWSSubscriptionsService_Model_ListRegisteredDestinationsResult/g" "${patch_php_file}"
    sed -i.bak "s/@return DestinationList/@return MWSSubscriptionsService_Model_DestinationList/g" "${patch_php_file}"
    sed -i.bak 's/@return this instance/@return $this/g' "${patch_php_file}"
    sed -i.bak 's/@return This instance./@return $this/g' "${patch_php_file}"
    sed -i.bak 's/@param MWSSubscriptionsService_Model_DestinationList destinationList/@param $value MWSSubscriptionsService_Model_DestinationList destinationList/g' "${patch_php_file}"
    sed -i.bak "s/@return List<Destination>/@return MWSSubscriptionsService_Model_Destination[]/g" "${patch_php_file}"
    sed -i.bak "s/@return AttributeKeyValueList/@return MWSSubscriptionsService_Model_AttributeKeyValueList/g" "${patch_php_file}"
    sed -i.bak "s/@return List<AttributeKeyValue>/@return MWSSubscriptionsService_Model_AttributeKeyValue[]/g" "${patch_php_file}"
    sed -i.bak "s/@return ListSubscriptionsResult/@return MWSSubscriptionsService_Model_ListSubscriptionsResult/g" "${patch_php_file}"
    sed -i.bak "s/@return SubscriptionList/@return MWSSubscriptionsService_Model_SubscriptionList/g" "${patch_php_file}"
    sed -i.bak "s/@return List<Subscription>/@return MWSSubscriptionsService_Model_Subscription[]/g" "${patch_php_file}"
    sed -i.bak "s/@return Destination/@return MWSSubscriptionsService_Model_Destination/g" "${patch_php_file}"
  fi
done
