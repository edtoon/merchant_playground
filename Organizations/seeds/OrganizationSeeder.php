<?php

use BlakePrinting\Db\DbSeed;
use BlakePrinting\Organizations\OrganizationDAO;
use BlakePrinting\Organizations\OrganizationTypeDAO;
use BlakePrinting\Organizations\OrganizationTypeVO;
use BlakePrinting\Organizations\OrganizationUtil;
use BlakePrinting\Util\Utils;

class OrganizationSeeder extends DbSeed {
    public final function run() {
        $connection =& $this->getConnection();
        $organizationDao =& OrganizationDAO::instance($connection);
        $organizationTypeDao =& OrganizationTypeDAO::instance($connection);

        Utils::forEachConstantInObjectClass(
            $organizationTypeDao,
            function($_, $constantName, $constantValue) use (&$organizationTypeDao) {
                $organizationTypeDao->findOrCreate(new OrganizationTypeVO($constantValue));
            },
            "ORGANIZATION_TYPE_"
        );

        Utils::forEachConstantInObjectClass(
            $organizationDao,
            function($_, $constantName, $constantValue) use (&$connection) {
                OrganizationUtil::addOrganizationFromConstant($constantValue, $connection);
            }, "ORGANIZATION_BASE_"
        );
    }
}
