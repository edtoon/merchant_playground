<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\Organizations\OrganizationDAO;

class OrganizationNonUniqueNames extends DbMigration {
    public function up() {
        $organizationDao =& OrganizationDAO::instance($this->getConnection());

        $this->tableDao($organizationDao)
            ->removeIndex("name")
            ->addIndex("name")
            ->save()
        ;
    }

    public function down() {
        $organizationDao =& OrganizationDAO::instance($this->getConnection());

        $this->tableDao($organizationDao)
            ->removeIndex("name")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;
    }
}
