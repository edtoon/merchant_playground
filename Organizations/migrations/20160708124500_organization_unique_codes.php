<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\Organizations\OrganizationDAO;

class OrganizationUniqueCodes extends DbMigration {
    public function up() {
        $this->tableDao(OrganizationDAO::instance($this->getConnection()))
            ->removeIndex("code")
            ->addIndex("code", ["unique" => true])
            ->save()
        ;
    }

    public function down() {
        $this->tableDao(OrganizationDAO::instance($this->getConnection()))
            ->removeIndex("code")
            ->addIndex("code")
            ->save()
        ;
    }
}
