<?php

use BlakePrinting\Contacts\ContactAddressDAO;
use BlakePrinting\Contacts\ContactPersonDAO;
use BlakePrinting\Contacts\ContactPhoneDAO;
use BlakePrinting\Db\DbMigration;
use BlakePrinting\Organizations\OrganizationContactAddressDAO;
use BlakePrinting\Organizations\OrganizationContactPersonDAO;
use BlakePrinting\Organizations\OrganizationContactPhoneDAO;
use BlakePrinting\Organizations\OrganizationDAO;
use BlakePrinting\Organizations\OrganizationOrganizationDAO;

class Organizations extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $organizationDao = new OrganizationDAO($connection);
        $this->tableDao($organizationDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $organizationOrganizationDao = new OrganizationOrganizationDAO($connection);
        $organizationTbl = $organizationDao->formatSchemaAndTableName();
        $this->tableDao($organizationOrganizationDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["parent_organization_id", "organization_id"]])
            ->addColumn("parent_organization_id", "integer", ["signed" => false])
            ->addColumn("organization_id", "integer", ["signed" => false])
            ->addForeignKey("parent_organization_id", $organizationTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("organization_id", $organizationTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $organizationContactAddressDAO = new OrganizationContactAddressDAO($connection);
        $contactAddressDao = new ContactAddressDAO($connection);
        $contactAddressTbl = $contactAddressDao->formatSchemaAndTableName();
        $this->tableDao($organizationContactAddressDAO, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["organization_id", "contact_address_id"]])
            ->addColumn("organization_id", "integer", ["signed" => false])
            ->addColumn("contact_address_id", "integer", ["signed" => false])
            ->addForeignKey("organization_id", $organizationTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("contact_address_id", $contactAddressTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $organizationContactPersonDao = new OrganizationContactPersonDAO($connection);
        $contactPersonDao = new ContactPersonDAO($connection);
        $contactPersonTbl = $contactPersonDao->formatSchemaAndTableName();
        $this->tableDao($organizationContactPersonDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["organization_id", "contact_person_id"]])
            ->addColumn("organization_id", "integer", ["signed" => false])
            ->addColumn("contact_person_id", "integer", ["signed" => false])
            ->addForeignKey("organization_id", $organizationTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("contact_person_id", $contactPersonTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $organizationContactPhoneDao = new OrganizationContactPhoneDAO($connection);
        $contactPhoneDao = new ContactPhoneDAO($connection);
        $contactPhoneTbl = $contactPhoneDao->formatSchemaAndTableName();
        $this->tableDao($organizationContactPhoneDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["organization_id", "contact_phone_id"]])
            ->addColumn("organization_id", "integer", ["signed" => false])
            ->addColumn("contact_phone_id", "integer", ["signed" => false])
            ->addForeignKey("organization_id", $organizationTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("contact_phone_id", $contactPhoneTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(new OrganizationContactPhoneDAO($connection));
        $this->dropDao(new OrganizationContactPersonDAO($connection));
        $this->dropDao(new OrganizationContactAddressDAO($connection));
        $this->dropDao(new OrganizationOrganizationDAO($connection));
        $this->dropDao(new OrganizationDAO($connection));
    }
}
