<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\Globalization\CurrencyDAO;
use BlakePrinting\Organizations\OrganizationDAO;

class OrganizationCurrency extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();
        $organizationDao =& OrganizationDAO::instance($connection);
        $currencyDao =& CurrencyDAO::instance($connection);

        $this->alterDao(
            $organizationDao,
            "ADD currency_id SMALLINT(5) UNSIGNED, " .
            "ADD CONSTRAINT organization_ibfk_1 FOREIGN KEY (currency_id) REFERENCES " . $currencyDao->formatSchemaAndTableName() . " (id)"
        );
    }

    public function down() {
        $connection =& $this->getConnection();
        $organizationDao =& OrganizationDAO::instance($connection);

        $this->alterDao($organizationDao, "DROP FOREIGN KEY organization_ibfk_1, DROP COLUMN currency_id");
    }
}
