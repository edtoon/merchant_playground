<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\Organizations\OrganizationDAO;

class OrganizationCode extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();
        $organizationDao = new OrganizationDAO($connection);

        $this->alterDao($organizationDao, "ADD code VARCHAR(191) NULL, ADD INDEX (code)");
    }

    public function down() {
        $connection =& $this->getConnection();
        $organizationDao = new OrganizationDAO($connection);

        $this->alterDao($organizationDao, "DROP INDEX code, DROP COLUMN code");
    }
}
