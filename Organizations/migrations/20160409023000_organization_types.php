<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\Organizations\OrganizationDAO;
use BlakePrinting\Organizations\OrganizationOrganizationTypeDAO;
use BlakePrinting\Organizations\OrganizationTypeDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class OrganizationTypes extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $organizationTypeDao = new OrganizationTypeDAO($connection);
        $this->tableDao($organizationTypeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $organizationDao = new OrganizationDAO($connection);
        $organizationOrganizationTypeDao = new OrganizationOrganizationTypeDAO($connection);
        $this->tableDao($organizationOrganizationTypeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["organization_id", "organization_type_id"]])
            ->addColumn("organization_id", "integer", ["signed" => false])
            ->addColumn("organization_type_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addForeignKey("organization_id", $organizationDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("organization_type_id", $organizationTypeDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(new OrganizationOrganizationTypeDAO($connection));
        $this->dropDao(new OrganizationTypeDAO($connection));
    }
}
