<?php

namespace BlakePrinting\Organizations;

use BlakePrinting\Db\ValueObject;
use JsonSerializable;

class OrganizationOrganizationTypeVO implements JsonSerializable, ValueObject {
    /** @var int */
    private $organizationId = null;
    /** @var int */
    private $organizationTypeId = null;

    public function jsonSerialize() {
        return [
            "organization_id" => $this->organizationId,
            "organization_type_id" => $this->organizationTypeId
        ];
    }

    /**
     * @return int
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * @param int $organizationId
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    }

    /**
     * @return int
     */
    public function getOrganizationTypeId()
    {
        return $this->organizationTypeId;
    }

    /**
     * @param int $organizationTypeId
     */
    public function setOrganizationTypeId($organizationTypeId)
    {
        $this->organizationTypeId = $organizationTypeId;
    }
}
