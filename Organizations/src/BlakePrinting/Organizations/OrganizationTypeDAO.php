<?php

namespace BlakePrinting\Organizations;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static OrganizationTypeDAO instance(PDO $connection = null, $schemaName = null)
 * @method OrganizationTypeVO findById($id, $options = null, $useCache = false)
 * @method OrganizationTypeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method OrganizationTypeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method OrganizationTypeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method OrganizationTypeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method OrganizationTypeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method OrganizationTypeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class OrganizationTypeDAO extends AbstractDataAccessObject {
    const ORGANIZATION_TYPE_PROSPECT = "prospect";
    const ORGANIZATION_TYPE_CUSTOMER = "customer";
    const ORGANIZATION_TYPE_VENDOR = "vendor";
    const ORGANIZATION_TYPE_SUPPLIER = "supplier";
    const ORGANIZATION_TYPE_DISTRIBUTOR = "distributor";
    const ORGANIZATION_TYPE_MANUFACTURER = "manufacturer";
    const ORGANIZATION_TYPE_PARTNER = "partner";
    const ORGANIZATION_TYPE_INVESTOR = "investor";
    const ORGANIZATION_TYPE_LENDER = "lender";
    const ORGANIZATION_TYPE_GOVERNMENT = "government";
    const ORGANIZATION_TYPE_INTERNAL = "internal";
    const ORGANIZATION_TYPE_REGULATOR = "regulator";
    const ORGANIZATION_TYPE_TAX_AGENCY = "tax_agency";
}
