<?php

namespace BlakePrinting\Organizations;

use BlakePrinting\Db\ValueObject;

abstract class AbstractEntityOrganizationVO implements ValueObject {
    /** @var int */
    private $organizationId = null;

    /**
     * @param int $organizationId
     */
    public function __construct($organizationId = null) {
        $this->organizationId = $organizationId;
    }

    /**
     * @return int
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * @param int $organizationId
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    }
}
