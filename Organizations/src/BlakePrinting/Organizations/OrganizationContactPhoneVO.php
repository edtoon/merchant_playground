<?php

namespace BlakePrinting\Organizations;

use BlakePrinting\Contacts\AbstractEntityContactPhoneVO;
use JsonSerializable;

class OrganizationContactPhoneVO extends AbstractEntityContactPhoneVO implements JsonSerializable {
    /** @var int */
    private $organizationId = null;

    public function jsonSerialize() {
        return [
            "organization_id" => $this->organizationId,
            "contact_phone_id" => $this->getContactPhoneId()
        ];
    }

    /**
     * @return int
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * @param int $organizationId
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    }
}
