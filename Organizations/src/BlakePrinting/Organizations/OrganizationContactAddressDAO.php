<?php

namespace BlakePrinting\Organizations;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static OrganizationContactAddressDAO instance(PDO $connection = null, $schemaName = null)
 * @method OrganizationContactAddressVO findById($id, $options = null, $useCache = false)
 * @method OrganizationContactAddressVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method OrganizationContactAddressVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method OrganizationContactAddressVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method OrganizationContactAddressVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method OrganizationContactAddressVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method OrganizationContactAddressVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class OrganizationContactAddressDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $organizationId
     * @param int $contactAddressId
     * @return int
     */
    public function &store($organizationId, $contactAddressId) {
        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value("organization_id", $organizationId, PDO::PARAM_INT)
            ->value("contact_address_id", $contactAddressId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
