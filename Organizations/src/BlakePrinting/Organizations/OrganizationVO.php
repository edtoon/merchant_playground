<?php

namespace BlakePrinting\Organizations;

use BlakePrinting\Db\ByteLimit;
use BlakePrinting\Db\ValueObject;
use JsonSerializable;
use Phinx\Db\Adapter\MysqlAdapter;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class OrganizationVO implements JsonSerializable, ValueObject {
    const NAME_LENGTH_BYTES = 191;
    const CODE_LENGTH_BYTES = 191;

    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var string */
    private $name = null;
    /** @var string */
    private $code = null;
    /** @var int */
    private $currencyId = null;

    public static function loadValidatorMetadata(ClassMetadata $metadata) {
        $metadata->addGetterConstraints(
            "id", [new Assert\Type("digit"), new Assert\Range(["min" => 1, "max" => MysqlAdapter::INT_REGULAR])]
        );
        $metadata->addGetterConstraints(
            "created", [new Assert\Type("digit"), new Assert\Range(["min" => 1, "max" => MysqlAdapter::INT_BIG])]
        );
        $metadata->addGetterConstraints(
            "updated", [new Assert\Type("digit"), new Assert\Range(["min" => 1, "max" => MysqlAdapter::INT_BIG])]
        );
        $metadata->addGetterConstraints(
            "name", [new Assert\NotBlank(), new ByteLimit(self::NAME_LENGTH_BYTES)]
        );
        $metadata->addGetterConstraints(
            "code", [new Assert\NotBlank(), new ByteLimit(self::CODE_LENGTH_BYTES)]
        );
        $metadata->addGetterConstraints(
            "currencyId", [new Assert\Type("digit"), new Assert\Range(["min" => 1, "max" => MysqlAdapter::INT_SMALL])]
        );
    }

    /**
     * @param string $name
     * @param string $code
     * @param int $currencyId
     */
    public function __construct($name = null, $code = null, $currencyId = null) {
        $this->name = $name;
        $this->code = $code;
        $this->currencyId = $currencyId;
    }

    public function jsonSerialize() {
        return [
            "id" => $this->id,
            "created" => $this->created,
            "updated" => $this->updated,
            "name" => $this->name,
            "code" => $this->code,
            "currency_id" => $this->currencyId
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getCurrencyId()
    {
        return $this->currencyId;
    }

    /**
     * @param int $currencyId
     */
    public function setCurrencyId($currencyId)
    {
        $this->currencyId = $currencyId;
    }
}
