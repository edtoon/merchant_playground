<?php

namespace BlakePrinting\Organizations;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use BlakePrinting\Globalization\CurrencyDAO;
use PDO;

/**
 * @method static OrganizationDAO instance(PDO $connection = null, $schemaName = null)
 * @method OrganizationVO findById($id, $options = null, $useCache = false)
 * @method OrganizationVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method OrganizationVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method OrganizationVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method OrganizationVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method OrganizationVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method OrganizationVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method OrganizationVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class OrganizationDAO extends AbstractDataAccessObject {
    const ORGANIZATION_BASE_AMAZON = ["name" => "Amazon.com, Inc.", "code" => "AMZN", "types" => [OrganizationTypeDAO::ORGANIZATION_TYPE_DISTRIBUTOR]];
    const ORGANIZATION_BASE_RAKUTEN = ["name" => "Rakuten, Inc.", "code" => "RKTN", "types" => [OrganizationTypeDAO::ORGANIZATION_TYPE_DISTRIBUTOR]];
    const ORGANIZATION_BASE_ZEBRA = ["name" => "Zebra Technologies Corp.", "code" => "ZBRA", "types" => [OrganizationTypeDAO::ORGANIZATION_TYPE_MANUFACTURER]];

    const ORGANIZATION_BLAKE_B_MARKETING = ["name" => "B Internet Marketing, Inc.", "currency" => CurrencyDAO::CURRENCY_USD, "types" => [OrganizationTypeDAO::ORGANIZATION_TYPE_INTERNAL]];
    const ORGANIZATION_BLAKE_INK_HERO_US = ["name" => "Ink Hero, Inc.", "currency" => CurrencyDAO::CURRENCY_USD, "types" => [OrganizationTypeDAO::ORGANIZATION_TYPE_INTERNAL]];
    const ORGANIZATION_BLAKE_NW_MERCH = ["name" => "Northwest Merchants, Inc.", "currency" => CurrencyDAO::CURRENCY_USD, "types" => [OrganizationTypeDAO::ORGANIZATION_TYPE_INTERNAL]];

    const ORGANIZATION_BLAKE_ORINK = ["name" => "Orink", "code" => "OR", "types" => [OrganizationTypeDAO::ORGANIZATION_TYPE_SUPPLIER]];
}
