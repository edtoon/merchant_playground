<?php

namespace BlakePrinting\Organizations;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static OrganizationOrganizationDAO instance(PDO $connection = null, $schemaName = null)
 * @method OrganizationOrganizationVO findById($id, $options = null, $useCache = false)
 * @method OrganizationOrganizationVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method OrganizationOrganizationVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method OrganizationOrganizationVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method OrganizationOrganizationVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method OrganizationOrganizationVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method OrganizationOrganizationVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class OrganizationOrganizationDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $parentOrganizationId
     * @param int $organizationId
     * @return int
     */
    public function &store($parentOrganizationId, $organizationId) {
        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value("parent_organization_id", $parentOrganizationId, PDO::PARAM_INT)
            ->value("organization_id", $organizationId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
