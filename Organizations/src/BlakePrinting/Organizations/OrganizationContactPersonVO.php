<?php

namespace BlakePrinting\Organizations;

use BlakePrinting\Db\ValueObject;
use JsonSerializable;

class OrganizationContactPersonVO implements JsonSerializable, ValueObject {
    /** @var int */
    private $organizationId = null;
    /** @var int */
    private $contactPersonId = null;

    public function jsonSerialize() {
        return [
            "organization_id" => $this->organizationId,
            "contact_person_id" => $this->contactPersonId
        ];
    }

    /**
     * @return int
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * @param int $organizationId
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    }

    /**
     * @return int
     */
    public function getContactPersonId()
    {
        return $this->contactPersonId;
    }

    /**
     * @param int $contactPersonId
     */
    public function setContactPersonId($contactPersonId)
    {
        $this->contactPersonId = $contactPersonId;
    }
}
