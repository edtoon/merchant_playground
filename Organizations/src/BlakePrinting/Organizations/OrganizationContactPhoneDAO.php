<?php

namespace BlakePrinting\Organizations;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static OrganizationContactPhoneDAO instance(PDO $connection = null, $schemaName = null)
 * @method OrganizationContactPhoneVO findById($id, $options = null, $useCache = false)
 * @method OrganizationContactPhoneVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method OrganizationContactPhoneVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method OrganizationContactPhoneVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method OrganizationContactPhoneVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method OrganizationContactPhoneVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method OrganizationContactPhoneVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class OrganizationContactPhoneDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $organizationId
     * @param int $contactPhoneId
     * @return int
     */
    public function &store($organizationId, $contactPhoneId) {
        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value("organization_id", $organizationId, PDO::PARAM_INT)
            ->value("contact_phone_id", $contactPhoneId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
