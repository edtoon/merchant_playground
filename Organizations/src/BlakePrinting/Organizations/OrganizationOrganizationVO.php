<?php

namespace BlakePrinting\Organizations;

use JsonSerializable;

class OrganizationOrganizationVO extends AbstractEntityOrganizationVO implements JsonSerializable {
    /** @var int */
    private $parentOrganizationId = null;

    public function jsonSerialize() {
        return [
            "parent_organization_id" => $this->parentOrganizationId,
            "organization_id" => $this->getOrganizationId()
        ];
    }

    /**
     * @return int
     */
    public function getParentOrganizationId()
    {
        return $this->parentOrganizationId;
    }

    /**
     * @param int $parentOrganizationId
     */
    public function setParentOrganizationId($parentOrganizationId)
    {
        $this->parentOrganizationId = $parentOrganizationId;
    }
}
