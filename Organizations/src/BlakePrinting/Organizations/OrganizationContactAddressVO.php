<?php

namespace BlakePrinting\Organizations;

use BlakePrinting\Contacts\AbstractEntityContactAddressVO;
use JsonSerializable;

class OrganizationContactAddressVO extends AbstractEntityContactAddressVO implements JsonSerializable {
    /** @var int */
    private $organizationId = null;

    public function jsonSerialize() {
        return [
            "organization_id" => $this->organizationId,
            "contact_address_id" => $this->getContactAddressId()
        ];
    }

    /**
     * @return int
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * @param int $organizationId
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    }
}
