<?php

namespace BlakePrinting\Organizations;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static OrganizationOrganizationTypeDAO instance(PDO $connection = null, $schemaName = null)
 * @method OrganizationOrganizationTypeVO findById($id, $options = null, $useCache = false)
 * @method OrganizationOrganizationTypeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method OrganizationOrganizationTypeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method OrganizationOrganizationTypeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method OrganizationOrganizationTypeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method OrganizationOrganizationTypeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method OrganizationOrganizationTypeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class OrganizationOrganizationTypeDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $organizationId
     * @param int $organizationTypeId
     * @return int
     */
    public function &store($organizationId, $organizationTypeId) {
        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value("organization_id", $organizationId, PDO::PARAM_INT)
            ->value("organization_type_id", $organizationTypeId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
