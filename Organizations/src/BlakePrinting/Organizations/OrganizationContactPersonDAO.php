<?php

namespace BlakePrinting\Organizations;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static OrganizationContactPersonDAO instance(PDO $connection = null, $schemaName = null)
 * @method OrganizationContactPersonVO findById($id, $options = null, $useCache = false)
 * @method OrganizationContactPersonVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method OrganizationContactPersonVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method OrganizationContactPersonVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method OrganizationContactPersonVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method OrganizationContactPersonVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method OrganizationContactPersonVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class OrganizationContactPersonDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $organizationId
     * @param int $contactPersonId
     * @return int
     */
    public function &store($organizationId, $contactPersonId) {
        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value("organization_id", $organizationId, PDO::PARAM_INT)
            ->value("contact_person_id", $contactPersonId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
