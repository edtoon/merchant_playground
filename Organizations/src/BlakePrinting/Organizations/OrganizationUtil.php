<?php

namespace BlakePrinting\Organizations;

use BlakePrinting\Contacts\ContactAddressVO;
use BlakePrinting\Contacts\ContactPersonAddressDAO;
use BlakePrinting\Contacts\ContactPersonDAO;
use BlakePrinting\Contacts\ContactPersonDataFeedDAO;
use BlakePrinting\Contacts\ContactPersonPersonTypeDAO;
use BlakePrinting\Contacts\ContactPersonTypeDAO;
use BlakePrinting\Contacts\ContactPersonVO;
use BlakePrinting\DataSources\DataFeedVO;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Globalization\CurrencyDAO;
use BlakePrinting\Globalization\CurrencyVO;
use BlakePrinting\Util\Utils;
use PDO;
use ReflectionClass;
use Stringy\StaticStringy as S;

class OrganizationUtil {
    private function __construct() { }

    /**
     * @param string $organizationType
     * @param PDO $connection
     * @param string|string[] $orderBy
     * @param string|string[]|int|int[] $limit
     * @param string|string[] $options
     * @return OrganizationVO[]
     */
    public static function &getOrganizationsByType($organizationType, PDO $connection = null, $orderBy = null, $limit = null, $options = null) {
        return OrganizationDAO::instance($connection)->findAll(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$organizationType, &$connection) {
                $organizationOrganizationTypeDao =& OrganizationOrganizationTypeDAO::instance($connection);
                $organizationTypeDao =& OrganizationTypeDAO::instance($connection);
                $organizationTypeId = $organizationTypeDao
                    ->findByColumn("name", $organizationType, PDO::PARAM_STR, null, null, null, true)
                    ->getId()
                ;

                $pdoQuery
                    ->join($organizationOrganizationTypeDao, "organization.id = organization_organization_type.organization_id")
                    ->where("organization_type_id", $organizationTypeId, PDO::PARAM_INT)
                ;
            },
            $orderBy, $limit, $options
        );
    }

    /**
     * @param array $constantValue
     * @param PDO $connection
     * @return OrganizationVO
     */
    public static function addOrganizationFromConstant(array $constantValue, PDO $connection = null) {
        $organizationDao =& OrganizationDAO::instance($connection);
        $organizationName = (empty($constantValue["name"]) ? null : $constantValue["name"]);
        $organizationCode = (empty($constantValue["code"]) ? null : $constantValue["code"]);
        $organizationCurrency = (empty($constantValue["currency"]) ? null : $constantValue["currency"]);
        $organizationTypes = (empty($constantValue["types"]) ? null : $constantValue["types"]);
        $organizationParent = (empty($constantValue["parent"]) ? null : $constantValue["parent"]);
        $currencyVo = ($organizationCurrency === null ? null : CurrencyDAO::instance($connection)->findByColumn(
            "iso_code", $organizationCurrency[0], PDO::PARAM_STR, null, null, null, true
        ));
        $organizationVo = $organizationDao->findOrCreate(new OrganizationVO(
            $organizationName, $organizationCode, ($currencyVo === null ? null : $currencyVo->getId())
        ));
        $organizationId = $organizationVo->getId();

        if($organizationTypes !== null) {
            $organizationTypeDao =& OrganizationTypeDAO::instance($connection);
            $organizationOrganizationTypeDao =& OrganizationOrganizationTypeDAO::instance($connection);

            foreach($organizationTypes as &$organizationType) {
                $organizationTypeId = $organizationTypeDao
                    ->findByColumn("name", $organizationType, PDO::PARAM_STR, null, null, null, true)
                    ->getId()
                ;

                $organizationOrganizationTypeDao->store($organizationId, $organizationTypeId);
            }

            unset($organizationType);
        }

        if($organizationParent !== null) {
            $organizationOrganizationDao =& OrganizationOrganizationDAO::instance($connection);
            $parentOrganizationId = $organizationDao
                ->findByColumn("name", $organizationParent["name"], PDO::PARAM_STR, null, null, null, true)
                ->getId()
            ;

            $organizationOrganizationDao->store($parentOrganizationId, $organizationId);
        }

        return $organizationVo;
    }

    /**
     * @param string $companyName
     * @param string $organizationType
     * @param PDO $connection
     * @param CurrencyVO $currencyVo
     * @param ContactAddressVO $contactAddressVo
     * @return OrganizationVO
     */
    public static function &getOrganizationVoByCompanyNameAndType($companyName, $organizationType = null, PDO &$connection = null, CurrencyVO $currencyVo = null, ContactAddressVO $contactAddressVo = null) {
        $organizationContactAddressDao =& OrganizationContactAddressDAO::instance($connection);
        $organizationDao =& OrganizationDAO::instance($connection);
        $companyName = Utils::getTrimmedStringOrNull($companyName);
        $currencyId = ($currencyVo === null ? null : $currencyVo->getId());
        $organizationVo = null;

        if($companyName !== null) {
            $daoReflectionClass =& Utils::getReflectionClassByName(get_class($organizationDao));
            $daoConstants = $daoReflectionClass->getConstants();
            $companyNameUpper = (string)S::toUpperCase($companyName);

            foreach($daoConstants as $constantName => &$constantValue) {
                $internalOrganizationTypes = $constantValue["types"];

                if($organizationType !== null) {
                    if(array_key_exists("types", $constantValue)) {
                        if(!in_array($organizationType, $internalOrganizationTypes, true)) {
                            continue;
                        }
                    }
                }

                if(strpos($constantName, "ORGANIZATION_") === 0) {
                    $internalOrganizationName = $constantValue["name"];
                    $internalOrganizationNameUpper = (string)S::toUpperCase($internalOrganizationName);

                    if($companyNameUpper === $internalOrganizationNameUpper) {
                        $organizationVo =& $organizationDao->findByColumn("name", $internalOrganizationName, PDO::PARAM_STR, null, null, null, true);
                        break;
                    }
                }
            }

            if($organizationVo === null) {
                $companyName = Utils::getStringTruncatedToByteLength($companyName, OrganizationVO::NAME_LENGTH_BYTES);
                $organizationVo =& $organizationDao->findOrCreate(new OrganizationVO($companyName, null, $currencyId));
            }

            $organizationId = $organizationVo->getId();

            if($contactAddressVo !== null) {
                $organizationContactAddressDao->store($organizationId, $contactAddressVo->getId());
            }

            if($organizationType !== null) {
                $organizationOrganizationTypeDao =& OrganizationOrganizationTypeDAO::instance($connection);
                $organizationTypeDao =& OrganizationTypeDAO::instance($connection);
                $organizationTypeVo =& $organizationTypeDao->findByColumn(
                    "name", $organizationType, PDO::PARAM_STR, null, null, null, true
                );

                $organizationOrganizationTypeDao->store($organizationId, $organizationTypeVo->getId());
            }
        }

        return $organizationVo;
    }

    /**
     * @param string $contactName
     * @param PDO $connection
     * @param DataFeedVO $dataFeedVo
     * @param ContactAddressVO $contactAddressVo
     * @param OrganizationVO $organizationVo
     * @return ContactPersonVO
     */
    public static function getContactPersonVoByName($contactName, PDO &$connection, DataFeedVO $dataFeedVo = null, ContactAddressVO $contactAddressVo = null, OrganizationVO $organizationVo = null) {
        $contactPersonAddressDao =& ContactPersonAddressDAO::instance($connection);
        $contactPersonDao =& ContactPersonDAO::instance($connection);
        $contactName = Utils::getTrimmedStringOrNull($contactName);
        $contactPersonVo = null;

        if($contactName !== null) {
            if($contactAddressVo === null) {
                $contactPersonVo =& $contactPersonDao->findOrCreate(new ContactPersonVO($contactName));
            } else {
                $contactPersonVo =& $contactPersonDao->findSingle(
                    function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$contactPersonAddressDao, &$contactAddressVo, $contactName) {
                        $pdoQuery
                            ->join($contactPersonAddressDao, [
                                "id = person_id",
                                "address_id = " . $contactAddressVo->getId()
                            ])
                            ->where("name", $contactName, PDO::PARAM_STR)
                        ;
                    },
                    null, null, "FOR UPDATE"
                );

                if($contactPersonVo === null) {
                    $contactPersonId =& $contactPersonDao->insert(new ContactPersonVO($contactName));
                    $contactPersonVo =& $contactPersonDao->findById($contactPersonId);

                    if($contactAddressVo !== null) {
                        $contactPersonAddressDao->store($contactPersonId, $contactAddressVo->getId());
                    }
                }
            }

            $contactPersonPersonTypeDao =& ContactPersonPersonTypeDAO::instance($connection);
            $contactPersonTypeDao =& ContactPersonTypeDAO::instance($connection);
            $contactPersonTypeVo =& $contactPersonTypeDao->findByColumn(
                "name", ContactPersonTypeDAO::PERSON_TYPE_CUSTOMER, PDO::PARAM_STR, null, null, null, true
            );
            $contactPersonId = $contactPersonVo->getId();

            $contactPersonPersonTypeDao->store($contactPersonId, $contactPersonTypeVo->getId());

            if($organizationVo !== null) {
                $organizationContactPersonDao =& OrganizationContactPersonDAO::instance($connection);

                $organizationContactPersonDao->store($organizationVo->getId(), $contactPersonId);
            }

            if($dataFeedVo !== null) {
                $contactPersonDataFeedDao =& ContactPersonDataFeedDAO::instance($connection);

                $contactPersonDataFeedDao->store($contactPersonId, $dataFeedVo->getId());
            }
        }

        return $contactPersonVo;
    }
}
