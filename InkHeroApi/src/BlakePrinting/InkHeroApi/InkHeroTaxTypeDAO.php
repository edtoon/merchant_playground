<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static InkHeroTaxTypeDAO instance(PDO $connection = null, $schemaName = null)
 * @method InkHeroTaxTypeVO findById($id, $options = null, $useCache = false)
 * @method InkHeroTaxTypeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroTaxTypeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroTaxTypeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroTaxTypeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroTaxTypeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroTaxTypeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroTaxTypeVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class InkHeroTaxTypeDAO extends AbstractDataAccessObject {
}
