<?php

namespace BlakePrinting\InkHeroApi\Extract;

use BlakePrinting\Db\PDOQuery;
use BlakePrinting\ETL\ExtractResult;
use BlakePrinting\Fields\FieldDAO;
use BlakePrinting\Fields\FieldDomainDAO;
use BlakePrinting\Fields\FieldDomainVO;
use BlakePrinting\Fields\FieldSchemaDAO;
use BlakePrinting\Fields\FieldSchemaVO;
use BlakePrinting\Fields\FieldTableDAO;
use BlakePrinting\Fields\FieldTableVO;
use BlakePrinting\Fields\FieldTypeDAO;
use BlakePrinting\Fields\FieldTypeVO;
use BlakePrinting\Fields\FieldVO;
use BlakePrinting\InkHeroApi\InkHeroAddressDAO;
use BlakePrinting\InkHeroApi\InkHeroAddressFieldSignedDAO;
use BlakePrinting\InkHeroApi\InkHeroAddressFieldSignedVO;
use BlakePrinting\InkHeroApi\InkHeroAddressFieldStringDAO;
use BlakePrinting\InkHeroApi\InkHeroAddressFieldStringVO;
use BlakePrinting\InkHeroApi\InkHeroAddressTypeDAO;
use BlakePrinting\InkHeroApi\InkHeroAddressTypeVO;
use BlakePrinting\InkHeroApi\InkHeroAddressVO;
use BlakePrinting\InkHeroApi\InkHeroChargeTypeDAO;
use BlakePrinting\InkHeroApi\InkHeroChargeTypeVO;
use BlakePrinting\InkHeroApi\InkHeroItemDAO;
use BlakePrinting\InkHeroApi\InkHeroItemVO;
use BlakePrinting\InkHeroApi\InkHeroOrderAddressDAO;
use BlakePrinting\InkHeroApi\InkHeroOrderAddressVO;
use BlakePrinting\InkHeroApi\InkHeroOrderDAO;
use BlakePrinting\InkHeroApi\InkHeroOrderItemDAO;
use BlakePrinting\InkHeroApi\InkHeroOrderItemVO;
use BlakePrinting\InkHeroApi\InkHeroOrderPriceDAO;
use BlakePrinting\InkHeroApi\InkHeroOrderPriceVO;
use BlakePrinting\InkHeroApi\InkHeroOrderVO;
use BlakePrinting\InkHeroApi\InkHeroPaymentMethodDAO;
use BlakePrinting\InkHeroApi\InkHeroPaymentMethodVO;
use BlakePrinting\InkHeroApi\InkHeroShippingMethodDAO;
use BlakePrinting\InkHeroApi\InkHeroShippingMethodVO;
use BlakePrinting\InkHeroApi\InkHeroSiteVO;
use BlakePrinting\InkHeroApi\InkHeroTaxTypeDAO;
use BlakePrinting\InkHeroApi\InkHeroTaxTypeVO;
use BlakePrinting\InkHeroApi\InkHeroApiFields;
use BlakePrinting\Util\Utils;
use DateTime;
use Exception;
use PDO;
use ReflectionClass;

class ExtractUtil {
    private function __construct() { }

    public static function &extractOrder(array &$order, InkHeroSiteVO &$inkHeroSiteVo, PDO &$connection = null) {
        $orderVo = new InkHeroOrderVO();

        $orderVo->setInkHeroSiteId($inkHeroSiteVo->getId());
        $orderVo->setCustomerId(self::arrayGetValue("customerid", $order));
        $orderVo->setIpv4(self::arrayGetValue("ipaddress", $order));
        $orderVo->setLanguage(self::arrayGetUtf8("language", $order));
        $orderVo->setOrderNumber(self::arrayGetValue("ordernumber", $order));
        $orderVo->setComments(self::arrayGetUtf8("ordercomments", $order));
        $orderVo->setSessionId(self::arrayGetValue("sessionid", $order));
        $orderVo->setLastStatusId(self::arrayGetValue("last_status", $order));
        $orderVo->setConfirmed(Utils::getBooleanOrNull(self::arrayGetValue("confirmed", $order)));

        $orderDate = self::arrayGetValue("orderdate", $order);
        $lastUpdateDate = self::arrayGetValue("last_update", $order);
        $paymentMethod = self::arrayGetUtf8("payment_method", $order);
        $shippingMethod = self::arrayGetUtf8("shipping_method", $order);
        $taxType = self::arrayGetUtf8("taxtype", $order);

        if($orderDate !== null) {
            $dateTime = new DateTime($orderDate);

            $orderVo->setOrderDate($dateTime->getTimestamp());
        }

        if($lastUpdateDate !== null) {
            $dateTime = new DateTime($lastUpdateDate);

            $orderVo->setLastUpdateDate($dateTime->getTimestamp());
        }

        if($paymentMethod !== null) {
            $paymentMethodVo =& InkHeroPaymentMethodDAO::instance($connection)->findOrCreate(
                new InkHeroPaymentMethodVO($paymentMethod)
            );

            $orderVo->setInkHeroPaymentMethodId($paymentMethodVo->getId());
        }

        if($shippingMethod !== null) {
            $shippingMethodVo =& InkHeroShippingMethodDAO::instance($connection)->findOrCreate(
                new InkHeroShippingMethodVO($shippingMethod)
            );

            $orderVo->setInkHeroShippingMethodId($shippingMethodVo->getId());
        }

        if($taxType !== null) {
            $taxTypeVo =& InkHeroTaxTypeDAO::instance($connection)->findOrCreate(
                new InkHeroTaxTypeVO($shippingMethod)
            );

            $orderVo->setInkHeroTaxTypeId($taxTypeVo->getId());
        }

        $extractResult = new ExtractResult();

        self::storeOrder($orderVo, $extractResult, $connection);

        $orderVo = $extractResult->getValueObject();

        if($orderVo instanceof InkHeroOrderVO) {
            $addressTypeDao =& InkHeroAddressTypeDAO::instance($connection);
            $keepAddressIds = [];

            Utils::forEachConstantInObjectClass(
                $addressTypeDao,
                function($_, $constantName, $constantValue) use (&$order, &$addressTypeDao, &$inkHeroSiteVo, &$extractResult, &$keepAddressIds, &$connection) {
                    if(array_key_exists($constantValue, $order)) {
                        $addressTypeVo =& $addressTypeDao->findByColumn("name", $constantValue, PDO::PARAM_STR, null, null, null, true);
                        $addressVo =& self::extractAddress($order[$constantValue], $addressTypeVo, $inkHeroSiteVo, $extractResult, $connection);

                        if($addressVo !== null) {
                            $keepAddressIds[] = $addressVo->getId();
                        }
                    }
                }, "ADDRESS_TYPE_"
            );

            $delete = PDOQuery::delete()
                ->from(InkHeroOrderAddressDAO::instance($connection))
                ->where("ink_hero_order_id", $orderVo->getId(), PDO::PARAM_INT)
            ;

            if(!empty($keepAddressIds)) {
                $delete->where("ink_hero_address_id NOT IN (" . implode(",", $keepAddressIds) . ")");
            }

            $delete->executeGetRowCount($connection);

            if(array_key_exists("order_lines", $order)) {
                self::extractPrice($order["order_lines"], $inkHeroSiteVo, $extractResult, $connection);
            }

            if(array_key_exists("order_items", $order)) {
                self::extractItems($order["order_items"], $inkHeroSiteVo, $extractResult, $connection);
            }
        }

        return $extractResult;
    }

    private static function extractItems(array &$items, InkHeroSiteVO &$inkHeroSiteVo, ExtractResult &$extractResult, PDO &$connection = null) {
        $orderVo = $extractResult->getValueObject();

        if($orderVo instanceof InkHeroOrderVO) {
            $inkHeroOrderItemDao =& InkHeroOrderItemDAO::instance($connection);
            $orderId = $orderVo->getId();
            $keepItemIds = [];

            if(Utils::isNonEmptyArray($items)) {
                $inkHeroItemDao =& InkHeroItemDAO::instance($connection);
                $itemCount = count($items);

                for($i = 0; $i < $itemCount; $i++) {
                    $item =& $items[$i];
                    $id = self::arrayGetValue("id", $item);
                    $itemId = self::arrayGetValue("itemid", $item);
                    $quantity = self::arrayGetValue("qty", $item);
                    $price = self::arrayGetValue("price", $item);
                    $name = self::arrayGetUtf8("itemname", $item);

                    if($quantity !== null) {
                        $quantity = intval($quantity);
                    }

                    $itemId = $inkHeroItemDao
                        ->findOrCreate(new InkHeroItemVO($inkHeroSiteVo->getId(), $itemId, $name))
                        ->getId()
                    ;
                    $keepItemIds[] = $itemId;
                    $newOrderItemVo = new InkHeroOrderItemVO($orderId, $itemId, $quantity, $price);
                    $existingOrderItemVo =& $inkHeroOrderItemDao->findSingle(
                        function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$orderId, &$itemId) {
                            $pdoQuery
                                ->where("ink_hero_order_id", $orderId, PDO::PARAM_INT)
                                ->where("ink_hero_item_id", $itemId, PDO::PARAM_INT)
                            ;
                        }, null, null, "FOR UPDATE"
                    );

                    if($existingOrderItemVo === null) {
                        $inkHeroOrderItemDao->insert($newOrderItemVo);
                    } else {
                        $differences = Utils::propertiesDifference($existingOrderItemVo, $newOrderItemVo);

                        if($differences !== null) {
                            $updateCount = PDOQuery::update()
                                ->table($inkHeroOrderItemDao)
                                ->set("quantity", $quantity, PDO::PARAM_INT)
                                ->set("price", $price, PDO::PARAM_STR)
                                ->where("ink_hero_order_id", $orderId, PDO::PARAM_INT)
                                ->where("ink_hero_item_id", $itemId, PDO::PARAM_INT)
                                ->executeGetRowCount($connection)
                            ;

                            if($updateCount !== 1) {
                                throw new Exception(
                                    "Too " . ($updateCount > 1 ? "many" : "few") . " results updating " .
                                    "item: " . Utils::getVarDump($newOrderItemVo) . ", " .
                                    "differences: " . Utils::getVarDump($differences)
                                );
                            }
                        }
                    }
                }
            }

            $delete = PDOQuery::delete()
                ->from($inkHeroOrderItemDao)
                ->where("ink_hero_order_id", $orderId, PDO::PARAM_INT)
            ;

            if(!empty($keepItemIds)) {
                $delete->where("ink_hero_item_id NOT IN (" . implode(",", $keepItemIds) . ")");
            }

            $delete->executeGetRowCount($connection);
        }
    }

    private static function extractPrice(array &$lines, InkHeroSiteVO &$inkHeroSiteVo, ExtractResult &$extractResult, PDO &$connection = null) {
        $orderVo = $extractResult->getValueObject();

        if($orderVo instanceof InkHeroOrderVO) {
            $inkHeroOrderPriceDao =& InkHeroOrderPriceDAO::instance($connection);
            $orderId = $orderVo->getId();
            $keepChargeTypeIds = [];

            if(Utils::isNonEmptyArray($lines)) {
                $inkHeroChargeTypeDao =& InkHeroChargeTypeDAO::instance($connection);
                $lineCount = count($lines);

                for($i = 0; $i < $lineCount; $i++) {
                    $line =& $lines[$i];
                    $lineName = self::arrayGetUtf8("linename", $line);
                    $lineType = self::arrayGetUtf8("type", $line);
                    $lineAmount = self::arrayGetUtf8("amount", $line);
                    $chargeTypeId = $inkHeroChargeTypeDao
                        ->findOrCreate(new InkHeroChargeTypeVO($inkHeroSiteVo->getId(), $lineType, $lineName))
                        ->getId()
                    ;
                    $keepChargeTypeIds[] = $chargeTypeId;
                    $newPriceVo = new InkHeroOrderPriceVO($orderId, $chargeTypeId, $lineAmount);
                    $existingPriceVo =& $inkHeroOrderPriceDao->findSingle(
                        function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$orderId, &$chargeTypeId) {
                            $pdoQuery
                                ->where("ink_hero_order_id", $orderId, PDO::PARAM_INT)
                                ->where("ink_hero_charge_type_id", $chargeTypeId, PDO::PARAM_INT)
                            ;
                        }, null, null, "FOR UPDATE"
                    );

                    if($existingPriceVo === null) {
                        $inkHeroOrderPriceDao->insert($newPriceVo);
                    } else {
                        $differences = Utils::propertiesDifference($existingPriceVo, $newPriceVo);

                        if($differences !== null) {
                            $updateCount = PDOQuery::update()
                                ->table($inkHeroOrderPriceDao)
                                ->set("amount", $lineAmount, PDO::PARAM_STR)
                                ->where("ink_hero_order_id", $orderId, PDO::PARAM_INT)
                                ->where("ink_hero_charge_type_id", $chargeTypeId, PDO::PARAM_INT)
                                ->executeGetRowCount($connection)
                            ;

                            if($updateCount !== 1) {
                                throw new Exception(
                                    "Too " . ($updateCount > 1 ? "many" : "few") . " results updating " .
                                    "price: " . Utils::getVarDump($newPriceVo) . ", " .
                                    "differences: " . Utils::getVarDump($differences)
                                );
                            }
                        }
                    }
                }
            }

            $delete = PDOQuery::delete()
                ->from($inkHeroOrderPriceDao)
                ->where("ink_hero_order_id", $orderId, PDO::PARAM_INT)
            ;

            if(!empty($keepChargeTypeIds)) {
                $delete->where("ink_hero_charge_type_id NOT IN (" . implode(",", $keepChargeTypeIds) . ")");
            }

            $delete->executeGetRowCount($connection);
        }
    }

    private static function &extractAddress(array &$address, InkHeroAddressTypeVO &$addressTypeVo, InkHeroSiteVO &$inkHeroSiteVo, ExtractResult &$extractResult, PDO &$connection = null) {
        $siteAddressId = self::arrayGetValue("addressid", $address);
        $addressLines = (array_key_exists("address_lines", $address) ? $address["address_lines"] : null);
        $addressVo = null;

        if(Utils::isNonEmptyArray($addressLines)) {
            $addressVo = new InkHeroAddressVO();
            $addressLineCount = count($addressLines);
            $nonEmptyLineCount = 0;

            if($addressLineCount > 10) {
                throw new Exception("Too many lines in address: " . Utils::getVarDump($address));
            }

            for($i = 0; $i < $addressLineCount; $i++) {
                $addressLine = Utils::getTrimmedStringOrNull($addressLines[$i]);

                if($addressLine === null) {
                    continue;
                }

                $addressLine = Utils::ensureUtf8($addressLine);
                $nonEmptyLineCount++;

                switch($i + 1) {
                    case 1:
                        $addressVo->setLine1($addressLine);
                    break;
                    case 2:
                        $addressVo->setLine2($addressLine);
                    break;
                    case 3:
                        $addressVo->setLine3($addressLine);
                    break;
                    case 4:
                        $addressVo->setLine4($addressLine);
                    break;
                    case 5:
                        $addressVo->setLine5($addressLine);
                    break;
                    case 6:
                        $addressVo->setLine6($addressLine);
                    break;
                    case 7:
                        $addressVo->setLine7($addressLine);
                    break;
                    case 8:
                        $addressVo->setLine8($addressLine);
                    break;
                    case 9:
                        $addressVo->setLine9($addressLine);
                    break;
                    case 10:
                        $addressVo->setLine10($addressLine);
                    break;
                }
            }

            if($nonEmptyLineCount > 0) {
                $addressVo =& InkHeroAddressDAO::instance($connection)->findOrCreate(
                    $addressVo, "FOR UPDATE",
                    ["line1", "line2", "line3", "line4", "line5", "line6", "line7", "line8", "line9", "line10"],
                    ["created", "updated"]
                );

                $orderVo = $extractResult->getValueObject();

                if($orderVo instanceof InkHeroOrderVO) {
                    InkHeroOrderAddressDAO::instance($connection)->insert(
                        new InkHeroOrderAddressVO(
                            $orderVo->getId(), $addressTypeVo->getId(), $addressVo->getId()
                        ), null, true
                    );
                }

                if($siteAddressId !== null && $inkHeroSiteVo !== null) {
                    $fieldTypeDao =& FieldTypeDAO::instance($connection);
                    $stringFieldTypeVo =& $fieldTypeDao->findByColumn(
                        "name", FieldTypeDAO::TYPE_STRING, PDO::PARAM_STR, null, null, null, true
                    );
                    $signedFieldTypeVo =& $fieldTypeDao->findByColumn(
                        "name", FieldTypeDAO::TYPE_SIGNED, PDO::PARAM_STR, null, null, null, true
                    );
                    $fieldDomainVo =& FieldDomainDAO::instance($connection)->findOrCreate(
                        new FieldDomainVO(InkHeroApiFields::FIELD_ADDRESS_ID["domain"])
                    );
                    $fieldSchemaVo =& FieldSchemaDAO::instance($connection)->findOrCreate(
                        new FieldSchemaVO($fieldDomainVo->getId(), $inkHeroSiteVo->getHost())
                    );
                    $fieldTableVo =& FieldTableDAO::instance($connection)->findOrCreate(
                        new FieldTableVO($fieldSchemaVo->getId(), InkHeroApiFields::FIELD_ADDRESS_ID["table"])
                    );
                    $addressIdFieldVo =& self::getAddressFieldVo(
                        $fieldTableVo, $signedFieldTypeVo, InkHeroApiFields::FIELD_ADDRESS_ID["field"], $connection
                    );

                    $addressIdVo = new InkHeroAddressFieldSignedVO($addressIdFieldVo->getId(), $siteAddressId);

                    $addressIdVo->setInkHeroAddressId($addressVo->getId());

                    InkHeroAddressFieldSignedDAO::instance($connection)->findOrCreate($addressIdVo);

                    $inkHeroAddressFieldStringDao =& InkHeroAddressFieldStringDAO::instance($connection);

                    if(array_key_exists("address_components", $address)) {
                        $addressComponents =& $address["address_components"];

                        if(Utils::isNonEmptyArray($addressComponents)) {
                            foreach($addressComponents as &$addressComponent) {
                                $componentName = self::arrayGetUtf8("entryname", $addressComponent);
                                $componentValue = self::arrayGetUtf8("entryvalue", $addressComponent);
                                $componentFieldVo =& self::getAddressFieldVo(
                                    $fieldTableVo, $stringFieldTypeVo, $componentName, $connection
                                );
                                $componentVo =& $inkHeroAddressFieldStringDao->findSingle(
                                    function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$addressVo, &$componentFieldVo) {
                                        $pdoQuery
                                            ->where("ink_hero_address_id", $addressVo->getId())
                                            ->where("field_id", $componentFieldVo->getId(), PDO::PARAM_INT)
                                        ;
                                    }
                                );

                                if($componentVo === null) {
                                    $componentVo = new InkHeroAddressFieldStringVO(
                                        $componentFieldVo->getId(), $componentValue
                                    );

                                    $componentVo->setInkHeroAddressId($addressVo->getId());

                                    $inkHeroAddressFieldStringDao->insert($componentVo);
                                } else if(!Utils::valuesMatch($componentValue, $componentVo->getValue())) {
                                    PDOQuery::update()
                                        ->table($inkHeroAddressFieldStringDao)
                                        ->set("value", $componentValue, PDO::PARAM_INT)
                                        ->where("ink_hero_address_id", $addressVo->getId(), PDO::PARAM_INT)
                                        ->where("field_id", $componentFieldVo->getId(), PDO::PARAM_INT)
                                        ->executeEnsuringRowCount($connection, 1)
                                    ;
                                }
                            }

                            unset($addressComponent);
                        }
                    }
                }
            }
        }

        return $addressVo;
    }

    private static function &getAddressFieldVo(FieldTableVO &$fieldTableVo, FieldTypeVO &$fieldTypeVo, $fieldName, PDO &$connection = null) {
        return FieldDAO::instance($connection)->findOrCreate(
            new FieldVO($fieldTableVo->getId(), $fieldTypeVo->getId(), $fieldName)
        );
    }

    private static function storeOrder(InkHeroOrderVO &$orderVo, ExtractResult &$extractResult, PDO &$connection = null) {
        $inkHeroOrderDao =& InkHeroOrderDAO::instance($connection);
        $existingOrderVo = null;

        if($orderVo->getId() !== null) {
            $existingOrderVo =& $inkHeroOrderDao->findById($orderVo->getId(), "FOR UPDATE");
        } else {
            $existingOrderVo =& $inkHeroOrderDao->findSingle(
                function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$orderVo) {
                    $pdoQuery
                        ->whereValueOrNull("ink_hero_site_id", $orderVo->getInkHeroSiteId(), PDO::PARAM_INT)
                        ->whereValueOrNull("order_date", $orderVo->getOrderDate(), PDO::PARAM_INT)
                        ->whereValueOrNull("order_number", $orderVo->getOrderNumber(), PDO::PARAM_STR)
                        ->whereValueOrNull("customer_id", $orderVo->getCustomerId(), PDO::PARAM_INT)
                        ->whereValueOrNull("session_id", $orderVo->getSessionId(), PDO::PARAM_STR)
                    ;
                }, null, null, "FOR UPDATE"
            );
        }

        if($existingOrderVo === null) {
            $insertId = PDOQuery::insert()
                ->into($inkHeroOrderDao)
                ->value("created", "UNIX_TIMESTAMP()")
                ->value("ink_hero_site_id", $orderVo->getInkHeroSiteId(), PDO::PARAM_INT)
                ->value("ink_hero_payment_method_id", $orderVo->getInkHeroPaymentMethodId(), PDO::PARAM_INT)
                ->value("ink_hero_shipping_method_id", $orderVo->getInkHeroShippingMethodId(), PDO::PARAM_INT)
                ->value("ink_hero_tax_type_id", $orderVo->getInkHeroTaxTypeId(), PDO::PARAM_INT)
                ->value("order_date", $orderVo->getOrderDate(), PDO::PARAM_INT)
                ->value("order_number", $orderVo->getOrderNumber(), PDO::PARAM_STR)
                ->value("customer_id", $orderVo->getCustomerId(), PDO::PARAM_INT)
                ->value("session_id", $orderVo->getSessionId(), PDO::PARAM_STR)
                ->value("language", $orderVo->getLanguage(), PDO::PARAM_STR)
                ->value("comments", $orderVo->getLanguage(), PDO::PARAM_STR)
                ->value("confirmed", $orderVo->isConfirmed(), PDO::PARAM_BOOL)
                ->value("last_status_id", $orderVo->getLastStatusId(), PDO::PARAM_INT)
                ->value("last_update_date", $orderVo->getLastUpdateDate(), PDO::PARAM_INT)
                ->value("ipv4", $orderVo->getIpv4(), PDO::PARAM_STR, "INET6_ATON")
                ->executeEnsuringLastInsertId($connection)
            ;

            if($insertId < 1) {
                throw new Exception("Got result: " . $insertId . " trying to insert order: " . Utils::getVarDump($orderVo));
            }

            $extractResult->setInserted(true);
            $extractResult->setValueObject($inkHeroOrderDao->findById($insertId));
        } else {
            $differences = Utils::propertiesDifference($existingOrderVo, $orderVo, ["id", "created", "updated"]);

            if($differences === null) {
                $extractResult->setValueObject($existingOrderVo);
            } else {
                $extractResult->setDifferences($differences);

                $updateCount = PDOQuery::update()
                    ->table($inkHeroOrderDao)
                    ->set("updated", "UNIX_TIMESTAMP()")
                    ->set("ink_hero_site_id", $orderVo->getInkHeroSiteId(), PDO::PARAM_INT)
                    ->set("ink_hero_payment_method_id", $orderVo->getInkHeroPaymentMethodId(), PDO::PARAM_INT)
                    ->set("ink_hero_shipping_method_id", $orderVo->getInkHeroShippingMethodId(), PDO::PARAM_INT)
                    ->set("ink_hero_tax_type_id", $orderVo->getInkHeroTaxTypeId(), PDO::PARAM_INT)
                    ->set("order_date", $orderVo->getOrderDate(), PDO::PARAM_INT)
                    ->set("order_number", $orderVo->getOrderNumber(), PDO::PARAM_STR)
                    ->set("customer_id", $orderVo->getCustomerId(), PDO::PARAM_INT)
                    ->set("session_id", $orderVo->getSessionId(), PDO::PARAM_STR)
                    ->set("language", $orderVo->getLanguage(), PDO::PARAM_STR)
                    ->set("comments", $orderVo->getComments(), PDO::PARAM_STR)
                    ->set("confirmed", $orderVo->isConfirmed(), PDO::PARAM_BOOL)
                    ->set("last_status_id", $orderVo->getLastStatusId(), PDO::PARAM_INT)
                    ->set("last_update_date", $orderVo->getLastUpdateDate(), PDO::PARAM_INT)
                    ->set("ipv4", $orderVo->getIpv4(), PDO::PARAM_STR, "INET6_ATON")
                    ->where("id", $existingOrderVo->getId(), PDO::PARAM_INT)
                    ->executeGetRowCount($connection)
                ;

                if($updateCount !== 1) {
                    throw new Exception(
                        "Too " . ($updateCount > 1 ? "many" : "few") . " results updating " .
                        "order: " . Utils::getVarDump($orderVo) . ", " .
                        "differences: " . Utils::getVarDump($differences)
                    );
                }

                $extractResult->setUpdated(true);
                $extractResult->setValueObject($inkHeroOrderDao->findById($existingOrderVo->getId()));
            }
        }

        if($extractResult->getValueObject() === null) {
            throw new Exception("Failed to find extracted order: " . Utils::getVarDump($orderVo));
        }
    }

    private static function arrayGetValue($key, array &$array) {
        if(array_key_exists($key, $array)) {
            return Utils::getTrimmedStringOrNull($array[$key]);
        }

        return null;
    }

    private static function arrayGetUtf8($key, array &$array) {
        return Utils::ensureUtf8(self::arrayGetValue($key, $array));
    }
}
