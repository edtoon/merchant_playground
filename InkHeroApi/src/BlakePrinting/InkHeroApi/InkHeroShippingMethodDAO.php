<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static InkHeroShippingMethodDAO instance(PDO $connection = null, $schemaName = null)
 * @method InkHeroShippingMethodVO findById($id, $options = null, $useCache = false)
 * @method InkHeroShippingMethodVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroShippingMethodVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroShippingMethodVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroShippingMethodVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroShippingMethodVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroShippingMethodVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroShippingMethodVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class InkHeroShippingMethodDAO extends AbstractDataAccessObject {
}
