<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Db\ValueObject;

class InkHeroOrderItemVO implements ValueObject {
    /** @var int */
    private $inkHeroOrderId = null;
    /** @var int */
    private $inkHeroItemId = null;
    /** @var int */
    private $quantity = null;
    /** @var string */
    private $price = null;

    public function __construct($inkHeroOrderId = null, $inkHeroItemId = null, $quantity = null, $price = null) {
        $this->inkHeroOrderId = $inkHeroOrderId;
        $this->inkHeroItemId = $inkHeroItemId;
        $this->quantity = $quantity;
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getInkHeroOrderId()
    {
        return $this->inkHeroOrderId;
    }

    /**
     * @param int $inkHeroOrderId
     */
    public function setInkHeroOrderId($inkHeroOrderId)
    {
        $this->inkHeroOrderId = $inkHeroOrderId;
    }

    /**
     * @return int
     */
    public function getInkHeroItemId()
    {
        return $this->inkHeroItemId;
    }

    /**
     * @param int $inkHeroItemId
     */
    public function setInkHeroItemId($inkHeroItemId)
    {
        $this->inkHeroItemId = $inkHeroItemId;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }
}
