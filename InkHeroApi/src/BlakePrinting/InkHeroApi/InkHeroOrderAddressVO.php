<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Db\ValueObject;

class InkHeroOrderAddressVO implements ValueObject {
    /** @var int */
    private $inkHeroOrderId = null;
    /** @var int */
    private $inkHeroAddressTypeId = null;
    /** @var int */
    private $inkHeroAddressId = null;

    public function __construct($inkHeroOrderId = null, $inkHeroAddressTypeId = null, $inkHeroAddressId = null) {
        $this->inkHeroOrderId = $inkHeroOrderId;
        $this->inkHeroAddressTypeId = $inkHeroAddressTypeId;
        $this->inkHeroAddressId = $inkHeroAddressId;
    }

    /**
     * @return int
     */
    public function getInkHeroOrderId()
    {
        return $this->inkHeroOrderId;
    }

    /**
     * @param int $inkHeroOrderId
     */
    public function setInkHeroOrderId($inkHeroOrderId)
    {
        $this->inkHeroOrderId = $inkHeroOrderId;
    }

    /**
     * @return int
     */
    public function getInkHeroAddressTypeId()
    {
        return $this->inkHeroAddressTypeId;
    }

    /**
     * @param int $inkHeroAddressTypeId
     */
    public function setInkHeroAddressTypeId($inkHeroAddressTypeId)
    {
        $this->inkHeroAddressTypeId = $inkHeroAddressTypeId;
    }

    /**
     * @return int
     */
    public function getInkHeroAddressId()
    {
        return $this->inkHeroAddressId;
    }

    /**
     * @param int $inkHeroAddressId
     */
    public function setInkHeroAddressId($inkHeroAddressId)
    {
        $this->inkHeroAddressId = $inkHeroAddressId;
    }
}
