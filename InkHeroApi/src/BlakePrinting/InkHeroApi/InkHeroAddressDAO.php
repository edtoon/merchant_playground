<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static InkHeroAddressDAO instance(PDO $connection = null, $schemaName = null)
 * @method InkHeroAddressVO findById($id, $options = null, $useCache = false)
 * @method InkHeroAddressVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroAddressVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroAddressVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroAddressVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroAddressVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroAddressVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroAddressVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class InkHeroAddressDAO extends AbstractDataAccessObject {
}
