<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use BlakePrinting\Globalization\CurrencyDAO;
use BlakePrinting\Organizations\OrganizationDAO;
use PDO;

/**
 * @method static InkHeroSiteDAO instance(PDO $connection = null, $schemaName = null)
 * @method InkHeroSiteVO findById($id, $options = null, $useCache = false)
 * @method InkHeroSiteVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroSiteVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroSiteVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroSiteVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroSiteVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroSiteVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroSiteVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class InkHeroSiteDAO extends AbstractDataAccessObject {
    const SITE_INK_HERO_COM = ["host" => "www.inkhero.com", "currency" => CurrencyDAO::CURRENCY_USD, "org" => OrganizationDAO::ORGANIZATION_BLAKE_INK_HERO_US];
    const SITE_INK_HERO_CA = ["host" => "www.inkhero.ca", "currency" => CurrencyDAO::CURRENCY_CAD, "org" => OrganizationDAO::ORGANIZATION_BLAKE_INK_HERO_US];
    const SITE_INK_HERO_CO_JP = ["host" => "www.inkhero.co.jp", "currency" => CurrencyDAO::CURRENCY_JPY, "org" => OrganizationDAO::ORGANIZATION_BLAKE_WWL];
    const SITE_INK_HERO_FR = ["host" => "www.inkhero.fr", "currency" => CurrencyDAO::CURRENCY_EUR, "org" => OrganizationDAO::ORGANIZATION_BLAKE_ROUTIQUE];
    const SITE_INK_HERO_NL = ["host" => "www.inkhero.nl", "currency" => CurrencyDAO::CURRENCY_EUR, "org" => OrganizationDAO::ORGANIZATION_BLAKE_KRB];
    const SITE_INK_HERO_DE = ["host" => "www.inkhero.de", "currency" => CurrencyDAO::CURRENCY_EUR, "org" => OrganizationDAO::ORGANIZATION_BLAKE_LRB];
    const SITE_INK_HERO_BE = ["host" => "www.inkhero.be", "currency" => CurrencyDAO::CURRENCY_EUR, "org" => OrganizationDAO::ORGANIZATION_BLAKE_KRB];
    const SITE_INK_HERO_CO_UK = ["host" => "www.inkhero.co.uk", "currency" => CurrencyDAO::CURRENCY_EUR, "org" => OrganizationDAO::ORGANIZATION_BLAKE_61];
    const SITE_BLAKE = ["host" => "www.blakeprintingsupply.com", "currency" => CurrencyDAO::CURRENCY_USD, "org" => OrganizationDAO::ORGANIZATION_BLAKE_NW_MERCH];
    const SITE_INKT_NL = ["host" => "www.inktmaxx.nl", "currency" => CurrencyDAO::CURRENCY_EUR, "org" => OrganizationDAO::ORGANIZATION_BLAKE_KRB];
    const SITE_INKT_BE = ["host" => "www.inktmaxx.be", "currency" => CurrencyDAO::CURRENCY_EUR, "org" => OrganizationDAO::ORGANIZATION_BLAKE_KRB];
    const SITE_PARADISE_CARTOUCHE_FR = ["host" => "www.paradisecartouche.fr", "currency" => CurrencyDAO::CURRENCY_EUR, "org" => OrganizationDAO::ORGANIZATION_BLAKE_ROUTIQUE];
    const SITE_PARADISE_CARTOUCHE_BE = ["host" => "www.paradisecartouche.be", "currency" => CurrencyDAO::CURRENCY_EUR, "org" => OrganizationDAO::ORGANIZATION_BLAKE_ROUTIQUE];
    const SITE_ALL_INKJETS_CO_UK = ["host" => "www.allinkjets.co.uk", "currency" => CurrencyDAO::CURRENCY_EUR, "org" => OrganizationDAO::ORGANIZATION_BLAKE_61];
    const SITE_ALL_INKJETS_CA = ["host" => "www.allinkjets.ca", "currency" => CurrencyDAO::CURRENCY_CAD, "org" => OrganizationDAO::ORGANIZATION_BLAKE_61];
    const SITE_TINTEN_TONER_UND_CO_DE = ["host" => "www.tinten-toner-und-co.de", "currency" => CurrencyDAO::CURRENCY_EUR, "org" => OrganizationDAO::ORGANIZATION_BLAKE_LRB];
    const SITE_INK_KING_JP = ["host" => "www.inkking.jp", "currency" => CurrencyDAO::CURRENCY_JPY, "org" => OrganizationDAO::ORGANIZATION_BLAKE_WWL];
    const SITE_CARTRIDGENET_CO_UK = ["host" => "www.cartridgenet.co.uk", "currency" => CurrencyDAO::CURRENCY_EUR, "org" => OrganizationDAO::ORGANIZATION_BLAKE_61];
    const SITE_PATRONENLAND_DE = ["host" => "www.patronenland.de", "currency" => CurrencyDAO::CURRENCY_EUR, "org" => OrganizationDAO::ORGANIZATION_BLAKE_LRB];
    const SITE_ENCRECHOIX_FR = ["host" => "www.encrechoix.fr", "currency" => CurrencyDAO::CURRENCY_EUR, "org" => OrganizationDAO::ORGANIZATION_BLAKE_ROUTIQUE];
    const SITE_1001_CARTOUCHE_CA = ["host" => "www.1001cartouche.ca", "currency" => CurrencyDAO::CURRENCY_CAD, "org" => OrganizationDAO::ORGANIZATION_BLAKE_INK_HERO_US];
    const SITE_DIRECT_INKJETS_COM = ["host" => "www.directinkjets.com", "currency" => CurrencyDAO::CURRENCY_USD, "org" => OrganizationDAO::ORGANIZATION_BLAKE_NW_MERCH];
    const SITE_9INKS_COM = ["host" => "www.9inks.com", "currency" => CurrencyDAO::CURRENCY_USD, "org" => OrganizationDAO::ORGANIZATION_BLAKE_NW_MERCH];
    const SITE_ZULU_INKS_CA = ["host" => "www.zuluinks.ca", "currency" => CurrencyDAO::CURRENCY_CAD, "org" => OrganizationDAO::ORGANIZATION_BLAKE_INK_HERO_US];
    const SITE_GTONER_COM = ["host" => "www.gtoner.com", "currency" => CurrencyDAO::CURRENCY_USD, "org" => OrganizationDAO::ORGANIZATION_BLAKE_NW_MERCH];
}
