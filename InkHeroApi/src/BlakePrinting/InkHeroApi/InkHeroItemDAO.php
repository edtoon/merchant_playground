<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static InkHeroItemDAO instance(PDO $connection = null, $schemaName = null)
 * @method InkHeroItemVO findById($id, $options = null, $useCache = false)
 * @method InkHeroItemVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroItemVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroItemVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroItemVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroItemVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroItemVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroItemVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class InkHeroItemDAO extends AbstractDataAccessObject {
}
