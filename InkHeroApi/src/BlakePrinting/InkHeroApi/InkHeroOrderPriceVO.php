<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Db\ValueObject;

class InkHeroOrderPriceVO implements ValueObject {
    /** @var int */
    private $inkHeroOrderId = null;
    /** @var int */
    private $inkHeroChargeTypeId = null;
    /** @var string */
    private $amount = null;

    public function __construct($inkHeroOrderId = null, $inkHeroChargeTypeId = null, $amount = null) {
        $this->inkHeroOrderId = $inkHeroOrderId;
        $this->inkHeroChargeTypeId = $inkHeroChargeTypeId;
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getInkHeroOrderId()
    {
        return $this->inkHeroOrderId;
    }

    /**
     * @param int $inkHeroOrderId
     */
    public function setInkHeroOrderId($inkHeroOrderId)
    {
        $this->inkHeroOrderId = $inkHeroOrderId;
    }

    /**
     * @return int
     */
    public function getInkHeroChargeTypeId()
    {
        return $this->inkHeroChargeTypeId;
    }

    /**
     * @param int $inkHeroChargeTypeId
     */
    public function setInkHeroChargeTypeId($inkHeroChargeTypeId)
    {
        $this->inkHeroChargeTypeId = $inkHeroChargeTypeId;
    }

    /**
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }
}
