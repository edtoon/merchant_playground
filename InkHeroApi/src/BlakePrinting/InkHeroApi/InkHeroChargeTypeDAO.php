<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static InkHeroChargeTypeDAO instance(PDO $connection = null, $schemaName = null)
 * @method InkHeroChargeTypeVO findById($id, $options = null, $useCache = false)
 * @method InkHeroChargeTypeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroChargeTypeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroChargeTypeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroChargeTypeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroChargeTypeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroChargeTypeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroChargeTypeVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class InkHeroChargeTypeDAO extends AbstractDataAccessObject {
    const TYPE_SUBTOTAL = "subtotal";
    const TYPE_SHIPPING = "shipping";
    const TYPE_TAX = "tax";
    const TYPE_TOTAL_WO_TAX = "totalwotax";
    const TYPE_TOTAL = "total";
}
