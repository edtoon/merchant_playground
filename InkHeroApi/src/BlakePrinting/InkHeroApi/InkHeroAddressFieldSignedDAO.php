<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static InkHeroAddressFieldSignedDAO instance(PDO $connection = null, $schemaName = null)
 * @method InkHeroAddressFieldSignedVO findById($id, $options = null, $useCache = false)
 * @method InkHeroAddressFieldSignedVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroAddressFieldSignedVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroAddressFieldSignedVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroAddressFieldSignedVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroAddressFieldSignedVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroAddressFieldSignedVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class InkHeroAddressFieldSignedDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
