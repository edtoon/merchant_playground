<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static InkHeroPaymentMethodDAO instance(PDO $connection = null, $schemaName = null)
 * @method InkHeroPaymentMethodVO findById($id, $options = null, $useCache = false)
 * @method InkHeroPaymentMethodVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroPaymentMethodVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroPaymentMethodVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroPaymentMethodVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroPaymentMethodVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroPaymentMethodVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroPaymentMethodVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class InkHeroPaymentMethodDAO extends AbstractDataAccessObject {
}
