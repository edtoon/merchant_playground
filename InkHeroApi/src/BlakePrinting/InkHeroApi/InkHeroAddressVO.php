<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Db\ValueObject;

class InkHeroAddressVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var string */
    private $line1 = null;
    /** @var string */
    private $line2 = null;
    /** @var string */
    private $line3 = null;
    /** @var string */
    private $line4 = null;
    /** @var string */
    private $line5 = null;
    /** @var string */
    private $line6 = null;
    /** @var string */
    private $line7 = null;
    /** @var string */
    private $line8 = null;
    /** @var string */
    private $line9 = null;
    /** @var string */
    private $line10 = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getLine1()
    {
        return $this->line1;
    }

    /**
     * @param string $line1
     */
    public function setLine1($line1)
    {
        $this->line1 = $line1;
    }

    /**
     * @return string
     */
    public function getLine2()
    {
        return $this->line2;
    }

    /**
     * @param string $line2
     */
    public function setLine2($line2)
    {
        $this->line2 = $line2;
    }

    /**
     * @return string
     */
    public function getLine3()
    {
        return $this->line3;
    }

    /**
     * @param string $line3
     */
    public function setLine3($line3)
    {
        $this->line3 = $line3;
    }

    /**
     * @return string
     */
    public function getLine4()
    {
        return $this->line4;
    }

    /**
     * @param string $line4
     */
    public function setLine4($line4)
    {
        $this->line4 = $line4;
    }

    /**
     * @return string
     */
    public function getLine5()
    {
        return $this->line5;
    }

    /**
     * @param string $line5
     */
    public function setLine5($line5)
    {
        $this->line5 = $line5;
    }

    /**
     * @return string
     */
    public function getLine6()
    {
        return $this->line6;
    }

    /**
     * @param string $line6
     */
    public function setLine6($line6)
    {
        $this->line6 = $line6;
    }

    /**
     * @return string
     */
    public function getLine7()
    {
        return $this->line7;
    }

    /**
     * @param string $line7
     */
    public function setLine7($line7)
    {
        $this->line7 = $line7;
    }

    /**
     * @return string
     */
    public function getLine8()
    {
        return $this->line8;
    }

    /**
     * @param string $line8
     */
    public function setLine8($line8)
    {
        $this->line8 = $line8;
    }

    /**
     * @return string
     */
    public function getLine9()
    {
        return $this->line9;
    }

    /**
     * @param string $line9
     */
    public function setLine9($line9)
    {
        $this->line9 = $line9;
    }

    /**
     * @return string
     */
    public function getLine10()
    {
        return $this->line10;
    }

    /**
     * @param string $line10
     */
    public function setLine10($line10)
    {
        $this->line10 = $line10;
    }
}
