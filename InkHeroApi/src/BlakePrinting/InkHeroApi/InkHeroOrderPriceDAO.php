<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static InkHeroOrderPriceDAO instance(PDO $connection = null, $schemaName = null)
 * @method InkHeroOrderPriceVO findById($id, $options = null, $useCache = false)
 * @method InkHeroOrderPriceVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroOrderPriceVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroOrderPriceVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroOrderPriceVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroOrderPriceVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroOrderPriceVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class InkHeroOrderPriceDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    public function getPriceSumByChargeType(InkHeroOrderVO &$orderVo, $chargeType) {
        $connection =& $this->getConnection();
        $inkHeroChargeTypeDao =& InkHeroChargeTypeDAO::instance($connection);

        return PDOQuery::select()
            ->column("SUM(amount)")
            ->from($inkHeroChargeTypeDao)
            ->join($this, ["ink_hero_charge_type.id = ink_hero_charge_type_id"])
            ->where("type", $chargeType, PDO::PARAM_STR)
            ->where("ink_hero_order_id", $orderVo->getId(), PDO::PARAM_INT)
            ->getSingleValue($this->getConnection())
        ;
    }
}
