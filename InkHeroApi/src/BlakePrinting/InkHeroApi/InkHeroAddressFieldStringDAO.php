<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static InkHeroAddressFieldStringDAO instance(PDO $connection = null, $schemaName = null)
 * @method InkHeroAddressFieldStringVO findById($id, $options = null, $useCache = false)
 * @method InkHeroAddressFieldStringVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroAddressFieldStringVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroAddressFieldStringVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroAddressFieldStringVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroAddressFieldStringVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroAddressFieldStringVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class InkHeroAddressFieldStringDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
