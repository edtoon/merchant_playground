<?php

namespace BlakePrinting\InkHeroApi\Transform;

use BlakePrinting\InkHeroApi\InkHeroSiteDAO;
use BlakePrinting\InkHeroApi\InkHeroSiteVO;
use BlakePrinting\Organizations\OrganizationTypeDAO;
use BlakePrinting\Organizations\OrganizationUtil;
use BlakePrinting\Util\Utils;
use PDO;

class TransformOrganizationUtil {
    private function __construct() { }

    public static function &getInternalOrganizationVoBySite(InkHeroSiteVO &$siteVo, PDO &$connection = null) {
        $siteReflectionClass =& Utils::getReflectionClassByName(InkHeroSiteDAO::class);
        $siteConstants = $siteReflectionClass->getConstants();
        $organizationVo = null;

        foreach($siteConstants as $siteConstantName => &$siteConstantValue) {
            if($siteConstantValue["host"] === $siteVo->getHost()) {
                $organizationVo =& OrganizationUtil::getOrganizationVoByCompanyNameAndType(
                    $siteConstantValue["org"]["name"], OrganizationTypeDAO::ORGANIZATION_TYPE_INTERNAL, $connection
                );

                break;
            }
        }

        unset($siteConstantValue);

        return $organizationVo;
    }
}
