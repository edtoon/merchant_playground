<?php

namespace BlakePrinting\InkHeroApi\Transform;

use BlakePrinting\InkHeroApi\InkHeroOrderDAO;
use BlakePrinting\InkHeroApi\InkHeroSiteVO;
use BlakePrinting\Sales\FulfillmentStatusDAO;
use BlakePrinting\Sales\FulfillmentStatusVO;
use BlakePrinting\Sales\PaymentMethodDAO;
use BlakePrinting\Sales\PaymentMethodVO;
use BlakePrinting\Sales\SalesChannelDAO;
use BlakePrinting\Sales\SalesChannelVO;
use BlakePrinting\Util\Utils;
use Exception;
use PDO;
use Stringy\StaticStringy as S;

class TransformSalesUtil {
    const PAYMENT_METHOD_CODE_TO_NAME = [
        "30 Days Billing" => PaymentMethodDAO::PAYMENT_METHOD_AFTER_PAY,
        "AfterPay" => PaymentMethodDAO::PAYMENT_METHOD_AFTER_PAY,
        "AuthorizeAIM" => PaymentMethodDAO::PAYMENT_METHOD_AUTHORIZE_NET,
        "AuthorizeNet" => PaymentMethodDAO::PAYMENT_METHOD_AUTHORIZE_NET,
        "BillInkPayment" => PaymentMethodDAO::PAYMENT_METHOD_BILL_INK,
        "BOL.COM" => PaymentMethodDAO::PAYMENT_METHOD_BOL_COM,
        "Buck" => PaymentMethodDAO::PAYMENT_METHOD_BUCKAROO,
        "Buckaroo" => PaymentMethodDAO::PAYMENT_METHOD_BUCKAROO,
        "COD" => PaymentMethodDAO::PAYMENT_METHOD_CASH_ON_DELIVERY,
        "FRCC" => PaymentMethodDAO::PAYMENT_METHOD_FRENCH_CREDIT_CARD,
        "iDeal" => PaymentMethodDAO::PAYMENT_METHOD_IDEAL,
        "PayPalCreditCard" => PaymentMethodDAO::PAYMENT_METHOD_PAYPAL_CREDIT_CARD,
        "PayPal" => PaymentMethodDAO::PAYMENT_METHOD_PAYPAL,
        "PrePay" => PaymentMethodDAO::PAYMENT_METHOD_PREPAY,
        "Pre-pay" => PaymentMethodDAO::PAYMENT_METHOD_PREPAY,
        "PriceMinister" => PaymentMethodDAO::PAYMENT_METHOD_PRICE_MINISTER,
        "Sofort" => PaymentMethodDAO::PAYMENT_METHOD_SOFORT,
        "Sogenactif" => PaymentMethodDAO::PAYMENT_METHOD_FRENCH_CREDIT_CARD,
        "Stripe" => PaymentMethodDAO::PAYMENT_METHOD_STRIPE,
        "UKCC" => PaymentMethodDAO::PAYMENT_METHOD_PAYPAL_CREDIT_CARD,
        "WorldPay" => PaymentMethodDAO::PAYMENT_METHOD_WORLDPAY
    ];

    private function __construct() { }

    /**
     * @param string $paymentMethodCode
     * @param PDO $connection
     * @return PaymentMethodVO
     * @throws Exception
     */
    public static function getPaymentMethodVoByCode($paymentMethodCode, PDO &$connection) {
        $paymentMethodCode = Utils::getTrimmedStringOrNull($paymentMethodCode);
        $paymentMethodName = null;
        $paymentMethodVo = null;

        if($paymentMethodCode !== null) {
            $upperCaseCode = (string)S::toUpperCase($paymentMethodCode);

            foreach(self::PAYMENT_METHOD_CODE_TO_NAME as $checkMethodCode => &$checkMethodName) {
                $upperCaseCheck = (string)S::toUpperCase($checkMethodCode);

                if($upperCaseCode === $upperCaseCheck) {
                    $paymentMethodDao =& PaymentMethodDAO::instance($connection);
                    $paymentMethodVo =& $paymentMethodDao->findByColumn("name", $checkMethodName, PDO::PARAM_STR, null, null, null, true);

                    break;
                }
            }

            unset($checkMethodName);

            if($paymentMethodVo === null) {
                throw new Exception("Couldn't find payment method for code: " . $paymentMethodCode);
            }
        }

        return $paymentMethodVo;
    }

    /**
     * @param string $lastStatusId
     * @param PDO $connection
     * @return FulfillmentStatusVO
     */
    public static function getFulfillmentStatusVoByLastStatusId($lastStatusId, PDO &$connection) {
        $paymentMethodName = null;
        $fulfillmentStatusName = null;
        $fulfillmentStatusVo = null;

        switch($lastStatusId) {
            case InkHeroOrderDAO::ORDER_STATUS_PROCESSING:
            case InkHeroOrderDAO::ORDER_STATUS_BACKORDERED:
                $fulfillmentStatusName = FulfillmentStatusDAO::FULFILLMENT_STATUS_PENDING;
            break;
            case InkHeroOrderDAO::ORDER_STATUS_PARTIAL:
                $fulfillmentStatusName = FulfillmentStatusDAO::FULFILLMENT_STATUS_PARTIAL;
            break;
            case InkHeroOrderDAO::ORDER_STATUS_SHIPPED:
            case InkHeroOrderDAO::ORDER_STATUS_DELIVERED:
                $fulfillmentStatusName = FulfillmentStatusDAO::FULFILLMENT_STATUS_PROCESSED;
            break;
            case InkHeroOrderDAO::ORDER_STATUS_CANCELLED:
                $fulfillmentStatusName = FulfillmentStatusDAO::FULFILLMENT_STATUS_CANCELLED;
            break;
        }

        if($fulfillmentStatusName !== null) {
            $fulfillmentStatusVo =& FulfillmentStatusDAO::instance($connection)
                ->findByColumn("name", $fulfillmentStatusName, PDO::PARAM_STR, null, null, null, true)
            ;
        }

        return $fulfillmentStatusVo;
    }

    /**
     * @param InkHeroSiteVO $inkHeroSiteVo
     * @param PDO $connection
     * @return SalesChannelVO
     */
    public static function &getSalesChannelVoByInkHeroSiteVo(InkHeroSiteVO &$inkHeroSiteVo, PDO &$connection = null) {
        $salesChannelDao =& SalesChannelDAO::instance($connection);
        $baseHost = $inkHeroSiteVo->getHost();
        $salesChannelName = null;
        $salesChannelVo = null;

        switch($baseHost) {
            case "allinkjets.co.uk":
                $salesChannelName = SalesChannelDAO::SALES_CHANNEL_ALL_INKJETS;
            break;
            case "inktmaxx.nl":
                $salesChannelName = SalesChannelDAO::SALES_CHANNEL_INKT_MAXX;
            break;
            case "paradisecartouche.fr":
                $salesChannelName = SalesChannelDAO::SALES_CHANNEL_PARADISE_CARTOUCHE;
            break;
            case "tinten-toner-und-co.de":
                $salesChannelName = SalesChannelDAO::SALES_CHANNEL_TINTEN_TONER;
            break;
        }

        if($salesChannelName === null) {
            if(S::startsWith($baseHost, "www.", false)) {
                $baseHost = (string)S::substr($baseHost, 4);
            }

            $salesChannelName = $baseHost;
        }

        return $salesChannelDao->findOrCreate(
            new SalesChannelVO($salesChannelName)
        );
    }
}
