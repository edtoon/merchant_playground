<?php

namespace BlakePrinting\InkHeroApi\Transform;

use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Fields\FieldDAO;
use BlakePrinting\InkHeroApi\InkHeroAddressDAO;
use BlakePrinting\InkHeroApi\InkHeroAddressFieldStringDAO;
use BlakePrinting\InkHeroApi\InkHeroAddressTypeDAO;
use BlakePrinting\InkHeroApi\InkHeroAddressVO;
use BlakePrinting\InkHeroApi\InkHeroOrderAddressDAO;
use BlakePrinting\InkHeroApi\InkHeroOrderVO;
use PDO;
use ReflectionClass;

class TransformUtil {
    const FIELD_NAME_ADDITION = "addition";
    const FIELD_NAME_ADDRESS1 = "address1";
    const FIELD_NAME_ADDRESS2 = "address2";
    const FIELD_NAME_CITY = "city";
    const FIELD_NAME_COMPANY = "company";
    const FIELD_NAME_COUNTRY = "country";
    const FIELD_NAME_FIRST_NAME = "firstname";
    const FIELD_NAME_HOUSE_NUMBER = "housenumber";
    const FIELD_NAME_LAST_NAME = "lastname";
    const FIELD_NAME_POSTAL_CODE = "postalcode";
    const FIELD_NAME_REGION = "region";
    const FIELD_NAME_STREET = "street";
    const FIELD_NAME_TERRITORY = "territory";

    private function __construct() { }

    public static function &getInkHeroAddressVoByOrder(InkHeroOrderVO &$orderVo, $addressType = null, PDO &$connection = null) {
        $inkHeroAddressDao =& InkHeroAddressDAO::instance($connection);

        return $inkHeroAddressDao->findSingle(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$inkHeroAddressDao, &$orderVo, &$addressType, &$connection) {
                $inkHeroOrderAddressTbl = InkHeroOrderAddressDAO::instance($connection)->formatSchemaAndTableName();
                $inkHeroAddressTbl = $inkHeroAddressDao->formatSchemaAndTableName();

                $pdoQuery
                    ->columns($inkHeroAddressTbl . ".*")
                    ->join($inkHeroOrderAddressTbl, ["ink_hero_address.id = ink_hero_order_address.ink_hero_address_id"])
                    ->where("ink_hero_order_id", $orderVo->getId(), PDO::PARAM_INT)
                ;

                if($addressType !== null) {
                    $addressTypeTbl = InkHeroAddressTypeDAO::instance($connection)->formatSchemaAndTableName();

                    $pdoQuery
                        ->join($addressTypeTbl, [
                            $inkHeroOrderAddressTbl . ".ink_hero_address_type_id = " . $addressTypeTbl . ".id"
                        ])
                        ->where($addressTypeTbl . ".name", $addressType, PDO::PARAM_STR)
                    ;
                }
            }
        );
    }

    public static function &getAddressFieldValue($fieldName, InkHeroAddressVO &$inkHeroAddressVo, PDO &$connection = null) {
        $addressFieldTbl = InkHeroAddressFieldStringDAO::instance($connection)->formatSchemaAndTableName();
        $addressTbl = InkHeroAddressDAO::instance($connection)->formatSchemaAndTableName();
        $fieldTbl = FieldDAO::instance($connection)->formatSchemaAndTableName();
        $query =& PDOQuery::select()
            ->column("value")
            ->from($addressTbl)
            ->join($addressFieldTbl, [
                $addressTbl . ".id = " . $addressFieldTbl . ".ink_hero_address_id"
            ])
            ->join($fieldTbl, [
                $addressFieldTbl . ".field_id = " . $fieldTbl . ".id"
            ])
            ->where($addressTbl . ".id", $inkHeroAddressVo->getId(), PDO::PARAM_INT)
            ->where($fieldTbl . ".name", $fieldName, PDO::PARAM_STR)
        ;

        return $query->getSingleValue($connection);
    }
}
