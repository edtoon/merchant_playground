<?php

namespace BlakePrinting\InkHeroApi\Transform;

use BlakePrinting\Catalog\CatalogProductDAO;
use BlakePrinting\Catalog\CatalogProductDataFeedDAO;
use BlakePrinting\Catalog\CatalogProductDataFeedVO;
use BlakePrinting\Catalog\CatalogProductVO;
use BlakePrinting\DataSources\DataFeedVO;
use BlakePrinting\InkHeroApi\InkHeroItemVO;
use PDO;

class TransformCatalogUtil {
    private function __construct() { }

    /**
     * @param InkHeroItemVO $inkHeroItemVo
     * @param PDO $connection
     * @param DataFeedVO $dataFeedVo
     * @return CatalogProductVO
     */
    public static function &getCatalogProductByItem(InkHeroItemVO &$inkHeroItemVo, PDO &$connection = null, DataFeedVO $dataFeedVo = null) {
        $catalogProductDao =& CatalogProductDAO::instance($connection);
        $catalogProductVo =& $catalogProductDao->findOrCreate(
            new CatalogProductVO($inkHeroItemVo->getName())
        );

        if($dataFeedVo !== null) {
            $catalogProductDataFeedDao =& CatalogProductDataFeedDAO::instance($connection);

            $catalogProductDataFeedDao->store(
                new CatalogProductDataFeedVO($catalogProductVo->getId(), $dataFeedVo->getId())
            );
        }

        return $catalogProductVo;
    }
}
