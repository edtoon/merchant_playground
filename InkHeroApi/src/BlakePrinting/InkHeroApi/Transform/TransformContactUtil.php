<?php

namespace BlakePrinting\InkHeroApi\Transform;

use BlakePrinting\Contacts\ContactAddressVO;
use BlakePrinting\Contacts\ContactUtil;
use BlakePrinting\DataSources\DataFeedVO;
use BlakePrinting\Globalization\CountryVO;
use BlakePrinting\InkHeroApi\InkHeroAddressVO;
use BlakePrinting\Util\Utils;
use PDO;

class TransformContactUtil {
    private function __construct() { }

    /**
     * @param CountryVO $countryVo
     * @param PDO $connection
     * @param DataFeedVO $dataFeedVo
     * @return ContactAddressVO
     */
    public static function &getContactAddressVoByCountryAndAddress(CountryVO &$countryVo, InkHeroAddressVO &$inkHeroAddressVo, PDO &$connection = null, DataFeedVO $dataFeedVo = null) {
        $contactAddressVo = new ContactAddressVO();
        $countryIsoCode = $countryVo->getIsoCode();
        $countryFieldPrefix = $countryIsoCode . "_";

        $contactAddressVo->setCountryId($countryVo->getId());

        $contactAddressVo->setLine1(Utils::getTrimmedStringOrNull(
            TransformUtil::getAddressFieldValue($countryFieldPrefix . TransformUtil::FIELD_NAME_ADDRESS1, $inkHeroAddressVo, $connection)
        ));
        $contactAddressVo->setLine2(Utils::getTrimmedStringOrNull(
            TransformUtil::getAddressFieldValue($countryFieldPrefix . TransformUtil::FIELD_NAME_ADDRESS2, $inkHeroAddressVo, $connection)
        ));
        $contactAddressVo->setCity(Utils::getTrimmedStringOrNull(
            TransformUtil::getAddressFieldValue($countryFieldPrefix . TransformUtil::FIELD_NAME_CITY, $inkHeroAddressVo, $connection)
        ));
        $contactAddressVo->setStateOrRegion(Utils::getTrimmedStringOrNull(
            TransformUtil::getAddressFieldValue($countryFieldPrefix . TransformUtil::FIELD_NAME_REGION, $inkHeroAddressVo, $connection)
        ));
        $contactAddressVo->setPostalCode(Utils::getTrimmedStringOrNull(
            TransformUtil::getAddressFieldValue($countryFieldPrefix . TransformUtil::FIELD_NAME_POSTAL_CODE, $inkHeroAddressVo, $connection)
        ));

        $territory = TransformUtil::getAddressFieldValue($countryFieldPrefix . TransformUtil::FIELD_NAME_TERRITORY, $inkHeroAddressVo, $connection);

        if($territory !== null && "0" !== $territory) {
            $contactAddressVo->setDistrict($territory);
        }

        if($contactAddressVo->getLine1() === null) {
            $line1 = null;
            $street = Utils::getTrimmedStringOrNull(
                TransformUtil::getAddressFieldValue($countryFieldPrefix . TransformUtil::FIELD_NAME_STREET, $inkHeroAddressVo, $connection)
            );

            if($street !== null) {
                $line1 = $street;
            }

            $houseNumber = Utils::getTrimmedStringOrNull(
                TransformUtil::getAddressFieldValue($countryFieldPrefix . TransformUtil::FIELD_NAME_HOUSE_NUMBER, $inkHeroAddressVo, $connection)
            );

            if($houseNumber !== null) {
                if($line1 === null) {
                    $line1 = $houseNumber;
                } else {
                    $line1 .= " " . $houseNumber;
                }
            }

            $addition = Utils::getTrimmedStringOrNull(
                TransformUtil::getAddressFieldValue($countryFieldPrefix . TransformUtil::FIELD_NAME_ADDITION, $inkHeroAddressVo, $connection)
            );

            if($addition !== null) {
                if($line1 === null) {
                    $line1 = $addition;
                } else {
                    $line1 .= " " . $addition;
                }
            }

            $contactAddressVo->setLine1($line1);
        }

        return ContactUtil::getContactAddressVoByPrototype($contactAddressVo, $connection, $dataFeedVo);
    }
}
