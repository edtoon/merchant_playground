<?php

namespace BlakePrinting\InkHeroApi\Transform;

use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Fields\FieldDAO;
use BlakePrinting\Globalization\CurrencyDAO;
use BlakePrinting\Globalization\GlobalizationUtil;
use BlakePrinting\InkHeroApi\InkHeroAddressDAO;
use BlakePrinting\InkHeroApi\InkHeroAddressFieldStringDAO;
use BlakePrinting\InkHeroApi\InkHeroAddressTypeDAO;
use BlakePrinting\InkHeroApi\InkHeroOrderAddressDAO;
use BlakePrinting\InkHeroApi\InkHeroOrderVO;
use BlakePrinting\InkHeroApi\InkHeroSiteDAO;
use BlakePrinting\InkHeroApi\InkHeroSiteVO;
use BlakePrinting\Util\Utils;
use PDO;

class TransformGlobalizationUtil {
    private function __construct() { }

    public static function &getCountryVoByOrder(InkHeroOrderVO &$orderVo, $addressType = null, PDO &$connection = null) {
        $addressFieldTbl = InkHeroAddressFieldStringDAO::instance($connection)->formatSchemaAndTableName();
        $addressTbl = InkHeroAddressDAO::instance($connection)->formatSchemaAndTableName();
        $orderAddressTbl = InkHeroOrderAddressDAO::instance($connection)->formatSchemaAndTableName();
        $fieldTbl = FieldDAO::instance($connection)->formatSchemaAndTableName();
        $countryQuery = PDOQuery::select()
            ->column("value")
            ->from($addressFieldTbl)
            ->join($addressTbl, [
                $addressFieldTbl . ".ink_hero_address_id = " . $addressTbl . ".id"
            ])
            ->join($orderAddressTbl, [
                $addressTbl . ".id = " . $orderAddressTbl . ".ink_hero_address_id"
            ])
            ->join($fieldTbl, [
                $addressFieldTbl . ".field_id = " . $fieldTbl . ".id"
            ])
            ->where($orderAddressTbl . ".ink_hero_order_id", $orderVo->getId(), PDO::PARAM_INT)
            ->where($fieldTbl . ".name", TransformUtil::FIELD_NAME_COUNTRY, PDO::PARAM_STR)
        ;

        if($addressType !== null) {
            $addressTypeTbl = InkHeroAddressTypeDAO::instance($connection)->formatSchemaAndTableName();

            $countryQuery
                ->join($addressTypeTbl, [
                    $orderAddressTbl . ".ink_hero_address_type_id = " . $addressTypeTbl . ".id"
                ])
                ->where($addressTypeTbl . ".name", $addressType, PDO::PARAM_STR)
            ;
        }

        return GlobalizationUtil::getCountryVoByNameOrCode(
            $countryQuery->getSingleValue($connection)
        );
    }

    public static function &getCurrencyVoBySite(InkHeroSiteVo &$siteVo, PDO &$connection = null) {
        $siteReflectionClass =& Utils::getReflectionClassByName(InkHeroSiteDAO::class);
        $siteConstants = $siteReflectionClass->getConstants();
        $currencyDao =& CurrencyDAO::instance($connection);
        $currency = CurrencyDAO::CURRENCY_EUR;
        $organizationVo = null;

        foreach($siteConstants as $siteConstantName => &$siteConstantValue) {
            if($siteConstantValue["host"] === $siteVo->getHost()) {
                $currency =& $siteConstantValue["currency"];

                break;
            }
        }

        unset($siteConstantValue);

        return $currencyDao->findByColumn("iso_code", $currency[0], PDO::PARAM_STR, null, null, null, true);
    }
}
