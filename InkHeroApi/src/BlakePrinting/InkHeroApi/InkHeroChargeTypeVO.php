<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Db\ValueObject;

class InkHeroChargeTypeVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $inkHeroSiteId = null;
    /** @var int */
    private $type = null;
    /** @var string */
    private $description = null;

    public function __construct($inkHeroSiteId = null, $type = null, $description = null) {
        $this->inkHeroSiteId = $inkHeroSiteId;
        $this->type = $type;
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getInkHeroSiteId()
    {
        return $this->inkHeroSiteId;
    }

    /**
     * @param int $inkHeroSiteId
     */
    public function setInkHeroSiteId($inkHeroSiteId)
    {
        $this->inkHeroSiteId = $inkHeroSiteId;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
}
