<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static InkHeroOrderItemDAO instance(PDO $connection = null, $schemaName = null)
 * @method InkHeroOrderItemVO findById($id, $options = null, $useCache = false)
 * @method InkHeroOrderItemVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroOrderItemVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroOrderItemVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroOrderItemVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroOrderItemVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroOrderItemVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class InkHeroOrderItemDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
