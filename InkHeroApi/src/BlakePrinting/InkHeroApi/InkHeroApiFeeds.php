<?php

namespace BlakePrinting\InkHeroApi;

class InkHeroApiFeeds {
    const DATA_SOURCE_INK_HERO = "Ink Hero";
    const DATA_FEED_INK_HERO_WEB_ORDERS = ["source" => self::DATA_SOURCE_INK_HERO, "feed" => "Website Orders"];
}
