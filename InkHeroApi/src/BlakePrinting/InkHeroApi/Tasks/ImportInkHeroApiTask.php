<?php

namespace BlakePrinting\InkHeroApi\Tasks;

use BlakePrinting\Contacts\ContactAddressAddressTypeDAO;
use BlakePrinting\Contacts\ContactAddressTypeDAO;
use BlakePrinting\DataSources\DataFeedDAO;
use BlakePrinting\DataSources\DataSourceDAO;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Fields\FieldUtil;
use BlakePrinting\InkHeroApi\Extract\ExtractUtil;
use BlakePrinting\InkHeroApi\InkHeroAddressTypeDAO;
use BlakePrinting\InkHeroApi\InkHeroApiClient;
use BlakePrinting\InkHeroApi\InkHeroApiFeeds;
use BlakePrinting\InkHeroApi\InkHeroApiFields;
use BlakePrinting\InkHeroApi\InkHeroChargeTypeDAO;
use BlakePrinting\InkHeroApi\InkHeroItemDAO;
use BlakePrinting\InkHeroApi\InkHeroOrderDAO;
use BlakePrinting\InkHeroApi\InkHeroOrderItemDAO;
use BlakePrinting\InkHeroApi\InkHeroOrderItemVO;
use BlakePrinting\InkHeroApi\InkHeroOrderPriceDAO;
use BlakePrinting\InkHeroApi\InkHeroOrderVO;
use BlakePrinting\InkHeroApi\InkHeroPaymentMethodDAO;
use BlakePrinting\InkHeroApi\InkHeroSiteDAO;
use BlakePrinting\InkHeroApi\InkHeroSiteVO;
use BlakePrinting\InkHeroApi\Transform\TransformCatalogUtil;
use BlakePrinting\InkHeroApi\Transform\TransformContactUtil;
use BlakePrinting\InkHeroApi\Transform\TransformGlobalizationUtil;
use BlakePrinting\InkHeroApi\Transform\TransformOrganizationUtil;
use BlakePrinting\InkHeroApi\Transform\TransformSalesUtil;
use BlakePrinting\InkHeroApi\Transform\TransformUtil;
use BlakePrinting\Organizations\OrganizationTypeDAO;
use BlakePrinting\Organizations\OrganizationUtil;
use BlakePrinting\Sales\Load\SalesOrderLoader;
use BlakePrinting\Sales\Orders\SalesOrderFieldUnsignedDAO;
use BlakePrinting\Tasks\AbstractTask;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use PDO;
use ReflectionClass;

class ImportInkHeroApiTask extends AbstractTask {
    /** @var InkHeroSiteVO */
    private $inkHeroSiteVo = null;
    /** @var int */
    private $uncommittedCount = 0;

    public function execute() {
        $logger =& LogUtil::getLogger();

        $logger->debug("Importing Ink Hero API items");

        $connection =& $this->getConnection();
        $inkHeroSiteDao =& InkHeroSiteDAO::instance($connection);
        $inkHeroOrderDao =& InkHeroOrderDAO::instance($connection);

        Utils::forEachConstantInObjectClass(
            $inkHeroSiteDao,
            function($_, $constantName, $constantValue) use (&$logger, &$inkHeroSiteDao, &$inkHeroOrderDao, &$connection) {
                $this->inkHeroSiteVo =& $inkHeroSiteDao->findOrCreate(
                    new InkHeroSiteVO($constantValue["host"])
                );

                $host = $this->inkHeroSiteVo->getHost();
                $lastOrderTime =& PDOQuery::select()
                    ->column("last_update_date")
                    ->from($inkHeroOrderDao)
                    ->where("ink_hero_site_id", $this->inkHeroSiteVo->getId(), PDO::PARAM_INT)
                    ->orderBy("last_update_date DESC")
                    ->limit(1)
                    ->getSingleValue($connection)
                ;

                $logger->debug("Site: " . $host);

                $apiClient = new InkHeroApiClient($host, '#############', '##############', '#############', '############');

                Utils::forEachConstantInObjectClass(
                    $inkHeroOrderDao,
                    function($_, $constantName, $constantValue) use (&$apiClient, &$logger, &$connection, $lastOrderTime) {
                        if($constantValue === InkHeroOrderDAO::ORDER_STATUS_PENDING) {
                            return;
                        }

                        $logger->debug("Checking status: " . $constantValue);

                        $apiClient->stream(
                            "WebService", "Orders",
                            [
                                "lastupdatedate" => ($lastOrderTime === null ? "2016-01-01 00:00:00" : ($lastOrderTime - 1)),
                                "status" => $constantValue
                            ],
                            function($result) {
                                $this->importOrder($result);
                                $this->incrementAndCommit();
                            }
                        );

                        $this->incrementAndCommit(1);
                    },
                    "ORDER_STATUS_"
                );
            },
            "SITE_"
        );

        return 0;
    }

    private function incrementAndCommit($threshold = 50) {
        $this->uncommittedCount++;

        if($this->uncommittedCount >= $threshold) {
            PDOUtil::commitConnection($this->getConnection());

            $this->uncommittedCount = 0;
        }
    }

    public function importOrder(array &$orderData) {
        PDOUtil::pauseSqlLogging();

        $connection =& $this->getConnection();
        $extractResult =& ExtractUtil::extractOrder($orderData, $this->inkHeroSiteVo);
        $orderVo = $extractResult->getValueObject();

        if($orderVo !== null && $orderVo instanceof InkHeroOrderVO &&
            ($extractResult->getInserted() || $extractResult->getUpdated())
        ) {
            $dataSourceVo =& DataSourceDAO::instance($connection)->findByColumn(
                "name", InkHeroApiFeeds::DATA_SOURCE_INK_HERO, PDO::PARAM_STR, null, null, null, true
            );
            $dataFeedVo =& DataFeedDAO::instance($connection)->findSingle(
                function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$dataSourceVo) {
                    $pdoQuery
                        ->where("data_source_id", $dataSourceVo->getId(), PDO::PARAM_INT)
                        ->where("name", InkHeroApiFeeds::DATA_FEED_INK_HERO_WEB_ORDERS["feed"], PDO::PARAM_STR);
                }
            );
            $currencyVo =& TransformGlobalizationUtil::getCurrencyVoBySite($this->inkHeroSiteVo, $connection);
            $billingCountryVo =& TransformGlobalizationUtil::getCountryVoByOrder($orderVo, InkHeroAddressTypeDAO::ADDRESS_TYPE_BILLING, $connection);
            $shippingCountryVo =& TransformGlobalizationUtil::getCountryVoByOrder($orderVo, InkHeroAddressTypeDAO::ADDRESS_TYPE_SHIPPING, $connection);
            $inkHeroPaymentMethodVo = ($orderVo->getInkHeroPaymentMethodId() === null ? null :
                InkHeroPaymentMethodDAO::instance($connection)->findById($orderVo->getInkHeroPaymentMethodId(), null, true)
            );
            $paymentMethodVo = ($inkHeroPaymentMethodVo === null ? null : TransformSalesUtil::getPaymentMethodVoByCode($inkHeroPaymentMethodVo->getCode(), $connection));
            $fulfillmentStatusVo = TransformSalesUtil::getFulfillmentStatusVoByLastStatusId($orderVo->getLastStatusId(), $connection);
            $salesChannelVo =& TransformSalesUtil::getSalesChannelVoByInkHeroSiteVo($this->inkHeroSiteVo, $connection);
            $inkHeroShippingAddressVo =& TransformUtil::getInkHeroAddressVoByOrder($orderVo, InkHeroAddressTypeDAO::ADDRESS_TYPE_SHIPPING, $connection);
            $shippingAddressVo = ($inkHeroShippingAddressVo === null ? null : TransformContactUtil::getContactAddressVoByCountryAndAddress($shippingCountryVo, $inkHeroShippingAddressVo, $connection, $dataFeedVo));
            $shipToOrganizationVo = ($inkHeroShippingAddressVo === null ? null : OrganizationUtil::getOrganizationVoByCompanyNameAndType(
                TransformUtil::getAddressFieldValue(TransformUtil::FIELD_NAME_COMPANY, $inkHeroShippingAddressVo, $connection),
                OrganizationTypeDAO::ORGANIZATION_TYPE_CUSTOMER, $connection, $currencyVo, $shippingAddressVo
            ));
            $shipToFirstName = ($shippingAddressVo === null ? null : TransformUtil::getAddressFieldValue(TransformUtil::FIELD_NAME_FIRST_NAME, $inkHeroShippingAddressVo, $connection));
            $shipToLastName = ($shippingAddressVo === null ? null : TransformUtil::getAddressFieldValue(TransformUtil::FIELD_NAME_LAST_NAME, $inkHeroShippingAddressVo, $connection));
            $shipToName = Utils::getTrimmedStringOrNull(($shipToFirstName ?: "") . " " . ($shipToLastName ?: ""));
            $shippingContactPersonVo = ($shipToName === null ? null : OrganizationUtil::getContactPersonVoByName($shipToName, $connection, $dataFeedVo, $shippingAddressVo, $shipToOrganizationVo));
            $inkHeroBillingAddressVo =& TransformUtil::getInkHeroAddressVoByOrder($orderVo, InkHeroAddressTypeDAO::ADDRESS_TYPE_BILLING, $connection);
            $billingAddressVo = ($inkHeroBillingAddressVo === null ? null : TransformContactUtil::getContactAddressVoByCountryAndAddress($billingCountryVo, $inkHeroBillingAddressVo, $connection, $dataFeedVo));
            $billToOrganizationVo = ($inkHeroBillingAddressVo === null ? null : OrganizationUtil::getOrganizationVoByCompanyNameAndType(
                TransformUtil::getAddressFieldValue(TransformUtil::FIELD_NAME_COMPANY, $inkHeroBillingAddressVo, $connection),
                OrganizationTypeDAO::ORGANIZATION_TYPE_CUSTOMER, $connection, $currencyVo, $billingAddressVo
            ));
            $billToFirstName = ($billingAddressVo === null ? null : TransformUtil::getAddressFieldValue(TransformUtil::FIELD_NAME_FIRST_NAME, $inkHeroBillingAddressVo, $connection));
            $billToLastName = ($billingAddressVo === null ? null : TransformUtil::getAddressFieldValue(TransformUtil::FIELD_NAME_LAST_NAME, $inkHeroBillingAddressVo, $connection));
            $billToName = Utils::getTrimmedStringOrNull(($billToFirstName ?: "") . " " . ($billToLastName ?: ""));
            $billingContactPersonVo = ($billToName === null ? null : OrganizationUtil::getContactPersonVoByName($billToName, $connection, $dataFeedVo, $billingAddressVo, $billToOrganizationVo));
            $internalOrganizationVo =& TransformOrganizationUtil::getInternalOrganizationVoBySite($this->inkHeroSiteVo, $connection);
            $inkHeroOrderPriceDao =& InkHeroOrderPriceDAO::instance($connection);
            $priceSubtotal = $inkHeroOrderPriceDao->getPriceSumByChargeType($orderVo, InkHeroChargeTypeDAO::TYPE_SUBTOTAL);
            $priceShipping = $inkHeroOrderPriceDao->getPriceSumByChargeType($orderVo, InkHeroChargeTypeDAO::TYPE_SHIPPING);
            $priceTax = $inkHeroOrderPriceDao->getPriceSumByChargeType($orderVo, InkHeroChargeTypeDAO::TYPE_TAX);
            $priceTotalNoTax = $inkHeroOrderPriceDao->getPriceSumByChargeType($orderVo, InkHeroChargeTypeDAO::TYPE_TOTAL_WO_TAX);
            $priceTotal = $inkHeroOrderPriceDao->getPriceSumByChargeType($orderVo, InkHeroChargeTypeDAO::TYPE_TOTAL);
            $orderIdFieldSpec = InkHeroApiFields::FIELD_ORDER_ID;

            $orderIdFieldSpec["schema"] = $this->inkHeroSiteVo->getHost();

            $orderIdFieldVo =& FieldUtil::findFieldVoBySpec($orderIdFieldSpec, $connection, true);
            $orderId = $orderVo->getId();
            $existingSalesOrderId =& PDOQuery::select()
                ->column("sales_order_id")
                ->from(SalesOrderFieldUnsignedDAO::instance($connection))
                ->where("field_id", $orderIdFieldVo->getId())
                ->where("value", $orderId, PDO::PARAM_INT)
                ->getSingleValue($connection)
            ;
            $loader = new SalesOrderLoader($connection);

            $loader->setId($existingSalesOrderId);
            $loader->setSellerOrganizationVo($internalOrganizationVo);
            $loader->setPurchaserOrganizationVo($billToOrganizationVo ?: $shipToOrganizationVo);
            $loader->setSalesChannelVo($salesChannelVo);
            $loader->setPurchaserCurrencyVo($currencyVo);
            $loader->setSaleCurrencyVo($currencyVo);
            $loader->setPaymentMethodVo($paymentMethodVo);
            $loader->setFulfillmentStatusVo($fulfillmentStatusVo);
            $loader->addUnsignedFieldValue($orderIdFieldVo->getId(), $orderId);

            if($shippingContactPersonVo !== null) {
                $loader->addContactPerson($shippingContactPersonVo);
            }

            if($shippingAddressVo !== null) {
                $shippingAddressTypeVo =& ContactAddressTypeDAO::instance($connection)->findByColumn(
                    "name", ContactAddressTypeDAO::ADDRESS_TYPE_SHIPPING, PDO::PARAM_STR, null, null, null, true
                );

                ContactAddressAddressTypeDAO::instance($connection)->store(
                    $shippingAddressVo->getId(), $shippingAddressTypeVo->getId()
                );

                $loader->addContactAddress($shippingAddressVo);
            }

            if($billingContactPersonVo !== null) {
                $loader->addContactPerson($billingContactPersonVo);
            }

            if($billingAddressVo !== null) {
                $loader->addContactAddress($billingAddressVo);
            }

            if($dataFeedVo !== null) {
                $loader->addDataFeed($dataFeedVo);
            }

            $itemDao =& InkHeroItemDAO::instance($connection);
            $orderItemDao =& InkHeroOrderItemDAO::instance($connection);
            $orderItemIterator =& $orderItemDao->iterate(
                function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$orderId) {
                    $pdoQuery
                        ->where("ink_hero_order_id", $orderId, PDO::PARAM_INT)
                    ;
                }
            );

            foreach($orderItemIterator as $orderItemVo) {
                if($orderItemVo instanceof InkHeroOrderItemVO) {
                    $itemVo =& $itemDao->findById($orderItemVo->getInkHeroItemId(), null, true);

                    $loader->addCatalogProduct(TransformCatalogUtil::getCatalogProductByItem($itemVo), $orderItemVo->getQuantity());
                }
            }

            $loader->loadSalesOrder(
                null, $orderVo->getOrderDate(), "1",
                $priceShipping,
                $priceSubtotal,
                ($priceTotal !== null ? $priceTotal : ($priceTotalNoTax === null ? null :
                    Utils::moneyCentsToString(
                        Utils::moneyStringToCents($priceSubtotal) +
                        Utils::moneyStringToCents($priceShipping ?: "0") +
                        Utils::moneyStringToCents($priceTax ?: "0")
                    )
                )),
                null
            );

            $logger =& LogUtil::getLogger();

            $logger->debug(
                "Feed: " . ($dataFeedVo === null ? "null" : $dataFeedVo->getName()) . ", " .
                "Channel: " . ($salesChannelVo === null ? "null" : $salesChannelVo->getName()) . ", " .
                "Biller: " . ($paymentMethodVo === null ? "null" : $paymentMethodVo->getName()) . ", " .
                "Currency: " . ($currencyVo === null ? "null" : $currencyVo->getIsoCode()) . ", " .
                "Billed in: " . ($billingCountryVo === null ? "null" : $billingCountryVo->getName()) . ", " .
                "Shipped to: " . ($shippingCountryVo === null ? "null" : $shippingCountryVo->getName()) . ", " .
                "Org: " . ($internalOrganizationVo === null ? "null" : $internalOrganizationVo->getName()) . ", " .
                "Order #" . $existingSalesOrderId
            );
        }

        PDOUtil::resumeSqlLogging();
    }
}
