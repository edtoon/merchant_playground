<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Db\ValueObject;

class InkHeroOrderVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $inkHeroSiteId = null;
    /** @var int */
    private $inkHeroPaymentMethodId = null;
    /** @var int */
    private $inkHeroShippingMethodId = null;
    /** @var int */
    private $inkHeroTaxTypeId = null;
    /** @var int */
    private $orderDate = null;
    /** @var string */
    private $orderNumber = null;
    /** @var int */
    private $customerId = null;
    /** @var string */
    private $ipv4 = null;
    /** @var string */
    private $sessionId = null;
    /** @var string */
    private $language = null;
    /** @var string */
    private $comments = null;
    /** @var bool */
    private $confirmed = null;
    /** @var int */
    private $lastStatusId = null;
    /** @var int */
    private $lastUpdateDate = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getInkHeroSiteId()
    {
        return $this->inkHeroSiteId;
    }

    /**
     * @param int $inkHeroSiteId
     */
    public function setInkHeroSiteId($inkHeroSiteId)
    {
        $this->inkHeroSiteId = $inkHeroSiteId;
    }

    /**
     * @return int
     */
    public function getInkHeroPaymentMethodId()
    {
        return $this->inkHeroPaymentMethodId;
    }

    /**
     * @param int $inkHeroPaymentMethodId
     */
    public function setInkHeroPaymentMethodId($inkHeroPaymentMethodId)
    {
        $this->inkHeroPaymentMethodId = $inkHeroPaymentMethodId;
    }

    /**
     * @return int
     */
    public function getInkHeroShippingMethodId()
    {
        return $this->inkHeroShippingMethodId;
    }

    /**
     * @param int $inkHeroShippingMethodId
     */
    public function setInkHeroShippingMethodId($inkHeroShippingMethodId)
    {
        $this->inkHeroShippingMethodId = $inkHeroShippingMethodId;
    }

    /**
     * @return int
     */
    public function getInkHeroTaxTypeId()
    {
        return $this->inkHeroTaxTypeId;
    }

    /**
     * @param int $inkHeroTaxTypeId
     */
    public function setInkHeroTaxTypeId($inkHeroTaxTypeId)
    {
        $this->inkHeroTaxTypeId = $inkHeroTaxTypeId;
    }

    /**
     * @return int
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * @param int $orderDate
     */
    public function setOrderDate($orderDate)
    {
        $this->orderDate = $orderDate;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param int $customerId
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    /**
     * @return string
     */
    public function getIpv4()
    {
        return $this->ipv4;
    }

    /**
     * @param string $ipv4
     */
    public function setIpv4($ipv4)
    {
        $this->ipv4 = $ipv4;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param string $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    /**
     * @return boolean
     */
    public function isConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * @param boolean $confirmed
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;
    }

    /**
     * @return int
     */
    public function getLastStatusId()
    {
        return $this->lastStatusId;
    }

    /**
     * @param int $lastStatusId
     */
    public function setLastStatusId($lastStatusId)
    {
        $this->lastStatusId = $lastStatusId;
    }

    /**
     * @return int
     */
    public function getLastUpdateDate()
    {
        return $this->lastUpdateDate;
    }

    /**
     * @param int $lastUpdateDate
     */
    public function setLastUpdateDate($lastUpdateDate)
    {
        $this->lastUpdateDate = $lastUpdateDate;
    }
}
