<?php

namespace BlakePrinting\InkHeroApi\CLI;

use BlakePrinting\CLI\CLICommand;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\InkHeroApi\Tasks\ImportInkHeroApiTask;
use BlakePrinting\Util\LogUtil;
use Exception;
use Monolog\Logger;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetOrdersCommand extends CLICommand {
    protected function configure() {
        $this
            ->setName("inkhero:api:get-orders")
            ->setDescription("Get orders")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $connection =& PDOUtil::getMysqlConnection();
        $task = new ImportInkHeroApiTask(self::class, self::class, "1", 60, $connection, 0);
        $exitCode = 1;

        try {
            if(!$task->lock()) {
                $output->writeln("Failed to lock task");
            } else {
                try {
                    $exitCode = $task->run();
                } finally {
                    $task->unlock();
                }

                if($exitCode === 0) {
                    PDOUtil::commitConnection($connection);

                    return true;
                }
            }
        } catch(Exception $e) {
            LogUtil::logException(Logger::ERROR, $e, "Error importing orders");
        } finally {
            PDOUtil::rollbackConnection($connection);
        }

        $output->writeln("Import failed with error #" . $exitCode);

        return false;
    }
}
