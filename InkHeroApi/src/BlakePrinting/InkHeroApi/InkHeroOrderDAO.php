<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static InkHeroOrderDAO instance(PDO $connection = null, $schemaName = null)
 * @method InkHeroOrderVO findById($id, $options = null, $useCache = false)
 * @method InkHeroOrderVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroOrderVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroOrderVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroOrderVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroOrderVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroOrderVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroOrderVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class InkHeroOrderDAO extends AbstractDataAccessObject {
    const ORDER_STATUS_PENDING = 1;
    const ORDER_STATUS_PROCESSING = 2;
    const ORDER_STATUS_SHIPPED = 3;
    const ORDER_STATUS_PARTIAL = 4;
    const ORDER_STATUS_DELIVERED = 5;
    const ORDER_STATUS_CANCELLED = 6;
    const ORDER_STATUS_BACKORDERED = 7;
}
