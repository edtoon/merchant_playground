<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Fields\AbstractEntityFieldStringVO;

class InkHeroAddressFieldStringVO extends AbstractEntityFieldStringVO {
    /** @var int */
    private $inkHeroAddressId = null;

    /**
     * @return int
     */
    public function getInkHeroAddressId()
    {
        return $this->inkHeroAddressId;
    }

    /**
     * @param int $inkHeroAddressId
     */
    public function setInkHeroAddressId($inkHeroAddressId)
    {
        $this->inkHeroAddressId = $inkHeroAddressId;
    }
}
