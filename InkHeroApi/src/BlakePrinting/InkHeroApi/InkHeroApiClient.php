<?php

namespace BlakePrinting\InkHeroApi;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\RequestOptions;
use JsonCollectionParser\Parser;

class InkHeroApiClient {
    /** @var string */
    private $host = null;
    /** @var string */
    private $hostKey = null;
    /** @var string */
    private $developerId = null;
    /** @var string */
    private $developerPassword = null;
    /** @var string */
    private $clientKey = null;
    /** @var TokenKey */
    private $tokenKey = null;

    function __construct($host, $hostKey, $developerId, $developerPassword, $clientKey) {
        $this->host = $host;
        $this->hostKey = $hostKey;
        $this->developerId = $developerId;
        $this->developerPassword = $developerPassword;
        $this->clientKey = $clientKey;
        $this->tokenKey = new TokenKey();
    }

    /** ################################# */
    /** ################################# */
    /** ################################# */
    /** Picture a super pretty API client */
    /**                ...                */
    /**     That's not what was here      */
    /** ################################# */
    /** ################################# */
    /** ################################# */
}
