<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Fields\FieldTypeDAO;

class InkHeroApiFields {
    const DOMAIN = "ink_hero";
    const TABLE_ADDRESSES = "addresses";
    const TABLE_ORDERS = "orders";
    const FIELD_ADDRESS_ID = [
        "domain" => self::DOMAIN,
        "table" => self::TABLE_ADDRESSES, "field" => "address_id", "type" => FieldTypeDAO::TYPE_SIGNED
    ];
    const FIELD_ORDER_ID = [
        "domain" => self::DOMAIN,
        "table" => self::TABLE_ORDERS, "field" => "order_id", "type" => FieldTypeDAO::TYPE_UNSIGNED
    ];

    private function __construct() { }
}
