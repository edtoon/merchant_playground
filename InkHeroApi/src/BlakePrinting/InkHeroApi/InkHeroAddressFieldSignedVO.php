<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Fields\AbstractEntityFieldIntVO;

class InkHeroAddressFieldSignedVO extends AbstractEntityFieldIntVO {
    /** @var int */
    private $inkHeroAddressId = null;

    /**
     * @return int
     */
    public function getInkHeroAddressId()
    {
        return $this->inkHeroAddressId;
    }

    /**
     * @param int $inkHeroAddressId
     */
    public function setInkHeroAddressId($inkHeroAddressId)
    {
        $this->inkHeroAddressId = $inkHeroAddressId;
    }
}
