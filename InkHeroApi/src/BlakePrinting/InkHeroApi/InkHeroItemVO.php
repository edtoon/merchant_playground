<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Db\ValueObject;

class InkHeroItemVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $inkHeroSiteId = null;
    /** @var int */
    private $itemId = null;
    /** @var int */
    private $name = null;

    public function __construct($inkHeroSiteId = null, $itemId = null, $name = null) {
        $this->inkHeroSiteId = $inkHeroSiteId;
        $this->itemId = $itemId;
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getInkHeroSiteId()
    {
        return $this->inkHeroSiteId;
    }

    /**
     * @param int $inkHeroSiteId
     */
    public function setInkHeroSiteId($inkHeroSiteId)
    {
        $this->inkHeroSiteId = $inkHeroSiteId;
    }

    /**
     * @return int
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * @param int $itemId
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;
    }

    /**
     * @return int
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param int $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
