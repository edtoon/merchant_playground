<?php

namespace BlakePrinting\InkHeroApi;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static InkHeroAddressTypeDAO instance(PDO $connection = null, $schemaName = null)
 * @method InkHeroAddressTypeVO findById($id, $options = null, $useCache = false)
 * @method InkHeroAddressTypeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroAddressTypeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroAddressTypeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroAddressTypeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method InkHeroAddressTypeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method InkHeroAddressTypeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method InkHeroAddressTypeVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class InkHeroAddressTypeDAO extends AbstractDataAccessObject {
    const ADDRESS_TYPE_BILLING = "billing_info";
    const ADDRESS_TYPE_SHIPPING = "shipping_info";
}
