<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\Fields\FieldDAO;
use BlakePrinting\InkHeroApi\InkHeroAddressDAO;
use BlakePrinting\InkHeroApi\InkHeroAddressFieldSignedDAO;
use BlakePrinting\InkHeroApi\InkHeroAddressFieldStringDAO;
use BlakePrinting\InkHeroApi\InkHeroAddressTypeDAO;
use BlakePrinting\InkHeroApi\InkHeroChargeTypeDAO;
use BlakePrinting\InkHeroApi\InkHeroItemDAO;
use BlakePrinting\InkHeroApi\InkHeroOrderAddressDAO;
use BlakePrinting\InkHeroApi\InkHeroOrderDAO;
use BlakePrinting\InkHeroApi\InkHeroOrderItemDAO;
use BlakePrinting\InkHeroApi\InkHeroOrderPriceDAO;
use BlakePrinting\InkHeroApi\InkHeroPaymentMethodDAO;
use BlakePrinting\InkHeroApi\InkHeroShippingMethodDAO;
use BlakePrinting\InkHeroApi\InkHeroSiteDAO;
use BlakePrinting\InkHeroApi\InkHeroTaxTypeDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class InkHeroApi extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();
        $fieldDao =& FieldDAO::instance($connection);
        $fieldTbl = $fieldDao->formatSchemaAndTableName();

        $siteDao =& InkHeroSiteDAO::instance($connection);
        $siteTbl = $siteDao->formatSchemaAndTableName();
        $this->tableDao($siteDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("host", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("host", ["unique" => true])
            ->save()
        ;

        $itemDao =& InkHeroItemDAO::instance($connection);
        $itemTbl = $itemDao->formatSchemaAndTableName();
        $this->tableDao($itemDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("ink_hero_site_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("item_id", "integer", ["null" => true])
            ->addColumn("name", "text", ["null" => true])
            ->addForeignKey("ink_hero_site_id", $siteTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("item_id")
            ->save()
        ;
        $this->alterDao($itemDao, "ADD INDEX (name(191))");

        $paymentMethodDao =& InkHeroPaymentMethodDAO::instance($connection);
        $paymentMethodTbl = $paymentMethodDao->formatSchemaAndTableName();
        $this->tableDao($paymentMethodDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("code", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("code", ["unique" => true])
            ->save()
        ;

        $shippingMethodDao =& InkHeroShippingMethodDAO::instance($connection);
        $shippingMethodTbl = $shippingMethodDao->formatSchemaAndTableName();
        $this->tableDao($shippingMethodDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("code", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("code", ["unique" => true])
            ->save()
        ;

        $taxTypeDao =& InkHeroTaxTypeDAO::instance($connection);
        $taxTypeTbl = $taxTypeDao->formatSchemaAndTableName();
        $this->tableDao($taxTypeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("code", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("code", ["unique" => true])
            ->save()
        ;

        $chargeTypeDao =& InkHeroChargeTypeDAO::instance($connection);
        $chargeTypeTbl = $chargeTypeDao->formatSchemaAndTableName();
        $this->tableDao($chargeTypeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("ink_hero_site_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("type", "string", ["limit" => 191, "null" => true])
            ->addColumn("description", "string", ["limit" => 191, "null" => true])
            ->addForeignKey("ink_hero_site_id", $siteTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("type")
            ->addIndex("description")
            ->save()
        ;

        $addressTypeDao =& InkHeroAddressTypeDAO::instance($connection);
        $addressTypeTbl = $addressTypeDao->formatSchemaAndTableName();
        $this->tableDao($addressTypeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $addressDao =& InkHeroAddressDAO::instance($connection);
        $addressTbl = $addressDao->formatSchemaAndTableName();
        $this->tableDao($addressDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("line1", "text", ["null" => true])
            ->addColumn("line2", "text", ["null" => true])
            ->addColumn("line3", "text", ["null" => true])
            ->addColumn("line4", "text", ["null" => true])
            ->addColumn("line5", "text", ["null" => true])
            ->addColumn("line6", "text", ["null" => true])
            ->addColumn("line7", "text", ["null" => true])
            ->addColumn("line8", "text", ["null" => true])
            ->addColumn("line9", "text", ["null" => true])
            ->addColumn("line10", "text", ["null" => true])
            ->addIndex("created")
            ->addIndex("updated")
            ->save()
        ;
        $this->alterDao($addressDao, "ADD INDEX (line1(191))");
        $this->alterDao($addressDao, "ADD INDEX (line2(191))");
        $this->alterDao($addressDao, "ADD INDEX (line3(191))");
        $this->alterDao($addressDao, "ADD INDEX (line4(191))");
        $this->alterDao($addressDao, "ADD INDEX (line5(191))");
        $this->alterDao($addressDao, "ADD INDEX (line6(191))");
        $this->alterDao($addressDao, "ADD INDEX (line7(191))");
        $this->alterDao($addressDao, "ADD INDEX (line8(191))");
        $this->alterDao($addressDao, "ADD INDEX (line9(191))");
        $this->alterDao($addressDao, "ADD INDEX (line10(191))");

        $orderDao =& InkHeroOrderDAO::instance($connection);
        $orderTbl = $orderDao->formatSchemaAndTableName();
        $this->tableDao($orderDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("ink_hero_site_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("ink_hero_payment_method_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY, "null" => true])
            ->addColumn("ink_hero_shipping_method_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY, "null" => true])
            ->addColumn("ink_hero_tax_type_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY, "null" => true])
            ->addColumn("order_date", "biginteger", ["signed" => false])
            ->addColumn("order_number", "string", ["limit" => 191])
            ->addColumn("customer_id", "integer", ["null" => true])
            ->addColumn("session_id", "string", ["limit" => 191, "null" => true])
            ->addColumn("language", "string", ["limit" => 191, "null" => true])
            ->addColumn("comments", "text", ["null" => true])
            ->addColumn("confirmed", "boolean", ["null" => true])
            ->addColumn("last_status_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY, "null" => true])
            ->addColumn("last_update_date", "biginteger", ["signed" => false, "null" => true])
            ->addForeignKey("ink_hero_site_id", $siteTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("ink_hero_payment_method_id", $paymentMethodTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("ink_hero_shipping_method_id", $shippingMethodTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("ink_hero_tax_type_id", $taxTypeTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("order_date")
            ->addIndex("order_number")
            ->addIndex("customer_id")
            ->addIndex("session_id")
            ->addIndex("language")
            ->addIndex("confirmed")
            ->addIndex("last_status_id")
            ->addIndex("last_update_date")
            ->save()
        ;
        $this->alterDao($orderDao, "ADD ipv4 BINARY(4), ADD KEY (ipv4)");
        $this->alterDao($orderDao, "ADD INDEX (comments(191))");

        $orderItemDao =& InkHeroOrderItemDAO::instance($connection);
        $this->tableDao($orderItemDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["ink_hero_order_id", "ink_hero_item_id"]])
            ->addColumn("ink_hero_order_id", "integer", ["signed" => false])
            ->addColumn("ink_hero_item_id", "integer", ["signed" => false])
            ->addColumn("quantity", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addColumn("price", "decimal", ["null" => true, "precision" => 19, "scale" => 4])
            ->addForeignKey("ink_hero_order_id", $orderTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("ink_hero_item_id", $itemTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("quantity")
            ->addIndex("price")
            ->save()
        ;

        $orderPriceDao =& InkHeroOrderPriceDAO::instance($connection);
        $this->tableDao($orderPriceDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["ink_hero_order_id", "ink_hero_charge_type_id"]])
            ->addColumn("ink_hero_order_id", "integer", ["signed" => false])
            ->addColumn("ink_hero_charge_type_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("amount", "decimal", ["null" => true, "precision" => 19, "scale" => 4])
            ->addForeignKey("ink_hero_order_id", $orderTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("ink_hero_charge_type_id", $chargeTypeTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("amount")
            ->save()
        ;

        $orderAddressDao =& InkHeroOrderAddressDAO::instance($connection);
        $this->tableDao($orderAddressDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["ink_hero_order_id", "ink_hero_address_type_id", "ink_hero_address_id"]])
            ->addColumn("ink_hero_order_id", "integer", ["signed" => false])
            ->addColumn("ink_hero_address_type_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("ink_hero_address_id", "integer", ["signed" => false])
            ->addForeignKey("ink_hero_order_id", $orderTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("ink_hero_address_type_id", $addressTypeTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("ink_hero_address_id", $addressTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $addressFieldSignedDao =& InkHeroAddressFieldSignedDAO::instance($connection);
        $this->tableDao($addressFieldSignedDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("ink_hero_address_id", "integer", ["signed" => false])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "biginteger", ["null" => true])
            ->addForeignKey("ink_hero_address_id", $addressTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $addressFieldStringDao =& InkHeroAddressFieldStringDAO::instance($connection);
        $this->tableDao($addressFieldStringDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("ink_hero_address_id", "integer", ["signed" => false])
            ->addColumn("field_id", "integer", ["signed" => false])
            ->addColumn("value", "text", ["null" => true])
            ->addForeignKey("ink_hero_address_id", $addressTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("field_id", $fieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(InkHeroAddressFieldStringDAO::instance($connection));
        $this->dropDao(InkHeroAddressFieldSignedDAO::instance($connection));
        $this->dropDao(InkHeroOrderAddressDAO::instance($connection));
        $this->dropDao(InkHeroOrderPriceDAO::instance($connection));
        $this->dropDao(InkHeroOrderItemDAO::instance($connection));
        $this->dropDao(InkHeroOrderDAO::instance($connection));
        $this->dropDao(InkHeroAddressDAO::instance($connection));
        $this->dropDao(InkHeroAddressTypeDAO::instance($connection));
        $this->dropDao(InkHeroChargeTypeDAO::instance($connection));
        $this->dropDao(InkHeroTaxTypeDAO::instance($connection));
        $this->dropDao(InkHeroShippingMethodDAO::instance($connection));
        $this->dropDao(InkHeroPaymentMethodDAO::instance($connection));
        $this->dropDao(InkHeroItemDAO::instance($connection));
        $this->dropDao(InkHeroSiteDAO::instance($connection));
    }
}
