<?php

require_once(__DIR__ . "/../vendor/autoload.php");

$app = new BlakePrinting\InkHeroApi\CLI\Main;
$app->run();
