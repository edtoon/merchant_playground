<?php

use BlakePrinting\DataSources\DataFeedDAO;
use BlakePrinting\DataSources\DataFeedVO;
use BlakePrinting\DataSources\DataSourceDAO;
use BlakePrinting\DataSources\DataSourceVO;
use BlakePrinting\Db\DbSeed;
use BlakePrinting\InkHeroApi\InkHeroAddressTypeDAO;
use BlakePrinting\InkHeroApi\InkHeroAddressTypeVO;
use BlakePrinting\InkHeroApi\InkHeroApiFeeds;
use BlakePrinting\InkHeroApi\InkHeroSiteDAO;
use BlakePrinting\InkHeroApi\InkHeroSiteVO;
use BlakePrinting\Util\Utils;

class InkHeroApiSchemaSeeder extends DbSeed {
    public function run() {
        $connection =& $this->getConnection();
        $siteDao =& InkHeroSiteDAO::instance($connection);
        $addressTypeDao =& InkHeroAddressTypeDAO::instance($connection);
        $dataSourceDao =& DataSourceDAO::instance($connection);
        $dataFeedDao =& DataFeedDAO::instance($connection);

        Utils::forEachConstantInObjectClass(
            $siteDao,
            function($_, $constantName, $constantValue) use (&$siteDao) {
                $siteDao->findOrCreate(new InkHeroSiteVO($constantValue["host"]));
            }, "SITE_"
        );

        Utils::forEachConstantInObjectClass(
            $addressTypeDao,
            function($_, $constantName, $constantValue) use (&$addressTypeDao) {
                $addressTypeDao->findOrCreate(new InkHeroAddressTypeVO($constantValue));
            }, "ADDRESS_TYPE_"
        );

        Utils::forEachConstantInObjectClass(
            InkHeroApiFeeds::class,
            function($_, $constantName, $constantValue) use (&$dataSourceDao) {
                $dataSourceDao->findOrCreate(new DataSourceVO($constantValue));
            }, "DATA_SOURCE_"
        );

        Utils::forEachConstantInObjectClass(
            InkHeroApiFeeds::class,
            function($_, $constantName, $constantValue) use (&$dataSourceDao, &$dataFeedDao) {
                $feedSpec =& $constantValue;
                $dataSourceVo =& $dataSourceDao->findOrCreate(new DataSourceVO($feedSpec["source"]));

                $dataFeedDao->findOrCreate(new DataFeedVO(
                    $dataSourceVo->getId(), $feedSpec["feed"]
                ));
            }, "DATA_FEED_"
        );
    }
}
