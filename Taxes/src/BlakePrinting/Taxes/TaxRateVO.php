<?php

namespace BlakePrinting\Taxes;

use BlakePrinting\Db\ValueObject;

class TaxRateVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $creator = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $updater = null;
    /** @var int */
    private $taxId = null;
    /** @var string */
    private $percentage = null;
    /** @var bool */
    private $variable = null;
    /** @var bool */
    private $includeInPrice = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param int $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getUpdater()
    {
        return $this->updater;
    }

    /**
     * @param int $updater
     */
    public function setUpdater($updater)
    {
        $this->updater = $updater;
    }

    /**
     * @return int
     */
    public function getTaxId()
    {
        return $this->taxId;
    }

    /**
     * @param int $taxId
     */
    public function setTaxId($taxId)
    {
        $this->taxId = $taxId;
    }

    /**
     * @return string
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * @param string $percentage
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;
    }

    /**
     * @return boolean
     */
    public function isVariable()
    {
        return $this->variable;
    }

    /**
     * @param boolean $variable
     */
    public function setVariable($variable)
    {
        $this->variable = $variable;
    }

    /**
     * @return boolean
     */
    public function isIncludeInPrice()
    {
        return $this->includeInPrice;
    }

    /**
     * @param boolean $includeInPrice
     */
    public function setIncludeInPrice($includeInPrice)
    {
        $this->includeInPrice = $includeInPrice;
    }
}
