<?php

namespace BlakePrinting\Taxes;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static TaxTagDAO instance(PDO $connection = null, $schemaName = null)
 * @method TaxTagVO findById($id, $options = null, $useCache = false)
 * @method TaxTagVO findByColumn($column, $value, $pdoTag = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaxTagVO findByProtoTag(ValueObject $protoTag, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaxTagVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaxTagVO[] findAllByColumn($column, $value, $pdoTag = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaxTagVO[] findAllByProtoTag(ValueObject $protoTag, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaxTagVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaxTagVO findOrCreate(ValueObject $protoTag, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class TaxTagDAO extends AbstractDataAccessObject {
}
