<?php

namespace BlakePrinting\Taxes;

use BlakePrinting\Db\ValueObject;

class TaxTaxTagVO implements ValueObject {
    /** @var int */
    private $taxId = null;
    /** @var int */
    private $taxTagId = null;

    public function __construct($taxId = null, $taxTagId = null) {
        $this->taxId = $taxId;
        $this->taxTagId = $taxTagId;
    }

    /**
     * @return int
     */
    public function getTaxId()
    {
        return $this->taxId;
    }

    /**
     * @param int $taxId
     */
    public function setTaxId($taxId)
    {
        $this->taxId = $taxId;
    }

    /**
     * @return int
     */
    public function getTaxTagId()
    {
        return $this->taxTagId;
    }

    /**
     * @param int $taxTagId
     */
    public function setTaxTagId($taxTagId)
    {
        $this->taxTagId = $taxTagId;
    }
}
