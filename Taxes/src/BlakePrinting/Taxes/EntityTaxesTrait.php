<?php

namespace BlakePrinting\Taxes;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\AbstractExtensibleEntity;
use BlakePrinting\Db\PDOQuery;
use Exception;
use Stringy\StaticStringy as S;

trait EntityTaxesTrait {
    /** @var AbstractEntityTaxVO[] */
    private $taxes = [];
    /** @var AbstractDataAccessObject */
    private $entityTaxDao = null;

    public function replaceTaxes() {
        if(!$this instanceof AbstractExtensibleEntity) {
            throw new Exception("Tried to replace taxes of object that's not an extensible entity: " . get_class($this));
        } else {
            $entityColumnName = $this->getEntityColumnName();
            $entityColumnValue = $this->getEntityColumnValue();

            if($this->entityTaxDao === null) {
                $this->entityTaxDao = $this->getExtensionDAO("Tax");
            }

            PDOQuery::delete()
                ->from($this->entityTaxDao)
                ->where($entityColumnName, $entityColumnValue, $this->getEntityColumnType())
                ->executeGetRowCount($this->entityTaxDao->getConnection())
            ;

            if(!empty($this->taxes)) {
                $voReflectionClass = $this->entityTaxDao->getVoReflectionClass();
                $setEntityMethod = $voReflectionClass->getMethod("set" . S::upperCamelize($entityColumnName));

                foreach($this->taxes as &$entityTaxVo) {
                    $setEntityMethod->invoke($entityTaxVo, $entityColumnValue);

                    $this->entityTaxDao->insert($entityTaxVo);
                }

                unset($entityTaxVo);
            }
        }
    }

    /**
     * @param AbstractEntityTaxVO $entityTaxVo
     */
    public function addTax(AbstractEntityTaxVO $entityTaxVo) {
        $this->taxes[] = $entityTaxVo;
    }

    /**
     * @return AbstractEntityTaxVO[]
     */
    public function getTaxes() {
        return $this->taxes;
    }

    /**
     * @return AbstractDataAccessObject
     */
    public function getEntityTaxDao()
    {
        return $this->entityTaxDao;
    }

    /**
     * @param AbstractDataAccessObject $entityTaxDao
     */
    public function setEntityTaxDao(AbstractDataAccessObject $entityTaxDao)
    {
        $this->entityTaxDao = $entityTaxDao;
    }
}
