<?php

namespace BlakePrinting\Taxes;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static TaxIdentifierTypeDAO instance(PDO $connection = null, $schemaName = null)
 * @method TaxIdentifierTypeVO findById($id, $options = null, $useCache = false)
 * @method TaxIdentifierTypeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaxIdentifierTypeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaxIdentifierTypeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaxIdentifierTypeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaxIdentifierTypeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaxIdentifierTypeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaxIdentifierTypeVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class TaxIdentifierTypeDAO extends AbstractDataAccessObject {
    const TAX_IDENTIFIER_TYPE_VAT = "VAT Number";
    const TAX_IDENTIFIER_TYPE_EIN = "Federal Employer Identification Number";
}
