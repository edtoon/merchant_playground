<?php

namespace BlakePrinting\Taxes;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static TaxTypeDAO instance(PDO $connection = null, $schemaName = null)
 * @method TaxTypeVO findById($id, $options = null, $useCache = false)
 * @method TaxTypeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaxTypeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaxTypeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaxTypeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaxTypeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaxTypeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaxTypeVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class TaxTypeDAO extends AbstractDataAccessObject {
    const TAX_TYPE_USE = "Use Tax";
    const TAX_TYPE_SALES = "Sales Tax";
    const TAX_TYPE_PURCHASE = "Purchase Tax";
    const TAX_TYPE_VAT_SALES = "VAT Sales";
    const TAX_TYPE_VAT_REDEMPTION = "VAT Redemption";
    const TAX_TYPE_VAT_SHIFTED = "VAT Shifted";
    const TAX_TYPE_VAT_MARGIN = "VAT Margin";
}
