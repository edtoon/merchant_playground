<?php

namespace BlakePrinting\Taxes;

use BlakePrinting\Db\ValueObject;

class TaxRateAreaVO implements ValueObject {
    /** @var int */
    private $taxRateId = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $creator = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $updater = null;
    /** @var int */
    private $start = null;
    /** @var int */
    private $end = null;
    /** @var string */
    private $city = null;
    /** @var string */
    private $district = null;
    /** @var string */
    private $stateOrRegion = null;
    /** @var string */
    private $postalCode = null;
    /** @var int */
    private $countryId = null;

    /**
     * @return int
     */
    public function getTaxRateId()
    {
        return $this->taxRateId;
    }

    /**
     * @param int $taxRateId
     */
    public function setTaxRateId($taxRateId)
    {
        $this->taxRateId = $taxRateId;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param int $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getUpdater()
    {
        return $this->updater;
    }

    /**
     * @param int $updater
     */
    public function setUpdater($updater)
    {
        $this->updater = $updater;
    }

    /**
     * @return int
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param int $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return int
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param int $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param string $district
     */
    public function setDistrict($district)
    {
        $this->district = $district;
    }

    /**
     * @return string
     */
    public function getStateOrRegion()
    {
        return $this->stateOrRegion;
    }

    /**
     * @param string $stateOrRegion
     */
    public function setStateOrRegion($stateOrRegion)
    {
        $this->stateOrRegion = $stateOrRegion;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return int
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * @param int $countryId
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;
    }
}
