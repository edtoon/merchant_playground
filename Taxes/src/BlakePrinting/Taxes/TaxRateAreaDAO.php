<?php

namespace BlakePrinting\Taxes;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static TaxRateAreaDAO instance(PDO $connection = null, $schemaName = null)
 * @method TaxRateAreaVO findById($id, $options = null, $useCache = false)
 * @method TaxRateAreaVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaxRateAreaVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaxRateAreaVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaxRateAreaVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaxRateAreaVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaxRateAreaVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaxRateAreaVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class TaxRateAreaDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }
}
