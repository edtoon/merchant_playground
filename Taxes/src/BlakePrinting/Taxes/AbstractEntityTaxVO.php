<?php

namespace BlakePrinting\Taxes;

use BlakePrinting\Db\ValueObject;

abstract class AbstractEntityTaxVO implements ValueObject {
    /** @var int */
    private $taxRateId = null;
    /** @var int */
    private $taxIdentifierTypeId = null;
    /** @var string */
    private $taxIdentifier = null;
    /** @var string */
    private $variablePercentage = null;
    /** @var string */
    private $taxAmount = null;

    /**
     * @param int $taxRateId
     * @param int $taxIdentifierTypeId
     * @param string $taxIdentifier
     * @param string $variablePercentage
     * @param string $taxAmount
     */
    public function __construct($taxRateId = null, $taxIdentifierTypeId = null, $taxIdentifier = null, $variablePercentage = null, $taxAmount = null) {
        $this->taxRateId = $taxRateId;
        $this->taxIdentifierTypeId = $taxIdentifierTypeId;
        $this->taxIdentifier = $taxIdentifier;
        $this->variablePercentage = $variablePercentage;
        $this->taxAmount = $taxAmount;
    }

    /**
     * @return int
     */
    public function getTaxRateId()
    {
        return $this->taxRateId;
    }

    /**
     * @param int $taxRateId
     */
    public function setTaxRateId($taxRateId)
    {
        $this->taxRateId = $taxRateId;
    }

    /**
     * @return int
     */
    public function getTaxIdentifierTypeId()
    {
        return $this->taxIdentifierTypeId;
    }

    /**
     * @param int $taxIdentifierTypeId
     */
    public function setTaxIdentifierTypeId($taxIdentifierTypeId)
    {
        $this->taxIdentifierTypeId = $taxIdentifierTypeId;
    }

    /**
     * @return string
     */
    public function getTaxIdentifier()
    {
        return $this->taxIdentifier;
    }

    /**
     * @param string $taxIdentifier
     */
    public function setTaxIdentifier($taxIdentifier)
    {
        $this->taxIdentifier = $taxIdentifier;
    }

    /**
     * @return string
     */
    public function getVariablePercentage()
    {
        return $this->variablePercentage;
    }

    /**
     * @param string $variablePercentage
     */
    public function setVariablePercentage($variablePercentage)
    {
        $this->variablePercentage = $variablePercentage;
    }

    /**
     * @return string
     */
    public function getTaxAmount()
    {
        return $this->taxAmount;
    }

    /**
     * @param string $taxAmount
     */
    public function setTaxAmount($taxAmount)
    {
        $this->taxAmount = $taxAmount;
    }
}
