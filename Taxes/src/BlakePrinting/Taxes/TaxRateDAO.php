<?php

namespace BlakePrinting\Taxes;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static TaxRateDAO instance(PDO $connection = null, $schemaName = null)
 * @method TaxRateVO findById($id, $options = null, $useCache = false)
 * @method TaxRateVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaxRateVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaxRateVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaxRateVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TaxRateVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TaxRateVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TaxRateVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class TaxRateDAO extends AbstractDataAccessObject {
}
