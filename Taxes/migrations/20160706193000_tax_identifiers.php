<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\Taxes\TaxIdentifierTypeDAO;
use BlakePrinting\Users\UserDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class TaxIdentifiers extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();
        $userDao =& UserDao::instance($connection);
        $userTbl = $userDao->formatSchemaAndTableName();

        $taxIdentifierTypeDao =& TaxIdentifierTypeDAO::instance($connection);
        $this->tableDao($taxIdentifierTypeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("name", "string", ["limit" => 191, "null" => true])
            ->addColumn("description", "text", ["null" => true])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(TaxIdentifierTypeDAO::instance($connection));
    }
}
