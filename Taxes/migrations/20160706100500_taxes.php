<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\Globalization\CountryDAO;
use BlakePrinting\Taxes\TaxRateAreaDAO;
use BlakePrinting\Taxes\TaxDAO;
use BlakePrinting\Taxes\TaxRateDAO;
use BlakePrinting\Taxes\TaxTagDAO;
use BlakePrinting\Taxes\TaxTaxTagDAO;
use BlakePrinting\Taxes\TaxTypeDAO;
use BlakePrinting\Users\UserDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class Taxes extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();
        $userDao =& UserDao::instance($connection);
        $userTbl = $userDao->formatSchemaAndTableName();

        $taxTypeDao =& TaxTypeDAO::instance($connection);
        $this->tableDao($taxTypeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("name", "string", ["limit" => 191, "null" => true])
            ->addColumn("description", "text", ["null" => true])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $taxDao =& TaxDAO::instance($connection);
        $taxTbl = $taxDao->formatSchemaAndTableName();
        $this->tableDao($taxDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("parent_tax_id", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addColumn("tax_type_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("code", "string", ["limit" => 191, "null" => true])
            ->addColumn("name", "string", ["limit" => 191, "null" => true])
            ->addColumn("description", "text", ["null" => true])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("parent_tax_id", $taxTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("tax_type_id", $taxTypeDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("code", ["unique" => true])
            ->addIndex("name")
            ->save()
        ;

        $taxRateDao =& TaxRateDAO::instance($connection);
        $this->tableDao($taxRateDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("tax_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addColumn("percentage", "decimal", ["null" => true, "precision" => 10, "scale" => 8])
            ->addColumn("variable", "integer", ["limit" => MysqlAdapter::INT_TINY, "precision" => 1, "default" => 0])
            ->addColumn("include_in_price", "integer", ["limit" => MysqlAdapter::INT_TINY, "precision" => 1, "default" => 0])
            ->addForeignKey("tax_id", $taxTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("percentage")
            ->addIndex("variable")
            ->addIndex("include_in_price")
            ->save()
        ;

        $taxRateAreaDao =& TaxRateAreaDAO::instance($connection);
        $countryDao =& CountryDAO::instance($connection);
        $this->tableDao($taxRateAreaDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("tax_rate_id", "integer", ["signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("start", "biginteger", ["null" => true, "signed" => false])
            ->addColumn("end", "biginteger", ["null" => true, "signed" => false])
            ->addColumn("city", "string", ["limit" => 191, "null" => true])
            ->addColumn("district", "string", ["limit" => 191, "null" => true])
            ->addColumn("state_or_region", "string", ["limit" => 191, "null" => true])
            ->addColumn("postal_code", "string", ["limit" => 191, "null" => true])
            ->addColumn("country_id", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addForeignKey("tax_rate_id", $taxRateDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("country_id", $countryDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("start")
            ->addIndex("end")
            ->addIndex("city")
            ->addIndex("district")
            ->addIndex("state_or_region")
            ->addIndex("postal_code")
            ->save()
        ;

        $taxTagDao =& TaxTagDAO::instance($connection);
        $this->tableDao($taxTagDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("name", "string", ["limit" => 191, "null" => true])
            ->addColumn("description", "text", ["null" => true])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $taxTaxTagDao =& TaxTaxTagDAO::instance($connection);
        $this->tableDao($taxTaxTagDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["tax_id", "tax_tag_id"]])
            ->addColumn("tax_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addColumn("tax_tag_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addForeignKey("tax_id", $taxTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("tax_tag_id", $taxTagDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(TaxTaxTagDAO::instance($connection));
        $this->dropDao(TaxTagDAO::instance($connection));
        $this->dropDao(TaxRateAreaDAO::instance($connection));
        $this->dropDao(TaxRateDAO::instance($connection));
        $this->dropDao(TaxDAO::instance($connection));
        $this->dropDao(TaxTypeDAO::instance($connection));
    }
}
