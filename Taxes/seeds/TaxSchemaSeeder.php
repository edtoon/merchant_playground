<?php

use BlakePrinting\Db\DbSeed;
use BlakePrinting\Taxes\TaxIdentifierTypeDAO;
use BlakePrinting\Taxes\TaxIdentifierTypeVO;
use BlakePrinting\Taxes\TaxTypeDAO;
use BlakePrinting\Taxes\TaxTypeVO;
use BlakePrinting\Util\Utils;

class TaxSchemaSeeder extends DbSeed {
    public function run() {
        $connection =& $this->getConnection();

        $taxTypeDao =& TaxTypeDAO::instance($connection);
        Utils::forEachConstantInObjectClass(
            $taxTypeDao,
            function($_, $constantName, $constantValue) use (&$taxTypeDao) {
                $taxTypeDao->findOrCreate(new TaxTypeVO($constantValue));
            }, "TAX_TYPE_"
        );

        $taxIdentifierTypeDao =& TaxIdentifierTypeDAO::instance($connection);
        Utils::forEachConstantInObjectClass(
            $taxIdentifierTypeDao,
            function($_, $constantName, $constantValue) use (&$taxIdentifierTypeDao) {
                $taxIdentifierTypeDao->findOrCreate(new TaxIdentifierTypeVO($constantValue));
            }, "TAX_IDENTIFIER_TYPE_"
        );
    }
}
