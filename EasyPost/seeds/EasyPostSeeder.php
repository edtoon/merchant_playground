<?php

use BlakePrinting\Db\DbSeed;
use BlakePrinting\EasyPost\EasyPostVerificationTypeDAO;
use BlakePrinting\EasyPost\EasyPostVerificationTypeVO;
use BlakePrinting\Util\Utils;

class EasyPostSeeder extends DbSeed {
    public function run() {
        $connection =& $this->getConnection();
        $verificationTypeDao =& EasyPostVerificationTypeDAO::instance($connection);

        Utils::forEachConstantInObjectClass(
            $verificationTypeDao,
            function($_, $constantName, $constantValue) use (&$verificationTypeDao) {
                $verificationTypeDao->findOrCreate(new EasyPostVerificationTypeVO($constantValue));
            }, "VERIFICATION_TYPE_"
        );
    }
}
