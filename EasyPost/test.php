<?php

use BlakePrinting\Db\PDOUtil;
use BlakePrinting\EasyPost\EasyPostAccountDAO;
use BlakePrinting\EasyPost\EasyPostAccountVO;
use BlakePrinting\EasyPost\EasyPostAddressDAO;
use BlakePrinting\EasyPost\EasyPostAddressVO;
use BlakePrinting\Util\ConfigUtil;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use EasyPost\Address;
use EasyPost\EasyPost;
use Stringy\StaticStringy as S;

require_once(__DIR__ . "/../vendor/autoload.php");

$config =& ConfigUtil::getConfig();
$logger =& LogUtil::getLogger();
$accountName = "blake";

EasyPost::setApiKey($config->retrieve("easypost", $accountName, "api_key"));

$connection =& PDOUtil::getMysqlConnection();
$addressDao =& EasyPostAddressDAO::instance($connection);
$accountVo =& EasyPostAccountDAO::instance($connection)->findOrCreate(new EasyPostAccountVO($accountName));
$addresses = Address::all();

if($addresses!== null) {
    if(!is_array($addresses)) {
        $logger->error("Non-array: " . Utils::getVarDump($addresses));
    } else {
        $voReflectionClass =& Utils::getReflectionClassByName(EasyPostAddressVO::class);

        foreach($addresses as &$address) {
            if(!$address instanceof Address) {
                $logger->error("Non-address: " . Utils::getVarDump($addresses));
            } else {
                $logger->debug("Address ID: " . $address["id"]);

                $verifications = null;
                $addressVo = new EasyPostAddressVO();

                $addressVo->setEasypostAccountId($accountVo->getId());

                foreach($address as $fieldName => $fieldValue) {
                    if($fieldName === "id") {
                        $fieldName = "identifier";
                    }

                    $fieldSetMethodName = "set" . (string)S::upperCamelize($fieldName);

                    if($voReflectionClass->hasMethod($fieldSetMethodName)) {
                        $fieldSetMethod = $voReflectionClass->getMethod($fieldSetMethodName);
                        $fieldSetMethod->invoke($addressVo, $fieldValue);
                    } else {
                        switch($fieldName) {
                            case "mode":
                            case "object":
                            break;
                            case "verifications":
                                $verifications = $fieldValue;
                            break;
                            default:
                                $logger->warn("Unrecognized address field: " . $fieldName . ", value: " . Utils::getVarDump($fieldValue));
                            break;
                        }
                    }
                }

                $id = $addressDao->store($addressVo);
                $addressVo = new EasyPostAddressVO();

                $addressVo->setId($id);

                $addressVo =& $addressDao->retrieve($addressVo);

                if("Edward Toon" === $addressVo->getName()) {
                    $logger->info("ADDRESS #" . $id . " IS ME, CREATED AT: " . $addressVo->getCreatedAt());

                    if(Utils::isNonEmptyArray($verifications)) {
                        $logger->info("VERIFICATIONS: " . Utils::getVarDump($verifications));
                    }
                }
           }
        }

        unset($address);
    }
}

PDOUtil::commitConnection($connection);

$addressVo = new EasyPostAddressVO();

$addressVo->setName("Edward Toon");
$addressVo->setCompany("B Internet Marketing Inc.");
$addressVo->setStreet1("2366 Eastlake Ave E #404");
$addressVo->setCity("Seattle");
$addressVo->setState("WA");
$addressVo->setZip("98102");
$addressVo->setCountry("US");

$addressVo =& $addressDao->retrieve($addressVo);

$logger->debug("Me: " . Utils::getVarDump($addressVo));

/*
$from = Address::create([
    "name" => $addressVo->getName(),
    "company" => $addressVo->getCompany(),
    "street1" => $addressVo->getStreet1(),
    "city" => $addressVo->getCity(),
    "state" => $addressVo->getState(),
    "zip" => $addressVo->getZip(),
    "country" => $addressVo->getCountry()
]);

$logger->debug("From: " . Utils::getVarDump($from));
*/
