<?php

namespace BlakePrinting\EasyPost;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static EasyPostAccountDAO instance(PDO $connection = null, $schemaName = null)
 * @method EasyPostAccountVO findById($id, $options = null, $useCache = false)
 * @method EasyPostAccountVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostAccountVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostAccountVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostAccountVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostAccountVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostAccountVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostAccountVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class EasyPostAccountDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null, $idColumnName = "id", $idColumnType = PDO::PARAM_INT) {
        parent::__construct($connection, $schemaName, "easypost_account", $idColumnName, $idColumnType);
    }
}
