<?php

namespace BlakePrinting\EasyPost;

use BlakePrinting\Contacts\ContactAddressVO;
use BlakePrinting\Contacts\ContactPersonVO;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Globalization\CountryDAO;
use BlakePrinting\Organizations\OrganizationVO;
use BlakePrinting\Util\ConfigUtil;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use EasyPost\Address;
use Exception;
use PDO;
use Stringy\StaticStringy as S;

class EasyPostAddressUtil {
    private function __construct() { }

    public static function retrieveAllAddresses(PDO &$connection = null) {
    }

    public static function &findOrCreateAddressVo(ContactAddressVO &$contactAddressVo, ContactPersonVO &$contactPersonVo = null, OrganizationVO &$organizationVo = null, EasyPostAccountVO &$easypostAccountVo = null, PDO &$connection = null) {
        $easypostAddressContactAddressDao =& EasyPostAddressContactAddressDAO::instance($connection);
        $easypostAddressContactAddressTbl = $easypostAddressContactAddressDao->formatSchemaAndTableName();
        $easypostAddressContactPersonDao =& EasyPostAddressContactPersonDAO::instance($connection);
        $easypostAddressContactPersonTbl = $easypostAddressContactPersonDao->formatSchemaAndTableName();
        $easypostAddressOrganizationDao =& EasyPostAddressOrganizationDAO::instance($connection);
        $easypostAddressOrganizationTbl = $easypostAddressOrganizationDao->formatSchemaAndTableName();
        $easypostAccountDao =& EasyPostAccountDAO::instance($connection);
        $easypostAccountTbl = $easypostAccountDao->formatSchemaAndTableName();
        $easypostAddressDao =& EasyPostAddressDAO::instance($connection);
        $easypostAddressTbl = $easypostAddressDao->formatSchemaAndTableName();
        $easypostAddressQuery =& PDOQuery::select()
            ->column($easypostAddressTbl . ".id")
            ->from($easypostAddressDao)
            ->join($easypostAddressContactAddressTbl, [$easypostAddressTbl . ".id = " . $easypostAddressContactAddressTbl . ".easypost_address_id"])
            ->where($easypostAddressContactAddressTbl . ".contact_address_id", $contactAddressVo->getId(), PDO::PARAM_INT)
            ->orderBy($easypostAddressTbl . ".id DESC")
            ->limit(1)
            ->options("FOR UPDATE")
        ;

        if($contactPersonVo !== null) {
            $easypostAddressQuery
                ->join($easypostAddressContactPersonTbl, [$easypostAddressTbl . ".id = " . $easypostAddressContactPersonTbl . ".easypost_address_id"])
                ->where($easypostAddressContactPersonTbl . ".contact_person_id", $contactPersonVo->getId(), PDO::PARAM_INT)
            ;
        }

        if($organizationVo !== null) {
            $easypostAddressQuery
                ->join($easypostAddressOrganizationTbl, [$easypostAddressTbl . ".id = " . $easypostAddressOrganizationTbl . ".easypost_address_id"])
                ->where($easypostAddressOrganizationTbl . ".organization_id", $organizationVo->getId(), PDO::PARAM_INT)
            ;
        }

        if($easypostAccountDao !== null) {
            $easypostAddressQuery
                ->join($easypostAccountTbl, [$easypostAddressTbl . ".easypost_account_id = " . $easypostAccountTbl . ".id"])
                ->where($easypostAccountTbl . ".id", $easypostAccountVo->getId(), PDO::PARAM_INT)
            ;
        }

        $easypostAddressId =& $easypostAddressQuery->getSingleValue($connection);
        $easypostAddressVo = null;

        if($easypostAddressId !== null) {
            $easypostAddressVo = new EasyPostAddressVo();

            $easypostAddressVo->setId($easypostAddressId);

            $easypostAddressVo =& $easypostAddressDao->retrieve($easypostAddressVo, "FOR UPDATE");
        } else {
            $easypostAddressVo =& self::getAddressPrototypeVo($contactAddressVo, $contactPersonVo, $organizationVo, $easypostAccountVo, $connection);
            $easypostAddressVo =& self::findOrCreateAddressByPrototypeVo($easypostAddressVo, $connection);

            EasyPostAddressContactAddressDAO::instance($connection)->insert(
                new EasyPostAddressContactAddressVO($easypostAddressVo->getId(), $contactAddressVo->getId())
            );

            if($contactPersonVo !== null) {
                EasyPostAddressContactPersonDAO::instance($connection)->insert(
                    new EasyPostAddressContactPersonVO($easypostAddressVo->getId(), $contactPersonVo->getId())
                );
            }

            if($organizationVo !== null) {
                EasyPostAddressOrganizationDAO::instance($connection)->insert(
                    new EasyPostAddressOrganizationVO($easypostAddressVo->getId(), $organizationVo->getId())
                );
            }
        }

        return $easypostAddressVo;
    }

    /**
     * @TODO us - line3, district
     * @TODO easypost - residential, carrierFacility, phone, email, federalTaxId, stateTaxId
     */
    public static function &getAddressPrototypeVo(ContactAddressVO &$contactAddressVo, ContactPersonVO &$contactPersonVo = null, OrganizationVO &$organizationVo = null, EasyPostAccountVO &$easypostAccountVo = null, PDO &$connection = null) {
        $countryVo =& CountryDAO::instance($connection)->findById($contactAddressVo->getCountryId(), null, true);
        $easypostAddressPrototypeVo = new EasyPostAddressVO();

        if($contactPersonVo !== null) {
            $easypostAddressPrototypeVo->setName($contactPersonVo->getName());
        }

        if($organizationVo !== null) {
            $easypostAddressPrototypeVo->setCompany($organizationVo->getName());
        }

        if($easypostAccountVo !== null) {
            $easypostAddressPrototypeVo->setEasypostAccountId($easypostAccountVo->getId());
        }

        $easypostAddressPrototypeVo->setStreet1($contactAddressVo->getLine1());
        $easypostAddressPrototypeVo->setStreet2($contactAddressVo->getLine2());
        $easypostAddressPrototypeVo->setCity($contactAddressVo->getCity());
        $easypostAddressPrototypeVo->setState($contactAddressVo->getStateOrRegion());
        $easypostAddressPrototypeVo->setZip($contactAddressVo->getPostalCode());
        $easypostAddressPrototypeVo->setCountry($countryVo->getIsoCode());

        return $easypostAddressPrototypeVo;
    }

    private static function &findOrCreateAddressByPrototypeVo(EasyPostAddressVO &$easypostAddressPrototypeVo, PDO &$connection = null) {
        $easypostAddressDao =& EasyPostAddressDAO::instance($connection);
        $easypostAddressVo =& $easypostAddressDao->retrieve($easypostAddressPrototypeVo, "FOR UPDATE");

        if($easypostAddressVo === null) {
            $easypostAccountVo =& EasyPostAccountDAO::instance($connection)->findById($easypostAddressPrototypeVo->getEasypostAccountId());
            $apiKey =& ConfigUtil::getConfig()->retrieve("easypost", $easypostAccountVo->getCode(), "api_key");
            $addressArray =& self::convertAddressVoToArray($easypostAddressPrototypeVo, $connection);
            $address =& self::execCreateAddress($apiKey, $addressArray, false, true);

            $easypostAddressVo =& self::storeAddressResponse($address, $easypostAccountVo, $connection);
        }

        return $easypostAddressVo;
    }

    private static function &storeAddressResponse(Address &$address, EasyPostAccountVO &$easypostAccountVo, PDO &$connection = null) {
        $logger =& LogUtil::getLogger();
        $easypostAddressDao =& EasyPostAddressDAO::instance($connection);
        $voReflectionClass = $easypostAddressDao->getVoReflectionClass();
        $easypostAddressVo = new EasyPostAddressVO();
        $verifications = null;

        $easypostAddressVo->setEasypostAccountId($easypostAccountVo->getId());

        foreach($address as $fieldName => $fieldValue) {
            if($fieldName === "id") {
                $fieldName = "identifier";
            }

            $fieldSetMethodName = "set" . (string)S::upperCamelize($fieldName);

            if($voReflectionClass->hasMethod($fieldSetMethodName)) {
                $fieldSetMethod = $voReflectionClass->getMethod($fieldSetMethodName);
                $fieldSetMethod->invoke($easypostAddressVo, $fieldValue);
            } else {
                switch($fieldName) {
                    case "mode":
                    case "object":
                    case "verifications":
                    break;
                    default:
                        $logger->warn("Unrecognized address field: " . $fieldName . ", value: " . Utils::getVarDump($fieldValue));
                    break;
                }
            }
        }

        $id =& $easypostAddressDao->store($easypostAddressVo);
        $easypostAddressVo = new EasyPostAddressVO();

        $easypostAddressVo->setId($id);

        return $easypostAddressDao->retrieve($easypostAddressVo);
    }

    /**
     * @param string $apiKey
     * @param array $addressParams
     * @param bool $verify
     * @param bool $verifyStrict
     * @return Address
     */
    private static function &execCreateAddress($apiKey, array &$addressParams, $verify = false, $verifyStrict = false) {
        $params = ["address" => $addressParams];
        $isUsAddress = (array_key_exists("country", $addressParams) && "US" === $addressParams["country"]);
        $verificationTypes = [];
        $strictVerificationTypes = [];
        $createAndVerify = false;

        if($verify) {
            $verificationTypes = [EasyPostVerificationTypeDAO::VERIFICATION_TYPE_DELIVERY];

            if($isUsAddress) {
                $verificationTypes[] = EasyPostVerificationTypeDAO::VERIFICATION_TYPE_ZIP4;
            }
        }

        if($verifyStrict) {
            $strictVerificationTypes = [EasyPostVerificationTypeDAO::VERIFICATION_TYPE_DELIVERY];

            if($isUsAddress) {
                $strictVerificationTypes[] = EasyPostVerificationTypeDAO::VERIFICATION_TYPE_ZIP4;
            }
        }

        if(Utils::isNonEmptyArray($verificationTypes)) {
            $params["verify"] = $verificationTypes;
            $createAndVerify = true;
        }

        if(Utils::isNonEmptyArray($strictVerificationTypes)) {
            $params["verify_strict"] = $strictVerificationTypes;
            $createAndVerify = true;
        }

        $logger =& LogUtil::getLogger();

        $logger->debug("Creating " . ($createAndVerify ? "verified" : "unverified") . " address with api key: " . $apiKey . ", params: " . Utils::getVarDump($params));

        $response = ($createAndVerify ? Address::create_and_verify($params, $apiKey) : Address::create($params, $apiKey));

        if(!$response instanceof Address) {
            throw new Exception("Unrecognized response from create address: " . Utils::getVarDump($response));
        }

        return $response;
    }

    /**
     * @param EasyPostAddressVO $easypostAddressVo
     * @param PDO|null $connection
     * @return array
     */
    private static function &convertAddressVoToArray(EasyPostAddressVO &$easypostAddressVo, PDO &$connection = null) {
        $easypostAddressDao =& EasyPostAddressDAO::instance($connection);
        $voReflectionMethods =& $easypostAddressDao->getVoReflectionMethods();
        $array = [];

        foreach($voReflectionMethods as &$voReflectionMethod) {
            $methodName = $voReflectionMethod->getName();
            $firstThreeChars = substr($methodName, 0, 3);
            $isGetMethod = ($firstThreeChars === "get");

            if($isGetMethod || substr($methodName, 0, 2) === "is") {
                $arrayKey = (string)S::underscored(substr($methodName, ($isGetMethod ? 3 : 2)));

                if(!in_array($arrayKey, ["id", "created", "updated", "easypost_account_id", "identifier", "created_at", "updated_at"])) {
                    $value = $voReflectionMethod->invoke($easypostAddressVo);

                    if($value !== null) {
                        $array[$arrayKey] = $value;
                    }
                }
            }
        }

        unset($voReflectionMethod);

        return $array;
    }
}
