<?php

namespace BlakePrinting\EasyPost;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static EasyPostAddressContactAddressDAO instance(PDO $connection = null, $schemaName = null)
 * @method EasyPostAddressContactAddressVO findById($id, $options = null, $useCache = false)
 * @method EasyPostAddressContactAddressVO findByColumn($column, $value, $pdoField = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostAddressContactAddressVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostAddressContactAddressVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostAddressContactAddressVO[] findAllByColumn($column, $value, $pdoField = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostAddressContactAddressVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostAddressContactAddressVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostAddressContactAddressVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class EasyPostAddressContactAddressDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, "easypost_address_contact_address", null, null);
    }
}
