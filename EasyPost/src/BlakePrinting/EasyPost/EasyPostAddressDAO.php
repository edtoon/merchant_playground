<?php

namespace BlakePrinting\EasyPost;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Db\ValueObject;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use DateTime;
use Exception;
use PDO;
use ReflectionClass;
use Stringy\StaticStringy as S;

/**
 * @method static EasyPostAddressDAO instance(PDO $connection = null, $schemaName = null)
 * @method EasyPostAddressVO findById($id, $options = null, $useCache = false)
 * @method EasyPostAddressVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostAddressVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostAddressVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostAddressVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostAddressVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostAddressVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostAddressVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class EasyPostAddressDAO extends AbstractDataAccessObject {
    const IDENTIFIER_PREFIX = "adr_";

    public function __construct(PDO $connection = null, $schemaName = null, $idColumnName = "id", $idColumnType = PDO::PARAM_INT) {
        parent::__construct($connection, $schemaName, "easypost_address", $idColumnName, $idColumnType);
    }

    /**
     * @param EasyPostAddressVO $easypostAddressVo
     * @param string|string[] $options
     * @parram array $includeNullColumns
     * @return EasyPostAddressVO
     */
    public function &retrieve(EasyPostAddressVO &$easypostAddressVo, $options = null, array $includeNullColumns = ["street1", "street2", "city", "state", "zip", "country", "name", "company", "phone", "email", "federal_tax_id", "state_tax_id"]) {
        $resultVo =& $this->findSingle(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$easypostAddressVo, &$includeNullColumns) {
                $voReflectionMethods =& $this->getVoReflectionMethods();
                $enforceNulls = ($easypostAddressVo->getId() === null && ($easypostAddressVo->getEasypostAccountId() === null || $easypostAddressVo->getIdentifier() === null));

                foreach($voReflectionMethods as &$voReflectionMethod) {
                    $methodName = $voReflectionMethod->getName();
                    $firstThreeChars = substr($methodName, 0, 3);
                    $isGetMethod = ($firstThreeChars === "get");

                    if(!$isGetMethod && $firstThreeChars === "set") {
                        $columnSpec = (string)S::underscored(substr($methodName, 3));

                        if($columnSpec === "identifier") {
                            $columnSpec = "CASE WHEN identifier IS NULL THEN NULL ELSE CONCAT('" . self::IDENTIFIER_PREFIX . "', HEX(identifier)) END AS identifier";
                        }

                        $pdoQuery->column($columnSpec);
                    } else if($isGetMethod || substr($methodName, 0, 2) === "is") {
                        $pdoType = PDOUtil::getPdoTypeFromReflectionMethod($voReflectionMethod);

                        if($pdoType !== null) {
                            $whereColumn = (string)S::underscored(substr($methodName, ($isGetMethod ? 3 : 2)));
                            $value = $voReflectionMethod->invoke($easypostAddressVo);

                            if($value !== null) {
                                if($methodName === "getIdentifier") {
                                    $pdoQuery->whereValueOrNull($whereColumn, self::getStrippedIdentifier($easypostAddressVo), PDO::PARAM_STR, "UNHEX");
                                } else {
                                    $pdoQuery->whereValueOrNull($whereColumn, $value, $pdoType);
                                }
                            } else if($enforceNulls && in_array($whereColumn, $includeNullColumns)) {
                                $pdoQuery->whereNull($whereColumn);
                            }
                        }
                    }
                }

                unset($voReflectionMethod);
            },
            "id DESC", 1, $options
        );

        if($resultVo !== null) {
            $resultVo->setCreatedAt(Utils::convertUtcTimestampToISO8601($resultVo->getCreatedAt()));
            $resultVo->setUpdatedAt(Utils::convertUtcTimestampToISO8601($resultVo->getUpdatedAt()));
        }

        return $resultVo;
    }

    /**
     * @param EasyPostAddressVO $easypostAddressVo
     * @return int
     */
    public function &store(EasyPostAddressVO &$easypostAddressVo) {
        $logger =& LogUtil::getLogger();
        $id = $easypostAddressVo->getId();
        $accountId = $easypostAddressVo->getEasypostAccountId();
        $identifier = self::getStrippedIdentifier($easypostAddressVo);
        $createdAt = $easypostAddressVo->getCreatedAt();
        $updatedAt = $easypostAddressVo->getUpdated();
        $connection =& $this->getConnection();

        if($accountId === null || $identifier === null) {
            throw new Exception("Can't store " . $this->getTableName() . " without an account and EasyPost object identifier");
        }

        if($createdAt !== null && !ctype_digit($createdAt)) {
            $createdAt = (new DateTime($createdAt))->getTimestamp();
        }

        if($updatedAt !== null && !ctype_digit($updatedAt)) {
            $updatedAt = (new DateTime($updatedAt))->getTimestamp();
        }

        $existingId =& PDOQuery::select()
            ->column("id")
            ->from($this)
            ->whereValueOrNull("easypost_account_id", $accountId, PDO::PARAM_INT)
            ->whereValueOrNull("identifier", $identifier, PDO::PARAM_STR, "UNHEX")
            ->options("FOR UPDATE")
            ->getSingleValue($connection)
        ;

        if($id !== null && $id !== $existingId) {
            throw new Exception(
                "Tried to store a different id (" . $id . ") for account #" . $accountId . ", " .
                "EasyPost id: " . $identifier . ", existing id: " . $existingId
            );
        }

        if($existingId === null) {
            $logger->debug("Inserting identifier: " . $identifier);

            $id =& PDOQuery::insert()
                ->into($this)
                ->value("created", "UNIX_TIMESTAMP()")
                ->value("easypost_account_id", $accountId, PDO::PARAM_INT)
                ->value("identifier", $identifier, PDO::PARAM_STR, "UNHEX")
                ->value("created_at", $createdAt, PDO::PARAM_STR)
                ->value("updated_at", $updatedAt, PDO::PARAM_STR)
                ->value("street1", $easypostAddressVo->getStreet1(), PDO::PARAM_STR)
                ->value("street2", $easypostAddressVo->getStreet2(), PDO::PARAM_STR)
                ->value("city", $easypostAddressVo->getCity(), PDO::PARAM_STR)
                ->value("state", $easypostAddressVo->getState(), PDO::PARAM_STR)
                ->value("zip", $easypostAddressVo->getZip(), PDO::PARAM_STR)
                ->value("country", $easypostAddressVo->getCountry(), PDO::PARAM_STR)
                ->value("residential", $easypostAddressVo->isResidential(), PDO::PARAM_BOOL)
                ->value("carrier_facility", $easypostAddressVo->getCarrierFacility(), PDO::PARAM_STR)
                ->value("name", $easypostAddressVo->getName(), PDO::PARAM_STR)
                ->value("company", $easypostAddressVo->getCompany(), PDO::PARAM_STR)
                ->value("phone", $easypostAddressVo->getPhone(), PDO::PARAM_STR)
                ->value("email", $easypostAddressVo->getEmail(), PDO::PARAM_STR)
                ->value("federal_tax_id", $easypostAddressVo->getFederalTaxId(), PDO::PARAM_STR)
                ->value("state_tax_id", $easypostAddressVo->getStateTaxId(), PDO::PARAM_STR)
                ->executeEnsuringLastInsertId($connection)
            ;
        } else {
            $id = $existingId;

            $logger->debug("Updating id: " . $id);

            PDOQuery::update()
                ->table($this)
                ->set("updated", "UNIX_TIMESTAMP()")
                ->set("easypost_account_id", $accountId, PDO::PARAM_INT)
                ->set("identifier", $identifier, PDO::PARAM_STR, "UNHEX")
                ->set("created_at", $createdAt, PDO::PARAM_STR)
                ->set("updated_at", $updatedAt, PDO::PARAM_STR)
                ->set("street1", $easypostAddressVo->getStreet1(), PDO::PARAM_STR)
                ->set("street2", $easypostAddressVo->getStreet2(), PDO::PARAM_STR)
                ->set("city", $easypostAddressVo->getCity(), PDO::PARAM_STR)
                ->set("state", $easypostAddressVo->getState(), PDO::PARAM_STR)
                ->set("zip", $easypostAddressVo->getZip(), PDO::PARAM_STR)
                ->set("country", $easypostAddressVo->getCountry(), PDO::PARAM_STR)
                ->set("residential", $easypostAddressVo->isResidential(), PDO::PARAM_BOOL)
                ->set("carrier_facility", $easypostAddressVo->getCarrierFacility(), PDO::PARAM_STR)
                ->set("name", $easypostAddressVo->getName(), PDO::PARAM_STR)
                ->set("company", $easypostAddressVo->getCompany(), PDO::PARAM_STR)
                ->set("phone", $easypostAddressVo->getPhone(), PDO::PARAM_STR)
                ->set("email", $easypostAddressVo->getEmail(), PDO::PARAM_STR)
                ->set("federal_tax_id", $easypostAddressVo->getFederalTaxId(), PDO::PARAM_STR)
                ->set("state_tax_id", $easypostAddressVo->getStateTaxId(), PDO::PARAM_STR)
                ->where("id", $id, PDO::PARAM_INT)
                ->executeGetRowCount($connection)
            ;
        }

        return $id;
    }

    /**
     * @param EasyPostAddressVO $easypostAddressVo
     * @return string
     */
    public static function &getStrippedIdentifier(EasyPostAddressVO &$easypostAddressVo) {
        $identifier = $easypostAddressVo->getIdentifier();
        $prefixLength = strlen(self::IDENTIFIER_PREFIX);

        if($identifier !== null && substr($identifier, 0, $prefixLength) === self::IDENTIFIER_PREFIX) {
            $identifier = substr($identifier, $prefixLength);
        }

        return $identifier;
    }
}
