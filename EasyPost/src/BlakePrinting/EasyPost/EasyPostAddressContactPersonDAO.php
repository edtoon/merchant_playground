<?php

namespace BlakePrinting\EasyPost;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static EasyPostAddressContactPersonDAO instance(PDO $connection = null, $schemaName = null)
 * @method EasyPostAddressContactPersonVO findById($id, $options = null, $useCache = false)
 * @method EasyPostAddressContactPersonVO findByColumn($column, $value, $pdoField = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostAddressContactPersonVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostAddressContactPersonVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostAddressContactPersonVO[] findAllByColumn($column, $value, $pdoField = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostAddressContactPersonVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostAddressContactPersonVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostAddressContactPersonVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class EasyPostAddressContactPersonDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, "easypost_address_contact_person", null, null);
    }
}
