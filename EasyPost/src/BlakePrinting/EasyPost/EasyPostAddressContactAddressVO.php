<?php

namespace BlakePrinting\EasyPost;

use BlakePrinting\Db\ValueObject;

class EasyPostAddressContactAddressVO implements ValueObject {
    /** @var int */
    private $easypostAddressId = null;
    /** @var int */
    private $contactAddressId = null;

    public function __construct($easypostAddressId = null, $contactAddressId = null) {
        $this->easypostAddressId = $easypostAddressId;
        $this->contactAddressId = $contactAddressId;
    }

    /**
     * @return int
     */
    public function getEasypostAddressId()
    {
        return $this->easypostAddressId;
    }

    /**
     * @param int $easypostAddressId
     */
    public function setEasypostAddressId($easypostAddressId)
    {
        $this->easypostAddressId = $easypostAddressId;
    }

    /**
     * @return int
     */
    public function getContactAddressId()
    {
        return $this->contactAddressId;
    }

    /**
     * @param int $contactAddressId
     */
    public function setContactAddressId($contactAddressId)
    {
        $this->contactAddressId = $contactAddressId;
    }
}
