<?php

namespace BlakePrinting\EasyPost;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static EasyPostAddressVerificationDAO instance(PDO $connection = null, $schemaName = null)
 * @method EasyPostAddressVerificationVO findById($id, $options = null, $useCache = false)
 * @method EasyPostAddressVerificationVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostAddressVerificationVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostAddressVerificationVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostAddressVerificationVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostAddressVerificationVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostAddressVerificationVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostAddressVerificationVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class EasyPostAddressVerificationDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null, $idColumnName = "id", $idColumnType = PDO::PARAM_INT) {
        parent::__construct($connection, $schemaName, "easypost_address_verification", $idColumnName, $idColumnType);
    }
}
