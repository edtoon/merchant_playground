<?php

namespace BlakePrinting\EasyPost;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static EasyPostVerificationErrorDAO instance(PDO $connection = null, $schemaName = null)
 * @method EasyPostVerificationErrorVO findById($id, $options = null, $useCache = false)
 * @method EasyPostVerificationErrorVO findByColumn($column, $value, $pdoField = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostVerificationErrorVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostVerificationErrorVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostVerificationErrorVO[] findAllByColumn($column, $value, $pdoField = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostVerificationErrorVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostVerificationErrorVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostVerificationErrorVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class EasyPostVerificationErrorDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null, $idColumnName = "id", $idColumnType = PDO::PARAM_INT) {
        parent::__construct($connection, $schemaName, "easypost_verification_error", $idColumnName, $idColumnType);
    }
}
