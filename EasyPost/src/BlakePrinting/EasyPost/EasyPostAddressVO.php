<?php

namespace BlakePrinting\EasyPost;

use BlakePrinting\Db\ValueObject;

class EasyPostAddressVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $easypostAccountId = null;
    /** @var string */
    private $identifier = null;
    /** @var int */
    private $createdAt = null;
    /** @var int */
    private $updatedAt = null;
    /** @var string */
    private $street1 = null;
    /** @var string */
    private $street2 = null;
    /** @var string */
    private $city = null;
    /** @var string */
    private $state = null;
    /** @var string */
    private $zip = null;
    /** @var string */
    private $country = null;
    /** @var bool */
    private $residential = null;
    /** @var string */
    private $carrierFacility = null;
    /** @var string */
    private $name = null;
    /** @var string */
    private $company = null;
    /** @var string */
    private $phone = null;
    /** @var string */
    private $email = null;
    /** @var string */
    private $federalTaxId = null;
    /** @var string */
    private $stateTaxId = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getEasypostAccountId()
    {
        return $this->easypostAccountId;
    }

    /**
     * @param int $easypostAccountId
     */
    public function setEasypostAccountId($easypostAccountId)
    {
        $this->easypostAccountId = $easypostAccountId;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string
     */
    public function getStreet1()
    {
        return $this->street1;
    }

    /**
     * @param string $street1
     */
    public function setStreet1($street1)
    {
        $this->street1 = $street1;
    }

    /**
     * @return string
     */
    public function getStreet2()
    {
        return $this->street2;
    }

    /**
     * @param string $street2
     */
    public function setStreet2($street2)
    {
        $this->street2 = $street2;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return boolean
     */
    public function isResidential()
    {
        return $this->residential;
    }

    /**
     * @param boolean $residential
     */
    public function setResidential($residential)
    {
        $this->residential = $residential;
    }

    /**
     * @return string
     */
    public function getCarrierFacility()
    {
        return $this->carrierFacility;
    }

    /**
     * @param string $carrierFacility
     */
    public function setCarrierFacility($carrierFacility)
    {
        $this->carrierFacility = $carrierFacility;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getFederalTaxId()
    {
        return $this->federalTaxId;
    }

    /**
     * @param string $federalTaxId
     */
    public function setFederalTaxId($federalTaxId)
    {
        $this->federalTaxId = $federalTaxId;
    }

    /**
     * @return string
     */
    public function getStateTaxId()
    {
        return $this->stateTaxId;
    }

    /**
     * @param string $stateTaxId
     */
    public function setStateTaxId($stateTaxId)
    {
        $this->stateTaxId = $stateTaxId;
    }
}
