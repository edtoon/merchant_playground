<?php

namespace BlakePrinting\EasyPost;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Db\ValueObject;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use DateTime;
use Exception;
use PDO;
use ReflectionClass;
use Stringy\StaticStringy as S;

/**
 * @method static EasyPostParcelDAO instance(PDO $connection = null, $schemaName = null)
 * @method EasyPostParcelVO findById($id, $options = null, $useCache = false)
 * @method EasyPostParcelVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostParcelVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostParcelVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostParcelVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostParcelVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostParcelVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostParcelVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class EasyPostParcelDAO extends AbstractDataAccessObject {
    const IDENTIFIER_PREFIX = "prcl_";

    public function __construct(PDO $connection = null, $schemaName = null, $idColumnName = "id", $idColumnType = PDO::PARAM_INT) {
        parent::__construct($connection, $schemaName, "easypost_parcel", $idColumnName, $idColumnType);
    }

    /**
     * @param EasyPostParcelVO $easypostParcelVo
     * @param string|string[] $options
     * @parram array $includeNullColumns
     * @return EasyPostParcelVO
     */
    public function &retrieve(EasyPostParcelVO &$easypostParcelVo, $options = null, array $includeNullColumns = ["length", "width", "height", "weight"]) {
        $resultVo =& $this->findSingle(
            function(PDOQuery &$pdoQuery, ReflectionClass &$reflectionClass) use (&$easypostParcelVo, &$includeNullColumns) {
                $voReflectionMethods =& $this->getVoReflectionMethods();
                $enforceNulls = ($easypostParcelVo->getId() === null && ($easypostParcelVo->getEasypostAccountId() === null || $easypostParcelVo->getIdentifier() === null));

                foreach($voReflectionMethods as &$voReflectionMethod) {
                    $methodName = $voReflectionMethod->getName();
                    $firstThreeChars = substr($methodName, 0, 3);
                    $isGetMethod = ($firstThreeChars === "get");

                    if(!$isGetMethod && $firstThreeChars === "set") {
                        $columnSpec = (string)S::underscored(substr($methodName, 3));

                        if($columnSpec === "identifier") {
                            $columnSpec = "CASE WHEN identifier IS NULL THEN NULL ELSE CONCAT('" . self::IDENTIFIER_PREFIX . "', HEX(identifier)) END AS identifier";
                        }

                        $pdoQuery->column($columnSpec);
                    } else if($isGetMethod || substr($methodName, 0, 2) === "is") {
                        $pdoType = PDOUtil::getPdoTypeFromReflectionMethod($voReflectionMethod);

                        if($pdoType !== null) {
                            $whereColumn = (string)S::underscored(substr($methodName, ($isGetMethod ? 3 : 2)));
                            $value = $voReflectionMethod->invoke($easypostParcelVo);

                            if($value !== null) {
                                if($methodName === "getIdentifier") {
                                    $pdoQuery->whereValueOrNull($whereColumn, self::getStrippedIdentifier($easypostParcelVo), PDO::PARAM_STR, "UNHEX");
                                } else {
                                    $pdoQuery->whereValueOrNull($whereColumn, $value, $pdoType);
                                }
                            } else if($enforceNulls && in_array($whereColumn, $includeNullColumns)) {
                                $pdoQuery->whereNull($whereColumn);
                            }
                        }
                    }
                }

                unset($voReflectionMethod);
            },
            "id DESC", 1, $options
        );

        if($resultVo !== null) {
            $resultVo->setCreatedAt(Utils::convertUtcTimestampToISO8601($resultVo->getCreatedAt()));
            $resultVo->setUpdatedAt(Utils::convertUtcTimestampToISO8601($resultVo->getUpdatedAt()));
        }

        return $resultVo;
    }

    /**
     * @param EasyPostParcelVO $easypostParcelVo
     * @return int
     */
    public function &store(EasyPostParcelVO &$easypostParcelVo) {
        $logger =& LogUtil::getLogger();
        $id = $easypostParcelVo->getId();
        $accountId = $easypostParcelVo->getEasypostAccountId();
        $identifier = self::getStrippedIdentifier($easypostParcelVo);
        $createdAt = $easypostParcelVo->getCreatedAt();
        $updatedAt = $easypostParcelVo->getUpdated();
        $connection =& $this->getConnection();

        if($accountId === null || $identifier === null) {
            throw new Exception("Can't store " . $this->getTableName() . " without an account and EasyPost object identifier");
        }

        if($createdAt !== null && !ctype_digit($createdAt)) {
            $createdAt = (new DateTime($createdAt))->getTimestamp();
        }

        if($updatedAt !== null && !ctype_digit($updatedAt)) {
            $updatedAt = (new DateTime($updatedAt))->getTimestamp();
        }

        $existingId =& PDOQuery::select()
            ->column("id")
            ->from($this)
            ->whereValueOrNull("easypost_account_id", $accountId, PDO::PARAM_INT)
            ->whereValueOrNull("identifier", $identifier, PDO::PARAM_STR, "UNHEX")
            ->options("FOR UPDATE")
            ->getSingleValue($connection)
        ;

        if($id !== null && $id !== $existingId) {
            throw new Exception(
                "Tried to store a different id (" . $id . ") for account #" . $accountId . ", " .
                "EasyPost id: " . $identifier . ", existing id: " . $existingId
            );
        }

        if($existingId === null) {
            $logger->debug("Inserting identifier: " . $identifier);

            $id =& PDOQuery::insert()
                ->into($this)
                ->value("created", "UNIX_TIMESTAMP()")
                ->value("easypost_account_id", $accountId, PDO::PARAM_INT)
                ->value("identifier", $identifier, PDO::PARAM_STR, "UNHEX")
                ->value("created_at", $createdAt, PDO::PARAM_STR)
                ->value("updated_at", $updatedAt, PDO::PARAM_STR)
                ->value("length", $easypostParcelVo->getLength(), PDO::PARAM_STR)
                ->value("width", $easypostParcelVo->getWidth(), PDO::PARAM_STR)
                ->value("height", $easypostParcelVo->getHeight(), PDO::PARAM_STR)
                ->value("weight", $easypostParcelVo->getWeight(), PDO::PARAM_STR)
                ->executeEnsuringLastInsertId($connection)
            ;
        } else {
            $id = $existingId;

            $logger->debug("Updating id: " . $id);

            PDOQuery::update()
                ->table($this)
                ->set("updated", "UNIX_TIMESTAMP()")
                ->set("easypost_account_id", $accountId, PDO::PARAM_INT)
                ->set("identifier", $identifier, PDO::PARAM_STR, "UNHEX")
                ->set("created_at", $createdAt, PDO::PARAM_STR)
                ->set("updated_at", $updatedAt, PDO::PARAM_STR)
                ->set("length", $easypostParcelVo->getLength(), PDO::PARAM_STR)
                ->set("width", $easypostParcelVo->getWidth(), PDO::PARAM_STR)
                ->set("height", $easypostParcelVo->getHeight(), PDO::PARAM_STR)
                ->set("weight", $easypostParcelVo->getWeight(), PDO::PARAM_STR)
                ->where("id", $id, PDO::PARAM_INT)
                ->executeGetRowCount($connection)
            ;
        }

        return $id;
    }

    /**
     * @param EasyPostParcelVO $easypostParcelVo
     * @return string
     */
    public static function &getStrippedIdentifier(EasyPostParcelVO &$easypostParcelVo) {
        $identifier = $easypostParcelVo->getIdentifier();
        $prefixLength = strlen(self::IDENTIFIER_PREFIX);

        if($identifier !== null && substr($identifier, 0, $prefixLength) === self::IDENTIFIER_PREFIX) {
            $identifier = substr($identifier, $prefixLength);
        }

        return $identifier;
    }
}
