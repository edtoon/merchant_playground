<?php

namespace BlakePrinting\EasyPost;

use BlakePrinting\Db\ValueObject;

class EasyPostVerificationErrorVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $easypostVerificationFieldId = null;
    /** @var string */
    private $error = null;

    public function __construct($easypostVerificationFieldId = null, $error = null) {
        $this->easypostVerificationFieldId = $easypostVerificationFieldId;
        $this->error = $error;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getEasypostVerificationFieldId()
    {
        return $this->easypostVerificationFieldId;
    }

    /**
     * @param int $easypostVerificationFieldId
     */
    public function setEasypostVerificationFieldId($easypostVerificationFieldId)
    {
        $this->easypostVerificationFieldId = $easypostVerificationFieldId;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param string $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }
}
