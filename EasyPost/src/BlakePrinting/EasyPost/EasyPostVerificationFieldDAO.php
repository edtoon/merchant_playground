<?php

namespace BlakePrinting\EasyPost;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static EasyPostVerificationFieldDAO instance(PDO $connection = null, $schemaName = null)
 * @method EasyPostVerificationFieldVO findById($id, $options = null, $useCache = false)
 * @method EasyPostVerificationFieldVO findByColumn($column, $value, $pdoField = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostVerificationFieldVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostVerificationFieldVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostVerificationFieldVO[] findAllByColumn($column, $value, $pdoField = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostVerificationFieldVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostVerificationFieldVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostVerificationFieldVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class EasyPostVerificationFieldDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null, $idColumnName = "id", $idColumnType = PDO::PARAM_INT) {
        parent::__construct($connection, $schemaName, "easypost_verification_field", $idColumnName, $idColumnType);
    }
}
