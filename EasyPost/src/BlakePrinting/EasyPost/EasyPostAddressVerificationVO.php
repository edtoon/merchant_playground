<?php

namespace BlakePrinting\EasyPost;

use BlakePrinting\Db\ValueObject;

class EasyPostAddressVerificationVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $easypostAddressId = null;
    /** @var int */
    private $easypostVerificationTypeId = null;
    /** @var bool */
    private $success = null;
    /** @var int */
    private $latitude = null;
    /** @var int */
    private $longitude = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getEasypostAddressId()
    {
        return $this->easypostAddressId;
    }

    /**
     * @param int $easypostAddressId
     */
    public function setEasypostAddressId($easypostAddressId)
    {
        $this->easypostAddressId = $easypostAddressId;
    }

    /**
     * @return int
     */
    public function getEasypostVerificationTypeId()
    {
        return $this->easypostVerificationTypeId;
    }

    /**
     * @param int $easypostVerificationTypeId
     */
    public function setEasypostVerificationTypeId($easypostVerificationTypeId)
    {
        $this->easypostVerificationTypeId = $easypostVerificationTypeId;
    }

    /**
     * @return boolean
     */
    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * @param boolean $success
     */
    public function setSuccess($success)
    {
        $this->success = $success;
    }

    /**
     * @return int
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param int $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return int
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param int $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }
}
