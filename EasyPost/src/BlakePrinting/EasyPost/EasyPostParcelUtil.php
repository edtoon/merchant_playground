<?php

namespace BlakePrinting\EasyPost;

use BlakePrinting\Db\PDOQuery;
use PDO;

class EasyPostParcelUtil {
    private function __construct() { }

    public static function &findOrCreateParcelVo($length, $width, $height, $weight, EasyPostAddressVO &$easypostAddressVo = null, EasyPostAccountVO &$easypostAccountVo = null, PDO &$connection = null) {
        $easypostParcelShipmentDao =& EasyPostParcelShipmentDAO::instance($connection);
        $easypostParcelShipmentTbl = $easypostParcelShipmentDao->formatSchemaAndTableName();
        $easypostAccountDao =& EasyPostAccountDAO::instance($connection);
        $easypostAccountTbl = $easypostAccountDao->formatSchemaAndTableName();
        $easypostParcelDao =& EasyPostParcelDAO::instance($connection);
        $easypostParcelTbl = $easypostParcelDao->formatSchemaAndTableName();
        $easypostParcelQuery =& PDOQuery::select()
            ->column($easypostParcelTbl . ".id")
            ->from($easypostParcelDao)
            ->join($easypostParcelShipmentTbl, [$easypostParcelTbl . ".id = " . $easypostParcelShipmentTbl . ".easypost_parcel_id"])
            ->where($easypostParcelShipmentTbl . ".shipment_id", $shipmentVo->getId(), PDO::PARAM_INT)
            ->orderBy($easypostAddressTbl . ".id DESC")
            ->limit(1)
            ->options("FOR UPDATE")
        ;

        if($contactPersonVo !== null) {
            $easypostAddressQuery
                ->join($easypostAddressContactPersonTbl, [$easypostAddressTbl . ".id = " . $easypostAddressContactPersonTbl . ".easypost_address_id"])
                ->where($easypostAddressContactPersonTbl . ".contact_person_id", $contactPersonVo->getId(), PDO::PARAM_INT)
            ;
        }

        if($organizationVo !== null) {
            $easypostAddressQuery
                ->join($easypostAddressOrganizationTbl, [$easypostAddressTbl . ".id = " . $easypostAddressOrganizationTbl . ".easypost_address_id"])
                ->where($easypostAddressOrganizationTbl . ".organization_id", $organizationVo->getId(), PDO::PARAM_INT)
            ;
        }

        if($easypostAccountDao !== null) {
            $easypostAddressQuery
                ->join($easypostAccountTbl, [$easypostAddressTbl . ".easypost_account_id = " . $easypostAccountTbl . ".id"])
                ->where($easypostAccountTbl . ".id", $easypostAccountVo->getId(), PDO::PARAM_INT)
            ;
        }

        $easypostAddressId =& $easypostAddressQuery->getSingleValue($connection);
        $easypostAddressVo = null;

        if($easypostAddressId !== null) {
            $easypostAddressVo = new EasyPostAddressVo();

            $easypostAddressVo->setId($easypostAddressId);

            $easypostAddressVo =& $easypostAddressDao->retrieve($easypostAddressVo, "FOR UPDATE");
        } else {
            $easypostAddressVo =& self::getAddressPrototypeVo($contactAddressVo, $contactPersonVo, $organizationVo, $easypostAccountVo, $connection);
            $easypostAddressVo =& self::findOrCreateAddressByPrototypeVo($easypostAddressVo, $connection);

            EasyPostAddressContactAddressDAO::instance($connection)->insert(
                new EasyPostAddressContactAddressVO($easypostAddressVo->getId(), $contactAddressVo->getId())
            );

            if($contactPersonVo !== null) {
                EasyPostAddressContactPersonDAO::instance($connection)->insert(
                    new EasyPostAddressContactPersonVO($easypostAddressVo->getId(), $contactPersonVo->getId())
                );
            }

            if($organizationVo !== null) {
                EasyPostAddressOrganizationDAO::instance($connection)->insert(
                    new EasyPostAddressOrganizationVO($easypostAddressVo->getId(), $organizationVo->getId())
                );
            }
        }

        return $easypostAddressVo;
    }
}
