<?php

namespace BlakePrinting\EasyPost;

use BlakePrinting\Db\ValueObject;

class EasyPostAddressContactPersonVO implements ValueObject {
    /** @var int */
    private $easypostAddressId = null;
    /** @var int */
    private $contactPersonId = null;

    public function __construct($easypostAddressId = null, $contactPersonId = null) {
        $this->easypostAddressId = $easypostAddressId;
        $this->contactPersonId = $contactPersonId;
    }

    /**
     * @return int
     */
    public function getEasypostAddressId()
    {
        return $this->easypostAddressId;
    }

    /**
     * @param int $easypostAddressId
     */
    public function setEasypostAddressId($easypostAddressId)
    {
        $this->easypostAddressId = $easypostAddressId;
    }

    /**
     * @return int
     */
    public function getContactPersonId()
    {
        return $this->contactPersonId;
    }

    /**
     * @param int $contactPersonId
     */
    public function setContactPersonId($contactPersonId)
    {
        $this->contactPersonId = $contactPersonId;
    }
}
