<?php

namespace BlakePrinting\EasyPost;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static EasyPostAddressOrganizationDAO instance(PDO $connection = null, $schemaName = null)
 * @method EasyPostAddressOrganizationVO findById($id, $options = null, $useCache = false)
 * @method EasyPostAddressOrganizationVO findByColumn($column, $value, $pdoField = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostAddressOrganizationVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostAddressOrganizationVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostAddressOrganizationVO[] findAllByColumn($column, $value, $pdoField = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostAddressOrganizationVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostAddressOrganizationVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostAddressOrganizationVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class EasyPostAddressOrganizationDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, "easypost_address_organization", null, null);
    }
}
