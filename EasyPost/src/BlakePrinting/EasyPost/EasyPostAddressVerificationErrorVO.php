<?php

namespace BlakePrinting\EasyPost;

use BlakePrinting\Db\ValueObject;

class EasyPostAddressVerificationErrorVO implements ValueObject {
    /** @var int */
    private $easypostAddressVerificationId = null;
    /** @var int */
    private $easypostVerificationErrorId = null;

    public function __construct($easypostAddressVerificationId = null, $easypostVerificationErrorId = null) {
        $this->easypostAddressVerificationId = $easypostAddressVerificationId;
        $this->easypostVerificationErrorId = $easypostVerificationErrorId;
    }

    /**
     * @return int
     */
    public function getEasypostAddressVerificationId()
    {
        return $this->easypostAddressVerificationId;
    }

    /**
     * @param int $easypostAddressVerificationId
     */
    public function setEasypostAddressVerificationId($easypostAddressVerificationId)
    {
        $this->easypostAddressVerificationId = $easypostAddressVerificationId;
    }

    /**
     * @return int
     */
    public function getEasypostVerificationErrorId()
    {
        return $this->easypostVerificationErrorId;
    }

    /**
     * @param int $easypostVerificationErrorId
     */
    public function setEasypostVerificationErrorId($easypostVerificationErrorId)
    {
        $this->easypostVerificationErrorId = $easypostVerificationErrorId;
    }
}
