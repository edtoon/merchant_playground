<?php

namespace BlakePrinting\EasyPost;

use BlakePrinting\Db\ValueObject;

class EasyPostAddressOrganizationVO implements ValueObject {
    /** @var int */
    private $easypostAddressId = null;
    /** @var int */
    private $organizationId = null;

    public function __construct($easypostAddressId = null, $organizationId = null) {
        $this->easypostAddressId = $easypostAddressId;
        $this->organizationId = $organizationId;
    }

    /**
     * @return int
     */
    public function getEasypostAddressId()
    {
        return $this->easypostAddressId;
    }

    /**
     * @param int $easypostAddressId
     */
    public function setEasypostAddressId($easypostAddressId)
    {
        $this->easypostAddressId = $easypostAddressId;
    }

    /**
     * @return int
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * @param int $organizationId
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    }
}
