<?php

namespace BlakePrinting\EasyPost;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static EasyPostAddressVerificationErrorDAO instance(PDO $connection = null, $schemaName = null)
 * @method EasyPostAddressVerificationErrorVO findById($id, $options = null, $useCache = false)
 * @method EasyPostAddressVerificationErrorVO findByColumn($column, $value, $pdoField = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostAddressVerificationErrorVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostAddressVerificationErrorVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostAddressVerificationErrorVO[] findAllByColumn($column, $value, $pdoField = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostAddressVerificationErrorVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostAddressVerificationErrorVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostAddressVerificationErrorVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class EasyPostAddressVerificationErrorDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, "easypost_address_verification_error", null, null);
    }
}
