<?php

namespace BlakePrinting\EasyPost;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static EasyPostVerificationTypeDAO instance(PDO $connection = null, $schemaName = null)
 * @method EasyPostVerificationTypeVO findById($id, $options = null, $useCache = false)
 * @method EasyPostVerificationTypeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostVerificationTypeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostVerificationTypeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostVerificationTypeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method EasyPostVerificationTypeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method EasyPostVerificationTypeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method EasyPostVerificationTypeVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class EasyPostVerificationTypeDAO extends AbstractDataAccessObject {
    const VERIFICATION_TYPE_DELIVERY = "delivery";
    const VERIFICATION_TYPE_ZIP4 = "zip4";

    public function __construct(PDO $connection = null, $schemaName = null, $idColumnName = "id", $idColumnType = PDO::PARAM_INT) {
        parent::__construct($connection, $schemaName, "easypost_verification_type", $idColumnName, $idColumnType);
    }
}
