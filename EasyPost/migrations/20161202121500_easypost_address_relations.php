<?php

use BlakePrinting\Contacts\ContactAddressDAO;
use BlakePrinting\Contacts\ContactPersonDAO;
use BlakePrinting\Db\DbMigration;
use BlakePrinting\EasyPost\EasyPostAddressContactAddressDAO;
use BlakePrinting\EasyPost\EasyPostAddressContactPersonDAO;
use BlakePrinting\EasyPost\EasyPostAddressDAO;
use BlakePrinting\EasyPost\EasyPostAddressOrganizationDAO;
use BlakePrinting\Organizations\OrganizationDAO;

class EasyPostAddressRelations extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();
        $addressTbl = EasyPostAddressDAO::instance($connection)->formatSchemaAndTableName();

        $addressContactAddressDao =& EasyPostAddressContactAddressDAO::instance($connection);
        $contactAddressTbl = ContactAddressDAO::instance($connection)->formatSchemaAndTableName();
        $this->tableDao($addressContactAddressDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("easypost_address_id", "integer", ["signed" => false])
            ->addColumn("contact_address_id", "integer", ["signed" => false])
            ->addForeignKey("easypost_address_id", $addressTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("contact_address_id", $contactAddressTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $addressContactPersonDao =& EasyPostAddressContactPersonDAO::instance($connection);
        $contactPersonTbl = ContactPersonDAO::instance($connection)->formatSchemaAndTableName();
        $this->tableDao($addressContactPersonDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("easypost_address_id", "integer", ["signed" => false])
            ->addColumn("contact_person_id", "integer", ["signed" => false])
            ->addForeignKey("easypost_address_id", $addressTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("contact_person_id", $contactPersonTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $addressOrganizationDao =& EasyPostAddressOrganizationDAO::instance($connection);
        $organizationTbl = OrganizationDAO::instance($connection)->formatSchemaAndTableName();
        $this->tableDao($addressOrganizationDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false])
            ->addColumn("easypost_address_id", "integer", ["signed" => false])
            ->addColumn("organization_id", "integer", ["signed" => false])
            ->addForeignKey("easypost_address_id", $addressTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("organization_id", $organizationTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(EasyPostAddressOrganizationDAO::instance($connection));
        $this->dropDao(EasyPostAddressContactPersonDAO::instance($connection));
        $this->dropDao(EasyPostAddressContactAddressDAO::instance($connection));
    }
}
