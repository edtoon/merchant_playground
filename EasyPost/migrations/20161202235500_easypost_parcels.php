<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\EasyPost\EasyPostAccountDAO;
use BlakePrinting\EasyPost\EasyPostParcelDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class EasyPostParcels extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $parcelDao =& EasyPostParcelDAO::instance($connection);
        $accountTbl = EasyPostAccountDAO::instance($connection)->formatSchemaAndTableName();
        $this->tableDao($parcelDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("easypost_account_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created_at", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updated_at", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("length", "decimal", ["null" => true, "precision" => 5, "scale" => 1])
            ->addColumn("width", "decimal", ["null" => true, "precision" => 5, "scale" => 1])
            ->addColumn("height", "decimal", ["null" => true, "precision" => 5, "scale" => 1])
            ->addColumn("weight", "decimal", ["null" => true, "precision" => 5, "scale" => 1])
            ->addForeignKey("easypost_account_id", $accountTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("created_at")
            ->addIndex("updated_at")
            ->addIndex("length")
            ->addIndex("width")
            ->addIndex("height")
            ->addIndex("weight")
            ->save()
        ;
        $this->alterDao($parcelDao, "ADD identifier BINARY(16) NOT NULL, ADD KEY (identifier)");
        $this->alterDao($parcelDao, "ADD UNIQUE (easypost_account_id, identifier)");
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(EasyPostParcelDAO::instance($connection));
    }
}
