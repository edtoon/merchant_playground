<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\EasyPost\EasyPostAccountDAO;
use BlakePrinting\EasyPost\EasyPostAddressDAO;
use BlakePrinting\EasyPost\EasyPostAddressVerificationDAO;
use BlakePrinting\EasyPost\EasyPostAddressVerificationErrorDAO;
use BlakePrinting\EasyPost\EasyPostVerificationErrorDAO;
use BlakePrinting\EasyPost\EasyPostVerificationFieldDAO;
use BlakePrinting\EasyPost\EasyPostVerificationTypeDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class EasyPost extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $accountDao =& EasyPostAccountDAO::instance($connection);
        $accountTbl = $accountDao->formatSchemaAndTableName();
        $this->tableDao($accountDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("code", "string", ["limit" => 191])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("code", ["unique" => true])
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $addressDao =& EasyPostAddressDAO::instance($connection);
        $addressTbl = $addressDao->formatSchemaAndTableName();
        $this->tableDao($addressDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("easypost_account_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created_at", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updated_at", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("street1", "string", ["null" => true, "limit" => 191])
            ->addColumn("street2", "string", ["null" => true, "limit" => 191])
            ->addColumn("city", "string", ["null" => true, "limit" => 191])
            ->addColumn("state", "string", ["null" => true, "limit" => 191])
            ->addColumn("zip", "string", ["null" => true, "limit" => 191])
            ->addColumn("country", "string", ["null" => true, "limit" => 191])
            ->addColumn("residential", "boolean", ["null" => true])
            ->addColumn("carrier_facility", "string", ["null" => true, "limit" => 191])
            ->addColumn("name", "string", ["null" => true, "limit" => 191])
            ->addColumn("company", "string", ["null" => true, "limit" => 191])
            ->addColumn("phone", "string", ["null" => true, "limit" => 191])
            ->addColumn("email", "string", ["null" => true, "limit" => 191])
            ->addColumn("federal_tax_id", "string", ["null" => true, "limit" => 191])
            ->addColumn("state_tax_id", "string", ["null" => true, "limit" => 191])
            ->addForeignKey("easypost_account_id", $accountTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("created_at")
            ->addIndex("updated_at")
            ->addIndex("street1")
            ->addIndex("street2")
            ->addIndex("city")
            ->addIndex("state")
            ->addIndex("zip")
            ->addIndex("country")
            ->addIndex("residential")
            ->addIndex("carrier_facility")
            ->addIndex("name")
            ->addIndex("company")
            ->addIndex("phone")
            ->addIndex("email")
            ->addIndex("federal_tax_id")
            ->addIndex("state_tax_id")
            ->save()
        ;
        $this->alterDao($addressDao, "ADD identifier BINARY(16) NOT NULL, ADD KEY (identifier)");
        $this->alterDao($addressDao, "ADD UNIQUE (easypost_account_id, identifier)");

        $verificationTypeDao =& EasyPostVerificationTypeDAO::instance($connection);
        $verificationTypeTbl = $verificationTypeDao->formatSchemaAndTableName();
        $this->tableDao($verificationTypeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $verificationFieldDao =& EasyPostVerificationFieldDAO::instance($connection);
        $verificationFieldTbl = $verificationFieldDao->formatSchemaAndTableName();
        $this->tableDao($verificationFieldDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $verificationErrorDao =& EasyPostVerificationErrorDAO::instance($connection);
        $verificationErrorTbl = $verificationErrorDao->formatSchemaAndTableName();
        $this->tableDao($verificationErrorDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("easypost_verification_field_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("error", "text")
            ->addForeignKey("easypost_verification_field_id", $verificationFieldTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->save()
        ;
        $this->alterDao($verificationErrorDao, "ADD INDEX (error(191))");

        $addressVerificationDao =& EasyPostAddressVerificationDAO::instance($connection);
        $addressVerificationTbl = $addressVerificationDao->formatSchemaAndTableName();
        $this->tableDao($addressVerificationDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("easypost_address_id", "integer", ["signed" => false])
            ->addColumn("easypost_verification_type_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("success", "boolean")
            ->addColumn("latitude", "decimal", ["null" => true, "precision" => 7, "scale" => 5])
            ->addColumn("longitude", "decimal", ["null" => true, "precision" => 8, "scale" => 5])
            ->addForeignKey("easypost_address_id", $addressTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("easypost_verification_type_id", $verificationTypeTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex(["easypost_address_id", "easypost_verification_type_id"], ["unique" => true])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("success")
            ->addIndex("latitude")
            ->addIndex("longitude")
            ->save()
        ;

        $errorDao =& EasyPostAddressVerificationErrorDAO::instance($connection);
        $this->tableDao($errorDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["easypost_address_verification_id", "easypost_verification_error_id"]])
            ->addColumn("easypost_address_verification_id", "biginteger", ["signed" => false])
            ->addColumn("easypost_verification_error_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addForeignKey("easypost_address_verification_id", $addressVerificationTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("easypost_verification_error_id", $verificationErrorTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(EasyPostAddressVerificationErrorDAO::instance($connection));
        $this->dropDao(EasyPostAddressVerificationDAO::instance($connection));
        $this->dropDao(EasyPostVerificationErrorDAO::instance($connection));
        $this->dropDao(EasyPostVerificationFieldDAO::instance($connection));
        $this->dropDao(EasyPostVerificationTypeDAO::instance($connection));
        $this->dropDao(EasyPostAddressDAO::instance($connection));
        $this->dropDao(EasyPostAccountDAO::instance($connection));
    }
}
