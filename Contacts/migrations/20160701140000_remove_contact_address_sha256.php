<?php

use BlakePrinting\Contacts\ContactAddressDAO;
use BlakePrinting\Db\DbMigration;

class RemoveContactAddressSha256 extends DbMigration {
    public function up() {
        $contactAddressDao =& ContactAddressDAO::instance($this->getConnection());

        $this->tableDao($contactAddressDao)
            ->removeIndex("sha256")
            ->removeColumn("sha256")
            ->save()
        ;
    }

    public function down() {
        $contactAddressDao =& ContactAddressDAO::instance($this->getConnection());

        $this->alterDao($contactAddressDao, "ADD sha256 BINARY(32) NOT NULL, ADD UNIQUE (sha256)");
    }
}
