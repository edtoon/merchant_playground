<?php

use BlakePrinting\Contacts\ContactPersonTypeDAO;
use BlakePrinting\Db\DbMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class ContactPersonTypeNameLimit extends DbMigration {
    public function up() {
        $this->alterDao(ContactPersonTypeDAO::instance($this->getConnection()), "MODIFY name VARCHAR(191) NOT NULL");
    }

    public function down() {
        $this->alterDao(ContactPersonTypeDAO::instance($this->getConnection()), "MODIFY name VARCHAR(10) NOT NULL");
    }
}
