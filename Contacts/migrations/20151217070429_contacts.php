<?php

use BlakePrinting\Contacts\ContactAddressDAO;
use BlakePrinting\Contacts\ContactAddressPhoneDAO;
use BlakePrinting\Contacts\ContactPersonAddressDAO;
use BlakePrinting\Contacts\ContactPersonDAO;
use BlakePrinting\Contacts\ContactPersonPersonTypeDAO;
use BlakePrinting\Contacts\ContactPersonPhoneDAO;
use BlakePrinting\Contacts\ContactPersonTypeDAO;
use BlakePrinting\Contacts\ContactPhoneDAO;
use BlakePrinting\Contacts\ContactPhoneTypeDAO;
use BlakePrinting\Db\DbMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class Contacts extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $contactAddressDao = new ContactAddressDAO($connection);
        $this->tableDao($contactAddressDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("line1", "string", ["limit" => 191, "null" => true])
            ->addColumn("line2", "string", ["limit" => 191, "null" => true])
            ->addColumn("line3", "string", ["limit" => 191, "null" => true])
            ->addColumn("city", "string", ["limit" => 191, "null" => true])
            ->addColumn("district", "string", ["limit" => 191, "null" => true])
            ->addColumn("state_or_region", "string", ["limit" => 191, "null" => true])
            ->addColumn("postal_code", "string", ["limit" => 191, "null" => true])
            ->addColumn("country_code", "string", ["limit" => 2, "null" => true])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("line1")
            ->addIndex("line2")
            ->addIndex("line3")
            ->addIndex("city")
            ->addIndex("district")
            ->addIndex("state_or_region")
            ->addIndex("postal_code")
            ->addIndex("country_code")
            ->save()
        ;
        $this->alterDao($contactAddressDao, "ADD sha256 BINARY(32) NOT NULL");
        $this->alterDao($contactAddressDao, "ADD UNIQUE (sha256)");

        $contactPersonTypeDao = new ContactPersonTypeDAO($connection);
        $this->tableDao($contactPersonTypeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("name", "string", ["limit" => 10])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $contactPersonDao = new ContactPersonDAO($connection);
        $this->tableDao($contactPersonDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name")
            ->save()
        ;

        $contactPersonPersonTypeDao = new ContactPersonPersonTypeDAO($connection);
        $contactPersonTypeTbl = $contactPersonTypeDao->formatSchemaAndTableName();
        $contactPersonTbl = $contactPersonDao->formatSchemaAndTableName();
        $this->tableDao($contactPersonPersonTypeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["person_id", "person_type_id"]])
            ->addColumn("person_id", "integer", ["signed" => false])
            ->addColumn("person_type_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addForeignKey("person_id", $contactPersonTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("person_type_id", $contactPersonTypeTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $contactPhoneTypeDao = new ContactPhoneTypeDAO($connection);
        $this->tableDao($contactPhoneTypeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $contactPhoneDao = new ContactPhoneDAO($connection);
        $contactPhoneTypeTbl = $contactPhoneTypeDao->formatSchemaAndTableName();
        $this->tableDao($contactPhoneDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("phone_type_id", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("phone", "string", ["limit" => 191])
            ->addForeignKey("phone_type_id", $contactPhoneTypeTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("phone", ["unique" => true])
            ->save()
        ;

        $contactAddressPhoneDao = new ContactAddressPhoneDAO($connection);
        $contactAddressTbl = $contactAddressDao->formatSchemaAndTableName();
        $contactPhoneTbl = $contactPhoneDao->formatSchemaAndTableName();
        $this->tableDao($contactAddressPhoneDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["address_id", "phone_id"]])
            ->addColumn("address_id", "integer", ["signed" => false])
            ->addColumn("phone_id", "integer", ["signed" => false])
            ->addForeignKey("address_id", $contactAddressTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("phone_id", $contactPhoneTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $contactPersonAddressDao = new ContactPersonAddressDAO($connection);
        $this->tableDao($contactPersonAddressDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["person_id", "address_id"]])
            ->addColumn("person_id", "integer", ["signed" => false])
            ->addColumn("address_id", "integer", ["signed" => false])
            ->addForeignKey("person_id", $contactPersonTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("address_id", $contactAddressTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $contactPersonPhoneDao = new ContactPersonPhoneDAO($connection);
        $this->tableDao($contactPersonPhoneDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["person_id", "phone_id"]])
            ->addColumn("person_id", "integer", ["signed" => false])
            ->addColumn("phone_id", "integer", ["signed" => false])
            ->addForeignKey("person_id", $contactPersonTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("phone_id", $contactPhoneTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(new ContactPersonPhoneDAO($connection));
        $this->dropDao(new ContactPersonAddressDAO($connection));
        $this->dropDao(new ContactAddressPhoneDAO($connection));
        $this->dropDao(new ContactPhoneDAO($connection));
        $this->dropDao(new ContactPhoneTypeDAO($connection));
        $this->dropDao(new ContactPersonPersonTypeDAO($connection));
        $this->dropDao(new ContactPersonDAO($connection));
        $this->dropDao(new ContactPersonTypeDAO($connection));
        $this->dropDao(new ContactAddressDAO($connection));
    }
}
