<?php

use BlakePrinting\Contacts\ContactAddressDAO;
use BlakePrinting\Contacts\ContactAddressDataFeedDAO;
use BlakePrinting\Contacts\ContactPersonDAO;
use BlakePrinting\Contacts\ContactPersonDataFeedDAO;
use BlakePrinting\Contacts\ContactPhoneDAO;
use BlakePrinting\Contacts\ContactPhoneDataFeedDAO;
use BlakePrinting\DataSources\DataFeedDAO;
use BlakePrinting\Db\DbMigration;

class ContactSources extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();
        $dataFeedDao =& DataFeedDAO::instance($connection);
        $dataFeedTbl = $dataFeedDao->formatSchemaAndTableName();

        $contactAddressDataFeedDao = ContactAddressDataFeedDAO::instance($connection);
        $contactAddressDao = ContactAddressDAO::instance($connection);
        $this->tableDao($contactAddressDataFeedDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["address_id", "data_feed_id"]])
            ->addColumn("address_id", "integer", ["signed" => false])
            ->addColumn("data_feed_id", "integer", ["signed" => false])
            ->addForeignKey("address_id", $contactAddressDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("data_feed_id", $dataFeedTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $contactPersonDataFeedDao = ContactPersonDataFeedDAO::instance($connection);
        $contactPersonDao = ContactPersonDAO::instance($connection);
        $this->tableDao($contactPersonDataFeedDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["person_id", "data_feed_id"]])
            ->addColumn("person_id", "integer", ["signed" => false])
            ->addColumn("data_feed_id", "integer", ["signed" => false])
            ->addForeignKey("person_id", $contactPersonDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("data_feed_id", $dataFeedTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;

        $contactPhoneDataFeedDao = ContactPhoneDataFeedDAO::instance($connection);
        $contactPhoneDao = ContactPhoneDAO::instance($connection);
        $this->tableDao($contactPhoneDataFeedDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["phone_id", "data_feed_id"]])
            ->addColumn("phone_id", "integer", ["signed" => false])
            ->addColumn("data_feed_id", "integer", ["signed" => false])
            ->addForeignKey("phone_id", $contactPhoneDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("data_feed_id", $dataFeedTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(ContactPhoneDataFeedDAO::instance($connection));
        $this->dropDao(ContactPersonDataFeedDAO::instance($connection));
        $this->dropDao(ContactAddressDataFeedDAO::instance($connection));
    }
}
