<?php

use BlakePrinting\Contacts\ContactAddressDAO;
use BlakePrinting\Db\DbMigration;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Globalization\CountryDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class ContactAddressCountryId extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();
        $countryDao =& CountryDAO::instance($connection);
        $countryTbl = $countryDao->formatSchemaAndTableName();

        $contactAddressDao =& ContactAddressDAO::instance($connection);
        $this->tableDao($contactAddressDao)
            ->addColumn("country_id", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->save()
        ;
        $this->alterDao(
            $contactAddressDao,
            "ADD CONSTRAINT " . $contactAddressDao->getTableName() . "_ibfk_country_id FOREIGN KEY (country_id) REFERENCES " . $countryTbl . " (id)"
        );
        PDOQuery::update()
            ->table($contactAddressDao)
            ->set("country_id", "(SELECT id FROM " . $countryTbl . " WHERE iso_code = country_code)")
            ->executeGetRowCount($connection)
        ;
        $this->tableDao($contactAddressDao)
            ->removeIndex("country_code")
            ->removeColumn("country_code")
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();
        $countryTbl = CountryDAO::instance($connection)->formatSchemaAndTableName();

        $contactAddressDao =& ContactAddressDAO::instance($connection);
        $this->tableDao($contactAddressDao)
            ->addColumn("country_code", "string", ["limit" => 2, "null" => true])
            ->addIndex("country_code")
            ->save()
        ;
        PDOQuery::update()
            ->table($contactAddressDao)
            ->set("country_code", "(SELECT iso_code FROM " . $countryTbl . " WHERE id = country_id)")
            ->executeGetRowCount($connection)
        ;
        $this->alterDao(
            $contactAddressDao,
            "DROP FOREIGN KEY " . $contactAddressDao->getTableName() . "_ibfk_country_id, " .
            "DROP COLUMN country_id"
        );
    }
}
