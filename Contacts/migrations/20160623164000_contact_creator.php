<?php

use BlakePrinting\Contacts\ContactAddressDAO;
use BlakePrinting\Contacts\ContactPersonDAO;
use BlakePrinting\Contacts\ContactPersonTypeDAO;
use BlakePrinting\Contacts\ContactPhoneDAO;
use BlakePrinting\Contacts\ContactPhoneTypeDAO;
use BlakePrinting\Db\DataAccessObject;
use BlakePrinting\Db\DbMigration;
use BlakePrinting\Users\UserDAO;

class ContactCreator extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();
        $userTbl = UserDAO::instance($connection)->formatSchemaAndTableName();
        $daoList = [
            ContactAddressDAO::instance($connection),
            ContactPersonTypeDAO::instance($connection),
            ContactPersonDAO::instance($connection),
            ContactPhoneTypeDAO::instance($connection),
            ContactPhoneDAO::instance($connection)
        ];

        foreach($daoList as &$dao) {
            if($dao instanceof DataAccessObject) {
                $tableName = $dao->getTableName();

                $this->alterDao(
                    $dao,
                    "ADD creator SMALLINT(5) UNSIGNED, " .
                    "ADD CONSTRAINT " . $tableName . "_ibfk_creator FOREIGN KEY (creator) REFERENCES " . $userTbl . " (id), " .
                    "ADD updater SMALLINT(5) UNSIGNED, " .
                    "ADD CONSTRAINT " . $tableName . "_ibfk_updater FOREIGN KEY (updater) REFERENCES " . $userTbl . " (id)"
                );
            }
        }

        unset($dao);
    }

    public function down() {
        $connection =& $this->getConnection();
        $daoList = [
            ContactAddressDAO::instance($connection),
            ContactPersonTypeDAO::instance($connection),
            ContactPersonDAO::instance($connection),
            ContactPhoneTypeDAO::instance($connection),
            ContactPhoneDAO::instance($connection)
        ];

        for($i = (count($daoList) - 1); $i >= 0; $i--) {
            $dao = $daoList[$i];
            if($dao instanceof DataAccessObject) {
                $tableName = $dao->getTableName();

                $this->alterDao(
                    $dao,
                    "DROP FOREIGN KEY " . $tableName . "_ibfk_updater, " .
                    "DROP COLUMN updater, " .
                    "DROP FOREIGN KEY " . $tableName . "_ibfk_creator, " .
                    "DROP COLUMN creator"
                );
            }
        }
    }
}
