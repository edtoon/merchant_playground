<?php

use BlakePrinting\Contacts\ContactAddressAddressTypeDAO;
use BlakePrinting\Contacts\ContactAddressDAO;
use BlakePrinting\Contacts\ContactAddressTypeDAO;
use BlakePrinting\Db\DbMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class ContactAddressTypes extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $contactAddressTypeDao =& ContactAddressTypeDAO::instance($connection);
        $contactAddressTypeTbl = $contactAddressTypeDao->formatSchemaAndTableName();
        $this->tableDao($contactAddressTypeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("name", "string", ["limit" => 10])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $contactAddressAddressTypeDao =& ContactAddressAddressTypeDAO::instance($connection);
        $contactAddressDao =& ContactAddressDAO::instance($connection);
        $this->tableDao($contactAddressAddressTypeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["address_id", "address_type_id"]])
            ->addColumn("address_id", "integer", ["signed" => false])
            ->addColumn("address_type_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addForeignKey("address_id", $contactAddressDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("address_type_id", $contactAddressTypeTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(ContactAddressAddressTypeDAO::instance($connection));
        $this->dropDao(ContactAddressTypeDAO::instance($connection));
    }
}
