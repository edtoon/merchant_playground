<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\ValueObject;

class ContactPersonPersonTypeVO implements ValueObject {
    /** @var int */
    private $personId = null;
    /** @var int */
    private $personTypeId = null;

    /**
     * @return int
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * @param int $personId
     */
    public function setPersonId($personId)
    {
        $this->personId = $personId;
    }

    /**
     * @return int
     */
    public function getPersonTypeId()
    {
        return $this->personTypeId;
    }

    /**
     * @param int $personTypeId
     */
    public function setPersonTypeId($personTypeId)
    {
        $this->personTypeId = $personTypeId;
    }
}
