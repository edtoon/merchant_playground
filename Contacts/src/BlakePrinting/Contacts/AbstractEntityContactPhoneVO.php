<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\ValueObject;

abstract class AbstractEntityContactPhoneVO implements ValueObject {
    /** @var int */
    private $contactPhoneId = null;

    /**
     * @param int $contactPhoneId
     */
    public function __construct($contactPhoneId = null) {
        $this->contactPhoneId = $contactPhoneId;
    }

    /**
     * @return int
     */
    public function getContactPhoneId()
    {
        return $this->contactPhoneId;
    }

    /**
     * @param int $contactPhoneId
     */
    public function setContactPhoneId($contactPhoneId)
    {
        $this->contactPhoneId = $contactPhoneId;
    }
}
