<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\DataSources\DataFeedVO;
use BlakePrinting\Util\Utils;
use PDO;

class ContactUtil {
    private function __construct() { }

    /**
     * @param ContactAddressVO $prototype
     * @param PDO $connection
     * @param DataFeedVO $dataFeedVo
     * @return ContactAddressVO
     */
    public static function &getContactAddressVoByPrototype(ContactAddressVO $prototype, PDO &$connection = null, DataFeedVO &$dataFeedVo = null) {
        $contactAddressDao =& ContactAddressDAO::instance($connection);

        $prototype->setLine1(Utils::getTrimmedStringOrNull($prototype->getLine1()));
        $prototype->setLine2(Utils::getTrimmedStringOrNull($prototype->getLine2()));
        $prototype->setLine3(Utils::getTrimmedStringOrNull($prototype->getLine3()));
        $prototype->setCity(Utils::getTrimmedStringOrNull($prototype->getCity()));
        $prototype->setDistrict(Utils::getTrimmedStringOrNull($prototype->getDistrict()));
        $prototype->setStateOrRegion(Utils::getTrimmedStringOrNull($prototype->getStateOrRegion()));
        $prototype->setPostalCode(Utils::getTrimmedStringOrNull($prototype->getPostalCode()));

        $contactAddressVo =& $contactAddressDao->findOrCreate(
            $prototype, "FOR UPDATE",
            ["line1", "line2", "line3", "city", "district", "state_or_region", "postal_code", "country_id"],
            ["created", "creator", "updated", "updater"]
        );

        if($dataFeedVo !== null) {
            $contactAddressDataFeedDao =& ContactAddressDataFeedDAO::instance($connection);

            $contactAddressDataFeedDao->store($contactAddressVo->getId(), $dataFeedVo->getId());
        }

        return $contactAddressVo;
    }
}
