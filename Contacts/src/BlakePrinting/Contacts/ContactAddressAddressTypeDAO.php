<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ContactAddressAddressTypeDAO instance(PDO $connection = null, $schemaName = null)
 * @method ContactAddressAddressTypeVO findById($id, $options = null, $useCache = false)
 * @method ContactAddressAddressTypeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactAddressAddressTypeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactAddressAddressTypeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ContactAddressAddressTypeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactAddressAddressTypeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactAddressAddressTypeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ContactAddressAddressTypeDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $contactAddressId
     * @param int $contactAddressTypeId
     * @return int
     */
    public function &store($contactAddressId, $contactAddressTypeId) {
        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value("address_id", $contactAddressId, PDO::PARAM_STR)
            ->value("address_type_id", $contactAddressTypeId, PDO::PARAM_STR)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
