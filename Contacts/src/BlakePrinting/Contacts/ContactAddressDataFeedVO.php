<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\DataSources\AbstractEntityDataFeedVO;

class ContactAddressDataFeedVO extends AbstractEntityDataFeedVO {
    /** @var int */
    private $addressId = null;

    public function __construct($addressId = null, $dataFeedId = null) {
        parent::__construct($dataFeedId);

        $this->addressId = $addressId;
    }

    /**
     * @return int
     */
    public function getAddressId()
    {
        return $this->addressId;
    }

    /**
     * @param int $addressId
     */
    public function setAddressId($addressId)
    {
        $this->addressId = $addressId;
    }
}
