<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\ValueObject;

class ContactAddressPhoneVO implements ValueObject {
    /** @var int */
    private $addressId = null;
    /** @var int */
    private $phoneId = null;

    /**
     * @return int
     */
    public function getAddressId()
    {
        return $this->addressId;
    }

    /**
     * @param int $addressId
     */
    public function setAddressId($addressId)
    {
        $this->addressId = $addressId;
    }

    /**
     * @return int
     */
    public function getPhoneId()
    {
        return $this->phoneId;
    }

    /**
     * @param int $phoneId
     */
    public function setPhoneId($phoneId)
    {
        $this->phoneId = $phoneId;
    }
}
