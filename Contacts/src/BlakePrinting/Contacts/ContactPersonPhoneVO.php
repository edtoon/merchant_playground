<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\ValueObject;

class ContactPersonPhoneVO implements ValueObject {
    /** @var int */
    private $personId = null;
    /** @var int */
    private $phoneId = null;

    /**
     * @return int
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * @param int $personId
     */
    public function setPersonId($personId)
    {
        $this->personId = $personId;
    }

    /**
     * @return int
     */
    public function getPhoneId()
    {
        return $this->phoneId;
    }

    /**
     * @param int $phoneId
     */
    public function setPhoneId($phoneId)
    {
        $this->phoneId = $phoneId;
    }
}
