<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ContactPersonDataFeedDAO instance(PDO $connection = null, $schemaName = null)
 * @method ContactPersonDataFeedVO findById($id, $options = null, $useCache = false)
 * @method ContactPersonDataFeedVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactPersonDataFeedVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactPersonDataFeedVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ContactPersonDataFeedVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactPersonDataFeedVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactPersonDataFeedVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ContactPersonDataFeedDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $personId
     * @param int $dataFeedId
     * @return int
     */
    public function &store($personId, $dataFeedId) {
        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value("person_id", $personId, PDO::PARAM_INT)
            ->value("data_feed_id", $dataFeedId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
