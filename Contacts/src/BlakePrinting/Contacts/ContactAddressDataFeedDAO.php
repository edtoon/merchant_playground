<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ContactAddressDataFeedDAO instance(PDO $connection = null, $schemaName = null)
 * @method ContactAddressDataFeedVO findById($id, $options = null, $useCache = false)
 * @method ContactAddressDataFeedVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactAddressDataFeedVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactAddressDataFeedVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ContactAddressDataFeedVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactAddressDataFeedVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactAddressDataFeedVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ContactAddressDataFeedDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $addressId
     * @param int $dataFeedId
     * @return int
     */
    public function &store($addressId, $dataFeedId) {
        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value("address_id", $addressId, PDO::PARAM_INT)
            ->value("data_feed_id", $dataFeedId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
