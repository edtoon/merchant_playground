<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ContactPhoneTypeDAO instance(PDO $connection = null, $schemaName = null)
 * @method ContactPhoneTypeVO findById($id, $options = null, $useCache = false)
 * @method ContactPhoneTypeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactPhoneTypeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactPhoneTypeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ContactPhoneTypeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactPhoneTypeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactPhoneTypeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ContactPhoneTypeDAO extends AbstractDataAccessObject {
    const PHONE_TYPE_HOME = "home";
    const PHONE_TYPE_OFFICE = "office";
    const PHONE_TYPE_FAX = "fax";
    const PHONE_TYPE_MOBILE = "mobile";
}
