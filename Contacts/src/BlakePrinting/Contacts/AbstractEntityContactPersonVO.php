<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\ValueObject;

abstract class AbstractEntityContactPersonVO implements ValueObject {
    /** @var int */
    private $contactPersonId = null;

    /**
     * @param int $contactPersonId
     */
    public function __construct($contactPersonId = null) {
        $this->contactPersonId = $contactPersonId;
    }

    /**
     * @return int
     */
    public function getContactPersonId()
    {
        return $this->contactPersonId;
    }

    /**
     * @param int $contactPersonId
     */
    public function setContactPersonId($contactPersonId)
    {
        $this->contactPersonId = $contactPersonId;
    }
}
