<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\ValueObject;

abstract class AbstractEntityContactAddressVO implements ValueObject {
    /** @var int */
    private $contactAddressId = null;

    /**
     * @param int $contactAddressId
     */
    public function __construct($contactAddressId = null) {
        $this->contactAddressId = $contactAddressId;
    }

    /**
     * @return int
     */
    public function getContactAddressId()
    {
        return $this->contactAddressId;
    }

    /**
     * @param int $contactAddressId
     */
    public function setContactAddressId($contactAddressId)
    {
        $this->contactAddressId = $contactAddressId;
    }
}
