<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\AbstractExtensibleEntity;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use Exception;
use Stringy\StaticStringy as S;

trait EntityContactPersonsTrait {
    /** @var ContactPersonVO[] */
    private $contactPersons = [];
    /** @var AbstractDataAccessObject */
    private $entityContactPersonDao = null;

    public function replaceContactPersons() {
        $this->updateContactPersons(true);
    }

    /**
     * @param bool $removePrevious
     * @throws Exception
     */
    public function updateContactPersons($removePrevious = true) {
        if(!$this instanceof AbstractExtensibleEntity) {
            throw new Exception("Tried to replace contact persons of object that's not an extensible entity: " . get_class($this));
        } else {
            $entityColumnName = $this->getEntityColumnName();
            $entityColumnValue = $this->getEntityColumnValue();
            $contactPersonIds = [];

            if($this->entityContactPersonDao === null) {
                $this->entityContactPersonDao = $this->getExtensionDAO("ContactPerson");
            }

            if(!empty($this->contactPersons)) {
                $voReflectionClass = $this->entityContactPersonDao->getVoReflectionClass();
                $setEntityMethod = $voReflectionClass->getMethod("set" . S::upperCamelize($entityColumnName));
                $setContactPersonIdMethod = $voReflectionClass->getMethod("setContactPersonId");

                foreach($this->contactPersons as &$contactPersonVo) {
                    $contactPersonId = $contactPersonVo->getId();
                    $contactPersonIds[] = $contactPersonId;
                    $vo = $voReflectionClass->newInstance();

                    if($vo instanceof ValueObject) {
                        $setEntityMethod->invoke($vo, $entityColumnValue);
                        $setContactPersonIdMethod->invoke($vo, $contactPersonId);

                        $this->entityContactPersonDao->insert($vo, null, true);
                    }
                }

                unset($contactPersonVo);
            }

            if($removePrevious) {
                $delete = PDOQuery::delete()
                    ->from($this->entityContactPersonDao)
                    ->where($entityColumnName, $entityColumnValue, $this->getEntityColumnType())
                ;

                if(!empty($contactPersonIds)) {
                    $delete->where("contact_person_id NOT IN (" . implode(",", $contactPersonIds) . ")");
                }

                $delete->executeGetRowCount($this->entityContactPersonDao->getConnection());
            }
        }
    }

    /**
     * @param ContactPersonVO $contactPersonVo
     */
    public function addContactPerson(ContactPersonVO $contactPersonVo) {
        $this->contactPersons[] = $contactPersonVo;
    }

    /**
     * @return ContactPersonVO[]
     */
    public function getContactPersons() {
        return $this->contactPersons;
    }

    /**
     * @return AbstractDataAccessObject
     */
    public function getEntityContactPersonDao()
    {
        return $this->entityContactPersonDao;
    }

    /**
     * @param AbstractDataAccessObject $entityContactPersonDao
     */
    public function setEntityContactPersonDao(AbstractDataAccessObject $entityContactPersonDao)
    {
        $this->entityContactPersonDao = $entityContactPersonDao;
    }
}
