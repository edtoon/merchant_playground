<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\ValueObject;

class ContactAddressAddressTypeVO implements ValueObject {
    /** @var int */
    private $addressId = null;
    /** @var int */
    private $addressTypeId = null;

    /**
     * @return int
     */
    public function getAddressId()
    {
        return $this->addressId;
    }

    /**
     * @param int $addressId
     */
    public function setAddressId($addressId)
    {
        $this->addressId = $addressId;
    }

    /**
     * @return int
     */
    public function getAddressTypeId()
    {
        return $this->addressTypeId;
    }

    /**
     * @param int $addressTypeId
     */
    public function setAddressTypeId($addressTypeId)
    {
        $this->addressTypeId = $addressTypeId;
    }
}
