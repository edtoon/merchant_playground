<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\AbstractExtensibleEntity;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use Exception;
use Stringy\StaticStringy as S;

trait EntityContactAddressesTrait {
    /** @var ContactAddressVO[] */
    private $contactAddresses = [];
    /** @var AbstractDataAccessObject */
    private $entityContactAddressDao = null;

    public function replaceContactAddresses() {
        $this->updateContactAddresses(true);
    }

    /**
     * @param bool $removePrevious
     * @throws Exception
     */
    public function updateContactAddresses($removePrevious = true) {
        if(!$this instanceof AbstractExtensibleEntity) {
            throw new Exception("Tried to replace contact addresses of object that's not an extensible entity: " . get_class($this));
        } else {
            $entityColumnName = $this->getEntityColumnName();
            $entityColumnValue = $this->getEntityColumnValue();
            $contactAddressIds = [];

            if($this->entityContactAddressDao === null) {
                $this->entityContactAddressDao = $this->getExtensionDAO("ContactAddress");
            }

            if(!empty($this->contactAddresses)) {
                $voReflectionClass = $this->entityContactAddressDao->getVoReflectionClass();
                $setEntityMethod = $voReflectionClass->getMethod("set" . S::upperCamelize($entityColumnName));
                $setContactAddressIdMethod = $voReflectionClass->getMethod("setContactAddressId");

                foreach($this->contactAddresses as &$contactAddressVo) {
                    $contactAddressId = $contactAddressVo->getId();
                    $contactAddressIds[] = $contactAddressId;
                    $vo = $voReflectionClass->newInstance();

                    if($vo instanceof ValueObject) {
                        $setEntityMethod->invoke($vo, $entityColumnValue);
                        $setContactAddressIdMethod->invoke($vo, $contactAddressId);

                        $this->entityContactAddressDao->insert($vo, null, true);
                    }
                }

                unset($contactAddressVo);
            }

            if($removePrevious) {
                $delete = PDOQuery::delete()
                    ->from($this->entityContactAddressDao)
                    ->where($entityColumnName, $entityColumnValue, $this->getEntityColumnType())
                ;

                if(!empty($contactAddressIds)) {
                    $delete->where("contact_address_id NOT IN (" . implode(",", $contactAddressIds) . ")");
                }

                $delete->executeGetRowCount($this->entityContactAddressDao->getConnection());
            }
        }
    }

    /**
     * @param ContactAddressVO $contactAddressVo
     */
    public function addContactAddress(ContactAddressVO $contactAddressVo) {
        $this->contactAddresses[] = $contactAddressVo;
    }

    /**
     * @return ContactAddressVO[]
     */
    public function getContactAddresses() {
        return $this->contactAddresses;
    }

    /**
     * @return AbstractDataAccessObject
     */
    public function getEntityContactAddressDao()
    {
        return $this->entityContactAddressDao;
    }

    /**
     * @param AbstractDataAccessObject $entityContactAddressDao
     */
    public function setEntityContactAddressDao(AbstractDataAccessObject $entityContactAddressDao)
    {
        $this->entityContactAddressDao = $entityContactAddressDao;
    }
}
