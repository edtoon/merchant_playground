<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\DataSources\AbstractEntityDataFeedVO;

class ContactPhoneDataFeedVO extends AbstractEntityDataFeedVO {
    /** @var int */
    private $phoneId = null;

    public function __construct($phoneId = null, $dataFeedId = null) {
        parent::__construct($dataFeedId);

        $this->phoneId = $phoneId;
    }

    /**
     * @return int
     */
    public function getPhoneId()
    {
        return $this->phoneId;
    }

    /**
     * @param int $phoneId
     */
    public function setPhoneId($phoneId)
    {
        $this->phoneId = $phoneId;
    }
}
