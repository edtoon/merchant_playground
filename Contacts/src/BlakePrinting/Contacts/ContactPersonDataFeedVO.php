<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\DataSources\AbstractEntityDataFeedVO;

class ContactPersonDataFeedVO extends AbstractEntityDataFeedVO {
    /** @var int */
    private $personId = null;

    public function __construct($personId = null, $dataFeedId = null) {
        parent::__construct($dataFeedId);

        $this->personId = $personId;
    }

    /**
     * @return int
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * @param int $personId
     */
    public function setPersonId($personId)
    {
        $this->personId = $personId;
    }
}
