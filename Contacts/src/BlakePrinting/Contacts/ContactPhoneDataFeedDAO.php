<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ContactPhoneDataFeedDAO instance(PDO $connection = null, $schemaName = null)
 * @method ContactPhoneDataFeedVO findById($id, $options = null, $useCache = false)
 * @method ContactPhoneDataFeedVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactPhoneDataFeedVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactPhoneDataFeedVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ContactPhoneDataFeedVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactPhoneDataFeedVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactPhoneDataFeedVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ContactPhoneDataFeedDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $phoneId
     * @param int $dataFeedId
     * @return int
     */
    public function &store($phoneId, $dataFeedId) {
        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value("phone_id", $phoneId, PDO::PARAM_INT)
            ->value("data_feed_id", $dataFeedId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
