<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ContactAddressTypeDAO instance(PDO $connection = null, $schemaName = null)
 * @method ContactAddressTypeVO findById($id, $options = null, $useCache = false)
 * @method ContactAddressTypeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactAddressTypeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactAddressTypeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ContactAddressTypeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactAddressTypeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactAddressTypeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ContactAddressTypeDAO extends AbstractDataAccessObject {
    const ADDRESS_TYPE_SHIPPING = "shipping";
    const ADDRESS_TYPE_HOME = "home";
    const ADDRESS_TYPE_OFFICE = "office";
}
