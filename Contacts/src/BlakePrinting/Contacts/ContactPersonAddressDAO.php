<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ContactPersonAddressDAO instance(PDO $connection = null, $schemaName = null)
 * @method ContactPersonAddressVO findById($id, $options = null, $useCache = false)
 * @method ContactPersonAddressVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactPersonAddressVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactPersonAddressVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ContactPersonAddressVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactPersonAddressVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactPersonAddressVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ContactPersonAddressDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $personId
     * @param int $addressId
     * @return int
     */
    public function &store($personId, $addressId) {
        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value("person_id", $personId, PDO::PARAM_INT)
            ->value("address_id", $addressId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
