<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\ByteLimit;
use BlakePrinting\Db\ValueObject;
use Phinx\Db\Adapter\MysqlAdapter;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class ContactPhoneTypeVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $creator = null;
    /** @var int */
    private $updated = null;
    /** @var int */
    private $updater = null;
    /** @var string */
    private $name = null;

    public static function loadValidatorMetadata(ClassMetadata $metadata) {
        $metadata->addGetterConstraints(
            "id", [new Assert\Type("digit"), new Assert\Range(["min" => 1, "max" => MysqlAdapter::INT_TINY])]
        );
        $metadata->addGetterConstraints(
            "created", [new Assert\Type("digit"), new Assert\Range(["min" => 1, "max" => MysqlAdapter::INT_BIG])]
        );
        $metadata->addGetterConstraints(
            "creator", [new Assert\Type("digit"), new Assert\Range(["min" => 1, "max" => MysqlAdapter::INT_SMALL])]
        );
        $metadata->addGetterConstraints(
            "updated", [new Assert\Type("digit"), new Assert\Range(["min" => 1, "max" => MysqlAdapter::INT_BIG])]
        );
        $metadata->addGetterConstraints(
            "updater", [new Assert\Type("digit"), new Assert\Range(["min" => 1, "max" => MysqlAdapter::INT_SMALL])]
        );
        $metadata->addGetterConstraints(
            "name", [new Assert\NotNull(), new Assert\NotBlank(), new ByteLimit(191)]
        );
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param int $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getUpdater()
    {
        return $this->updater;
    }

    /**
     * @param int $updater
     */
    public function setUpdater($updater)
    {
        $this->updater = $updater;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
