<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ContactAddressPhoneDAO instance(PDO $connection = null, $schemaName = null)
 * @method ContactAddressPhoneVO findById($id, $options = null, $useCache = false)
 * @method ContactAddressPhoneVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactAddressPhoneVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactAddressPhoneVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ContactAddressPhoneVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactAddressPhoneVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactAddressPhoneVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ContactAddressPhoneDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $addressId
     * @param int $phoneId
     * @return int
     */
    public function &store($addressId, $phoneId) {
        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value("address_id", $addressId, PDO::PARAM_INT)
            ->value("phone_id", $phoneId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
