<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ContactPhoneDAO instance(PDO $connection = null, $schemaName = null)
 * @method ContactPhoneVO findById($id, $options = null, $useCache = false)
 * @method ContactPhoneVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactPhoneVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactPhoneVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ContactPhoneVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactPhoneVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactPhoneVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ContactPhoneDAO extends AbstractDataAccessObject {
}
