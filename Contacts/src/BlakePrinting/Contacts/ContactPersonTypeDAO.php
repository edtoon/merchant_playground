<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ContactPersonTypeDAO instance(PDO $connection = null, $schemaName = null)
 * @method ContactPersonTypeVO findById($id, $options = null, $useCache = false)
 * @method ContactPersonTypeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactPersonTypeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactPersonTypeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ContactPersonTypeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactPersonTypeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactPersonTypeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ContactPersonTypeDAO extends AbstractDataAccessObject {
    const PERSON_TYPE_PROSPECT = "prospect";
    const PERSON_TYPE_CUSTOMER = "customer";
    const PERSON_TYPE_SUPPLIER = "supplier";
    const PERSON_TYPE_VENDOR = "vendor";
    const PERSON_TYPE_PARTNER = "partner";
    const PERSON_TYPE_EMPLOYEE = "employee";
    const PERSON_TYPE_ADVISOR = "advisor";
    const PERSON_TYPE_DIRECTOR = "director";
    const PERSON_TYPE_OBSERVER = "observer";
}
