<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ContactAddressDAO instance(PDO $connection = null, $schemaName = null)
 * @method ContactAddressVO findById($id, $options = null, $useCache = false)
 * @method ContactAddressVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactAddressVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactAddressVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ContactAddressVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactAddressVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactAddressVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ContactAddressVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class ContactAddressDAO extends AbstractDataAccessObject {
}
