<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ContactPersonPhoneDAO instance(PDO $connection = null, $schemaName = null)
 * @method ContactPersonPhoneVO findById($id, $options = null, $useCache = false)
 * @method ContactPersonPhoneVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactPersonPhoneVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactPersonPhoneVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ContactPersonPhoneVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactPersonPhoneVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactPersonPhoneVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ContactPersonPhoneDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $personId
     * @param int $phoneId
     * @return int
     */
    public function &store($personId, $phoneId) {
        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value("person_id", $personId, PDO::PARAM_INT)
            ->value("phone_id", $phoneId, PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
