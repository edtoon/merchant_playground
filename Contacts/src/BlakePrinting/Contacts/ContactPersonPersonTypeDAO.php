<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static ContactPersonPersonTypeDAO instance(PDO $connection = null, $schemaName = null)
 * @method ContactPersonPersonTypeVO findById($id, $options = null, $useCache = false)
 * @method ContactPersonPersonTypeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactPersonPersonTypeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactPersonPersonTypeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method ContactPersonPersonTypeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method ContactPersonPersonTypeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method ContactPersonPersonTypeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class ContactPersonPersonTypeDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $contactPersonId
     * @param int $contactPersonTypeId
     * @return int
     */
    public function &store($contactPersonId, $contactPersonTypeId) {
        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value("person_id", $contactPersonId, PDO::PARAM_STR)
            ->value("person_type_id", $contactPersonTypeId, PDO::PARAM_STR)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
