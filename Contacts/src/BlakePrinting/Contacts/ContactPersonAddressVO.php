<?php

namespace BlakePrinting\Contacts;

use BlakePrinting\Db\ValueObject;

class ContactPersonAddressVO implements ValueObject {
    /** @var int */
    private $personId = null;
    /** @var int */
    private $addressId = null;

    /**
     * @return int
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * @param int $personId
     */
    public function setPersonId($personId)
    {
        $this->personId = $personId;
    }

    /**
     * @return int
     */
    public function getAddressId()
    {
        return $this->addressId;
    }

    /**
     * @param int $addressId
     */
    public function setAddressId($addressId)
    {
        $this->addressId = $addressId;
    }
}
