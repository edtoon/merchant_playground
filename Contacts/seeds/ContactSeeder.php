<?php

use BlakePrinting\Contacts\ContactAddressTypeDAO;
use BlakePrinting\Contacts\ContactAddressTypeVO;
use BlakePrinting\Contacts\ContactPersonTypeDAO;
use BlakePrinting\Contacts\ContactPersonTypeVO;
use BlakePrinting\Contacts\ContactPhoneTypeDAO;
use BlakePrinting\Contacts\ContactPhoneTypeVO;
use BlakePrinting\Db\DbSeed;
use BlakePrinting\Util\Utils;

class ContactSeeder extends DbSeed {
    public final function run() {
        $connection =& $this->getConnection();
        $contactAddressTypeDao =& ContactAddressTypeDAO::instance($connection);
        $contactPersonTypeDao =& ContactPersonTypeDAO::instance($connection);
        $contactPhoneTypeDao =& ContactPhoneTypeDAO::instance($connection);

        Utils::forEachConstantInObjectClass(
            $contactAddressTypeDao,
            function($_, $constantName, $constantValue) use (&$contactAddressTypeDao) {
                $contactAddressTypeVo =& $contactAddressTypeDao->findByColumn("name", $constantValue, PDO::PARAM_STR, null, null, "FOR UPDATE");

                if($contactAddressTypeVo === null) {
                    $contactAddressTypeVo = new ContactAddressTypeVO();

                    $contactAddressTypeVo->setName($constantValue);

                    $contactAddressTypeDao->insert($contactAddressTypeVo);
                }
            },
            "ADDRESS_TYPE_"
        );

        Utils::forEachConstantInObjectClass(
            $contactPersonTypeDao,
            function($_, $constantName, $constantValue) use (&$contactPersonTypeDao) {
                $contactPersonTypeVo =& $contactPersonTypeDao->findByColumn("name", $constantValue, PDO::PARAM_STR, null, null, "FOR UPDATE");

                if($contactPersonTypeVo === null) {
                    $contactPersonTypeVo = new ContactPersonTypeVO();

                    $contactPersonTypeVo->setName($constantValue);

                    $contactPersonTypeDao->insert($contactPersonTypeVo);
                }
            },
            "PERSON_TYPE_"
        );

        Utils::forEachConstantInObjectClass(
            $contactPhoneTypeDao,
            function($_, $constantName, $constantValue) use (&$contactPhoneTypeDao) {
                $contactPhoneTypeVo =& $contactPhoneTypeDao->findByColumn("name", $constantValue, PDO::PARAM_STR, null, null, "FOR UPDATE");

                if($contactPhoneTypeVo === null) {
                    $contactPhoneTypeVo = new ContactPhoneTypeVO();

                    $contactPhoneTypeVo->setName($constantValue);

                    $contactPhoneTypeDao->insert($contactPhoneTypeVo);
                }
            },
            "PHONE_TYPE_"
        );
    }
}
