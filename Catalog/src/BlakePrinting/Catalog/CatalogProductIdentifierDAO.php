<?php

namespace BlakePrinting\Catalog;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static CatalogProductIdentifierDAO instance(PDO $connection = null, $schemaName = null)
 * @method CatalogProductIdentifierVO findById($id, $options = null, $useCache = false)
 * @method CatalogProductIdentifierVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogProductIdentifierVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogProductIdentifierVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method CatalogProductIdentifierVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogProductIdentifierVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogProductIdentifierVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class CatalogProductIdentifierDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $productId
     * @param int $dataSourceIdentifierId
     * @param string $identifier
     * @return int
     */
    public function &store($productId, $dataSourceIdentifierId, $identifier) {
        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value("product_id", $productId, PDO::PARAM_INT)
            ->value("data_source_identifier_id", $dataSourceIdentifierId, PDO::PARAM_INT)
            ->value("identifier", $identifier, PDO::PARAM_STR)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
