<?php

namespace BlakePrinting\Catalog;

use BlakePrinting\Db\ValueObject;
use BlakePrinting\Util\Utils;
use JsonSerializable;

class CatalogPartPartVO implements JsonSerializable, ValueObject {
    /** @var int */
    private $parentPartId = null;
    /** @var int */
    private $partId = null;
    /** @var int */
    private $quantity = null;

    public function jsonSerialize() {
        return [
            "vo" => Utils::getReflectionClassByName(get_class($this))->getShortName(),
            "parent_part_id" => $this->parentPartId,
            "part_id" => $this->partId,
            "quantity" => $this->quantity
        ];
    }

    /**
     * @return int
     */
    public function getParentPartId()
    {
        return $this->parentPartId;
    }

    /**
     * @param int $parentPartId
     */
    public function setParentPartId($parentPartId)
    {
        $this->parentPartId = $parentPartId;
    }

    /**
     * @return int
     */
    public function getPartId()
    {
        return $this->partId;
    }

    /**
     * @param int $partId
     */
    public function setPartId($partId)
    {
        $this->partId = $partId;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
}
