<?php

namespace BlakePrinting\Catalog;

use BlakePrinting\Db\ValueObject;
use BlakePrinting\Util\Utils;
use JsonSerializable;

class CatalogQualityGradeVO implements JsonSerializable, ValueObject {
    /** @var int */
    private $id = null;
    /** @var string */
    private $name = null;

    public function __construct($name = null) {
        $this->name = $name;
    }

    public function jsonSerialize() {
        return [
            "vo" => Utils::getReflectionClassByName(get_class($this))->getShortName(),
            "id" => $this->id,
            "name" => $this->name
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
