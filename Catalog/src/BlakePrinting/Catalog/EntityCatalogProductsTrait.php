<?php

namespace BlakePrinting\Catalog;

use BlakePrinting\Db\AbstractExtensibleEntity;
use BlakePrinting\Db\PDOQuery;
use Exception;
use Stringy\StaticStringy as S;

trait EntityCatalogProductsTrait {
    /** @var CatalogProductVO[] */
    private $catalogProducts = [];
    /** @var AbstractEntityCatalogProductDAO */
    private $entityCatalogProductDao = null;

    public function replaceCatalogProducts() {
        $this->updateCatalogProducts(true);
    }

    /**
     * @param bool $removePrevious
     * @throws Exception
     */
    public function updateCatalogProducts($removePrevious = true) {
        if(!$this instanceof AbstractExtensibleEntity) {
            throw new Exception("Tried to replace catalog products of object that's not an extensible entity: " . get_class($this));
        } else {
            $entityColumnName = $this->getEntityColumnName();
            $entityColumnValue = $this->getEntityColumnValue();
            $catalogProductIds = [];

            if($this->entityCatalogProductDao === null) {
                $this->entityCatalogProductDao = $this->getExtensionDAO("CatalogProduct");
            }

            if(!empty($this->catalogProducts)) {
                $voReflectionClass = $this->entityCatalogProductDao->getVoReflectionClass();
                $setEntityMethod = $voReflectionClass->getMethod("set" . S::upperCamelize($entityColumnName));
                $setCatalogProductIdMethod = $voReflectionClass->getMethod("setCatalogProductId");
                $setQuantityMethod = $voReflectionClass->getMethod("setQuantity");

                foreach($this->catalogProducts as $catalogProductId => &$quantity) {
                    $catalogProductIds[] = $catalogProductId;
                    $vo = $voReflectionClass->newInstance();

                    if($vo instanceof AbstractEntityCatalogProductVO) {
                        $setEntityMethod->invoke($vo, $entityColumnValue);
                        $setCatalogProductIdMethod->invoke($vo, $catalogProductId);
                        $setQuantityMethod->invoke($vo, $quantity);

                        $this->entityCatalogProductDao->store($vo);
                    }
                }

                unset($quantity);
            }

            if($removePrevious) {
                $delete = PDOQuery::delete()
                    ->from($this->entityCatalogProductDao)
                    ->where($entityColumnName, $entityColumnValue, $this->getEntityColumnType())
                ;

                if(!empty($catalogProductIds)) {
                    $delete->where("catalog_product_id NOT IN (" . implode(",", $catalogProductIds) . ")");
                }

                $delete->executeGetRowCount($this->entityCatalogProductDao->getConnection());
            }
        }
    }

    /**
     * @param CatalogProductVO $catalogProductVo
     * @param int $quantity
     */
    public function addCatalogProduct(CatalogProductVO $catalogProductVo, $quantity) {
        $catalogProductId = $catalogProductVo->getId();
        $existingQuantity = (array_key_exists($catalogProductId, $this->catalogProducts) ? $this->catalogProducts[$catalogProductId] : 0);

        $this->catalogProducts[$catalogProductVo->getId()] = ($existingQuantity + $quantity);
    }

    /**
     * @return array
     */
    public function getCatalogProducts() {
        return $this->catalogProducts;
    }

    /**
     * @return AbstractEntityCatalogProductDAO
     */
    public function getEntityCatalogProductDao()
    {
        return $this->entityCatalogProductDao;
    }

    /**
     * @param AbstractEntityCatalogProductDAO $entityCatalogProductDao
     */
    public function setEntityCatalogProductDao(AbstractEntityCatalogProductDAO $entityCatalogProductDao)
    {
        $this->entityCatalogProductDao = $entityCatalogProductDao;
    }
}
