<?php

namespace BlakePrinting\Catalog;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static CatalogProductDAO instance(PDO $connection = null, $schemaName = null)
 * @method CatalogProductVO findById($id, $options = null, $useCache = false)
 * @method CatalogProductVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogProductVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogProductVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method CatalogProductVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogProductVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogProductVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method CatalogProductVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class CatalogProductDAO extends AbstractDataAccessObject {
}
