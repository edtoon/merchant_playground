<?php

namespace BlakePrinting\Catalog;

use BlakePrinting\Db\ValueObject;
use BlakePrinting\Util\Utils;
use JsonSerializable;

class CatalogProductPartVO implements JsonSerializable, ValueObject {
    /** @var int */
    private $productId = null;
    /** @var int */
    private $partId = null;
    /** @var int */
    private $quantity = null;

    public function jsonSerialize() {
        return [
            "vo" => Utils::getReflectionClassByName(get_class($this))->getShortName(),
            "product_id" => $this->productId,
            "part_id" => $this->partId,
            "quantity" => $this->quantity
        ];
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return int
     */
    public function getPartId()
    {
        return $this->partId;
    }

    /**
     * @param int $partId
     */
    public function setPartId($partId)
    {
        $this->partId = $partId;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
}
