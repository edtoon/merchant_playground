<?php

namespace BlakePrinting\Catalog;

use BlakePrinting\Db\ValueObject;

abstract class AbstractEntityCatalogProductVO implements ValueObject {
    /** @var int */
    private $catalogProductId = null;
    /** @var int */
    private $quantity = null;

    /**
     * @param int $catalogProductId
     */
    public function __construct($catalogProductId = null, $quantity = null) {
        $this->catalogProductId = $catalogProductId;
        $this->quantity = $quantity;
    }

    /**
     * @return int
     */
    public function getCatalogProductId()
    {
        return $this->catalogProductId;
    }

    /**
     * @param int $catalogProductId
     */
    public function setCatalogProductId($catalogProductId)
    {
        $this->catalogProductId = $catalogProductId;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
}
