<?php

namespace BlakePrinting\Catalog;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static CatalogPartIdentifierDAO instance(PDO $connection = null, $schemaName = null)
 * @method CatalogPartIdentifierVO findById($id, $options = null, $useCache = false)
 * @method CatalogPartIdentifierVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogPartIdentifierVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogPartIdentifierVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method CatalogPartIdentifierVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogPartIdentifierVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogPartIdentifierVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class CatalogPartIdentifierDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $partId
     * @param int $dataSourceIdentifierId
     * @param string $identifier
     * @return int
     */
    public function &store($partId, $dataSourceIdentifierId, $identifier) {
        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value("part_id", $partId, PDO::PARAM_INT)
            ->value("data_source_identifier_id", $dataSourceIdentifierId, PDO::PARAM_INT)
            ->value("identifier", $identifier, PDO::PARAM_STR)
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
