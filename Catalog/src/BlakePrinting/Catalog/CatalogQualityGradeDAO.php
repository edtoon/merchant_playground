<?php

namespace BlakePrinting\Catalog;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static CatalogQualityGradeDAO instance(PDO $connection = null, $schemaName = null)
 * @method CatalogQualityGradeVO findById($id, $options = null, $useCache = false)
 * @method CatalogQualityGradeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogQualityGradeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogQualityGradeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method CatalogQualityGradeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogQualityGradeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogQualityGradeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method CatalogQualityGradeVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class CatalogQualityGradeDAO extends AbstractDataAccessObject {
    const GRADE_GREAT = "great";
    const GRADE_FAIR = "fair";
    const GRADE_POOR = "poor";
}
