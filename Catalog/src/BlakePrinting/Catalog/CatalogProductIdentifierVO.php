<?php

namespace BlakePrinting\Catalog;

use BlakePrinting\Db\ValueObject;
use BlakePrinting\Util\Utils;
use JsonSerializable;

class CatalogProductIdentifierVO implements JsonSerializable, ValueObject {
    /** @var int */
    private $productId = null;
    /** @var int */
    private $dataSourceIdentifierId = null;
    /** @var string */
    private $identifier = null;

    public function jsonSerialize() {
        return [
            "vo" => Utils::getReflectionClassByName(get_class($this))->getShortName(),
            "product_id" => $this->productId,
            "data_source_identifier_id" => $this->dataSourceIdentifierId,
            "identifier" => $this->identifier
        ];
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return int
     */
    public function getDataSourceIdentifierId()
    {
        return $this->dataSourceIdentifierId;
    }

    /**
     * @param int $dataSourceIdentifierId
     */
    public function setDataSourceIdentifierId($dataSourceIdentifierId)
    {
        $this->dataSourceIdentifierId = $dataSourceIdentifierId;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }
}
