<?php

namespace BlakePrinting\Catalog;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static CatalogProductPartDAO instance(PDO $connection = null, $schemaName = null)
 * @method CatalogProductPartVO findById($id, $options = null, $useCache = false)
 * @method CatalogProductPartVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogProductPartVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogProductPartVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method CatalogProductPartVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogProductPartVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogProductPartVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class CatalogProductPartDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $productId
     * @param int $partId
     * @param int $quantity
     * @return int
     */
    public function &store($productId, $partId, $quantity) {
        return PDOQuery::insert()
            ->into($this)
            ->value("product_id", $productId, PDO::PARAM_INT)
            ->value("part_id", $partId, PDO::PARAM_INT)
            ->value("quantity", $quantity, PDO::PARAM_INT)
            ->onDuplicateValue("quantity", "VALUES(quantity)")
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
