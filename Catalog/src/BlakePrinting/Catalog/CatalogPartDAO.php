<?php

namespace BlakePrinting\Catalog;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static CatalogPartDAO instance(PDO $connection = null, $schemaName = null)
 * @method CatalogPartVO findById($id, $options = null, $useCache = false)
 * @method CatalogPartVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogPartVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogPartVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method CatalogPartVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogPartVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogPartVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method CatalogPartVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class CatalogPartDAO extends AbstractDataAccessObject {
}
