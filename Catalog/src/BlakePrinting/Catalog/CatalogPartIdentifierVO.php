<?php

namespace BlakePrinting\Catalog;

use BlakePrinting\Db\ValueObject;
use BlakePrinting\Util\Utils;
use JsonSerializable;

class CatalogPartIdentifierVO implements JsonSerializable, ValueObject {
    /** @var int */
    private $partId = null;
    /** @var int */
    private $dataSourceIdentifierId = null;
    /** @var string */
    private $identifier = null;

    public function jsonSerialize() {
        return [
            "vo" => Utils::getReflectionClassByName(get_class($this))->getShortName(),
            "part_id" => $this->partId,
            "data_source_identifier_id" => $this->dataSourceIdentifierId,
            "identifier" => $this->identifier
        ];
    }

    /**
     * @return int
     */
    public function getPartId()
    {
        return $this->partId;
    }

    /**
     * @param int $partId
     */
    public function setPartId($partId)
    {
        $this->partId = $partId;
    }

    /**
     * @return int
     */
    public function getDataSourceIdentifierId()
    {
        return $this->dataSourceIdentifierId;
    }

    /**
     * @param int $dataSourceIdentifierId
     */
    public function setDataSourceIdentifierId($dataSourceIdentifierId)
    {
        $this->dataSourceIdentifierId = $dataSourceIdentifierId;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }
}
