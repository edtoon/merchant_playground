<?php

namespace BlakePrinting\Catalog;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static CatalogPartPartDAO instance(PDO $connection = null, $schemaName = null)
 * @method CatalogPartPartVO findById($id, $options = null, $useCache = false)
 * @method CatalogPartPartVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogPartPartVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogPartPartVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method CatalogPartPartVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogPartPartVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogPartPartVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class CatalogPartPartDAO extends AbstractDataAccessObject {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, null, null, null);
    }

    /**
     * @param int $parentPartId
     * @param int $partId
     * @param int $quantity
     * @return int
     */
    public function &store($parentPartId, $partId, $quantity) {
        return PDOQuery::insert()
            ->into($this)
            ->value("parent_part_id", $parentPartId, PDO::PARAM_INT)
            ->value("part_id", $partId, PDO::PARAM_INT)
            ->value("quantity", $quantity, PDO::PARAM_INT)
            ->onDuplicateValue("quantity", "VALUES(quantity)")
            ->executeGetRowCount($this->getConnection())
        ;
    }
}
