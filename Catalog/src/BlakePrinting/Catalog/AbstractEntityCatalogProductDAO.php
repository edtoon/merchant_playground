<?php

namespace BlakePrinting\Catalog;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use PDO;
use Stringy\StaticStringy as S;

abstract class AbstractEntityCatalogProductDAO extends AbstractDataAccessObject {
    /** @var string */
    private $entityColumnName = null;

    public function __construct(PDO $connection = null, $schemaName = null, $entityColumnName = null) {
        parent::__construct($connection, $schemaName, null, null, null);

        $this->entityColumnName = $entityColumnName;
    }

    public function store(AbstractEntityCatalogProductVO $abstractEntityCatalogProductVo) {
        $voReflectionClass =& $this->getVoReflectionClass();
        $getEntityPrimaryKeyMethod = $voReflectionClass->getMethod("get" . S::upperCamelize($this->entityColumnName));

        return PDOQuery::insert()
            ->into($this)
            ->value($this->entityColumnName, $getEntityPrimaryKeyMethod->invoke($abstractEntityCatalogProductVo), PDO::PARAM_INT)
            ->value("catalog_product_id", $abstractEntityCatalogProductVo->getCatalogProductId(), PDO::PARAM_INT)
            ->value("quantity", $abstractEntityCatalogProductVo->getQuantity(), PDO::PARAM_INT)
            ->onDuplicateValue("quantity", "VALUES(quantity)")
            ->executeGetRowCount($this->getConnection())
        ;
    }

    /**
     * @return string
     */
    public function getEntityColumnName()
    {
        return $this->entityColumnName;
    }

    /**
     * @param string $entityColumnName
     */
    public function setEntityColumnName($entityColumnName)
    {
        $this->entityColumnName = $entityColumnName;
    }
}
