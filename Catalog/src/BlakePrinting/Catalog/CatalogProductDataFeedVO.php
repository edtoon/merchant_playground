<?php

namespace BlakePrinting\Catalog;

use BlakePrinting\DataSources\AbstractEntityDataFeedVO;

class CatalogProductDataFeedVO extends AbstractEntityDataFeedVO {
    /** @var int */
    private $catalogProductId = null;

    /**
     * @param int $catalogProductId
     * @param int $dataFeedId
     */
    public function __construct($catalogProductId = null, $dataFeedId = null) {
        parent::__construct($dataFeedId);

        $this->catalogProductId = $catalogProductId;
    }

    /**
     * @return int
     */
    public function getCatalogProductId()
    {
        return $this->catalogProductId;
    }

    /**
     * @param int $catalogProductId
     */
    public function setCatalogProductId($catalogProductId)
    {
        $this->catalogProductId = $catalogProductId;
    }
}
