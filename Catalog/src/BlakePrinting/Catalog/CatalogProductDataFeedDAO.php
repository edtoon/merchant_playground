<?php

namespace BlakePrinting\Catalog;

use BlakePrinting\DataSources\AbstractEntityDataFeedDAO;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static CatalogProductDataFeedDAO instance(PDO $connection = null, $schemaName = null)
 * @method CatalogProductDataFeedVO findById($id, $options = null, $useCache = false)
 * @method CatalogProductDataFeedVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogProductDataFeedVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogProductDataFeedVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method CatalogProductDataFeedVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CatalogProductDataFeedVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CatalogProductDataFeedVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method void store(CatalogProductDataFeedVO $vo)
 */
class CatalogProductDataFeedDAO extends AbstractEntityDataFeedDAO {
    public function __construct(PDO $connection = null, $schemaName = null) {
        parent::__construct($connection, $schemaName, "catalog_product_id");
    }
}
