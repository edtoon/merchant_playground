<?php

use BlakePrinting\Catalog\CatalogQualityGradeDAO;
use BlakePrinting\Catalog\CatalogQualityGradeVO;
use BlakePrinting\Db\DbSeed;
use BlakePrinting\Util\Utils;

class CatalogSeeder extends DbSeed {
    public final function run() {
        $connection =& $this->getConnection();
        $catalogQualityGradeDao =& CatalogQualityGradeDAO::instance($connection);

        Utils::forEachConstantInObjectClass(
            $catalogQualityGradeDao,
            function($_, $constantName, $constantValue) use (&$catalogQualityGradeDao) {
                $catalogQualityGradeDao->findOrCreate(new CatalogQualityGradeVO($constantValue));
            },
            "GRADE_"
        );
    }
}
