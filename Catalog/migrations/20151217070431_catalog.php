<?php

use BlakePrinting\Catalog\CatalogPartDAO;
use BlakePrinting\Catalog\CatalogPartIdentifierDAO;
use BlakePrinting\Catalog\CatalogPartPartDAO;
use BlakePrinting\Catalog\CatalogProductDAO;
use BlakePrinting\Catalog\CatalogProductIdentifierDAO;
use BlakePrinting\Catalog\CatalogProductPartDAO;
use BlakePrinting\DataSources\DataSourceIdentifierDAO;
use BlakePrinting\Db\DbMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class Catalog extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $partDao = new CatalogPartDAO($connection);
        $partTbl = $partDao->formatSchemaAndTableName();
        $this->tableDao($partDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $partPartDao = new CatalogPartPartDAO($connection);
        $this->tableDao($partPartDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["parent_part_id", "part_id"]])
            ->addColumn("parent_part_id", "biginteger", ["signed" => false])
            ->addColumn("part_id", "biginteger", ["signed" => false])
            ->addColumn("quantity", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addForeignKey("parent_part_id", $partTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("part_id", $partTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("quantity")
            ->save()
        ;

        $partIdentifierDao = new CatalogPartIdentifierDAO($connection);
        $dataSourceIdentifierDao = new DataSourceIdentifierDAO($connection);
        $dataSourceIdentifierTbl = $dataSourceIdentifierDao->formatSchemaAndTableName();
        $this->tableDao($partIdentifierDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["part_id", "data_source_identifier_id", "identifier"]])
            ->addColumn("part_id", "biginteger", ["signed" => false])
            ->addColumn("data_source_identifier_id", "integer", ["signed" => false])
            ->addColumn("identifier", "string", ["limit" => 191])
            ->addForeignKey("part_id", $partTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("data_source_identifier_id", $dataSourceIdentifierTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("identifier")
            ->save()
        ;

        $productDao = new CatalogProductDAO($connection);
        $this->tableDao($productDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "biginteger", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $productPartDao = new CatalogProductPartDAO($connection);
        $productTbl = $productDao->formatSchemaAndTableName();
        $this->tableDao($productPartDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["product_id", "part_id"]])
            ->addColumn("product_id", "biginteger", ["signed" => false])
            ->addColumn("part_id", "biginteger", ["signed" => false])
            ->addColumn("quantity", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addForeignKey("product_id", $productTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("part_id", $partTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("quantity")
            ->save()
        ;

        $productIdentifierDao = new CatalogProductIdentifierDAO($connection);
        $this->tableDao($productIdentifierDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["product_id", "data_source_identifier_id", "identifier"]])
            ->addColumn("product_id", "biginteger", ["signed" => false])
            ->addColumn("data_source_identifier_id", "integer", ["signed" => false])
            ->addColumn("identifier", "string", ["limit" => 191])
            ->addForeignKey("product_id", $productTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("data_source_identifier_id", $dataSourceIdentifierTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("identifier")
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(new CatalogProductIdentifierDAO($connection));
        $this->dropDao(new CatalogProductPartDAO($connection));
        $this->dropDao(new CatalogProductDAO($connection));
        $this->dropDao(new CatalogPartIdentifierDAO($connection));
        $this->dropDao(new CatalogPartPartDAO($connection));
        $this->dropDao(new CatalogPartDAO($connection));
    }
}
