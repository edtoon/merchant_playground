<?php

use BlakePrinting\Catalog\CatalogQualityGradeDAO;
use BlakePrinting\Db\DbMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CatalogQualityGrades extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $catalogQualityGradeDao = new CatalogQualityGradeDAO($connection);
        $this->tableDao($catalogQualityGradeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(new CatalogQualityGradeDAO($connection));
    }
}
