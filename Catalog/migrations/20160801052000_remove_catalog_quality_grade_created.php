<?php

use BlakePrinting\Catalog\CatalogQualityGradeDAO;
use BlakePrinting\Db\DbMigration;

class RemoveCatalogQualityGradeCreated extends DbMigration {
    public function up() {
        $catalogQualityGradeDao =& CatalogQualityGradeDAO::instance($this->getConnection());

        $this->tableDao($catalogQualityGradeDao)
            ->removeIndex("created")
            ->removeColumn("created")
            ->save()
        ;
    }

    public function down() {
        $catalogQualityGradeDao =& CatalogQualityGradeDAO::instance($this->getConnection());

        $this->tableDao($catalogQualityGradeDao)
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addIndex("created")
            ->save()
        ;
    }
}
