<?php

use BlakePrinting\Catalog\CatalogProductDAO;
use BlakePrinting\Catalog\CatalogProductDataFeedDAO;
use BlakePrinting\DataSources\DataFeedDAO;
use BlakePrinting\Db\DbMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CatalogProductDataFeeds extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $catalogProductDataFeedDao =& CatalogProductDataFeedDAO::instance($connection);
        $catalogProductDao =& CatalogProductDAO::instance($connection);
        $dataFeedDao =& DataFeedDAO::instance($connection);
        $this->tableDao($catalogProductDataFeedDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["catalog_product_id", "data_feed_id"]])
            ->addColumn("catalog_product_id", "biginteger", ["signed" => false])
            ->addColumn("data_feed_id", "integer", ["signed" => false])
            ->addForeignKey("catalog_product_id", $catalogProductDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("data_feed_id", $dataFeedDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(CatalogProductDataFeedDAO::instance($connection));
    }
}
