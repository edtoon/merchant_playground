#!/bin/bash
#
# Run the various import tasks at intervals that have been found to be relatively safe with regards to quota usage
#

set -o errexit

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${BASE_DIR}/../lib/lib-config.sh"

LOG_DIR="$(config_get log_dir)"
TMP_DIR="$(config_get tmp_dir)"

check_time_since() {
  interval="${1}" && shift
  task="${1}" && shift
  app="${1}" && shift
  task_file="${TMP_DIR}/merchant_task_${task}"
  current_time="$(date +%s)"
  previous_time="$(cat "${task_file}" 2>/dev/null || true)"
  elapsed_time=0

  if [ ! -z "${previous_time}" ];
  then
    elapsed_time=$((current_time-previous_time))
  fi

  if [ ! -f "${task_file}" ] || [ ${elapsed_time} -ge ${interval} ];
  then
    echo "Current time: ${current_time}, task: ${task}, previous time: ${previous_time}, elapsed time: ${elapsed_time}" >> "${LOG_DIR}/execute_tasks.log" 2>&1
    date +%s > "${task_file}"
    1>>"${LOG_DIR}/${task}.log" "${BASE_DIR}/run_php.sh" "${app}/cli.php" $* 2>&1
  fi
}

main() {
  sudo mkdir -p "${LOG_DIR}" "${TMP_DIR}"
  sudo chown "${USER}:${USER}" "${LOG_DIR}" "${TMP_DIR}"

  while true;
  do
    if [ -f "${TMP_DIR}/stop_merchant_tasks" ];
    then
        sudo rm "${TMP_DIR}/stop_merchant_tasks"
        exit 0
    fi

    sleep 1

    check_time_since 300 mws_list_marketplaces AmazonMwsSellers amazon:mws:sellers:list-marketplaces
    check_time_since 600 mws_list_orders AmazonMwsOrders amazon:mws:orders:list-orders
    check_time_since 20 mws_list_order_items AmazonMwsOrders amazon:mws:orders:list-order-items
    check_time_since 600 mws_list_reports AmazonMwsReports amazon:mws:reports:list-reports
    check_time_since 90 mws_list_report_requests AmazonMwsReports amazon:mws:reports:list-requests
    check_time_since 600 mws_retrieve_report_request AmazonMwsReports amazon:mws:reports:retrieve-request
    check_time_since 15 mws_transform_report TransformAmazonReports transform:amazon:report
#    check_time_since 600 mws_acknowledge_report_request AmazonMwsReports amazon:mws:reports:acknowledge-request
  done
}

main $*
