#!/bin/bash
#
# Remove Docker containers that are no longer running
#

set -o errexit

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${BASE_DIR}/../lib/lib-docker.sh"

for container_id in $(docker_exec ps -a | grep -E 'Exited|Dead' | cut -d ' ' -f 1)
do
  docker_exec rm "${container_id}"
done
