<?php

require_once(is_dir(__DIR__ . "/../vendor") ? (__DIR__ . "/../vendor/autoload.php") : "/app/vendor/autoload.php");

use BlakePrinting\Build\ComposerUtil;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Debug\ErrorHandler;

function main() {
    ErrorHandler::register();

    $output = new ConsoleOutput();
    $pathsWithDeps =& ComposerUtil::executeAndReturnPathsWithDeps($output);
    $orderedDeps =& ComposerUtil::getOrderedDependenciesFromPathsWithDeps($pathsWithDeps);

    ComposerUtil::writeOrderedDependenciesToFile($orderedDeps);
}

main();
