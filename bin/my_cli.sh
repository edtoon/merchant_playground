#!/bin/bash
#
# Open a MySQL command line in the running MySQL container
#

set -o errexit

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${BASE_DIR}/../lib/lib-config.sh"
. "${BASE_DIR}/../lib/lib-docker.sh"

DB_ID="${DB_ID:=$(config_get database.default)}"
MYSQL_DOCKER_NAME="${MYSQL_DOCKER_NAME:=$(config_get database.${DB_ID}.docker)}"
CHARSET="${CHARSET:=$(config_get database.${DB_ID}.charset)}"
ROOT_USERNAME="${ROOT_USERNAME:=$(config_get database.${DB_ID}.root_username)}"
ROOT_PASSWORD="${ROOT_PASSWORD:=$(config_get database.${DB_ID}.root_password)}"

docker_exec exec -it "${MYSQL_DOCKER_NAME}" mysql --default-character-set=${CHARSET} -u${ROOT_USERNAME} -p${ROOT_PASSWORD} mysql
