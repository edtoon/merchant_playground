#!/bin/bash
#
# Run the MySQL database
#

set -o errexit

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${BASE_DIR}/../lib/lib-blake-environment.sh"
. "${BASE_DIR}/../lib/lib-config.sh"
. "${BASE_DIR}/../lib/lib-docker.sh"

[[ "${BLAKE_ENVIRONMENT}" = "dev" ]] || exit 1

DB_ID="${DB_ID:=$(config_get database.default)}"
DOCKER_NAME="${DOCKER_NAME:=$(config_get database.${DB_ID}.docker)}"
DATA_DIR="${DATA_DIR:=$(config_get data_dir)/${DOCKER_NAME}}"
ROOT_PASSWORD="${ROOT_PASSWORD:=$(config_get database.${DB_ID}.root_password)}"

sudo mkdir -p "${DATA_DIR}"
docker_exec rm "${DOCKER_NAME}" > /dev/null 2>&1 || true
docker_exec run -d -e MYSQL_ROOT_PASSWORD="${ROOT_PASSWORD}" --name "${DOCKER_NAME}" -v "${DATA_DIR}:/var/lib/mysql" -v "${DATA_DIR}:/home/mysql" blakeprinting/mysql
