#!/bin/bash
#
# Run memcached
#

set -o errexit

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${BASE_DIR}/../lib/lib-config.sh"
. "${BASE_DIR}/../lib/lib-docker.sh"

MEMCACHE_ID="${MEMCACHE_ID:=$(config_get memcache.default)}"
MEMCACHE_MEMORY_MB="${MEMCACHE_MEMORY_MB:=$(config_get memcache.${MEMCACHE_ID}.memory_mb)}"
DOCKER_NAME="${DOCKER_NAME:=$(config_get memcache.${MEMCACHE_ID}.docker)}"

docker_exec rm "${DOCKER_NAME}" > /dev/null 2>&1 || true
docker_exec run -d --name "${DOCKER_NAME}" memcached -m "${MEMCACHE_MEMORY_MB}"
