#!/bin/bash
#
# Run a PHP file or a command inside a Docker container
#

set -o errexit

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${BASE_DIR}/../lib/lib-blake-environment.sh"
. "${BASE_DIR}/../lib/lib-command.sh"
. "${BASE_DIR}/../lib/lib-config.sh"
. "${BASE_DIR}/../lib/lib-coreutils.sh"
. "${BASE_DIR}/../lib/lib-docker.sh"

APP_NAME="${APP_NAME:=php}"
APP_DIR="${APP_DIR:=$(readlink_exec -f ${BASE_DIR}/../)}"
DATA_DIR="${DATA_DIR:=$(config_get data_dir)/${APP_NAME}}"
PHP_OPTS="${PHP_OPTS:=-d memory_limit=512M}"

usage() {
  >&2 printf "Syntax: $0 [-i] script.php arg1 arg2 ...\n"
  >&2 printf "    OR: $0 [-i] -c command line\n"
  >&2 printf "    OR: $0 [-i] -a script.sh arg1 arg2 ...\n"
  >&2 printf "    OR: APP_NAME='SupervisorAppName' $0 -s\n"
  exit 1
}

run_type="php"
docker_image="blakeprinting/php-cli"
docker_interact=""
docker_name="${DOCKER_NAME}"

while getopts ":acis" opt;
do
  case "${opt}" in
    a)
      [ "cmd" == "${run_type}" ] && usage
      run_type="app"
      ;;
    c)
      [ "app" == "${run_type}" ] && usage
      run_type="cmd"
      ;;
    s)
      [ "php" == "${run_type}" ] || usage
      docker_image="blakeprinting/php-supervisor"
      run_type="supervisor"
      [ -z "${docker_name}" ] && [ "php" != "${APP_NAME}" ] && docker_name="merchant_supervisor_${APP_NAME,,}"
      ;;
    i)
      [ "supervisor" == "${run_type}" ] && usage
      docker_interact="-it"
      ;;
    \?)
      echo "Invalid option: -${OPTARG}" >&2
      ;;
  esac
done
shift $((OPTIND-1))

[ "supervisor" != "${run_type}" ] && [ $# -lt 1 ] && echo "Syntax: $0 [file1] [file2] ..." >&2 && exit 1

if [ "dev" != "${BLAKE_ENVIRONMENT}" ] && [ ! -d "${DATA_DIR}" ];
then
  sudo mkdir -p "${DATA_DIR}"
fi

docker_cmd=""

if [ "${HAS_DOCKER}" = "true" ];
then
  MEMCACHE_ID="${MEMCACHE_ID:=$(config_get memcache.default)}"
  MEMCACHE_DOCKER_NAME="${MEMCACHE_DOCKER_NAME:=$(config_get memcache.${MEMCACHE_ID}.docker)}"
  DB_ID="${DB_ID:=$(config_get database.default)}"
  MYSQL_DOCKER_NAME="${MYSQL_DOCKER_NAME:=$(config_get database.${DB_ID}.docker || true)}"
  docker_cmd="docker_exec run --rm ${docker_interact}"

  for container_name in $(docker_exec ps --format '{{.Names}}')
  do
    case ${container_name} in
      ${MEMCACHE_DOCKER_NAME})
        docker_cmd+=" --link ${MEMCACHE_DOCKER_NAME}:${MEMCACHE_DOCKER_NAME}"
      ;;
      ${MYSQL_DOCKER_NAME})
        docker_cmd+=" --link ${MYSQL_DOCKER_NAME}:${MYSQL_DOCKER_NAME}"
      ;;
    esac
  done

  if [ "blakeprinting/php-supervisor" = "${docker_image}" ];
  then
    if [ -d "${APP_DIR}/${APP_NAME}/supervisor" ];
    then
      docker_cmd+=" -v ${APP_DIR}/${APP_NAME}/supervisor:/etc/supervisor/conf.d"
    elif [ -d "${APP_DIR}/supervisor" ];
    then
      docker_cmd+=" -v ${APP_DIR}/supervisor:/etc/supervisor/conf.d"
    fi
  fi

  if [ ! -z "${docker_name}" ];
  then
    docker_cmd+=" --name ${docker_name}"
  fi

  if [ "app" != "${APP_NAME}" ];
  then
    docker_cmd+=" -e MERCHANT_APP=${APP_NAME}"
  fi

  docker_cmd+=" -v ${APP_DIR}:${BLAKE_APP_DIR} -v ${DATA_DIR}:/data -w ${BLAKE_APP_DIR}"
  docker_cmd+=" -e MERCHANT_ENV=${BLAKE_ENVIRONMENT}"
  docker_cmd+=" ${docker_image}:latest"
fi

if [ "php" == "${run_type}" ];
then
  app=$1
  shift
  ${docker_cmd} php ${PHP_OPTS} -f "${app}" -- $@
elif [ "app" == "${run_type}" ];
then
  app=$1
  shift
  ${docker_cmd} "${app}" $@
else
  ${docker_cmd} $@
fi
