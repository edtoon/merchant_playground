#!/bin/bash

set -o errexit

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${BASE_DIR}/../lib/lib-aws.sh"

for region in eu-west-1 ap-southeast-1 ap-southeast-2 eu-central-1 ap-northeast-1 ap-northeast-2 us-east-1 sa-east-1 us-west-1 us-west-2
do
  echo "Region: $region"
  aws rds --region "${region}" describe-db-instances
done
