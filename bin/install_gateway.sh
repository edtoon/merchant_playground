#!/bin/bash
# See https://github.com/Homebrew/homebrew-php for updated info on installing PHP via Homebrew

set -o errexit

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${BASE_DIR}/../lib/lib-brew.sh"
. "${BASE_DIR}/../lib/lib-operating-system.sh"

[ "${OPERATING_SYSTEM}" = "darwin" ] || exit 1
[ $# != 2 ] && echo "Syntax: $0 <api_host> <api_key>" >&2 && exit 1

API_HOST="${1}"
API_KEY="${2}"
BLAKE_HOME="${BLAKE_HOME:=${BREW_HOME}/blake}"
MERCHANT_DIR="${MERCHANT_DIR:=${BLAKE_HOME}/merchant}"
LAUNCHD_PLIST="${LAUNCH_PLIST:=/Library/LaunchDaemons/com.blakeprinting.gateway.plist}"
PHP_HOME="${PHP_HOME:=$("${BREW_EXE}" --prefix php56)}"
PHP_CONFD_FILE="${PHP_CONFD_FILE:=blake.ini}"

if [ ! -d "${PHP_HOME}" ];
then
  "${BREW_EXE}" tap homebrew/dupes
  "${BREW_EXE}" tap homebrew/versions
  "${BREW_EXE}" tap homebrew/homebrew-php
  "${BREW_EXE}" install php56 --with-homebrew-curl --with-postgresql
  "${BREW_EXE}" install php56-mcrypt
  "${BREW_EXE}" install php56-yaml

  PHP_HOME="$("${BREW_EXE}" --prefix php56)"

  echo "Add the following line to your ~/.bash_profile file to use brew's PHP on the command line:"
  echo "export PATH=\"\$(brew --prefix php56)/bin:\$PATH\""
fi

PHP_EXE="${PHP_HOME}/bin/php"
PHP_CONFD_DIR="$("${PHP_EXE}" --ini|grep 'Scan for additional'|head -1|sed 's/Scan for additional .ini files in: //g')"

if [ ! -d "${PHP_CONFD_DIR}" ];
then
  >&2 echo "PHP conf.d directory: ${PHP_CONFD_DIR} does not exist, something must have gone wrong"
  exit 1
fi

if [ ! -f "${PHP_CONFD_DIR}/${PHP_CONFD_FILE}" ];
then
  sudo cat > "${PHP_CONFD_DIR}/${PHP_CONFD_FILE}" << __EOF__
always_populate_raw_post_data=-1
date.timezone=UTC
default_charset=UTF-8
display_errors=stderr
display_startup_errors=On
error_reporting=-1
log_errors=On
__EOF__
fi

[ -d "${MERCHANT_DIR}/current" ] || {
  sudo mkdir -p "${MERCHANT_DIR}"
  sudo ln -s "${BASE_DIR}/../" "${MERCHANT_DIR}/current"
}

TMP_FILE="$(mktemp)"
cat > "${TMP_FILE}" << __EOF__
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
  <dict>
    <key>Label</key>
    <string>com.blakeprinting.gateway</string>
    <key>ProgramArguments</key>
    <array>
      <string>${PHP_EXE}</string>
      <string>${MERCHANT_DIR}/current/Zebra/gateway.php</string>
      <string>${API_HOST}</string>
      <string>${API_KEY}</string>
    </array>
    <key>StartInterval</key>
    <integer>5</integer>
    <key>StandardOutPath</key>
    <string>/var/log/blake/gateway.out</string>
    <key>StandardErrorPath</key>
    <string>/var/log/blake/gateway.err</string>
  </dict>
</plist>
__EOF__
sudo chown root:wheel "${TMP_FILE}"
sudo mv "${TMP_FILE}" /Library/LaunchDaemons/com.blakeprinting.gateway.plist
sudo launchctl load /Library/LaunchDaemons/com.blakeprinting.gateway.plist
