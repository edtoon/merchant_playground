#!/bin/bash

# https://gist.github.com/joemiller/6049831
# https://dl.dropboxusercontent.com/u/6943630/raid0_ec2_Ubuntu_1204.txt
# http://agiletesting.blogspot.com/2011/05/setting-up-raid-0-across-ephemeral.html

set -x
set -e

METADATA_URL="http://169.254.169.254/2012-01-12"

if [ -f "/mnt/.devices_mounted" ]; then
  echo "Devices are already mounted"
  exit 0
fi

apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends mdadm lvm2 xfsprogs

if [ "$(df -h|grep xvda1|head -1|awk '{print $1}')" == "/dev/xvda1" ]; then
  echo "Detected 'xvd' drive naming scheme"
  drive_scheme='xvd'
else
  echo "Detected 'sd' drive naming scheme"
  drive_scheme='sd'
fi

drives=""
drive_count=0
mappings="$(curl --silent ${METADATA_URL}/meta-data/block-device-mapping/)"

for mapping in $mappings; do
  add="false"

  case $mapping in
    ebs*)
      add="true"
    ;;
    ephemeral*)
      add="true"
    ;;
  esac

  if [ "$add" = "true" ];
  then
    device_name="$(curl --silent ${METADATA_URL}/meta-data/block-device-mapping/${mapping})"
    device_name=$(echo ${device_name} | sed "s/sd/${drive_scheme}/")
    device_path="/dev/${device_name}"

    if [ -b ${device_path} ]; then
      echo "Detected device: ${device_path}"
      drives="${drives} ${device_path}"
      drive_count="$((drive_count + 1))"
    else
      echo "Device: ${device_path} was mapped as ${mapping} but not found"
    fi
  fi
done

if [ "${drives}" = "" ]; then
  mkdir -p /mnt
else
  umount /mnt || true

  for drive in ${drives}; do
    dd if=/dev/zero of=${drive} bs=4096 count=1024
  done

  modprobe dm-mod
  partprobe || true
  mdadm --create --verbose /dev/md0 --force --level=0 --chunk=256 --raid-devices=${drive_count} ${drives}
  uuid=`mdadm --detail /dev/md0 | grep UUID | awk '{print $3}'`
  blockdev --setra 65536 /dev/md0
  pvcreate /dev/md0
  vgcreate vg0 /dev/md0
  total_pe=`pvdisplay | grep "Total PE" | awk '{print $3}'`
  lvcreate --name combodrive -l${total_pe} vg0
  mkfs.xfs /dev/vg0/combodrive
  mount -t xfs -o noatime /dev/vg0/combodrive /mnt

  sed -i 's/exit 0//' /etc/rc.local
  echo "/sbin/modprobe dm-mod" | tee -a /etc/rc.local
  echo "/sbin/lvscan" | tee -a /etc/rc.local
  echo "/sbin/lvchange -ay /dev/vg0/combodrive" | tee -a /etc/rc.local
  echo "/bin/mount -t xfs -o noatime /dev/vg0/combodrive /mnt" | tee -a /etc/rc.local
  echo "chmod 777 /mnt" | tee -a /etc/rc.local
  echo "exit 0" | tee -a /etc/rc.local

  echo "" | tee -a /etc/mdadm/mdadm.conf
  echo "DEVICE ${drives}" | tee -a /etc/mdadm/mdadm.conf
  echo "ARRAY /dev/md0 level=raid0 num-devices=${drive_count} UUID=${uuid}" | tee -a /etc/mdadm/mdadm.conf
fi

echo "${drives}" > /mnt/.devices_mounted
