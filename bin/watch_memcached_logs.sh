#!/bin/bash
#
# Tail the active memcache logs
#

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${BASE_DIR}/../lib/lib-config.sh"
. "${BASE_DIR}/../lib/lib-docker.sh"

MEMCACHE_ID="${MEMCACHE_ID:=$(config_get memcache.default)}"
DOCKER_NAME="${DOCKER_NAME:=$(config_get memcache.${MEMCACHE_ID}.docker)}"

docker_exec logs -f "${DOCKER_NAME}"
