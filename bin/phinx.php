<?php

require_once(is_dir(__DIR__ . "/../vendor") ? (__DIR__ . "/../vendor/autoload.php") : "/app/vendor/autoload.php");

use BlakePrinting\Build\ComposerUtil;
use BlakePrinting\Build\PhinxUtil;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Debug\ErrorHandler;

function main(&$argc, &$argv) {
    ErrorHandler::register();

    $output = new ConsoleOutput();
    $command = ($argc > 1 ? $argv[1] : null);

    if($command === null || !in_array($command, ["migrate", "seed", "rollback"])) {
        $output->writeln("Syntax: " . $argv[0] . " <migrate / seed / rollback> [module-dir]");
        exit(1);
    }

    if($argc > 2) {
        PhinxUtil::executePhinx($output, realpath($argv[2]), $command);
    } else {
        $orderedDeps =& ComposerUtil::readOrderedDependenciesFromFile();

        foreach($orderedDeps as &$dependency) {
            try {
                PhinxUtil::executePhinx($output, dirname($dependency), $command);
            } catch(Exception $e) {
                if(($command == "migrate" && $e->getCode() == PhinxUtil::ERR_NO_MIGRATIONS) ||
                    ($command == "seed" && $e->getCode() == PhinxUtil::ERR_NO_SEEDS)) {
                    continue;
                }

                throw $e;
            }
        }

        unset($dependency);
    }
}

main($argc, $argv);
