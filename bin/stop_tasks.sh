#!/bin/bash
#
# Notify the task executor to stop running
#

set -o errexit

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${BASE_DIR}/../lib/lib-config.sh"

TMP_DIR="$(config_get tmp_dir)"

touch "${TMP_DIR}/stop_merchant_tasks"
