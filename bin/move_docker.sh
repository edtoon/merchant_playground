#!/bin/bash

while [ ! -f '/mnt/.devices_mounted' ]; do sleep 1; done

mkdir -p /mnt/blake/data
tmpfile="$(mktemp)"
cat > "${tmpfile}" << __EOF__

DOCKER_OPTS="-g /mnt/blake/data/docker"
__EOF__
cat "${tmpfile}" >> /etc/default/docker
rm "${tmpfile}"
service docker stop

while docker info > /dev/null 2>&1; do sleep 1; done

mv /var/lib/docker /mnt/blake/data/
ln -s /mnt/blake/data/docker /var/lib/docker

service docker start

until docker info > /dev/null 2>&1; do sleep 1; done

touch /mnt/blake/data/docker/.docker_ready
