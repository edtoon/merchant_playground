#!/bin/bash
#
# Tail the active MySQL database logs
#

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${BASE_DIR}/../lib/lib-blake-environment.sh"
. "${BASE_DIR}/../lib/lib-config.sh"
. "${BASE_DIR}/../lib/lib-docker.sh"

[[ "${BLAKE_ENVIRONMENT}" != "dev" ]] && exit 1

DB_ID="${DB_ID:=$(config_get database.default)}"
DOCKER_NAME="${DOCKER_NAME:=$(config_get database.${DB_ID}.docker)}"

docker_exec logs -f "${DOCKER_NAME}"
