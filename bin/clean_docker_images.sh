#!/bin/bash
#
# Remove Docker images that are no longer referenced by a tag or an active container
#

set -o errexit

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${BASE_DIR}/../lib/lib-docker.sh"

for image_id in $(docker_exec images -qf dangling=true)
do
  docker_exec rmi "${image_id}"
done
