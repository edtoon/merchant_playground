#!/bin/bash
#
# Run the Apache/FPM server that hosts the Admin web application
#

set -o errexit

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${BASE_DIR}/../lib/lib-blake-environment.sh"
. "${BASE_DIR}/../lib/lib-config.sh"
. "${BASE_DIR}/../lib/lib-docker.sh"

SRC_DIR="${SRC_DIR:=$(config_get src_dir)}"
WEB_APP="${WEB_APP:=$(config_get web.default)}"
DOCKER_NAME="${DOCKER_NAME:=$(config_get web.${WEB_APP}.docker)}"
WEB_DIR="${WEB_DIR:=${SRC_DIR}/${WEB_APP}/web}"
MEMCACHE_ID="${MEMCACHE_ID:=$(config_get memcache.default)}"
MEMCACHE_DOCKER_NAME="${MEMCACHE_DOCKER_NAME:=$(config_get memcache.${MEMCACHE_ID}.docker)}"
docker_link="--link ${MEMCACHE_DOCKER_NAME}:${MEMCACHE_DOCKER_NAME}"

if [ "dev" = "${BLAKE_ENVIRONMENT}" ];
then
  DB_ID="${DB_ID:=$(config_get database.default)}"
  MYSQL_DOCKER_NAME="${MYSQL_DOCKER_NAME:=$(config_get database.${DB_ID}.docker)}"
  docker_link="${docker_link} --link ${MYSQL_DOCKER_NAME}:${MYSQL_DOCKER_NAME}"
fi

docker_exec rm "${DOCKER_NAME}" > /dev/null 2>&1 || true
docker_exec run -d --name "${DOCKER_NAME}" -e "MERCHANT_ENV=${BLAKE_ENVIRONMENT}" ${docker_link} \
  -v "${WEB_DIR}:/var/www/html:ro" -v "${SRC_DIR}:${BLAKE_APP_DIR}:ro" -p 80:80 blakeprinting/php-web
