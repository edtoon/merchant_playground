#!/bin/bash
#
# Tail the active merchant logs
#

set -o errexit

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${BASE_DIR}/../lib/lib-config.sh"

LOG_DIR="$(config_get log_dir)"

tail -F "${LOG_DIR}"/*.log
