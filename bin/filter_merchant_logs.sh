#!/bin/bash
#
# Check the active import logs for any lines that don't fit the most commonly seen patterns
#

set -o errexit

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${BASE_DIR}/../lib/lib-config.sh"

LOG_DIR="$(config_get log_dir)"

cat "${LOG_DIR}"/*.log | \
  grep -Ev '^$|DEBUG|unrecognized state| is pending| was cancelled|Use of iconv|Missing price|ed 1 new order item|ed 1 updated order item|^Current time: |.INFO: Stored 100 '
