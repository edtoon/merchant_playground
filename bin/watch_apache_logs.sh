#!/bin/bash
#
# Tail the active Apache server logs
#

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${BASE_DIR}/../lib/lib-config.sh"
. "${BASE_DIR}/../lib/lib-docker.sh"

WEB_ID="${WEB_ID:=$(config_get web.default)}"
DOCKER_NAME="${DOCKER_NAME:=$(config_get web.${WEB_ID}.docker)}"

docker_exec logs -f "${DOCKER_NAME}"
