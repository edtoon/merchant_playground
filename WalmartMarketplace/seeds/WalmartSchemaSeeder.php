<?php

use BlakePrinting\Db\DbSeed;
use BlakePrinting\Util\ConfigUtil;
use BlakePrinting\Util\Utils;
use BlakePrinting\WalmartMarketplace\WalmartMarketplaceCredentialDAO;
use BlakePrinting\WalmartMarketplace\WalmartMarketplaceCredentialVO;
use BlakePrinting\WalmartMarketplace\WalmartRefundReasonDAO;
use BlakePrinting\WalmartMarketplace\WalmartRefundReasonVO;
use BlakePrinting\WalmartMarketplace\WalmartShippingCarrierDAO;
use BlakePrinting\WalmartMarketplace\WalmartShippingCarrierVO;
use BlakePrinting\WalmartMarketplace\WalmartShippingMethodDAO;
use BlakePrinting\WalmartMarketplace\WalmartShippingMethodVO;
use BlakePrinting\WalmartMarketplace\WalmartUtil;

class WalmartSchemaSeeder extends DbSeed {
    public function run() {
        $connection =& $this->getConnection();
        $config =& ConfigUtil::getConfig();
        $consumerId = $config->retrieve(WalmartUtil::CONFIG_CATEGORY, WalmartUtil::CONFIG_CONSUMER_ID);

        $credentialDao =& WalmartMarketplaceCredentialDAO::instance($connection);
        $credentialDao->findOrCreate(new WalmartMarketplaceCredentialVO($consumerId));

        $shippingCarrierDao =& WalmartShippingCarrierDAO::instance($connection);
        Utils::forEachConstantInObjectClass(
            $shippingCarrierDao,
            function($_, $constantName, $constantValue) use (&$shippingCarrierDao) {
                $shippingCarrierDao->findOrCreate(new WalmartShippingCarrierVO($constantValue));
            }, "WALMART_SHIPPING_CARRIER_"
        );

        $shippingMethodDao =& WalmartShippingMethodDAO::instance($connection);
        Utils::forEachConstantInObjectClass(
            $shippingMethodDao,
            function($_, $constantName, $constantValue) use (&$shippingMethodDao) {
                $shippingMethodDao->findOrCreate(new WalmartShippingMethodVO($constantValue));
            }, "WALMART_SHIPPING_METHOD_"
        );

        $refundReasonDao =& WalmartRefundReasonDAO::instance($connection);
        Utils::forEachConstantInObjectClass(
            $refundReasonDao,
            function($_, $constantName, $constantValue) use (&$refundReasonDao) {
                $refundReasonDao->findOrCreate(new WalmartRefundReasonVO($constantValue));
            }, "WALMART_REFUND_REASON_"
        );
    }
}
