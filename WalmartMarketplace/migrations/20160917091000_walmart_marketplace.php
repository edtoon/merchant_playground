<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\Users\UserDAO;
use BlakePrinting\WalmartMarketplace\WalmartAddressDAO;
use BlakePrinting\WalmartMarketplace\WalmartAddressTypeDAO;
use BlakePrinting\WalmartMarketplace\WalmartChargeNameDAO;
use BlakePrinting\WalmartMarketplace\WalmartChargeTypeDAO;
use BlakePrinting\WalmartMarketplace\WalmartEmailDAO;
use BlakePrinting\WalmartMarketplace\WalmartItemDAO;
use BlakePrinting\WalmartMarketplace\WalmartMarketplaceCredentialDAO;
use BlakePrinting\WalmartMarketplace\WalmartPhoneDAO;
use BlakePrinting\WalmartMarketplace\WalmartRefundReasonDAO;
use BlakePrinting\WalmartMarketplace\WalmartShippingCarrierDAO;
use BlakePrinting\WalmartMarketplace\WalmartShippingMethodDAO;
use BlakePrinting\WalmartMarketplace\WalmartTaxDAO;
use BlakePrinting\WalmartMarketplace\WalmartUnitOfMeasurementDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class WalmartMarketplace extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();
        $userDao =& UserDao::instance($connection);
        $userTbl = $userDao->formatSchemaAndTableName();

        $credentialDao =& WalmartMarketplaceCredentialDAO::instance($connection);
        $this->tableDao($credentialDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("creator", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("updater", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("consumer_id", "string", ["limit" => 191])
            ->addForeignKey("creator", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addForeignKey("updater", $userTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex("consumer_id", ["unique" => true])
            ->save()
        ;

        $shippingCarrierDao =& WalmartShippingCarrierDAO::instance($connection);
        $this->tableDao($shippingCarrierDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("code", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("code", ["unique" => true])
            ->save()
        ;

        $shippingMethodDao =& WalmartShippingMethodDAO::instance($connection);
        $this->tableDao($shippingMethodDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("code", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("code", ["unique" => true])
            ->save()
        ;

        $unitOfMeasurementDao =& WalmartUnitOfMeasurementDAO::instance($connection);
        $this->tableDao($unitOfMeasurementDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("code", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("code", ["unique" => true])
            ->save()
        ;

        $chargeTypeDao =& WalmartChargeTypeDAO::instance($connection);
        $this->tableDao($chargeTypeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("code", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("code", ["unique" => true])
            ->save()
        ;

        $chargeNameDao =& WalmartChargeNameDAO::instance($connection);
        $this->tableDao($chargeNameDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $taxDao =& WalmartTaxDAO::instance($connection);
        $this->tableDao($taxDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $refundReasonDao =& WalmartRefundReasonDAO::instance($connection);
        $this->tableDao($refundReasonDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("code", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("code", ["unique" => true])
            ->save()
        ;

        $addressTypeDao =& WalmartAddressTypeDAO::instance($connection);
        $this->tableDao($addressTypeDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $addressDao =& WalmartAddressDAO::instance($connection);
        $this->tableDao($addressDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("walmart_address_type_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_TINY, "null" => true])
            ->addColumn("name", "string", ["limit" => 191, "null" => true])
            ->addColumn("address1", "string", ["limit" => 191, "null" => true])
            ->addColumn("address2", "string", ["limit" => 191, "null" => true])
            ->addColumn("city", "string", ["limit" => 191, "null" => true])
            ->addColumn("state", "string", ["limit" => 191, "null" => true])
            ->addColumn("postal_code", "string", ["limit" => 191, "null" => true])
            ->addColumn("country", "string", ["limit" => 191, "null" => true])
            ->addForeignKey("walmart_address_type_id", $addressTypeDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("name")
            ->addIndex("address1")
            ->addIndex("address2")
            ->addIndex("city")
            ->addIndex("state")
            ->addIndex("postal_code")
            ->addIndex("country")
            ->save()
        ;

        $emailDao =& WalmartEmailDAO::instance($connection);
        $this->tableDao($emailDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("email", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("email", ["unique" => true])
            ->save()
        ;

        $phoneDao =& WalmartPhoneDAO::instance($connection);
        $this->tableDao($phoneDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("phone", "string", ["limit" => 191])
            ->addIndex("created")
            ->addIndex("phone", ["unique" => true])
            ->save()
        ;

        $itemDao =& WalmartItemDAO::instance($connection);
        $this->tableDao($itemDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("sku", "string", ["limit" => 191, "null" => true])
            ->addColumn("product_name", "text", ["null" => true])
            ->addIndex("created")
            ->addIndex("sku")
            ->save()
        ;
        $this->alterDao($itemDao, "ADD INDEX (product_name(191))");
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(WalmartItemDAO::instance($connection));
        $this->dropDao(WalmartPhoneDAO::instance($connection));
        $this->dropDao(WalmartEmailDAO::instance($connection));
        $this->dropDao(WalmartAddressDAO::instance($connection));
        $this->dropDao(WalmartAddressTypeDAO::instance($connection));
        $this->dropDao(WalmartRefundReasonDAO::instance($connection));
        $this->dropDao(WalmartTaxDAO::instance($connection));
        $this->dropDao(WalmartChargeNameDAO::instance($connection));
        $this->dropDao(WalmartChargeTypeDAO::instance($connection));
        $this->dropDao(WalmartUnitOfMeasurementDAO::instance($connection));
        $this->dropDao(WalmartShippingMethodDAO::instance($connection));
        $this->dropDao(WalmartShippingCarrierDAO::instance($connection));
        $this->dropDao(WalmartMarketplaceCredentialDAO::instance($connection));
    }
}
