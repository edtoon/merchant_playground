<?php

namespace BlakePrinting\WalmartMarketplace;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static WalmartPhoneDAO instance(PDO $connection = null, $schemaName = null)
 * @method WalmartPhoneVO findById($id, $options = null, $useCache = false)
 * @method WalmartPhoneVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartPhoneVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartPhoneVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartPhoneVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartPhoneVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartPhoneVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartPhoneVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class WalmartPhoneDAO extends AbstractDataAccessObject {
}
