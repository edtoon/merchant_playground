<?php

namespace BlakePrinting\WalmartMarketplace;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static WalmartChargeNameDAO instance(PDO $connection = null, $schemaName = null)
 * @method WalmartChargeNameVO findById($id, $options = null, $useCache = false)
 * @method WalmartChargeNameVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartChargeNameVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartChargeNameVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartChargeNameVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartChargeNameVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartChargeNameVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartChargeNameVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class WalmartChargeNameDAO extends AbstractDataAccessObject {
}
