<?php

namespace BlakePrinting\WalmartMarketplace;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static WalmartTaxDAO instance(PDO $connection = null, $schemaName = null)
 * @method WalmartTaxVO findById($id, $options = null, $useCache = false)
 * @method WalmartTaxVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartTaxVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartTaxVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartTaxVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartTaxVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartTaxVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartTaxVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class WalmartTaxDAO extends AbstractDataAccessObject {
}
