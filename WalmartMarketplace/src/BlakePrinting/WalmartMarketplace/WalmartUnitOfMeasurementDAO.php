<?php

namespace BlakePrinting\WalmartMarketplace;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static WalmartUnitOfMeasurementDAO instance(PDO $connection = null, $schemaName = null)
 * @method WalmartUnitOfMeasurementVO findById($id, $options = null, $useCache = false)
 * @method WalmartUnitOfMeasurementVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartUnitOfMeasurementVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartUnitOfMeasurementVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartUnitOfMeasurementVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartUnitOfMeasurementVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartUnitOfMeasurementVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartUnitOfMeasurementVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class WalmartUnitOfMeasurementDAO extends AbstractDataAccessObject {
}
