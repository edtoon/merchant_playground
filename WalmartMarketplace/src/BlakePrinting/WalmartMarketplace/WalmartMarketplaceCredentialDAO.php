<?php

namespace BlakePrinting\WalmartMarketplace;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static WalmartMarketplaceCredentialDAO instance(PDO $connection = null, $schemaName = null)
 * @method WalmartMarketplaceCredentialVO findById($id, $options = null, $useCache = false)
 * @method WalmartMarketplaceCredentialVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartMarketplaceCredentialVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartMarketplaceCredentialVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartMarketplaceCredentialVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartMarketplaceCredentialVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartMarketplaceCredentialVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartMarketplaceCredentialVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class WalmartMarketplaceCredentialDAO extends AbstractDataAccessObject {
}
