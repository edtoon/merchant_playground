<?php

namespace BlakePrinting\WalmartMarketplace;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static WalmartRefundReasonDAO instance(PDO $connection = null, $schemaName = null)
 * @method WalmartRefundReasonVO findById($id, $options = null, $useCache = false)
 * @method WalmartRefundReasonVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartRefundReasonVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartRefundReasonVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartRefundReasonVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartRefundReasonVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartRefundReasonVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartRefundReasonVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class WalmartRefundReasonDAO extends AbstractDataAccessObject {
    const WALMART_REFUND_REASON_BILLING_ERR = "BillingError";
    const WALMART_REFUND_REASON_TAX_EXEMPT = "TaxExemptCustomer";
    const WALMART_REFUND_REASON_NOT_AS_ADV = "ItemNotAsAdvertised";
    const WALMART_REFUND_REASON_INCORRECT = "IncorrectItemReceived";
    const WALMART_REFUND_REASON_CANCELLED = "CancelledYetShipped";
    const WALMART_REFUND_REASON_DID_NOT_REC = "ItemNotReceivedByCustomer";
    const WALMART_REFUND_REASON_SHIP_PRICE = "IncorrectShippingPrice";
    const WALMART_REFUND_REASON_DAMAGED = "DamagedItem";
    const WALMART_REFUND_REASON_DEFECTIVE = "DefectiveItem";
    const WALMART_REFUND_REASON_CHANGED_MIND = "CustomerChangedMind";
    const WALMART_REFUND_REASON_LATE = "CustomerReceivedItemLate";
    const WALMART_REFUND_REASON_INCOMPLETE = "Missing Parts / Instructions";
    const WALMART_REFUND_REASON_GOODWILL = "Finance -> Goodwill";
    const WALMART_REFUND_REASON_ROLLBACK = "Finance -> Rollback";
}
