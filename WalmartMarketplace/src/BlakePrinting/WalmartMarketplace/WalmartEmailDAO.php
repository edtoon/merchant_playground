<?php

namespace BlakePrinting\WalmartMarketplace;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static WalmartEmailDAO instance(PDO $connection = null, $schemaName = null)
 * @method WalmartEmailVO findById($id, $options = null, $useCache = false)
 * @method WalmartEmailVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartEmailVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartEmailVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartEmailVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartEmailVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartEmailVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartEmailVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class WalmartEmailDAO extends AbstractDataAccessObject {
}
