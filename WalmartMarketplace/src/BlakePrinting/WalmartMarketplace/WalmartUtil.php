<?php

namespace BlakePrinting\WalmartMarketplace;

use BlakePrinting\Util\ConfigUtil;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\RequestOptions;
use phpseclib\Crypt\RSA;
use Psr\Http\Message\ResponseInterface;
use Ramsey\Uuid\Uuid;

class WalmartUtil {
    const CONFIG_CATEGORY = "walmart_marketplace";
    const CONFIG_CONSUMER_ID = "consumer_id";
    const CONFIG_SECRET_KEY = "secret_key";

    /** @var RSA */
    private static $rsaPrivateKey = null;

    private function __construct() { }

    /**
     * @param string $requestPath
     * @param WalmartMarketplaceCredentialVO $walmartMarketplaceCredentialVo
     * @param string $requestId
     * @return ResponseInterface
     */
    public static function executeSignedRequest($requestPath, WalmartMarketplaceCredentialVO $walmartMarketplaceCredentialVo, $requestId = null) {
        $consumerId = $walmartMarketplaceCredentialVo->getConsumerId();
        $baseUrl = "https://marketplace.walmartapis.com" . $requestPath;
        $httpMethod = "GET";
        $timestamp = Utils::getCurrentTimeMillis();
        $stringToSign =
            $consumerId . "\n" .
            $baseUrl . "\n" .
            $httpMethod . "\n" .
            $timestamp . "\n"
        ;
        $signatureBinary = "";

        openssl_sign($stringToSign, $signatureBinary, self::getRsaPrivateKey(), "SHA256");

        $signatureBase64 = base64_encode($signatureBinary);

        if($requestId === null) {
            $requestId = Uuid::uuid4()->toString();
        }

        $logger =& LogUtil::getLogger();

        $logger->debug("Executing Walmart request ID: " . $requestId . " for path: " . $requestPath . " with consumer ID: " . $consumerId);

        $guzzle = new GuzzleClient();
        $response = $guzzle->request(
            $httpMethod, $baseUrl,
            [
                RequestOptions::HEADERS => [
                    "WM_SVC.NAME" => "Walmart Marketplace",
                    "WM_CONSUMER.ID" => $consumerId,
                    "WM_SEC.TIMESTAMP" => $timestamp,
                    "WM_SEC.AUTH_SIGNATURE" => $signatureBase64,
                    "WM_QOS.CORRELATION_ID" => $requestId
                ],
                RequestOptions::HTTP_ERRORS => true
            ]
        );

        return $response;
    }

    private static function &getRsaPrivateKey() {
        if(self::$rsaPrivateKey === null) {
            $config =& ConfigUtil::getConfig();
            $secretKey = $config->retrieve(self::CONFIG_CATEGORY, self::CONFIG_SECRET_KEY);
            $rsa = new RSA();

            $rsa->setPrivateKeyFormat(RSA::PRIVATE_FORMAT_PKCS8);
            $rsa->loadKey($secretKey);

            self::$rsaPrivateKey = $rsa->getPrivateKey(RSA::PRIVATE_FORMAT_PKCS1);
        }

        return self::$rsaPrivateKey;
    }
}
