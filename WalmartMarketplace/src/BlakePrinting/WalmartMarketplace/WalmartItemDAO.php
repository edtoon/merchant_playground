<?php

namespace BlakePrinting\WalmartMarketplace;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static WalmartItemDAO instance(PDO $connection = null, $schemaName = null)
 * @method WalmartItemVO findById($id, $options = null, $useCache = false)
 * @method WalmartItemVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartItemVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartItemVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartItemVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartItemVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartItemVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartItemVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class WalmartItemDAO extends AbstractDataAccessObject {
}
