<?php

namespace BlakePrinting\WalmartMarketplace;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static WalmartAddressDAO instance(PDO $connection = null, $schemaName = null)
 * @method WalmartAddressVO findById($id, $options = null, $useCache = false)
 * @method WalmartAddressVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartAddressVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartAddressVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartAddressVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartAddressVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartAddressVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartAddressVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class WalmartAddressDAO extends AbstractDataAccessObject {
}
