<?php

namespace BlakePrinting\WalmartMarketplace;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static WalmartShippingMethodDAO instance(PDO $connection = null, $schemaName = null)
 * @method WalmartShippingMethodVO findById($id, $options = null, $useCache = false)
 * @method WalmartShippingMethodVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartShippingMethodVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartShippingMethodVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartShippingMethodVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartShippingMethodVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartShippingMethodVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartShippingMethodVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class WalmartShippingMethodDAO extends AbstractDataAccessObject {
    const WALMART_SHIPPING_METHOD_STANDARD = "Standard";
    const WALMART_SHIPPING_METHOD_EXPRESS = "Express";
    const WALMART_SHIPPING_METHOD_ONE_DAY = "OneDay";
    const WALMART_SHIPPING_METHOD_FREIGHT = "Freight";
    const WALMART_SHIPPING_METHOD_GROUND = "Ground";
}
