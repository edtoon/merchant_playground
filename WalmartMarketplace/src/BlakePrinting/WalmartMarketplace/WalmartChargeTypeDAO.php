<?php

namespace BlakePrinting\WalmartMarketplace;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static WalmartChargeTypeDAO instance(PDO $connection = null, $schemaName = null)
 * @method WalmartChargeTypeVO findById($id, $options = null, $useCache = false)
 * @method WalmartChargeTypeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartChargeTypeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartChargeTypeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartChargeTypeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartChargeTypeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartChargeTypeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartChargeTypeVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class WalmartChargeTypeDAO extends AbstractDataAccessObject {
}
