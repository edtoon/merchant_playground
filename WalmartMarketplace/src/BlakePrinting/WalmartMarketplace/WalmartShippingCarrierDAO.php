<?php

namespace BlakePrinting\WalmartMarketplace;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static WalmartShippingCarrierDAO instance(PDO $connection = null, $schemaName = null)
 * @method WalmartShippingCarrierVO findById($id, $options = null, $useCache = false)
 * @method WalmartShippingCarrierVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartShippingCarrierVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartShippingCarrierVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartShippingCarrierVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartShippingCarrierVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartShippingCarrierVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartShippingCarrierVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class WalmartShippingCarrierDAO extends AbstractDataAccessObject {
    const WALMART_SHIPPING_CARRIER_OTHER = "OtherCarrier";
    const WALMART_SHIPPING_CARRIER_UPS = "UPS";
    const WALMART_SHIPPING_CARRIER_USPS = "USPS";
    const WALMART_SHIPPING_CARRIER_FEDEX = "FedEx";
    const WALMART_SHIPPING_CARRIER_AIRBORNE = "Airborne";
    const WALMART_SHIPPING_CARRIER_ONTRAC = "OnTrac";
}
