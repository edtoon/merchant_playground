<?php

namespace BlakePrinting\WalmartMarketplace;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static WalmartAddressTypeDAO instance(PDO $connection = null, $schemaName = null)
 * @method WalmartAddressTypeVO findById($id, $options = null, $useCache = false)
 * @method WalmartAddressTypeVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartAddressTypeVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartAddressTypeVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartAddressTypeVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method WalmartAddressTypeVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method WalmartAddressTypeVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method WalmartAddressTypeVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class WalmartAddressTypeDAO extends AbstractDataAccessObject {
}
