#!/bin/bash
#
# Download CLDR data
#

set -o errexit
set -o nounset

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

VEND_DIR="${VEND_DIR:=${BASE_DIR}/vendor}"
DL_DIR="${DL_DIR:=${BASE_DIR}/downloads}"

declare -a VENDOR_DOWNLOADS=(
  "unicode_cldr_core:https://codeload.github.com/unicode-cldr/cldr-core/zip/29.0.0#core.zip"
  "unicode_cldr_localenames:https://codeload.github.com/unicode-cldr/cldr-localenames-full/zip/29.0.0#localenames-full.zip"
  "unicode_cldr_numbers:https://codeload.github.com/unicode-cldr/cldr-numbers-full/zip/29.0.0#numbers-full.zip"
)

unzip -v > /dev/null 2>&1 || sudo apt-get install unzip

mkdir -p "${DL_DIR}" "${VEND_DIR}"

for vendor_download in ${VENDOR_DOWNLOADS[@]}
do
  vendor_dir="${VEND_DIR}/${vendor_download%%:*}"
  vendor_url="${vendor_download#*:}"

  if [ ! -d "${vendor_dir}" ];
  then
    download_filename="${vendor_url##*/}"
    download_path="${DL_DIR}/${download_filename}"

    if [ ! -f "${download_path}" ];
    then
      echo "Downloading: ${download_path: -4}"
      curl -o "${download_path}" "${vendor_url}"
    fi

    if [ "${download_path: -4}" = ".zip" ];
    then
      mkdir -p "${vendor_dir}"
      unzip "${download_path}" -d "${vendor_dir}"
    else
      echo "Downloaded: ${download_path: -4}"
    fi
  fi
done
