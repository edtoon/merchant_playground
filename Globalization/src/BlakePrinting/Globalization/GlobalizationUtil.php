<?php

namespace BlakePrinting\Globalization;

use BlakePrinting\Util\Utils;
use Exception;
use PDO;
use Stringy\StaticStringy as S;

class GlobalizationUtil {
    private function __construct() { }

    /**
     * @param string $isoCode
     * @param PDO $connection
     * @return CurrencyVO
     * @throws Exception
     */
    public static function &getCurrencyVoByIsoCode($isoCode, PDO &$connection = null) {
        $isoCode = Utils::getTrimmedStringOrNull($isoCode);
        $currencyVo = null;

        if($isoCode !== null) {
            $currencyVo =& CurrencyDAO::instance($connection)->findByColumn("iso_code", $isoCode, PDO::PARAM_STR, null, null, null, true);

            if($currencyVo === null) {
                throw new Exception("Couldn't find currency for code: " . $isoCode);
            }
        }

        return $currencyVo;
    }

    /**
     * @param string $country
     * @param PDO $connection
     * @return CountryVO
     * @throws Exception
     */
    public static function &getCountryVoByNameOrCode($country, PDO &$connection = null) {
        $country = Utils::getTrimmedStringOrNull($country);
        $countryVo = null;

        if($country !== null) {
            $countryCode = null;

            if(S::length($country) === 2) {
                $countryCode = $country;
            } else {
                switch($country) {
                    case "La Réunion":
                        $countryCode = "RE";
                        break;
                }
            }

            $countryDao =& CountryDAO::instance($connection);

            if($countryCode !== null) {
                $countryVo =& $countryDao->findByColumn("iso_code", $countryCode, PDO::PARAM_STR, null, null, null, true);
            } else {
                $countryVo =& $countryDao->findByColumn("name", $country, PDO::PARAM_STR, null, null, null, true);
            }

            if($countryVo === null) {
                throw new Exception("Couldn't find country: " . $country);
            }
        }

        return $countryVo;
    }
}
