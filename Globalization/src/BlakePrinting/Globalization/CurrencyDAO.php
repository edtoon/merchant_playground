<?php

namespace BlakePrinting\Globalization;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @link https://en.wikipedia.org/wiki/ISO_4217
 *
 * @method static CurrencyDAO instance(PDO $connection = null, $schemaName = null)
 * @method CurrencyVO findById($id, $options = null, $useCache = false)
 * @method CurrencyVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CurrencyVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CurrencyVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method CurrencyVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CurrencyVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CurrencyVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method CurrencyVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class CurrencyDAO extends AbstractDataAccessObject {
    const CURRENCY_AED = ["AED", 2, "United Arab Emirates dirham"];
    const CURRENCY_ANG = ["ANG", 2, "Netherlands Antillean guilder"];
    const CURRENCY_ARS = ["ARS", 2, "Argentine peso"];
    const CURRENCY_AUD = ["AUD", 2, "Australian dollar"];
    const CURRENCY_BDT = ["BDT", 2, "Bangladeshi taka"];
    const CURRENCY_BRL = ["BRL", 2, "Brazilian real"];
    const CURRENCY_CAD = ["CAD", 2, "Canadian dollar"];
    const CURRENCY_CHF = ["CHF", 2, "Swiss franc"];
    const CURRENCY_CLP = ["CLP", 0, "Chilean peso"];
    const CURRENCY_CNY = ["CNY", 2, "Chinese yuan"];
    const CURRENCY_COP = ["COP", 2, "Colombian peso"];
    const CURRENCY_CRC = ["CRC", 2, "Costa Rican colon"];
    const CURRENCY_CZK = ["CZK", 2, "Czech koruna"];
    const CURRENCY_DKK = ["DKK", 2, "Danish krone"];
    const CURRENCY_DOP = ["DOP", 2, "Dominican peso"];
    const CURRENCY_EGP = ["EGP", 2, "Egyptian pound"];
    const CURRENCY_EUR = ["EUR", 2, "Euro"];
    const CURRENCY_GBP = ["GBP", 2, "Pound sterling"];
    const CURRENCY_HKD = ["HKD", 2, "Hong Kong dollar"];
    const CURRENCY_HUF = ["HUF", 2, "Hungarian forint"];
    const CURRENCY_IDR = ["IDR", 2, "Indonesian rupiah"];
    const CURRENCY_ILS = ["ILS", 2, "Israeli new shekel"];
    const CURRENCY_INR = ["INR", 2, "Indian rupee"];
    const CURRENCY_JOD = ["JOD", 3, "Jordanian dinar"];
    const CURRENCY_JPY = ["JPY", 0, "Japanese yen"];
    const CURRENCY_KHR = ["KHR", 2, "Cambodian riel"];
    const CURRENCY_KRW = ["KRW", 0, "South Korean won"];
    const CURRENCY_KWD = ["KWD", 3, "Kuwaiti dinar"];
    const CURRENCY_MXN = ["MXN", 2, "Mexican peso"];
    const CURRENCY_NOK = ["NOK", 2, "Norwegian krone"];
    const CURRENCY_NZD = ["NZD", 2, "New Zealand dollar"];
    const CURRENCY_PHP = ["PHP", 2, "Philippine peso"];
    const CURRENCY_PKR = ["PKR", 2, "Pakistani rupee"];
    const CURRENCY_PLN = ["PLN", 2, "Polish złoty"];
    const CURRENCY_QAR = ["QAR", 2, "Qatari riyal"];
    const CURRENCY_RUB = ["RUB", 2, "Russian ruble"];
    const CURRENCY_SAR = ["SAR", 2, "Saudi riyal"];
    const CURRENCY_SEK = ["SEK", 2, "Swedish krona"];
    const CURRENCY_SGD = ["SGD", 2, "Singapore dollar"];
    const CURRENCY_THB = ["THB", 2, "Thai baht"];
    const CURRENCY_TRY = ["TRY", 2, "Turkish lira"];
    const CURRENCY_TWD = ["TWD", 2, "New Taiwan dollar"];
    const CURRENCY_USD = ["USD", 2, "United States dollar"];
    const CURRENCY_VND = ["VND", 0, "Vietnamese dong"];
    const CURRENCY_ZAR = ["ZAR", 2, "South African rand"];
}
