<?php

namespace BlakePrinting\Globalization;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @link https://en.wikipedia.org/wiki/ISO_3166-1
 *
 * @method static CountryDAO instance(PDO $connection = null, $schemaName = null)
 * @method CountryVO findById($id, $options = null, $useCache = false)
 * @method CountryVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CountryVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CountryVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method CountryVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method CountryVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method CountryVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 */
class CountryDAO extends AbstractDataAccessObject {
    const IGNORE_CLDR_COUNTRIES = ["AN", "BV", "HM", "CP", "EU", "QO", "ZZ"];
}
