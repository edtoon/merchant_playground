<?php

namespace BlakePrinting\Globalization;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @link http://php.net/manual/en/datetimezone.getlocation.php
 *
 * @method static TimezoneDAO instance(PDO $connection = null, $schemaName = null)
 * @method TimezoneVO findById($id, $options = null, $useCache = false)
 * @method TimezoneVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TimezoneVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TimezoneVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TimezoneVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method TimezoneVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method TimezoneVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method TimezoneVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class TimezoneDAO extends AbstractDataAccessObject {
    const DEFAULT_TIMEZONE = "America/Los_Angeles";
}
