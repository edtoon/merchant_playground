<?php

namespace BlakePrinting\Globalization;

use BlakePrinting\Util\ConfigUtil;
use BlakePrinting\Util\Utils;
use Exception;
use PDO;

class CldrUtil {
    /* @var CountryVO[] */
    private static $euCountries = null;

    private function __construct() { }

    /**
     * @param PDO $connection
     * @return CountryVO[]
     */
    public static function getEuCountries(PDO &$connection) {
        if(self::$euCountries === null) {
            $countryDao =& CountryDAO::instance($connection);
            $territoryContainmentJson = Utils::decodeJson(file_get_contents(CldrUtil::getCoreDirectory() . "/supplemental/territoryContainment.json"));
            $euTerritories = $territoryContainmentJson["supplemental"]["territoryContainment"]["EU"]["_contains"];
            $euCountries = [];

            ksort($euTerritories);

            foreach($euTerritories as &$countryCode) {
                if(CldrUtil::isValidCountryCode($countryCode)) {
                    $countryVo = $countryDao->findByColumn("iso_code", $countryCode, PDO::PARAM_STR, null, null, "FOR UPDATE");

                    if($countryVo !== null) {
                        $euCountries[] = $countryVo;
                    }
                }
            }

            unset($countryCode);

            self::$euCountries =& $euCountries;
        }

        return self::$euCountries;
    }

    public static function isValidCountryCode($countryCode) {
        return ($countryCode !== null && strlen($countryCode) === 2 && ctype_alpha($countryCode) && !in_array($countryCode, CountryDAO::IGNORE_CLDR_COUNTRIES));
    }

    /**
     * @return string
     */
    public static function getCoreDirectory() {
        return self::getDataDirectory("core");
    }

    /**
     * @return string
     */
    public static function getLocaleNamesDirectory() {
        return self::getDataDirectory("localenames");
    }

    /**
     * @return string
     */
    public static function getNumbersDirectory() {
        return self::getDataDirectory("numbers");
    }

    /**
     * @param string $module
     * @return string
     * @throws Exception
     */
    private static function getDataDirectory($module) {
        $vendorDir = ConfigUtil::getVendorDir() . DIRECTORY_SEPARATOR . "unicode_cldr_" . $module;

        if(!is_dir($vendorDir)) {
            throw new Exception("Couldn't find vendor directory for module: " . $module);
        }

        $subDirs = glob($vendorDir . DIRECTORY_SEPARATOR . "*", GLOB_ONLYDIR);

        foreach($subDirs as &$dir) {
            if(!in_array($dir, $subDirs)) {
                $subDirs[] = $dir;
            }
        }

        unset($dir);

        if(count($subDirs) !== 1) {
            throw new Exception("Couldn't determine content directory for module: " . $module . ", candidates were: " . Utils::getVarDump($subDirs));
        }

        return $subDirs[0];
    }
}
