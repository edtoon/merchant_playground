<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\Globalization\CurrencyDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class Currencies extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $currencyDao = new CurrencyDAO($connection);
        $this->tableDao($currencyDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("iso_code", "string", ["limit" => 3])
            ->addColumn("decimals", "integer", ["null" => true, "signed" => false, "limit" => MysqlAdapter::INT_TINY])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("iso_code", ["unique" => true])
            ->addIndex("decimals")
            ->addIndex("name", ["unique" => true])
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(new CurrencyDAO($connection));
    }
}
