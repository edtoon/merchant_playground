<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\Globalization\CountryDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class Countries extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $countryDao =& CountryDAO::instance($connection);
        $this->tableDao($countryDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("iso_code", "string", ["limit" => 2])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("iso_code", ["unique" => true])
            ->addIndex("name", ["unique" => true])
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(CountryDAO::instance($connection));
    }
}
