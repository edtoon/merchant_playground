<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\Globalization\TimezoneDAO;

class TimezoneLongerLatAndLong extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $timezoneDao = TimezoneDAO::instance($connection);
        $this->tableDao($timezoneDao)
            ->changeColumn("latitude", "decimal", ["null" => true, "precision" => 17, "scale" => 15])
            ->changeColumn("longitude", "decimal", ["null" => true, "precision" => 18, "scale" => 15])
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $timezoneDao = TimezoneDAO::instance($connection);
        $this->tableDao($timezoneDao)
            ->changeColumn("latitude", "decimal", ["null" => true, "precision" => 7, "scale" => 5])
            ->changeColumn("longitude", "decimal", ["null" => true, "precision" => 8, "scale" => 5])
            ->save()
        ;
    }
}
