<?php

use BlakePrinting\Db\DbMigration;
use BlakePrinting\Globalization\TimezoneDAO;
use Phinx\Db\Adapter\MysqlAdapter;

class Timezones extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $timezoneDao = TimezoneDAO::instance($connection);
        $this->tableDao($timezoneDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_SMALL])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("name", "string", ["limit" => 191])
            ->addColumn("country_code", "string", ["limit" => 2, "null" => true])
            ->addColumn("latitude", "decimal", ["null" => true, "precision" => 7, "scale" => 5])
            ->addColumn("longitude", "decimal", ["null" => true, "precision" => 8, "scale" => 5])
            ->addColumn("comments", "text", ["null" => true])
            ->addColumn("meta_zone", "integer", ["limit" => MysqlAdapter::INT_TINY, "precision" => 1, "default" => 0])
            ->addIndex("name", ["unique" => true])
            ->addIndex("country_code")
            ->addIndex("latitude")
            ->addIndex("longitude")
            ->addIndex("meta_zone")
            ->save()
        ;
        $this->alterDao($timezoneDao, "ADD INDEX (comments(191))");
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(TimezoneDAO::instance($connection));
    }
}
