<?php

use BlakePrinting\Db\DbSeed;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Globalization\CldrUtil;
use BlakePrinting\Globalization\CountryDAO;
use BlakePrinting\Globalization\CountryVO;
use BlakePrinting\Globalization\CurrencyDAO;
use BlakePrinting\Globalization\CurrencyVO;
use BlakePrinting\Globalization\TimezoneDAO;
use BlakePrinting\Globalization\TimezoneVO;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;

class GlobalizationSeeder extends DbSeed {
    public final function run() {
        $this->populateCurrencies();
        $this->populateCountries();
        $this->populateTimezones();
    }

    private function populateCurrencies() {
        $connection =& $this->getConnection();
        $currencyDao =& CurrencyDAO::instance($connection);

        Utils::forEachConstantInObjectClass(
            $currencyDao,
            function($_, $constantName, $constantValue) use (&$currencyDao) {
                $currencyDao->findOrCreate(new CurrencyVO($constantValue[0], $constantValue[1], $constantValue[2]));
            },
            "CURRENCY_"
        );
    }

    private function populateCountries() {
        $connection =& $this->getConnection();
        $countryDao =& CountryDAO::instance($connection);
        $territoriesJson = Utils::decodeJson(file_get_contents(CldrUtil::getLocaleNamesDirectory() . "/main/en/territories.json"));
        $territories = $territoriesJson["main"]["en"]["localeDisplayNames"]["territories"];

        foreach($territories as $countryCode => $countryName) {
            if(CldrUtil::isValidCountryCode($countryCode)) {
                $countryVo =& $countryDao->findByColumn("iso_code", $countryCode, PDO::PARAM_STR, null, null, "FOR UPDATE");

                if($countryVo === null) {
                    $countryVo = new CountryVO();

                    $countryVo->setIsoCode($countryCode);
                    $countryVo->setName($countryName);

                    $countryDao->insert($countryVo);
                }
            }
        }
    }

    private function populateTimezones() {
        $connection =& $this->getConnection();
        $rootConnection =& PDOUtil::getRootConnection();
        $timezoneDao =& TimezoneDAO::instance($connection);
        $phpTimezoneIdentifiers = DateTimeZone::listIdentifiers();
        $logger =& LogUtil::getLogger();

        foreach($phpTimezoneIdentifiers as &$phpTimezoneIdentifier) {
            $dateTimeZone = new DateTimeZone($phpTimezoneIdentifier);
            $location = $dateTimeZone->getLocation();
            $countryCode = (empty($location["country_code"]) ? null : $location["country_code"]);
            $latitude = (empty($location["latitude"]) ? null : $location["latitude"]);
            $longitude = (empty($location["longitude"]) ? null : $location["longitude"]);
            $comments = (empty($location["comments"]) ? null : $location["comments"]);
            $metaZone = false;

            if($latitude === 0 && $longitude === 0) {
                $logger->info("Skipped: " . $phpTimezoneIdentifier . " - " . $countryCode . " - " . $latitude . "," . $longitude . " (" . $comments . ")");
                continue;
            }

            if($countryCode === "??") {
                $countryCode = null;
            }

            if($countryCode === null && $longitude === null && $latitude === null) {
                $metaZone = true;
            }

            $mysqlTimezoneId = PDOQuery::select()
                ->column("time_zone_id")
                ->from("mysql.time_zone_name")
                ->where("name", $phpTimezoneIdentifier, PDO::PARAM_STR)
                ->getSingleValue($rootConnection)
            ;

            if($mysqlTimezoneId === null) {
                $logger->warn("PHP timezone not found in MySQL: " . $phpTimezoneIdentifier);
                continue;
            }

            $timezoneVo =& $timezoneDao->findByColumn("name", $phpTimezoneIdentifier, PDO::PARAM_STR);

            if($timezoneVo === null) {
                $timezoneDao->insert(
                    new TimezoneVO($phpTimezoneIdentifier, $countryCode, $latitude, $longitude, $comments, $metaZone)
                );
            }
        }

        unset($phpTimezoneIdentifier);
    }
}
