<?php

namespace BlakePrinting\DataSources;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOQuery;
use PDO;
use Stringy\StaticStringy as S;

class AbstractEntityDataFeedDAO extends AbstractDataAccessObject {
    /** @var string */
    private $entityColumnName = null;

    public function __construct(PDO $connection = null, $schemaName = null, $entityColumnName = null) {
        parent::__construct($connection, $schemaName, null, null, null);

        $this->entityColumnName = $entityColumnName;
    }

    public function store(AbstractEntityDataFeedVO $abstractEntityDataFeedVo) {
        $voReflectionClass =& $this->getVoReflectionClass();
        $getEntityPrimaryKeyMethod = $voReflectionClass->getMethod("get" . S::upperCamelize($this->entityColumnName));

        return PDOQuery::insert()
            ->ignore()
            ->into($this)
            ->value($this->entityColumnName, $getEntityPrimaryKeyMethod->invoke($abstractEntityDataFeedVo), PDO::PARAM_INT)
            ->value("data_feed_id", $abstractEntityDataFeedVo->getDataFeedId(), PDO::PARAM_INT)
            ->executeGetRowCount($this->getConnection())
        ;
    }

    /**
     * @return string
     */
    public function getEntityColumnName()
    {
        return $this->entityColumnName;
    }

    /**
     * @param string $entityColumnName
     */
    public function setEntityColumnName($entityColumnName)
    {
        $this->entityColumnName = $entityColumnName;
    }
}
