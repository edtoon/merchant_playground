<?php

namespace BlakePrinting\DataSources;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static DataSourceIdentifierDAO instance(PDO $connection = null, $schemaName = null)
 * @method DataSourceIdentifierVO findById($id, $options = null, $useCache = false)
 * @method DataSourceIdentifierVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method DataSourceIdentifierVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method DataSourceIdentifierVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method DataSourceIdentifierVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method DataSourceIdentifierVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method DataSourceIdentifierVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method DataSourceIdentifierVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class DataSourceIdentifierDAO extends AbstractDataAccessObject {
    const IDENTIFIER_TYPE_ASIN = "ASIN";
    const IDENTIFIER_TYPE_ITEM_ID = "ItemId";
    const IDENTIFIER_TYPE_KIT_ID = "KitId";
    const IDENTIFIER_TYPE_FNSKU = "FnSKU";
    const IDENTIFIER_TYPE_SKU = "SKU";
    const IDENTIFIER_TYPE_UPC = "UPC";
}
