<?php

namespace BlakePrinting\DataSources;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static DataFeedDAO instance(PDO $connection = null, $schemaName = null)
 * @method DataFeedVO findById($id, $options = null, $useCache = false)
 * @method DataFeedVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method DataFeedVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method DataFeedVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method DataFeedVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method DataFeedVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method DataFeedVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method DataFeedVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class DataFeedDAO extends AbstractDataAccessObject {
}
