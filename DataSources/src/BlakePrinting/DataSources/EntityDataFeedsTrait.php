<?php

namespace BlakePrinting\DataSources;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\AbstractExtensibleEntity;
use BlakePrinting\Db\ValueObject;
use Exception;
use Stringy\StaticStringy as S;

trait EntityDataFeedsTrait {
    /** @var DataFeedVO[] */
    private $dataFeeds = [];
    /** @var AbstractDataAccessObject */
    private $entityDataFeedDao = null;

    /**
     * @throws Exception
     */
    public function insertDataFeeds() {
        if(!$this instanceof AbstractExtensibleEntity) {
            throw new Exception("Tried to add data feeds to object that's not an extensible entity: " . get_class($this));
        } else {
            $entityColumnName = $this->getEntityColumnName();
            $entityColumnValue = $this->getEntityColumnValue();
            $dataFeedIds = [];

            if($this->entityDataFeedDao === null) {
                $this->entityDataFeedDao = $this->getExtensionDAO("DataFeed");
            }

            if(!empty($this->dataFeeds)) {
                $voReflectionClass = $this->entityDataFeedDao->getVoReflectionClass();
                $setEntityMethod = $voReflectionClass->getMethod("set" . S::upperCamelize($entityColumnName));
                $setDataFeedIdMethod = $voReflectionClass->getMethod("setDataFeedId");

                foreach($this->dataFeeds as &$dataFeedVo) {
                    $dataFeedId = $dataFeedVo->getId();
                    $dataFeedIds[] = $dataFeedId;
                    $vo = $voReflectionClass->newInstance();

                    if($vo instanceof ValueObject) {
                        $setEntityMethod->invoke($vo, $entityColumnValue);
                        $setDataFeedIdMethod->invoke($vo, $dataFeedId);

                        $this->entityDataFeedDao->insert($vo, null, true);
                    }
                }

                unset($dataFeedVo);
            }
        }
    }

    /**
     * @param DataFeedVO $dataFeedVo
     */
    public function addDataFeed(DataFeedVO $dataFeedVo) {
        $this->dataFeeds[] = $dataFeedVo;
    }

    /**
     * @return DataFeedVO[]
     */
    public function getDataFeeds() {
        return $this->dataFeeds;
    }

    /**
     * @return AbstractDataAccessObject
     */
    public function getEntityDataFeedDao()
    {
        return $this->entityDataFeedDao;
    }

    /**
     * @param AbstractDataAccessObject $entityDataFeedDao
     */
    public function setEntityDataFeedDao(AbstractDataAccessObject $entityDataFeedDao)
    {
        $this->entityDataFeedDao = $entityDataFeedDao;
    }
}
