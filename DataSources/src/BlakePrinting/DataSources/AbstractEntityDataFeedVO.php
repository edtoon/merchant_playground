<?php

namespace BlakePrinting\DataSources;

use BlakePrinting\Db\ValueObject;

abstract class AbstractEntityDataFeedVO implements ValueObject {
    /** @var int */
    private $dataFeedId = null;

    /**
     * @param int $dataFeedId
     */
    public function __construct($dataFeedId = null) {
        $this->dataFeedId = $dataFeedId;
    }

    /**
     * @return int
     */
    public function getDataFeedId()
    {
        return $this->dataFeedId;
    }

    /**
     * @param int $dataFeedId
     */
    public function setDataFeedId($dataFeedId)
    {
        $this->dataFeedId = $dataFeedId;
    }
}
