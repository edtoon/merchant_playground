<?php

namespace BlakePrinting\DataSources;

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\ValueObject;
use PDO;

/**
 * @method static DataSourceDAO instance(PDO $connection = null, $schemaName = null)
 * @method DataSourceVO findById($id, $options = null, $useCache = false)
 * @method DataSourceVO findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method DataSourceVO findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method DataSourceVO findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method DataSourceVO[] findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false)
 * @method DataSourceVO[] findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = [])
 * @method DataSourceVO[] findAll(callable $func = null, $orderBy = null, $limit = null, $options = null)
 * @method DataSourceVO findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);
 */
class DataSourceDAO extends AbstractDataAccessObject {
}
