<?php

use BlakePrinting\DataSources\DataFeedDAO;
use BlakePrinting\DataSources\DataSourceDAO;
use BlakePrinting\Db\DbMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class DataFeeds extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $dataFeedDao = DataFeedDAO::instance($connection);
        $dataSourceDao = DataSourceDAO::instance($connection);
        $this->tableDao($dataFeedDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("data_source_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addColumn("name", "string", ["limit" => 191])
            ->addForeignKey("data_source_id", $dataSourceDao->formatSchemaAndTableName(), "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex(["data_source_id", "name"], ["unique" => true])
            ->addIndex("name")
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(new DataFeedDAO($connection));
    }
}
