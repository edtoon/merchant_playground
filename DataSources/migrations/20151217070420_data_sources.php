<?php

use BlakePrinting\DataSources\DataSourceDAO;
use BlakePrinting\DataSources\DataSourceIdentifierDAO;
use BlakePrinting\Db\DbMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class DataSources extends DbMigration {
    public function up() {
        $connection =& $this->getConnection();

        $dataSourceDao = new DataSourceDAO($connection);
        $this->tableDao($dataSourceDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("name", "string", ["limit" => 191])
            ->addIndex("name", ["unique" => true])
            ->save()
        ;

        $dataSourceIdentifierDao = new DataSourceIdentifierDAO($connection);
        $dataSourceTbl = $dataSourceDao->formatSchemaAndTableName();
        $this->tableDao($dataSourceIdentifierDao, ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["id"]])
            ->addColumn("id", "integer", ["identity" => true, "signed" => false])
            ->addColumn("created", "biginteger", ["signed" => false])
            ->addColumn("updated", "biginteger", ["signed" => false, "null" => true])
            ->addColumn("data_source_id", "integer", ["signed" => false, "limit" => MysqlAdapter::INT_MEDIUM])
            ->addColumn("name", "string", ["limit" => 191])
            ->addForeignKey("data_source_id", $dataSourceTbl, "id", ["update" => "RESTRICT", "delete" => "RESTRICT"])
            ->addIndex("created")
            ->addIndex("updated")
            ->addIndex(["data_source_id", "name"], ["unique" => true])
            ->addIndex("name")
            ->save()
        ;
    }

    public function down() {
        $connection =& $this->getConnection();

        $this->dropDao(new DataSourceIdentifierDAO($connection));
        $this->dropDao(new DataSourceDAO($connection));
    }
}
