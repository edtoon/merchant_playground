<?php

use BlakePrinting\Db\DbSeed;
use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Util\LogUtil;
use Monolog\Logger;
use Stringy\StaticStringy as S;

class CurrencySeeder extends DbSeed {
    const FED_UPDATE_TIME = (14 * 60 * 60) + (15 * 60);

    public function run() {
        $connection =& $this->getConnection();

        try {
            $this->importFedH10JpyRates("FRB_H10.csv");

            PDOUtil::commitConnection($connection);
        } catch(Exception $e) {
            LogUtil::logException(Logger::ERROR, $e, "Error importing currency CSV files");
        }

        PDOUtil::rollbackConnection($connection);
    }

    /*
     * http://www.federalreserve.gov/releases/h10/hist/dat00_ja.htm
     */
    private function importFedH10JpyRates($spreadsheetFile) {
        $fh = fopen(__DIR__ . DIRECTORY_SEPARATOR . $spreadsheetFile, "r");

        if($fh === false) {
            $logger =& LogUtil::getLogger();

            $logger->error("Failed to open file: " . $spreadsheetFile);
        } else {
            try {
                $timezone = new DateTimeZone("America/New_York");
                $connection =& $this->getConnection();
                $headerLine = true;

                while(($data = fgetcsv($fh, null, ",")) !== false) {
                    if($headerLine === true) {
                        $headerLine = false;

                        continue;
                    }

                    if(count($data) < 12) {
                        continue;
                    }

                    $date = $data[0];
                    $rate = $data[11];

                    if($date !== null && $rate !== null && $date != "ND" && $rate != "ND" && S::startsWith($date, "201")) {
                        $dateTime = new DateTime($date);
                        $dateTime->setTimezone($timezone);
                        $time = $dateTime->getTimestamp() + self::FED_UPDATE_TIME;

                        PDOQuery::insert()
                            ->into("merchant.currency_frb_h10_usd_to_jpy")
                            ->value("time", $time, PDO::PARAM_STR)
                            ->value("rate", $rate, PDO::PARAM_STR)
                            ->onDuplicateAllValues()
                            ->executeGetRowCount($connection)
                        ;
                    }
                }
            } finally {
                fclose($fh);
            }
        }
    }
}
