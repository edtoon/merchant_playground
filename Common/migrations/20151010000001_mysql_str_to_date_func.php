<?php

use BlakePrinting\Db\DbUtil;
use Phinx\Migration\AbstractMigration;

// http://stackoverflow.com/questions/24209988/str-to-date-and-iso8601-atomtime-format
class MysqlStrToDateFunc extends AbstractMigration {
    private $functionName = DbUtil::FUNC_FROM_ISO8601_SUBSET;

    public function up() {
        $sql = <<< __END__
          CREATE FUNCTION `$this->functionName`(in_ts TINYTEXT) RETURNS DATETIME
            DETERMINISTIC
            NO SQL
          BEGIN

            -- this function takes an input timestamp value in a supported subset of iso8601 values, and
            -- and converts it to the equivalent MySQL datetime value, expressed in the current session's
            -- time zone.  Since this is also the timezone that columns in the TIMESTAMP data type expect,
            -- this causes the input value to be stored correctly in the native TIMESTAMP format, which is.
            -- UTC under the hood.

            -- if you are taking the value here and stuffing it into a DATETIME column, you need to have your
            -- session @@time_zone set to the same zone in which that column should be stored, or use
            -- CONVERT(from_iso('input value'),'UTC','Your Desired Time Zone');

            -- unexpected values will be cast as a datetime.

            DECLARE offset_sign TINYINT DEFAULT NULL;
            DECLARE offset_hours TINYINT DEFAULT NULL;
            DECLARE offset_minutes TINYINT DEFAULT NULL;

            -- 2014-02-01T23:59:59Z --
            IF (in_ts REGEXP '^[[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2}T[[:digit:]]{2}:[[:digit:]]{2}:[[:digit:]]{2}Z$') THEN
              RETURN CONVERT_TZ(REPLACE(REPLACE(in_ts,'T',' '),'Z',''),'UTC',@@time_zone);

            -- 2014-09-18T07:00-06:00 -- or +05:00
            ELSEIF (in_ts REGEXP '^[[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2}T[[:digit:]]{2}:[[:digit:]]{2}:[[:digit:]]{2}[+-][[:digit:]]{2}:[[:digit:]]{2}$') THEN
               SET offset_sign = IF(SUBSTRING(in_ts FROM 20 FOR 1) = '+', -1, +1); # we need to flip the sign, to "back out" the offset to get a time in UTC
               SET offset_hours = CAST(SUBSTRING(in_ts FROM 21 FOR 2) AS SIGNED) * offset_sign;
               SET offset_minutes = CAST(SUBSTRING(in_ts FROM 24 FOR 2) AS SIGNED) * offset_sign;
               RETURN CONVERT_TZ(DATE_ADD(DATE_ADD(REPLACE(SUBSTRING(in_ts FROM 1 FOR 19),'T',' '), INTERVAL offset_hours HOUR), INTERVAL offset_minutes MINUTE),'UTC',@@time_zone);

            -- unexpected format -- let MySQL's built-in functions do the best they can; this will throw warnings
            -- if the input is not a yyyy-mm-dd hh:mm:ss datetime literal; alternately this could return NULL.
            ELSE
              RETURN CAST(in_ts AS DATETIME);
            END IF;

          END;
__END__;

        $this->execute($sql);
    }

    public function down() {
        $this->execute("DROP FUNCTION IF EXISTS " . $this->functionName);
    }
}
