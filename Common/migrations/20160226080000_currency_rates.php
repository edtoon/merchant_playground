<?php

use BlakePrinting\Db\DbMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CurrencyRates extends DbMigration {
    public function up() {
        $this->table("currency_frb_h10_usd_to_jpy", ["charset" => "utf8mb4", "collation" => "utf8mb4_bin", "id" => false, "primary_key" => ["time"]])
            ->addColumn("time", "biginteger", ["signed" => false])
            ->addColumn("rate", "string", ["limit" => 20])
            ->addIndex("time", ["unique" => true])
            ->addIndex("rate")
            ->save()
        ;
    }

    public function down() {
        $this->drop("currency_frb_h10_usd_to_jpy");
    }
}
