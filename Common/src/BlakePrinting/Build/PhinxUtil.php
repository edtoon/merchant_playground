<?php

namespace BlakePrinting\Build;

use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Util\ConfigUtil;
use Exception;
use Phinx\Config\Config;
use Phinx\Migration\Manager;
use Stringy\StaticStringy as S;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\OutputInterface;

class PhinxUtil {
    const ERR_NO_MIGRATIONS = 1;
    const ERR_NO_SEEDS = 2;

    private function __construct() { }

    /**
     * @param OutputInterface $output
     * @param string $moduleDir
     * @param string $command
     * @throws Exception
     */
    public static function executePhinx(OutputInterface &$output, $moduleDir, $command = "migrate") {
        $appDir = ConfigUtil::getAppDir();

        try {
            ConfigUtil::setAppDir($moduleDir);

            $moduleName = basename($moduleDir);
            $config =& ConfigUtil::getConfig(true);
            $database =& $config->retrieve("database", "default");
            $connectionParams =& PDOUtil::getConnectionParams($database);
            $host =& $connectionParams["host"];
            $port =& $connectionParams["port"];
            $schemas =& $config->retrieve("database", $database, "schemas");
            $defaultSchemaName =& self::findConnectSchema($moduleName, $database);
            $username =& $config->retrieve("database", $database, "username");
            $password =& $config->retrieve("database", $database, "password");
            $charset =& $config->retrieve("database", $database, "charset");
            $collation =& $config->retrieve("database", $database, "collation");
            $environment = "development";
            $migrationTable = "phinx_" . strtolower($moduleName);
            $options = [
                "paths" => [],
                "environments" => [
                    "default_migration_table" => $migrationTable,
                    "default_database" => $environment,
                    "development" => [
                        "adapter" => "mysql",
                        "host" => $host,
                        "port" => $port,
                        "name" => $defaultSchemaName,
                        "user" => $username,
                        "pass" => $password,
                        "charset" => $charset,
                        "collation" => $collation
                    ]
                ]
            ];

            if(is_dir($moduleDir . "/migrations")) {
                $options["paths"]["migrations"] = $moduleDir . "/migrations";
            } else if($command == "migrate") {
                throw new Exception("Migrations directory not found in module: " . $moduleDir, self::ERR_NO_MIGRATIONS);
            }

            if(is_dir($moduleDir . "/seeds")) {
                $options["paths"]["seeds"] = $moduleDir . "/seeds";
            } else if($command == "seed") {
                throw new Exception("Seeds directory not found in module: " . $moduleDir, self::ERR_NO_SEEDS);
            }

            $rootConnection =& PDOUtil::getRootConnection($database);

            foreach($schemas as $schema => &$schemaConfig) {
                if(isset($schemaConfig["name"])) {
                    $schemaName = $schemaConfig["name"];

                    $rootConnection->exec("CREATE DATABASE IF NOT EXISTS `" . $schemaName . "` CHARACTER SET = `" . $charset . "` COLLATE = `" . $collation . "`");
                    $rootConnection->exec("GRANT ALL ON `" . $schemaName . "`.* TO '" . $username . "'@'%' IDENTIFIED BY '" . $password . "'");
                }
            }

            unset($schemaConfig);

            $rootConnection->exec("FLUSH PRIVILEGES");

            $phinxConfig = new Config($options);
            $manager = new Manager($phinxConfig, new ArrayInput([]), $output);

            if($command == "migrate") {
                $manager->migrate($environment);
            } else if($command == "seed") {
                $manager->seed($environment);
            } else if($command == "rollback") {
                $manager->rollback($environment);
            }
        } finally {
            ConfigUtil::setAppDir($appDir);
            ConfigUtil::setConfig();
        }
    }

    private static function &findConnectSchema($moduleName, $database) {
        $config =& ConfigUtil::getConfig();
        $rawResult = $config->retrieve("database", $database, "schemas", strtolower($moduleName), "name");

        if($rawResult !== null) {
            return $rawResult;
        }

        $checkName = (string)S::underscored($moduleName);

        while(!empty($checkName)) {
            $checkResult = $config->retrieve("database", $database, "schemas", $checkName, "name");

            if($checkResult !== null) {
                return $checkResult;
            }

            $lastUnderscoreIdx = strrpos($checkName, "_");

            if($lastUnderscoreIdx === false) {
                break;
            }

            $checkName = substr($checkName, 0, $lastUnderscoreIdx);
        }

        $defaultSchema =& $config->retrieve("database", $database, "schemas", "default");
        $defaultSchemaName =& $config->retrieve("database", $database, "schemas", $defaultSchema, "name");

        return $defaultSchemaName;
    }
}
