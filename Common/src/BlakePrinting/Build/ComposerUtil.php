<?php

namespace BlakePrinting\Build;

use BlakePrinting\Util\Utils;
use Composer\Factory;
use Composer\Installer;
use Composer\IO\ConsoleIO;
use Exception;
use ReflectionClass;
use Symfony\Component\Console\Helper\DebugFormatterHelper;
use Symfony\Component\Console\Helper\FormatterHelper;
use Symfony\Component\Console\Helper\HelperSet;
use Symfony\Component\Console\Helper\ProcessHelper;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\OutputInterface;
use Wikimedia\Composer\Logger;
use Wikimedia\Composer\Merge\ExtraPackage;
use Wikimedia\Composer\MergePlugin;

class ComposerUtil {
    const DEPS_FILE = "/app/composer.deps";

    private function __construct() { }

    /**
     * @param string $dir
     * @return array ['/app/Baz/composer.json':['/app/Foo/composer.json','/app/Common/composer.json']]
     */
    public static function &executeAndReturnPathsWithDeps(OutputInterface &$output, $dir = "/app") {
        $cwd = getcwd();

        try {
            chdir($dir);

            $input = new ArrayInput([]);
            $input->setInteractive(false);
            $io = new ConsoleIO(
                $input, $output, new HelperSet([
                    new FormatterHelper(),
                    new DebugFormatterHelper(),
                    new ProcessHelper(),
                    new QuestionHelper()
                ])
            );
            $logger = new Logger('custom-composer', $io);
            $composer = Factory::create($io);

            Installer::create($io, $composer)
                ->setDevMode(true)
                ->setOptimizeAutoloader(true)
                ->setPreferStable(true)
                ->setUpdate(true)
                ->run()
            ;

            $composerFiles = [];
            $pluginClass = new ReflectionClass(MergePlugin::class);
            $loadedFilesProperty = $pluginClass->getProperty("loadedFiles");

            $loadedFilesProperty->setAccessible(true);

            foreach($composer->getPluginManager()->getPlugins() as &$plugin) {
                if($plugin instanceof MergePlugin) {
                    $loadedFiles = $loadedFilesProperty->getValue($plugin);

                    foreach($loadedFiles as $loadedFile => $bool) {
                        $loadedFilePath = realpath($dir . "/" . $loadedFile);
                        $loadedFileDir = dirname($loadedFilePath);
                        $loadedFileName = basename($loadedFilePath);

                        if(!isset($composerFiles[$loadedFilePath])) {
                            $composerFiles[$loadedFilePath] = [];
                        }

                        chdir($loadedFileDir);

                        $package = new ExtraPackage($loadedFileName, $composer, $logger);
                        $requiredFilePaths = [];

                        foreach($package->getRequires() as &$requiredFiles) {
                            foreach(glob($requiredFiles) as &$requiredFile) {
                                $requiredFilePath = realpath($requiredFile);

                                if(!in_array($requiredFilePath, $requiredFilePaths)) {
                                    $requiredFilePaths[] = $requiredFilePath;
                                }

                                if(!isset($composerFiles[$requiredFilePath])) {
                                    $composerFiles[$requiredFilePath] = [];
                                }
                            }

                            unset($requiredFile);
                        }

                        unset($requiredFiles);

                        $composerFiles[$loadedFilePath] = $composerFiles[$loadedFilePath] + $requiredFilePaths;
                    }

                    break;
                }
            }

            unset($plugin);
        } finally {
            chdir($cwd);
        }

        return $composerFiles;
    }

    /**
     * @param array $pathsWithDeps ['/app/Baz/composer.json':['/app/Foo/composer.json','/app/Common/composer.json']]
     * @return string[]
     * @throws Exception
     */
    public static function &getOrderedDependenciesFromPathsWithDeps(array &$pathsWithDeps) {
        $orderedDeps = [];
        $previousCount = 0;

        while(($count = count($pathsWithDeps)) > 0) {
            if($count == $previousCount) {
                break;
            }

            $previousCount = $count;
            $removePath = null;

            foreach($pathsWithDeps as $path => &$deps) {
                if(empty($deps)) {
                    $orderedDeps[] = $path;
                    $removePath = $path;

                    break;
                }
            }

            unset($deps);

            if($removePath !== null) {
                unset($pathsWithDeps[$removePath]);

                foreach($pathsWithDeps as $childPath => &$childDeps) {
                    if(($idx = array_search($removePath, $childDeps)) !== false) {
                        unset($pathsWithDeps[$childPath][$idx]);
                    }
                }

                unset($childDeps);
            }
        }

        $remaining = count($pathsWithDeps);

        if($remaining > 0) {
            throw new Exception("Could not resolve all component dependencies - " . $remaining . " parents remaining: " . Utils::getVarDump($pathsWithDeps));
        }

        return $orderedDeps;
    }

    /**
     * @param array $orderedDeps
     */
    public static function writeOrderedDependenciesToFile(array &$orderedDeps) {
        $fh = fopen(self::DEPS_FILE, "wb");

        try {
            if($fh === false) {
                throw new Exception("Could not write dependencies file: " . self::DEPS_FILE);
            }

            fwrite($fh, serialize($orderedDeps));
        } finally {
            if($fh !== false) {
                fclose($fh);
            }
        }
    }

    /**
     * @return string[]
     * @throws Exception
     */
    public static function &readOrderedDependenciesFromFile() {
        $fh = fopen(self::DEPS_FILE, "rb");

        try {
            if($fh === false) {
                throw new Exception("Could not find dependencies file: " . self::DEPS_FILE);
            }

            $orderedDeps = unserialize(fread($fh, filesize(self::DEPS_FILE)));

            return $orderedDeps;
        } finally {
            if($fh !== false) {
                fclose($fh);
            }
        }
    }
}
