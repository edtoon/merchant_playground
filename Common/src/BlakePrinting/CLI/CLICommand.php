<?php

namespace BlakePrinting\CLI;

use BlakePrinting\Util\LogUtil;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class CLICommand extends Command {
    public final function run(InputInterface $input, OutputInterface $output) {
        LogUtil::configureLogger(get_class($this));
        parent::run($input, $output);
    }
}
