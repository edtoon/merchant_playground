<?php

namespace BlakePrinting\CLI;

use Symfony\Component\Console\Application;
use Symfony\Component\Debug\ErrorHandler;

abstract class CLIApplication extends Application {
    public function __construct($name, $version) {
        parent::__construct($name, $version);
        ErrorHandler::register();
    }
}
