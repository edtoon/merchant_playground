<?php

namespace BlakePrinting\Test;

use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Util\ConfigUtil;
use Exception;

abstract class AbstractDbTestCase extends AbstractTestCase {
    public function setUp() {
        parent::setUp();

        if(!isset($GLOBALS["__PHPUNIT_CONFIGURATION_FILE"])) {
            throw new Exception("Can't find phpunit config file");
        }

        $testDir = dirname($GLOBALS["__PHPUNIT_CONFIGURATION_FILE"]) . DIRECTORY_SEPARATOR . "tests";
        $config =& ConfigUtil::createFromDirectory($testDir);

        ConfigUtil::setConfig($config);

        $database =& $config->retrieve("database", "default");
        $username =& $config->retrieve("database", $database, "username");
        $password =& $config->retrieve("database", $database, "password");
        $charset =& $config->retrieve("database", $database, "charset");
        $collation =& $config->retrieve("database", $database, "collation");
        $schemaName =& $config->retrieve("database", $database, "schemas", "test", "name");
        $rootConnection =& PDOUtil::getRootConnection();

        $rootConnection->exec("DROP DATABASE IF EXISTS `" . $schemaName . "`");
        $rootConnection->exec("CREATE DATABASE IF NOT EXISTS `" . $schemaName . "` CHARACTER SET = `" . $charset . "` COLLATE = `" . $collation . "`");
        $rootConnection->exec("GRANT ALL ON `" . $schemaName . "`.* TO '" . $username . "'@'%' IDENTIFIED BY '" . $password . "'");
        $rootConnection->exec("FLUSH PRIVILEGES");

        foreach(["select", "insert", "update", "delete"] as $stmtType) {
            $config->store("log", "sql", $stmtType, "bind", true);
            $config->store("log", "sql", $stmtType, "statement", true);
            $config->store("log", "sql", $stmtType, "result", true);
        }
    }

    public function tearDown() {
        $config =& ConfigUtil::getConfig();

        foreach(["select", "insert", "update", "delete"] as $stmtType) {
            $config->store("log", "sql", $stmtType, "bind", false);
            $config->store("log", "sql", $stmtType, "statement", false);
            $config->store("log", "sql", $stmtType, "result", false);
        }

        self::cleanUpDatabase();
        parent::tearDown();
    }

    public static function tearDownAfterClass() {
        self::cleanUpDatabase();
        parent::tearDownAfterClass();
    }

    private static function cleanUpDatabase() {
        $connections =& PDOUtil::getAllConnections();

        foreach($connections as &$connection) {
            PDOUtil::rollbackConnection($connection);
        }

        unset($connection);

        $config =& ConfigUtil::getConfig();
        $database =& $config->retrieve("database", "default");
        $schemaName =& $config->retrieve("database", $database, "schemas", "test", "name");
        $rootConnection =& PDOUtil::getRootConnection($database, $schemaName);

        $rootConnection->exec("DROP DATABASE IF EXISTS `" . $schemaName . "`");
    }
}
