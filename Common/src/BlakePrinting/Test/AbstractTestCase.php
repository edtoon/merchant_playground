<?php

namespace BlakePrinting\Test;

use BlakePrinting\Util\ConfigUtil;
use BlakePrinting\Util\LogUtil;
use PHPUnit_Framework_TestCase;

abstract class AbstractTestCase extends PHPUnit_Framework_TestCase {
    public static function setUpBeforeClass() {
        parent::setUpBeforeClass();
        self::resetConfig();
    }

    public static function tearDownAfterClass() {
        self::resetConfig();
        parent::tearDownAfterClass();
    }

    public function setUp() {
        parent::setUp();

        fwrite(STDERR, PHP_EOL);

        $testName = get_class($this) . "::" . $this->getName();

        LogUtil::configureLogger($testName);

        $logger =& LogUtil::getLogger();

        $logger->debug("-- Begin " . $testName . " --");
    }

    public function tearDown() {
        $logger =& LogUtil::getLogger();

        $logger->debug("-- End " . get_class($this) . "::" . $this->getName() . " --");

        fwrite(STDERR, PHP_EOL);

        parent::tearDown();
    }

    private static function resetConfig() {
        ConfigUtil::setAppDir();
        ConfigUtil::setConfig();
    }
}
