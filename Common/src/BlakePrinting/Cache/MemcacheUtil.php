<?php

namespace BlakePrinting\Cache;

use BlakePrinting\Util\ConfigUtil;
use BlakePrinting\Util\Utils;
use Exception;
use Memcached;

class MemcacheUtil {
    private static $memcaches = [];

    private function __construct() { }

    /**
     * @param string $pool
     *
     * @return Memcached
     */
    public static function &getMemcached($pool = null) {
        $config =& ConfigUtil::getConfig();

        $pool === null && $pool =& $config->retrieve("memcache", "default");

        if(empty(self::$memcaches[$pool])) {
            $config =& ConfigUtil::getConfig();
            $docker =& $config->retrieve("memcache", $pool, "docker");

            if(!empty($docker)) {
                $environmentValue = getenv(strtoupper($docker) . "_PORT");
                $connectionData = Utils::parseDockerPortVariable($environmentValue);

                if(!empty($connectionData["host"]) && !empty($connectionData["port"])) {
                    $memcached = new Memcached($pool);

                    $memcached->setOption(Memcached::OPT_BINARY_PROTOCOL, true);

                    if(empty($memcached->getServerList())) {
                        if($memcached->addServer($connectionData["host"], $connectionData["port"]) === false) {
                            throw new Exception("Failed to connect to memcache server at " . $connectionData["host"] . ":" . $connectionData["port"]);
                        }
                    }

                    self::$memcaches[$pool] =& $memcached;
                }
            }
        }

        return self::$memcaches[$pool];
    }
}
