<?php

namespace BlakePrinting\ETL;

use BlakePrinting\Db\ValueObject;

class ExtractResult {
    /** @var ValueObject */
    private $valueObject = null;
    /** @var bool */
    private $inserted = null;
    /** @var bool */
    private $updated = null;
    /** @var array */
    private $differences = null;

    /**
     * @param ValueObject $valueObject
     * @param bool $inserted
     * @param bool $updated
     * @param array $differences
     */
    public function __construct(ValueObject &$valueObject = null, $inserted = false, $updated = false, array &$differences = null) {
        $this->valueObject =& $valueObject;
        $this->inserted = $inserted;
        $this->updated = $updated;
        $this->differences =& $differences;
    }

    /**
     * @return ValueObject
     */
    public function getValueObject()
    {
        return $this->valueObject;
    }

    /**
     * @param ValueObject $valueObject
     */
    public function setValueObject(ValueObject $valueObject)
    {
        $this->valueObject = $valueObject;
    }

    /**
     * @return bool
     */
    public function getInserted()
    {
        return $this->inserted;
    }

    /**
     * @param bool $inserted
     */
    public function setInserted($inserted)
    {
        $this->inserted = $inserted;
    }

    /**
     * @return bool
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param bool $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return array
     */
    public function getDifferences()
    {
        return $this->differences;
    }

    /**
     * @param array $differences
     */
    public function setDifferences(array $differences)
    {
        $this->differences = $differences;
    }
}
