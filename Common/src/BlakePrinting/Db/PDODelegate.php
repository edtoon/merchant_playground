<?php

namespace BlakePrinting\Db;

use PDO;

class PDODelegate extends PDO {
    /** @var string */
    private $isolationLevel = null;
    /** @var PDO */
    private $pdo = null;
    /** @var string */
    private $uniqid = null;

    public function __construct(PDO $pdo) {
        $this->pdo =& $pdo;
        $this->uniqid = uniqid('', true);
    }

    public function getUniqId() {
        return $this->uniqid;
    }

    public function beginTransaction() {
        if($this->inTransaction()) {
            return false;
        }

        return call_user_func_array([$this->pdo, "beginTransaction"], func_get_args());
    }

    public function commit() {
        return call_user_func_array([$this->pdo, "commit"], func_get_args());
    }

    public function errorCode() {
        return call_user_func_array([$this->pdo, "errorCode"], func_get_args());
    }

    public function errorInfo() {
        return call_user_func_array([$this->pdo, "errorInfo"], func_get_args());
    }

    public function exec($statement) {
        $upperStatement = strtoupper($statement);

        if(substr($upperStatement, 0, 32) == "SET TRANSACTION ISOLATION LEVEL ") {
            $isolationLevel = substr($upperStatement, 32);

            if($this->isolationLevel == $isolationLevel) {
                return false;
            }

            $this->isolationLevel = $isolationLevel;
        }

        return call_user_func_array([$this->pdo, "exec"], func_get_args());
    }

    public function getAttribute($_ = null) {
        return call_user_func_array([$this->pdo, "getAttribute"], func_get_args());
    }

    public function inTransaction() {
        return call_user_func_array([$this->pdo, "inTransaction"], func_get_args());
    }

    public function lastInsertId($_ = null) {
        return call_user_func_array([$this->pdo, "lastInsertId"], func_get_args());
    }

    public function prepare($_ = null, $__ = null) {
        return call_user_func_array([$this->pdo, "prepare"], func_get_args());
    }

    public function query($_ = null, $__ = null, $___ = null) {
        return call_user_func_array([$this->pdo, "query"], func_get_args());
    }

    public function quote($_ = null, $__ = null) {
        return call_user_func_array([$this->pdo, "quote"], func_get_args());
    }

    public function rollBack() {
        return call_user_func_array([$this->pdo, "rollBack"], func_get_args());
    }

    public function setAttribute($_ = null, $__ = null) {
        return call_user_func_array([$this->pdo, "setAttribute"], func_get_args());
    }
}
