<?php

namespace BlakePrinting\Db;

use BlakePrinting\AmazonAws\AwsConfigUtil;
use BlakePrinting\Util\ConfigUtil;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use Exception;
use PDO;
use phpDocumentor\Reflection\DocBlock;
use phpDocumentor\Reflection\DocBlock\Tag\ReturnTag;
use ReflectionMethod;

final class PDOUtil {
    private static $connections = [];
    private static $prePauseConfig = null;

    private function __construct() { }

    public static function pauseSqlLogging() {
        if(self::$prePauseConfig === null) {
            $config =& ConfigUtil::getConfig();
            self::$prePauseConfig = $config->retrieve("log", "sql");

            foreach(["select", "insert", "update", "delete"] as $stmtType) {
                $config->store("log", "sql", $stmtType, "bind", false);
                $config->store("log", "sql", $stmtType, "statement", false);
                $config->store("log", "sql", $stmtType, "result", false);
            }
        }
    }

    public static function resumeSqlLogging() {
        if(self::$prePauseConfig !== null) {
            $config =& ConfigUtil::getConfig();

            $config->store("log", "sql", self::$prePauseConfig);

            self::$prePauseConfig = null;
        }
    }

    /**
     * @return int
     */
    public static function getCurrentTimestamp(PDO $conn) {
        $stmt = $conn->query("SELECT UNIX_TIMESTAMP()");

        if($stmt !== false) {
            return (int)$stmt->fetchColumn();
        }

        return new Exception("Couldn't get current timestamp");
    }

    /**
     * @param string $typeName
     * @param int $defaultPdoType
     *
     * @return int
     */
    public static function mapTypeNameToPdoType($typeName, $defaultPdoType = null) {
        if($typeName !== null) {
            switch($typeName) {
                case "int":
                case "integer":
                    return PDO::PARAM_INT;
                case "string":
                    return PDO::PARAM_STR;
                case "bool":
                case "boolean":
                    return PDO::PARAM_BOOL;
                case "float":
                    return PDO::PARAM_STR;
            }
        }

        return $defaultPdoType;
    }

    /**
     * @param ReflectionMethod $reflectionMethod
     * @param string $defaultVarType
     * @return int
     */
    public static function getPdoTypeFromReflectionMethod(ReflectionMethod &$reflectionMethod, $defaultVarType = null) {
        $docComment = $reflectionMethod->getDocComment();
        $typeName = $defaultVarType;

        if(!empty($docComment)) {
            $docBlock = new DocBlock($docComment);
            $docVars = $docBlock->getTagsByName("return");

            if(!empty($docVars)) {
                $docVar = $docVars[0];

                if($docVar instanceof ReturnTag) {
                    $typeName = $docVar->getType();
                }
            }
        }

        return self::mapTypeNameToPdoType($typeName);
    }

    public static function commitConnection(PDO $conn) {
        if($conn->inTransaction()) {
            $conn->commit();
        }
    }

    public static function rollbackConnection(PDO $conn) {
        if($conn->inTransaction()) {
            $conn->rollBack();
        }
    }

    /**
     * @param string $dbName
     * @param string $dbSchema
     * @return PDO
     */
    public static function &getRootConnection($dbName = null, $dbSchema = "mysql") {
        $config =& ConfigUtil::getConfig();
        $database = ($dbName !== null ? $dbName : $config->retrieve("database", "default"));
        $rootUsername =& $config->retrieve("database", $database, "root_username");
        $rootPassword =& $config->retrieve("database", $database, "root_password");
        $connectionParams =& PDOUtil::getConnectionParams($database, $rootUsername, $rootPassword);
        $rootUsername =& $connectionParams["user"];
        $rootPassword =& $connectionParams["pass"];

        return PDOUtil::getMysqlConnection($database, $dbSchema, $rootUsername, $rootPassword);
    }

    /**
     * @param string $dbName
     * @param string $dbSchema
     * @param string $dbUser
     * @param string $dbPass
     * @param string $dbCharset
     * @param string $dbCollation
     * @param string $dbTimeZone
     *
     * @return PDO
     */
    public static function &getMysqlConnection($dbName = null, $dbSchema = null, $dbUser = null, $dbPass = null, $dbCharset = null, $dbCollation = null, $dbTimeZone = null) {
        $config =& ConfigUtil::getConfig();

        $dbName === null && $dbName =& $config->retrieve("database", "default");
        $dbSchema === null && $dbSchema =& $config->retrieve("database", $dbName, "schemas", "default");
        $dbUser === null && $dbUser =& $config->retrieve("database", $dbName, "username");
        $dbPass === null && $dbPass =& $config->retrieve("database", $dbName, "password");
        $dbCharset === null && $dbCharset =& $config->retrieve("database", $dbName, "charset");
        $dbCollation === null && $dbCollation =& $config->retrieve("database", $dbName, "collation");
        $dbTimeZone === null && $dbTimeZone =& $config->retrieve("database", $dbName, "timezone");

        $dbSchemaName =& $config->retrieve("database", $dbName, "schemas", $dbSchema, "name");

        if($dbSchemaName === null) {
            $dbSchemaName =& $dbSchema;
        }

        if(!isset(self::$connections[$dbName])) {
            self::$connections[$dbName] = [];
        }

        $dbConnections =& self::$connections[$dbName];

        if(!array_key_exists($dbSchemaName, $dbConnections)) {
            $dbConnections[$dbSchemaName] = [];
        }

        $schemaConnections =& $dbConnections[$dbSchemaName];

        if(!array_key_exists($dbUser, $schemaConnections)) {
            $schemaConnections[$dbUser] = [];
        }

        $userConnections =& $schemaConnections[$dbUser];

        if(!array_key_exists($dbCharset, $userConnections)) {
            $userConnections[$dbCharset] = [];
        }

        $charsetConnections =& $userConnections[$dbCharset];
        $connection = null;

        if(!array_key_exists($dbCollation, $charsetConnections)) {
            $connection =& self::_createMysqlConnection($dbName, $dbSchemaName, $dbUser, $dbPass, $dbCharset, $dbCollation, $dbTimeZone);

            $charsetConnections[$dbCollation] =& $connection;
        }

        return $charsetConnections[$dbCollation];
    }

    public static function &getAllConnections() {
        $connectionIds = [];
        $allConnections = [];

        foreach(self::$connections as $database => &$schemas) {
            foreach($schemas as $schema => &$users) {
                foreach($users as $user => &$charsets) {
                    foreach($charsets as $charset => &$collations) {
                        foreach($collations as $collation => &$connection) {
                            if($connection !== null) {
                                $id = self::getUniqueIdentifierForConnection($connection);

                                if(!array_key_exists($id, $connection)) {
                                    $connectionIds[$id] = true;
                                    $allConnections[] =& $connection;
                                }
                            }
                        }

                        unset($connection);
                    }

                    unset($collations);
                }

                unset($charsets);
            }

            unset($users);
        }

        unset($schemas);

        return $allConnections;
    }

    public static function getDatabaseNameByConnection(PDO $conn) {
        $logger =& LogUtil::getLogger();
        $findId = self::getUniqueIdentifierForConnection($conn);

        foreach(self::$connections as $database => &$schemas) {
            foreach($schemas as $schema => &$users) {
                foreach($users as $user => &$charsets) {
                    foreach($charsets as $charset => &$collations) {
                        foreach($collations as $collation => &$connection) {
                            if($connection === null) {
                                $logger->warn(
                                    "Found a null connection for db: " . $database . ", schema: " . $schema . ", " .
                                    "user: " . $user . ", charset: " . $charset . ", collation: " . $collation
                                );
                            } else {
                                $id = self::getUniqueIdentifierForConnection($connection);
                                $cmp = strcmp($findId, $id);

                                if($conn === $connection || $cmp === 0) {
                                    return $database;
                                }
                            }
                        }

                        unset($connection);
                    }

                    unset($collations);
                }

                unset($charsets);
            }

            unset($users);
        }

        unset($schemas);

        return null;
    }

    public static function getUniqueIdentifierForConnection(PDO $connection) {
        if($connection instanceof PDODelegate) {
            return $connection->getUniqId();
        } else {
            return spl_object_hash($connection);
        }
    }

    /**
     * @param string $dbName
     * @param string $dbSchemaName
     * @param string $dbUser
     * @param string $dbPass
     * @param string $dbCharset
     * @param string $dbCollation
     * @param string $dbTimeZone
     *
     * @return PDO
     */
    private static function &_createMysqlConnection(&$dbName, &$dbSchemaName, &$dbUser, &$dbPass, &$dbCharset, &$dbCollation, &$dbTimeZone) {
        $conn =& self::_createConnection($dbName, $dbSchemaName, $dbUser, $dbPass, "mysql");

        $conn->exec(
            "SET NAMES " . $dbCharset . " " .
            "COLLATE " . $dbCollation . ", " .
            "COLLATION_CONNECTION = " . $dbCollation
        );

        if(!empty($dbTimeZone)) {
            $conn->exec("SET TIME_ZONE = " . $dbTimeZone);
        }

        $conn->exec("SET TRANSACTION ISOLATION LEVEL READ COMMITTED");

        return $conn;
    }

    /**
     * @param string $dbName
     * @param string $dbSchemaName
     * @param string $dbUser
     * @param string $dbPass
     * @param string $scheme
     *
     * @return PDO
     */
    private static function &_createConnection(&$dbName, &$dbSchemaName, &$dbUser, &$dbPass, $scheme) {
        $connectionParams =& self::getConnectionParams($dbName, $dbUser, $dbPass);
        $connectionString = $scheme . ":dbname=" . $dbSchemaName . ";host=" . $connectionParams["host"] . (empty($connectionParams["port"]) ? "" : (";port=" . $connectionParams["port"]));
        $logger =& LogUtil::getLogger();

        $logger->debug("Connecting to: " . $connectionString);

        $conn = new PDO($connectionString, $connectionParams["user"], $connectionParams["pass"]);
        $delegate = new PDODelegate($conn);

        $delegate->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $delegate;
    }

    /**
     * @param string $dbName
     * @param string $dbUser
     * @param string $dbPass
     *
     * @return array
     */
    public static function &getConnectionParams($dbName, $dbUser = null, $dbPass = null) {
        $config =& ConfigUtil::getConfig();
        $dbDocker =& $config->retrieve("database", $dbName, "docker");
        $connectionParams = [];

        if(!empty($dbDocker)) {
            $environmentValue = getenv(strtoupper($dbDocker) . "_PORT");
            $connectionParams = Utils::parseDockerPortVariable($environmentValue);

            $connectionParams["user"] =& $dbUser;
            $connectionParams["pass"] =& $dbPass;
        }

        if(empty($connectionParams["host"])) {
            $config =& ConfigUtil::getConfig();
            $dbHost =& $config->retrieve("database", $dbName, "host");
            $dbPort =& $config->retrieve("database", $dbName, "port");

            if(!empty($dbHost)) {
                $connectionParams["host"] =& $dbHost;
            }

            if(!empty($dbPort)) {
                $connectionParams["port"] =& $dbPort;
            }
        }

        if(empty($connectionParams["host"])) {
            $config =& ConfigUtil::getConfig();
            $defaultDbName =& $config->retrieve("database", "default");

            if($dbName == $defaultDbName) {
                $storedRdsConfig =& AwsConfigUtil::getStoredRdsConfig(ConfigUtil::getEnvironmentName());

                if($storedRdsConfig !== null) {
                    $connectionParams["host"] = $storedRdsConfig->getEndpoint();
                    $connectionParams["port"] = $storedRdsConfig->getPort();
                    $connectionParams["user"] = $storedRdsConfig->getUsername();
                    $connectionParams["pass"] = $storedRdsConfig->getPassword();
                }
            }
        }

        if(empty($connectionParams["user"]) && !empty($dbUser)) {
            $connectionParams["user"] = $dbUser;
        }

        if(empty($connectionParams["pass"]) && !empty($dbPass)) {
            $connectionParams["pass"] = $dbPass;
        }

        return $connectionParams;
    }

    /**
     * @return int
     */
    public static function logMysqlWarnings(PDO $conn) {
        if($conn->getAttribute(PDO::ATTR_DRIVER_NAME) != "mysql") {
            return 0;
        }

        $warningCountResult = $conn->query("SELECT @@warning_count");
        $warningCountRow = $warningCountResult->fetch();
        $warningCountTotal = $warningCountRow[0];
        $warningCount = $warningCountTotal;

        if($warningCount > 0) {
            $logger =& LogUtil::getLogger();
            $warnings = [];

            foreach($conn->query("SHOW WARNINGS") as $warning) {
                if($warningCount == $warningCountTotal) {
                    $logger->debug("MySQL warning count: " . $warningCount);
                }

                $warningCount--;

                $warning =
                    "MySQL warning - level: " . $warning["Level"] .
                    ", code: " . $warning["Code"] .
                    ", message: " . $warning["Message"]
                ;

                $logger->warn($warning);

                $warnings[] = $warning;
            }

            if(!empty($warnings)) {
                throw new Exception(implode("\n", $warnings));
            }
        }

        return $warningCountTotal;
    }
}
