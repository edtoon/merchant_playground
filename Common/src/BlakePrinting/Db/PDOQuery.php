<?php

namespace BlakePrinting\Db;

use BlakePrinting\Util\ConfigUtil;
use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use Stringy\StaticStringy as S;
use Exception;
use PDO;
use PDOStatement;

class PDOQuery {
    const CONF_LOG_BIND = "bind";
    const CONF_LOG_CATEGORY = "sql";
    const CONF_LOG_RESULT = "result";
    const CONF_LOG_STATEMENT = "statement";
    const DATA_ATTR_ALIAS = "alias";
    const DATA_ATTR_NO_REBIND = "no_rebind";
    const DATA_ATTR_FUNC = "func";
    const DATA_ATTR_LOG = "log";
    const DATA_ATTR_OPERATOR = "operator";
    const DATA_ATTR_SPEC = "spec";
    const DATA_ATTR_TYPE = "type";
    const DATA_ATTR_VALUE = "value";
    const JOIN_ATTR_TABLE = "table";
    const JOIN_ATTR_ON = "on";
    const QUERY_ENCLOSE_BODY = 0b00000001;
    const QUERY_ENCLOSE_ORDER_BY = 0b00000010;
    const QUERY_ENCLOSE_LIMIT = 0b00000100;
    const QUERY_ENCLOSE_OFFSET = 0b00001000;
    const QUERY_ENCLOSE_ALL = (self::QUERY_ENCLOSE_BODY | self::QUERY_ENCLOSE_ORDER_BY | self::QUERY_ENCLOSE_LIMIT | self::QUERY_ENCLOSE_OFFSET);
    const QUERY_TYPE_SELECT = "SELECT";
    const QUERY_TYPE_INSERT = "INSERT";
    const QUERY_TYPE_UPDATE = "UPDATE";
    const QUERY_TYPE_DELETE = "DELETE";

    private $type = null;
    private $lowerType = null;
    private $requireConditions = false;
    private $includeLastInsertId = false;
    private $lastInsertId = null;
    private $rowCount = 0;
    private $calculatedRowCount = 0;
    private $enclose = 0;
    private $distinct = false;
    private $calculateFoundRows = false;
    private $ignore = false;
    protected $columns = [];
    private $tables = [];
    private $join = [];
    private $leftJoin = [];
    /** @var PDOQuery[] */
    private $union = [];
    /** @var PDOQuery[] */
    private $unionAll = [];
    protected $values = [];
    private $onDuplicateValues = [];
    private $groupBy = [];
    private $orderBy = [];
    private $limit = [];
    private $offset = null;
    private $options = [];

    /**
     * @param string $type
     */
    private function __construct($type) {
        $this->type = $type;
        $this->lowerType = strtolower($type);
    }

    /**
     * @return int
     */
    public function getLastInsertId() {
        return $this->lastInsertId;
    }

    /**
     * @return int
     */
    public function getRowCount() {
        return $this->rowCount;
    }

    /**
     * @return int
     */
    public function getCalculatedRowCount() {
        return $this->calculatedRowCount;
    }

    /**
     * @return bool
     */
    public function getIncludeLastInsertId() {
        return $this->includeLastInsertId;
    }

    /**
     * @return self
     */
    public static function &select() {
        $query = new self(self::QUERY_TYPE_SELECT);

        return $query;
    }

    /**
     * @return self
     */
    public static function &insert() {
        $insert = new self(self::QUERY_TYPE_INSERT);

        return $insert;
    }

    /**
     * @return self
     */
    public static function &update() {
        $update = new self(self::QUERY_TYPE_UPDATE);

        return $update;
    }

    /**
     * @return self
     */
    public static function &delete() {
        $delete = new self(self::QUERY_TYPE_DELETE);

        return $delete;
    }

    /**
     * @param int $enclose
     *
     * @return $this
     */
    public function &enclose($enclose = self::QUERY_ENCLOSE_ALL) {
        $this->validate_operation(self::QUERY_TYPE_SELECT);

        $this->enclose = $enclose;

        return $this;
    }

    /**
     * @param bool $requireConditions
     *
     * @return $this
     */
    public function &requireConditions($requireConditions = true) {
        $this->validate_operation(self::QUERY_TYPE_SELECT, self::QUERY_TYPE_UPDATE, self::QUERY_TYPE_DELETE);

        $this->requireConditions = $requireConditions;

        return $this;
    }

    /**
     * @param bool $distinct
     *
     * @return $this
     */
    public function &distinct($distinct = true) {
        $this->validate_operation(self::QUERY_TYPE_SELECT);

        $this->distinct = $distinct;

        return $this;
    }

    /**
     * @param bool $calculateFoundRows
     *
     * @return $this
     */
    public function &calculateFoundRows($calculateFoundRows = true) {
        $this->validate_operation(self::QUERY_TYPE_SELECT);

        $this->calculateFoundRows = $calculateFoundRows;

        return $this;
    }

    /**
     * @param bool $ignore
     *
     * @return $this
     */
    public function &ignore($ignore = true) {
        $this->validate_operation(self::QUERY_TYPE_INSERT);

        $this->ignore = $ignore;

        return $this;
    }

    /**
     * @param string|string[] $column
     *
     * @return $this
     */
    public function &column() {
        $this->validate_operation(self::QUERY_TYPE_SELECT);

        $column = func_get_args();

        self::mapSpecsAndValuesToTarget(self::DATA_ATTR_ALIAS, $column, $this->columns);

        return $this;
    }

    /**
     * @param string|string[] $columns
     *
     * @return $this
     */
    public function &columns() {
        $this->validate_operation(self::QUERY_TYPE_SELECT);

        $columns = func_get_args();

        self::mapSpecsAndValuesToTarget(self::DATA_ATTR_ALIAS, $columns, $this->columns);

        return $this;
    }

    /**
     * @param (string|DataAccessObject)|(string|DataAccessObject)[] $table
     *
     * @return $this
     */
    public function &table() {
        $this->validate_operation(self::QUERY_TYPE_UPDATE);

        $tables = func_get_args();

        self::mapSpecsAndValuesToTarget(self::DATA_ATTR_ALIAS, $tables, $this->tables);

        return $this;
    }

    /**
     * @param (string|DataAccessObject)|(string|DataAccessObject)[] $tables
     *
     * @return $this
     */
    public function &tables() {
        $this->validate_operation(self::QUERY_TYPE_UPDATE);

        $tables = func_get_args();

        self::mapSpecsAndValuesToTarget(self::DATA_ATTR_ALIAS, $tables, $this->tables);

        return $this;
    }

    /**
     * @param (string|DataAccessObject)|(string|DataAccessObject)[] $tables
     *
     * @return $this
     */
    public function &from() {
        $this->validate_operation(self::QUERY_TYPE_SELECT, self::QUERY_TYPE_DELETE);

        $tables = func_get_args();

        self::mapSpecsAndValuesToTarget(self::DATA_ATTR_ALIAS, $tables, $this->tables);

        return $this;
    }

    /**
     * @param (string|DataAccessObject)|(string|DataAccessObject)[] $tables
     *
     * @return $this
     */
    public function &into() {
        $this->validate_operation(self::QUERY_TYPE_INSERT);

        $tables = func_get_args();

        self::mapSpecsAndValuesToTarget(self::DATA_ATTR_ALIAS, $tables, $this->tables);

        return $this;
    }

    /**
     * @param string|string[] $table
     * @param string|string[] $on
     *
     * @return $this
     */
    public function &join($table, $on = null) {
        $this->validate_operation(self::QUERY_TYPE_SELECT);

        if(!empty($on)) {
            $join = [];

            if(is_array($table)) {
                $join[self::JOIN_ATTR_TABLE] =& $table;
            } else if($table instanceof DataAccessObject) {
                $join[self::JOIN_ATTR_TABLE] = [self::DATA_ATTR_SPEC => $table->formatSchemaAndTableName()];
            } else {
                $join[self::JOIN_ATTR_TABLE] = [self::DATA_ATTR_SPEC => $table];
            }

            if(is_array($on)) {
                $join[self::JOIN_ATTR_ON] =& $on;
            } else {
                $join[self::JOIN_ATTR_ON] = [$on];
            }

            $this->join[] = $join;
        }

        return $this;
    }

    /**
     * @param string|string[] $table
     * @param string|string[] $on
     *
     * @return $this
     */
    public function &leftJoin($table, $on = null) {
        $this->validate_operation(self::QUERY_TYPE_SELECT);

        if(!empty($on)) {
            $leftJoin = [];

            if(is_array($table)) {
                $leftJoin[self::JOIN_ATTR_TABLE] =& $table;
            } else if($table instanceof DataAccessObject) {
                $leftJoin[self::JOIN_ATTR_TABLE] = [self::DATA_ATTR_SPEC => $table->formatSchemaAndTableName()];
            } else {
                $leftJoin[self::JOIN_ATTR_TABLE] = [self::DATA_ATTR_SPEC => $table];
            }

            if(is_array($on)) {
                $leftJoin[self::JOIN_ATTR_ON] =& $on;
            } else {
                $leftJoin[self::JOIN_ATTR_ON] = [$on];
            }

            $this->leftJoin[] = $leftJoin;
        }

        return $this;
    }

    /**
     * @param PDOQuery|PDOQuery[] $union
     * @return $this
     */
    public function &union($union) {
        $this->validate_operation(self::QUERY_TYPE_SELECT);

        if(!is_array($union)) {
            $this->union[] = $union;
        } else {
            foreach($union as &$individualUnion) {
                $this->union[] = $individualUnion;
            }

            unset($individualUnion);
        }

        return $this;
    }

    /**
     * @param PDOQuery|PDOQuery[] $unionAll
     * @return $this
     */
    public function &unionAll($unionAll) {
        $this->validate_operation(self::QUERY_TYPE_SELECT);

        if(!is_array($unionAll)) {
            $this->unionAll[] = $unionAll;
        } else {
            foreach($unionAll as &$individualUnionAll) {
                $this->unionAll[] = $individualUnionAll;
            }

            unset($individualUnionAll);
        }

        return $this;
    }

    /**
     * @param string|string[] $column
     * @param string $value
     * @param string $type
     * @param string $func
     * @param bool $log
     *
     * @return $this
     */
    public function &setIfNotNull($column, $value = null, $type = null, $func = null, $log = true) {
        if($value !== null) {
            $this->set($column, $value, $type, $func, $log);
        }

        return $this;
    }

    /**
     * @param string|string[] $column
     * @param string $value
     * @param string $type
     * @param string $func
     * @param bool $log
     *
     * @return $this
     */
    public function &set($column, $value = null, $type = null, $func = null, $log = true) {
        $this->validate_operation(self::QUERY_TYPE_UPDATE);

        if(is_array($column)) {
            self::mapSpecsAndValuesToTarget(self::DATA_ATTR_VALUE, $column, $this->columns);
        } else {
            $this->columns[] = [
                self::DATA_ATTR_SPEC => $column,
                self::DATA_ATTR_VALUE => $value,
                self::DATA_ATTR_TYPE => $type,
                self::DATA_ATTR_FUNC => $func,
                self::DATA_ATTR_LOG => $log,
            ];
        }

        return $this;
    }

    /**
     * @param string|string[] $column
     * @param string $value
     * @param string $type
     * @param string $operator
     * @param string $func
     * @param bool $log
     *
     * @return $this
     */
    public function &where($column, $value = null, $type = null, $operator = "=", $func = null, $log = true) {
        $this->validate_operation(self::QUERY_TYPE_SELECT, self::QUERY_TYPE_UPDATE, self::QUERY_TYPE_DELETE);

        if(is_array($column)) {
            self::mapSpecsAndValuesToTarget(self::DATA_ATTR_VALUE, $column, $this->values);
        } else {
            $this->values[] = [
                self::DATA_ATTR_SPEC => $column,
                self::DATA_ATTR_VALUE => $value,
                self::DATA_ATTR_TYPE => $type,
                self::DATA_ATTR_OPERATOR => $operator,
                self::DATA_ATTR_FUNC => $func,
                self::DATA_ATTR_LOG => $log
            ];
        }

        return $this;
    }

    /**
     * @param string|string[] $column
     * @param string $value
     * @param string $type
     * @param string $func
     * @param bool $log
     *
     * @return $this
     */
    public function &whereValueOrNull($column, $value = null, $type = null, $func = null, $log = true) {
        $this->validate_operation(self::QUERY_TYPE_SELECT, self::QUERY_TYPE_UPDATE, self::QUERY_TYPE_DELETE);

        if($value !== null) {
            $this->where($column, $value, $type, "=", $func, $log);
        } else {
            $this->where($column, "NULL", null, "IS", $func, $log);
        }

        return $this;
    }

    /**
     * @param string|string[] $column
     * @param string $func
     *
     * @return $this
     */
    public function &whereNull($column, $func = null) {
        $this->validate_operation(self::QUERY_TYPE_SELECT, self::QUERY_TYPE_UPDATE, self::QUERY_TYPE_DELETE);

        $this->where($column, "NULL", null, "IS", $func);

        return $this;
    }

    /**
     * @param string|string[] $column
     * @param string $func
     *
     * @return $this
     */
    public function &whereNotNull($column, $func = null) {
        $this->validate_operation(self::QUERY_TYPE_SELECT, self::QUERY_TYPE_UPDATE, self::QUERY_TYPE_DELETE);

        $this->where($column, "NULL", null, "IS NOT", $func);

        return $this;
    }

    /**
     * @param string|string[] $column
     * @param string $value
     * @param string $type
     * @param string $func
     * @param bool $log
     *
     * @return $this
     */
    public function &value($column, $value = null, $type = null, $func = null, $log = true) {
        $this->validate_operation(self::QUERY_TYPE_INSERT);

        if(is_array($column)) {
            self::mapSpecsAndValuesToTarget(self::DATA_ATTR_VALUE, $column, $this->values);
        } else {
            $this->values[] = [
                self::DATA_ATTR_SPEC => $column,
                self::DATA_ATTR_VALUE => $value,
                self::DATA_ATTR_TYPE => $type,
                self::DATA_ATTR_FUNC => $func,
                self::DATA_ATTR_LOG => $log
            ];
        }

        return $this;
    }

    /**
     * @param string|string[] $column
     * @param string $value
     * @param string $type
     * @param string $func
     * @param bool $log
     *
     * @return $this
     */
    public function &onDuplicateValue($column, $value = null, $type = null, $func = null, $log = true) {
        $this->validate_operation(self::QUERY_TYPE_INSERT);

        if(is_array($column)) {
            self::mapSpecsAndValuesToTarget(self::DATA_ATTR_VALUE, $column, $this->onDuplicateValues);
        } else {
            $this->onDuplicateValues[] = [
                self::DATA_ATTR_SPEC => $column,
                self::DATA_ATTR_VALUE => $value,
                self::DATA_ATTR_TYPE => $type,
                self::DATA_ATTR_FUNC => $func,
                self::DATA_ATTR_LOG => $log
            ];
        }

        return $this;
    }

    /**
     * @param bool $log
     *
     * @return $this
     */
    public function &onDuplicateAllValues($log = true) {
        foreach($this->values as &$valueData) {
            $spec =& $valueData[self::DATA_ATTR_SPEC];

            $this->onDuplicateValue($spec, "VALUES(" . $spec . ")", null, null, $log);
        }

        unset($valueData);

        return $this;
    }

    /**
     * @param string|string[] $group
     *
     * @return $this
     */
    public function &groupBy() {
        $this->validate_operation(self::QUERY_TYPE_SELECT);

        $group = func_get_args();

        self::mapSpecsAndValuesToTarget(self::DATA_ATTR_ALIAS, $group, $this->groupBy);

        return $this;
    }

    /**
     * @param string|string[] $orderBy
     *
     * @return $this
     */
    public function &orderBy($orderBy) {
        $this->validate_operation(self::QUERY_TYPE_SELECT, self::QUERY_TYPE_UPDATE, self::QUERY_TYPE_DELETE);

        if(is_array($orderBy)) {
            $this->orderBy = array_merge($this->orderBy, $orderBy);
        } else {
            $this->orderBy[] = $orderBy;
        }

        return $this;
    }

    /**
     * @param string|string[]|int|int[] $limit
     *
     * @return $this
     */
    public function &limit($limit) {
        $this->validate_operation(self::QUERY_TYPE_SELECT, self::QUERY_TYPE_UPDATE, self::QUERY_TYPE_DELETE);

        if(func_num_args() > 1) {
            $limit = func_get_args();
        }

        if(is_array($limit)) {
            $this->limit = array_merge($this->limit, $limit);
        } else {
            $this->limit[] = $limit;
        }

        return $this;
    }

    /**
     * @param string|int $offset
     *
     * @return $this
     */
    public function &offset($offset) {
        $this->validate_operation(self::QUERY_TYPE_SELECT);

        $this->offset = $offset;

        return $this;
    }

    /**
     * @param string|string[] $options
     *
     * @return $this
     */
    public function &options($options = null) {
        $this->validate_operation(self::QUERY_TYPE_SELECT, self::QUERY_TYPE_INSERT);

        if(!empty($options)) {
            if(is_array($options)) {
                $this->options = array_merge($this->options, $options);
            } else {
                $this->options[] = $options;
            }
        }

        return $this;
    }

    /**
     * @param string|string[] $orderBy
     * @param string|string[]|int|int[] $limit
     * @param string|string[] $options
     *
     * @return $this
     */
    public function &addOrderByLimitAndOptions(&$orderBy, &$limit, &$options) {
        if($orderBy !== null) {
            $this->orderBy($orderBy);
        }

        if($limit !== null) {
            $this->limit($limit);
        }

        if($options !== null) {
            $this->options($options);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function withLastInsertId() {
        $this->includeLastInsertId = true;

        return $this;
    }

    /**
     * @return string
     */
    public function &generate() {
        $columnsNotEmpty = Utils::isNonEmptyArray($this->columns);
        $tablesNotEmpty = Utils::isNonEmptyArray($this->tables);
        $joinNotEmpty = Utils::isNonEmptyArray($this->join);
        $leftJoinNotEmpty = Utils::isNonEmptyArray($this->leftJoin);
        $unionNotEmpty = Utils::isNonEmptyArray($this->union);
        $unionAllNotEmpty = Utils::isNonEmptyArray($this->unionAll);
        $groupByNotEmpty = Utils::isNonEmptyArray($this->groupBy);
        $valuesNotEmpty = Utils::isNonEmptyArray($this->values);
        $optionsNotEmpty = Utils::isNonEmptyArray($this->options);
        $sql = "";

        if($this->requireConditions && !$valuesNotEmpty) {
            throw new Exception("Query required conditions but none were provided");
        }

        if($this->enclose != 0) {
            $sql .= "(";
        }

        $sql .= $this->type;

        if($this->type == self::QUERY_TYPE_SELECT) {
            if($this->distinct === true) {
                $sql .= " DISTINCT";
            }

            if($this->calculateFoundRows === true) {
                $sql .= " SQL_CALC_FOUND_ROWS";
            }

            $specsAndAliasesClause = self::reduceSpecsAndAliasesToClause($this->columns);
            $sql .= " ";

            if($specsAndAliasesClause !== null && is_string($specsAndAliasesClause) && strlen($specsAndAliasesClause) > 0) {
                $sql .= $specsAndAliasesClause;
            } else {
                $sql .= "*";
            }
        }

        $tablesClause = null;

        if($tablesNotEmpty) {
            $tablesClause = self::reduceSpecsAndAliasesToClause($this->tables);

            if(!empty($tablesClause)) {
                switch($this->type) {
                    case self::QUERY_TYPE_SELECT:
                        if(strpos($tablesClause, "dual") === false) {
                            $tablesClause = ("(" . $tablesClause . ")");
                        }

                        $sql .= " FROM";
                    break;
                    case self::QUERY_TYPE_DELETE:
                        $sql .= " FROM";
                    break;
                    case self::QUERY_TYPE_INSERT:
                        if($this->ignore === true) {
                            $sql .= " IGNORE";
                        }

                        $sql .= " INTO";
                    break;
                }

                $sql .= " " . $tablesClause;
            }
        }

        if($this->type == self::QUERY_TYPE_INSERT && $valuesNotEmpty) {
            $firstItem = true;

            foreach($this->values as &$valueData) {
                if($firstItem) {
                    $firstItem = false;

                    $sql .= " (";
                } else {
                    $sql .= ", ";
                }

                $sql .= $valueData[self::DATA_ATTR_SPEC];
            }

            unset($valueData);

            if(!$firstItem) {
                $sql .= ")";
            }
        }

        if($joinNotEmpty) {
            foreach($this->join as &$join) {
                $table =& $join[self::JOIN_ATTR_TABLE];
                $sql .= " JOIN " . $table[self::DATA_ATTR_SPEC] . ((!array_key_exists(self::DATA_ATTR_ALIAS, $table)) ? "" : (" AS " . $table[self::DATA_ATTR_ALIAS]));
                $firstItem = true;

                foreach($join[self::JOIN_ATTR_ON] as &$on) {
                    $sql .= ($firstItem ? " ON " : " AND ") . $on;

                    $firstItem = false;
                }

                unset($on);
            }

            unset($join);
        }

        if($leftJoinNotEmpty) {
            foreach($this->leftJoin as &$leftJoin) {
                $table =& $leftJoin[self::JOIN_ATTR_TABLE];
                $sql .= " LEFT JOIN " . $table[self::DATA_ATTR_SPEC] . ((!array_key_exists(self::DATA_ATTR_ALIAS, $table)) ? "" : (" AS " . $table[self::DATA_ATTR_ALIAS]));
                $firstItem = true;

                foreach($leftJoin[self::JOIN_ATTR_ON] as &$on) {
                    $sql .= ($firstItem ? " ON " : " AND ") . $on;

                    $firstItem = false;
                }

                unset($on);
            }

            unset($leftJoin);
        }

        if($this->type == self::QUERY_TYPE_UPDATE && $columnsNotEmpty) {
            $firstItem = true;

            foreach($this->columns as &$columnData) {
                if($firstItem) {
                    $firstItem = false;

                    $sql .= " SET ";
                } else {
                    $sql .= ", ";
                }

                $valueStr = (isset($columnData[self::DATA_ATTR_TYPE]) ? "?" : $columnData[self::DATA_ATTR_VALUE]);
                $sql .= $columnData[self::DATA_ATTR_SPEC] . " = ";
                $sql .= (empty($columnData[self::DATA_ATTR_FUNC]) ? $valueStr : ($columnData[self::DATA_ATTR_FUNC] . "(" . $valueStr . ")"));
            }

            unset($columnData);
        }

        if($valuesNotEmpty) {
            $firstItem = true;

            foreach($this->values as &$valueData) {
                if(!isset($valueData[self::DATA_ATTR_SPEC])) {
                    continue;
                }

                if($firstItem) {
                    $firstItem = false;

                    if($this->type != self::QUERY_TYPE_INSERT) {
                        $sql .= " WHERE ";
                    } else {
                        $sql .= " VALUES (";
                    }
                } else {
                    if($this->type == self::QUERY_TYPE_INSERT) {
                        $sql .= ", ";
                    } else {
                        $sql .= " AND ";
                    }
                }

                if($this->type != self::QUERY_TYPE_INSERT) {
                    $sql .= $valueData[self::DATA_ATTR_SPEC];
                }

                $operator = isset($valueData[self::DATA_ATTR_OPERATOR]) ? $valueData[self::DATA_ATTR_OPERATOR] : null;
                $hasType = isset($valueData[self::DATA_ATTR_TYPE]);
                $hasFunc = (!empty($valueData[self::DATA_ATTR_FUNC]));
                $hasValue = array_key_exists(self::DATA_ATTR_VALUE, $valueData);
                $value = ($hasValue ? $valueData[self::DATA_ATTR_VALUE] : null);
                $valueStr = null;

                if($hasValue) {
                    if($valueStr === null) {
                        $valueStr = ($hasType ? "?" : $valueData[self::DATA_ATTR_VALUE]);
                    }

                    if(!empty($valueData[self::DATA_ATTR_FUNC])) {
                        $valueStr = ($valueData[self::DATA_ATTR_FUNC] . "(" . $valueStr . ")");
                    }
                }

                if(!empty($operator)) {
                    if($hasType && !$hasFunc && $hasValue && $value === null) {
                        switch($operator) {
                            case "=":
                                $operator = "IS";
                                $valueStr = "NULL";
                                $valueData[self::DATA_ATTR_NO_REBIND] = 1;
                                break;
                            case "!=":
                            case "<>":
                                $operator = "IS NOT";
                                $valueStr = "NULL";
                                $valueData[self::DATA_ATTR_NO_REBIND] = 1;
                                break;
                        }
                    }
                } else if($this->type !== self::QUERY_TYPE_INSERT) {
                    continue;
                }

                if(!empty((string)S::trim($valueStr))) {
                    if(!empty($operator)) {
                        $sql .= " " . $operator;
                    }

                    if($this->type !== self::QUERY_TYPE_INSERT) {
                        $sql .= " ";
                    }

                    $sql .= $valueStr;
                }
            }

            unset($valueData);

            if(!$firstItem && $this->type == self::QUERY_TYPE_INSERT) {
                $sql .= ")";
            }
        }

        if($this->enclose != 0) {
            if(($this->enclose & self::QUERY_ENCLOSE_ORDER_BY) && Utils::isNonEmptyArray($this->orderBy)) {
                $sql .= " ORDER BY " . implode(", ", $this->orderBy);
            }

            if(($this->enclose & self::QUERY_ENCLOSE_LIMIT) && Utils::isNonEmptyArray($this->limit)) {
                $sql .= " LIMIT " . implode(", ", $this->limit);
            }

            if(($this->enclose & self::QUERY_ENCLOSE_OFFSET) && $this->offset !== null) {
                $sql .= " OFFSET " . $this->offset;
            }

            $sql .= ")";
        }

        if($unionNotEmpty) {
            foreach($this->union as &$unionQuery) {
                $sql .= " UNION ";
                $sql .= $unionQuery->generate();
            }

            unset($unionQuery);
        }

        if($unionAllNotEmpty) {
            foreach($this->unionAll as &$unionAllQuery) {
                $sql .= " UNION ALL ";
                $sql .= $unionAllQuery->generate();
            }

            unset($unionAllQuery);
        }

        if($groupByNotEmpty) {
            $specsAndAliasesClause = self::reduceSpecsAndAliasesToClause($this->groupBy);

            if($specsAndAliasesClause !== null && is_string($specsAndAliasesClause) && strlen($specsAndAliasesClause) > 0) {
                $sql .= " GROUP BY " . $specsAndAliasesClause;
            }
        }

        if(!($this->enclose & self::QUERY_ENCLOSE_ORDER_BY) && Utils::isNonEmptyArray($this->orderBy)) {
            $sql .= " ORDER BY " . implode(", ", $this->orderBy);
        }

        if(!($this->enclose & self::QUERY_ENCLOSE_LIMIT) && Utils::isNonEmptyArray($this->limit)) {
            $sql .= " LIMIT " . implode(", ", $this->limit);
        }

        if(!($this->enclose & self::QUERY_ENCLOSE_OFFSET) && $this->offset !== null) {
            $sql .= " OFFSET " . $this->offset;
        }

        if($this->type == self::QUERY_TYPE_INSERT && Utils::isNonEmptyArray($this->onDuplicateValues)) {
            $firstItem = true;

            foreach($this->onDuplicateValues as &$duplicateData) {
                if($firstItem) {
                    $firstItem = false;

                    $sql .= " ON DUPLICATE KEY UPDATE ";
                } else {
                    $sql .= ", ";
                }

                $valueStr = (isset($duplicateData[self::DATA_ATTR_TYPE]) ? "?" : $duplicateData[self::DATA_ATTR_VALUE]);
                $sql .= $duplicateData[self::DATA_ATTR_SPEC] . " = ";
                $sql .= (empty($duplicateData[self::DATA_ATTR_FUNC]) ? $valueStr : ($duplicateData[self::DATA_ATTR_FUNC] . "(" . $valueStr . ")"));
            }

            unset($duplicateData);
        }

        if($optionsNotEmpty) {
            $sql .= " " . implode(" ", $this->options);
        }

        $config =& ConfigUtil::getConfig();

        if($config->retrieve(self::DATA_ATTR_LOG, self::CONF_LOG_CATEGORY, $this->lowerType, self::CONF_LOG_STATEMENT) === true) {
            $logger =& LogUtil::getLogger();

            $logger->debug("Generated " . $this->lowerType . ": " . $sql);
        }

        return $sql;
    }

    /**
     * @return PDOStatement
     */
    public function &execute(PDO $conn = null) {
        if($conn === null) {
            $conn = PDOUtil::getMysqlConnection();
        }

        if(!$conn->inTransaction() && (
            in_array($this->type, [self::QUERY_TYPE_INSERT, self::QUERY_TYPE_UPDATE, self::QUERY_TYPE_DELETE]) ||
            in_array("FOR UPDATE", $this->options)
        )) {
            $conn->beginTransaction();
        }

        $sql = $this->generate();
        $stmt = $conn->prepare($sql);
        $bindCount = 0;

        $this->bindStatementValues($stmt, $this->columns, $bindCount);

        foreach($this->union as &$unionQuery) {
            $unionQuery->bindStatementValues($stmt, $unionQuery->columns, $bindCount);
        }

        unset($unionQuery);

        foreach($this->unionAll as &$unionAllQuery) {
            $unionAllQuery->bindStatementValues($stmt, $unionAllQuery->columns, $bindCount);
        }

        unset($unionAllQuery);

        $this->bindStatementValues($stmt, $this->values, $bindCount);

        foreach($this->union as &$unionQuery) {
            $unionQuery->bindStatementValues($stmt, $unionQuery->values, $bindCount);
        }

        unset($unionQuery);

        foreach($this->unionAll as &$unionAllQuery) {
            $unionAllQuery->bindStatementValues($stmt, $unionAllQuery->values, $bindCount);
        }

        unset($unionAllQuery);

        $stmt->execute();

        if($this->calculateFoundRows) {
            $calcStmt = $conn->prepare("SELECT FOUND_ROWS()");

            $calcStmt->execute();

            $calcResult = $calcStmt->fetch(PDO::FETCH_NUM);
            $calcStmt->closeCursor();

            $this->calculatedRowCount = $calcResult[0];
        }

        $this->rowCount = $stmt->rowCount();

        $config =& ConfigUtil::getConfig();

        if($config->retrieve(self::DATA_ATTR_LOG, self::CONF_LOG_CATEGORY, $this->lowerType, self::CONF_LOG_RESULT) === true) {
            $logger =& LogUtil::getLogger();

            $logger->debug(
                $this->type . " " . ($this->type == self::QUERY_TYPE_SELECT ? "returned" : "affected") . " " .
                $this->rowCount . " row" . ($this->rowCount == 1 ? "" : "s") . "."
            );
        }

        if($this->includeLastInsertId === true) {
            $this->lastInsertId = $conn->lastInsertId();
        }

        PDOUtil::logMysqlWarnings($conn);

        return $stmt;
    }

    /**
     * @return int
     */
    public function &executeGetLastInsertId(PDO $conn = null) {
        $this->includeLastInsertId = true;

        self::execute($conn);

        return $this->lastInsertId;
    }

    /**
     * @return int
     */
    public function &executeEnsuringLastInsertId(PDO $conn = null) {
        $this->includeLastInsertId = true;

        self::execute($conn);

        if($this->lastInsertId <= 0) {
            throw new Exception(
                "No last insert id returned from " . $this->lowerType . " on " .
                "tables: " . Utils::getVarDump($this->tables) . ", row count: " . $this->rowCount . ", values: " . Utils::getVarDump($this->values)
            );
        }

        return $this->lastInsertId;
    }

    /**
     * @return int
     */
    public function &executeGetRowCount(PDO $conn = null) {
        self::execute($conn);

        return $this->rowCount;
    }

    /**
     * @param int $ensureRowCount
     * @return int
     * @throws Exception
     */
    public function &executeEnsuringRowCount(PDO $conn = null, $ensureRowCount) {
        self::execute($conn);

        if($ensureRowCount != $this->rowCount) {
            throw new Exception(
                "Too " . ($this->rowCount > $ensureRowCount ? "many" : "few") . " results executing " . $this->lowerType . " on " .
                "tables: " . Utils::getVarDump($this->tables) . ", row count: " . $this->rowCount . ", values: " . Utils::getVarDump($this->values)
            );
        }

        return $this->rowCount;
    }

    /**
     * @return array
     */
    public function &getResults(PDO $conn = null) {
        $this->validate_operation(self::QUERY_TYPE_SELECT, self::QUERY_TYPE_INSERT);

        $stmt =& self::execute($conn);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt = null;

        return $results;
    }

    /**
     * @throws Exception
     */
    public function &getSingleRow(PDO $conn = null) {
        $results =& $this->getResults($conn);
        $resultCount = count($results);
        $result = null;

        if($resultCount === 1) {
            $result = $results[0];
        } else if($resultCount > 1) {
            throw new Exception(
                "Too many entries found for statement type: " . $this->type . ", tables: " . Utils::getVarDump($this->tables) . ", " .
                "result count: " . $resultCount . ", columns: " . Utils::getVarDump($this->columns) . ", " .
                "values: " . Utils::getVarDump($this->values) . ", results: " . Utils::getVarDump($results)
            );
        }

        return $result;
    }

    /**
     * @throws Exception
     */
    public function &getSingleValue(PDO $conn = null) {
        $result = $this->getSingleRow($conn);

        if($result !== null) {
            $columnCount = count($result);

            if($columnCount != 1) {
                throw new Exception("Too " . ($columnCount > 1 ? "many" : "few") . " columns found in result: " . Utils::getVarDump($result));
            }

            foreach($result as $key => &$value) {
                return $value;
            }

            unset($value);
        }

        return $result;
    }

    /**
     * @throws Exception
     */
    public function &getColumnValues(PDO $conn = null) {
        $results =& $this->getResults($conn);
        $rows = [];

        foreach($results as &$result) {
            $columnCount = count($result);

            if($columnCount != 1) {
                throw new Exception("Too " . ($columnCount > 1 ? "many" : "few") . " columns found in result: " . Utils::getVarDump($result));
            }

            foreach($result as $key => &$value) {
                $rows[] =& $value;
                break;
            }

            unset($value);
        }

        unset($result);

        return $rows;
    }

    /**
     * @throws Exception
     */
    private function validate_operation() {
        $validTypes = func_get_args();

        if(Utils::isNonEmptyArray($validTypes)) {
            if(in_array($this->type, $validTypes)) {
                return;
            }
        }

        throw new Exception(
            "Operation: " . Utils::getCallingFunctionName() . " " .
            "is not valid for statements of type: " . $this->type . ", " .
            "valid types are: " . implode(", ", $validTypes)
        );
    }

    /**
     * @return string
     */
    private static function &reduceSpecsAndAliasesToClause(&$specsAndAliases) {
        $sql = array_reduce($specsAndAliases, function($clause, $specAndAlias) {
            if(isset($specAndAlias[self::DATA_ATTR_SPEC])) {
                $spec =& $specAndAlias[self::DATA_ATTR_SPEC];

                if($spec !== null) {
                    $alias = (isset($specAndAlias[self::DATA_ATTR_ALIAS]) ? $specAndAlias[self::DATA_ATTR_ALIAS] : null);

                    if(!isset($clause) || strlen($clause) < 1) {
                        $clause = "";
                    } else {
                        $clause .= ", ";
                    }

                    $clause .= $spec;

                    if($alias !== null && strlen($alias) > 0) {
                        $clause .= " AS " . $alias;
                    }
                }
            }

            return $clause;
        });

        return $sql;
    }

    /**
     * @param string $mappingType
     * @param array $targetArray
     * @param int $depth
     */
    private static function mapSpecsAndValuesToTarget($mappingType, &$specsAndValues, array &$targetArray, $depth = 0) {
        if($specsAndValues === null) {
            return;
        }

        if(!is_array($specsAndValues)) {
            if($specsAndValues instanceof DataAccessObject) {
                $targetArray[] = [self::DATA_ATTR_SPEC => $specsAndValues->formatSchemaAndTableName()];
            } else if(!is_string($specsAndValues) || strlen($specsAndValues) > 0) {
                $targetArray[] = [self::DATA_ATTR_SPEC => $specsAndValues];
            }

            return;
        }

        $count = count($specsAndValues);

        if($count < 1) {
            return;
        }

        if(Utils::isAssociativeArray($specsAndValues)) {
            if($depth == 0 && count($specsAndValues) == 1) {
                foreach($specsAndValues as &$value) {
                    self::mapSpecsAndValuesToTarget($mappingType, $value, $targetArray, 1);
                }

                unset($value);
            } else {
                foreach($specsAndValues as &$value) {
                    if($value !== null) {
                        if(is_array($value)) {
                            self::mapSpecsAndValuesToTarget($mappingType, $value, $targetArray, ($depth + 1));
                        } else if(!is_string($value) || strlen($value) > 0) {
                            $targetArray[] = [self::DATA_ATTR_SPEC => $value];
                        }
                    }
                }

                unset($value);
            }
        } else if(array_key_exists(self::DATA_ATTR_SPEC, $specsAndValues)) {
            $targetArray[] = $specsAndValues;
        } else {
            foreach($specsAndValues as $key => &$value) {
                if(is_scalar($key) && is_scalar($value)) {
                    $targetArray[] = [self::DATA_ATTR_SPEC => $key, $mappingType => $value];
                } else if($value !== null) {
                    if(is_array($value)) {
                        self::mapSpecsAndValuesToTarget($mappingType, $value, $targetArray, ($depth + 1));
                    } else if(is_string($value)) {
                        if(!empty($value)) {
                            $targetArray[] = [self::DATA_ATTR_SPEC => $value];
                        }
                    } else {
                        $targetArray[] = [self::DATA_ATTR_SPEC => $value];
                    }
                }
            }

            unset($value);
        }
    }

    /**
     * @param int $bindCount
     * @param bool $isRebind
     */
    protected function bindStatementValues(PDOStatement &$stmt, array &$values, &$bindCount = 0, $isRebind = false) {
        if(empty($values) || !is_array($values)) {
            return;
        }

        foreach($values as &$valueData) {
            if(isset($valueData[self::DATA_ATTR_TYPE])) {
                $this->bindStatementValue($stmt, $valueData, $bindCount, $isRebind);
            }
        }

        unset($valueData);
    }

    /**
     * @param int $bindCount
     * @param bool $isRebind
     */
    private function bindStatementValue(PDOStatement &$stmt, array &$valueData, &$bindCount, $isRebind = false) {
        if($isRebind && isset($valueData[self::DATA_ATTR_NO_REBIND])) {
            return;
        }

        $config =& ConfigUtil::getConfig();
        $logBind = ($config->retrieve(self::DATA_ATTR_LOG, self::CONF_LOG_CATEGORY, $this->lowerType, self::CONF_LOG_BIND) === true);
        $bindType =& $valueData[self::DATA_ATTR_TYPE];
        $bindValue =& $valueData[self::DATA_ATTR_VALUE];

        $bindCount++;

        if($bindType == PDO::PARAM_BOOL) {
            $boolValue = Utils::getBooleanOrNull($bindValue);
            $intValue = (($boolValue === null) ? null : ($boolValue === true ? 1 : 0));

            if($logBind) {
                $logger =& LogUtil::getLogger();

                $logger->debug("Bind #" . $bindCount . ", type: " . $bindType . ", value: " . Utils::getVarDump($intValue));
            }

            $stmt->bindValue($bindCount, $intValue, PDO::PARAM_INT);
        } else {
            if($logBind && isset($valueData[self::DATA_ATTR_LOG]) && $valueData[self::DATA_ATTR_LOG] === true) {
                $logger =& LogUtil::getLogger();

                $logger->debug("Bind #" . $bindCount . ", type: " . $bindType . ", value: " . Utils::getVarDump($bindValue));
            }

            $stmt->bindValue($bindCount, $bindValue, $bindType);
        }
    }
}
