<?php

namespace BlakePrinting\Db;

use Exception;
use PDO;

interface DataAccessObject {
    /**
     * @return PDO
     */
    public function &getConnection();

    /**
     * @param PDO $connection
     */
    public function setConnection(PDO $connection);

    /**
     * @return string
     * @throws Exception
     */
    public function getSchemaName();

    /**
     * @param string $schemaName
     */
    public function setSchemaName($schemaName);

    /**
     * @return string
     * @throws Exception
     */
    public function getTableName();

    /**
     * @param string $tableName
     */
    public function setTableName($tableName);

    /**
     * @return string
     * @throws Exception
     */
    public function formatSchemaAndTableName();

    /**
     * @return PDOQuery
     */
    public function &getLastQuery();

    /**
     * @param mixed $id
     * @param string|string[] $options
     * @param bool $useCache
     * @return ValueObject|null
     * @throws Exception
     */
    public function &findById($id, $options = null, $useCache = false);

    /**
     * @param string $column
     * @param mixed $value
     * @param int $pdoType
     * @param string|string[] $orderBy
     * @param string|string[]|int|int[] $limit
     * @param string|string[] $options
     * @param bool $useCache
     * @return ValueObject|null
     * @throws Exception
     */
    public function &findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false);

    /**
     * @param ValueObject $prototype
     * @param string|string[] $orderBy
     * @param string|string[]|int|int[] $limit
     * @param string|string[] $options
     * @param string[] $includeNullColumns
     * @param string[] $ignoreColumns
     * @return ValueObject|null
     * @throws Exception
     */
    public function &findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = []);

    /**
     * @param callable $func
     * @param string|string[] $orderBy
     * @param string|string[]|int|int[] $limit
     * @param string|string[] $options
     * @return ValueObject|null
     * @throws Exception
     */
    public function &findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null);

    /**
     * @param string $column
     * @param mixed $value
     * @param int $pdoType
     * @param string|string[] $orderBy
     * @param string|string[]|int|int[] $limit
     * @param string|string[] $options
     * @param bool $useCache
     * @return ValueObject[]
     * @throws Exception
     */
    public function &findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false);

    /**
     * @param ValueObject $prototype
     * @param string|string[] $orderBy
     * @param string|string[]|int|int[] $limit
     * @param string|string[] $options
     * @param string[] $includeNullColumns
     * @param string[] $ignoreColumns
     * @return ValueObject[]
     * @throws Exception
     */
    public function &findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = []);

    /**
     * @param callable $func
     * @param string|string[] $orderBy
     * @param string|string[]|int|int[] $limit
     * @param string|string[] $options
     * @return ValueObject[]
     * @throws Exception
     */
    public function &findAll(callable $func = null, $orderBy = null, $limit = null, $options = null);

    /**
     * @param callable $func
     * @param string|string[] $orderBy
     * @param string|string[]|int|int[] $limit
     * @param string|string[] $options
     * @return ValueObjectIterator
     * @throws Exception
     */
    public function &iterate(callable $func = null, $orderBy = null, $limit = null, $options = null);

    /**
     * @param ValueObject $prototype
     * @param callable $func
     * @param bool $ignore
     * @param string|string[] $options
     * @param string[] $includeNullColumns
     * @param string[] $ignoreColumns
     * @return int
     */
    public function &insert(ValueObject $prototype, callable $func = null, $ignore = false, $options = null, array $includeNullColumns = [], array $ignoreColumns = []);

    /**
     * @param ValueObject $prototype
     * @param callable|null $func
     * @param string[] $includeNullColumns
     * @param string[] $whereColumns
     * @param string[] $ignoreColumns
     * @param bool $requireConditions
     * @return int
     */
    public function &update(ValueObject $prototype, callable $func = null, array $includeNullColumns = [], array $whereColumns = [], array $ignoreColumns = [], $requireConditions = true);

    /**
     * @param ValueObject $prototype
     * @param string $selectOptions
     * @param string[] $includeNullColumns
     * @param string[] $ignoreColumns
     * @return ValueObject|int
     * @throws Exception
     */
    public function &findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []);

    /**
     * @return void
     */
    public function flushCache();
}
