<?php

namespace BlakePrinting\Db;

interface ExtensibleEntity {
    /**
     * @return string
     */
    public function getEntityClassName();

    /**
     * @return string
     */
    public function getEntityColumnName();

    /**
     * @return mixed
     */
    public function getEntityColumnValue();

    /**
     * @return int
     */
    public function getEntityColumnType();
}
