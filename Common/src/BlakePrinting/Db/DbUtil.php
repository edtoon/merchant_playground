<?php

namespace BlakePrinting\Db;

use BlakePrinting\Util\ConfigUtil;
use BlakePrinting\Util\Utils;
use DateTime;
use Exception;
use phpDocumentor\Reflection\DocBlock;
use phpDocumentor\Reflection\DocBlock\Tag\ParamTag;
use Stringy\StaticStringy as S;
use ReflectionClass;
use ReflectionMethod;

class DbUtil {
    const FUNC_FROM_ISO8601_SUBSET = "from_iso8601_subset";
    const MYSQL_DATE_FORMAT = "Y-m-d H:i:s";

    private function __construct() { }

    /**
     * @param string $dateString
     * @return string
     * @throws Exception
     */
    public static function convertMysqlDatetimeToTimestamp($dateString) {
        $dateString = Utils::getTrimmedStringOrNull($dateString);

        if($dateString !== null && $dateString !== "0000-00-00 00:00:00") {
            $dateTime = DateTime::createFromFormat(self::MYSQL_DATE_FORMAT, $dateString);

            if($dateTime !== null && $dateString === $dateTime->format(self::MYSQL_DATE_FORMAT)) {
                return $dateTime->getTimestamp();
            } else {
                throw new Exception("Invalid date string: " . $dateString);
            }
        }

        return null;
    }

    /**
     * @param string $func
     * @param string $schema
     * @param string $database
     * @return string
     */
    public static function formatFunction($func, $schema = null, $database = null) {
        $config =& ConfigUtil::getConfig();
        $schemaName = null;

        if($database === null) {
            $database =& $config->retrieve("database", "default");
        }

        if($schema === null) {
            $schema =& $config->retrieve("database", $database, "schemas", "default");
            $schemaName =& $config->retrieve("database", $database, "schemas", $schema, "name");
        }

        if(!empty($schemaName)) {
            return $schemaName . "." . $func;
        }

        return $func;
    }

    /**
     * @param array $results
     * @param ReflectionClass $reflectionClass
     * @return ValueObject[]
     */
    public static function &mapResultsToValueObjects(array &$results, ReflectionClass $reflectionClass) {
        $valueObjects = null;

        if($results !== null) {
            $valueObjects = [];

            foreach($results as &$result) {
                $valueObject =& self::mapResultToValueObject($result, $reflectionClass);
                $valueObjects[] =& $valueObject;
            }

            unset($result);
        }

        return $valueObjects;
    }

    /**
     * @param array $result
     * @param ReflectionClass $reflectionClass
     * @return ValueObject
     */
    public static function &mapResultToValueObject(array $result, ReflectionClass $reflectionClass) {
        $methods = $reflectionClass->getMethods(ReflectionMethod::IS_PUBLIC);
        $valueObject = $reflectionClass->newInstance();

        foreach($methods as &$method) {
            if($method->getNumberOfParameters() == 1) {
                $methodName = $method->getName();

                if(strlen($methodName) >= 4 && substr($methodName, 0, 3) == "set") {
                    $columnName = (string)S::underscored(substr($methodName, 3));

                    foreach($result as $field => &$value) {
                        if($field == $columnName) {
                            $docComment = $method->getDocComment();
                            $typeName = null;

                            if(!empty($docComment)) {
                                $docBlock = new DocBlock($docComment);
                                $docVars = $docBlock->getTagsByName("param");

                                if(!empty($docVars)) {
                                    $docVar = $docVars[0];

                                    if($docVar instanceof ParamTag) {
                                        $typeName = $docVar->getType();

                                        if($typeName == "bool" || $typeName == "boolean") {
                                            if(ctype_digit($value)) {
                                                if($value == 1) {
                                                    $value = true;
                                                } else if($value == 0) {
                                                    $value = false;
                                                }
                                            }
                                        } else if($typeName == "string" && substr($methodName, 0, 5) == "setIp" && !empty($value)) {
                                            $value = Utils::getIpFromBin($value);
                                        }
                                    }
                                }
                            }

                            $method->invoke($valueObject, $value);

                            break;
                        }
                    }

                    unset($value);
                }
            }
        }

        unset($method);

        return $valueObject;
    }
}
