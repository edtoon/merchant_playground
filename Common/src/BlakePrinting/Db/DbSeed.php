<?php

namespace BlakePrinting\Db;

use Exception;
use Phinx\Db\Adapter\PdoAdapter;
use Phinx\Seed\AbstractSeed;

class DbSeed extends AbstractSeed {
    /** @var PDODelegate */
    private $delegate = null;

    /**
     * @return PDODelegate
     * @throws Exception
     */
    public function &getConnection() {
        if($this->delegate === null) {
            $adapter = $this->getAdapter();

            if($adapter instanceof PdoAdapter) {
                $connection = $adapter->getConnection();

                $this->delegate = new PDODelegate($connection);
            } else {
                throw new Exception("Adapter was not for PDO - class is: " . get_class($adapter));
            }
        }

        return $this->delegate;
    }
}
