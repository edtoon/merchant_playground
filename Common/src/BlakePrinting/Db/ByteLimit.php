<?php

namespace BlakePrinting\Db;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\InvalidOptionsException;
use Symfony\Component\Validator\Exception\MissingOptionsException;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class ByteLimit extends Constraint {
    public $message = "Value exceeded limit of {{ limit }} byte.|Value exceeded limit of {{ limit }} bytes.";
    public $limit;

    public function __construct($options = null) {
        if(null !== $options && !is_array($options)) {
            $options = ["limit" => $options];
        }

        parent::__construct($options);

        if(null === $this->limit) {
            throw new MissingOptionsException('Option "limit" must be given for constraint ' .  __CLASS__, ['limit']);
        }

        if((!is_int($this->limit) && !ctype_digit($this->limit)) || $this->limit < 0) {
            throw new InvalidOptionsException(
                'Option "limit" must be a number greater than or equal to zero for constraint ' . __CLASS__, ['limit']
            );
        }
    }
}
