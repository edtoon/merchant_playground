<?php

namespace BlakePrinting\Db;

use BlakePrinting\Util\ConfigUtil;
use BlakePrinting\Util\Utils;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Cache\CacheProvider;
use Stringy\StaticStringy as S;
use Exception;
use PDO;
use ReflectionClass;
use ReflectionMethod;

abstract class AbstractDataAccessObject implements DataAccessObject {
    /** @var array */
    private static $instanceCache = [];
    /** @var PDO */
    private $connection = null;
    /** @var string */
    private $schemaName = null;
    /** @var string */
    private $tableName = null;
    /** @var string */
    private $idColumnName = null;
    /** @var int */
    private $idColumnType = null;
    /** @var CacheProvider */
    private $cache = null;
    /** @var bool */
    private $reflectionDataLoaded = false;
    /** @var string */
    private $className = null;
    /** @var ReflectionClass */
    private $reflectionClass = null;
    /** @var string */
    private $voClassName = null;
    /** @var ReflectionClass */
    private $voReflectionClass = null;
    /** @var string */
    private $database = null;
    /** @var string */
    private $loadedSchemaName = null;
    /** @var string */
    private $loadedTableName = null;
    /** @var ReflectionMethod[] */
    private $voReflectionMethods = null;
    /** @var array */
    private $reflectedColumns = null;
    /** @var PDOQuery */
    private $pdoQuery = null;

    /**
     * @link http://php.net/manual/en/pdo.constants.php
     * @param PDO $connection
     * @param string $schemaName
     * @param string $tableName
     * @param string $idColumnName
     * @param int $idColumnType
     */
    public function __construct(PDO $connection = null, $schemaName = null, $tableName = null, $idColumnName = "id", $idColumnType = PDO::PARAM_INT) {
        $this->setConnection($connection);
        $this->setSchemaName($schemaName);
        $this->setTableName($tableName);
        $this->setIdColumnName($idColumnName);
        $this->setIdColumnType($idColumnType);
    }

    /**
     * @return DataAccessObject
     * @throws Exception
     */
    public static function &instance() {
        $args = func_get_args();
        $argCount = count($args);
        /** @var PDO $connection */
        $connection = null;
        $setConnection = true;
        $extraArgsFound = false;
        $cacheKeyExtra = "";

        for($i = 0; $i < $argCount; $i++) {
            $arg = $args[$i];

            if($arg instanceof PDO) {
                if($connection !== null) {
                    throw new Exception("Multiple PDO instances specified, can't determine which to use");
                }

                $connection = $arg;
                $setConnection = false;
            } else {
                $cacheKeyExtra .= (($extraArgsFound ? "_" : "") . Utils::getVarDump($arg));
                $extraArgsFound = true;
            }
        }

        if($connection === null) {
            $connection = PDOUtil::getMysqlConnection();
        }

        $uniqueId = PDOUtil::getUniqueIdentifierForConnection($connection);
        $cacheKey = static::class . "#" . $uniqueId . ($extraArgsFound ? ("::" . $cacheKeyExtra) : "");

        if(isset(self::$instanceCache[$cacheKey])) {
            return self::$instanceCache[$cacheKey];
        }

        $reflectionClass = Utils::getReflectionClassByName(static::class);
        /** @var DataAccessObject $instance */
        $instance = $reflectionClass->newInstanceArgs($args);

        if($setConnection) {
            $instance->setConnection($connection);
        }

        self::$instanceCache[$cacheKey] =& $instance;

        return $instance;
    }

    /**
     * @inheritdoc
     */
    public function &getConnection() {
        if($this->connection === null) {
            $this->setConnection(PDOUtil::getMysqlConnection());
        }

        return $this->connection;
    }

    /**
     * @inheritdoc
     */
    public function setConnection(PDO $connection = null) {
        $this->connection =& $connection;
        $this->reflectionDataLoaded = false;
    }

    /**
     * @inheritdoc
     */
    public function getSchemaName() {
        $this->loadReflectionData();

        return $this->loadedSchemaName;
    }

    /**
     * @inheritdoc
     */
    public function setSchemaName($schemaName) {
        $this->schemaName = $schemaName;
        $this->reflectionDataLoaded = false;
    }

    /**
     * @inheritdoc
     */
    public function getTableName() {
        $this->loadReflectionData();

        return $this->loadedTableName;
    }

    /**
     * @inheritdoc
     */
    public function setTableName($tableName) {
        $this->tableName = $tableName;
        $this->reflectionDataLoaded = false;
    }

    /**
     * @return string
     */
    public function getIdColumnName() {
        return $this->idColumnName;
    }

    /**
     * @param string $idColumnName
     */
    public function setIdColumnName($idColumnName) {
        $this->idColumnName = $idColumnName;
        $this->reflectionDataLoaded = false;
    }

    /**
     * @link http://php.net/manual/en/pdo.constants.php
     * @return int
     */
    public function getIdColumnType() {
        return $this->idColumnType;
    }

    /**
     * @link http://php.net/manual/en/pdo.constants.php
     * @param int $idColumnType
     */
    public function setIdColumnType($idColumnType) {
        $this->idColumnType = $idColumnType;
        $this->reflectionDataLoaded = false;
    }

    /**
     * @return ReflectionClass
     * @throws Exception
     */
    public function getReflectionClass() {
        $this->loadReflectionData();

        return $this->reflectionClass;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getVoClassName() {
        $this->loadReflectionData();

        return $this->voClassName;
    }

    /**
     * @return ReflectionClass
     * @throws Exception
     */
    public function &getVoReflectionClass() {
        $this->loadReflectionData();

        return $this->voReflectionClass;
    }

    /**
     * @return ReflectionMethod[]
     * @throws Exception
     */
    public function &getVoReflectionMethods() {
        $this->loadReflectionData();

        if($this->voReflectionMethods === null) {
            $this->voReflectionMethods = $this->voReflectionClass->getMethods(ReflectionMethod::IS_PUBLIC);
        }

        return $this->voReflectionMethods;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getReflectedColumns() {
        $this->loadReflectionData();

        if($this->reflectedColumns === null) {
            $this->reflectedColumns = [];

            foreach($this->getVoReflectionMethods() as &$method) {
                if($method->getNumberOfParameters() == 0) {
                    $methodName = $method->getName();
                    $methodNameLen = strlen($methodName);
                    $propertyName = null;

                    if($methodNameLen >= 3) {
                        if(substr($methodName, 0, 2) == "is") {
                            $propertyName = substr($methodName, 2);
                        } else if($methodNameLen >= 4 && substr($methodName, 0, 3) == "get") {
                            $propertyName = substr($methodName, 3);
                        }
                    }

                    if($propertyName !== null) {
                        $this->reflectedColumns[$propertyName] = [
                            (string)S::underscored($propertyName),
                            PDOUtil::getPdoTypeFromReflectionMethod($method),
                            $method
                        ];
                    }
                }
            }

            unset($method);
        }

        ksort($this->reflectedColumns);

        return $this->reflectedColumns;
    }

    /**
     * @inheritdoc
     */
    public function &getLastQuery() {
        return $this->pdoQuery;
    }

    /**
     * @inheritdoc
     */
    public function &findById($id, $options = null, $useCache = false) {
        return $this->findByColumn($this->idColumnName, $id, $this->idColumnType, null, null, $options, $useCache);
    }

    /**
     * @inheritdoc
     */
    public function &findByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false) {
        $cacheKey = null;
        $cache = null;

        if($useCache) {
            $cacheKey = $column . ":" . $orderBy . ":" . $limit . ":" . Utils::getVarDump($value);
            $cache = $this->getCache();
            $cachedValue = $cache->fetch($cacheKey);

            if($cachedValue !== false) {
                return $cachedValue;
            }
        }

        $this->pdoQuery =& $this->createQuery($orderBy, $limit, $options);

        if($value === null) {
            $this->pdoQuery->whereNull($column);
        } else {
            $this->pdoQuery->where($column, $value, $pdoType);
        }

        $result =& $this->pdoQuery->getSingleRow($this->getConnection());
        $valueObject = null;

        if($result !== null) {
            $valueObject =& DbUtil::mapResultToValueObject($result, $this->getVoReflectionClass());
        }

        if($useCache) {
            $cache->save($cacheKey, $valueObject);
        }

        return $valueObject;
    }

    /**
     * @inheritdoc
     */
    public function &findByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = []) {
        $this->pdoQuery = $this->createQueryFromPrototype($prototype, $orderBy, $limit, $options, $includeNullColumns, $ignoreColumns);
        $result =& $this->pdoQuery->getSingleRow($this->getConnection());
        $valueObject = null;

        if($result !== null) {
            $valueObject =& DbUtil::mapResultToValueObject($result, $this->getVoReflectionClass());
        }

        return $valueObject;
    }

    /**
     * @inheritdoc
     */
    public function &findSingle(callable $func = null, $orderBy = null, $limit = null, $options = null) {
        $voReflectionClass = $this->getVoReflectionClass();
        $this->pdoQuery =& $this->createQuery($orderBy, $limit, $options);

        if($func !== null) {
            $func($this->pdoQuery, $voReflectionClass);
        }

        $result =& $this->pdoQuery->getSingleRow($this->getConnection());
        $valueObject = null;

        if($result !== null) {
            $valueObject =& DbUtil::mapResultToValueObject($result, $voReflectionClass);
        }

        return $valueObject;
    }

    /**
     * @inheritdoc
     */
    public function &findAllByColumn($column, $value, $pdoType = null, $orderBy = null, $limit = null, $options = null, $useCache = false) {
        $cacheKey = null;
        $cache = null;

        if($useCache) {
            $cacheKey = $column . ":" . $orderBy . ":" . $limit . ":" . Utils::getVarDump($value);
            $cache = $this->getCache();
            $cachedValue = $cache->fetch($cacheKey);

            if($cachedValue !== false) {
                return $cachedValue;
            }
        }

        $this->pdoQuery =& $this->createQuery($orderBy, $limit, $options);

        if($value === null) {
            $this->pdoQuery->whereNull($column);
        } else {
            $this->pdoQuery->where($column, $value, $pdoType);
        }

        $results =& $this->pdoQuery->getResults($this->getConnection());
        $valueObjects =& DbUtil::mapResultsToValueObjects($results, $this->getVoReflectionClass());

        if($useCache) {
            $cache->save($cacheKey, $valueObjects);
        }

        return $valueObjects;
    }

    /**
     * @inheritdoc
     */
    public function &findAllByPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = []) {
        $this->pdoQuery =& $this->createQueryFromPrototype($prototype, $orderBy, $limit, $options, $includeNullColumns, $ignoreColumns);
        $results =& $this->pdoQuery->getResults($this->getConnection());

        return DbUtil::mapResultsToValueObjects($results, $this->getVoReflectionClass());
    }

    /**
     * @inheritdoc
     */
    public function &findAll(callable $func = null, $orderBy = null, $limit = null, $options = null) {
        $voReflectionClass = $this->getVoReflectionClass();
        $this->pdoQuery =& $this->createQuery($orderBy, $limit, $options);

        if($func !== null) {
            $func($this->pdoQuery, $voReflectionClass);
        }

        $results =& $this->pdoQuery->getResults($this->getConnection());

        return DbUtil::mapResultsToValueObjects($results, $voReflectionClass);
    }

    /**
     * @inheritdoc
     */
    public function &iterate(callable $func = null, $orderBy = null, $limit = null, $options = null) {
        $this->pdoQuery =& $this->createQuery($orderBy, $limit, $options);

        if($func !== null) {
            $func($this->pdoQuery, $this->getVoReflectionClass());
        }

        $stmt =& $this->pdoQuery->execute($this->getConnection());
        $iterator = new ValueObjectIterator($this, $stmt);

        return $iterator;
    }

    /**
     * @inheritdoc
     */
    public function &insert(ValueObject $prototype, callable $func = null, $ignore = false, $options = null, array $includeNullColumns = [], array $ignoreColumns = []) {
        $this->flushCache();

        $connection =& $this->getConnection();
        $this->pdoQuery =& $this->createInsertFromPrototype($prototype, $includeNullColumns, $ignoreColumns)->ignore($ignore)->options($options);

        if($func !== null) {
            $func($this->pdoQuery, $this->getVoReflectionClass());
        }

        if($this->pdoQuery->getIncludeLastInsertId()) {
            return $this->pdoQuery->executeEnsuringLastInsertId($connection);
        } else if($ignore !== true) {
            return $this->pdoQuery->executeEnsuringRowCount($connection, 1);
        } else {
            return $this->pdoQuery->executeGetRowCount($connection);
        }
    }

    /**
     * @inheritdoc
     */
    public function &update(ValueObject $prototype, callable $func = null, array $includeNullColumns = [], array $whereColumns = [], array $ignoreColumns = [], $requireConditions = true) {
        $this->flushCache();

        $this->pdoQuery =& $this->createUpdateFromPrototype($prototype, $includeNullColumns, $whereColumns, $ignoreColumns)->requireConditions($requireConditions);

        if($func !== null) {
            $func($this->pdoQuery, $this->getVoReflectionClass());
        }

        return $this->pdoQuery->executeGetRowCount($this->getConnection());
    }

    /**
     * @inheritdoc
     */
    public function &findOrCreate(ValueObject $prototype, $selectOptions = "FOR UPDATE", array $includeNullColumns = [], array $ignoreColumns = []) {
        $valueObject =& $this->findByPrototype($prototype, null, null, $selectOptions, $includeNullColumns, $ignoreColumns);

        if($valueObject === null) {
            $connection =& $this->getConnection();
            $insert = $this->createInsertFromPrototype($prototype, $includeNullColumns, $ignoreColumns);

            if($insert->getIncludeLastInsertId()) {
                $id =& $insert->executeEnsuringLastInsertId($connection);
                $valueObject =& $this->findById($id);
            } else {
                $insert->executeEnsuringRowCount($connection, 1);

                $valueObject =& $this->findByPrototype($prototype, null, null, $selectOptions, $includeNullColumns, $ignoreColumns);
            }

            if($valueObject === null) {
                throw new Exception("Couldn't find or create value object: " . Utils::getVarDump($prototype));
            }
        }

        return $valueObject;
    }

    /**
     * @inheritdoc
     */
    public function flushCache() {
        $this->getCache()->flushAll();
    }

    /**
     * @param ValueObject $prototype
     * @param string|string[] $orderBy
     * @param string|string[]|int|int[] $limit
     * @param string|string[] $options
     * @param string[] $includeNullColumns
     * @param string[] $ignoreColumns
     * @return PDOQuery
     * @throws Exception
     */
    private function &createQueryFromPrototype(ValueObject $prototype, $orderBy = null, $limit = null, $options = null, array $includeNullColumns = [], array $ignoreColumns = []) {
        $this->verifyPrototypeClass($prototype);

        $pdoQuery =& $this->createQuery($orderBy, $limit, $options);
        $reflectedColumns = $this->getReflectedColumns();

        foreach($reflectedColumns as $propertyName => &$reflectedColumnData) {
            /** @var ReflectionMethod $getMethod */
            $getMethod = $reflectedColumnData[2];
            $value = $getMethod->invoke($prototype);
            $columnName = $reflectedColumnData[0];
            $columnType = $reflectedColumnData[1];

            if(in_array($columnName, $ignoreColumns)) {
                continue;
            } else if($value !== null) {
                $pdoQuery->where($columnName, $value, $columnType);
            } else if(in_array($columnName, $includeNullColumns)) {
                $pdoQuery->whereValueOrNull($columnName, $value, $columnType);
            }
        }

        unset($reflectedColumnData);

        return $pdoQuery;
    }

    /**
     * @param ValueObject $prototype
     * @param array $includeNullColumns
     * @param array $ignoreColumns
     * @return PDOQuery
     */
    public function &createInsertFromPrototype(ValueObject $prototype, array $includeNullColumns = [], array $ignoreColumns = []) {
        $this->verifyPrototypeClass($prototype);

        $insert =& PDOQuery::insert()->into($this);
        $reflectedColumns = $this->getReflectedColumns();

        foreach($reflectedColumns as $propertyName => &$reflectedColumnData) {
            /** @var ReflectionMethod $getMethod */
            $getMethod = $reflectedColumnData[2];
            $value = $getMethod->invoke($prototype);
            $columnName = $reflectedColumnData[0];
            $columnType = $reflectedColumnData[1];

            if("id" === $columnName && $columnType === PDO::PARAM_INT && $columnName === $this->idColumnName && $columnType === $this->idColumnType) {
                $insert->withLastInsertId();
            }

            if($columnName == "created" && $columnType === PDO::PARAM_INT) {
                $insert->value($columnName, "UNIX_TIMESTAMP()");
            } else if(in_array($columnName, $ignoreColumns)) {
                continue;
            } else if(($columnName == "updated" || $columnName == "updater") && $columnType === PDO::PARAM_INT) {
                continue;
            } else if($value !== null || in_array($columnName, $includeNullColumns)) {
                $insert->value($columnName, $value, $columnType);
            }
        }

        unset($reflectedColumnData);

        return $insert;
    }

    /**
     * @param ValueObject $prototype
     * @param array $includeNullColumns
     * @param array $whereColumns
     * @param array $ignoreColumns
     * @return PDOQuery
     */
    public function &createUpdateFromPrototype(ValueObject $prototype, array $includeNullColumns = [], array $whereColumns = [], array $ignoreColumns = []) {
        $this->verifyPrototypeClass($prototype);

        $update =& PDOQuery::update()->table($this);
        $reflectedColumns = $this->getReflectedColumns();

        foreach($reflectedColumns as $propertyName => &$reflectedColumnData) {
            /** @var ReflectionMethod $getMethod */
            $getMethod = $reflectedColumnData[2];
            $value = $getMethod->invoke($prototype);
            $columnName = $reflectedColumnData[0];
            $columnType = $reflectedColumnData[1];

            if($columnName == "updated" && $columnType === PDO::PARAM_INT) {
                $update->set($columnName, "UNIX_TIMESTAMP()");
            } else if(in_array($columnName, $ignoreColumns)) {
                continue;
            } else if(in_array($columnName, $whereColumns)) {
                $update->whereValueOrNull($columnName, $value, $columnType);
            } else if($columnName === $this->idColumnName) {
                if(!empty($whereColumns)) {
                    if($value !== null || in_array($columnName, $includeNullColumns)) {
                        $update->set($columnName, $value, $columnType);
                    }
                } else if($value !== null || in_array($columnName, $includeNullColumns)) {
                    $update->whereValueOrNull($columnName, $value, $columnType);
                }
            } else if(($columnName == "created" || $columnName == "creator") && $columnType === PDO::PARAM_INT) {
                continue;
            } else if($value !== null || in_array($columnName, $includeNullColumns)) {
                $update->set($columnName, $value, $columnType);
            }
        }

        unset($reflectedColumnData);

        return $update;
    }

    private function verifyPrototypeClass(ValueObject $prototype) {
        $prototypeClass = get_class($prototype);
        $voClassName = $this->getVoClassName();

        if($voClassName != $prototypeClass) {
            throw new Exception("Object of class: " . $prototypeClass . " found, expected class: " . $voClassName);
        }
    }

    /**
     * @param string|string[] $orderBy
     * @param string|string[]|int|int[] $limit
     * @param string|string[] $options
     * @return PDOQuery
     */
    private function &createQuery($orderBy = null, $limit = null, $options = null) {
        return PDOQuery::select()->from($this)->addOrderByLimitAndOptions($orderBy, $limit, $options);
    }

    public function formatSchemaAndTableName() {
        $this->loadReflectionData();

        if($this->loadedSchemaName !== null) {
            return $this->loadedSchemaName . "." . $this->loadedTableName;
        }

        return $this->loadedTableName;
    }

    /**
     * @throws Exception
     */
    private function loadReflectionData() {
        if($this->reflectionDataLoaded) {
            return;
        }

        $config =& ConfigUtil::getConfig();

        $this->className = get_class($this);
        $this->reflectionClass =& Utils::getReflectionClassByName($this->className);
        $this->voClassName = ((strrpos($this->className, "DAO", -3) !== false) ? (substr($this->className, 0, -3) . "VO") : $this->className);
        $this->voReflectionClass =& Utils::getReflectionClassByName($this->voClassName);
        $this->database = (PDOUtil::getDatabaseNameByConnection($this->getConnection()) ?: $config->retrieve("database", "default"));

        $loadSchemaName = null;

        if($this->schemaName !== null) {
            $configSchemaName = $config->retrieve("database", $this->database, "schemas", $this->schemaName, "name");

            if($configSchemaName !== null) {
                $loadSchemaName = $configSchemaName;
            } else {
                $loadSchemaName = $this->schemaName;
            }
        } else {
            $namespace = $this->reflectionClass->getNamespaceName();

            if(!empty($namespace) && substr($namespace, 0, 14) == "BlakePrinting\\") {
                $subNamespace = substr($namespace, 14);
                $moduleEndPos = strpos($subNamespace, "\\");
                $module = $subNamespace;

                if($moduleEndPos !== false) {
                    $module = substr($subNamespace, 0, $moduleEndPos);
                }

                $configSchemaName = $config->retrieve("database", $this->database, "schemas", strtolower($module), "name");

                if($configSchemaName !== null) {
                    $loadSchemaName = $configSchemaName;
                } else {
                    $underscored = (string)S::underscored($module);
                    $configSchemaName = $config->retrieve("database", $this->database, "schemas", $underscored, "name");

                    if($configSchemaName !== null) {
                        $loadSchemaName = $configSchemaName;
                    }
                }
            }

            if($loadSchemaName === null) {
                $loadSchemaName = $config->retrieve("database", $this->database, "schemas", "default");
            }
        }

        $this->loadedSchemaName = $loadSchemaName;

        $loadTableName = $this->tableName;

        if($loadTableName === null) {
            $namespaceEndPos = strrpos($this->className, "\\");
            $tableName = ($namespaceEndPos === false ? $this->className : substr($this->className, $namespaceEndPos + 1));

            if(strrpos($tableName, "DAO", -3) !== false) {
                $tableName = substr($tableName, 0, -3);
            }

            $loadTableName = (string)S::underscored($tableName);
        }

        $this->loadedTableName = $loadTableName;

        if($this->cache !== null) {
            $this->cache->flushAll();
        }

        $this->voReflectionMethods = null;
        $this->reflectedColumns = null;
        $this->reflectionDataLoaded = true;
    }

    /**
     * @return CacheProvider
     */
    public function getCache() {
        if($this->cache === null) {
            $this->cache = new ArrayCache();
        }

        return $this->cache;
    }
}
