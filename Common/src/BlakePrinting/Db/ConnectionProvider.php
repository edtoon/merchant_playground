<?php

namespace BlakePrinting\Db;

use PDO;

interface ConnectionProvider {
    /**
     * @return PDO
     */
    public function getConnection();
}
