<?php

namespace BlakePrinting\Db;

use BlakePrinting\Util\Utils;
use Exception;

class AbstractExtensibleEntity implements ExtensibleEntity {
    /** @var string */
    private $entityClassName = null;
    /** @var string */
    private $entityColumnName = null;
    /** @var mixed */
    private $entityColumnValue = null;
    /** @var int */
    private $entityColumnType = null;

    /**
     * @return string
     */
    public function getEntityClassName()
    {
        return $this->entityClassName;
    }

    /**
     * @param string $entityClassName
     */
    public function setEntityClassName($entityClassName)
    {
        $this->entityClassName = $entityClassName;
    }

    /**
     * @return string
     */
    public function getEntityColumnName()
    {
        return $this->entityColumnName;
    }

    /**
     * @param string $entityColumnName
     */
    public function setEntityColumnName($entityColumnName)
    {
        $this->entityColumnName = $entityColumnName;
    }

    /**
     * @return mixed
     */
    public function getEntityColumnValue()
    {
        return $this->entityColumnValue;
    }

    /**
     * @param mixed $entityColumnValue
     */
    public function setEntityColumnValue($entityColumnValue)
    {
        $this->entityColumnValue = $entityColumnValue;
    }

    /**
     * @return int
     */
    public function getEntityColumnType()
    {
        return $this->entityColumnType;
    }

    /**
     * @param int $entityColumnType
     */
    public function setEntityColumnType($entityColumnType)
    {
        $this->entityColumnType = $entityColumnType;
    }

    /**
     * @return AbstractDataAccessObject
     * @throws Exception
     */
    public function getEntityDAO() {
        return $this->getConnectedDAO();
    }

    /**
     * @param $extendingClassName
     * @return AbstractDataAccessObject
     * @throws Exception
     */
    public function getExtensionDAO($extendingClassName) {
        return $this->getConnectedDAO($extendingClassName);
    }

    /**
     * @param string $extension
     * @return AbstractDataAccessObject
     * @throws Exception
     */
    private function getConnectedDAO($extension = "") {
        $reflectionClass = Utils::getReflectionClassByName($this->getEntityBaseName() . $extension . "DAO");
        $instanceMethod = $reflectionClass->getMethod("instance");
        $connection = null;

        if($this instanceof ConnectionProvider) {
            $connection = $this->getConnection();
        }

        if($connection === null) {
            return $instanceMethod->invoke(null);
        } else {
            return $instanceMethod->invoke(null, $connection);
        }
    }

    protected function getEntityBaseName() {
        $entityName = $this->getEntityClassName();

        if(strrpos($entityName, "DAO", -3) !== false) {
            return substr($entityName, 0, -3);
        } else if(strrpos($entityName, "VO", -2) !== false) {
            return substr($entityName, 0, -2);
        }

        return $entityName;
    }
}
