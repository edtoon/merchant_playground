<?php

namespace BlakePrinting\Db;

use BlakePrinting\Util\Utils;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ByteLimitValidator extends ConstraintValidator {
    public function validate($value, Constraint $constraint) {
        if(!$constraint instanceof ByteLimit) {
            throw new UnexpectedTypeException($constraint, ByteLimit::class);
        }

        if(null !== $value) {
            $byteLength = Utils::getStringByteLength($value);

            if($byteLength > $constraint->limit) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter("{{ value }}", $this->formatValue((string)$value))
                    ->setParameter("{{ limit }}", $constraint->limit)
                    ->setInvalidValue($value)
                    ->setPlural((int)$constraint->limit)
                    ->addViolation()
                ;
            }
        }
    }
}
