<?php

namespace BlakePrinting\Db;

use Phinx\Db\Adapter\PdoAdapter;
use Phinx\Migration\AbstractMigration;
use Exception;

class DbMigration extends AbstractMigration {
    /** @var PDODelegate */
    private $delegate = null;

    /**
     * @return PDODelegate
     * @throws Exception
     */
    public function &getConnection() {
        if($this->delegate === null) {
            $adapter = $this->getAdapter();

            if($adapter instanceof PdoAdapter) {
                $connection = $adapter->getConnection();

                $this->delegate = new PDODelegate($connection);
            } else {
                throw new Exception("Adapter was not for PDO - class is: " . get_class($adapter));
            }
        }

        return $this->delegate;
    }

    protected function tableDao(DataAccessObject $dao, array $options = []) {
        $defaultSchemaName = $this->getDefaultSchemaName();
        $daoSchemaName = $dao->getSchemaName();

        if($daoSchemaName != $defaultSchemaName) {
            return $this->table($dao->formatSchemaAndTableName(), $options);
        }

        return $this->table($dao->getTableName(), $options);
    }

    /**
     * @param string $change
     */
    protected function alterDao(DataAccessObject $dao, $change) {
        $defaultSchemaName = $this->getDefaultSchemaName();
        $daoSchemaName = $dao->getSchemaName();

        if($daoSchemaName != $defaultSchemaName) {
            return $this->alter($dao->formatSchemaAndTableName(), $change);
        }

        return $this->alter($dao->getTableName(), $change);
    }

    /**
     * @param string $tableName
     * @param string $change
     */
    protected function alter($tableName, $change) {
        return $this->execute("ALTER TABLE " . $tableName . " " . $change);
    }

    protected function dropDao(DataAccessObject $dao) {
        $defaultSchemaName = $this->getDefaultSchemaName();
        $daoSchemaName = $dao->getSchemaName();

        if($daoSchemaName != $defaultSchemaName) {
            $this->drop($dao->formatSchemaAndTableName());
        }

        $this->drop($dao->getTableName());
    }

    /**
     * @param string $tableName
     */
    protected function drop($tableName) {
        $table = $this->table($tableName);

        if($table->exists()) {
            $table->drop();
        }
    }

    private function getDefaultSchemaName() {
        $adapterOptions = $this->getAdapter()->getOptions();

        if(isset($adapterOptions["name"])) {
            return $adapterOptions["name"];
        }

        return null;
    }
}
