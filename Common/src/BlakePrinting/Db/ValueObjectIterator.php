<?php

namespace BlakePrinting\Db;

use Iterator;
use PDO;
use PDOStatement;
use ReflectionClass;

class ValueObjectIterator implements Iterator {
    /** @var ReflectionClass */
    private $voReflectionClass = null;
    /** @var PDOStatement */
    private $stmt = null;
    /** @var ValueObject */
    private $result = null;
    /** @var int */
    private $key = null;

    public function __construct(AbstractDataAccessObject $dao, PDOStatement $stmt) {
        $this->voReflectionClass = $dao->getVoReflectionClass();
        $this->stmt = $stmt;
    }

    /**
     * @return ValueObject
     */
    public function current() {
        return $this->result;
    }

    public function next() {
        $this->key++;
        $next = $this->stmt->fetch(PDO::FETCH_ASSOC);

        if($next !== false) {
            $this->result =& DbUtil::mapResultToValueObject($next, $this->voReflectionClass);
        } else {
            $this->result = null;
        }
    }

    public function key() {
        return $this->key;
    }

    public function valid() {
        return ($this->current() !== null);
    }

    public function rewind() {
        $this->next();
    }
}
