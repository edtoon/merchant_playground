<?php

namespace BlakePrinting\Util;

use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\PDOUtil;
use DateTime;
use DateTimeZone;
use Exception;
use ForceUTF8\Encoding;
use PDO;
use phpseclib\Crypt\RSA;
use ReflectionClass;
use ReflectionException;
use ReflectionObject;
use ReflectionProperty;
use Stringy\StaticStringy as S;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use XMLReader;

class Utils {
    /**
     * http://www.localizingjapan.com/blog/2012/01/30/detecting-and-conveting-japanese-multibyte-encodings-in-php/
     * https://web.archive.org/web/20131211174550/http://asia-gazette.com/news/japan/109
     * https://web.archive.org/web/20130210205149/http://pastebin.com/NEKaB72F
     */
    const JAPANESE_ENCODINGS = [
        "Shift-JIS", "EUC-JP", "JIS", "SJIS", "JIS-ms", "eucJP-win", "SJIS-win", "ISO-2022-JP",
        "ISO-2022-JP-MS", "SJIS-mac", "SJIS-Mobile#DOCOMO", "SJIS-Mobile#KDDI",
        "SJIS-Mobile#SOFTBANK", "UTF-8-Mobile#DOCOMO", "UTF-8-Mobile#KDDI-A",
        "UTF-8-Mobile#KDDI-B", "UTF-8-Mobile#SOFTBANK", "ISO-2022-JP-MOBILE#KDDI",
        // everything from this point onwards is not from the article
        "SJIS-2004", "EUC-JP-2004", "ISO-2022-JP-2004"
    ];
    const DETECTABLE_JAPANESE_ENCODINGS = ["EUC-JP", "SJIS", "eucJP-win", "SJIS-win", "JIS", "ISO-2022-JP"];

    private static $reflectionClasses = [];
    private static $availableEncodingList = null;

    private function __construct() { }

    /**
     * @param string $value
     * @param bool $force
     * @return string
     */
    public static function ensureUtf8($value) {
        if($value !== null) {
            $logger =& LogUtil::getLogger();
            $detectedEncoding = mb_detect_encoding($value, self::getAvailableEncodingList(), true);
            $orig = $value;

            if($detectedEncoding !== false && $detectedEncoding !== "UTF-8") {
                $value = mb_convert_encoding($value, "UTF-8", self::getAvailableEncodingList());

                if(md5($value) !== md5($orig)) {
                    $logger->debug("Converted " . $detectedEncoding . " value from: " . $orig . " to: " . $value);
                }
            } else {
                $value = self::sanitizeUtf8($value);

                if(md5($value) !== md5($orig)) {
                    $logger->debug("Sanitized value from: " . $orig . " to: " . $value);
                }
            }
        }

        return $value;
    }

    /**
     * @param string $value
     * @return string
     */
    public static function sanitizeUtf8($value) {
        $logger =& LogUtil::getLogger();

        for($step = 1; $step <= 3; $step++) {
            $beforeCount = S::countSubstr($value, "?");

            switch($step) {
                case 1:
                    $sanitized = Encoding::toUTF8($value);
                break;
                case 2:
                    $sanitized = Encoding::UTF8FixWin1252Chars($value);
                break;
                default:
                    $sanitized = Encoding::fixUTF8($value);
                break;
            }

            $afterCount = S::countSubstr($sanitized, "?");

            if($sanitized !== $value) {
                $logger->debug("Sanitize step #" . $step . " - before: " . $value . ", after: " . $sanitized);
            }

            if($beforeCount === $afterCount) {
                $value = $sanitized;
            }
        }

        return $value;
    }

    public static function getAvailableEncodingList() {
        if(self::$availableEncodingList === null) {
            self::$availableEncodingList = implode(",", mb_list_encodings());
        }

        return self::$availableEncodingList;
    }

    /**
     * @param XMLReader $xmlReader
     * @return string
     */
    public static function getTrimmedXmlOrNull(XMLReader &$xmlReader) {
        return self::getTrimmedStringOrNull(self::getXmlReaderText($xmlReader));
    }

    /**
     * @param XMLReader $xmlReader
     * @return string
     */
    public static function getXmlReaderText(XMLReader &$xmlReader) {
        $value = null;

        while($xmlReader->read()) {
            if($xmlReader->nodeType === XMLReader::COMMENT) {
                continue;
            }

            if(
                $xmlReader->nodeType !== XMLReader::TEXT &&
                $xmlReader->nodeType !== XMLReader::CDATA &&
                $xmlReader->nodeType !== XMLReader::WHITESPACE &&
                $xmlReader->nodeType !== XMLReader::SIGNIFICANT_WHITESPACE
            ) {
                break;
            }

            if($value === null) {
                $value = "";
            }

            $value .= $xmlReader->value;
        }

        return $value;
    }

    public static function getDockerContainerId() {
        $fh = fopen("/proc/self/cgroup", "r");

        if($fh) {
            try {
                while(($line = fgets($fh)) !== false) {
                    $line = trim($line);
                    $dockerPos = strpos($line, ":/docker/");

                    if($dockerPos !== false) {
                        return substr($line, $dockerPos + 9);
                    }
                }
            } finally {
                fclose($fh);
            }
        }

        return null;
    }

    /**
     * @param string|null $environmentValue
     * @return array ["protocol" => "tcp", "host" => "172.1.2.3", "port" => "3333"]
     */
    public static function parseDockerPortVariable($environmentValue) {
        $protocol = null;
        $host = null;
        $port = null;

        if(!empty($environmentValue)) {
            $colonPos = strpos($environmentValue, ":");

            if($colonPos !== false) {
                $protocol = (substr($environmentValue, 0, $colonPos) ?: null);
                $slashPos = strrpos($environmentValue, "/");

                if($slashPos !== false) {
                    $portPos = strrpos($environmentValue, ":");

                    if($portPos === false) {
                        $host = (substr($environmentValue, $slashPos + 1) ?: null);
                    } else {
                        $host = (substr($environmentValue, $slashPos + 1, $portPos - $slashPos - 1) ?: null);
                        $port = (substr($environmentValue, $portPos + 1) ?: null);
                    }
                }
            }
        }

        return [
            "protocol" => $protocol,
            "host" => $host,
            "port" => $port
        ];
    }

    /**
     * @param string $json
     * @return object
     */
    public static function decodeJson($json) {
        return json_decode($json, true, 512, JSON_BIGINT_AS_STRING);
    }

    /**
     * @param string|object $json
     * @param string|ReflectionClass|null $class
     * @param array $classArgs
     * @param bool $convertBlanksToNull
     * @return array|object
     * @throws Exception
     */
    public static function &createObjectFromJson($json, $class = null, array $classArgs = [], $convertBlanksToNull = false) {
        $decodedJson = (is_object($json) ? $json : null);

        if($decodedJson === null) {
            $decodedJson = self::decodeJson($json);
        }

        if($class === null) {
            return $decodedJson;
        }

        return self::createCopy($decodedJson, $class, $classArgs, [], $convertBlanksToNull);
    }

    /**
     * @param array|object $original
     * @param string|ReflectionClass|null $class
     * @param array $constructorArgs
     * @param array $excludeKeys
     * @param bool $convertBlanksToNull
     * @return array|object
     * @throws Exception
     */
    public static function &createCopy($original, $class = null, array $constructorArgs = [], array $excludeKeys = [], $convertBlanksToNull = false) {
        $copy = null;

        if($class === null) {
            if(is_object($original)) {
                $class =& self::getReflectionClassByName(get_class($original));
            }
        } else if(is_string($class)) {
            $class =& self::getReflectionClassByName($class);
        } else if(!($class instanceof ReflectionClass)) {
            throw new Exception("Don't know how to create a copy of class: " . self::getVarDump($class));
        }

        if($class === null) {
            if(!is_array($original)) {
                throw new Exception("Couldn't determine appropriate class to create when copying object: " . self::getVarDump($original));
            }

            $copy = [];
        } else {
            $copy = (empty($constructorArgs) ? $class->newInstance() : $class->newInstanceArgs($constructorArgs));
        }

        self::copyProperties($original, $copy, $excludeKeys, $convertBlanksToNull);

        return $copy;
    }

    /**
     * @link http://php.net/manual/en/class.reflectionproperty.php#reflectionproperty.constants.modifiers
     * @param object|array $from
     * @param object|array $to
     * @param array $excludeKeys
     * @param bool $convertBlanksToNull
     * @param int $objectPropertyFilter
     * @throws Exception
     */
    public static function copyProperties($from, $to, array $excludeKeys = [], $convertBlanksToNull = false, $objectPropertyFilter = ReflectionProperty::IS_PRIVATE | ReflectionProperty::IS_PROTECTED | ReflectionProperty::IS_PUBLIC) {
        $fromIsObject = is_object($from);
        $fromIsArray = (!$fromIsObject && is_array($from));

        if(!$fromIsObject && !$fromIsArray) {
            throw new Exception("Invalid type type to copy from: " . self::getVarDump($from));
        }

        $toIsObject = is_object($to);
        $toIsArray = (!$toIsObject && is_array($to));

        if(!$toIsObject && !$toIsArray) {
            throw new Exception("Invalid type to copy to: " . self::getVarDump($to));
        }

        $fromClass = ($fromIsObject ? get_class($from) : null);
        $toClass = ($toIsObject ? get_class($to) : null);
        $toReflection = ($toClass === null ? null : ($toClass === 'stdClass' ? new ReflectionObject($to) : self::getReflectionClassByName($toClass)));
        $fromProperties = (is_array($from) ? $from : []);

        if($fromIsObject) {
            $fromReflectionObject = new ReflectionObject($from);
            $fromReflectionProperties = $fromReflectionObject->getProperties($objectPropertyFilter);

            foreach($fromReflectionProperties as &$fromReflectionProperty) {
                $fromReflectionProperty->setAccessible(true);

                $fromProperties[$fromReflectionProperty->getName()] = $fromReflectionProperty->getValue($from);
            }

            unset($fromReflectionProperty);
        }

        foreach($fromProperties as $key => &$value) {
            if(in_array($key, $excludeKeys, true)) {
                continue;
            }

            if($toIsArray) {
                $to[$key] = $value;
            } else {
                $reflectionProperty = null;

                try {
                    $reflectionProperty = $toReflection->getProperty($key);
                } catch(ReflectionException $re) {
                    if($fromIsObject && $fromClass !== $toClass) {
                        try {
                            $reflectionProperty = $toReflection->getProperty(S::camelize($key));
                        } catch(ReflectionException $re) { }
                    }
                }

                if($reflectionProperty !== null) {
                    $reflectionProperty->setAccessible(true);

                    if($convertBlanksToNull && $value !== null && is_string($value) && S::isBlank($value)) {
                        $reflectionProperty->setValue($to, null);
                    } else {
                        $reflectionProperty->setValue($to, $value);
                    }
                }
            }
        }

        unset($value);
    }

    /**
     * Same as propertiesDifference but simply returns true if there are differences, false if there are not.
     *
     * @link http://php.net/manual/en/class.reflectionproperty.php#reflectionproperty.constants.modifiers
     * @param array|object $a
     * @param array|object $b
     * @param array $excludeKeys
     * @param int $objectPropertyFilter
     * @return bool
     */
    public static function propertiesDiffer($a, $b, array $excludeKeys = [], $objectPropertyFilter = ReflectionProperty::IS_PRIVATE | ReflectionProperty::IS_PROTECTED | ReflectionProperty::IS_PUBLIC) {
        $aProperties = self::getArrayOrObjectProperties($a, $objectPropertyFilter);
        $bProperties = self::getArrayOrObjectProperties($b, $objectPropertyFilter);
        $aCount = count($aProperties);
        $bCount = count($bProperties);

        if($aCount !== $bCount) {
            return true;
        }

        if($aCount > 0) {
            $aKeys = array_keys($aProperties);
            $aKeyCount = count($aKeys);

            for($i = 0; $i < $aKeyCount; $i++) {
                $key =& $aKeys[$i];

                if($excludeKeys !== null && in_array($key, $excludeKeys, true)) {
                    continue;
                }

                $valueA = $aProperties[$key];

                if(!array_key_exists($key, $bProperties)) {
                    return true;
                } else {
                    $valueB = $bProperties[$key];

                    if(self::valuesMatch($valueA, $valueB)) {
                        unset($aProperties[$key]);
                        unset($bProperties[$key]);
                    } else {
                        return true;
                    }
                }
            }
        }

        if($bCount > 0) {
            $bKeys = array_keys($bProperties);
            $bKeyCount = count($bKeys);

            for($i = 0; $i < $bKeyCount; $i++) {
                $key =& $bKeys[$i];

                if($excludeKeys !== null && in_array($key, $excludeKeys, true)) {
                    continue;
                }

                $valueB = $bProperties[$key];

                if(!array_key_exists($key, $aProperties)) {
                    return true;
                } else {
                    $valueA = $aProperties[$key];

                    if(!self::valuesMatch($valueA, $valueB)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Single-depth check of properties using ===.
     * Using == does not enforce types in a reasonable way and === for objects just checks the internal object id.
     *
     * @link http://php.net/manual/en/class.reflectionproperty.php#reflectionproperty.constants.modifiers
     * @param array|object $a
     * @param array|object $b
     * @param array $excludeKeys
     * @param int $objectPropertyFilter
     * @return array
     */
    public static function propertiesDifference($a, $b, array $excludeKeys = [], $objectPropertyFilter = ReflectionProperty::IS_PRIVATE | ReflectionProperty::IS_PROTECTED | ReflectionProperty::IS_PUBLIC) {
        $aProperties = self::getArrayOrObjectProperties($a, $objectPropertyFilter);
        $bProperties = self::getArrayOrObjectProperties($b, $objectPropertyFilter);
        $aCount = count($aProperties);
        $bCount = count($bProperties);
        $hasDifferences = false;
        $differencesA = [];
        $differencesB = [];

        if($aCount > 0) {
            $aKeys = array_keys($aProperties);
            $aKeyCount = count($aKeys);

            for($i = 0; $i < $aKeyCount; $i++) {
                $key =& $aKeys[$i];

                if($excludeKeys !== null && in_array($key, $excludeKeys, true)) {
                    continue;
                }

                $valueA = $aProperties[$key];

                if(!array_key_exists($key, $bProperties)) {
                    $differencesA[$key] = $valueA;
                    $hasDifferences = true;
                } else {
                    $valueB = $bProperties[$key];

                    if(self::valuesMatch($valueA, $valueB)) {
                        unset($aProperties[$key]);
                        unset($bProperties[$key]);
                    } else {
                        $differencesA[$key] = $valueA;
                        $differencesB[$key] = $valueB;
                        $hasDifferences = true;
                    }
                }
            }
        }

        if($bCount > 0) {
            $bKeys = array_keys($bProperties);
            $bKeyCount = count($bKeys);

            for($i = 0; $i < $bKeyCount; $i++) {
                $key =& $bKeys[$i];

                if($excludeKeys !== null && in_array($key, $excludeKeys, true)) {
                    continue;
                }

                $valueB = $bProperties[$key];

                if(!array_key_exists($key, $aProperties)) {
                    $differencesB[$key] = $valueB;
                    $hasDifferences = true;
                } else {
                    $valueA = $aProperties[$key];

                    if(!self::valuesMatch($valueA, $valueB)) {
                        $differencesA[$key] = $valueA;
                        $differencesB[$key] = $valueB;
                        $hasDifferences = true;
                    }
                }
            }
        }

        $result = null;

        if($hasDifferences) {
            $result = [$differencesA, $differencesB];
        }

        return $result;
    }

    /**
     * @link http://php.net/manual/en/class.reflectionproperty.php#reflectionproperty.constants.modifiers
     * @param array|object $val
     * @param int $objectPropertyFilter
     * @return array
     */
    public static function &getArrayOrObjectProperties($val, $objectPropertyFilter) {
        if(is_array($val)) {
            return $val;
        }

        $properties = [];
        $class = get_class($val);
        $reflection = ($class === 'stdClass' ? new ReflectionObject($val) : self::getReflectionClassByName($class));
        $reflectionProperties = $reflection->getProperties($objectPropertyFilter);

        foreach($reflectionProperties as &$reflectionProperty) {
            $reflectionProperty->setAccessible(true);

            $properties[$reflectionProperty->getName()] = $reflectionProperty->getValue($val);
        }

        unset($reflectionProperty);

        return $properties;
    }

    /**
     * Using == does not enforce types in a reasonable way and === for objects just checks the internal object id.
     *
     * @param mixed $a
     * @param mixed $b
     * @return bool
     */
    public static function valuesMatch($a, $b) {
        if($a === $b) {
            return true;
        }

        $typeA = gettype($a);
        $typeB = gettype($b);

        if($typeA === "string") {
            switch($typeB) {
                case "integer":
                    return (intval($a) === $b);
                case "double":
                    return (floatval($a) === $b);
            }
        } else if($typeB === "string") {
            switch($typeA) {
                case "integer":
                    return ($a === intval($b));
                case "double":
                    return ($a === floatval($b));
            }
        }

        return false;
    }

    /**
     * @param ReflectionClass|object|string $object
     * @param callable $func
     * @param string|null $nameStartsWith
     * @param string|null $nameContains
     * @throws Exception
     */
    public static function forEachConstantInObjectClass($object, callable $func, $nameStartsWith = null, $nameContains = null) {
        $reflectionClass = null;

        if($object instanceof ReflectionClass) {
            $reflectionClass =& $object;
        } else if(is_object($object)) {
            $reflectionClass =& self::getReflectionClassByName(get_class($object));
        } else if(is_string($object)) {
            $reflectionClass =& self::getReflectionClassByName($object);
        }

        if($reflectionClass === null) {
            throw new Exception("Couldn't determine class for object: " . self::getVarDump($object));
        }

        $constants = $reflectionClass->getConstants();

        foreach($constants as $constantName => &$constantValue) {
            if($nameStartsWith === null || strpos($constantName, $nameStartsWith) === 0) {
                if($nameContains === null || strpos($constantName, $nameContains) !== false) {
                    $func($object, $constantName, $constantValue);
                }
            }
        }

        unset($constantValue);
    }

    /**
     * @param string $plaintext
     * @param string $keyContents
     * @return string
     */
    public static function encryptWithKey($plaintext, $keyContents) {
        $rsa = new RSA();
        $rsa->setEncryptionMode(RSA::ENCRYPTION_OAEP);
        $rsa->setMGFHash("sha256");

        $rsa->loadKey($keyContents);

        return $rsa->encrypt($plaintext);
    }

    /**
     * @param string $ciphertext
     * @param string $keyContents
     * @return string
     * @throws Exception
     */
    public static function decryptWithKey($ciphertext, $keyContents) {
        $rsa = new RSA();
        $rsa->setEncryptionMode(RSA::ENCRYPTION_OAEP);
        $rsa->setMGFHash("sha256");

        $rsa->loadKey($keyContents);

        $value = $rsa->decrypt($ciphertext);

        if($value === false) {
            throw new Exception("Failed to decrypt");
        }

        return $value;
    }

    /**
     * @param array $excludeChars
     * @param int $min
     * @param int $max
     * @return string
     */
    public static function getRandomPrintableAsciiCharacter(array $excludeChars = [], $min = 32, $max = 126) {
        $asciiCodes = range($min, $max);
        $maxIdx = count($asciiCodes) - 1;
        $result = null;

        while(true) {
            $char = chr($asciiCodes[random_int(0, $maxIdx)]);

            if(in_array($char, $excludeChars) === false) {
                $result = $char;

                break;
            }
        }

        return $result;
    }

    /**
     * @return int
     */
    public static function getUtcTime() {
        return (time() - date('Z'));
    }

    /**
     * @param int $time
     * @return string
     */
    public static function convertUtcTimestampToISO8601($time) {
        if($time !== null) {
            $dateTime = new DateTime("@" . $time);

            return $dateTime->format(DateTime::ATOM);
        }

        return null;
    }

    /**
     * @param string $date
     * @return int
     */
    public static function convertUtcDateToJstTimestamp($date) {
        return self::convertUtcTimestampToJst(self::convertUtcDateToTimestamp($date));
    }

    /**
     * @param $time
     * @return int
     */
    public static function convertUtcTimestampToJst($time) {
        return self::convertUtcTimestampToTimezone($time, new DateTimeZone("Asia/Tokyo"));
    }

    /**
     * @param int $time
     * @param string|DateTimeZone $timezone
     * @return int
     */
    public static function convertUtcTimestampToTimezone($time, $timezone = "UTC") {
        if($time !== null) {
            $dateTime = new DateTime("@" . $time);

            $dateTime->setTimezone(($timezone instanceof DateTimeZone ? $timezone : new DateTimeZone($timezone)));

            return $dateTime->getTimestamp();
        }

        return null;
    }

    /**
     * @param string $date
     * @param string|null $format
     * @return int
     * @throws Exception
     */
    public static function convertUtcDateToTimestamp($date, $format = "Y-m-d H:i:s") {
        return self::convertUtcDateToDateTime($date, $format)->getTimestamp();
    }

    /**
     * @param string $date
     * @param string|null $format
     * @return DateTime
     * @throws Exception
     */
    public static function convertUtcDateToDateTime($date, $format = "Y-m-d H:i:s") {
        if($date !== null) {
            $dateTime = DateTime::createFromFormat($format, $date);

            if($dateTime === false) {
                throw new Exception("Unrecognized date: " . $date . ", expected format: " . $format);
            }

            return $dateTime;
        }

        return null;
    }

    /**
     * @param string $priceUsd
     * @param int|null $time
     * @param PDO|null $connection
     * @return float
     * @throws Exception
     */
    public static function convertUsdToJpy($priceUsd, $time = null, PDO $connection = null) {
        if($time === null) {
            $time = time();
        }

        if($connection === null) {
            $connection =& PDOUtil::getMysqlConnection();
        }

        $exchangeRate =& PDOQuery::select()
            ->column("rate")
            ->from("merchant.currency_frb_h10_usd_to_jpy")
            ->where("time", $time, PDO::PARAM_INT, "<=")
            ->orderBy("time DESC")
            ->limit(1)
            ->getSingleValue($connection)
        ;
        $priceJpy = $priceUsd * $exchangeRate;
        $logger =& LogUtil::getLogger();

        $logger->debug("Converted USD " . $priceUsd . " to JPY " . $priceJpy . " using rate: " . $exchangeRate . " @ time: " . $time);

        return $priceJpy;
    }

    public static function moneyStringToCents($str) {
        $periodIdx = strpos($str, ".");

        if($periodIdx === false) {
            $str .= ".0000";
        } else {
            while(strlen($str) <= ($periodIdx + 4)) {
                $str .= "0";
            }
        }

        return intval(str_replace(".", "", $str));
    }

    public static function moneyCentsToString($cents) {
        $centsStr = "" . $cents;
        $outputStr = null;

        if(strlen($centsStr) < 5) {
            $outputStr = "0." . $centsStr;
        } else {
            $outputStr = substr($centsStr, 0, strlen($centsStr) - 4) . "." . substr($centsStr, strlen($centsStr) - 4);
        }

        $periodIdx = strpos($outputStr, ".");

        while(strlen($outputStr) <= ($periodIdx + 4)) {
            $outputStr .= "0";
        }

        return $outputStr;
    }

    /**
     * @link https://www.mikemackintosh.com/5-tips-for-working-with-ipv6-in-php/
     *
     * @param string $bin
     * @return string
     * @throws Exception
     */
    public static function getIpFromBin($bin) {
        $len = strlen($bin);

        if($len == 16 || $len == 4) {
            $value = inet_ntop(pack("A" . $len, $bin));

            if($value !== false) {
                return $value;
            }
        }

        throw new Exception("Please provide a valid 4 or 16 byte string");
    }

    /**
     * @param string $className
     * @return ReflectionClass
     */
    public static function &getReflectionClassByName($className) {
        $reflectionClass = null;

        if(isset(self::$reflectionClasses[$className])) {
            $reflectionClass =& self::$reflectionClasses[$className];
        } else {
            $reflectionClass = new ReflectionClass($className);

            self::$reflectionClasses[$className] =& $reflectionClass;
        }

        return $reflectionClass;
    }

    /**
     * @return string|null
     */
    public static function getCallingFunctionName() {
        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 3);

        if(isset($backtrace[2]["function"])) {
            return $backtrace[2]["function"];
        }

        return null;
    }

    /**
     * @return string
     */
    public static function getRunningScriptPath() {
        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        $count = count($backtrace);
        $file = $backtrace[$count - 1]["file"];

        return $file;
    }

    /**
     * @return string
     */
    public static function getVarDump() {
        ob_start();
        call_user_func_array("var_dump", func_get_args());
        return ob_get_clean();
    }

    /**
     * @param mixed $value
     * @return int
     */
    public static function getStringByteLength($value) {
        if($value === null) {
            return 0;
        }

        if(!is_scalar($value) && !(is_object($value) && method_exists($value, "__toString"))) {
            throw new UnexpectedTypeException($value, "string");
        }

        $stringValue = (string)$value;

        return (ini_get("mbstring.func_overload") ? mb_strlen($stringValue, "8bit") : strlen($stringValue));
    }

    /**
     * @param mixed $value
     * @param int $maxBytes
     * @return string
     */
    public static function getStringTruncatedToByteLength($value, $maxBytes) {
        while(self::getStringByteLength($value) > $maxBytes) {
            $length = S::length($value);
            $value = (string)S::truncate($value, ($length - 1));
        }

        return $value;
    }

    public static function isIntegerValue($checkValue) {
        if($checkValue !== null) {
            if(is_int($checkValue)) {
                return true;
            } else if(is_string($checkValue)) {
                $length = S::length($checkValue);

                if($length > 1 && ((string)S::substr($checkValue, 0, 1)) == "-") {
                    $checkValue = (string)S::substr($checkValue, 1);
                }

                if(ctype_digit($checkValue)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static function isFloatValue($checkValue) {
        if($checkValue !== null) {
            $type = gettype($checkValue);

            switch($type) {
                case "integer":
                case "double":
                    return true;
                case "string":
                    $length = S::length($checkValue);

                    if($length > 1 && ((string)S::substr($checkValue, 0, 1)) == "-") {
                        $checkValue = (string)S::substr($checkValue, 1);
                    }

                    if(ctype_digit($checkValue) || preg_match("/^\\d+\\.\\d+$/", $checkValue) === 1) {
                        return true;
                    }
                break;
            }
        }

        return false;
    }

    /**
     * @link http://stackoverflow.com/questions/173400/how-to-check-if-php-array-is-associative-or-sequential
     *
     * @param mixed $array
     * @return bool
     */
    public static function isAssociativeArray($array) {
        if(is_array($array)) {
            for(reset($array); is_int(key($array)); next($array));

            return is_null(key($array));
        }

        return false;
    }

    /**
     * @param mixed $array
     * @return bool
     */
    public static function isNonEmptyArray($array) {
        if($array !== null && is_array($array) && count($array) > 0) {
            return true;
        }

        return false;
    }

    /**
     * @param bool|string|int|null $value
     * @return bool
     * @throws Exception
     */
    public static function getBooleanOrNull($value) {
        if($value === null) {
            return null;
        }

        if(is_bool($value)) {
            return $value;
        }

        if(is_string($value)) {
            $upper = (string)S::toUpperCase($value);

            if($upper === "TRUE" || $upper === "T") {
                return true;
            }

            if($upper === "FALSE" || $upper === "F") {
                return false;
            }

            if($upper === "YES" || $upper === "Y") {
                return true;
            }

            if($upper === "NO" || $upper === "N") {
                return false;
            }

            if($upper === "ON") {
                return true;
            }

            if($upper === "OFF") {
                return false;
            }
        } else if(is_int($value)) {
            if($value === 1) {
                return true;
            } else if($value === 0) {
                return false;
            }
        }

        throw new Exception("Value cannot be converted to boolean: " . self::getVarDump($value));
    }

    /**
     * @param string $value
     * @return string
     */
    public static function getTrimmedStringOrNull($value) {
        if($value !== null && S::length($value) > 0) {
            $value = S::trim($value);

            if(S::length($value) > 0) {
                return (string)$value;
            }
        }

        return null;
    }

    /**
     * @param string $value
     * @return string
     */
    public static function getStringOrNull($value) {
        if($value !== null && S::length($value) > 0 && !S::isBlank($value)) {
            return $value;
        }

        return null;
    }

    /**
     * Replicate Java's System.currentTimeMillis()
     * @return float
     */
    public static function getCurrentTimeMillis() {
        return round(microtime(true) * 1000);
    }
}
