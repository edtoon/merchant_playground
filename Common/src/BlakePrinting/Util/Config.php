<?php

namespace BlakePrinting\Util;

use Stringy\StaticStringy as S;
use Exception;

class Config {

    private $config = [];

    public function __construct(array &$config = []) {
        $this->config =& $config;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function &store() {
        $argCount = func_num_args();
        $args = func_get_args();

        if($argCount > 0) {
            $arg = $args[0];

            if(is_array($arg)) {
                if($argCount == 1) {
                    call_user_func_array([$this, __FUNCTION__], $arg);
                } else {
                    throw new Exception("Tried to store config with an array as the root category - args were: " . Utils::getVarDump($args));
                }
            } else if($argCount == 1) {
                throw new Exception("No value passed to store under key: " . $arg);
            } else if($argCount == 2) {
                $this->config[$arg] =& $args[1];
            } else {
                $category =& $this->config;
                $lastCategoryIdx = ($argCount - 2);
                $i = 0;

                while(true) {
                    if(!is_scalar($arg)) {
                        throw new Exception("Tried to store config with a non-scalar category - args were: " . Utils::getVarDump($args));
                    }

                    if(!isset($category[$arg])) {
                        $category[$arg] = [];
                    }

                    $category =& $category[$arg];

                    $i++;

                    if(!is_array($category)) {
                        throw new Exception(
                            "Encountered a non-array category when trying to store config - " .
                            "location was: " . Utils::getVarDump(array_slice($args, 0, $i)) .
                            ", args were: " . Utils::getVarDump($args)
                        );
                    }

                    $arg =& $args[$i];

                    if($i == $lastCategoryIdx) {
                        $category[$arg] =& $args[($argCount - 1)];
                        break;
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @throws Exception
     */
    public function &retrieve() {
        $argCount = func_num_args();
        $args = func_get_args();
        $value = null;

        if($argCount > 0) {
            $arg = $args[0];

            if(is_array($arg)) {
                if($argCount == 1) {
                    call_user_func_array([$this, __FUNCTION__], $arg);
                } else {
                    throw new Exception("Tried to retrieve config with an array as the root category - args were: " . Utils::getVarDump($args));
                }
            } else {
                $category =& $this->config;
                $i = 0;

                while(true) {
                    if(!is_scalar($arg)) {
                        throw new Exception("Tried to retrieve config with a non-scalar category - args were: " . Utils::getVarDump($args));
                    }

                    if(!isset($category[$arg])) {
                        break;
                    }

                    $category =& $category[$arg];

                    $i++;

                    if($i == $argCount) {
                        $value =& $category;

                        break;
                    }

                    if(!is_array($category)) {
                        throw new Exception(
                            "Encountered a non-array category when trying to retrieve config - " .
                            "location was: " . Utils::getVarDump(array_slice($args, 0, $i)) .
                            ", args were: " . Utils::getVarDump($args)
                        );
                    }

                    $arg =& $args[$i];
                }
            }
        }

        return $value;
    }

    /**
     * @param string $name
     */
    public function &__call($name, array $arguments = []) {
        if(!empty($this->config) && strlen($name) >= 4) {
            $cmd = substr($name, 0, 3);
            $configProperty = (string)S::underscored(substr($name, 3));

            if($cmd == "get") {
                if(array_key_exists($configProperty, $this->config)) {
                    return $this->config[$configProperty];
                }

                $value = null;

                return $value;
            } else if($cmd == "set") {
                if(is_array($arguments) && count($arguments) == 1) {
                    $this->config[$configProperty] =& $arguments[0];
                } else {
                    $this->config[$configProperty] =& $arguments;
                }

                return $this;
            }
        }

        $value = false;

        return $value;
    }
}
