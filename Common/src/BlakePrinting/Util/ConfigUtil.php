<?php

namespace BlakePrinting\Util;

use Exception;
use Symfony\Component\Yaml\Parser;

class ConfigUtil {
    const DEFAULT_APP = "Admin";
    const DEFAULT_DIR = "/app";
    const ENV_DEV = "dev";
    const ENV_PROD = "prod";

    /** @var string */
    private static $appDir = null;
    /** @var Config */
    private static $instance = null;
    /** @var Parser */
    private static $parser = null;
    /** @var string */
    private static $env = null;

    private function __construct() { }

    public static function getConfigFileNames() {
        return ["config." . self::getEnvironmentName() . ".yml", "config.yml"];
    }

    /**
     * @param string $env
     */
    public static function setEnvironmentName($env) {
        self::$env = $env;
    }

    /**
     * @return string
     */
    public static function getEnvironmentName() {
        if(empty(self::$env)) {
            self::$env = getenv("MERCHANT_ENV");

            if(empty(self::$env)) {
                throw new Exception("Could not find merchant environment property");
            }
        }

        return self::$env;
    }

    /**
     * @param string $appDir
     */
    public static function setAppDir($appDir = null) {
        self::$appDir =& $appDir;
    }

    /**
     * @return string
     */
    public static function &getAppDir() {
        if(self::$appDir === null) {
            self::configureAppDir();
        }

        return self::$appDir;
    }

    public static function getVendorDir() {
        $appDir = self::getAppDir();
        $checkDir = $appDir;

        do {
            $vendorDir = $checkDir . DIRECTORY_SEPARATOR . "vendor";

            if(is_dir($vendorDir)) {
                return $vendorDir;
            }

            $checkDir = realpath($checkDir . DIRECTORY_SEPARATOR . "..");
        } while(strlen($checkDir) > 1);

        throw new Exception("Can't find vendor directory");
    }

    public static function setConfig(Config &$config = null) {
        self::$instance =& $config;
    }

    /**
     * @param bool $refresh
     * @return Config
     */
    public static function &getConfig($refresh = false) {
        if(self::$instance === null || $refresh === true) {
            self::$instance =& self::createFromDirectory();
        }

        return self::$instance;
    }

    /**
     * @param String $dir
     * @return Config
     * @throws Exception
     */
    public static function &createFromDirectory($dir = null) {
        if($dir === null) {
            return self::createFromFiles();
        } else if(!is_dir($dir)) {
            throw new Exception("No such directory: " . $dir);
        }

        $files = [];

        foreach(self::getConfigFileNames() as &$configFileName) {
            $files[] = $dir . DIRECTORY_SEPARATOR . $configFileName;
        }

        unset($configFileName);

        return self::createFromFiles($files);
    }

    /**
     * @param string $file
     * @return Config
     * @throws Exception
     */
    public static function &createFromFile($file = null)
    {
        return self::createFromFiles([$file]);
    }

    /**
     * @param string $file
     * @return Config
     * @throws Exception
     */
    public static function &createFromFiles(array $files = []) {
        $configFileNames = self::getConfigFileNames();

        if(empty($files)) {
            foreach($configFileNames as &$configFileName) {
                $files[] = self::getAppDir() . DIRECTORY_SEPARATOR . $configFileName;
            }

            unset($configFileName);
        }

        foreach($files as &$file) {
            $dir = dirname($file);

            foreach($configFileNames as &$configFileName) {
                $candidateFile = $dir . DIRECTORY_SEPARATOR . $configFileName;

                if(in_array($candidateFile, $files) === false) {
                    $files[] = $candidateFile;
                }

                if(substr($dir, 0, 4) == self::DEFAULT_DIR) {
                    while($dir != self::DEFAULT_DIR && !empty($dir)) {
                        $parentDir = dirname($dir);
                        $candidateDefaultFile = $parentDir . DIRECTORY_SEPARATOR . $configFileName;

                        if(in_array($candidateDefaultFile, $files) === false) {
                            $files[] = $candidateDefaultFile;
                        }

                        if($parentDir == self::DEFAULT_DIR) {
                            break;
                        }

                        $dir = $parentDir;
                    }
                }
            }

            unset($configFileName);
        }

        unset($file);

        $yamlConfig = null;

        foreach($files as &$readFile) {
            if(file_exists($readFile)) {
                $config = self::getParser()->parse(file_get_contents($readFile));

                if($yamlConfig === null) {
                    $yamlConfig = $config;
                } else {
                    $yamlConfig = array_replace_recursive($config, $yamlConfig);
                }
            }
        }

        unset($readFile);

        $instance = new Config($yamlConfig);

        return $instance;
    }

    /**
     * @param string $yamlString
     * @return Config
     */
    public static function &createFromString($yamlString = null) {
        if(empty($yamlString)) {
            $instance = new Config();

            return $instance;
        }

        $yamlConfig = self::getParser()->parse($yamlString);
        $instance = new Config($yamlConfig);

        return $instance;
    }

    private static function &getParser() {
        if(self::$parser === null) {
            self::$parser = new Parser();
        }

        return self::$parser;
    }

    private static function configureAppDir() {
        $appDir = null;

        if(isset($GLOBALS["__PHPUNIT_CONFIGURATION_FILE"])) {
            $testDir = dirname($GLOBALS["__PHPUNIT_CONFIGURATION_FILE"]);

            $appDir = dirname($testDir);
        } else {
            $scriptPath = realpath(Utils::getRunningScriptPath());

            if(substr($scriptPath, 0, 4) == self::DEFAULT_DIR) {
                $dir = dirname($scriptPath);

                while($dir != self::DEFAULT_DIR) {
                    $parentDir = dirname($dir);

                    if($parentDir == self::DEFAULT_DIR) {
                        break;
                    }

                    $dir = $parentDir;
                }

                $appDir = $dir;
            } else {
                $app = getenv("MERCHANT_APP");

                if(!empty($app)) {
                    $appDir = self::DEFAULT_DIR . DIRECTORY_SEPARATOR . $app;
                } else if(substr($scriptPath, 0, 13) != "/var/www/html") {
                    $appDir = dirname($scriptPath);
                } else {
                    $appDir = self::DEFAULT_DIR . DIRECTORY_SEPARATOR . self::DEFAULT_APP;
                }
            }
        }

        self::setAppDir($appDir);
    }
}
