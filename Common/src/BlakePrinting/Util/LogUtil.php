<?php

namespace BlakePrinting\Util;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Exception;

class LogUtil {
    protected static $instance;

    private function __construct() { }

    /**
     * @param string $message
     */
    public static function logException($level, Exception $e, $message = null, $depth = 0) {
        $logger =& self::getLogger();
        $messageIsEmpty = empty($message);

        if(!$messageIsEmpty) {
            $logger->log($level, "Caught exception - " . $message);
        }

        $depth++;

        $logger->log($level,
            (($messageIsEmpty) ? "Caught exception of" : ("Exception[" . $depth . "]")) .
            " class: [" . get_class($e) . "], code: [" . $e->getCode() . "], file: [" . $e->getFile() . "]" .
            ", line: [" . $e->getLine() . "], message: [" . $e->getMessage() . "], trace: [" . $e->getTraceAsString() . "]"
        );

        if($e->getPrevious() !== null) {
            self::logException($level, $e->getPrevious(), null, $depth);
        }
    }

    /**
     * @return Logger
     */
    public static function &getLogger() {
        if(!self::$instance) {
            self::configureLogger();
        }

        return self::$instance;
    }

    /**
     * @param string $category
     */
    public static function configureLogger($category = null) {
        if(empty($category)) {
            $category = "BlakePrinting";
        }

        $instance = new Logger($category);

        $instance->pushHandler(new StreamHandler("php://stderr", Logger::DEBUG));

        self::$instance =& $instance;
    }
}
