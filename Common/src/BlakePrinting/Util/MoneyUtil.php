<?php

namespace BlakePrinting\Util;

use Stringy\StaticStringy as S;

class MoneyUtil {
    private function __construct() { }

    /**
     * @param string $str
     * @return string
     */
    public static function stripThousandsSeparator($str) {
        if($str !== null) {
            $lastComma = S::indexOfLast($str, ",");
            $lastPeriod = S::indexOfLast($str, ".");

            if($lastComma !== false && $lastPeriod !== false) {
                if($lastComma < $lastPeriod) {
                    $str = (string)S::replace($str, ",", "");
                    $lastComma = false;
                    $lastPeriod = S::indexOfLast($str, ".");
                } else {
                    $str = (string)S::replace($str, ".", "");
                    $lastComma = S::indexOfLast($str, ",");
                    $lastPeriod = false;
                }
            }

            if($lastComma !== false) {
                $str = (
                    (string)S::replace((string)S::substr($str, 0, $lastComma), ",", "") .
                    (string)S::substr($str, $lastComma)
                );
            }

            if($lastPeriod !== false) {
                $str = (
                    (string)S::replace((string)S::substr($str, 0, $lastPeriod), ".", "") .
                    (string)S::substr($str, $lastPeriod)
                );
            }
        }

        return $str;
    }

    /**
     * @param string $str
     * @param int $decimals
     * @return string
     */
    public static function toCents($str, $decimals = 2) {
        if($str === null) {
            return null;
        }

        $str = self::stripThousandsSeparator($str);
        $length = S::length($str);
        $separatorIdx = S::indexOfLast($str, ",");
        $separatorIdx = ($separatorIdx === false ? S::indexOfLast($str, ".") : $separatorIdx);

        if($separatorIdx !== false) {
            $str = (
                ((string)S::substr($str, 0, $separatorIdx)) .
                ($separatorIdx === ($length - 1) ? "" : ($decimals < 1 ? "" : (string)S::substr($str, $separatorIdx + 1, $decimals)))
            );

            while(S::length($str) < ($separatorIdx + $decimals)) {
                $str .= "0";
            }
        } else if(S::length((string)S::replace($str, "0", "")) > 0) {
            for($i = 0; $i < $decimals; $i++) {
                $str .= "0";
            }
        }

        return intval($str);
    }

    /**
     * @param int $cents
     * @param int $decimals
     * @param string $separator
     * @return string
     */
    public static function fromCents($cents, $decimals = 2, $separator = ".") {
        if($cents === null) {
            return null;
        }

        if($decimals < 1) {
            return "" . $cents;
        }

        $centsStr = "" . $cents;
        $negative = false;

        if($centsStr[0] === "-") {
            $centsStr = substr($centsStr, 1);
            $negative = true;
        }

        $length = strlen($centsStr);
        $outputStr = "";

        if($length <= $decimals) {
            $requiredPadding = $decimals - $length;
            $outputStr = "0" . $separator;

            for($i = 0; $i < $requiredPadding; $i++) {
                $outputStr .= "0";
            }

            $outputStr .= $centsStr;
        } else {
            for($i = 0; $i < $length; $i++) {
                if($i === ($length - $decimals)) {
                    $outputStr .= $separator;
                }

                $outputStr .= $centsStr[$i];
            }
        }

        return ($negative ? "-" : "") . $outputStr;
    }
}
