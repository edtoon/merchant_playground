<?php

use BlakePrinting\Test\AbstractTestCase;
use BlakePrinting\Util\Utils;

class UtilsTest extends AbstractTestCase {
    public function testPropertiesDiffs() {
        $this->assertFalse(Utils::propertiesDiffer([], []));
        $this->assertFalse(Utils::propertiesDiffer(["a", "b", "c"], ["a", "b", "c"]));
        $this->assertFalse(Utils::propertiesDiffer(["x" => "y", 3 => null], ["x" => "y", 3 => null]));
        $this->assertFalse(Utils::propertiesDiffer([13], [13]));
        $this->assertTrue(Utils::propertiesDiffer([], [1]));
        $this->assertEmpty(Utils::propertiesDifference([], []));
        $this->assertEmpty(Utils::propertiesDifference(["fdsa" => 3, "joe" => "schmoe"], ["joe" => "schmoe"], ["fdsa"]));
        $this->assertNotEmpty(Utils::propertiesDifference(["fdsa" => 3, "joe" => "schmoe"], ["joe" => "schmoe", 6]));
    }
}
