<?php

use BlakePrinting\Util\ConfigUtil;
use BlakePrinting\Test\AbstractTestCase;

class ConfigUtilTest extends AbstractTestCase {
    public function testBaseConfig() {
        ConfigUtil::setAppDir(dirname(__DIR__));
        $config =& ConfigUtil::getConfig(true);
        $database =& $config->retrieve("database", "default");
        $this->assertEquals("merchant", $database);
        $schema =& $config->retrieve("database", $database, "schemas", "default");
        $this->assertEquals("merchant", $schema);
        $schemaName =& $config->retrieve("database", $database, "schemas", $schema, "name");
        $this->assertEquals("merchant", $schemaName);
        $this->assertEquals("merchant", $config->retrieve("database", $database, "username"));
        $this->assertEquals("merchant", $config->retrieve("database", $database, "password"));
        $this->assertEquals("utf8mb4", $config->retrieve("database", $database, "charset"));
        $this->assertEquals("utf8mb4_bin", $config->retrieve("database", $database, "collation"));
    }

    public function testMergeConfig() {
        ConfigUtil::setAppDir(__DIR__);
        $config =& ConfigUtil::getConfig(true);
        $database =& $config->retrieve("database", "default");
        $schema =& $config->retrieve("database", $database, "schemas", "default");
        $this->assertEquals("test", $config->retrieve("database", $database, "username"));
        $this->assertEquals("FooBar", $config->retrieve("database", $database, "schemas", "foo", "name"));
        $this->assertEquals("test", $config->retrieve("database", $database, "schemas", $schema, "name"));
    }

    public function testSettersAndGetters() {
        $config =& ConfigUtil::createFromString("foo: bar\nsection:\n    section-entry: SecVal\n");
        $this->assertEquals("bar", $config->getFoo());
        $this->assertEquals("SecVal", $config->getSection()["section-entry"]);
        $config->setNada();
        $this->assertEmpty($config->getNada());
        $config->setBaz("bat");
        $this->assertEquals("bat", $config->getBaz());
        $config->setBam("bang", "wham");
        $this->assertEquals("wham", $config->getBam()[1]);
        $config->setBoom(["make", "room"]);
        $this->assertEquals("room", $config->getBoom()[1]);
        $config->setNo(["okbro"]);
        $this->assertEquals(["okbro"], $config->getNo());
        $config->setFluent("value")->setAnother(["fluent" => "value2"]);
        $this->assertEquals("value", $config->getFluent());
        $this->assertEquals("value2", $config->getAnother()["fluent"]);
        $config->setBoolz(true);
        $this->assertTrue($config->getBoolz() === true);
        $config->setBowlz(false);
        $this->assertTrue($config->getBowlz() === false);
    }

    public function testStore() {
        $config =& ConfigUtil::createFromString("empty: false\n");
        $config->store();
        try {
            $config->store("foo");
            $this->fail("Store with category but no value should throw an exception");
        } catch(Exception $e) { }
        $this->assertNull($config->getStore());
        $config->store("foo", "bar");
        $this->assertEquals("bar", $config->getFoo());
        $config->store("by", "us", "f", "u");
        $this->assertEquals("u", $config->getBy()["us"]["f"]);
        $config->store("ooh", true);
        $this->assertTrue($config->getOoh() === true);
        $config->store("boo", false);
        $this->assertTrue($config->getBoo() === false);
        $config->store(["l1", "l2", "l3", "l4", "l5"]);
        $this->assertEquals("l5", $config->getL1()["l2"]["l3"]["l4"]);
        try {
            $config->store(["badroot1", "badroot2", "badroot3"], "a", "b", "c");
            $this->fail("Store with array as root category should throw an exception");
        } catch(Exception $e) { }
        try {
            $config->store("good1", "good2", ["bad", "stuff", "here"], "boo");
            $this->fail("Store with a non-scalar as a category should throw an exception");
        } catch(Exception $e) { }
        $this->assertEquals([], $config->getGood1()["good2"]);
        $config->store("i", "am", "a", "meanie");
        try {
            $config->store("i", "am", "a", "meanie", "head");
            $this->fail("Store with a non-array key in category hierarchy should throw an exception");
        } catch(Exception $e) { }
    }

    public function testRetrieve() {
        $config =& ConfigUtil::createFromString("empty: true\n");
        $config->retrieve();
        $this->assertTrue($config->retrieve("empty") === true);
        try {
            $config->retrieve("foo");
            $this->fail("Retrieve with a missing category should throw an exception");
        } catch(Exception $e) { }
        $config->store("foo", "bar", "baz");
        try {
            $config->retrieve("foo", "bar", "baz");
            $this->fail("Retrieve with a non-array in category hierarchy should throw an exception");
        } catch(Exception $e) { }
    }
}
