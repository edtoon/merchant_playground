<?php

use BlakePrinting\Db\PDOQuery;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Test\AbstractDbTestCase;

class PDOQueryTest extends AbstractDbTestCase {
    public function testSelect() {
        $this->assertEquals("SELECT *", PDOQuery::select()->generate());
        $this->assertEquals("SELECT *", PDOQuery::select()->column()->generate());
        $this->assertEquals("SELECT *", PDOQuery::select()->columns()->generate());
        $this->assertEquals("SELECT *", PDOQuery::select()->column(null)->generate());
        $this->assertEquals("SELECT *", PDOQuery::select()->columns(null)->generate());
        $this->assertEquals("SELECT *", PDOQuery::select()->columns("")->generate());
        $this->assertEquals("SELECT *", PDOQuery::select()->columns("*")->generate());
        $this->assertEquals("SELECT id", PDOQuery::select()->column("id")->generate());
        $this->assertEquals("SELECT id", PDOQuery::select()->columns("id")->generate());
        $this->assertEquals("SELECT id, name", PDOQuery::select()->column("id")->column("name")->generate());
        $this->assertEquals("SELECT DISTINCT name", PDOQuery::select()->distinct()->column("name")->generate());
        $this->assertEquals("SELECT NULL", PDOQuery::select()->column("NULL")->generate());
        $this->assertEquals("SELECT NULL", PDOQuery::select()->columns("NULL")->generate());
        $this->assertEquals("SELECT NULL AS id", PDOQuery::select()->column(["NULL" => "id"])->generate());
        $this->assertEquals("SELECT NULL AS id", PDOQuery::select()->columns(["NULL" => "id"])->generate());
        $this->assertEquals("SELECT 0", PDOQuery::select()->columns("0")->generate());
        $this->assertEquals("SELECT 0", PDOQuery::select()->columns(0)->generate());
        // numeric strings in associative array keys are always converted to int :(
        $this->assertEquals("SELECT zilch", PDOQuery::select()->columns(["0" => "zilch"])->generate());
        $this->assertEquals("SELECT zilch", PDOQuery::select()->columns([0 => "zilch"])->generate());
        $this->assertEquals("SELECT 0, 1, 7, 9", PDOQuery::select()->columns(0, 1, 7, 9)->generate());
        $this->assertEquals("SELECT a, b, c", PDOQuery::select()->columns("a", "b", "c")->generate());
        $this->assertEquals("SELECT a, 1, 'steak', sauce", PDOQuery::select()->columns("a", 1, "'steak'", "sauce")->generate());
        $this->assertEquals("SELECT 'john' AS login", PDOQuery::select()->columns("'john' AS login")->generate());
        $this->assertEquals("SELECT 1, 2", PDOQuery::select()->columns(["1", "2"])->generate());
        $this->assertEquals("SELECT 3, 4", PDOQuery::select()->columns([3, 4])->generate());
        $this->assertEquals("SELECT 'bar' AS foo", PDOQuery::select()->columns(["'bar'" => "foo"])->generate());
        $this->assertEquals(
            "SELECT id, 'jim' AS name",
            PDOQuery::select()->column(["3" => "id"])->column(["'jim'" => "name"])->generate()
        );
        $this->assertEquals(
            "SELECT id, 'jim' AS name",
            PDOQuery::select()->columns(["3" => "id"])->columns(["'jim'" => "name"])->generate()
        );
        $this->assertEquals(
            "SELECT nextval('my_table_id_seq'::regclass) AS id",
            PDOQuery::select()->columns(["nextval('my_table_id_seq'::regclass)" => "id"])->generate()
        );
        $this->assertEquals(
            "SELECT id FROM ((select nextval('my_table_id_seq'::regclass) AS id) next_id)",
            PDOQuery::select()->columns("id")->from("(select nextval('my_table_id_seq'::regclass) AS id) next_id")->generate()
        );
        $this->assertEquals(
            "SELECT rand() AS id, 'john' AS login",
            PDOQuery::select()->columns(["rand()" => "id", "'john'" => "login"])->generate()
        );
        $this->assertEquals(
            "SELECT uuid() AS id2",
            PDOQuery::select()->columns([PDOQuery::DATA_ATTR_SPEC => "uuid()", PDOQuery::DATA_ATTR_ALIAS => "id2"])->generate()
        );
        $this->assertEquals(
            "SELECT id, uuid() AS uuid, name",
            PDOQuery::select()->columns(
                [
                    "id",
                    [PDOQuery::DATA_ATTR_SPEC => "uuid()", PDOQuery::DATA_ATTR_ALIAS => "uuid"],
                    "name"
                ]
            )->generate()
        );
        $this->assertEquals(
            "SELECT id, uuid() AS uuid, name",
            PDOQuery::select()->columns(
                "id",
                [PDOQuery::DATA_ATTR_SPEC => "uuid()", PDOQuery::DATA_ATTR_ALIAS => "uuid"],
                "name"
            )->generate()
        );
    }

    public function testSelectOptions() {
        $this->assertEquals("SELECT id FOR UPDATE", PDOQuery::select()->columns("id")->options("FOR UPDATE")->generate());
    }

    public function testSelectFrom() {
        $this->assertEquals(
            "SELECT name FROM (users AS usr_tbl)",
            PDOQuery::select()->column("name")->from(["users" => "usr_tbl"])->generate()
        );
        $this->assertEquals(
            "SELECT rand() AS id FROM dual",
            PDOQuery::select()->columns(["rand()" => "id"])->from("dual")->generate()
        );
        $this->assertEquals(
            "SELECT * FROM (users, groups, passwords)",
            PDOQuery::select()->from(["users", "groups", "passwords"])->generate()
        );
        $this->assertEquals(
            "SELECT a, b FROM (foo, bar)",
            PDOQuery::select()->columns("a", "b")->from("foo", "bar")->generate()
        );
        $this->assertEquals(
            "SELECT id FROM ((SELECT nextval('users_id_seq'::regclass) AS id) next_id)",
            PDOQuery::select()->columns("id")->from("(SELECT nextval('users_id_seq'::regclass) AS id) next_id")->generate()
        );
    }

    public function testWhere() {
        $this->assertEquals(
            "SELECT 1 FROM dual WHERE 2 = 2",
            PDOQuery::select()->columns("1")->from("dual")->where("2", "2")->generate()
        );
        $this->assertEquals(
            "SELECT * FROM (users) WHERE deleted_date IS NOT NULL AND id BETWEEN 5 and 7",
            PDOQuery::select()->from("users")
                ->where("deleted_date", "NULL", null, "IS NOT")
                ->where([PDOQuery::DATA_ATTR_SPEC => "id BETWEEN 5 and 7"])
                ->generate()
        );
        $this->assertEquals(
            "SELECT login FROM (users) WHERE id < ? AND UPPER(fname) LIKE 'J%'",
            PDOQuery::select()->columns("login")
                ->from("users")
                ->where("id", 3, PDO::PARAM_INT, "<")
                ->where("UPPER(fname)", "'J%'", null, "LIKE")
                ->generate()
        );
        $this->assertEquals(
            "SELECT 1 FROM dual WHERE 'foo' IN ('bar', 'baz')",
            PDOQuery::select()->columns("1")->from("dual")->where("'foo'", "('bar', 'baz')", null, "IN")->generate()
        );
        $this->assertEquals(
            "SELECT id FROM (users) WHERE UPPER(login) = UPPER(?)",
            PDOQuery::select()->columns("id")->from("users")->where("UPPER(login)", "zzz", PDO::PARAM_STR, "=", "UPPER")->generate()
        );
        $this->assertEquals(
            "SELECT id FROM (users) WHERE date_deleted IS NULL",
            PDOQuery::select()->columns("id")->from("users")->whereNull("date_deleted")->generate()
        );
        $this->assertEquals(
            "SELECT id FROM (users) WHERE date_activated IS NOT NULL",
            PDOQuery::select()->columns("id")->from("users")->whereNotNull("date_activated")->generate()
        );
        $this->assertEquals(
            "SELECT id FROM (refund_requests) WHERE NOT EXISTS (SELECT 1 FROM accepted_refunds WHERE refund_request_id = id) ORDER BY id ASC LIMIT 1",
            PDOQuery::select()->columns("id")->from("refund_requests")
                ->where([PDOQuery::DATA_ATTR_SPEC => "NOT EXISTS (SELECT 1 FROM accepted_refunds WHERE refund_request_id = id)"])
                ->orderBy("id ASC")->limit(1)->generate()
        );
    }

    public function testLeftJoin() {
        $this->assertEquals(
            "SELECT id, COALESCE(callback_generated_id, immediate_id) AS response_id " .
            "FROM (request) " .
            "LEFT JOIN callback_request " .
            "ON request.method = 'callback' " .
            "AND request.id = callback_request.request_id",
            PDOQuery::select()
                ->columns([
                    "id", [PDOQuery::DATA_ATTR_SPEC => "COALESCE(callback_generated_id, immediate_id)", PDOQuery::DATA_ATTR_ALIAS => "response_id"]
                ])->from("request")
                ->leftJoin("callback_request", [
                    "request.method = 'callback'", "request.id = callback_request.request_id"
                ])
                ->generate()
        );

        $this->assertEquals(
            "SELECT req.id, COALESCE(cb.callback_generated_id, req.immediate_id) AS response_id " .
            "FROM (request AS req) " .
            "LEFT JOIN callback_request AS cb " .
            "ON req.method = 'callback' " .
            "AND req.id = cb.request_id",
            PDOQuery::select()
                ->columns([
                    "req.id", [PDOQuery::DATA_ATTR_SPEC => "COALESCE(cb.callback_generated_id, req.immediate_id)", PDOQuery::DATA_ATTR_ALIAS => "response_id"]
                ])->from("request AS req")
                ->leftJoin("callback_request AS cb", [
                    "req.method = 'callback'", "req.id = cb.request_id"
                ])
                ->generate()
        );

        $this->assertEquals(
            "SELECT request.id, request.report_request_id, request.generated_report_id " .
            "FROM ((report_request request, report_request_retrieve retrieve, report_request_marketplace_instance request_mktpl)) " .
            "LEFT JOIN report report " .
            "ON request.report_request_id = report.report_request_id " .
            "AND report.acknowledged_date IS NULL " .
            "AND (report.acknowledged IS NULL OR report.acknowledged = 0) " .
            "AND EXISTS (" .
                "SELECT 1 FROM report_marketplace_instance report_mktpl " .
                "WHERE report_mktpl.report_id = report.id " .
                "AND report_mktpl.marketplace_instance_id = request_mktpl.marketplace_instance_id" .
            ") " .
            "WHERE request.id = retrieve.report_request_id " .
            "AND request.report_processing_status = '_DONE_' " .
            "AND request.updated IS NOT NULL " .
            "AND request_mktpl.marketplace_instance_id = ? " .
            "AND (request.generated_report_id IS NOT NULL OR report.report_id IS NOT NULL) " .
            "AND (retrieve.updated IS NULL OR retrieve.updated <= request.updated)",
            PDOQuery::select()
                ->columns(["request.id", "request.report_request_id", "request.generated_report_id"])
                ->from([PDOQuery::DATA_ATTR_SPEC => "(report_request request, report_request_retrieve retrieve, report_request_marketplace_instance request_mktpl)"])
                ->leftJoin("report report", [
                    "request.report_request_id = report.report_request_id",
                    "report.acknowledged_date IS NULL",
                    "(report.acknowledged IS NULL OR report.acknowledged = 0)",
                    "EXISTS (" .
                        "SELECT 1 FROM report_marketplace_instance report_mktpl " .
                        "WHERE report_mktpl.report_id = report.id " .
                        "AND report_mktpl.marketplace_instance_id = request_mktpl.marketplace_instance_id" .
                    ")"
                ])
                ->where("request.id", "retrieve.report_request_id")
                ->where("request.report_processing_status", "'_DONE_'")
                ->where("request.updated", "NULL", null, "IS NOT")
                ->where("request_mktpl.marketplace_instance_id", 9, PDO::PARAM_INT)
                ->where([PDOQuery::DATA_ATTR_SPEC => "(" .
                    "request.generated_report_id IS NOT NULL OR report.report_id IS NOT NULL" .
                ")"])
                ->where([PDOQuery::DATA_ATTR_SPEC => "(" .
                    "retrieve.updated IS NULL " .
                    "OR " .
                    "retrieve.updated <= request.updated" .
                ")"])
                ->generate()
        );
    }

    public function testGroupBy() {
        $this->assertEquals(
            "SELECT lname, COUNT(*) FROM (users, groups) WHERE users.group_id = groups.id GROUP BY lname",
            PDOQuery::select()
                ->column("lname")
                ->column("COUNT(*)")
                ->from(["users", "groups"])
                ->where("users.group_id", "groups.id")
                ->groupBy("lname")
                ->generate()
        );
    }

    public function testOrderBy() {
        $this->assertEquals(
            "SELECT * FROM (users, groups) WHERE users.group_id = groups.id ORDER BY groups.name asc, users.lname DESC, users.fname",
            PDOQuery::select()->columns("*")
                ->from(["users", "groups"])
                ->where("users.group_id", "groups.id")
                ->orderBy(["groups.name asc", "users.lname DESC", "users.fname"])
                ->generate()
        );
    }

    public function testLimit() {
        $this->assertEquals(
            "SELECT * FROM (users) ORDER BY id DESC LIMIT 10, 1",
            PDOQuery::select()
                ->from("users")
                ->orderBy("id DESC")
                ->limit([10, 1])
                ->generate()
        );
    }

    public function testRequestListNextTokenQuery() {
        $this->assertEquals(
            "SELECT id, next_token FROM (report_request_list, report_request_list_marketplace_instance mktpl) " .
            "WHERE id = report_request_list_id AND marketplace_instance_id = ? " .
            "AND max_count = ? AND requested_from_date IS NULL AND requested_to_date IS NULL " .
            "AND NOT EXISTS (" .
                "SELECT 1 FROM report_request_list_marketplace_instance wrong_mktpl " .
                "WHERE report_request_list_id = id " .
                "AND wrong_mktpl.marketplace_instance_id <> mktpl.marketplace_instance_id LIMIT 1" .
            ") AND NOT EXISTS (" .
                "SELECT 1 FROM report_request_list_mws_report_request_id " .
                "WHERE report_request_list_id = id LIMIT 1" .
            ") AND NOT EXISTS (" .
                "SELECT 1 FROM report_request_list_report_type " .
                "WHERE report_request_list_id = id LIMIT 1" .
            ") AND NOT EXISTS (" .
                "SELECT 1 FROM report_request_list_report_processing_status " .
                "WHERE report_request_list_id = id LIMIT 1" .
            ")",
            PDOQuery::select()->columns(["id", "next_token"])
                ->from(["report_request_list", "report_request_list_marketplace_instance mktpl"])
                ->where("id", "report_request_list_id")
                ->where("marketplace_instance_id", 5, PDO::PARAM_INT)
                ->where("max_count", 100, PDO::PARAM_INT)
                ->where("requested_from_date", "NULL", null, "IS")
                ->where("requested_to_date", "NULL", null, "IS")
                ->where([PDOQuery::DATA_ATTR_SPEC => "NOT EXISTS (" .
                    "SELECT 1 FROM report_request_list_marketplace_instance wrong_mktpl " .
                    "WHERE report_request_list_id = id " .
                    "AND wrong_mktpl.marketplace_instance_id <> mktpl.marketplace_instance_id LIMIT 1)"
                ])
                ->where([PDOQuery::DATA_ATTR_SPEC => "NOT EXISTS (" .
                    "SELECT 1 FROM report_request_list_mws_report_request_id " .
                    "WHERE report_request_list_id = id LIMIT 1)"
                ])
                ->where(
                    "NOT EXISTS (" .
                    "SELECT 1 FROM report_request_list_report_type " .
                    "WHERE report_request_list_id = id LIMIT 1)",
                    null, null, null
                )
                ->where([PDOQuery::DATA_ATTR_SPEC => "NOT EXISTS (" .
                    "SELECT 1 FROM report_request_list_report_processing_status " .
                    "WHERE report_request_list_id = id LIMIT 1)"
                ])
                ->generate()
        );
    }

    public function testNotInQuery() {
        $marketplaceInstanceListId = 9;

        $this->assertEquals(
            "SELECT DISTINCT marketplace_instance_id " .
            "FROM (marketplace_instance_list_result) " .
            "WHERE marketplace_instance_list_id < ? " .
            "AND marketplace_instance_id NOT IN (" .
                "SELECT marketplace_instance_id FROM marketplace_instance_list_result " .
                "WHERE marketplace_instance_list_id = " . $marketplaceInstanceListId .
            ")",
            PDOQuery::select()->distinct()->column("marketplace_instance_id")->from("marketplace_instance_list_result")
            ->where("marketplace_instance_list_id", $marketplaceInstanceListId, PDO::PARAM_INT, "<")
            ->where(
                "marketplace_instance_id",
                "(" .
                    "SELECT marketplace_instance_id FROM marketplace_instance_list_result " .
                    "WHERE marketplace_instance_list_id = " . $marketplaceInstanceListId .
                ")",
                null, "NOT IN"
            )
            ->generate()
        );
    }

    public function testAndNotExistsQuery() {
        $this->assertEquals(
            "SELECT " .
                "amazon_order_id " .
            "FROM " .
                "(test.orders) " .
            "WHERE " .
                "marketplace_instance_id = ? " .
            "AND " .
                "NOT EXISTS (SELECT 1 FROM test.order_item_extract WHERE order_id = id) " .
            "LIMIT 1",
            PDOQuery::select()->columns("amazon_order_id")
            ->from("test.orders")
            ->where("marketplace_instance_id", 1, PDO::PARAM_INT)
            ->where("NOT EXISTS (SELECT 1 FROM test.order_item_extract WHERE order_id = id)")
            ->limit(1)->generate()
        );
    }

    public function testInsert() {
        $this->assertEquals(
            "INSERT INTO order_list (" .
                "marketplace_instance_id, last_updated_after, next_token, response_header_metadata" .
            ") VALUES (" .
                "?, ?, ?, ?" .
            ") ON DUPLICATE KEY UPDATE " .
                "last_updated_after = VALUES(last_updated_after), " .
                "next_token = VALUES(next_token), " .
                "response_header_metadata = VALUES(response_header_metadata)"
            ,
            PDOQuery::insert()
                ->into("order_list")
                ->value("marketplace_instance_id", 9, PDO::PARAM_INT)
                ->value("last_updated_after", "2015-12-1", PDO::PARAM_STR)
                ->value("next_token", "ThisShouldGetYouTheNextOne", PDO::PARAM_STR)
                ->value("response_header_metadata", "You called get orders", PDO::PARAM_STR)
                ->onDuplicateValue("last_updated_after", "VALUES(last_updated_after)")
                ->onDuplicateValue("next_token", "VALUES(next_token)")
                ->onDuplicateValue("response_header_metadata", "VALUES(response_header_metadata)")
                ->generate()
        );
        $this->assertEquals(
            "INSERT INTO foo (a, b, x, y_z) " .
            "VALUES (?, ?, ?, ?) " .
            "ON DUPLICATE KEY UPDATE " .
            "a = VALUES(a), " .
            "b = VALUES(b), " .
            "x = VALUES(x), " .
            "y_z = VALUES(y_z)",
            PDOQuery::insert()
                ->into("foo")
                ->value("a", "fdsjakfdsa", PDO::PARAM_STR)
                ->value("b", "_b_here_", PDO::PARAM_STR)
                ->value("x", true, PDO::PARAM_BOOL)
                ->value("y_z", 3, PDO::PARAM_INT)
                ->onDuplicateAllValues()
                ->generate()
        );
    }

    public function testInsertExecuteGetRowCount() {
        $mysql =& PDOUtil::getMysqlConnection();
        $table = __FUNCTION__;
        $createTableStmt = $mysql->prepare("CREATE TABLE " . $table . "(a TINYINT NOT NULL, b CHAR(5))");
        $createTableStmt->execute();
        $createTableStmt = null;
        $pdoQuery = PDOQuery::insert()
            ->into($table)
            ->value("a", 7, PDO::PARAM_INT)
            ->value("b", "BLAKE", PDO::PARAM_STR, "LOWER")
        ;

        $this->assertEquals("INSERT INTO " . $table . " (a, b) VALUES (?, LOWER(?))", $pdoQuery->generate());

        $rowCount = $pdoQuery->executeGetRowCount($mysql);

        PDOUtil::commitConnection($mysql);

        $this->assertEquals(1, $rowCount);

        $result = PDOQuery::select()->columns("a", "b")->from($table)->where("b", "'blake'")->getSingleRow($mysql);

        $this->assertNotNull($result);
        $this->assertEquals(7, $result["a"]);
        $this->assertEquals("blake", $result["b"]);
    }

    public function testInsertOnDuplicateValue() {
        $this->assertEquals(
            "INSERT INTO foo (name) VALUES (?) ON DUPLICATE KEY UPDATE bar = VALUES(bar), baz = 33, bat = LOWER(?)",
            PDOQuery::insert()
                ->into("foo")
                ->value("name", "idk", PDO::PARAM_STR)
                ->onDuplicateValue("bar", "VALUES(bar)")
                ->onDuplicateValue("baz", "33")
                ->onDuplicateValue("bat", "33", PDO::PARAM_STR, "LOWER")
                ->generate()
        );
    }

    public function testInsertIgnore() {
        $mysql =& PDOUtil::getMysqlConnection();
        $table = __FUNCTION__;
        $createTableStmt = $mysql->prepare(
            "CREATE TABLE " . $table . "(" .
                "id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, " .
                "name VARCHAR(120) NOT NULL, " .
                "status TINYINT UNSIGNED, " .
                "UNIQUE KEY (name)" .
            ")"
        );
        $createTableStmt->execute();
        $createTableStmt = null;
        $pdoQuery = PDOQuery::insert()->into($table)->value("name", "jake", PDO::PARAM_STR);

        $this->assertEquals(1, $pdoQuery->executeGetRowCount($mysql));
        $this->assertEquals("INSERT INTO " . $table . " (name) VALUES (?)", $pdoQuery->generate());
        $this->assertEquals(0, $pdoQuery->ignore()->executeGetRowCount($mysql));
        $this->assertEquals("INSERT IGNORE INTO " . $table . " (name) VALUES (?)", $pdoQuery->generate());

        try {
            PDOQuery::insert()->into($table)->value("name", "'jake'")->executeGetRowCount($mysql);

            $this->fail("Insert without ignore violating a unique constraint should throw an exception");
        } catch(Exception $e) { }

        $this->assertEquals(2, $pdoQuery->ignore(false)->onDuplicateValue("status", "99")->executeGetRowCount($mysql));
        $this->assertEquals("INSERT INTO " . $table . " (name) VALUES (?) ON DUPLICATE KEY UPDATE status = 99", $pdoQuery->generate());
    }

    public function testUpdate() {
        $this->assertEquals(
            "UPDATE order_item SET quantity_shipped = ? WHERE id = ?",
            PDOQuery::update()
                ->table("order_item")
                ->set("quantity_shipped", 13, PDO::PARAM_INT)
                ->where("id", 17962, PDO::PARAM_INT)
                ->generate()
        );

        $this->assertEquals(
            "UPDATE users SET status = ? WHERE login = 'john' AND status = 1",
            PDOQuery::update()
                ->table("users")
                ->set("status", 0, PDO::PARAM_INT)
                ->where("login", "'john'")
                ->where("status", "1")
                ->generate()
        );
    }

    public function testDelete() {
        $this->assertEquals(
            "DELETE FROM users WHERE status = ? AND login_date < DATE_SUB(NOW(), INTERVAL 1 YEAR) ORDER BY id ASC LIMIT 10",
            PDOQuery::delete()
                ->from("users")
                ->where("status", 0, PDO::PARAM_INT)
                ->where("login_date", "DATE_SUB(NOW(), INTERVAL 1 YEAR)", null, "<")
                ->orderBy("id ASC")
                ->limit(10)
                ->generate()
        );
    }

    public function testUnion() {
        $this->assertEquals(
            "SELECT name FROM (products) UNION SELECT name FROM (parts)",
            PDOQuery::select()->column("name")->from("products")->union(
                PDOQuery::select()->column("name")->from("parts")
            )->generate()
        );
    }
}
