<?php

use BlakePrinting\Test\AbstractTestCase;
use BlakePrinting\Util\MoneyUtil;

class MoneyUtilTest extends AbstractTestCase {
    public function testStripThousandsSeparator() {
        $this->assertSame("0", MoneyUtil::stripThousandsSeparator("0"));
        $this->assertSame("0.0", MoneyUtil::stripThousandsSeparator("0.0"));
        $this->assertSame("0,0", MoneyUtil::stripThousandsSeparator("0,0"));
        $this->assertSame("0.01", MoneyUtil::stripThousandsSeparator("0.01"));
        $this->assertSame("0,01", MoneyUtil::stripThousandsSeparator("0,01"));
        $this->assertSame("1000.01", MoneyUtil::stripThousandsSeparator("1,000.01"));
        $this->assertSame("1000,01", MoneyUtil::stripThousandsSeparator("1.000,01"));
        $this->assertSame("1000000.01", MoneyUtil::stripThousandsSeparator("1,000,000.01"));
        $this->assertSame("1000000,01", MoneyUtil::stripThousandsSeparator("1.000.000,01"));
    }

    public function testToCents() {
        $this->assertNull(MoneyUtil::toCents(null));
        $this->assertEquals(100, MoneyUtil::toCents("1"));
        $this->assertEquals(10, MoneyUtil::toCents("1", 1));
        $this->assertEquals(100000, MoneyUtil::toCents("1", 5));
        $this->assertEquals(100, MoneyUtil::toCents("1."));
        $this->assertEquals(100, MoneyUtil::toCents("1.0"));
        $this->assertEquals(100, MoneyUtil::toCents("1.007"));
        $this->assertEquals(754007332, MoneyUtil::toCents("754007.332", 3));
        $this->assertEquals(-100, MoneyUtil::toCents("-1"));
        $this->assertEquals(-10, MoneyUtil::toCents("-1", 1));
        $this->assertEquals(-100000, MoneyUtil::toCents("-1", 5));
        $this->assertEquals(-100, MoneyUtil::toCents("-1."));
        $this->assertEquals(-100, MoneyUtil::toCents("-1.0"));
        $this->assertEquals(-100, MoneyUtil::toCents("-1.007"));
        $this->assertEquals(-754007332, MoneyUtil::toCents("-754007.332", 3));
    }

    public function testFromCents() {
        $this->assertNull(MoneyUtil::fromCents(null));
        $this->assertSame("0.00", MoneyUtil::fromCents(0));
        $this->assertSame("0.01", MoneyUtil::fromCents(1));
        $this->assertSame("1.0", MoneyUtil::fromCents(10, 1));
        $this->assertSame("1.00000", MoneyUtil::fromCents(100000, 5));
        $this->assertSame("1.000", MoneyUtil::fromCents(1000, 3));
        $this->assertSame("754007.332", MoneyUtil::fromCents(754007332, 3));
        $this->assertSame("-0.01", MoneyUtil::fromCents(-1));
        $this->assertSame("-1.0", MoneyUtil::fromCents(-10, 1));
        $this->assertSame("-1.00000", MoneyUtil::fromCents(-100000, 5));
        $this->assertSame("-1.000", MoneyUtil::fromCents(-1000, 3));
        $this->assertSame("-754007.332", MoneyUtil::fromCents(-754007332, 3));
    }

    public function testTaxCalc() {
        $total = "17.99";
        $subtotal = "14.87";
        $totalCents = MoneyUtil::toCents($total, 2);
        $this->assertSame(1799, $totalCents);
        $subtotalCents = MoneyUtil::toCents($subtotal, 2);
        $this->assertSame(1487, $subtotalCents);
        $taxCents = $totalCents - $subtotalCents;
        $this->assertSame("3.12", MoneyUtil::fromCents($taxCents));
    }
}
