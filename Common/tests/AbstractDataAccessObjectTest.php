<?php

use BlakePrinting\Db\AbstractDataAccessObject;
use BlakePrinting\Db\PDOUtil;
use BlakePrinting\Db\ValueObject;
use BlakePrinting\Test\AbstractDbTestCase;

class AbstractDaoTestDAO extends AbstractDataAccessObject {
}

class AbstractDaoTestVO implements ValueObject {
    /** @var int */
    private $id = null;
    /** @var int */
    private $created = null;
    /** @var int */
    private $updated = null;
    /** @var string */
    private $name = null;
    /** @var int */
    private $status = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
}

class AbstractDataAccessObjectTest extends AbstractDbTestCase {
    public function testAbstractDao() {
        $mysql =& PDOUtil::getMysqlConnection();
        $dao = new AbstractDaoTestDAO($mysql, "test");
        $daoTbl = $dao->formatSchemaAndTableName();

        $mysql->exec(
            "CREATE TABLE $daoTbl (" .
                "id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, " .
                "created BIGINT UNSIGNED NOT NULL, " .
                "updated BIGINT UNSIGNED, " .
                "name VARCHAR(120) NOT NULL, " .
                "status TINYINT UNSIGNED, " .
                "UNIQUE KEY (name)" .
            ")"
        );

        $this->assertEmpty($dao->findAllByPrototype(
            self::createVo(null, null, null, null, 1)
        ));

        $expected = "INSERT INTO " . $daoTbl . " (created, name, status) VALUES (UNIX_TIMESTAMP(), ?, ?)";
        $t1Vo = self::createVo(null, null, null, "t1", 0);
        $this->assertEquals($expected, $dao->createInsertFromPrototype($t1Vo)->generate());

        $id = $dao->insert($t1Vo);
        $this->assertNotNull($id);
        $this->assertEquals(1, $id);
        /** @var AbstractDaoTestVO $t1Vo */
        $t1Vo =& $dao->findById($id);
        $this->assertNotNull($t1Vo);
        $this->assertNotNull($t1Vo->getCreated());
        $this->assertGreaterThan(0, $t1Vo->getCreated());
        $this->assertNotNull($dao->findByPrototype($t1Vo));
        $this->assertEquals(1, count($dao->findAllByPrototype($t1Vo)));

        $this->assertEmpty($dao->findAllByPrototype(
            self::createVo(null, null, null, null, 1)
        ));

        $t2Vo = self::createVo(null, null, null, "t2", 1);
        $id = $dao->insert($t2Vo);
        $this->assertEquals(2, $id);
        $this->assertEquals(1, count($dao->findAllByPrototype(
            self::createVo(null, null, null, null, 1)
        )));

        $dao->insert(self::createVo(null, null, null, "t3", 1));
        $dao->insert(self::createVo(null, null, null, "t4", 1));
        $dao->insert(self::createVo(null, null, null, "t5", 2));

        $results =& $dao->findAllByPrototype(
            self::createVo(null, null, null, null, 1), "name desc", 2
        );

        $this->assertEquals(2, count($results));

        $result =& $results[0];

        if($result instanceof AbstractDaoTestVO) {
            $this->assertEquals("t4", $result->getName());
        } else {
            $this->fail("Result should have been an " . AbstractDaoTestVO::class);
        }

        $expected = "UPDATE " . $daoTbl . " SET updated = UNIX_TIMESTAMP()";
        $this->assertEquals($expected, $dao->createUpdateFromPrototype(self::createVo())->generate());

        $expected = "UPDATE " . $daoTbl . " SET status = ?, updated = UNIX_TIMESTAMP() WHERE id = ?";
        $this->assertEquals(
            $expected, $dao->createUpdateFromPrototype(
                self::createVo(3, null, null, null, 1)
            )->generate()
        );

        try {
            $dao->update(self::createVo(null, null, null, null, 66));
            $this->fail("Should have generated an exception when no conditions are specified");
        } catch(Exception $e) { }

        $dao->update(
            self::createVo(null, null, null, null, 66),
            null, [], [], [], false
        );
    }

    private static function &createVo($id = null, $created = null, $updated = null, $name = null, $status = null) {
        $vo = new AbstractDaoTestVO();

        $vo->setId($id);
        $vo->setUpdated($updated);
        $vo->setName($name);
        $vo->setStatus($status);

        return $vo;
    }
}
