#!/bin/bash
#
# Test the entire project at once
#

set -o errexit

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${BASE_DIR}/lib/lib-blake-environment.sh"
. "${BASE_DIR}/lib/lib-docker.sh"

[ "${HAS_DOCKER}" = "true" ] || exit 1

if [ -z "$1" ];
then
  for phpunit_file in `ls ${BASE_DIR}/*/phpunit.xml`
  do
    phpunit_dir="$(basename $(dirname ${phpunit_file}))"

    echo "=== Testing: ${phpunit_dir}"

    "${BASE_DIR}/bin/run_php.sh" "${BLAKE_APP_DIR}/vendor/bin/phpunit" -c "${BLAKE_APP_DIR}/${phpunit_dir}/phpunit.xml"
  done
else
  for module in $*
  do
    echo "=== Testing: ${module}"

    "${BASE_DIR}/bin/run_php.sh" "${BLAKE_APP_DIR}/vendor/bin/phpunit" -c "${BLAKE_APP_DIR}/${module}/phpunit.xml"
  done
fi
