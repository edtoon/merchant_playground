<?php

require_once(is_dir(__DIR__ . "/../vendor") ? (__DIR__ . "/../vendor/autoload.php") : "/app/vendor/autoload.php");

use BlakePrinting\Util\LogUtil;
use BlakePrinting\Util\Utils;
use BlakePrinting\Zebra\ZebraDiscoveryClient;
use BlakePrinting\Zebra\ZebraDiscoveryResponse;
use BlakePrinting\Zebra\Zeti\ZetiUtil;
use BlakePrinting\Zebra\ZebraZplClient;
use GuzzleHttp\Client as GuzzleClient;
use Symfony\Component\Debug\ErrorHandler;

function syntax($scriptName) {
    echo("Syntax: " . $scriptName . " (no arguments) - Retrieve details of paired Zebra RFD8500 scanner" . PHP_EOL);
    echo("Syntax: " . $scriptName . " <hostname> <apiKey> - Discover networked Zebra printers and notify the specified host of the results, retrieving queue entries to print" . PHP_EOL);

    exit(1);
}

function main(&$argc, &$argv) {
    $logger =& LogUtil::getLogger();

    ErrorHandler::register();

    if($argc < 2) {
        $serialDevices = glob("/dev/tty.RFD8500*-R");

        if(count($serialDevices) < 1) {
            $logger->debug("No RFD8500 serial device found");
        } else {
            $serialDevice = $serialDevices[0];

            $logger->debug("# Opening serial device: " . $serialDevice);

            while(($fd = fopen($serialDevice, "w+")) === false) {
                $logger->debug("# Waiting for serial device");
                sleep(1);
            }

            try {
                stream_set_blocking($fd, 0);
                sleep(1);

                $status = ZetiUtil::connect($fd, null, true);

                if($status !== "Connection Successful") {
                    $logger->error("Connect failed, status was: " . $status);
                } else {
                    $zetiCapabilities =& ZetiUtil::getCapabilities($fd);
                    $zetiDeviceInfo =& ZetiUtil::getDeviceInfo($fd);
                    $zetiRegulatory =& ZetiUtil::getRegulatory($fd);
                    $zetiVersion =& ZetiUtil::getVersion($fd);

                    $logger->debug(
                        "Serial device: " . $serialDevice . " has " .
                        "version: " . Utils::getVarDump($zetiVersion) . ", " .
                        "regulatory: " . Utils::getVarDump($zetiRegulatory) . ", " .
                        "device info: " . Utils::getVarDump($zetiDeviceInfo) . ", " .
                        "capabilities: " . Utils::getVarDump($zetiCapabilities)
                    );
                }
            } finally {
                fclose($fd);
            }

            return;
        }
    } else if($argc == 3) {
        $gatewayHost =& $argv[1];
        $gatewayApiKey =& $argv[2];

        if(!empty($gatewayApiKey) && !empty($gatewayHost)) {
            $printerDiscoveryResponses = ZebraDiscoveryClient::discoverNetworkedPrinters();

            foreach($printerDiscoveryResponses as &$discoveryResponse) {
                $logger->debug("Discovered printer: " . Utils::getVarDump($discoveryResponse));
            }

            unset($discoveryResponse);

            if(!empty($printerDiscoveryResponses)) {
                merchantApiPing($gatewayHost, $gatewayApiKey, $printerDiscoveryResponses);
            }

            return;
        }
    }

    syntax($argv[0]);
}

/**
 * @param string $apiHost
 * @param string $apiKey
 * @param ZebraDiscoveryResponse[] $discoveryResponses
 */
function merchantApiPing($apiHost, $apiKey, array &$discoveryResponses) {
    $logger =& LogUtil::getLogger();

    $logger->debug("Registering " . count($discoveryResponses) . " printers with api host: " . $apiHost . ", key: " . $apiKey);

    $guzzle = new GuzzleClient();
    $response = $guzzle->request("POST", "http://" . $apiHost . "/gw/v1", [
        "json" => [
            "api_key" => $apiKey,
            "action" => "ping",
            "printers" => $discoveryResponses
        ]
    ]);

    if($response->getStatusCode() !== 200) {
        $logger->error("Error processing request, response was: " . Utils::getVarDump($response));
        exit(1);
    }

    $responseBody = (string)$response->getBody();

    $logger->debug("Response: " . $responseBody);

    $responseData = json_decode($responseBody, true, 512, JSON_BIGINT_AS_STRING);

    if($responseData["total"] > 0) {
        foreach($responseData["rows"] as &$queuedLabel) {
            $labelId =& $queuedLabel["id"];
            $asciiData = "gg" . str_pad($labelId, 10, "0", STR_PAD_LEFT);

            $logger->debug("Printing label #" . $labelId . ", type: " . $queuedLabel["type"] . ", ascii: " . $asciiData);

            sendZplToPrinter(
                $queuedLabel["ipv4"], $queuedLabel["port"],
                "^XA\n" .
                "^RS8\n" .
                "^FO50,50^BY3\n" .
                "^BCN,100,Y,N,N\n" .
                "^FD" . $asciiData . "^FS\n" .
                "^RFW,H,1,2,1^FD3400^FS\n" .
                "^RFW,A,2,12,1^FD" . $asciiData . "^FS" .
                "^XZ"
            );

            merchantApiComplete($apiHost, $apiKey, $labelId, $queuedLabel["printer_id"]);
        }
    }
}

/**
 * @param string $apiHost
 * @param string $apiKey
 * @param int $labelId
 * @param int $printerId
 */
function merchantApiComplete($apiHost, $apiKey, $labelId, $printerId) {
    $logger =& LogUtil::getLogger();

    $logger->debug("Sending acknowledgement of completion of label #" . $labelId . " on printer #" . $printerId);

    $guzzle = new GuzzleClient();
    $response = $guzzle->request("POST", "http://" . $apiHost . "/gw/v1", [
        "json" => [
            "api_key" => $apiKey,
            "action" => "complete",
            "label_id" => $labelId,
            "printer_id" => $printerId
        ]
    ]);

    if($response->getStatusCode() !== 200) {
        $logger->error("Error processing request, response was: " . Utils::getVarDump($response));
        exit(1);
    }

    $responseBody = (string)$response->getBody();

    $logger->debug("Response: " . $responseBody);
}

function sendZplToPrinter($ipAddress, $port, $str, $requireResponse = false, $initialWait = 0.75, $timeout = 5) {
    $logger =& LogUtil::getLogger();
    $zebraClient = new ZebraZplClient($ipAddress, $port);
    $socket =& $zebraClient->getSocket();

    if($socket === false) {
        $errorCode = socket_last_error();
        $errorMessage = socket_strerror($errorCode);

        $logger->debug("Received socket error #" . $errorCode . " while connecting to the printer at: " . $ipAddress . ":" . $port . " - " . $errorMessage);
        exit(1);
    }

    $logger->debug("================================================================================");
    $logger->debug("Sending ZPL to: " . $ipAddress);
    $logger->debug("================================================================================");
    $logger->debug($str);

    $response =& $zebraClient->sendAndGetResponse($str, $requireResponse, $initialWait, $timeout);

    if(!empty($response)) {
        $logger->debug("================================================================================");
        $logger->debug("Response:");
        $logger->debug("================================================================================");
        $logger->debug($response);
    }

    $zebraClient->disconnect();
}

main($argc, $argv);
