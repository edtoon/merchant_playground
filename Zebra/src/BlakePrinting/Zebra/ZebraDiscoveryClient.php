<?php

namespace BlakePrinting\Zebra;

use BlakePrinting\Util\LogUtil;
use Exception;
use Monolog\Logger;

class ZebraDiscoveryClient {
    const MAX_DISCOVERY_TIME_SECS = 6; // six seconds is what the Zebra example code uses
    const DISCOVERY_PORT = 4201;
    const BROADCAST_ADDRESS = "255.255.255.255";
    const MULTICAST_ADDRESS = "224.0.1.55";

    private function __construct() { }

    /**
     * @param string $bindIp
     * @return ZebraDiscoveryResponse[]
     * @throws Exception
     */
    public static function discoverNetworkedPrinters($bindIp = null) {
        $logger =& LogUtil::getLogger();

        if($bindIp === null) {
            $bindIp = gethostbyname(gethostname());
        }

        $socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);

        if($socket === false) {
            $errorCode = socket_last_error();

            self::throwSocketException($socket, "trying to create a socket", $errorCode);
        }

        $discoveredIps = [];
        $discoveredResponses = [];
        $broadcastResult = false;
        $multicastResult = false;

        try {
            if(socket_bind($socket, $bindIp, 0) === false) {
                self::throwSocketException($socket, "trying to bind to socket");
            }

            if(socket_set_option($socket, SOL_SOCKET, SO_BROADCAST, 1) === false) {
                self::throwSocketException($socket, "trying to set socket to broadcast mode");
            }

            if(socket_set_nonblock($socket) === false) {
                self::throwSocketException($socket, "trying to set socket to nonblocking");
            }

            $discoveryPacketData = "\x2e\x2c\x3a\x01\x00\x00";

            try {
                $broadcastResult = socket_sendto($socket, $discoveryPacketData, strlen($discoveryPacketData), 0, self::BROADCAST_ADDRESS, self::DISCOVERY_PORT);
            } catch(Exception $e) { }

            if($broadcastResult !== false) {
                $broadcastResponses =& self::readDiscoveryResponses($socket);

                self::parseDiscoveryResponses($broadcastResponses, $discoveredResponses, $discoveredIps);
            }

            if(empty($discoveredResponses)) {
                try {
                    $multicastResult = socket_sendto($socket, $discoveryPacketData, strlen($discoveryPacketData), 0, self::MULTICAST_ADDRESS, self::DISCOVERY_PORT);
                } catch(Exception $e) { }

                if($multicastResult !== false) {
                    $multicastResponses =& self::readDiscoveryResponses($socket);

                    self::parseDiscoveryResponses($multicastResponses, $discoveredResponses, $discoveredIps);
                }
            }
        } finally {
            socket_close($socket);
        }

        if($broadcastResult === false && $multicastResult === false && rand(1, 6) == 6) {
            $logger->warn("Both broadcast and multicast discovery failed");
        }

        return $discoveredResponses;
    }

    private static function parseDiscoveryResponses(&$ipsAndResponses, &$discoveryResponses, &$discoveredIps) {
        $logger =& LogUtil::getLogger();

        foreach($ipsAndResponses as $respondingIp => &$discoveryResponseData) {
            if(!in_array($respondingIp, $discoveredIps)) {
                $discoveryResponse = null;

                try {
                    $discoveryResponse = new ZebraDiscoveryResponse($discoveryResponseData);
                } catch(Exception $e) {
                    LogUtil::logException(Logger::ERROR, $e, "Error parsing discovery response from ip: " . $respondingIp);

                    continue;
                }

                $discoveredIp = $discoveryResponse->getIpAddress();

                if($discoveredIp === null) {
                    $logger->warn("Received a blank discovery response from ip: " . $respondingIp);

                    continue;
                }

                if($respondingIp != $discoveredIp) {
                    if(in_array($discoveredIp, $discoveredIps)) {
                        continue;
                    }

                    $logger->warn("Received discovery response from ip: " . $respondingIp . " that reported its own address as: " . $discoveredIp);
                }

                $discoveredIps[] = $discoveredIp;
                $discoveryResponses[] =& $discoveryResponse;
            }
        }

        unset($discoveryResponse);
    }

    /**
     * @param resource $socket
     * @return array
     * @throws Exception
     */
    private static function &readDiscoveryResponses(&$socket) {
        $logger =& LogUtil::getLogger();
        $responses = [];
        $startTime = microtime(true);

        while(true) {
            $buffer = null;
            $from = null;
            $port = null;

            if(socket_recvfrom($socket, $buffer, 1024, 0, $from, $port) === false) {
                $errorCode = socket_last_error($socket);

                if($errorCode != 35) {
                    self::throwSocketException($socket, "trying to read discovery responses", $errorCode);
                }
            }

            if($from !== null && strlen($from) > 0 && !isset($responses[$from])) {
                $responses[$from] = "";

                $logger->debug("Response from: " . $from . " on port: " . $port);
            }

            if($buffer !== null && strlen($buffer) > 0) {
                $responses[$from] .= $buffer;
            }

            $currentTime = microtime(true);

            if(($currentTime - $startTime) > self::MAX_DISCOVERY_TIME_SECS) {
                break;
            }

            usleep(1000);
        }

        return $responses;
    }

    /**
     * @param resource|bool $socket
     * @param string $action
     * @param int $errorCode
     * @throws Exception
     */
    private static function throwSocketException(&$socket, $action = null, $errorCode = null) {
        if($errorCode === null) {
            $errorCode = socket_last_error($socket);
        }

        $errorMessage = socket_strerror($errorCode);

        throw new Exception("Received socket error #" . $errorCode . (empty($action) ? ", message: " : (" while " . $action . ":")) . $errorMessage, $errorCode);
    }
}
