<?php

namespace BlakePrinting\Zebra;

use Exception;
use JsonSerializable;

class ZebraDiscoveryResponse implements JsonSerializable {
    const REQUIRED_FORMAT_VERSION = 3;
    const MIN_PACKET_SIZE = 375;
    const PORT_STATUS_ONLINE = "Online";
    const PORT_STATUS_OFFLINE = "Offline";
    const PORT_STATUS_TONER_LOW = "Toner Low";
    const PORT_STATUS_PAPER_OUT = "Paper Out";
    const PORT_STATUS_PAPER_JAMMED = "Paper Jammed";
    const PORT_STATUS_DOOR_OPEN = "Door Open";
    const PORT_STATUS_PRINTER_ERROR = "Printer Error";
    const PORT_STATUS_UNKNOWN = "Unknown";
    const DEFAULT_PRINTER_PORT = 9100;
    const ALTERNATE_PRINTER_PORT = 6101;
    const ALTERNATE_PORT_SERIES_IDENTIFIERS = ["QL", "RW", "MZ", "P4T", "MQ", "MU"];

    private static $possiblePortStatuses = [
        1 => self::PORT_STATUS_ONLINE,
        2 => self::PORT_STATUS_OFFLINE,
        3 => self::PORT_STATUS_TONER_LOW,
        4 => self::PORT_STATUS_PAPER_OUT,
        5 => self::PORT_STATUS_PAPER_JAMMED,
        6 => self::PORT_STATUS_DOOR_OPEN,
        7 => self::PORT_STATUS_PRINTER_ERROR,
        8 => self::PORT_STATUS_UNKNOWN
    ];

    /** @var string */
    private $companyAbbreviation = null;
    /** @var string */
    private $dateCode = null;
    /** @var string */
    private $defaultGateway = null;
    /** @var int */
    private $discoveryVersion = null;
    /** @var string */
    private $encryptedGetCommunityName = null;
    /** @var string */
    private $encryptedSetCommunityName = null;
    /** @var string */
    private $firmwareVersion = null;
    /** @var string */
    private $hardwareAddress = null;
    /** @var string */
    private $ipAddress = null;
    /** @var int */
    private $printerPort = null;
    /** @var string */
    private $portName = null;
    /** @var string */
    private $portStatus = null;
    /** @var string */
    private $productName = null;
    /** @var string */
    private $productNumber = null;
    /** @var string */
    private $serialNumber = null;
    /** @var string */
    private $subnetMask = null;
    /** @var string */
    private $systemName = null;
    /** @var bool */
    private $usingNetProtocol = null;

    public function __construct(&$response) {
        if($response !== null) {
            $length = strlen($response);

            if($length < 3) {
                throw new Exception(
                    "Response too short - should be more than 3 characters, was " . $length . " - " .
                    "hex value: " . bin2hex($response) . ", text value: " . $response
                );
            }

            $discoveryVersion = self::convertByteToInt($response[3]);

            if($discoveryVersion != self::REQUIRED_FORMAT_VERSION) {
                throw new Exception(
                    "We only know how to parse the discovery response format version: " . self::REQUIRED_FORMAT_VERSION . ", " .
                    "received version was: " . $discoveryVersion
                );
            }

            if($length < self::MIN_PACKET_SIZE) {
                throw new Exception(
                    "Received version " . self::REQUIRED_FORMAT_VERSION . " discovery response that was too short - " .
                    "must be at least 375 bytes, response was " . $length . ": " . $response
                );
            }

            $this->discoveryVersion = $discoveryVersion;
            $this->companyAbbreviation = self::getStringFromResponse($response, 49, 5);
            $this->dateCode = self::getStringFromResponse($response, 32, 7);
            $this->defaultGateway = self::getAddressFromResponse($response, 80, 4);
            $this->encryptedGetCommunityName = bin2hex(substr($response, 212, 244));
            $this->encryptedSetCommunityName = bin2hex(substr($response, 244, 276));
            $this->firmwareVersion = self::getStringFromResponse($response, 39, 10);
            $this->hardwareAddress = bin2hex(substr($response, 54, 6));
            $this->ipAddress = self::getAddressFromResponse($response, 72, 4);
            $this->productName = self::getStringFromResponse($response, 12, 20);
            $this->productNumber = self::getStringFromResponse($response, 4, 8);
            $this->portStatus = self::getPortStatusFromResponse($response);
            $this->portName = self::getStringFromResponse($response, 359, 16);
            $this->serialNumber = self::getStringFromResponse($response, 60, 10);
            $this->subnetMask = self::getAddressFromResponse($response, 76, 4);
            $this->systemName = self::getStringFromResponse($response, 84, 25);
            $this->usingNetProtocol = self::getBooleanFromResponse($response, 70, 2);

            if(in_array(substr($this->productName, 0, 2), self::ALTERNATE_PORT_SERIES_IDENTIFIERS)) {
                $this->printerPort = self::ALTERNATE_PRINTER_PORT;
            } else {
                $this->printerPort = self::DEFAULT_PRINTER_PORT;
            }
        }
    }

    public function jsonSerialize() {
        return [
            "discovery_version" => $this->discoveryVersion,
            "company_abbreviation" => $this->companyAbbreviation,
            "date_code" => $this->dateCode,
            "default_gateway_v4" => $this->defaultGateway,
            "encrypted_get_community_name" => $this->encryptedGetCommunityName,
            "encrypted_set_community_name" => $this->encryptedSetCommunityName,
            "firmware_version" => $this->firmwareVersion,
            "hardware_address" => $this->hardwareAddress,
            "ip_address_v4" => $this->ipAddress,
            "product_name" => $this->productName,
            "product_number" => $this->productNumber,
            "port_status" => $this->portStatus,
            "port_name" => $this->portName,
            "serial_number" => $this->serialNumber,
            "subnet_mask" => $this->subnetMask,
            "system_name" => $this->systemName,
            "use_network" => $this->usingNetProtocol,
            "printer_port" => $this->printerPort
        ];
    }

    private static function getAddressFromResponse(&$response, $offset, $length) {
        $max = $offset + $length;
        $str = "";

        for($position = $offset; $position < $max; ++$position) {
            $str .= self::convertByteToInt($response[$position]);

            if(($position + 1) < $max) {
                $str .= ".";
            }
        }

        return $str;
    }

    private static function getBooleanFromResponse(&$response, $offset, $length) {
        $max = $offset + $length;

        for($position = $offset; $position < $max; ++$position) {
            if(ord($response[$position]) == 1) {
                return true;
            }
        }

        return false;
    }

    private static function getPortStatusFromResponse(&$response) {
        $portStatus = self::convertByteToInt(substr($response, 358, 1));

        if(isset(self::$possiblePortStatuses[$portStatus])) {
            return self::$possiblePortStatuses[$portStatus];
        }

        return self::PORT_STATUS_UNKNOWN;
    }

    private static function getStringFromResponse(&$response, $offset, $length) {
        $max = $offset + $length;
        $str = "";

        for($position = $offset; $position < $max; ++$position) {
            $char = $response[$position];

            if(ord($char) === 0) {
                break;
            }

            $str .= $char;
        }

        return $str;
    }

    private static function convertByteToInt($byte) {
        $ascii = ord($byte);

        if($ascii < 0) {
            $ascii += 256;
        }

        return $ascii;
    }

    /**
     * @return string
     */
    public function getCompanyAbbreviation()
    {
        return $this->companyAbbreviation;
    }

    /**
     * @return string
     */
    public function getDateCode()
    {
        return $this->dateCode;
    }

    /**
     * @return string
     */
    public function getDefaultGateway()
    {
        return $this->defaultGateway;
    }

    /**
     * @return int
     */
    public function getDiscoveryVersion()
    {
        return $this->discoveryVersion;
    }

    /**
     * @return string
     */
    public function getEncryptedGetCommunityName()
    {
        return $this->encryptedGetCommunityName;
    }

    /**
     * @return string
     */
    public function getEncryptedSetCommunityName()
    {
        return $this->encryptedSetCommunityName;
    }

    /**
     * @return string
     */
    public function getFirmwareVersion()
    {
        return $this->firmwareVersion;
    }

    /**
     * @return string
     */
    public function getHardwareAddress()
    {
        return $this->hardwareAddress;
    }

    /**
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * @return int
     */
    public function getPrinterPort()
    {
        return $this->printerPort;
    }

    /**
     * @return string
     */
    public function getPortName()
    {
        return $this->portName;
    }

    /**
     * @return string
     */
    public function getPortStatus()
    {
        return $this->portStatus;
    }

    /**
     * @return string
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * @return string
     */
    public function getProductNumber()
    {
        return $this->productNumber;
    }

    /**
     * @return string
     */
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * @return string
     */
    public function getSubnetMask()
    {
        return $this->subnetMask;
    }

    /**
     * @return string
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

    /**
     * @return boolean
     */
    public function isUsingNetProtocol()
    {
        return $this->usingNetProtocol;
    }
}
