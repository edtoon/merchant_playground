<?php

namespace BlakePrinting\Zebra;

use Zebra\Zpl\Builder;

/**
 *
 * Not all of these work on every printer, and we don't have any way to distinguish ~commands from ^commands at the moment.
 *
 * @method $this cc(string $caretCharacter) Change Caret
 * @method $this cd(string $delimiterCharacter) Change Delimiter
 * @method $this cf(string $font, int $charHeightInDots, int $charWidthInDots) Change Alphanumeric Default Font
 * @method $this ci(string $charset, $s1 = null, $d1 = null, $s2 = null, $d2 = null) Change International Font/Encoding
 * @method $this cm(string $aliasForB, string $aliasForE, string $aliasForR, string $aliasForA, string $multipleAlias) Change Memory Letter Designation
 * @method $this co(bool $cacheOn = true, int $additionalKb = 40, int $cacheType = 0) Cache On
 * @method $this ct(string $tildeCharacter) Change Tilde
 * @method $this cv(bool $validateCodes) Code Validation
 * @method $this cw(string $oldFont, string $newFontDeviceNameAndExtension = "UNKNOWN") Font Identifier
 * @method $this db(string $fontDeviceNameAndExtension, string $orientation, int $maxHeightInDots, int $maxWidthInDots, int $baseDotsFromTop, int $spaceWidth, int $numberOfChars, string $copyrightHolder, string $data) Download Bitmap Font
 * @method $this de(string $tableDeviceNameAndExtension, int $tableSize, string $data) Download Encoding
 * @method $this df(string $formatDeviceNameAndExtension) Download Format
 * @method $this dg(string $graphicDeviceNameAndExtension, int $totalBytes, int $bytesPerRow, string $data) Download Graphics
 * @method $this dn() Abort Download Graphic
 * @method $this ds(string $fontDeviceNameAndExtension, int $sizeInBytes, string $data) Download Intellifont (Scalable Font)
 * @method $this dt(string $fontDeviceNameAndExtension, int $sizeInBytes, string $data) Download Bounded TrueType Font
 * @method $this du(string $fontDeviceNameAndExtension, int $sizeInBytes, string $data) Download Unbounded TrueType Font
 * @method $this dy(string $objectDeviceAndName, string $format, string $extension, int $sizeInBytes, int $bytesPerRow, string $data) Download Objects
 * @method $this eg(string $graphicDeviceNameAndExtension) Erase Download Graphics
 * @method $this fb(int $widthOfLine, int $maxLines, int $dotsBetweenLines, string $justification, int $hangingIndentDots) Field Block
 * @method $this fc(string $primaryIndicator, string $secondaryIndicator, string $tertiaryIndicator) Field Clock
 * @method $this fd(string $data) Field Data
 * @method $this fh(string $hexIndicator) Field Hexadecimal Indicator
 * @method $this fl(string $fontDeviceNameAndExtension, string $baseFontPattern, int $linkOrUnlink) Font Link
 * @method $this fm(string $x1LocationInDots, string $y1LocationInDots, string $x2 = null, string $y2 = null) Multiple Field Origin Locations
 * @method $this fn(string $fieldNumber) Field Number
 * @method $this fo(int $xLocationInDots, int $yLocationInDots, string $justification) Field Origin
 * @method $this fp(string $direction, int $additionalGapInDots) Field Parameter
 * @method $this fr() Field Reverse Print
 * @method $this fs() Field Separator
 * @method $this ft(int $xLocationInDots, int $yLocationInDots, string $justification) Field Typeset
 * @method $this fv(string $data) Field Variable
 * @method $this fw(string $rotate, string $justification) Field Orientation
 * @method $this fx(string $comment) Comment
 * @method $this gb(int $widthInDots, int $heightInDots, int $borderThicknessInDots, string $blackOrWhite, int $degreeOfRounding) Graphic Box
 * @method $this gc(int $diameterInDots, int $borderThicknessInDots, string $blackOrWhite) Graphic Circle
 * @method $this gd(int $widthInDots, int $heightInDots, int $borderThicknessInDots, string $blackOrWhite, string $orientation) Graphic Diagonal Line
 * @method $this ge(int $widthInDots, int $heightInDots, int $borderThicknessInDots, string $blackOrWhite) Graphic Ellipse
 * @method $this gf(string $compressionType, int $binaryByteCount, int $graphicFieldCount, int $bytesPerRow, $data)
 * @method $this gs(string $orientation, int $proportionalHeightInDots, int $proportionalWidthInDots) Graphic Symbol
 * @method $this hb() Battery Status
 * @method $this hd() Head Diagnostic
 * @method $this hf(string $fileDeviceNameAndExtension) Host Format
 * @method $this hg(string $graphicDeviceNameAndExtension) Host Graphic
 * @method $this hh() Configuration Label Return
 * @method $this hi() Host Identification
 * @method $this hm() Host RAM Status
 * @method $this hq(string $queryType) Host Query
 * @method $this hs() Host Status Return
 * @method $this ht() Host Linked Fonts List
 * @method $this hu() Host ZebraNet Alert Configuration
 * @method $this hv(int $fieldNumber, int $numberOfBytes, string $header, string $termination, string $cmd) Host Verification
 * @method $this hw(string $directoryPattern, string $format = null) Host Directory List
 * @method $this hy(string $graphicDeviceNameAndExtension) Upload Graphics
 * @method $this hz(string $description, string $deviceNameAndExtension = null, bool $longFilenameSupport = false) Display Description Information
 * @method $this id(string $objectDeviceNameAndExtension) Object Delete
 * @method $this il(string $graphicDeviceNameAndExtension) Image Load
 * @method $this im(string $graphicDeviceNameAndExtension) Image Move
 * @method $this is(string $graphicDeviceNameAndExtension, bool $printAfterStoring = true) Image Save
 * @method $this ja() Cancel All
 * @method $this jb(string $device = null) Initialize Flash Memory
 * @method $this jc() Set Media Sensor Calibration
 * @method $this jd() Enable Communications Diagnostics
 * @method $this je() Disable Diagnostics
 * @method $this jf(bool $pauseOnLowVoltage = true) Set Battery Condition
 * @method $this jg() Graphing Sensor Calibration
 * @method $this jh(string $mediaOrSuppliesWarning, int $labelsPerRoll, bool $mediaReplaced, string $ribbonLength, bool $ribbonReplaced, string $maintenance, int $headCleaningInterval, bool $headClean, int $headLifeThreshold, bool $headReplaced) Early Warning Settings
 * @method $this ji(string $programDeviceNameAndExtension, bool $console, bool $echo, string $memoryAllocation) Start ZBI (Zebra BASIC Interpreter)
 * @method $this jj(int $operationalMode, int $applicationMode, string $startPrintSignalMode, string $labelErrorMode, string $reprintMode, string $ribbonLowMode) Set Auxiliary Port
 * @method $this jl() Set Label Length
 * @method $this jm(string $dotsPerMillimeter) Set Dots per Millimeter
 * @method $this jn() Head Test Fatal
 * @method $this jo() Head Test Non-Fatal
 * @method $this jp() Pause and Cancel Format
 * @method $this jq() Terminate Zebra BASIC Interpreter
 * @method $this jr() Power On Reset
 * @method $this js(string $selection) Sensor Select
 * @method $this jt(int $labelsBetweenHeadTests, bool $manuallySelect, int $firstElementToCheck, int $lastElementToCheck) Head Test Interval
 * @method $this ju(string $configuration) Configuration Update
 * @method $this jw(string $tension) Set Ribbon Tension
 * @method $this jx() Cancel Current Partially Input Format
 * @method $this jz(bool $reprint) Reprint After Error
 * @method $this kb() Kill Battery (Battery Discharge Mode)
 * @method $this kd(int $format) Select Date and Time Format (for Real Time Clock)
 * @method $this kl(int $language) Define Language
 * @method $this kn(string $name, string $description) Define Printer Name
 * @method $this kp(string $password, int $passwordLevel = null) Define Password
 * @method $this lf() List Font Links
 * @method $this lh(int $xPositionInDots, int $yPositionInDots) Label Home
 * @method $this ll(int $yPositionInDots) Label Length
 * @method $this lr(bool $reverse) Label Reverse Print
 * @method $this ls(int $shiftLeftInDots) Label Shift
 * @method $this lt(int $topInDotRows) Label Top
 * @method $this ma(string $type, bool $print, string $printLabelThreshold, int $frequency, string $units) Set Maintenance Alerts
 * @method $this mc(bool $clear) Map Clear
 * @method $this md(int $darknessLevel) Media Darkness
 * @method $this mf(string $powerUpAction, string $closePrintHeadAction) Media Feed
 * @method $this mi(string $type, string $message) Set Maintenance Information Message
 * @method $this ml(int $maxLengthInDots) Maximum Label Length
 * @method $this mm(string $mode, bool $prepeel) Print Mode
 * @method $this mn(string $media, int $blackMarkOffsetInDots) Media Tracking
 * @method $this mp(string $protectMode) Mode Protection
 * @method $this mt(string $mediaType) Media Type
 * @method $this mu(string $units, int $baseInDpi, $desiredDpi) Set Units of Measurement
 * @method $this mw(bool $warn) Modify Head Cold Warning
 * @method $this nc(int $device) Select the Primary Network Device
 * @method $this nd(int $device, string $ipResolution, string $ipAddress, string $subnetMask, string $defaultGateway, string $winsServer, bool $checkConnectTimeout, int $connectTimeoutSecs, int $arpIntervalMins, int $rawPort) Change Network Settings
 * @method $this ni(string $networkId) Network ID Number
 * @method $this nr() Set All Network Printers Transparent
 * @method $this ns(string $ipResolution, string $ipAddress, string $subnetMask, string $defaultGateway, string $winsServer, bool $checkConnectTimeout, int $connectTimeoutSecs, int $arpIntervalMins, int $rawPort) Change Wired Network Settings
 * @method $this nt() Set Currently Connected Printer Transparent
 */
class ZebraZplBuilder extends Builder {
}
