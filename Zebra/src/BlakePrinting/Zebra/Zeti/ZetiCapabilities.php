<?php

namespace BlakePrinting\Zebra\Zeti;

class ZetiCapabilities {
    /** @var string */
    private $serialNumber = null;
    /** @var string */
    private $modelName = null;
    /** @var string */
    private $manufacturerName = null;
    /** @var string */
    private $manufacturingDate = null;
    /** @var string */
    private $scannerName = null;
    /** @var string */
    private $asciiVersion = null;
    /** @var string */
    private $selectFilters = null;
    /** @var int */
    private $minPower = null;
    /** @var int */
    private $maxPower = null;
    /** @var int */
    private $powerSteps = null;
    /** @var string */
    private $airProtocolVersion = null;
    /** @var string */
    private $maxAccessSequence = null;
    /** @var string */
    private $bdAddress = null;

    /**
     * @return string
     */
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * @param string $serialNumber
     */
    public function setSerialNumber($serialNumber)
    {
        $this->serialNumber = $serialNumber;
    }

    /**
     * @return string
     */
    public function getModelName()
    {
        return $this->modelName;
    }

    /**
     * @param string $modelName
     */
    public function setModelName($modelName)
    {
        $this->modelName = $modelName;
    }

    /**
     * @return string
     */
    public function getManufacturerName()
    {
        return $this->manufacturerName;
    }

    /**
     * @param string $manufacturerName
     */
    public function setManufacturerName($manufacturerName)
    {
        $this->manufacturerName = $manufacturerName;
    }

    /**
     * @return string
     */
    public function getManufacturingDate()
    {
        return $this->manufacturingDate;
    }

    /**
     * @param string $manufacturingDate
     */
    public function setManufacturingDate($manufacturingDate)
    {
        $this->manufacturingDate = $manufacturingDate;
    }

    /**
     * @return string
     */
    public function getScannerName()
    {
        return $this->scannerName;
    }

    /**
     * @param string $scannerName
     */
    public function setScannerName($scannerName)
    {
        $this->scannerName = $scannerName;
    }

    /**
     * @return string
     */
    public function getAsciiVersion()
    {
        return $this->asciiVersion;
    }

    /**
     * @param string $asciiVersion
     */
    public function setAsciiVersion($asciiVersion)
    {
        $this->asciiVersion = $asciiVersion;
    }

    /**
     * @return string
     */
    public function getSelectFilters()
    {
        return $this->selectFilters;
    }

    /**
     * @param string $selectFilters
     */
    public function setSelectFilters($selectFilters)
    {
        $this->selectFilters = $selectFilters;
    }

    /**
     * @return int
     */
    public function getMinPower()
    {
        return $this->minPower;
    }

    /**
     * @param int $minPower
     */
    public function setMinPower($minPower)
    {
        $this->minPower = $minPower;
    }

    /**
     * @return int
     */
    public function getMaxPower()
    {
        return $this->maxPower;
    }

    /**
     * @param int $maxPower
     */
    public function setMaxPower($maxPower)
    {
        $this->maxPower = $maxPower;
    }

    /**
     * @return int
     */
    public function getPowerSteps()
    {
        return $this->powerSteps;
    }

    /**
     * @param int $powerSteps
     */
    public function setPowerSteps($powerSteps)
    {
        $this->powerSteps = $powerSteps;
    }

    /**
     * @return string
     */
    public function getAirProtocolVersion()
    {
        return $this->airProtocolVersion;
    }

    /**
     * @param string $airProtocolVersion
     */
    public function setAirProtocolVersion($airProtocolVersion)
    {
        $this->airProtocolVersion = $airProtocolVersion;
    }

    /**
     * @return string
     */
    public function getMaxAccessSequence()
    {
        return $this->maxAccessSequence;
    }

    /**
     * @param string $maxAccessSequence
     */
    public function setMaxAccessSequence($maxAccessSequence)
    {
        $this->maxAccessSequence = $maxAccessSequence;
    }

    /**
     * @return string
     */
    public function getBdAddress()
    {
        return $this->bdAddress;
    }

    /**
     * @param string $bdAddress
     */
    public function setBdAddress($bdAddress)
    {
        $this->bdAddress = $bdAddress;
    }
}
