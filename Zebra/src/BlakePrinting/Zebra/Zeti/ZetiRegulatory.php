<?php

namespace BlakePrinting\Zebra\Zeti;

class ZetiRegulatory {
    /** @var string */
    private $region = null;
    /** @var bool */
    private $hopping = null;
    /** @var int[] */
    private $enabledChannels = [];

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return boolean
     */
    public function isHopping()
    {
        return $this->hopping;
    }

    /**
     * @param boolean $hopping
     */
    public function setHopping($hopping)
    {
        $this->hopping = $hopping;
    }

    /**
     * @return int[]
     */
    public function getEnabledChannels()
    {
        return $this->enabledChannels;
    }

    /**
     * @param int[] $enabledChannels
     */
    public function setEnabledChannels($enabledChannels)
    {
        $this->enabledChannels = $enabledChannels;
    }
}
