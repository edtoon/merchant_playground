<?php

namespace BlakePrinting\Zebra\Zeti;

use BlakePrinting\Util\Utils;

class ZetiUtil {
    private function __construct() { }

    /**
     * @param resource $fd
     */
    public static function &connect($fd, $password = null, $override = false, $disableLowPower = false) {
        $response = "";
        $status = null;

        fwrite($fd,
            "connect" .
            ($password !== null ? (" .password " . $password) : "") .
            ($override === true ? (" .override") : "") .
            ($disableLowPower === true ? (" .disablelowpower") : "") .
            "\n"
        );

        while(($read = fread($fd, 1024)) !== false) {
            $response .= $read;

            echo($read);

            if(strpos($response, "Command:connect ,Status:") !== false && substr($response, -2, 2) == "\r\n") {
                $status = trim(substr($response, strpos($response, "Status:") + 7));

                break;
            }
        }

        return $status;
    }

    /**
     * @param resource $fd
     * @return ZetiCapabilities
     */
    public static function &getCapabilities($fd) {
        $zetiCapabilities = null;
        $response = "";
        $lastLineCount = 0;

        fwrite($fd, "getcapabilities\n");

        while(($read = fread($fd, 1024)) !== false) {
            $response .= $read;

            echo($read);

            $lines = explode("\r\n", $response);
            $lineCount = count($lines);

            if($lastLineCount != $lineCount) {
                $lastLineCount = $lineCount;

                if($lineCount > 2 && strpos($response, "Command:getcapabilities ,Status:OK,Name:,Value:") !== false &&
                    substr($response, -2, 2) == "\r\n" && empty($lines[$lineCount - 1]) && empty($lines[$lineCount - 2])
                ) {
                    $zetiCapabilities = new ZetiCapabilities();

                    foreach($lines as &$line) {
                        $line = trim($line);
                        $lastCommaPos = strrpos($line, ",");

                        if($lastCommaPos !== false) {
                            $prefix = substr($line, 0, $lastCommaPos);
                            $value = substr($line, $lastCommaPos + 1);

                            switch($prefix) {
                                case ",,SERIAL_NUMBER":
                                    $zetiCapabilities->setSerialNumber($value);
                                break;
                                case ",,MODEL_NAME":
                                    $zetiCapabilities->setModelName($value);
                                break;
                                case ",,MANUFACTURER_NAME":
                                    $zetiCapabilities->setManufacturerName($value);
                                break;
                                case ",,MANUFACTURING_DATE":
                                    $zetiCapabilities->setManufacturingDate($value);
                                break;
                                case ",,SCANNER_NAME":
                                    $zetiCapabilities->setScannerName($value);
                                break;
                                case ",,ASCII_VERSION":
                                    $zetiCapabilities->setAsciiVersion($value);
                                break;
                                case ",,SELECT_FILTERS":
                                    $zetiCapabilities->setSelectFilters($value);
                                break;
                                case ",,MIN_POWER":
                                    $zetiCapabilities->setMinPower($value);
                                break;
                                case ",,MAX_POWER":
                                    $zetiCapabilities->setMaxPower($value);
                                break;
                                case ",,POWER_STEPS":
                                    $zetiCapabilities->setPowerSteps($value);
                                break;
                                case ",,AIR_PROTOCOL_VERSION":
                                    $zetiCapabilities->setAirProtocolVersion($value);
                                break;
                                case ",,MAX_ACCESS_SEQUENCE":
                                    $zetiCapabilities->setMaxAccessSequence($value);
                                break;
                                case ",,BD_ADDRESS":
                                    $zetiCapabilities->setBdAddress($value);
                                break;
                            }
                        }
                    }

                    unset($line);

                    break;
                }
            }
        }

        return $zetiCapabilities;
    }

    /**
     * @param resource $fd
     * @return ZetiDeviceInfo
     */
    public static function &getDeviceInfo($fd) {
        $zetiDeviceInfo = null;
        $response = "";
        $lastLineCount = 0;

        fwrite($fd, "getdeviceinfo .battery .temperature .power\n");

        while(($read = fread($fd, 1024)) !== false) {
            $response .= $read;

            echo($read);

            $lines = explode("\r\n", $response);
            $lineCount = count($lines);

            if($lastLineCount != $lineCount) {
                $lastLineCount = $lineCount;

                if($lineCount > 3 && strpos($response, "Power:") !== false && substr($response, -2, 2) == "\r\n" && empty($lines[$lineCount - 1])) {
                    $zetiDeviceInfo = new ZetiDeviceInfo();

                    foreach($lines as &$line) {
                        $line = trim($line);

                        if(strpos($line, "Notification:") !== 0) {
                            continue;
                        }

                        $parts = explode(",", trim($line));
                        $lineValues = [];

                        foreach($parts as &$part) {
                            $colonPos = strpos($part, ":");

                            if($colonPos !== false) {
                                $lineValues[substr($part, 0, $colonPos)] = substr($part, $colonPos + 1);
                            }
                        }

                        unset($part);

                        switch($lineValues["Notification"]) {
                            case "BatteryEvent":
                                if(isset($lineValues["Cause"])) {
                                    $zetiDeviceInfo->setBatteryCause($lineValues["Cause"]);
                                }

                                if(isset($lineValues["Level"])) {
                                    $zetiDeviceInfo->setBatteryLevel($lineValues["Level"]);
                                }

                                if(isset($lineValues["Charging"])) {
                                    $zetiDeviceInfo->setBatteryCharging(Utils::getBooleanOrNull($lineValues["Charging"]));
                                }
                            break;
                            case "TemperatureEvent":
                                if(isset($lineValues["Cause"])) {
                                    $zetiDeviceInfo->setTemperatureCause($lineValues["Cause"]);
                                }

                                if(isset($lineValues["STM32 Temp"])) {
                                    $zetiDeviceInfo->setCpuTemperature($lineValues["STM32 Temp"]);
                                }

                                if(isset($lineValues["Radio PA Temp"])) {
                                    $zetiDeviceInfo->setRadioTemperature($lineValues["Radio PA Temp"]);
                                }
                            break;
                            case "PowerEvent":
                                if(isset($lineValues["Cause"])) {
                                    $zetiDeviceInfo->setPowerCause($lineValues["Cause"]);
                                }

                                if(isset($lineValues["Voltage"])) {
                                    $zetiDeviceInfo->setVoltage($lineValues["Voltage"]);
                                }

                                if(isset($lineValues["Current"])) {
                                    $zetiDeviceInfo->setCurrent($lineValues["Current"]);
                                }

                                if(isset($lineValues["Power"])) {
                                    $zetiDeviceInfo->setPower($lineValues["Power"]);
                                }
                            break;
                        }
                    }

                    unset($line);

                    break;
                }
            }
        }

        return $zetiDeviceInfo;
    }

    /**
     * @param resource $fd
     * @return ZetiRegulatory
     */
    public static function &getRegulatory($fd) {
        $zetiRegulatory = null;
        $response = "";

        fwrite($fd, "setregulatory .noexec\n");

        while(($read = fread($fd, 1024)) !== false) {
            $response .= $read;

            echo($read);

            if(strpos($response, "Command:setregulatory ") !== false && substr($response, -2, 2) == "\r\n") {
                $zetiRegulatory = new ZetiRegulatory();
                $words = explode(" ", $response);
                $enabledChannels = [];
                $variable = null;

                foreach($words as &$word) {
                    if(!empty($word)) {
                        switch($word) {
                            case ".region":
                                $variable = "region";
                            break;
                            case ".hoppingon":
                                $variable = null;

                                $zetiRegulatory->setHopping(true);
                            break;
                            case ".enabledchannels":
                                $variable = "enabledchannels";
                            break;
                            case ".noexec":
                                $variable = null;
                            break;
                            default:
                                if($variable !== null) {
                                    switch($variable) {
                                        case "region":
                                            $zetiRegulatory->setRegion($word);
                                        break;
                                        case "enabledchannels":
                                            if(is_numeric($word)) {
                                                $enabledChannels[] = $word;
                                            } else {
                                                $variable = null;
                                            }
                                        break;
                                    }
                                }
                            break;
                        }
                    }
                }

                unset($word);

                $zetiRegulatory->setEnabledChannels($enabledChannels);

                break;
            }
        }

        return $zetiRegulatory;
    }

    /**
     * @param resource $fd
     * @return ZetiVersion
     */
    public static function &getVersion($fd) {
        $zetiVersion = null;
        $response = "";
        $lastLineCount = 0;

        fwrite($fd, "getversion\n");

        while(($read = fread($fd, 1024)) !== false) {
            $response .= $read;

            echo($read);

            $lines = explode("\r\n", $response);
            $lineCount = count($lines);

            if($lastLineCount != $lineCount) {
                $lastLineCount = $lineCount;

                if($lineCount > 2 && strpos($response, "Command:getversion ,Status:OK,Device:,Version:") !== false &&
                    substr($response, -2, 2) == "\r\n" && empty($lines[$lineCount - 1]) && empty($lines[$lineCount - 2])
                ) {
                    $zetiVersion = new ZetiVersion();

                    foreach($lines as &$line) {
                        $line = trim($line);
                        $lastCommaPos = strrpos($line, ",");

                        if($lastCommaPos !== false) {
                            $prefix = substr($line, 0, $lastCommaPos);
                            $value = substr($line, $lastCommaPos + 1);

                            if($value !== false) {
                                switch($prefix) {
                                    case ",,GENX_DEVICE":
                                        $zetiVersion->setGenXDevice($value);
                                    break;
                                    case ",,BLUETOOTH":
                                        $zetiVersion->setBluetooth($value);
                                    break;
                                    case ",,NGE":
                                        $zetiVersion->setNge($value);
                                    break;
                                    case ",,PL33":
                                        $zetiVersion->setPl33($value);
                                    break;
                                    case ",,HARDWARE":
                                        $zetiVersion->setHardware($value);
                                    break;
                                }
                            }
                        }
                    }

                    unset($line);

                    break;
                }
            }
        }

        return $zetiVersion;
    }
}
