<?php

namespace BlakePrinting\Zebra\Zeti;

class ZetiVersion {
    /** @var string */
    private $genXDevice = null;
    /** @var string */
    private $bluetooth = null;
    /** @var string */
    private $nge = null;
    /** @var string */
    private $pl33 = null;
    /** @var string */
    private $hardware = null;

    /**
     * @return string
     */
    public function getGenXDevice()
    {
        return $this->genXDevice;
    }

    /**
     * @param string $genXDevice
     */
    public function setGenXDevice($genXDevice)
    {
        $this->genXDevice = $genXDevice;
    }

    /**
     * @return string
     */
    public function getBluetooth()
    {
        return $this->bluetooth;
    }

    /**
     * @param string $bluetooth
     */
    public function setBluetooth($bluetooth)
    {
        $this->bluetooth = $bluetooth;
    }

    /**
     * @return string
     */
    public function getNge()
    {
        return $this->nge;
    }

    /**
     * @param string $nge
     */
    public function setNge($nge)
    {
        $this->nge = $nge;
    }

    /**
     * @return string
     */
    public function getPl33()
    {
        return $this->pl33;
    }

    /**
     * @param string $pl33
     */
    public function setPl33($pl33)
    {
        $this->pl33 = $pl33;
    }

    /**
     * @return string
     */
    public function getHardware()
    {
        return $this->hardware;
    }

    /**
     * @param string $hardware
     */
    public function setHardware($hardware)
    {
        $this->hardware = $hardware;
    }
}
