<?php

namespace BlakePrinting\Zebra\Zeti;

class ZetiDeviceInfo {
    /** @var string */
    private $batteryCause = null;
    /** @var int */
    private $batteryLevel = null;
    /** @var bool */
    private $batteryCharging = null;
    /** @var string */
    private $temperatureCause = null;
    /** @var int */
    private $cpuTemperature = null;
    /** @var int */
    private $radioTemperature = null;
    /** @var string */
    private $powerCause = null;
    /** @var float */
    private $voltage = null;
    /** @var float */
    private $current = null;
    /** @var float */
    private $power = null;

    /**
     * @return string
     */
    public function getBatteryCause()
    {
        return $this->batteryCause;
    }

    /**
     * @param string $batteryCause
     */
    public function setBatteryCause($batteryCause)
    {
        $this->batteryCause = $batteryCause;
    }

    /**
     * @return int
     */
    public function getBatteryLevel()
    {
        return $this->batteryLevel;
    }

    /**
     * @param int $batteryLevel
     */
    public function setBatteryLevel($batteryLevel)
    {
        $this->batteryLevel = $batteryLevel;
    }

    /**
     * @return boolean
     */
    public function isBatteryCharging()
    {
        return $this->batteryCharging;
    }

    /**
     * @param boolean $batteryCharging
     */
    public function setBatteryCharging($batteryCharging)
    {
        $this->batteryCharging = $batteryCharging;
    }

    /**
     * @return string
     */
    public function getTemperatureCause()
    {
        return $this->temperatureCause;
    }

    /**
     * @param string $temperatureCause
     */
    public function setTemperatureCause($temperatureCause)
    {
        $this->temperatureCause = $temperatureCause;
    }

    /**
     * @return int
     */
    public function getCpuTemperature()
    {
        return $this->cpuTemperature;
    }

    /**
     * @param int $cpuTemperature
     */
    public function setCpuTemperature($cpuTemperature)
    {
        $this->cpuTemperature = $cpuTemperature;
    }

    /**
     * @return int
     */
    public function getRadioTemperature()
    {
        return $this->radioTemperature;
    }

    /**
     * @param int $radioTemperature
     */
    public function setRadioTemperature($radioTemperature)
    {
        $this->radioTemperature = $radioTemperature;
    }

    /**
     * @return string
     */
    public function getPowerCause()
    {
        return $this->powerCause;
    }

    /**
     * @param string $powerCause
     */
    public function setPowerCause($powerCause)
    {
        $this->powerCause = $powerCause;
    }

    /**
     * @return float
     */
    public function getVoltage()
    {
        return $this->voltage;
    }

    /**
     * @param float $voltage
     */
    public function setVoltage($voltage)
    {
        $this->voltage = $voltage;
    }

    /**
     * @return float
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * @param float $current
     */
    public function setCurrent($current)
    {
        $this->current = $current;
    }

    /**
     * @return float
     */
    public function getPower()
    {
        return $this->power;
    }

    /**
     * @param float $power
     */
    public function setPower($power)
    {
        $this->power = $power;
    }
}
