<?php

namespace BlakePrinting\Zebra;

use Exception;

class ZebraSocketUtil {
    private function __construct() { }

    /**
     * @param bool $require
     * @param float $initialWait
     * @param float $timeout
     * @param bool $ensureFullXml
     * @return string
     */
    public static function &readResponse($socket, $require = false, $initialWait = 0.75, $timeout = 5, $ensureFullXml = true) {
        $buffer = null;
        $response = "";
        $start = microtime(true);

        while(true) {
            while(false !== ($bytes = socket_recv($socket, $buffer, 1024, MSG_DONTWAIT))) {
                if($bytes > 0) {
                    if($buffer !== null) {
                        $response .= $buffer;

                        echo($buffer);
                    }
                }
            }

            $current = microtime(true);

            if($timeout > 0 && $current > ($start + $timeout)) {
                throw new Exception("Timed out waiting for response from socket");
            } else if($require && empty($response)) {
                continue;
            } else if($initialWait > 0 && $current > ($start + $initialWait)) {
                if($ensureFullXml) {
                    if(strpos($response, "<ZEBRA-ELTRON-PERSONALITY>") !== FALSE && strpos($response, "</ZEBRA-ELTRON-PERSONALITY>") === false) {
                        continue;
                    }
                }

                break;
            }
        }

        return $response;
    }
}
