<?php

namespace BlakePrinting\Zebra;

use Zebra\Client;

class ZebraZplClient extends Client {
    /**
     * @param string $str
     * @return string
     */
    public function &sendAndGetResponse($str, $require = false, $initialWait = 0.75, $timeout = 5) {
        $this->send($str);

        return $this->getResponse($require, $initialWait, $timeout);
    }

    /**
     * @param bool $require
     * @param float $initialWait
     * @param float $timeout
     * @return string
     */
    public function &getResponse($require = false, $initialWait = 0.75, $timeout = 5) {
        return ZebraSocketUtil::readResponse($this->socket, $require, $initialWait, $timeout);
    }

    /**
     * @return resource
     */
    public function &getSocket() {
        return $this->socket;
    }

    /**
     * @return array
     */
    public function getLastError() {
        return parent::getLastError();
    }

    public function disconnect() {
        if($this->socket !== null) {
            parent::disconnect();
        }

        $this->socket = null;
    }
}
